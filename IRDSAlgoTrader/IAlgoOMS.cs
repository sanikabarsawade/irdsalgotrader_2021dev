﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading;
using System.Diagnostics;
using System.Linq;
using System.Data.SQLite;
using System.IO;
using System.Threading.Tasks;
using System.Timers;
using System.Net.NetworkInformation;
using System.Windows;

namespace IRDSAlgoOMS
{
    
    [Guid("8585a564-42c1-4020-b965-427fff8e04e5")]
    [InterfaceType(ComInterfaceType.InterfaceIsIDispatch)]
    public interface IAlgoOMS
    {
        [DispId(99)]
        bool SetBroker(int brokername);

        [DispId(100)]
        string PlaceOrder(string userID,
                        string Exchange,
                        string TradingSymbol,
                        string TransactionType,
                        int Quantity,
                        string OrderType,
                        decimal? Price = null,
                        string Product = Constants.PRODUCT_MIS,
                        string Validity = Constants.VALIDITY_DAY,
                        int? DisclosedQuantity = null,
                        decimal? TriggerPrice = null,
                        decimal? SquareOffValue = null,
                        decimal? StoplossValue = null,
                        decimal? TrailingStoploss = null,
                        string Variety = Constants.VARIETY_REGULAR,      
                        string Tag = "");

        [DispId(101)]
        string GetFutureSymbolName(string userID,string NSESymbol);

        [DispId(102)]
        int GetLotSize(string userID, string TradingSymbol, string Exchange);

        [DispId(103)]
        void CreateKiteConnectObject(string userID, string iniFilePath, MainWindow m_mainWindow = null, bool isFromEXE = false);

        [DispId(104)]
        void WriteLog(string symbol, string logMessage);

        [DispId(105)]
        int ForeceFulRelogin(string userID);

        [DispId(106)]
        int IntrumentsCSVToDB(string userID);

        [DispId(107)]
        bool LoadConfigFile(string userID, string iniFilePath);

        [DispId(108)]
        string PlaceMarketOrder(string userID,
                              string Exchange,
                              string TradingSymbol,
                              string TransactionType,
                              int Quantity,
                              string Product = Constants.PRODUCT_MIS);

        [DispId(109)]
        string PlaceLimitOrder(string userID, 
                             string Exchange,
                             string TradingSymbol,
                             string TransactionType,
                             int Quantity,
                             decimal Price, 
                             string Product = Constants.PRODUCT_MIS);

        [DispId(110)]
        string PlaceStopOrder(string userID, 
                            string Exchange,
                            string TradingSymbol,
                            string TransactionType,
                            int Quantity,
                            int DisclosedQuantity,
                            decimal TriggerPrice,
                            string Product = Constants.PRODUCT_MIS);

        [DispId(111)]
        string PlaceBOLimitOrder(string userID, 
                               string Exchange,
                               string TradingSymbol,
                               string TransactionType,
                               int Quantity,
                               decimal Price,
                               decimal SquareOffValue,
                               decimal StoplossValue,
                               string Product = Constants.PRODUCT_MIS);

        [DispId(112)]
        string PlaceBOStopOrder(string userID, 
                              string Exchange,
                              string TradingSymbol,
                              string TransactionType,
                              int Quantity,
                              decimal Price,
                              decimal TriggerPrice,
                              decimal SquareOffValue,
                              decimal StoplossValue,
                              string Product = Constants.PRODUCT_MIS);

        [DispId(113)]
        bool IsPendingOrder(string userID, 
                            string TradingSymbol, 
                            string Exchange, 
                            string TransactionType,
                            bool isPendingToOpenOrder,
                            string Product,
                            string OrderStatus = Constants.ORDER_STATUS_PENDING);

        [DispId(114)]
        bool IsPendingLimitOrder(string userID,
                                 string TradingSymbol, 
                                 string Exchange, 
                                 string TransactionType,
                                 string Product = Constants.PRODUCT_MIS);

        [DispId(115)]
        bool IsPendingStopOrder(string userID, 
                                string TradingSymbol, 
                                string Exchange, 
                                string TransactionType,
                                string Product = Constants.PRODUCT_MIS);

        [DispId(116)]//sanika::31-Dec-2020::Added status parameter
        bool CancelPendingOrder(string userID, 
                                string TradingSymbol,
                                string Exchange, 
                                string TransactionType,
                                string Status, 
                                bool isCancelToOpenOrder,
                                string Variety = Constants.VARIETY_REGULAR);

        [DispId(117)]
        bool CancelPendingLimitOrder(string userID, 
                                     string TradingSymbol,
                                     string Exchange, 
                                     string TransactionType);

        [DispId(118)]
        bool CancelPendingStopOrder(string userID, 
                                    string TradingSymbol,
                                    string Exchange, 
                                    string TransactionType);

        [DispId(119)]
        bool ModifyLimitOrder(string userID,
                              string TradingSymbol, 
                              string TransactionType, 
                              decimal price, 
                              string Exchange = Constants.EXCHANGE_NSE, 
                              string Product = Constants.PRODUCT_MIS);

        [DispId(120)]
        bool ModifyStopOrder(string userID, 
                             string TradingSymbol, 
                             string TransactionType, 
                             decimal price, 
                             string Exchange = Constants.EXCHANGE_NSE, 
                             string Product = Constants.PRODUCT_MIS);

        [DispId(121)]
        bool ModifyBOLimitOrder(string userID, 
                                string TradingSymbol, 
                                string TransactionType, 
                                decimal Price, 
                                string Exchange = Constants.EXCHANGE_NSE, 
                                string Product = Constants.PRODUCT_MIS);

        [DispId(122)]
        bool ModifyBOStopOrder(string userID, 
                               string TradingSymbol, 
                               string TransactionType, 
                               decimal Price, 
                               string Exchange = Constants.EXCHANGE_NSE, 
                               string Product = Constants.PRODUCT_MIS);

        [DispId(123)]
        bool CancelAllPendingOrder(string userID);

        [DispId(124)]
        bool CancelPendingBOOrder(string userID, 
                                  string TradingSymbol,
                                  string Exchange, 
                                  string TransactionType);

        [DispId(125)]
        bool CancelPendingBOOrderWithParentOrder(string userID, 
                                                 string TradingSymbol,
                                                 string Exchange, 
                                                 string TransactionType);

        [DispId(126)]
        bool CloseOrder(string userID, 
                        string TradingSymbol,
                        string Exchange = Constants.EXCHANGE_NSE,
                        string Product = Constants.PRODUCT_MIS);

        [DispId(127)]
        bool CloseBOOrder(string userID, string TradingSymbol,string Exchange);

        [DispId(128)]
        bool CloseAllOrder(string userID);

        [DispId(129)]
        double GetLastPrice(string userID, string TradingSymbol);

        [DispId(130)]
        double GetOpenPostionPrice(string userID, 
                                   string TradingSymbol,
                                   string Exchange = Constants.EXCHANGE_NSE,
                                   string Product = Constants.PRODUCT_MIS);

        [DispId(131)]
        bool IsPendingBOOrder(string userID, 
                              string TradingSymbol,
                              string Exchange, 
                              string TransactionType,
                              string Product = Constants.PRODUCT_MIS);

        [DispId(132)]
        bool IsPendingBOOrderWithParent(string userID, 
                                        string TradingSymbol, 
                                        string Exchange, 
                                        string TransactionType,
                                        string Product = Constants.PRODUCT_MIS);

        [DispId(133)]
        PositionResponse GetOpenPosition(string userID);

        [DispId(134)]
        double RoundOff(string userID, string symbol, decimal normalValue, bool flag);

        [DispId(135)]
        void CheckTradingSymbolPresentOrNot(string userID, List<string> list);

        [DispId(136)]
        string getTickCurrentTime();

        [DispId(137)]
        List<string> getOHCLValues(string TradingSymbol);

        [DispId(138)]
        string getVariety(string TradingSymbol, string Exchange);

        [DispId(139)]
        bool GetLoginStatus(string userID);

        [DispId(140)]
        bool stopThread();

        [DispId(141)]
        double GetStopPrice(string userID, 
                            string TradingSymbol, 
                            string Exchange, 
                            string TransactionType);

        [DispId(142)]
        double GetLimitPrice(string userID, 
                             string TradingSymbol, 
                             string Exchange, 
                             string TransactionType);

        [DispId(143)]
        string GetTransactionType(string userID, string TradingSymbol,string Exchange);

        [DispId(144)]
        void WriteDataIntoFile(string userID,
                               string TradingSymbol,
                               string DateTime,
                               double Open,
                               double High,
                               double Low,
                               double Close,
                               double Volume,
                               bool bWriteinDB,
                               bool bCommit);

        [DispId(145)]
        bool GetHighLow(string userID, 
                        string TradingSymbol,
                        int  barCount, 
                        out double dHigh, 
                        out double dLow);

        [DispId(146)]
        double GetOpenPostionPricebyOrderID(string userID, 
                                            string TradingSymbol, 
                                            string Exchange, 
                                            string orderId);

        [DispId(147)]
        double GetOpenPostionPricebySymbol(string userID, 
                                           string TradingSymbol, 
                                           string Exchange);

        [DispId(148)]
        void DownloadData(string userID, 
                          List<string> TradingSymbolList, 
                          int daysForHistroricalData);

        [DispId(149)]
        bool IsPositionOpen(string userID, string TradingSymbol, string Exchange);

        [DispId(150)]
        string GetOpenPostionDirection(string userID, 
                                       string TradingSymbol, 
                                       string Exchange = Constants.EXCHANGE_NSE,
                                       string Product = Constants.PRODUCT_MIS);

        [DispId(151)]
        double GetStopPricebyOrderID(string userID, 
                                     string TradingSymbol, 
                                     string Exchange, 
                                     string orderId);

        [DispId(152)]
        void RollOver(string userID,string TradingSymbol, string Exchange, string Product);

        [DispId(153)]
        void AddOrderIdInMasterList(string userID, string TradingSymbol, string OrderId);

        [DispId(154)]
        void RemoveOrderIdFromMasterList(string userID, string TradingSymbol);

        [DispId(155)]
        string GetOrderIdFromMasterList(string userID, string TradingSymbol);

        [DispId(156)]
        bool CheckWithinTime(string userID, string strDateTime, int interval, string key);

        [DispId(157)]
        void InsertMessageDetails(string userID, 
                                  string datestr, 
                                  int signal, 
                                  string name, 
                                  string key);

        [DispId(158)]
        void GetMessagePosition(string userID, string tablename);

        [DispId(159)]
        string GetOpenPostionDirectionByOrderID(string userID,
                                      string TradingSymbol,
                                      string Exchange,
                                      string OrderId);

        [DispId(160)]
        string GetOrderStatusByOrderID(string userID,
                                      string TradingSymbol,
                                      string Exchange,
                                      string OrderId);

        [DispId(161)]
        bool Connect(string userID);

        [DispId(162)]
        string GetStartTime(string userID);

        [DispId(163)]
        string GetEndTime(string userID);

        [DispId(164)]
        string GetTimelLimitToPlaceOrder(string userID);

        [DispId(165)]
        string GetTimelLimitToExistOrder(string userID);

        [DispId(166)]
        double GetOverAllProfit(string userID);

        [DispId(167)]
        double GetOverAllLoss(string userID);

        [DispId(168)]
        string PlaceCOMarketOrder(string userID,
                               string Exchange,
                               string TradingSymbol,
                               string TransactionType,
                               int Quantity, 
                               decimal TriggerPrice,
                               string Product = Constants.PRODUCT_MIS);

        [DispId(169)]
        string PlaceCOLimitOrder(string userID,
                               string Exchange,
                               string TradingSymbol,
                               string TransactionType,
                               int Quantity,
                               decimal Price,
                               decimal TriggerPrice,
                               string Product = Constants.PRODUCT_MIS);

        [DispId(170)]
        bool CloseCOOrder(string userID, 
                          string TradingSymbol, 
                          string Exchange = Constants.EXCHANGE_NSE, 
                          string Product = Constants.PRODUCT_MIS);

        [DispId(171)]
        bool CloseCOOrderWithOrderId(string userID, 
                                     string OrderId, 
                                     string Product = Constants.VARIETY_CO);

        [DispId(172)]
        string GetOrderId(string userID, 
                          string TradingSymbol, 
                          string Exchange ,
                          string TransactionType,
                          string Product);

        [DispId(173)]
        string GetParentOrderId(string userID, 
                                string orderId, 
                                string TradingSymbol, 
                                string Exchange);

        [DispId(174)]
        bool GetBidAndAskPrice(string userID, 
                               long InstrumentId,
                               string Exchange, 
                               out double BidPrice, 
                               out double AskPrice);

        [DispId(175)]
        int GetOpenPostionQuantityByOrderID(string userID,
                                      string TradingSymbol,
                                      string Exchange,
                                      string OrderId);

        [DispId(176)]
        int GetOpenPostionQuantity(string userID,
                                    string TradingSymbol,
                                    string Exchange,
                                    string Product);

        [DispId(177)]
        bool CloseOrderWithQuantity(string userID,
                                    string TradingSymbol,
                                    int Quantity,
                                    string Exchange = Constants.EXCHANGE_NSE,
                                    string Product = Constants.PRODUCT_MIS);
        
        [DispId(178)]
        void WriteProfitIntoDB(string userID, 
                               string tblName, 
                               string TradingSymbol, 
                               string Datetime, 
                               double Profit);

        [DispId(179)]
        double GetProfitFromDB(string userID, string tblName, string symbol, string datetime);

        [DispId(180)]
        void DeleteAllProfitRecords(string userID, string tblName);

        [DispId(181)]
        double GetTotalProfitFromDB(string userID, string tblName, string datetime);

        [DispId(182)]
        bool SetSymbolLastOrder(string userID, string TradingSymbol, string TransactionType);

        [DispId(183)]
        bool RemoveSymbolLastOrder(string userID, string TradingSymbol, string TransactionType);

        [DispId(184)]
        string GetSymbolLastOrder(string userID, string TradingSymbol);

        [DispId(185)]
        bool AddManuallyOrderQuantity(string userID, string TradingSymbol, int Quantity);

        [DispId(186)]
        bool RemoveManuallyOrderQuantity(string userID, string TradingSymbol);

        [DispId(187)]
        int GetManuallyOrderQuantity(string userID, string TradingSymbol);

        [DispId(188)]
        int GetOrderQuantityByDirection(string userID, 
                                        string TradingSymbol, 
                                        string TransactionType);

        [DispId(189)]
        bool GetChangeInPercent(string userID, 
                                string TradingSymbol,
                                string tickTime,
                                out double highPercent, 
                                out double lowPercent);

        [DispId(190)]
        string CloseOrderEx(string userID, 
                            string TradingSymbol, 
                            string Exchange = Constants.EXCHANGE_NSE, 
                            string Product = Constants.PRODUCT_MIS);

        [DispId(191)]
        bool GetHighLowFromDB(string userID, 
                              string TradingSymbol, 
                              string StartTime,
                              string EndTime,
                              out double dHigh, 
                              out double dLow);

        [DispId(192)]
        string FetchTableName(string userID);

        //IRDSPM::Pratiksha::06-11-2020::For fetching table details
        [DispId(193)]
        List<string> FetchDetails(string exchange, string userID);

        [DispId(194)]
        string GetOpenPostionProdcutByOrderID(string userID,
                                   string TradingSymbol,
                                   string Exchange,
                                   string OrderId);

        [DispId(195)]
        bool RiskManagement(string userID, 
                            string TradingSymbol, 
                            string Exchange, 
                            double RiskPercent);

        [DispId(196)]
        void GetFirstTwoTick(string userID, 
                             string TradingSymbol, 
                             string FirstTickTime, 
                             string SecondTickTime, 
                             out double firstCandleHigh, 
                             out double firstCandleLow, 
                             out double secondCandleHigh, 
                             out double secondCandleLow);

        [DispId(197)]
        void GetCurrentHighLow(string userID,
                               string TradingSymbol,                                
                               string startTime, 
                               string endTime, 
                               int Interval,
                               out double dHigh, 
                               out double dLow);

        [DispId(198)]
        void GetTwoTickFromHistoricalData(string userID, 
                                          string TradingSymbol, 
                                          string FirstTickTime, 
                                          string SecondTickTime, 
                                          int interval, 
                                          out double firstCandleHigh, 
                                          out double firstCandleLow, 
                                          out double secondCandleHigh, 
                                          out double secondCandleLow);
        [DispId(199)]
        List<string> GetHighLowClose(string userID,
                                     string TradingSymbol,
                                     int interval,
                                     int period,
                                     string startTime,
                                     string endTime);

        [DispId(200)]
        bool ModifyStopOrderWithOrderId(string userID,
                                        string TradingSymbol,
                                        string TransactionType,
                                        decimal TriggerPrice,
                                        string OrderId,
                                        int Quantity,
                                        string Exchange = Constants.EXCHANGE_NSE,
                                        string Product = Constants.PRODUCT_MIS);

        [DispId(201)]
        bool CancelOrder(string userID,
                         string orderId,
                         string TradingSymbol,
                         string ProductType = Constants.VARIETY_REGULAR);

        [DispId(202)]
        bool IsOverallAllProfitLossHit(string userID);

        [DispId(203)]
        double GetIndividualProfitLoss(string userID, string TradingSymbol, string Exchange);

        [DispId(204)]
        double GetPNL(string userID);

        [DispId(205)]
        double GetLastBarLow(string userID, string TradingSymbol);

        [DispId(206)]
        bool ModifySLOrderWithOrderId(string userID,
                                       string TradingSymbol,
                                       string TransactionType,
                                       decimal Price,
                                       decimal TriggerPrice,
                                       string OrderId,
                                       int Quantity,
                                       string Exchange = Constants.EXCHANGE_NSE,
                                       string Product = Constants.PRODUCT_MIS);
        
        [DispId(207)]
        double GetLastBarHigh(string userID, string TradingSymbol);

        [DispId(208)]
        double GetBidPrice(string userID, string TradingSymbol);

        [DispId(209)]
        double GetAskPrice(string userID, string TradingSymbol);

        //IRDSPM::Pratiksha::28-04-2021::For symbol setting form
        [DispId(210)]
        List<string> GetOptionsSymbols();
        [DispId(211)]
        List<string> fetchExpiryDtForOptionSymbol(string symbol, string table);


        [DispId(212)]
        double GetLimitPricebyOrderID(string userID,
                                     string TradingSymbol,
                                     string Exchange,
                                     string orderId);
        [DispId(213)]
        double GetMargin(string userID);

        //IRDS::Pratiksha::07-06-2021::For fetching quantity from db for selected symbol and expiry
        [DispId(214)]
        string fetchLotsizeForOptionSymbol(string symbol, string table, string expiryDt);
        [DispId(215)]
        void ClearMasterList(string userID);

        [DispId(216)]
        void AddNotificationInQueue(string userID, string message);

        [DispId(217)]
        void AddGUINotification(string userID, string Message);

        [DispId(218)]
        void AddNotificationInQueueForToast(string userID, string message);
        [DispId(219)]
        List<string> GetInstrumentsSymbols(string Exchange);

        [DispId(220)]
        void CreateKiteConnectObject(string userID, string iniFilePath, bool isFromEXE = false);
        [DispId(221)]
        bool GetChangeInPercentIndex(string userID,
                                     string TradingSymbol,
                                     string tickTime,
                                     out double highPercent,
                                     out double lowPercent);
    }

    //[Guid("79AEE69F-5187-4C5E-8DED-A5A5D9DA1D57"),

    [Guid("89daaa5a-3a69-4179-bd7b-861320219970"),
    ClassInterface(ClassInterfaceType.None)]
    public class AlgoOMS : IAlgoOMS
    {
        public bool isZerodhaBroker = true;
        public bool isSamcoBroker = false;
        public bool isCompositeBroker = false;

        Dictionary<string, KiteConnectWrapper> kiteConnectObjects = new Dictionary<string, KiteConnectWrapper>();
        Dictionary<string, SamcoConnectWrapper> samcoConnectObjects = new Dictionary<string, SamcoConnectWrapper>();
        Dictionary<string, CompositeConnectWrapper> compositeConnectObjects = new Dictionary<string, CompositeConnectWrapper>();
        Thread checkTime = null;

        SQLiteConnection sql_con;
        SQLiteCommand sql_cmd;
        SQLiteDataReader sql_read;
        SQLiteTransaction tr;

        static readonly object _CreateKiteobject = new object();
        static readonly object _CreateSamcoobject = new object();
        static readonly object _CreateCompositeobject = new object();
        static readonly object _CreateWindows = new object();

        static ExecutionForm m_objExecutionForm = null;
        static MainWindow m_objMainWindow = null;

        string currentTime = "";
        public KiteConnectWrapper kiteWrapperConnect = null;
        public SamcoConnectWrapper samcoWrapperConnect = null;
        public CompositeConnectWrapper compositeWrapperConnect = null;
        public AlgoOMS()
        {
            //nothing to do
        }

        public string GetFutureSymbolName(string userID,string TradingSymbol) //trading symbol
        {
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp = new KiteConnectWrapper();
                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    return null;
                }
                TradingSymbol += kiteConnectWrapperTemp.FetchTableName();
            }
            else if (isSamcoBroker)
            {
                SamcoConnectWrapper samcoConnectWrapperTemp = new SamcoConnectWrapper();
                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    return null;
                }

                TradingSymbol += samcoConnectWrapperTemp.FetchTableName();
            }
            else
            {
                CompositeConnectWrapper compositeConnectWrapperTemp = new CompositeConnectWrapper();
                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                }
                else
                {
                    return "";
                }

                TradingSymbol += compositeConnectWrapperTemp.FetchTableName();
            }
            return TradingSymbol;
        }

        public int GetLotSize(string userID, string TradingSymbol, string Exchange)
        {
            int lotSize = 0;
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp = new KiteConnectWrapper();
                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    return 0;
                }
                lotSize = kiteConnectWrapperTemp.GetLotSize(TradingSymbol, Exchange);
            }
            else if (isSamcoBroker)
            {
                SamcoConnectWrapper samcoConnectWrapperTemp = new SamcoConnectWrapper();
                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    return 0;
                }

                lotSize = samcoConnectWrapperTemp.GetLotSize(TradingSymbol, Exchange);
            }
            else
            {
                CompositeConnectWrapper compositeConnectWrapperTemp = new CompositeConnectWrapper();
                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                }
                else
                {
                    return 0;
                }

                lotSize = compositeConnectWrapperTemp.GetLotSize(TradingSymbol, Exchange);
            }
            WriteLog(TradingSymbol + " " + userID, "GetLotSize :  lotSize " + lotSize);
            return lotSize;
        }

        public string PlaceOrder(string userID,
                               string Exchange,//NSE,NFO
                               string TradingSymbol,//NormalSymbolName
                               string TransactionType,//"BUY","SELL"
                               int Quantity,//in numbers
                               string OrderType,//"MARKET"//"LIMIT"//"SL-M"//"SL"
                               decimal? Price = null,
                               string Product = Constants.PRODUCT_MIS,//"MIS""CNC""NRML"                    
                               string Validity = Constants.VALIDITY_DAY,//""DAY"",//"IOC"
                               int? DisclosedQuantity = null,
                               decimal? TriggerPrice = null,
                               decimal? SquareOffValue = null,
                               decimal? StoplossValue = null,
                               decimal? TrailingStoploss = null,
                               string Variety = Constants.VARIETY_REGULAR,        //"regular","bo","co","amo";
                               string Tag = "")
        {
            
            string orderId = "";
            if (isZerodhaBroker)
            {
            KiteConnectWrapper kiteConnectWrapperTemp = new KiteConnectWrapper();
            if (kiteConnectObjects.ContainsKey(userID))
            {
                kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
            }
            else
            {
                return "";
            }
           
                orderId = kiteConnectWrapperTemp.PlaceOrder(Exchange, TradingSymbol, TransactionType, Quantity, OrderType, Price, Product, Validity, DisclosedQuantity, TriggerPrice, SquareOffValue, StoplossValue, TrailingStoploss, Variety, Tag);
            WriteLog(TradingSymbol + " " + userID, "PlaceOrder : orderId " + orderId);
            }
            else if(isSamcoBroker)
            {
                SamcoConnectWrapper samcoConnectWrapperTemp = new SamcoConnectWrapper();
                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    return "";
                }

                orderId = samcoConnectWrapperTemp.PlaceOrder(Exchange, TradingSymbol, TransactionType, Quantity, OrderType, Price, Product, Validity, DisclosedQuantity, TriggerPrice, SquareOffValue, StoplossValue, TrailingStoploss, Variety, Tag);
                WriteLog(TradingSymbol + " " + userID, "PlaceOrder : orderId " + orderId);
            }
            else
            {
                CompositeConnectWrapper compositeConnectWrapperTemp = new CompositeConnectWrapper();
                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                }
                else
                {
                    return "";
                }

                orderId = compositeConnectWrapperTemp.PlaceOrder(Exchange, TradingSymbol, TransactionType, Quantity, OrderType, Price, Product, Validity, DisclosedQuantity, TriggerPrice, SquareOffValue, StoplossValue, TrailingStoploss, Variety, Tag);
                WriteLog(TradingSymbol + " " + userID, "PlaceOrder : orderId " + orderId);
            }
            return orderId;
        }

        public string PlaceMarketOrder(string userID,
                              string Exchange,
                              string TradingSymbol,
                              string TransactionType,
                              int Quantity,
                              string Product = Constants.PRODUCT_MIS)
        {
            string orderId;
            if (isZerodhaBroker)
            {
                orderId = PlaceOrder(userID, Exchange, TradingSymbol, TransactionType, Quantity, Constants.ORDER_TYPE_MARKET, Product: Product);
            }
            else if(isSamcoBroker)
            {
                orderId = PlaceOrder(userID, Exchange, TradingSymbol, TransactionType, Quantity, "MKT", Product: Product);
            }
            else
            {
                orderId = PlaceOrder(userID, Exchange, TradingSymbol, TransactionType, Quantity, Constants.ORDER_TYPE_MARKET, Product: Product);
            }
            return orderId;
        }

        public string PlaceLimitOrder(string userID,
                            string Exchange,
                            string TradingSymbol,
                            string TransactionType,
                            int Quantity,
                            decimal Price,
                            string Product = Constants.PRODUCT_MIS)
        {           
            string orderId;
            if (isZerodhaBroker)
            {
                orderId = PlaceOrder(userID, Exchange, TradingSymbol, TransactionType, Quantity, Constants.ORDER_TYPE_LIMIT, Price, Product: Product);
            }
            else if(isSamcoBroker)
            {
                orderId = PlaceOrder(userID, Exchange, TradingSymbol, TransactionType, Quantity, "L", Price, Product: Product);
            }
            else
            {
                orderId = PlaceOrder(userID, Exchange, TradingSymbol, TransactionType, Quantity, Constants.ORDER_TYPE_LIMIT, Price, Product: Product);
            }
            return orderId;
        }

        public string PlaceStopOrder(string userID,
                            string Exchange,
                            string TradingSymbol,
                            string TransactionType,
                            int Quantity,
                            int DisclosedQuantity,
                            decimal TriggerPrice,
                            string Product = Constants.PRODUCT_MIS)
        {
            string orderId = "NA";
            if (isZerodhaBroker)
            {
                orderId = PlaceOrder(userID, Exchange, TradingSymbol, TransactionType, Quantity, Constants.ORDER_TYPE_SLM, DisclosedQuantity: DisclosedQuantity, TriggerPrice: TriggerPrice, Product: Product);
            }
            else if(isSamcoBroker)
            {
                //sanika::6-oct-2020::Added condition for disclosed quantity error
                if (DisclosedQuantity == 0)
                {
                    orderId = PlaceOrder(userID, Exchange, TradingSymbol, TransactionType, Quantity, Constants.ORDER_TYPE_SLM, DisclosedQuantity: null, TriggerPrice: TriggerPrice, Product: Product);
                }
                else
                {
                    orderId = PlaceOrder(userID, Exchange, TradingSymbol, TransactionType, Quantity, Constants.ORDER_TYPE_SLM, DisclosedQuantity: DisclosedQuantity, TriggerPrice: TriggerPrice, Product: Product);
                }
            }
            else
            {
                orderId = PlaceOrder(userID, Exchange, TradingSymbol, TransactionType, Quantity, "STOPMARKET", DisclosedQuantity: DisclosedQuantity, TriggerPrice: TriggerPrice, Product: Product);
            }
            
            return orderId;
        }

        public string PlaceBOLimitOrder(string userID,
                               string Exchange,
                               string TradingSymbol,
                               string TransactionType,
                               int Quantity,
                               decimal Price,
                               decimal SquareOffValue,
                               decimal StoplossValue,
                               string Product = Constants.PRODUCT_MIS)
        {           
            string orderId = "NA";
            if (isZerodhaBroker)
            {
                orderId = PlaceOrder(userID, Exchange, TradingSymbol, TransactionType, Quantity, Constants.ORDER_TYPE_LIMIT, Price, SquareOffValue: SquareOffValue, StoplossValue: StoplossValue, Variety: Constants.VARIETY_BO, Product: Product);
            }
            else if(isSamcoBroker)
            {
                orderId = PlaceOrder(userID, Exchange, TradingSymbol, TransactionType, Quantity, "L", Price, SquareOffValue: SquareOffValue, StoplossValue: StoplossValue, Variety: Constants.VARIETY_BO, Product: Product);
            }
            //else
            //{
            //    orderId = PlaceOrder(userID, Exchange, TradingSymbol, TransactionType, Quantity, "L", Price, SquareOffValue: SquareOffValue, StoplossValue: StoplossValue, Variety: Constants.VARIETY_BO, Product: Product);
            //}
            return orderId;
        }

        public string PlaceBOStopOrder(string userID,
                              string Exchange,
                              string TradingSymbol,
                              string TransactionType,
                              int Quantity,
                              decimal Price,
                              decimal TriggerPrice,
                              decimal SquareOffValue,
                              decimal StoplossValue,
                              string Product = Constants.PRODUCT_MIS)
        {            
            string orderId = null;
            if (isZerodhaBroker)
            {
                orderId = PlaceOrder(userID, Exchange, TradingSymbol, TransactionType, Quantity, Constants.ORDER_TYPE_SL, Price, TriggerPrice: TriggerPrice, SquareOffValue: SquareOffValue, StoplossValue: StoplossValue, Variety: Constants.VARIETY_BO, Product: Product);
            }
            if (isSamcoBroker)
            {
                orderId = PlaceOrder(userID, Exchange, TradingSymbol, TransactionType, Quantity, Constants.ORDER_TYPE_SL, Price, TriggerPrice: TriggerPrice, SquareOffValue: SquareOffValue, StoplossValue: StoplossValue, Variety: Constants.VARIETY_BO, Product: Product);
            }
            else
            {
                orderId = PlaceOrder(userID, Exchange, TradingSymbol, TransactionType, Quantity, Constants.ORDER_TYPE_SL, Price, TriggerPrice: TriggerPrice, SquareOffValue: SquareOffValue, StoplossValue: StoplossValue, Variety: Constants.VARIETY_BO, Product: Product);
            }
            return orderId;
        }

        public bool IsPendingOrder(string userID, string TradingSymbol,string Exchange, string TransactionType, bool isPendingToOpenOrder,string Product, string OrderStatus = Constants.ORDER_STATUS_PENDING)
        {
            if (isZerodhaBroker)
            {
            KiteConnectWrapper kiteConnectWrapperTemp;
            if (kiteConnectObjects.ContainsKey(userID))
            {
                kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
            }
            else
            {
                WriteLog(TradingSymbol + " " + userID, "IsPendingOrder: return false");
                return false;
            }            
            if (kiteConnectWrapperTemp.IsPendingOrder(TradingSymbol, Exchange, TransactionType, OrderStatus,Product, isPendingToOpenOrder))
            {               
                return true;
            }           
            return false;
        }
            else if (isSamcoBroker)
            {
                SamcoConnectWrapper samcoConnectWrapperTemp;
                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "IsPendingOrder: return false");
                    return false;
                }
                if (samcoConnectWrapperTemp.IsPendingOrder(TradingSymbol, Exchange, TransactionType, OrderStatus, Product, isPendingToOpenOrder))
                {
                    return true;
                }
                return false;
            }
            else
            {
                CompositeConnectWrapper compositeConnectWrapperTemp;
                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "IsPendingOrder: return false");
                    return false;
                }//sanika::19-Jan-2021::passed order status as it is because stop order and limit order has same order status
                if (compositeConnectWrapperTemp.IsPendingOrder(TradingSymbol, Exchange, TransactionType, OrderStatus, Product, isPendingToOpenOrder))
                {
                    return true;
                }
                return false;
            }
        }

        public bool IsPendingLimitOrder(string userID, string TradingSymbol, string Exchange, string TransactionType,string Product = Constants.PRODUCT_MIS)
        {
            bool res = false;
            if (isZerodhaBroker)
            {
                res = IsPendingOrder(userID, TradingSymbol, Exchange, TransactionType, false, Product, Constants.ORDER_STATUS_OPEN);
            }
            else if(isSamcoBroker)
            {
                res = IsPendingOrder(userID, TradingSymbol, Exchange, TransactionType, false, Product, Constants.ORDER_STATUS_OPEN);
            }
            else if(isCompositeBroker)
            {
                //sanika::19-Jan-2021::Added broker condition and passed order type also because stop order and limit order has same order status
                res = IsPendingOrder(userID, TradingSymbol, Exchange, TransactionType, false, Product, Constants.ORDER_TYPE_LIMIT +"_"+ Constants.ORDER_STATUS_OPEN);
            }

            return res;
        }

        public bool IsPendingStopOrder(string userID, string TradingSymbol, string Exchange, string TransactionType,string Product = Constants.PRODUCT_MIS)
        {
            bool res = false;
            if (isZerodhaBroker)
            {
                res = IsPendingOrder(userID, TradingSymbol, Exchange, TransactionType, false, Product: Product);
            }
            else if(isSamcoBroker)
            {
                //IRDSPM::Pratiksha::26-05-2021::For samco changes
                res = IsPendingOrder(userID, TradingSymbol, Exchange, TransactionType, false, Product: Product);
            }
            //sanika::19-Jan-2021::Added broker condition and passed order type also because stop order and limit order has same order status
            else if (isCompositeBroker)
            {
                res = IsPendingOrder(userID, TradingSymbol, Exchange, TransactionType, false, Product: Product,OrderStatus:"STOPMARKET_"+Constants.ORDER_STATUS_OPEN);
            }
            return res;
        }
        //sanika::31-Dec-2020::Added status parameter
        public bool CancelPendingOrder(string userID, string TradingSymbol,string Exchange, string TransactionType,string Status, bool isCancelToOpenOrder, string Variety = Constants.VARIETY_REGULAR)
        {
            string orderId;
            if (isZerodhaBroker)
            {
            KiteConnectWrapper kiteConnectWrapperTemp;

            if (kiteConnectObjects.ContainsKey(userID))
            {
                kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
            }
            else
            {
                WriteLog(TradingSymbol + " " + userID, "CancelPendingOrder: return false");
                return false;
            }
            if (kiteConnectWrapperTemp.cancelPendingOrderWithTransactionType(TradingSymbol,Exchange, TransactionType, Variety,Status, isCancelToOpenOrder))
            {                
                return true;
            }           
            return false;
        }
            else if (isSamcoBroker)
            {
                SamcoConnectWrapper samcoConnectWrapperTemp;

                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "CancelPendingOrder: return false");
                    return false;
                }

                if (samcoConnectWrapperTemp.cancelPendingOrderWithTransactionType(TradingSymbol, Exchange, TransactionType, Variety, isCancelToOpenOrder))
                {
                    return true;
                }
                return false;
            }
            else
            {
                CompositeConnectWrapper compositeConnectWrapperTemp;

                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "CancelPendingOrder: return false");
                    return false;
                }

                if (compositeConnectWrapperTemp.cancelPendingOrderWithTransactionType(TradingSymbol, Exchange, TransactionType, Variety, Status, isCancelToOpenOrder))
                {
                    return true;
                }
                return false;
            }
        }

        public bool CancelPendingLimitOrder(string userID, string TradingSymbol,string Exchange, string TransactionType)
        {            //sanika::31-Dec-2020::Added status parameter
            bool res = false;
            if (isZerodhaBroker)
            {
                res = CancelPendingOrder(userID, TradingSymbol, Exchange, TransactionType, Constants.ORDER_STATUS_OPEN, false);
            }
            else if(isSamcoBroker)
            {
                res = CancelPendingOrder(userID, TradingSymbol, Exchange, TransactionType, Constants.ORDER_STATUS_OPEN, false);
            }
            else if(isCompositeBroker) //sanika::22-Jan-2021::limit and pending order has same status
            {
                res = CancelPendingOrder(userID, TradingSymbol, Exchange, TransactionType,Constants.ORDER_TYPE_LIMIT+"_"+Constants.ORDER_STATUS_OPEN, false);
            }
            return res;
        }

        public bool CancelPendingStopOrder(string userID, string TradingSymbol,string Exchange, string TransactionType)
        {            //sanika::31-Dec-2020::Added status parameter
            bool res = false;
            if (isZerodhaBroker)
            {
                res = CancelPendingOrder(userID, TradingSymbol, Exchange, TransactionType,  Constants.ORDER_STATUS_PENDING, false);
            }
            else if(isSamcoBroker)
            {
                res = CancelPendingOrder(userID, TradingSymbol, Exchange, TransactionType, Constants.ORDER_STATUS_PENDING, false);
            }
            else if(isCompositeBroker) //sanika::22-Jan-2021::limit and pending order has same status
            {
                res = CancelPendingOrder(userID, TradingSymbol, Exchange, TransactionType, "STOPMARKET_" + Constants.ORDER_STATUS_PENDING, false);
            }
            return res;
        }

        public bool CancelPendingBOOrder(string userID, string TradingSymbol,string Exchange, string TransactionType)
        {            
            bool res = CancelPendingOrder(userID, TradingSymbol,Exchange, TransactionType, Constants.ORDER_STATUS_OPEN, false, Constants.VARIETY_BO);           
            return res;
        }

        public bool CancelPendingBOOrderWithParentOrder(string userID, string TradingSymbol,string Exchange, string TransactionType)
        {            
            bool res = CancelPendingOrder(userID, TradingSymbol, Exchange, TransactionType, Constants.ORDER_STATUS_OPEN, true, Constants.VARIETY_BO);            
            return res;
        }

        public void CreateKiteConnectObject(string userID, string iniFilePath, MainWindow m_mainWindow, bool isFromEXE = false)
        {
            //sanika::27-oct-2020::Added for client release
            DateTime dateTime = DateTime.Now;
            WriteLog("StartGUI", "dateTime: " + dateTime);
            DateTime ExpireDate = DateTime.ParseExact("31-12-2021", "dd-MM-yyyy", null);
            WriteLog("StartGUI", "ExpireDate: " + ExpireDate);
            if (dateTime < ExpireDate)
            {
                var key = userID;
                if (isZerodhaBroker)
                {

                    if (!kiteConnectObjects.ContainsKey(key))
                    {
                        lock (_CreateKiteobject)
                        {
                            if (userID != "" && iniFilePath != "" && !kiteConnectObjects.ContainsKey(key))
                            {
                                kiteConnectObjects.Add(userID, kiteWrapperConnect = new KiteConnectWrapper(iniFilePath));
                            }
                            Thread.Sleep(100);
                        }
                    }

                    try
                    {
                        if (kiteWrapperConnect.GetShowGUIFlag() == true)
                        {
                            WriteLog("StartGUI", "GUI flag is true");
                            Thread thread = new Thread(() => StartGUI(kiteWrapperConnect, m_mainWindow, isFromEXE));
                            thread.SetApartmentState(ApartmentState.STA);
                            thread.Start();
                        }
                        //sanika::11-sep-2020::Added for bridge connection
                        else if (isFromEXE == true)
                        {
                            WriteLog("StartGUI", "GUI call from exe");
                            Thread thread = new Thread(() => StartGUI(kiteWrapperConnect, m_mainWindow, isFromEXE));
                            thread.SetApartmentState(ApartmentState.STA);
                            thread.Start();
                        }
                        kiteWrapperConnect.loadIsFromEXEValue(isFromEXE);
                    }
                    catch (Exception e)
                    {

                    }
                }
                else if (isSamcoBroker)
                {
                    if (!samcoConnectObjects.ContainsKey(key))
                    {
                        lock (_CreateSamcoobject)
                        {
                            if (userID != "" && iniFilePath != "" && !samcoConnectObjects.ContainsKey(key))
                            {
                                samcoConnectObjects.Add(userID, samcoWrapperConnect = new SamcoConnectWrapper(iniFilePath));
                            }
                            Thread.Sleep(100);
                        }
                    }

                    try
                    {
                        if (samcoWrapperConnect.GetShowGUIFlag() == true)
                        {
                            Thread thread = new Thread(() => StartGUI(samcoWrapperConnect, m_mainWindow, isFromEXE));
                            thread.SetApartmentState(ApartmentState.STA);
                            thread.Start();
                        }
                        else if (isFromEXE == true)
                        {
                            Thread thread = new Thread(() => StartGUI(samcoWrapperConnect, m_mainWindow, isFromEXE));
                            thread.SetApartmentState(ApartmentState.STA);
                            thread.Start();
                        }
                        samcoWrapperConnect.loadIsFromEXEValue(isFromEXE);
                    }
                    catch (Exception e)
                    {

                    }
                }
                else
                {
                    if (!compositeConnectObjects.ContainsKey(key))
                    {
                        lock (_CreateSamcoobject)
                        {
                            if (userID != "" && iniFilePath != "" && !compositeConnectObjects.ContainsKey(key))
                            {
                                compositeConnectObjects.Add(userID, compositeWrapperConnect = new CompositeConnectWrapper(iniFilePath));
                            }
                            Thread.Sleep(100);
                        }
                    }

                    try
                    {
                        if (compositeWrapperConnect.GetShowGUIFlag() == true)
                        {
                            Thread thread = new Thread(() => StartGUI(compositeWrapperConnect, m_mainWindow, isFromEXE));
                            thread.SetApartmentState(ApartmentState.STA);
                            thread.Start();
                        }
                        else if (isFromEXE == true)
                        {
                            Thread thread = new Thread(() => StartGUI(compositeWrapperConnect, m_mainWindow, isFromEXE));
                            thread.SetApartmentState(ApartmentState.STA);
                            thread.Start();
                        }
                        compositeWrapperConnect.loadIsFromEXEValue(isFromEXE);
                    }
                    catch (Exception e)
                    {

                    }
                }
            }
            else
            {
                MessageBox.Show("Please Contact IRDS Support Team!!");
                return;
            }
        }


        public void CreateKiteConnectObject(string userID, string iniFilePath, bool isFromEXE = false)
        {
            //sanika::27-oct-2020::Added for client release
            DateTime dateTime = DateTime.Now;
            WriteLog("StartGUI", "dateTime: " + dateTime);
            DateTime ExpireDate = DateTime.ParseExact("31-12-2021", "dd-MM-yyyy", null);
            WriteLog("StartGUI", "ExpireDate: " + ExpireDate);
            if (dateTime < ExpireDate)
            {
                var key = userID;
                if (isZerodhaBroker)
                {

                    if (!kiteConnectObjects.ContainsKey(key))
                    {
                        lock (_CreateKiteobject)
                        {
                            if (userID != "" && iniFilePath != "" && !kiteConnectObjects.ContainsKey(key))
                            {
                                kiteConnectObjects.Add(userID, kiteWrapperConnect = new KiteConnectWrapper(iniFilePath));
                            }
                            Thread.Sleep(100);
                        }
                    }

                    try
                    {
                        if (kiteWrapperConnect.GetShowGUIFlag() == true)
                        {
                            WriteLog("StartGUI", "GUI flag is true");
                            Thread thread = new Thread(() => StartGUI(kiteWrapperConnect, isFromEXE));
                            thread.SetApartmentState(ApartmentState.STA);
                            thread.Start();
                        }
                        //sanika::11-sep-2020::Added for bridge connection
                        else if (isFromEXE == true)
                        {
                            WriteLog("StartGUI", "GUI call from exe");
                            Thread thread = new Thread(() => StartGUI(kiteWrapperConnect, isFromEXE));
                            thread.SetApartmentState(ApartmentState.STA);
                            thread.Start();
                        }
                        kiteWrapperConnect.loadIsFromEXEValue(isFromEXE);
                    }
                    catch (Exception e)
                    {

                    }
                }
                else if (isSamcoBroker)
                {
                    if (!samcoConnectObjects.ContainsKey(key))
                    {
                        lock (_CreateSamcoobject)
                        {
                            if (userID != "" && iniFilePath != "" && !samcoConnectObjects.ContainsKey(key))
                            {
                                samcoConnectObjects.Add(userID, samcoWrapperConnect = new SamcoConnectWrapper(iniFilePath));
                            }
                            Thread.Sleep(100);
                        }
                    }

                    try
                    {
                        if (samcoWrapperConnect.GetShowGUIFlag() == true)
                        {
                            Thread thread = new Thread(() => StartGUI(samcoWrapperConnect, isFromEXE));
                            thread.SetApartmentState(ApartmentState.STA);
                            thread.Start();
                        }
                        else if (isFromEXE == true)
                        {
                            Thread thread = new Thread(() => StartGUI(samcoWrapperConnect, isFromEXE));
                            thread.SetApartmentState(ApartmentState.STA);
                            thread.Start();
                        }
                        samcoWrapperConnect.loadIsFromEXEValue(isFromEXE);
                    }
                    catch (Exception e)
                    {

                    }
                }
                else
                {
                    if (!compositeConnectObjects.ContainsKey(key))
                    {
                        lock (_CreateSamcoobject)
                        {
                            if (userID != "" && iniFilePath != "" && !compositeConnectObjects.ContainsKey(key))
                            {
                                compositeConnectObjects.Add(userID, compositeWrapperConnect = new CompositeConnectWrapper(iniFilePath));
                            }
                            Thread.Sleep(100);
                        }
                    }

                    try
                    {
                        if (compositeWrapperConnect.GetShowGUIFlag() == true)
                        {
                            Thread thread = new Thread(() => StartGUI(compositeWrapperConnect, isFromEXE));
                            thread.SetApartmentState(ApartmentState.STA);
                            thread.Start();
                        }
                        else if (isFromEXE == true)
                        {
                            Thread thread = new Thread(() => StartGUI(compositeWrapperConnect, isFromEXE));
                            thread.SetApartmentState(ApartmentState.STA);
                            thread.Start();
                        }
                        compositeWrapperConnect.loadIsFromEXEValue(isFromEXE);
                    }
                    catch (Exception e)
                    {

                    }
                }
            }
            else
            {
                MessageBox.Show("Please Contact IRDS Support Team!!");
                return;
            }
        }

        [STAThread]
        public void StartGUI(dynamic kiteConnectWrapper, MainWindow m_mainWindowObj, bool isFromEXE = false)
        {            
            try
            {
                WriteLog("StartGUI", "In startGUI");
                lock (_CreateWindows)
                {
                    //if (m_objMainWindow == null)
                    //{
                    //    string userId = kiteConnectWrapper.GetUserID();
                    //    WriteLog("StartGUI", "userId = "+ userId);
                    //    List<string> realTimeSymbols = new List<string>();
                    //    kiteConnectWrapper.GetRealTimeSymbols(out realTimeSymbols);
                    //    {
                    //        WriteLog("StartGUI", "Going to load main window");
                    //        m_objMainWindow = new MainWindow();
                    //        WriteLog("StartGUI", "main window obj created");
                    //        m_objMainWindow.loadValues(kiteConnectWrapper, userId, realTimeSymbols, isFromEXE);
                    //        WriteLog("StartGUI", "after loadvaluesssss");
                    //        m_objMainWindow.ShowDialog();
                    //    }
                    //}
                    //else
                    //{
                    //    WriteLog("StartGUI", "m_objMainWindow is not null");
                    //}
                    //if (m_mainWindowObj == null)
                    {
                        string userId = kiteConnectWrapper.GetUserID();
                        List<string> realTimeSymbols = new List<string>();
                        kiteConnectWrapper.GetRealTimeSymbols(out realTimeSymbols);
                        m_mainWindowObj.loadValues(kiteConnectWrapper, userId, realTimeSymbols, isFromEXE);
                    }
            }
            }
            catch (Exception e)
            {
                WriteLog("StartGUI", "Exception Error Message = " + e.Message);
            }

        }

        [STAThread]
        public void StartGUI(dynamic kiteConnectWrapper, bool isFromEXE = false)
        {
            try
            {
                WriteLog("StartGUI", "In startGUI");
                lock (_CreateWindows)
                {
                    if (m_objMainWindow == null)
                    {
                        string userId = kiteConnectWrapper.GetUserID();
                        WriteLog("StartGUI", "userId = " + userId);
                        List<string> realTimeSymbols = new List<string>();
                        kiteConnectWrapper.GetRealTimeSymbols(out realTimeSymbols);
                        {
                            WriteLog("StartGUI", "Going to load main window");
                            m_objMainWindow = new MainWindow();
                            WriteLog("StartGUI", "main window obj created");
                            m_objMainWindow.loadValues(kiteConnectWrapper, userId, realTimeSymbols, isFromEXE);
                            WriteLog("StartGUI", "after loadvaluesssss");
                            m_objMainWindow.ShowDialog();
                        }
                    }
                    else
                    {
                        WriteLog("StartGUI", "m_objMainWindow is not null");
                    }
                }
            }
            catch (Exception e)
            {
                WriteLog("StartGUI", "Exception Error Message = " + e.Message);
            }

        }

        public void WriteLog(string TradingSymbol, string logMessage)
        {
            try
            {
                SymbolLogs log = new SymbolLogs();
                log.createFile(TradingSymbol);
                log.LogMessage(logMessage, MessageType.Informational);
                log = null;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : " + e.Message);
                Trace.WriteLine("Exception : " + e.Message);
            }
        }

        public int ForeceFulRelogin(string userID)
        {
            if (isZerodhaBroker)
            {
            KiteConnectWrapper kiteConnectWrapperTemp;

            if (kiteConnectObjects.ContainsKey(userID))
            {
                kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
            }
            else
            {
                return 0;
            }

            kiteConnectWrapperTemp.ForceFulReLogin();
            }
            else if(isSamcoBroker)
            {
                SamcoConnectWrapper samcoConnectWrapperTemp;

                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    return 0;
                }
                samcoConnectWrapperTemp.ForceFulReLogin();
            }
            return 0;
        }

        public int IntrumentsCSVToDB(string userID)
        {
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp;

                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    return 0;
                }

                kiteConnectWrapperTemp.IntrumentsCSVToDB();
            }
            else
            {
                if (isSamcoBroker)
                {
                    SamcoConnectWrapper samcoConnectWrapperTemp;

                    if (samcoConnectObjects.ContainsKey(userID))
                    {
                        samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                    }
                    else
                    {
                        return 0;
                    }

                    samcoConnectWrapperTemp.IntrumentsCSVToDB();
                }
                else
                {
                    if(isCompositeBroker)
                    {
                        CompositeConnectWrapper compositeConnectWrapperTemp;

                        if (compositeConnectObjects.ContainsKey(userID))
                        {
                            compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                        }
                        else
                        {
                            return 0;
                        }
                        compositeConnectWrapperTemp.IntrumentsCSVToDB();
                    }
                }
            }
            return 0;
        }

        public bool LoadConfigFile(string userID, string iniFilePath)
        {
            if (isZerodhaBroker)
            {
            KiteConnectWrapper kiteConnectWrapperTemp;

            if (kiteConnectObjects.ContainsKey(userID))
            {
                kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
            }
            else
            {
                return false;
            }

            if (kiteConnectWrapperTemp.isReloadINI(iniFilePath))
            {
                return true;
            }
            }
            else
            {
                SamcoConnectWrapper samcoConnectWrapperTemp;

                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    return false;
                }

                if (samcoConnectWrapperTemp.isReloadINI(iniFilePath))
                {
                    return true;
                }
            }
            return false;
        }

        public bool ModifyLimitOrder(string userID, string TradingSymbol, String TransactionType, decimal Price, string Exchange = Constants.EXCHANGE_NSE, string Product = Constants.PRODUCT_MIS)
        {
            bool res = false;
            if (isZerodhaBroker)
            {
            KiteConnectWrapper kiteConnectWrapperTemp;
            if (kiteConnectObjects.ContainsKey(userID))
            {
                kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
            }
            else
            {
                WriteLog(TradingSymbol + " " + userID, "ModifyLimitOrder: return false");
                return false;
            }           
                 res = kiteConnectWrapperTemp.ModifyLimitOrder(TradingSymbol, TransactionType, Price, Exchange, Product, Constants.VARIETY_REGULAR);
        }
            else if(isSamcoBroker)
            {
                SamcoConnectWrapper samcoConnectWrapperTemp;
                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "ModifyLimitOrder: return false");
                    return false;
                }
                 res = samcoConnectWrapperTemp.ModifyLimitOrder(TradingSymbol, TransactionType, Price, Exchange, Product, Constants.VARIETY_REGULAR);
            }
            else if (isCompositeBroker)
            {
                CompositeConnectWrapper compositeConnectWrapperTemp;
                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "ModifyLimitOrder: return false");
                    return false;
                }
                res = compositeConnectWrapperTemp.ModifyLimitOrder(TradingSymbol, TransactionType, Price, Exchange, Product, Constants.VARIETY_REGULAR);
            }
                return res;
            }

        public bool ModifyStopOrder(string userID, string TradingSymbol, string TransactionType, decimal TriggerPrice, string Exchange = Constants.EXCHANGE_NSE, string Product = Constants.PRODUCT_MIS)
        {
            if (isZerodhaBroker)
            { 
            KiteConnectWrapper kiteConnectWrapperTemp;

            if (kiteConnectObjects.ContainsKey(userID))
            {
                kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
            }
            else
            {
                WriteLog(TradingSymbol + " " + userID, "ModifyStopOrder: return false");
                return false;
            }            
            bool res = kiteConnectWrapperTemp.ModifyStopOrder(TradingSymbol, TransactionType, TriggerPrice, Exchange, Product);
            return res;
        }
            else if (isSamcoBroker)
            {
                SamcoConnectWrapper samcoConnectWrapperTemp;
                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "ModifyLimitOrder: return false");
                    return false;
                }
                bool res = samcoConnectWrapperTemp.ModifyStopOrder(TradingSymbol, TransactionType, TriggerPrice, Exchange, Product);
                return res;
            }
            else
            {
                CompositeConnectWrapper compositeConnectWrapperTemp;

                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "ModifyLimitOrder: return false");
                    return false;
                }
                bool res = compositeConnectWrapperTemp.ModifyStopOrder(TradingSymbol, TransactionType, TriggerPrice, Exchange, Product);
                return res;
            }
        }

        public bool ModifyBOLimitOrder(string userID, string TradingSymbol, String TransactionType, decimal Price, string Exchange = Constants.EXCHANGE_NSE, string Product = Constants.PRODUCT_MIS)
        {
            bool res = false;
            if (isZerodhaBroker)
            {
            KiteConnectWrapper kiteConnectWrapperTemp;
            if (kiteConnectObjects.ContainsKey(userID))
            {
                kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
            }
            else
            {
                WriteLog(TradingSymbol + " " + userID, "ModifyBOLimitOrder: return false");
                return false;
            }           
                res = kiteConnectWrapperTemp.ModifyLimitOrder(TradingSymbol, TransactionType, Price, Exchange, Product, Constants.VARIETY_BO);

        }
            else if(isSamcoBroker)
            {
                SamcoConnectWrapper samcoConnectWrapperTemp;
                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "ModifyLimitOrder: return false");
                    return false;
                }
                res = samcoConnectWrapperTemp.ModifyLimitOrder(TradingSymbol, TransactionType, Price, Exchange, Product, Constants.VARIETY_BO);
            }
            else if (isCompositeBroker)
            {
                CompositeConnectWrapper CompositeConnectWrapperTemp;
                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out CompositeConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "ModifyLimitOrder: return false");
                    return false;
                }
                res = CompositeConnectWrapperTemp.ModifyLimitOrder(TradingSymbol, TransactionType, Price, Exchange, Product, Constants.VARIETY_BO);
            }
                return res;
            }

        public bool ModifyBOStopOrder(string userID, string TradingSymbol, String TransactionType, decimal Price, string Exchange = Constants.EXCHANGE_NSE, string Product = Constants.PRODUCT_MIS)
        {
            bool res = false;
            if (isZerodhaBroker)
            {
            KiteConnectWrapper kiteConnectWrapperTemp;
            if (kiteConnectObjects.ContainsKey(userID))
            {
                kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
            }
            else
            {
                WriteLog(TradingSymbol + " " + userID, "ModifyBOStopOrder: return false");
                return false;
            }           
                res = kiteConnectWrapperTemp.ModifyBOStopOrder(TradingSymbol, TransactionType, Price, Exchange, Product);
        }
            else if(isSamcoBroker)
            {
                SamcoConnectWrapper samcoConnectWrapperTemp;
                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "ModifyLimitOrder: return false");
                    return false;
                }
                 res = samcoConnectWrapperTemp.ModifyBOStopOrder(TradingSymbol, TransactionType, Price, Exchange, Product);
            }           
            else if (isCompositeBroker)
            {
                CompositeConnectWrapper CompositeConnectWrapperTemp;
                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out CompositeConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "ModifyLimitOrder: return false");
                    return false;
                }
                res = CompositeConnectWrapperTemp.ModifyBOStopOrder(TradingSymbol, TransactionType, Price, Exchange, Product);
            }
                return res;
            }

        public bool CancelAllPendingOrder(string userID)
        {
            if (isZerodhaBroker)
            {
            KiteConnectWrapper kiteConnectWrapperTemp;

            if (kiteConnectObjects.ContainsKey(userID))
            {
                kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
            }
            else
            {
                return false;
            }

            if (kiteConnectWrapperTemp.CancelAllPendingOrder())
            {
                return true;
            }
        }
            else if(isSamcoBroker)
            {
                SamcoConnectWrapper samcoConnectWrapperTemp;

                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    return false;
                }

                if (samcoConnectWrapperTemp.CancelAllPendingOrder())
                {
                    return true;
                }

            }
            else if (isCompositeBroker)
            {
                CompositeConnectWrapper CompositeConnectWrapperTemp;

                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out CompositeConnectWrapperTemp);
                }
                else
                {
                return false;
            }

                if (CompositeConnectWrapperTemp.CancelAllPendingOrder())
                {
                    return true;
                }
            }
            return false;
        }

        //sanika::21-oct-2020::added new function to return orderid of closed order
        public string CloseOrderEx(string userID, string TradingSymbol, string Exchange = Constants.EXCHANGE_NSE, string Product = Constants.PRODUCT_MIS)
        {
            string orderId = "NA";
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp;

                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "CloseOrder: return NA");
                    return orderId;
                }

                orderId = kiteConnectWrapperTemp.CloseOrderEx(TradingSymbol, Exchange, Product);
                WriteLog(TradingSymbol + " " + userID, "CloseOrder : orderId = " + orderId);              
                
            }
            else if (isSamcoBroker)
            {
                SamcoConnectWrapper samcoConnectWrapperTemp;

                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "CloseOrder: return NA");
                    return orderId;
                }

                //orderId = samcoConnectWrapperTemp.CloseOrderEx(TradingSymbol, Product, Exchange);
                WriteLog(TradingSymbol + " " + userID, "CloseOrder : orderId = " + orderId);                
            }
            else
            {
                CompositeConnectWrapper compositeConnectWrapperTemp;

                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "CloseOrder: return NA");
                    return orderId;
                }

                orderId = compositeConnectWrapperTemp.CloseOrderEx(TradingSymbol, Exchange, Product);
                WriteLog(TradingSymbol + " " + userID, "CloseOrder : orderId = " + orderId);
            }
            return orderId;
        }

        public bool CloseOrder(string userID, string TradingSymbol,string Exchange = Constants.EXCHANGE_NSE,string Product = Constants.PRODUCT_MIS)
        {
            bool isOrderClose = false;
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp;

                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "CloseOrder: return false");
                    return isOrderClose;
                }

                if (kiteConnectWrapperTemp.CloseOrder(TradingSymbol, Exchange,Product))
                {
                    WriteLog(TradingSymbol + " " + userID, "CloseOrder : returns true");
                    isOrderClose =  true;
                }                
            }
            else if (isSamcoBroker)
            {
                SamcoConnectWrapper samcoConnectWrapperTemp;

                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "CloseOrder: return false");
                    return isOrderClose;
                }

                if (samcoConnectWrapperTemp.CloseOrder(TradingSymbol, Exchange, Product))
                {
                    WriteLog(TradingSymbol + " " + userID, "CloseOrder : returns true");
                    isOrderClose = true;
                }               
            }
            else if(isCompositeBroker)//sanika::16-Jan-2021::Added condition
            {
                CompositeConnectWrapper compositeConnectWrapperTemp;

                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "CloseOrder: return false");
                    return isOrderClose;
                }

                if (compositeConnectWrapperTemp.CloseOrder(TradingSymbol, Exchange, Product))
                {
                    WriteLog(TradingSymbol + " " + userID, "CloseOrder : returns true");
                    isOrderClose = true;
                }
            }
            return isOrderClose;
        }

        public bool CloseBOOrder(string userID, string TradingSymbol,string Exchange)
        {
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp;

                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "CloseBOOrder: return false");
                    return false;
                }

                if (kiteConnectWrapperTemp.CloseBOOrder(TradingSymbol, Exchange))
                {
                    WriteLog(TradingSymbol + " " + userID, "CloseBOOrder : returns true");
                }
            }
            else if(isSamcoBroker)
            {
                SamcoConnectWrapper samcoConnectWrapperTemp;

                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "CloseBOOrder: return false");
                    return false;
                }

                if (samcoConnectWrapperTemp.CloseBOOrder(TradingSymbol, Exchange))
                {
                    WriteLog(TradingSymbol + " " + userID, "CloseBOOrder : returns true");
                }
            }
            //IRDSPM::Pratiksha::26-05-2021::For composite 
            else if(isCompositeBroker)
            {
                CompositeConnectWrapper CompositeConnectWrapperTemp;

                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out CompositeConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "CloseBOOrder: return false");
                    return false;
                }

                if (CompositeConnectWrapperTemp.CloseBOOrder(TradingSymbol, Exchange))
                {
                    WriteLog(TradingSymbol + " " + userID, "CloseBOOrder : returns true");
                }
            }
            return false;
        }

        public bool CloseAllOrder(string userID)
        {
            if (isZerodhaBroker)
            {
            KiteConnectWrapper kiteConnectWrapperTemp;

            if (kiteConnectObjects.ContainsKey(userID))
            {
                kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
            }
            else
            {
                return false;
            }

            if (kiteConnectWrapperTemp.CloseAllOpenOrders())
            {
                return true;
            }
        }
            else if(isSamcoBroker)
            {
                SamcoConnectWrapper samcoConnectWrapperTemp;

                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    return false;
                }

                if (samcoConnectWrapperTemp.CloseAllOpenOrders())
                {
                    return true;
                }
            }
            else if(isCompositeBroker)
            {
                CompositeConnectWrapper CompositeConnectWrapperTemp;

                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out CompositeConnectWrapperTemp);
                }
                else
                {
                return false;
            }

                if (CompositeConnectWrapperTemp.CloseAllOpenOrders())
                {
                    return true;
                }
            }
            return false;
        }

        public double GetLastPrice(string userID, string TradingSymbol)
        {
            double lastPrice = 0;
            try
            {
                if (isZerodhaBroker)
                {
                    KiteConnectWrapper kiteConnectWrapperTemp;

                    if (kiteConnectObjects.ContainsKey(userID))
                    {
                        kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                    }
                    else
                    {
                        WriteLog(TradingSymbol + " " + userID, "GetLatPrice: return 0");
                        return 0;
                    }
                    string ltp = kiteConnectWrapperTemp.GetLastTradedPrice(TradingSymbol);
                    //sanika::11-sep-2020::Added condition for exception of input string format incorrect
                    if (ltp != "")
                    {
                        lastPrice = Convert.ToDouble(ltp);
                    }
                    else
                    {
                        lastPrice = 0;
                    }
                }
                else if(isSamcoBroker)
                {
                    SamcoConnectWrapper samcoConnectWrapperTemp;

                    if (samcoConnectObjects.ContainsKey(userID))
                    {
                        samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                    }
                    else
                    {
                        WriteLog(TradingSymbol + " " + userID, "GetLatPrice: return 0");
                        return 0;
                    }
                    string ltp = samcoConnectWrapperTemp.GetLastTradedPrice(TradingSymbol);
                    //sanika::11-sep-2020::Added condition for exception of input string format incorrect
                    if (ltp != "")
                    {
                        lastPrice = Convert.ToDouble(ltp);
                    }
                    else
                    {
                        lastPrice = 0;
                    }                    
                }
                else if (isCompositeBroker)
                {
                    CompositeConnectWrapper compositeConnectWrapperTemp;

                    if (compositeConnectObjects.ContainsKey(userID))
                    {
                        compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                    }
                    else
                    {
                        WriteLog(TradingSymbol + " " + userID, "GetLatPrice: return 0");
                        return 0;
                    }
                    string ltp = compositeConnectWrapperTemp.GetLastTradedPrice(TradingSymbol);
                    //sanika::11-sep-2020::Added condition for exception of input string format incorrect
                    if (ltp != "")
                    {
                        lastPrice = Convert.ToDouble(ltp);
                    }
                    else
                    {
                        lastPrice = 0;
                    }
                }
            }
            catch(Exception e)
            {
                WriteLog(TradingSymbol + " " + userID, "WriteDataIntoFile: Exception Error Message = " + e.Message);
            }
            return lastPrice;
        }

        public double GetOpenPostionPrice(string userID, string TradingSymbol, string Exchange = Constants.EXCHANGE_NSE, string Product = Constants.PRODUCT_MIS)
        {            
            double openPrice = 0;
            if (isZerodhaBroker)
            {
            KiteConnectWrapper kiteConnectWrapperTemp;

            if (kiteConnectObjects.ContainsKey(userID))
            {
                kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
            }
            else
            {
                WriteLog(TradingSymbol + " " + userID, "GetOpenPostionPrice: return 0");
                return 0;
            }
                openPrice = kiteConnectWrapperTemp.GetOpenPostionPrice(TradingSymbol, Exchange, Product);
            }
            else if(isSamcoBroker)
            {
                SamcoConnectWrapper samcoConnectWrapperTemp;

                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "GetOpenPostionPrice: return 0");
                    return 0;
                }
                openPrice = samcoConnectWrapperTemp.GetOpenPostionPrice(TradingSymbol, Exchange, Product);
            }
            else if(isCompositeBroker)
            {
                CompositeConnectWrapper CompositeConnectWrapperTemp;

                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out CompositeConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "GetOpenPostionPrice: return 0");
                    return 0;
                }
                openPrice = CompositeConnectWrapperTemp.GetOpenPostionPrice(TradingSymbol, Exchange, Product);
            }
            return openPrice;
        }

        public string GetOpenPostionDirection(string userID, string TradingSymbol,string Exchange = Constants.EXCHANGE_NSE,string Product = Constants.PRODUCT_MIS)
        {
            string direction = null;
            if (isZerodhaBroker)
            {
            KiteConnectWrapper kiteConnectWrapperTemp;

            if (kiteConnectObjects.ContainsKey(userID))
            {
                kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
            }
            else
            {
                WriteLog(TradingSymbol + " " + userID, "GetOpenPostionDirection: return null");
                return null;
            }
                direction = kiteConnectWrapperTemp.GetOpenPostionDirection(TradingSymbol, Exchange, Product);
            }
            else if(isSamcoBroker)
            {
                SamcoConnectWrapper samcoConnectWrapperTemp;

                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "GetOpenPostionDirection: return null");
                    return null;
                }
                direction = samcoConnectWrapperTemp.GetOpenPostionDirection(TradingSymbol, Exchange, Product);
            }
            else if (isCompositeBroker)
            {
                CompositeConnectWrapper compositeConnectWrapperTemp;

                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "GetOpenPostionDirection: return null");
                    return null;
                }
                direction = compositeConnectWrapperTemp.GetOpenPostionDirection(TradingSymbol, Exchange, Product);
            }
            return direction;

        }


        public double GetOpenPostionPricebyOrderID(string userID, string TradingSymbol, string Exchange,string orderId)
        {
            double openPrice = 0;
            if (isZerodhaBroker)
            {
            KiteConnectWrapper kiteConnectWrapperTemp;

            if (kiteConnectObjects.ContainsKey(userID))
            {
                kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
            }
            else
            {
                WriteLog(TradingSymbol + " " + userID, "GetOpenPostionPrice: return 0");
                return 0;
            }
                openPrice = kiteConnectWrapperTemp.GetOpenPostionPricebyOrderID(TradingSymbol, Exchange, orderId);
            }
            else if (isSamcoBroker)
            {
                SamcoConnectWrapper samcoConnectWrapperTemp;

                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "GetOpenPostionPrice: return 0");
                    return 0;
                }
                openPrice = samcoConnectWrapperTemp.GetOpenPostionPricebyOrderID(TradingSymbol, Exchange, orderId);
            }
            else
            {
                CompositeConnectWrapper compositeConnectWrapperTemp;

                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "GetOpenPostionPrice: return 0");
                    return 0;
                }
                openPrice = compositeConnectWrapperTemp.GetOpenPostionPricebyOrderID(TradingSymbol, Exchange, orderId);
            }
            return openPrice;
        }
        public double GetOpenPostionPricebySymbol(string userID, string TradingSymbol, string Exchange)
        {
            double openPrice;
            if (isZerodhaBroker)
            {
            //WriteLog(TradingSymbol + " " + userID, "GetOpenPostionPrice: Going fetch open price");
            KiteConnectWrapper kiteConnectWrapperTemp;

            if (kiteConnectObjects.ContainsKey(userID))
            {
                kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
            }
            else
            {
                WriteLog(TradingSymbol + " " + userID, "GetOpenPostionPrice: return 0");
                return 0;
            }
                openPrice = kiteConnectWrapperTemp.GetOpenPostionPricebySymbol(TradingSymbol, Exchange);
            }
            else if(isSamcoBroker)
            {
                SamcoConnectWrapper samcoConnectWrapperTemp;

                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "GetOpenPostionPrice: return 0");
                    return 0;
                }
                openPrice = samcoConnectWrapperTemp.GetOpenPostionPricebySymbol(TradingSymbol, Exchange);
            }
             else
            {
                CompositeConnectWrapper CompositeConnectWrapperTemp;

                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out CompositeConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "GetOpenPostionPrice: return 0");
                    return 0;
                }
                openPrice = CompositeConnectWrapperTemp.GetOpenPostionPricebySymbol(TradingSymbol, Exchange);
            }
            return openPrice;
        }
        public bool IsPendingBOOrder(string userID, string TradingSymbol,string Exchange, string TransactionType,string Product = Constants.PRODUCT_MIS)
        {
           
            bool res = IsPendingOrder(userID, TradingSymbol, Exchange, TransactionType, false,Product:Product);           
            return res;
        }

        public bool IsPendingBOOrderWithParent(string userID, string TradingSymbol, string Exchange, string TransactionType, string Product = Constants.PRODUCT_MIS)
        {
            bool res = IsPendingOrder(userID, TradingSymbol, Exchange, TransactionType, true, Product: Product);            
            return res;
        }

        public bool GetHighLow(string userID,string TradingSymbol,int barCount , out double dHigh, out double dLow)
        {
            dHigh = 0;
            dLow = 0;
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp = new KiteConnectWrapper();
                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    return false;
                }
                kiteConnectWrapperTemp.GetHighLow(TradingSymbol, barCount, out dHigh, out dLow);
            }
            else if (isSamcoBroker)
            {
                SamcoConnectWrapper samcoConnectWrapperTemp;

                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    return false;
                }
                samcoConnectWrapperTemp.GetHighLow(TradingSymbol, barCount, out dHigh, out dLow);
            }
            else
            {
                CompositeConnectWrapper compositeConnectWrapperTemp;
                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                }
                else
                {
                    return false;
                }
                compositeConnectWrapperTemp.GetHighLow(TradingSymbol, barCount, out dHigh, out dLow);
            }
            return true;
        }

        public bool GetCloseValuesList(string userID, string TradingSymbol, int barCount, out List<double>closeValues, out List<string> dateTimeValues)
        {
            closeValues = new List<double>();
            dateTimeValues = new List<string>();
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp = new KiteConnectWrapper();
                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    return false;
                }
                kiteConnectWrapperTemp.GetCloseValuesList(TradingSymbol, barCount, out closeValues, out dateTimeValues);
            }
            else if (isSamcoBroker)
            {
                SamcoConnectWrapper samcoConnectWrapperTemp;

                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    return false;
                }
                samcoConnectWrapperTemp.GetCloseValuesList(TradingSymbol, barCount, out closeValues, out dateTimeValues);
            }
            else
            {
                CompositeConnectWrapper compositeConnectWrapperTemp;

                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                }
                else
                {
                    return false;
                }
                compositeConnectWrapperTemp.GetCloseValuesList(TradingSymbol, barCount, out closeValues, out dateTimeValues);
            }
            return true;
        }

        public PositionResponse GetOpenPosition(string userID)
        {
            dynamic position = 0;
            if (isZerodhaBroker)
            {
            KiteConnectWrapper kiteConnectWrapperTemp = new KiteConnectWrapper();
            if (kiteConnectObjects.ContainsKey(userID))
            {
                kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
            }
                position = kiteConnectWrapperTemp.GetPositions();
            }
            else if (isSamcoBroker)
            {
                SamcoConnectWrapper SamcoConnectWrapperTemp = new SamcoConnectWrapper();
                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out SamcoConnectWrapperTemp);
                }
                position = SamcoConnectWrapperTemp.GetPositions();
            }
            else
            {
                CompositeConnectWrapper CompositeConnectWrapperTemp = new CompositeConnectWrapper();
                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out CompositeConnectWrapperTemp);
            }
                position = CompositeConnectWrapperTemp.GetPositions();
            }
            return position;
        }

        public double RoundOff(string userID, string symbol, decimal normalValue, bool flag)
        {
            decimal roundedStopLossValue;
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp;
                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    return 0;
                }

                roundedStopLossValue = kiteConnectWrapperTemp.RoundStopLossValueOfAFL(symbol, normalValue, flag);
            }
            else if (isSamcoBroker)
            {
                SamcoConnectWrapper samcoConnectWrapperTemp;

                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    return 0;
                }
                roundedStopLossValue = samcoConnectWrapperTemp.RoundStopLossValueOfAFL(symbol, normalValue, flag);
            }
            else
            {
                CompositeConnectWrapper compositeConnectWrapperTemp;

                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                }
                else
                {
                    return 0;
                }
                roundedStopLossValue = compositeConnectWrapperTemp.RoundStopLossValueOfAFL(symbol, normalValue, flag);
            }
            double returnvalue = Convert.ToDouble(roundedStopLossValue);
            return returnvalue;
        }
       

        public void CheckTradingSymbolPresentOrNot(string userID, List<string> list)
        {
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp;

                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    return;
                }
                kiteConnectWrapperTemp.CheckTradingSymbolPresentOrNot(list);
            }
            else if (isSamcoBroker)
            {
                SamcoConnectWrapper samcoConnectWrapperTemp;

                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    return;
                }
                samcoConnectWrapperTemp.CheckTradingSymbolPresentOrNot(list);
            }
            else
            {
                CompositeConnectWrapper compositeConnectWrapperTemp;

                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                }
                else
                {
                    return;
                }
                compositeConnectWrapperTemp.CheckTradingSymbolPresentOrNot(list);
            }
            return;
        }

        public string getTickCurrentTime()
        {
            if (isZerodhaBroker)
            {
                return kiteWrapperConnect.getTickCurrentTime();
            }
            else if (isSamcoBroker)
            {
                return samcoWrapperConnect.getTickCurrentTime();
            }
            else
            {
                return compositeWrapperConnect.getTickCurrentTime();
            }
        }

        public List<string> getOHCLValues(string TradingSymbol)
        {
            if (isZerodhaBroker)
            {
                return kiteWrapperConnect.getOHCLValues(TradingSymbol);
            }
            else
            {
                return samcoWrapperConnect.getOHCLValues(TradingSymbol);
            }
        }

        public string getVariety(string TradingSymbol,string Exchange)
        {
            if (isZerodhaBroker)
            {
                return kiteWrapperConnect.getVariety(TradingSymbol, Exchange);
            }
            else
            {
                return samcoWrapperConnect.getVariety(TradingSymbol, Exchange);
            }
        }

        public bool stopThread()
        {
            if (isZerodhaBroker)
            {
                return kiteWrapperConnect.StopThread();
            }
            else if(isSamcoBroker)
            {
                return samcoWrapperConnect.StopThread();
            }
            //sanika::18-Jan-2021::Added condition for composite 
            else if(isCompositeBroker)
            {
                return compositeWrapperConnect.StopThread();
            }
            return false;
        }

        public double GetStopPrice(string userID, string TradingSymbol,string Exchange, string TransactionType)
        {
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp;
                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "GetStopPrice: return 0");
                    return 0;
                }

                return kiteConnectWrapperTemp.GetStopPrice(TradingSymbol, Exchange, TransactionType);
            }
            else if (isSamcoBroker)
            {
                SamcoConnectWrapper samcoConnectWrapperTemp;
                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "GetStopPrice: return 0");
                    return 0;
                }

                return samcoConnectWrapperTemp.GetStopPrice(TradingSymbol, Exchange, TransactionType);
            }
            else
            {
                CompositeConnectWrapper compositeConnectWrapperTemp;
           
                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "GetStopPrice: return 0");
                    return 0;
                }
                return compositeConnectWrapperTemp.GetStopPrice(TradingSymbol, Exchange, TransactionType);
            }
        }

        public double GetLimitPrice(string userID, string TradingSymbol,string Exchange, string TransactionType)
        {
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp;
                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "GetLimitPrice: return 0");
                    return 0;
                }

                return kiteConnectWrapperTemp.GetLimitPrice(TradingSymbol, Exchange, TransactionType);
            }
            else if (isSamcoBroker)
            {
                SamcoConnectWrapper samcoConnectWrapperTemp;
                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "GetStopPrice: return 0");
                    return 0;
                }

                return samcoConnectWrapperTemp.GetLimitPrice(TradingSymbol, Exchange, TransactionType);
            }
            else
            {
                CompositeConnectWrapper compositeConnectWrapperTemp;

                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "GetStopPrice: return 0");
                    return 0;
                }

                return compositeConnectWrapperTemp.GetLimitPrice(TradingSymbol, Exchange, TransactionType);
            }

        }

        public string GetTransactionType(string userID, string TradingSymbol, string Exchange)
        {
            string BuyOrSell;
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp;

                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "GetTransactionType: return null");
                    return null;
                }
                BuyOrSell = kiteConnectWrapperTemp.GetTransactionType(TradingSymbol, Exchange);
            }
            else if (isSamcoBroker)
            {
                SamcoConnectWrapper samcoConnectWrapperTemp;
                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "GetTransactionType: return null");
                    return null;
                }

                BuyOrSell = samcoConnectWrapperTemp.GetTransactionType(TradingSymbol, Exchange);
            }
            else
            {
                CompositeConnectWrapper compositeConnectWrapperTemp;

                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "GetTransactionType: return null");
                    return null;
                }

                BuyOrSell = compositeConnectWrapperTemp.GetTransactionType(TradingSymbol, Exchange);
            }
            // WriteLog(TradingSymbol + " " + userID, "GetTransactionType: " + BuyOrSell);           
            return BuyOrSell;
        }

        public void WriteDataIntoFile(string userID,string TradingSymbol, string DateTime, double Open, double High, double Low, double Close, double Volume,bool bWriteinDB, bool bCommit)
        {
            try
            {
                if (isZerodhaBroker)
                {
                    //WriteLog(TradingSymbol + " " + userID, "WriteDataIntoFile: In function");
                    KiteConnectWrapper kiteConnectWrapperTemp;

                    if (kiteConnectObjects.ContainsKey(userID))
                    {
                        kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                    }
                    else
                    {
                        WriteLog(TradingSymbol + " " + userID, "WriteDataIntoFile: return");
                        return;
                    }
                    kiteConnectWrapperTemp.WriteDataIntoFile(TradingSymbol, DateTime, Open, High, Low, Close, Volume, bWriteinDB, bCommit);
                    //WriteLog(TradingSymbol + " " + userID, "WriteDataIntoFile: End");        
                }
                else if(isSamcoBroker)
                {
                    SamcoConnectWrapper samcoConnectWrapperTemp;
                    if (samcoConnectObjects.ContainsKey(userID))
                    {
                        samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                    }
                    else
                    {
                        WriteLog(TradingSymbol + " " + userID, "WriteDataIntoFile: return");
                        return;
                    }

                    samcoConnectWrapperTemp.WriteDataIntoFile(TradingSymbol, DateTime, Open, High, Low, Close, Volume, bWriteinDB, bCommit);
                }
            }
            catch(Exception e)
            {
                WriteLog(TradingSymbol + " " + userID, "WriteDataIntoFile: error" + e.Message);
            }
        }       

        public void DownloadData(string userID, List<string> TradingSymbolList,int daysForHistroricalData)
        {
            try
            {
                if (isZerodhaBroker)
                {
                    KiteConnectWrapper kiteConnectWrapperTemp;
                    if (kiteConnectObjects.ContainsKey(userID))
                    {
                        kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                    }
                    else
                    {
                        return;
                    }
                    if (userID == "AY1598")
                    {
                        //kiteConnectWrapperTemp.DownloadDataEx(TradingSymbolList, daysForHistroricalData,false);
                        kiteConnectWrapperTemp.DownloadHistoricalData(TradingSymbolList, daysForHistroricalData, false);
                    }
                    else
                    {
                        kiteConnectWrapperTemp.ConnectAccount();
                        kiteConnectWrapperTemp.DownloadHistoricalData(TradingSymbolList, daysForHistroricalData, true);
                        //kiteConnectWrapperTemp.DownloadDataEx(TradingSymbolList, daysForHistroricalData,true);
                    }
                }
                else if(isSamcoBroker)
                {
                    SamcoConnectWrapper samcoConnectWrapperTemp;
                    if (samcoConnectObjects.ContainsKey(userID))
                    {
                        samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                    }
                    else
                    {
                        return;
                    }

                    samcoConnectWrapperTemp.DownloadDataEx(TradingSymbolList, daysForHistroricalData);
                }
                else
                {
                    CompositeConnectWrapper compositeConnectWrapperTemp;
                    if (compositeConnectObjects.ContainsKey(userID))
                    {
                        compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                    }
                    else
                    {
                        return;
                    }

                    compositeConnectWrapperTemp.DownloadDataEx(TradingSymbolList, daysForHistroricalData);
                }
            }
            catch(Exception er)
            {

            }
        }

        public bool IsPositionOpen(string userID, string TradingSymbol, string Exchange)
        {
            bool isOpen = false;
            try
            {
                if (isZerodhaBroker)
                {
                KiteConnectWrapper kiteConnectWrapperTemp;
                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    return false;
                }
                isOpen = kiteConnectWrapperTemp.IsPositionOpen(TradingSymbol, Exchange);
                }
                else if (isSamcoBroker)
                {
                    SamcoConnectWrapper samcoConnectWrapperTemp;

                    if (samcoConnectObjects.ContainsKey(userID))
                    {
                        samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                    }
                    else
                    {
                        return false;
                    }
                    isOpen = samcoConnectWrapperTemp.IsPositionOpen(TradingSymbol, Exchange);
                }
                else
                {
                    CompositeConnectWrapper compositeConnectWrapperTemp;

                    if (compositeConnectObjects.ContainsKey(userID))
                    {
                        compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                    }
                    else
                    {
                        return false;
                    }
                    isOpen = compositeConnectWrapperTemp.IsPositionOpen(TradingSymbol, Exchange);
                }
            }
            catch(Exception er)
            {

            }
            return isOpen;
        }

        public double GetStopPricebyOrderID(string userID, string TradingSymbol, string Exchange, string orderId)
        {
            double stopPrice;
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp;

                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "GetStopPricebyOrderID: return 0");
                    return 0;
                }
                stopPrice = kiteConnectWrapperTemp.GetStopPricebyOrderID(TradingSymbol, Exchange, orderId);
            }
            else if (isSamcoBroker)
            {
                SamcoConnectWrapper samcoConnectWrapperTemp;

                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "GetStopPricebyOrderID: return 0");
                    return 0;
                }
                stopPrice = samcoConnectWrapperTemp.GetStopPricebyOrderID(TradingSymbol, Exchange, orderId);
            }
            else
            {
                CompositeConnectWrapper compositeConnectWrapperTemp;

                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "GetStopPricebyOrderID: return 0");
                    return 0;
                }
                stopPrice = compositeConnectWrapperTemp.GetStopPricebyOrderID(TradingSymbol, Exchange, orderId);
            }
            return stopPrice;
        }

        public void RollOver(string userID,string TradingSymbol, string Exchange, string Product)
        {
            try
            {
                if (isZerodhaBroker)
                { 
                    KiteConnectWrapper kiteConnectWrapperTemp;

                    if (kiteConnectObjects.ContainsKey(userID))
                    {
                        kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                    }
                    else
                    {
                        WriteLog(TradingSymbol + " " + userID, "RollOver: return 0");
                        return;
                    }
                    kiteConnectWrapperTemp.RollOver(TradingSymbol, Exchange, Product);
                }
                else if(isSamcoBroker)
                {
                    SamcoConnectWrapper samcoConnectWrapperTemp;

                    if (samcoConnectObjects.ContainsKey(userID))
                    {
                        samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                    }
                    else
                    {
                        WriteLog(TradingSymbol + " " + userID, "RollOver: return 0");
                        return;
                    }
                    samcoConnectWrapperTemp.RollOver(TradingSymbol, Exchange, Product);
                }
            }
            catch(Exception e)
            {

            }
            return;
        }

        public void AddOrderIdInMasterList(string userID, string TradingSymbol, string OrderId)
        {
            try
            {
                if (isZerodhaBroker)
                {
                KiteConnectWrapper kiteConnectWrapperTemp;

                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "AddOrderIdInMasterList: return 0");
                    return;
                }
                kiteConnectWrapperTemp.AddOrderIdInMasterList(TradingSymbol, OrderId);
            }
                else if (isSamcoBroker)
                {
                    SamcoConnectWrapper samcoConnectWrapperTemp;

                    if (samcoConnectObjects.ContainsKey(userID))
                    {
                        samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                    }
                    else
                    {
                        WriteLog(TradingSymbol + " " + userID, "AddOrderIdInMasterList: return 0");
                        return;
                    }
                    samcoConnectWrapperTemp.AddOrderIdInMasterList(TradingSymbol, OrderId);
                }
                else
                {
                    CompositeConnectWrapper compositeConnectWrapperTemp;

                    if (compositeConnectObjects.ContainsKey(userID))
                    {
                        compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                    }
                    else
                    {
                        WriteLog(TradingSymbol + " " + userID, "AddOrderIdInMasterList: return 0");
                        return;
                    }
                    compositeConnectWrapperTemp.AddOrderIdInMasterList(TradingSymbol, OrderId);
                }
            }
            catch (Exception e)
            {
                WriteLog(TradingSymbol + " " + userID, "AddOrderIdInMasterList : Exception Error Message = "+e.Message);
            }
            return;
        }

        public void RemoveOrderIdFromMasterList(string userID, string TradingSymbol)
        {
            try
            {
                if (isZerodhaBroker)
                {
                KiteConnectWrapper kiteConnectWrapperTemp;

                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "RemoveOrderIdFromMasterList: return 0");
                    return;
                }
                kiteConnectWrapperTemp.RemoveOrderIdFromMasterList(TradingSymbol);
            }
                else if (isSamcoBroker)
                {
                    SamcoConnectWrapper samcoConnectWrapperTemp;

                    if (samcoConnectObjects.ContainsKey(userID))
                    {
                        samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                    }
                    else
                    {
                        WriteLog(TradingSymbol + " " + userID, "AddOrderIdInMasterList: return 0");
                        return;
                    }
                    samcoConnectWrapperTemp.RemoveOrderIdFromMasterList(TradingSymbol);
                }
                else
                {
                    CompositeConnectWrapper compositeConnectWrapperTemp;

                    if (compositeConnectObjects.ContainsKey(userID))
                    {
                        compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                    }
                    else
                    {
                        WriteLog(TradingSymbol + " " + userID, "AddOrderIdInMasterList: return 0");
                        return;
                    }
                    compositeConnectWrapperTemp.RemoveOrderIdFromMasterList(TradingSymbol);
                }
            }
            catch (Exception e)
            {
                WriteLog(TradingSymbol + " " + userID, "RemoveOrderIdFromMasterList : Exception Error Message = " + e.Message);
            }
            return;
        }

        public string GetOrderIdFromMasterList(string userID, string TradingSymbol)
        {
            string orderID = "NA";
            try
            {
                if (isZerodhaBroker)
                {
                    KiteConnectWrapper kiteConnectWrapperTemp;

                    if (kiteConnectObjects.ContainsKey(userID))
                    {
                        kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                    }
                    else
                    {
                        WriteLog(TradingSymbol + " " + userID, "GetOrderIdFromMasterList: return 0");
                        return orderID;
                    }
                    orderID = kiteConnectWrapperTemp.GetOrderIdFromMasterList(TradingSymbol);
                }
                else if (isSamcoBroker)
                {
                    SamcoConnectWrapper samcoConnectWrapperTemp;

                    if (samcoConnectObjects.ContainsKey(userID))
                    {
                        samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                    }
                    else
                    {
                        WriteLog(TradingSymbol + " " + userID, "GetOrderIdFromMasterList: return 0");
                        return orderID;
                    }
                    orderID = samcoConnectWrapperTemp.GetOrderIdFromMasterList(TradingSymbol);
                }
                else
                {
                    CompositeConnectWrapper compositeConnectWrapperTemp;

                    if (compositeConnectObjects.ContainsKey(userID))
                    {
                        compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                    }
                    else
                    {
                        WriteLog(TradingSymbol + " " + userID, "GetOrderIdFromMasterList: return 0");
                        return orderID;
                    }
                    orderID = compositeConnectWrapperTemp.GetOrderIdFromMasterList(TradingSymbol);
                }
            }
            catch (Exception e)
            {
                WriteLog(TradingSymbol + " " + userID, "GetOrderIdFromMasterList : Exception Error Message = " + e.Message);
            }
            return orderID;
        }

        //IRDSPM::Jyoti::26-06-2020::Time difference calculation using interval
        public bool CheckWithinTime(string userID,string strDateTime, int interval, string key)
        {
            try
            {
                if (isZerodhaBroker)
                {
                    KiteConnectWrapper kiteConnectWrapperTemp;

                    if (kiteConnectObjects.ContainsKey(userID))
                    {
                        kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                    }
                    else
                    {
                        return false;
                    }

                    return kiteConnectWrapperTemp.CheckWithinTime(strDateTime, interval, key);
                }
                else if(isSamcoBroker)
                {
                    SamcoConnectWrapper samcoConnectWrapperTemp;

                    if (samcoConnectObjects.ContainsKey(userID))
                    {
                        samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                    }
                    else
                    {
                        return false;
                    }

                    return samcoConnectWrapperTemp.CheckWithinTime(strDateTime, interval, key);
                }
            }
            catch(Exception e)
            {

            }
            return false;
        }

        public void InsertMessageDetails(string userID,string datestr, int signal, string name, string key)
        {
            try
            {
                if (isZerodhaBroker)
                {
                KiteConnectWrapper kiteConnectWrapperTemp;

                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    return;
                }

                kiteConnectWrapperTemp.InsertMessageDetails(datestr, signal, name, key);
            }
                else if (isSamcoBroker)
                {
                    SamcoConnectWrapper SamcoConnectWrapperTemp;

                    if (samcoConnectObjects.ContainsKey(userID))
                    {
                        samcoConnectObjects.TryGetValue(userID, out SamcoConnectWrapperTemp);
                    }
                    else
                    {
                        return;
                    }

                    SamcoConnectWrapperTemp.InsertMessageDetails(datestr, signal, name, key);
                }
            }
            catch (Exception e)
            {

            }            
        }        

        //IRDSPM::Pratiksha::26-06-2020::retrival of key
        public void GetMessagePosition(string userID,string tablename)
        {
            try
            {
                if (isZerodhaBroker)
                {
                KiteConnectWrapper kiteConnectWrapperTemp;

                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    return;
                }

                kiteConnectWrapperTemp.GetMessagePosition(tablename);
            }
                else if (isSamcoBroker)
                {
                    SamcoConnectWrapper samcoConnectWrapperTemp;

                    if (samcoConnectObjects.ContainsKey(userID))
                    {
                        samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                    }
                    else
                    {
                        return;
                    }

                    samcoConnectWrapperTemp.GetMessagePosition(tablename);
                }
            }
            catch (Exception e)
            {

            }            
        }

        public string GetOpenPostionDirectionByOrderID(string userID,
                                      string TradingSymbol,
                                      string Exchange,
                                      string OrderId)
        {
            string direction;
            if (isZerodhaBroker)
            {
            KiteConnectWrapper kiteConnectWrapperTemp;

            if (kiteConnectObjects.ContainsKey(userID))
            {
                kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
            }
            else
            {
                WriteLog(TradingSymbol + " " + userID, "GetOpenPostionPrice: return 0");
                return "";
            }
                direction = kiteConnectWrapperTemp.GetOpenPostionDirectionByOrderId(TradingSymbol, Exchange, OrderId);
            }
            else if(isSamcoBroker)
            {
                SamcoConnectWrapper samcoConnectWrapperTemp;

                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "GetOpenPostionPrice: return 0");
                    return "";
                }
                direction = samcoConnectWrapperTemp.GetOpenPostionDirectionByOrderId(TradingSymbol, Exchange, OrderId);
            }
            else
            {
                CompositeConnectWrapper compositeConnectWrapperTemp;

                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "GetOpenPostionPrice: return 0");
                    return "";
                }
                direction = compositeConnectWrapperTemp.GetOpenPostionDirectionByOrderId(TradingSymbol, Exchange, OrderId);
            }
            return direction;
        }

        public string GetOrderStatusByOrderID(string userID,
                                      string TradingSymbol,
                                      string Exchange,
                                      string OrderId)
        {
            string status = "ordernotupdated";
            if (isZerodhaBroker)
            {
            KiteConnectWrapper kiteConnectWrapperTemp;

            if (kiteConnectObjects.ContainsKey(userID))
            {
                kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
            }
            else
            {
                WriteLog(TradingSymbol + " " + userID, "GetOpenPostionPrice: return 0");
                return status;
            }
                status = kiteConnectWrapperTemp.GetOrderStatusByOrderID(TradingSymbol, Exchange, OrderId);
            }
            else if (isSamcoBroker)
            {
                SamcoConnectWrapper samcoConnectWrapperTemp;

                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "GetOrderStatusByOrderID: return 0");
                    return status;
                }
                status = samcoConnectWrapperTemp.GetOrderStatusByOrderID(TradingSymbol, Exchange, OrderId);
            }
            else
            {
                CompositeConnectWrapper compositeConnectWrapperTemp;

                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "GetOrderStatusByOrderID: return 0");
                    return status;
                }
                status = compositeConnectWrapperTemp.GetOrderStatusByOrderID(TradingSymbol, Exchange, OrderId);
            }
            return status;
        }

        //IRDSPM::Pratiksha::01-07-2020::For getting list of symbol --start
        public List<string> Getlistsymbols(string exchange)
        {
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp = new KiteConnectWrapper();
                return kiteConnectWrapperTemp.FetchDetails(exchange);
            }
            else
            {
                return samcoWrapperConnect.FetchDetails(exchange);
            }
        }
        //IRDSPM::Pratiksha::01-07-2020::For getting list of symbol --end

        public bool GetLoginStatus(string userID)
        {
            bool isLoggedIn = false;
            try
            {
                if (isZerodhaBroker)
                {
                    KiteConnectWrapper kiteConnectWrapperTemp;

                    if (kiteConnectObjects.ContainsKey(userID))
                    {
                        kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                    }
                    else
                    {
                        return false;
                    }
                    
                    isLoggedIn = kiteConnectWrapperTemp.GetConnectionStatus(false);
                }
                else if (isSamcoBroker)
                {
                    SamcoConnectWrapper samcoConnectWrapperTemp;

                    if (samcoConnectObjects.ContainsKey(userID))
                    {
                        samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                    }
                    else
                    {
                        return false;
                    }
                    //sanika::11-sep-2020::Added for bridge connection
                    isLoggedIn = samcoConnectWrapperTemp.GetConnectionStatus(false);
                }
                else
                {
                    if(isCompositeBroker)
                    {
                        CompositeConnectWrapper compositeConnectWrapperTemp;

                        if (compositeConnectObjects.ContainsKey(userID))
                        {
                            compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                        }
                        else
                        {
                            return false;
                        }
                        isLoggedIn = compositeConnectWrapperTemp.GetConnectionStatus();
                    }
                }
            }
            catch(Exception e)
            {

            }
            return isLoggedIn;
        }

        public bool Connect(string userID)
        {
            bool isLoggedIn = false;
            try
            {
                if (isZerodhaBroker)
                {
                KiteConnectWrapper kiteConnectWrapperTemp;

                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    return false;
                }
                isLoggedIn = kiteConnectWrapperTemp.Connect();
                }
                else if(isSamcoBroker)
                {
                    SamcoConnectWrapper samcoConnectWrapperTemp;

                    if (samcoConnectObjects.ContainsKey(userID))
                    {
                        samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                    }
                    else
                    {
                        return false;
                    }
                    isLoggedIn = samcoConnectWrapperTemp.Connect();
                }
                else
                {
                    CompositeConnectWrapper compositeConnectWrapperTemp;

                    if (compositeConnectObjects.ContainsKey(userID))
                    {
                        compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                    }
                    else
                    {
                        return false;
                    }
                    isLoggedIn = compositeConnectWrapperTemp.Connect();
                }
            }
            catch (Exception e)
            {

            }
            return isLoggedIn;
        }

        public string GetStartTime(string userID)
        {
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp;

                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    return "";
                }

                return kiteConnectWrapperTemp.GetStartTime();
            }
            else if (isSamcoBroker)
            {
                SamcoConnectWrapper samcoConnectWrapperTemp;

                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    return "";
                }
                return samcoConnectWrapperTemp.GetStartTime();
            }
            return "";
        }

        public string GetEndTime(string userID)
        {
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp;

                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    return "";
                }

                return kiteConnectWrapperTemp.GetEndTime();
            }
            else if (isSamcoBroker)
            {
                SamcoConnectWrapper samcoConnectWrapperTemp;

                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    return "";
                }
                return samcoConnectWrapperTemp.GetEndTime();
            }
            return "";
        }

        public string GetTimelLimitToPlaceOrder(string userID)
        {
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp;

                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    return "";
                }

                return kiteConnectWrapperTemp.GetTimeLimitToPlaceOrder();
            }
            else if (isSamcoBroker)
            {
                SamcoConnectWrapper samcoConnectWrapperTemp;

                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    return "";
                }
                return samcoConnectWrapperTemp.GetTimeLimitToPlaceOrder();
            }
            return "";
        }

        public string GetTimelLimitToExistOrder(string userID)
        {
            if (isZerodhaBroker)
            { 
                KiteConnectWrapper kiteConnectWrapperTemp;

                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    return "";
                }

                return kiteConnectWrapperTemp.GetTimeLimitToExistOrder();
            }
            else if (isSamcoBroker)
            {
                SamcoConnectWrapper samcoConnectWrapperTemp;

                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    return "";
                }
                return samcoConnectWrapperTemp.GetTimeLimitToExistOrder();
            }
            return "";
        }

        public double GetOverAllLoss(string userID)
        {
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp;

                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    return 0;
                }

                return kiteConnectWrapperTemp.GetOverAllLoss();
            }
            else if (isSamcoBroker)
            {
                SamcoConnectWrapper samcoConnectWrapperTemp;

                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    return 0;
                }
                return samcoConnectWrapperTemp.GetOverAllLoss();
            }
            return 0;
        }

        public double GetOverAllProfit(string userID)
        {
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp;

                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    return 0;
                }

                return kiteConnectWrapperTemp.GetOverAllProfit();
            }
            else if (isSamcoBroker)
            {
                SamcoConnectWrapper samcoConnectWrapperTemp;

                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    return 0;
                }
                return samcoConnectWrapperTemp.GetOverAllProfit();
            }
            return 0;

        }

        public bool SetBroker(int brokername)
        {
            //IRDS::03-Jul-2020::Jyoti::added for parameter as int changes
            BrokerNames brokerName = (BrokerNames)brokername;
            switch (brokerName)
            {
                case BrokerNames.ZERODHABROKER:
                    {
                        isCompositeBroker = false;
                        isSamcoBroker = false;
                        isZerodhaBroker = true;
                    }
                    break;

                case BrokerNames.SAMCOBROKER:
                    {
                        isZerodhaBroker = false;
                        isCompositeBroker = false;
                        isSamcoBroker = true;
                    }
                    break;

                case BrokerNames.COMPOSITEBROKER:
                    {
                        isZerodhaBroker = false;
                        isSamcoBroker = false;
                        isCompositeBroker = true;
                    }
                    break;
            }

            return true;
        }

        public string PlaceCOMarketOrder(string userID,
                               string Exchange,
                               string TradingSymbol,
                               string TransactionType,
                               int Quantity,
                               decimal TriggerPrice,
                               string Product = Constants.PRODUCT_MIS)
        {
            string orderId = "NA";
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp;

                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    return orderId;
                }

                return kiteConnectWrapperTemp.PlaceOrder(Exchange,TradingSymbol,TransactionType,Quantity,Constants.ORDER_TYPE_MARKET,TriggerPrice:TriggerPrice, Variety:Constants.VARIETY_CO);
            }
            else if (isSamcoBroker)
            {
                SamcoConnectWrapper SamcoConnectWrapperTemp;

                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out SamcoConnectWrapperTemp);
                }
                else
                {
                    return orderId;
                }

                return SamcoConnectWrapperTemp.PlaceOrder(Exchange, TradingSymbol, TransactionType, Quantity, Constants.ORDER_TYPE_MARKET, TriggerPrice: TriggerPrice, Variety: Constants.VARIETY_CO);
            }
            return orderId;
        }

        public string PlaceCOLimitOrder(string userID,
                               string Exchange,
                               string TradingSymbol,
                               string TransactionType,
                               int Quantity,
                               decimal Price,
                               decimal TriggerPrice,
                               string Product = Constants.PRODUCT_MIS)
        {
            string orderId = "NA";
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp;

                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    return orderId;
                }

                return kiteConnectWrapperTemp.PlaceOrder(Exchange, TradingSymbol, TransactionType, Quantity, Constants.ORDER_TYPE_LIMIT,Price, TriggerPrice: TriggerPrice, Variety: Constants.VARIETY_CO);
            }
            else if (isSamcoBroker)
            {
                SamcoConnectWrapper samcoConnectWrapperTemp;

                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    return orderId;
                }

                return samcoConnectWrapperTemp.PlaceOrder(Exchange, TradingSymbol, TransactionType, Quantity, Constants.ORDER_TYPE_LIMIT, Price, TriggerPrice: TriggerPrice, Variety: Constants.VARIETY_CO);
            }
            return orderId;
        }

        public bool CloseCOOrder(string userID, string TradingSymbol, string Exchange = Constants.EXCHANGE_NSE, string Product = Constants.PRODUCT_MIS)
        {
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp;

                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "CloseCOOrder: return false");
                    return false;
                }

                if (kiteConnectWrapperTemp.CloseCOOrder(TradingSymbol, Exchange, Product))
                {
                    WriteLog(TradingSymbol + " " + userID, "CloseCOOrder : returns true");
                    return true;
                }                
            }
            else if (isSamcoBroker)
            {
                SamcoConnectWrapper samcoConnectWrapperTemp;

                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "CloseCOOrder: return false");
                    return false;
                }

                if (samcoConnectWrapperTemp.CloseCOOrder(TradingSymbol, Exchange, Product))
                {
                    WriteLog(TradingSymbol + " " + userID, "CloseCOOrder : returns true");
                    return true;
                }
            }
            return false;

        }

        public bool CloseCOOrderWithOrderId(string userID, string OrderId, string Product = Constants.VARIETY_CO)
        {
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp;

                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    //WriteLog(TradingSymbol + " " + userID, "CloseCOOrder: return false");
                    return false;
                }

                if (kiteConnectWrapperTemp.CloseCOOrderWithOrder(OrderId, Product))
                {
                   // WriteLog(TradingSymbol + " " + userID, "CloseCOOrder : returns true");
                    return true;
                }
            }
            else if (isSamcoBroker)
            {
                SamcoConnectWrapper samcoConnectWrapperTemp;

                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    //WriteLog(TradingSymbol + " " + userID, "CloseCOOrder: return false");
            return false;
                }

                if (samcoConnectWrapperTemp.CloseCOOrderWithOrder(OrderId, Product))
                {
                    // WriteLog(TradingSymbol + " " + userID, "CloseCOOrder : returns true");
                    return true;
                }
            }
            return false;
        }

        public string GetOrderId(string userID, string TradingSymbol, string Exchange, string TransactionType, string Product)
        {
            string orderId = "ordernotfound";
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp;

                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    //WriteLog(TradingSymbol + " " + userID, "CloseCOOrder: return false");
                    return orderId;
                }

                orderId = kiteConnectWrapperTemp.GetOrderId(TradingSymbol, Exchange, TransactionType, Product);
               
            }
            else if (isSamcoBroker)
            {
                SamcoConnectWrapper samcoConnectWrapperTemp;

                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    //WriteLog(TradingSymbol + " " + userID, "CloseCOOrder: return false");
                    return orderId;
                }

                orderId = samcoConnectWrapperTemp.GetOrderId(TradingSymbol, Exchange, TransactionType, Product);
            }
            return orderId;
        }

        public string GetParentOrderId(string userID, string orderId,string TradingSymbol,string Exchange)
        {
            string parentOrderId = "ordernotfound";
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp;

                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    //WriteLog(TradingSymbol + " " + userID, "CloseCOOrder: return false");
                    return parentOrderId;
                }

                parentOrderId = kiteConnectWrapperTemp.GetParentOrderId(TradingSymbol,Exchange,orderId);

            }
            else if (isSamcoBroker)
            {
                SamcoConnectWrapper samcoConnectWrapperTemp;

                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    //WriteLog(TradingSymbol + " " + userID, "CloseCOOrder: return false");
                    return parentOrderId;
                }

                parentOrderId = samcoConnectWrapperTemp.GetParentOrderId(TradingSymbol, Exchange, orderId);
            }
            return parentOrderId;
        }

        public bool GetBidAndAskPrice(string userID, long InstrumentId,string Exchange, out double BidPrice,out double AskPrice)
        {
            BidPrice = 0;
            AskPrice = 0;
            CompositeConnectWrapper compositeConnectWrapperTemp = new CompositeConnectWrapper();
            if (compositeConnectObjects.ContainsKey(userID))
            {
                compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
            }
            else
            {
                return false;
            }
            if(compositeConnectWrapperTemp.GetBidAndAskPrice(InstrumentId,Exchange, out BidPrice,out AskPrice))
            {
                return true;
            }
            return false;
        }
        public int GetOptionsLotSize(string userID, string OptionsSymbol, string Exchange)
        {
            int lotSize = 0;
            if (isCompositeBroker)
            {
                CompositeConnectWrapper compositeConnectWrapperTemp = new CompositeConnectWrapper();
                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                }
                else
                {
                    return 0;
                }
                lotSize = compositeConnectWrapperTemp.GetOptionsLotSize(OptionsSymbol, Exchange);
            }
            return lotSize;
        }

        public string GetToken(string userID, string symbol, string exchange)
        {
            string token = "";
            if (isCompositeBroker)
            {
                CompositeConnectWrapper compositeConnectWrapperTemp = new CompositeConnectWrapper();
                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                }
                else
                {
                    return "";
                }
                token = compositeWrapperConnect.GetToken(symbol);
            }
            return token;
        }

        public string GetOptionsInstrumentToken(string userID, string OptionsSymbol, string Exchange)
        {
            string instrumentToken = "";
            //sanika::27-Jul-2021::added code for zerodha
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp = new KiteConnectWrapper();
                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    return "";
                }
                instrumentToken = kiteConnectWrapperTemp.getOptionsInstrumentToken(OptionsSymbol, Exchange);
            }
            if (isCompositeBroker)
            {
                CompositeConnectWrapper compositeConnectWrapperTemp = new CompositeConnectWrapper();
                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                }
                else
                {
                    return "";
                }
                instrumentToken = compositeConnectWrapperTemp.getOptionsInstrumentToken(OptionsSymbol, Exchange);
            }
            return instrumentToken;
        }

        public int GetOpenPostionQuantityByOrderID(string userID,
                                      string TradingSymbol,
                                      string Exchange,
                                      string OrderId)
        {
            int quantity =0;
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp;

                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "GetOpenPostionQuantityByOrderID: return 0");
                    return quantity;
                }
                quantity = kiteConnectWrapperTemp.GetOpenPostionQuantityByOrderID(TradingSymbol, Exchange, OrderId);
            }
            else if (isSamcoBroker)
            {
                SamcoConnectWrapper samcoConnectWrapperTemp;
           
                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "GetOpenPostionQuantityByOrderID: return 0");
                    return quantity;
                }
                quantity = samcoConnectWrapperTemp.GetOpenPostionQuantityByOrderID(TradingSymbol, Exchange, OrderId);
            }
            return quantity;
        }

        public int GetOpenPostionQuantity(string userID,
                                  string TradingSymbol,
                                  string Exchange,
                                  string Product)
        {
            int quantity = 0;
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp;

                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "GetOpenPostionQuantityByOrderID: return 0");
                    return quantity;
                }
                quantity = kiteConnectWrapperTemp.GetOpenPostionQuantity(TradingSymbol, Exchange, Product);
            }           
            else if (isSamcoBroker)
            {
                SamcoConnectWrapper samcoConnectWrapperTemp;

                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "GetOpenPostionQuantity: return 0");
                    return quantity;
                }
                quantity = samcoConnectWrapperTemp.GetOpenPostionQuantity(TradingSymbol, Exchange, Product);
            }
            return quantity;
        }

        public bool CloseOrderWithQuantity(string userID, string TradingSymbol, int Quantity,string Exchange = Constants.EXCHANGE_NSE, string Product = Constants.PRODUCT_MIS)
        {
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp;

                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "CloseOrder: return false");
                    return false;
                }

                if (kiteConnectWrapperTemp.CloseOrderWithQuantity(TradingSymbol, Exchange, Product, Quantity))
                {
                    WriteLog(TradingSymbol + " " + userID, "CloseOrder : returns true");
                    return true;
                }                
            }
            else if (isSamcoBroker)
            {
                SamcoConnectWrapper samcoConnectWrapperTemp;

                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "CloseOrder: return false");
                    return false;
                }

                if (samcoConnectWrapperTemp.CloseOrderWithQuantity(TradingSymbol, Exchange, Product, Quantity))
                {
                    WriteLog(TradingSymbol + " " + userID, "CloseOrder : returns true");
                    return true;
                }
            }
            return false;
        }

        public void WriteProfitIntoDB(string userID, string tblName, string TradingSymbol, string Datetime, double Profit)
        {
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp = null;

                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "WriteProfitIntoDB: return false");
                }

                kiteConnectWrapperTemp.WriteProfitIntoDB(tblName, TradingSymbol, Datetime, Profit);
            }
        }

        public double GetProfitFromDB(string userID, string tblName, string symbol, string datetime)
        {
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp = null;

                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    //WriteLog(TradingSymbol + " " + userID, "WriteProfitIntoDB: return false");
                }

                return kiteConnectWrapperTemp.GetProfitFromDB(tblName, symbol, datetime);
            }
            return 0;
        }

        public void DeleteAllProfitRecords(string userID, string tblName)
        {
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp = null;

                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    //WriteLog(TradingSymbol + " " + userID, "WriteProfitIntoDB: return false");
                }

                kiteConnectWrapperTemp.DeleteAllProfitRecords(tblName);
            }
        }

        public double GetTotalProfitFromDB(string userID, string tblName, string datetime)
        {
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp = null;

                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    //WriteLog(TradingSymbol + " " + userID, "WriteProfitIntoDB: return false");
                }

                //return kiteConnectWrapperTemp.GetTotalProfitFromDB(tblName, datetime);
            }
            return 0;
        }

        public bool SetSymbolLastOrder(string userID, string TradingSymbol, string TransactionType)
        {
            bool isSymbolSet = false;
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp = null;

                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    return isSymbolSet;
                }

                isSymbolSet =  kiteConnectWrapperTemp.SetSymbolLastOrder(TradingSymbol, TransactionType);
            }
            else if (isSamcoBroker)
            {
                SamcoConnectWrapper samcoConnectWrapperTemp = null;

                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    return isSymbolSet;
                }

                isSymbolSet = samcoConnectWrapperTemp.SetSymbolLastOrder(TradingSymbol, TransactionType);
            }
            else
            {
                CompositeConnectWrapper compositeConnectWrapperTemp = null;

                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                }
                else
                {
                    return isSymbolSet;
                }

                isSymbolSet = compositeConnectWrapperTemp.SetSymbolLastOrder(TradingSymbol, TransactionType);
            }
            return isSymbolSet;
        }

        public bool RemoveSymbolLastOrder(string userID, string TradingSymbol, string TransactionType)
        {
            bool isRemoveSymbol = false;
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp = null;

                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    return isRemoveSymbol;
                }

                isRemoveSymbol = kiteConnectWrapperTemp.RemoveSymbolLastOrder(TradingSymbol, TransactionType);
            }
            else if (isSamcoBroker)
            {
                SamcoConnectWrapper samcoConnectWrapperTemp = null;

                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    return isRemoveSymbol;
                }

                isRemoveSymbol = samcoConnectWrapperTemp.RemoveSymbolLastOrder(TradingSymbol, TransactionType);
            }
            else
            {
                CompositeConnectWrapper compositeConnectWrapperTemp = null;

                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                }
                else
                {
                    return isRemoveSymbol;
                }

                isRemoveSymbol = compositeConnectWrapperTemp.RemoveSymbolLastOrder(TradingSymbol, TransactionType);
            }
            return isRemoveSymbol;
        }

        public string GetSymbolLastOrder(string userID, string TradingSymbol)
        {
            string lastOrder = "NA";
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp = null;

                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    return lastOrder;
                }

                lastOrder = kiteConnectWrapperTemp.GetSymbolLastOrder(TradingSymbol);
            }
            else if (isSamcoBroker)
            {
                SamcoConnectWrapper samcoConnectWrapperTemp = null;

                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    return lastOrder;
                }

                lastOrder = samcoConnectWrapperTemp.GetSymbolLastOrder(TradingSymbol);
            }
            else
            {
                CompositeConnectWrapper compositeConnectWrapperTemp = null;

                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                }
                else
                {
                    return lastOrder;
                }

                lastOrder = compositeConnectWrapperTemp.GetSymbolLastOrder(TradingSymbol);
            }
            return lastOrder;
        }

        public bool AddManuallyOrderQuantity(string userID, string TradingSymbol, int Quanity)
        {
            bool isSymbolSet = false;
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp = null;

                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    return isSymbolSet;
                }

                isSymbolSet = kiteConnectWrapperTemp.AddManuallyOrderQuantity(TradingSymbol, Quanity);
            }
            else if (isSamcoBroker)
            {
                SamcoConnectWrapper samcoConnectWrapperTemp = null;

                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    return isSymbolSet;
                }

                isSymbolSet = samcoConnectWrapperTemp.AddManuallyOrderQuantity(TradingSymbol, Quanity);
            }
            return isSymbolSet;
        }

        public bool RemoveManuallyOrderQuantity(string userID, string TradingSymbol)
        {
            bool isRemoveSymbol = false;
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp = null;

                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    return isRemoveSymbol;
                }

                isRemoveSymbol = kiteConnectWrapperTemp.RemoveManuallyOrderQuantity(TradingSymbol);
            }
            else if (isSamcoBroker)
            {
                SamcoConnectWrapper samcoConnectWrapperTemp = null;

                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    return isRemoveSymbol;
                }

                isRemoveSymbol = samcoConnectWrapperTemp.RemoveManuallyOrderQuantity(TradingSymbol);
            }
            return isRemoveSymbol;
        }

        public int GetManuallyOrderQuantity(string userID, string TradingSymbol)
        {
            int lastOrderQuantity = 0;
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp = null;

                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    return lastOrderQuantity;
                }

                lastOrderQuantity = kiteConnectWrapperTemp.GetManuallyOrderQuantity(TradingSymbol);
            }
            else if (isSamcoBroker)
            {
                SamcoConnectWrapper samcoConnectWrapperTemp = null;

                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    return lastOrderQuantity;
                }

                lastOrderQuantity = samcoConnectWrapperTemp.GetManuallyOrderQuantity(TradingSymbol);
            }
            return lastOrderQuantity;
        }

        public int GetOrderQuantityByDirection(string userID, string TradingSymbol, string TransactionType)
        {
            int lastOrderQuantity = 0;
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp = null;

                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    return lastOrderQuantity;
                }

                lastOrderQuantity = kiteConnectWrapperTemp.GetOrderQuantityByDirection(TradingSymbol, TransactionType);
            }
            return lastOrderQuantity;
        }

        public bool GetChangeInPercent(string userID,string TradingSymbol,string tickTime,out double highPercent, out double lowPercent)
        {
            highPercent = 0;
            lowPercent = 0;
            try
            {
                if (isZerodhaBroker)
                {
                KiteConnectWrapper kiteConnectWrapperTemp = null;

                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    return false;
                }
                    kiteConnectWrapperTemp.GetChangeInPercent(TradingSymbol, tickTime, out highPercent, out lowPercent);
                }
                else if (isSamcoBroker)
                {
                    SamcoConnectWrapper samcoConnectWrapperTemp = null;

                    if (samcoConnectObjects.ContainsKey(userID))
                    {
                        samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                    }
                    else
                    {
                        return false;
                    }
                    samcoConnectWrapperTemp.GetChangeInPercent(TradingSymbol, tickTime, out highPercent, out lowPercent);
                    //samcoConnectWrapperTemp.GetChangeInPercent(TradingSymbol);
                }
                else if (isCompositeBroker)
                {
                    CompositeConnectWrapper compositeConnectWrapperTemp = null;

                    if (compositeConnectObjects.ContainsKey(userID))
                    {
                        compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                    }
                    else
                    {
                        return false;
                    }

                   compositeConnectWrapperTemp.GetChangeInPercent(TradingSymbol, tickTime, out highPercent, out lowPercent);
                }
            }
            catch(Exception e)
            {
                WriteLog(TradingSymbol + " " + userID, "GetChangeInPercent : Exception Error Message = "+e.Message);
            }
            return true;
        }
        //Sandip::15-August-2021::added function for index
        public bool GetChangeInPercentIndex(string userID, string TradingSymbol, string tickTime, out double highPercent, out double lowPercent)
        {
            highPercent = 0;
            lowPercent = 0;
            try
            {
                if (isZerodhaBroker)
                {
                    KiteConnectWrapper kiteConnectWrapperTemp = null;

                    if (kiteConnectObjects.ContainsKey(userID))
                    {
                        kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                    }
                    else
                    {
                        return false;
                    }
                    kiteConnectWrapperTemp.GetChangeInPercentIndex(TradingSymbol, tickTime, out highPercent, out lowPercent);
                }
                else if (isSamcoBroker)
                {
                    SamcoConnectWrapper samcoConnectWrapperTemp = null;

                    if (samcoConnectObjects.ContainsKey(userID))
                    {
                        samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                    }
                    else
                    {
                        return false;
                    }
                    samcoConnectWrapperTemp.GetChangeInPercent(TradingSymbol, tickTime, out highPercent, out lowPercent);
                    //samcoConnectWrapperTemp.GetChangeInPercent(TradingSymbol);
                }
                else if (isCompositeBroker)
                {
                    CompositeConnectWrapper compositeConnectWrapperTemp = null;

                    if (compositeConnectObjects.ContainsKey(userID))
                    {
                        compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                    }
                    else
                    {
                        return false;
                    }

                    compositeConnectWrapperTemp.GetChangeInPercent(TradingSymbol, tickTime, out highPercent, out lowPercent);
                }
            }
            catch (Exception e)
            {
                WriteLog(TradingSymbol + " " + userID, "GetChangeInPercent : Exception Error Message = " + e.Message);
            }
            return true;
        }

        //sanika::5-nov-2020::added function for rajesh sir's startegy
        public bool GetHighLowFromDB(string userID, string TradingSymbol, string StartTime, string EndTime, out double dHigh, out double dLow)
        {           
            dHigh = 0;
            dLow = 0;
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp = new KiteConnectWrapper();
                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    return false;
                }
                kiteConnectWrapperTemp.GetHighLowFromDB(TradingSymbol, StartTime, EndTime, out dHigh, out dLow);
            }                
            //IRDSPM::Pratiksha::26-05-2021::For samco changes
            else if (isSamcoBroker)
            {
                SamcoConnectWrapper samcoConnectWrapperTemp = new SamcoConnectWrapper();
                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    return false;
                }
                samcoConnectWrapperTemp.GetHighLowFromDB(TradingSymbol, StartTime, EndTime, out dHigh, out dLow);
            }
            else if (isCompositeBroker)
            {
                CompositeConnectWrapper compositeConnectWrapperTemp = new CompositeConnectWrapper();
                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                }
                else
                {
                    return false;
                }
                compositeConnectWrapperTemp.GetHighLowFromDB(TradingSymbol, StartTime, EndTime, out dHigh, out dLow);
            }

            return true;
        } 
        

        //for rajesh sir's strategy
        public string FetchTableName(string userID)
        {
            string tableName = "";
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp = new KiteConnectWrapper();
                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    return tableName;
                }
                tableName = kiteConnectWrapperTemp.FetchTableName();
            }
            //IRDSPM::Pratiksha::02-06-2021::For samco
            else if (isSamcoBroker)
            {
                SamcoConnectWrapper SamcoConnectWrapperTemp = new SamcoConnectWrapper();
                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out SamcoConnectWrapperTemp);
                }
                else
                {
                    return tableName;
                }
                tableName = SamcoConnectWrapperTemp.FetchTableName();
            }
            //Sanika::16-Jan-2021::Added for composite
            else
            {
                CompositeConnectWrapper compositeConnectWrapperTemp = new CompositeConnectWrapper();
                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                }
                else
                {
                    return tableName;
                }
                tableName = compositeConnectWrapperTemp.FetchTableName();
            }
            return tableName;
        }

        public List<string> FetchDetails(string exchange , string userID)
        {
            List<string> Exchanges = null;
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp = new KiteConnectWrapper();
                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    return Exchanges;
                }
                Exchanges = kiteConnectWrapperTemp.FetchDetails(exchange);
            }
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp = new KiteConnectWrapper();
                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    return Exchanges;
                }
                Exchanges = kiteConnectWrapperTemp.FetchDetails(exchange);
            }
            else if (isSamcoBroker)
            {
                //IRDSPM::PRatiksha::02-06-2021::For samco
                SamcoConnectWrapper samcoConnectWrapperTemp = new SamcoConnectWrapper();
                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    return Exchanges;
                }
                Exchanges = samcoConnectWrapperTemp.FetchDetails(exchange);
            }
            else if (isCompositeBroker)
            {
                CompositeConnectWrapper compositeConnectWrapperTemp = new CompositeConnectWrapper();
                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                }
                else
                {
                    return Exchanges;
                }
                Exchanges = compositeConnectWrapperTemp.FetchDetails(exchange);
            }
            return Exchanges;
        }

        //sanika::12-Nov-2020::Added to get product of open position
        public string GetOpenPostionProdcutByOrderID(string userID,
                                    string TradingSymbol,
                                    string Exchange,
                                    string OrderId)
        {
            string product = "productnotfound";
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp;

                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "GetOpenPostionPrice: return 0");
                    return "";
                }
                product = kiteConnectWrapperTemp.GetOpenPostionProductByOrderID(TradingSymbol, Exchange, OrderId);
            }           
            return product;
        }

        //sanika::16-Dec-2020::Added for risk management
        public bool RiskManagement(string userID,string TradingSymbol, string Exchange,double RiskPercent)
        {
            bool res = false;
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp = new KiteConnectWrapper();
                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "RiskManagement: return false");
                    return res;
                }
                res = kiteConnectWrapperTemp.RiskManagement(TradingSymbol, Exchange, RiskPercent);
            }
            return res;
        }

        public void GetFirstTwoTick(string userID, string TradingSymbol,string FirstTickTime,string SecondTickTime,  out double firstCandleHigh, out double firstCandleLow, out double  secondCandleHigh, out double  secondCandleLow)
        {
            firstCandleHigh = firstCandleLow = secondCandleHigh = secondCandleLow = 0;
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp = new KiteConnectWrapper();
                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "GetFirstTwoTick: return false");
                    return;
                }
                kiteConnectWrapperTemp.GetFirstTwoTick(TradingSymbol, FirstTickTime, SecondTickTime, out firstCandleHigh, out firstCandleLow, out secondCandleHigh, out secondCandleLow);
            }
            else if (isSamcoBroker)
            { }
            else if(isCompositeBroker)
            {
                CompositeConnectWrapper compositeConnectWrapperTemp;// = new CompositeConnectWrapper();
                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "GetFirstTwoTick: return false");
                    return;
                }
                compositeConnectWrapperTemp.GetFirstTwoTick(TradingSymbol, FirstTickTime, SecondTickTime, out firstCandleHigh, out firstCandleLow, out secondCandleHigh, out secondCandleLow);
            }
        }

        public void GetCurrentHighLow(string userID,string  TradingSymbol,string StartTime,string EndTime, int Interval, out double dHigh, out double dLow)
        {
            dHigh = dLow = 0;
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp = new KiteConnectWrapper();
                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "GetCurrentHighLow: return false");
                    return;
                }
                kiteConnectWrapperTemp.GetCurrentHighLow(TradingSymbol,StartTime,EndTime,Interval, out dHigh, out dLow);
            }
            if (isSamcoBroker)
            { }
            if(isCompositeBroker)
            {
                CompositeConnectWrapper compositeConnectWrapperTemp;// = new CompositeConnectWrapper();
                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "GetCurrentHighLow: return false");
                    return;
                }
                compositeConnectWrapperTemp.GetCurrentHighLow(TradingSymbol, StartTime, EndTime, Interval, out dHigh, out dLow);
            }
        }

        public void GetTwoTickFromHistoricalData(string userID,string TradingSymbol, string FirstTickTime, string SecondTickTime, int interval, out double firstCandleHigh, out double firstCandleLow, out double secondCandleHigh, out double secondCandleLow)
        {
            firstCandleHigh = firstCandleLow = secondCandleHigh = secondCandleLow = 0;
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp = new KiteConnectWrapper();
                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "GetTwoTickFromHistoricalData : return false");
                    return;
                }
                kiteConnectWrapperTemp.GetTwoTickFromHistoricalData(TradingSymbol, FirstTickTime, SecondTickTime, interval,out firstCandleHigh, out firstCandleLow,out secondCandleHigh,out secondCandleLow);
            }
             else if(isCompositeBroker)
            {
                CompositeConnectWrapper compositeConnectWrapperTemp = null;
                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                }
                else
                {
                    //return "";
                }

                compositeConnectWrapperTemp.GetTwoTickFromHistoricalData(TradingSymbol, FirstTickTime, SecondTickTime, interval, out firstCandleHigh, out firstCandleLow, out secondCandleHigh, out secondCandleLow);
            }
        }

        public List<string> GetHighLowClose(string userID, string TradingSymbol,int interval,int period,string startTime,string endTime)
        {
            List<string> list = new List<string>();
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp = new KiteConnectWrapper();
                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "GetHighLowClose: return false");
                    return list;
                }
                list =  kiteConnectWrapperTemp.GetHighLowClose(TradingSymbol, interval, period, startTime, endTime);
            }
            else if (isSamcoBroker)
            {
                //SamcoConnectWrapper samcoConnectWrapperTemp = new SamcoConnectWrapper();
                //if (samcoConnectObjects.ContainsKey(userID))
                //{
                //    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                //}
                //else
                //{
                //    WriteLog(TradingSymbol + " " + userID, "GetHighLowClose: return false");
                //    return list;
                //}
                //list = samcoConnectWrapperTemp.GetHighLowClose(TradingSymbol, interval, period, startTime, endTime);
            }
            else if (isCompositeBroker)
            {
                CompositeConnectWrapper compositeConnectWrapperTemp = new CompositeConnectWrapper();
                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "GetHighLowClose: return false");
                    return list;
                }
                list = compositeConnectWrapperTemp.GetHighLowClose(TradingSymbol, interval, period, startTime,endTime);
            }
            return list;
        }

        public bool ModifyStopOrderWithOrderId(string userID ,string TradingSymbol, string TransactionType, decimal TriggerPrice, string OrderId, int Quantity, string Exchange = Constants.EXCHANGE_NSE, string Product = Constants.PRODUCT_MIS)
        {
           bool isModified = false; 
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp = new KiteConnectWrapper();
                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "ModifyStopOrderWithOrderId: return false");
                    return isModified;
                }
                isModified = kiteConnectWrapperTemp.ModifyStopOrderWithOrderId(TradingSymbol,TransactionType,TriggerPrice,OrderId,Quantity,Exchange,Product);
            }
            else if (isSamcoBroker)
            { }
            else if(isCompositeBroker)
            {
                CompositeConnectWrapper compositeConnectWrapperTemp = new CompositeConnectWrapper();
                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "ModifyStopOrderWithOrderId: return false");
                    return isModified;
                }
                isModified = compositeConnectWrapperTemp.ModifyStopOrderWithOrderId(TradingSymbol, TransactionType, TriggerPrice, OrderId, Quantity, Exchange, Product);
            }
            return isModified;
        }

        //sanika::29-July-2021::Changed return type
        public bool CancelOrder(string userID,string orderId, string TradingSymbol, string ProductType = Constants.VARIETY_REGULAR)
        {
            bool isOrderCancelled = false;
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp = new KiteConnectWrapper();
                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "CancelOrder: return false");
                    return false;
                }
                kiteConnectWrapperTemp.CancelOrder(orderId, ProductType, TradingSymbol);
            }
            else if (isSamcoBroker)
            { }
            else if (isCompositeBroker)
            {
                CompositeConnectWrapper compositeConnectWrapperTemp = new CompositeConnectWrapper();
                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "CancelOrder: return false");
                    return false;
                }
                isOrderCancelled = compositeConnectWrapperTemp.CancelOrder(orderId, ProductType, TradingSymbol);
            }

            return isOrderCancelled;
        }

        public bool IsOverallAllProfitLossHit(string userID)
        {
            bool IsOverallAllProfitLossHit = false;

            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp = new KiteConnectWrapper();
                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {                    
                    return IsOverallAllProfitLossHit;
                }
                IsOverallAllProfitLossHit =  kiteConnectWrapperTemp.IsOverallAllProfitLossHit();
            }
            else if (isSamcoBroker)
            { }
            else if (isCompositeBroker)
            {
                CompositeConnectWrapper compositeConnectWrapperTemp = new CompositeConnectWrapper();
                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                }
                else
                {
                    return IsOverallAllProfitLossHit;
                }
                IsOverallAllProfitLossHit = compositeConnectWrapperTemp.IsOverallAllProfitLossHit();
            }
            return IsOverallAllProfitLossHit;
        }

        public double GetIndividualProfitLoss(string userID,string TradingSymbol, string Exchange)
        {
            double profitLoss = 0;

            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp = new KiteConnectWrapper();
                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    return profitLoss;
                }
                profitLoss = kiteConnectWrapperTemp.GetIndividualProfitLoss(TradingSymbol,Exchange);
            }
            else if (isSamcoBroker)
            { }
            else if (isCompositeBroker)
            {
                CompositeConnectWrapper compositeConnectWrapperTemp = new CompositeConnectWrapper();
                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                }
                else
                {
                    return profitLoss;
                }
                profitLoss = compositeConnectWrapperTemp.GetIndividualProfitLoss(TradingSymbol, Exchange);
            }
            return profitLoss;
        }

        public double GetPNL(string userID)
        {
            double profitLoss = 0;

            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp = new KiteConnectWrapper();
                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    return profitLoss;
                }
                profitLoss = kiteConnectWrapperTemp.GetTotalMTM();
            }
            else if (isSamcoBroker)
            { }
            else if (isCompositeBroker)
            {
                CompositeConnectWrapper compositeConnectWrapperTemp = new CompositeConnectWrapper();
                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                }
                else
                {
                    return profitLoss;
                }
                profitLoss = compositeConnectWrapperTemp.GetTotalMTM();
            }
            return profitLoss;
        }

        public double GetLastBarHigh(string userID,string TradingSymbol)
        {
            double High = 0;

            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp = new KiteConnectWrapper();
                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    return High;
                }
                string high = kiteConnectWrapperTemp.GetLastBarHigh(TradingSymbol);
                if (high != null)
                    High = Convert.ToDouble(high);
            }
            else if (isSamcoBroker)
            { }
            else if (isCompositeBroker)
            {
                CompositeConnectWrapper compositeConnectWrapperTemp = new CompositeConnectWrapper();
                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                }
                else
                {
                    return High;
                }
                string high = compositeConnectWrapperTemp.GetHighPrice(TradingSymbol);
                if(high != null)
                    High = Convert.ToDouble(high);
            }
            return High;
        }

        public double GetLastBarLow(string userID, string TradingSymbol)
        {
            double Low = 0;

            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp = new KiteConnectWrapper();
                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    return Low;
                }
                string low = kiteConnectWrapperTemp.GetLastBarLow(TradingSymbol);
                if (low != null)
                    Low = Convert.ToDouble(low);
            }
            else if (isSamcoBroker)
            { }
            else if (isCompositeBroker)
            {
                CompositeConnectWrapper compositeConnectWrapperTemp = new CompositeConnectWrapper();
                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                }
                else
                {
                    return Low;
                }
                string low = compositeConnectWrapperTemp.GetLowPrice(TradingSymbol);
                if (low != null)
                    Low = Convert.ToDouble(low);
            }
            return Low;
        }
        
        public void AddNotificationInQueue(string userID,string message)
        {
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp = new KiteConnectWrapper();
                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    return;
                }
                kiteConnectWrapperTemp.AddNotificationInQueue(message);
            }
            else if (isSamcoBroker)
            {
                SamcoConnectWrapper samcoConnectWrapperTemp = new SamcoConnectWrapper();
                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {                   
                    return;
                }
                samcoConnectWrapperTemp.AddNotificationInQueue(message);
            }
            else if (isCompositeBroker)
            {
                CompositeConnectWrapper compositeConnectWrapperTemp = new CompositeConnectWrapper();
                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                }
                else
                {
                    return;
                }
                compositeConnectWrapperTemp.AddNotificationInQueue(message);
            }
        }
        
        //sanika::26-Apr-2021::Added to modify sl order with order id
        public bool ModifySLOrderWithOrderId(string userID, string TradingSymbol, string TransactionType,decimal Price, decimal TriggerPrice, string OrderId, int Quantity, string Exchange = Constants.EXCHANGE_NSE, string Product = Constants.PRODUCT_MIS)
        {
            bool isModified = false;
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp = new KiteConnectWrapper();
                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "ModifySLOrderWithOrderId: return false");
                    return isModified;
                }
                isModified = kiteConnectWrapperTemp.ModifySLOrderWithOrderId(TradingSymbol, TransactionType, TriggerPrice, OrderId, Quantity, Exchange, Product);
            }
            else if (isSamcoBroker)
            { }
            else if (isCompositeBroker)
            {
                CompositeConnectWrapper compositeConnectWrapperTemp = new CompositeConnectWrapper();
                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "ModifySLOrderWithOrderId: return false");
                    return isModified;
                }
                isModified = compositeConnectWrapperTemp.ModifySLOrderWithOrderId(TradingSymbol, TransactionType,Price, TriggerPrice, OrderId, Quantity, Exchange, Product);
            }
            return isModified;
        }

        //sanika::26-Apr-2021::Added to get ask price
        public double GetAskPrice(string userID, string TradingSymbol)
        {
            double askPrice = 0;
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp = new KiteConnectWrapper();
                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "GetAskPrice: return false");
                    return askPrice;
                }
                string ask =  kiteConnectWrapperTemp.GetAskPrice(TradingSymbol);
                if (ask != null)
                    askPrice = Convert.ToDouble(ask);
            }
            else if (isSamcoBroker)
            { }
            else if (isCompositeBroker)
            {
                CompositeConnectWrapper compositeConnectWrapperTemp = new CompositeConnectWrapper();
                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "GetAskPrice: return false");
                    return askPrice;
                }
                string ask = compositeConnectWrapperTemp.GetAskPrice(TradingSymbol);
                if (ask != null)
                    askPrice = Convert.ToDouble(ask);               
            }
            return askPrice;
        }

        //sanika::26-Apr-2021::Added to get bid price
        public double GetBidPrice(string userID, string TradingSymbol)
        {
            double bidPrice = 0;
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp = new KiteConnectWrapper();
                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "GetBidPrice: return false");
                    return bidPrice;
                }
                string bid = kiteConnectWrapperTemp.GetBidPrice(TradingSymbol);
                if (bid != null)
                    bidPrice = Convert.ToDouble(bid);
            }
            else if (isSamcoBroker)
            { }
            else if (isCompositeBroker)
            {
                CompositeConnectWrapper compositeConnectWrapperTemp = new CompositeConnectWrapper();
                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "GetBidPrice: return false");
                    return bidPrice;
                }
                string bid = compositeConnectWrapperTemp.GetBidPrice(TradingSymbol);
                if (bid != null)
                    bidPrice = Convert.ToDouble(bid);
            }
            return bidPrice;
        }

        //IRDSPM::Pratiksha::26-04-2021::For getting symbol for symbol setting form
        public List<string> GetOptionsSymbols()
        {
            List<string> OptionSymbolListFromDB = new List<string>();

            if (isZerodhaBroker)
            {
                OptionSymbolListFromDB = kiteWrapperConnect.GetSymbolsKite;
                if (OptionSymbolListFromDB.Count == 0)
                {
                    kiteWrapperConnect.GetAllSymbolsFromDBdata();
                    OptionSymbolListFromDB = kiteWrapperConnect.GetSymbolsKite;
                }
            }
            else if (isSamcoBroker)
            {
                //OptionSymbolListFromDB = samcoWrapperConnect.GetSymbolsSamco;
            }
            else if (isCompositeBroker)
            {
                OptionSymbolListFromDB = CompositeConnectWrapper.m_OptionSymbols;
                //IRDSPM::Pratiksha::07-06-2021::For refreshing the symbols list
                if (OptionSymbolListFromDB.Count == 0)
                {
                    compositeWrapperConnect.getSymbolsFromOptions();
                    OptionSymbolListFromDB = CompositeConnectWrapper.m_OptionSymbols;
                }
            }
            return OptionSymbolListFromDB;
        }

        public List<string> fetchExpiryDtForOptionSymbol(string symbol, string table)
        {
            List<string> SymbolListFromDB = new List<string>();

            if (isZerodhaBroker)
            {
                SymbolListFromDB = kiteWrapperConnect.GetSymbolexpiryDateFromDBdata(symbol, table);
            }
            else if (isSamcoBroker)
            {
               // SymbolListFromDB = samcoWrapperConnect.GetSymbolsSamco;
            }
            else if (isCompositeBroker)
            {
                SymbolListFromDB = compositeWrapperConnect.GetSymbolexpiryDateFromDBdata(symbol, table);
            }
            return SymbolListFromDB;
        }


        public string fetchLotsizeForOptionSymbol(string symbol, string table, string expiryDt)
        {
            string lotsize = null;
            if (isZerodhaBroker)
            {
                lotsize = kiteWrapperConnect.GetLotsizeexpiryDateFromDBdata(symbol, table, expiryDt);
            }
            else if (isCompositeBroker)
            {
                lotsize = compositeWrapperConnect.GetLotsizeexpiryDateFromDBdata(symbol, table, expiryDt);
            }
            return lotsize;
        }

        public double GetLimitPricebyOrderID(string userID, string TradingSymbol, string Exchange, string orderId)
        {
            double stopPrice;
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp;

                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "GetLimitPricebyOrderID: return 0");
                    return 0;
                }
                stopPrice = kiteConnectWrapperTemp.GetLimitPricebyOrderID(TradingSymbol, Exchange, orderId);
            }
            else if (isSamcoBroker)
            {
                SamcoConnectWrapper samcoConnectWrapperTemp;

                if (samcoConnectObjects.ContainsKey(userID))
                {
                    samcoConnectObjects.TryGetValue(userID, out samcoConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "GetLimitPricebyOrderID: return 0");
                    return 0;
                }
                stopPrice = samcoConnectWrapperTemp.GetLimitPricebyOrderID(TradingSymbol, Exchange, orderId);
            }
            else
            {
                CompositeConnectWrapper compositeConnectWrapperTemp;

                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                }
                else
                {
                    WriteLog(TradingSymbol + " " + userID, "GetLimitPricebyOrderID: return 0");
                    return 0;
                }
                stopPrice = compositeConnectWrapperTemp.GetLimitPricebyOrderID(TradingSymbol, Exchange, orderId);
            }
            return stopPrice;
        }
        public double GetMargin(string userID)
        {
            double margin = 0;
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp = new KiteConnectWrapper();
                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {                    
                    dynamic res = kiteConnectWrapperTemp.GetMarginFromStructure();
                    if (res != null && res.Count != 0)
                        margin = res.Available;
                    return margin;
                }
                
            }
            else if (isSamcoBroker)
            { }
            else if (isCompositeBroker)
            {
                CompositeConnectWrapper compositeConnectWrapperTemp = new CompositeConnectWrapper();
                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                }
                else
                {                    
                    return margin;
                }
                dynamic res = compositeConnectWrapperTemp.GetMarginFromStructure();
                //sanika::20-July-2021::added count condition 
                if(res != null && res.Count != 0)
                    margin =Convert.ToDouble( res["Available"]);
            }
            return margin;
        }

        //sanika::9-Jun-2021::Added to clear master list
        public void ClearMasterList(string userID)
        {           
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp = new KiteConnectWrapper();
                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    return;
                }
                kiteConnectWrapperTemp.ClearMasterList();
            }
            else if (isSamcoBroker)
            { }
            else if (isCompositeBroker)
            {
                CompositeConnectWrapper compositeConnectWrapperTemp = new CompositeConnectWrapper();
                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                }
                else
                {
                    return;
                }

                compositeConnectWrapperTemp.ClearMasterList();
            }
            return;
        }

        //sanika::9-July-2021::Added add gui notification
        public void AddGUINotification(string userID,string Message)
        {
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp = new KiteConnectWrapper();
                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    return;
                }
                kiteConnectWrapperTemp.AddNotificationInQueueToast(Message);
            }
            else if (isSamcoBroker)
            { }
            else if (isCompositeBroker)
            {
                CompositeConnectWrapper compositeConnectWrapperTemp = new CompositeConnectWrapper();
                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                }
                else
                {
                    return;
                }

                compositeConnectWrapperTemp.AddNotificationInQueueToast(Message);
            }
            return;
        }
        
        //IRDSPM::Pratiksha::09-07-2021::For toastNotification
        public void AddNotificationInQueueForToast(string userID, string message)
        {
            if (isZerodhaBroker)
            {
                KiteConnectWrapper kiteConnectWrapperTemp = new KiteConnectWrapper();
                if (kiteConnectObjects.ContainsKey(userID))
                {
                    kiteConnectObjects.TryGetValue(userID, out kiteConnectWrapperTemp);
                }
                else
                {
                    return;
                }
                kiteConnectWrapperTemp.AddNotificationInQueueToast(message);
            }
            else if (isSamcoBroker)
            {
               
            }
            else if (isCompositeBroker)
            {
                CompositeConnectWrapper compositeConnectWrapperTemp = new CompositeConnectWrapper();
                if (compositeConnectObjects.ContainsKey(userID))
                {
                    compositeConnectObjects.TryGetValue(userID, out compositeConnectWrapperTemp);
                }
                else
                {
                    return;
                }
                compositeConnectWrapperTemp.AddNotificationInQueueToast(message);
            }
        }
        public List<string> GetInstrumentsSymbols(string Exchange)
        {
            List<string> InstrumentsSymbolListFromDB = new List<string>();
            try
            {
                if (isZerodhaBroker)
                {
                    if (kiteWrapperConnect != null)
                    {
                        //Getting only options symbol name
                        if (Exchange == Constants.EXCHANGE_NSE)
                        {
                            InstrumentsSymbolListFromDB = kiteWrapperConnect.GetInstrumentSymbolsKite;
                        }
                        else if (Exchange == Constants.EXCHANGE_NFO)
                        {
                            InstrumentsSymbolListFromDB = kiteWrapperConnect.GetFutureSymbolsKite;
                        }
                        else if (Exchange == Constants.EXCHANGE_NFO_OPT)
                        {
                            InstrumentsSymbolListFromDB = kiteWrapperConnect.GetOtionsSymbolsKite;
                        }
                    }
                }
                //else if (isSamcoBroker)
                //{
                //    //OptionSymbolListFromDB = samcoWrapperConnect.GetSymbolsSamco;
                //}
                else if (isCompositeBroker)//sanika::28-Jul-2021::changed the variable name
                {
                    InstrumentsSymbolListFromDB = CompositeConnectWrapper.m_OptionSymbols;
                    //IRDSPM::Pratiksha::07-06-2021::For refreshing the symbols list
                    if (InstrumentsSymbolListFromDB.Count == 0)
                    {
                        compositeWrapperConnect.getSymbolsFromOptions();
                        InstrumentsSymbolListFromDB = CompositeConnectWrapper.m_OptionSymbols;
                    }
                }
            }
            catch (Exception e)
            {
                WriteLog("StartGUI", "GetInstrumentsSymbols: Exception Error Message = " + e.Message);
            }
            return InstrumentsSymbolListFromDB;
        }
    }
}
