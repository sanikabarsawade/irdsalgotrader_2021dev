﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRDSAlgoOMS
{
    public enum AFLSignalTypes
    {
        Buy,
        Sell
    }

    public enum MessageType
    {
        Informational,
        Error,
        Exception
    }

    public enum BrokerNames
    {
        ZERODHABROKER,
        SAMCOBROKER,
        COMPOSITEBROKER
    }

    public enum TradeState
    {
        NONE,
        CREATED, // # Trade created but not yet order placed, might have not triggered
        ACTIVE, //# order placed and trade is active
        COMPLETED,//# completed when exits due to SL/Target/SquareOff
        CANCELLED, //# cancelled/rejected comes under this state only
        DISABLED, //# disable trade if not triggered within the time limits or for any other reason
    }

    //public enum Segments
    // {
    //     Equity,
    //     Commodity,
    //     Futures,
    //     Currency
    // }

    // public enum TransactionTypes
    // {
    //     Buy,
    //     Sell
    // }

    // public enum OrderTypes
    // {
    //     MARKET,
    //     LIMIT,
    //     SL,
    //     SLM
    // }

    // public enum ProductTypes
    // {
    //     MIS,
    //     CNC,
    //     NRML
    // }

    // public enum VarietyTypes
    // {
    //     Regular,
    //     BO,
    //     CO,
    //     AMO
    // }

    // public enum TickerModes
    // {
    //     Full,
    //     Quote,
    //     LTP
    // }

    // public enum PositionTypes
    // {
    //     Day,
    //     Overnight
    // }

    // public enum ValidityTypes
    // {
    //     DAY,
    //     IOC,
    //     AMO
    // }

    // public enum Exchanges
    // {
    //     NSE,
    //     BSE,
    //     NFO,
    //     CDS,
    //     MCX
    // }

    // public enum CandleIntervals
    // {
    //     Minute,
    //     ThreeMinute,
    //     FiveMinute,
    //     TenMinute,
    //     FifteenMinute,
    //     ThirtyMinute,
    //     SixtyMinute,
    //     Day
    // }

    // public enum SIPFrequency
    // {
    //     Weekly,
    //     Monthly,
    //     Quarterly
    // }

    // public enum SIPStatus
    // {
    //     Active,
    //     Paused
    // }

    // public class Constants
    // {
    //     static Dictionary<string, List<string>> values = new Dictionary<string, List<string>>
    //     {
    //         [typeof(Segments).Name] = new List<string> { "equity", "commodity", "futures", "currency" },
    //         [typeof(TransactionTypes).Name] = new List<string> { "BUY", "SELL" },
    //         [typeof(OrderTypes).Name] = new List<string> { "MARKET", "LIMIT", "SL", "SL-M" },
    //         [typeof(ProductTypes).Name] = new List<string> { "MIS", "CNC", "NRML" },
    //         [typeof(VarietyTypes).Name] = new List<string> { "regular", "bo", "co", "amo" },
    //         [typeof(TickerModes).Name] = new List<string> { "full", "quote", "ltp" },
    //         [typeof(PositionTypes).Name] = new List<string> { "day", "overnight" },
    //         [typeof(ValidityTypes).Name] = new List<string> { "DAY", "IOC", "AMO" },
    //         [typeof(Exchanges).Name] = new List<string> { "NSE", "BSE", "NFO", "CDS", "MCX"},
    //         [typeof(CandleIntervals).Name] = new List<string> { "minute", "3minute", "5minute", "10minute", "15minute", "30minute", "60minute", "day" },
    //         [typeof(SIPFrequency).Name] = new List<string> { "weekly", "monthly", "quarterly" },
    //         [typeof(SIPStatus).Name] = new List<string> { "active", "paused" },
    //     };

    //     public static T ToEnum<T>(string value)
    //     {
    //         return (T)(object) values[typeof(T).Name].IndexOf(value);
    //     }

    //     public static string ToValue<T>(T enumValue)
    //     {
    //         if(enumValue == null)
    //             return "";
    //         var index = Enum.GetNames(typeof(T)).ToList().IndexOf(enumValue.ToString());
    //         return values[typeof(T).Name][index];
    //     }
    //}


    //composite market data
    public enum MarketDataPorts
    {
        /// <summary>
        /// Level I vents
        /// </summary>
        touchlineEvent = 1501,
        /// <summary>
        /// Level II events
        /// </summary>
        marketDepthEvent = 1502,
        /// <summary>
        /// Top gainers and loser event
        /// </summary>
        topGainerLosserEvent = 1503,
        /// <summary>
        /// Index event
        /// </summary>
        indexDataEvent = 1504,
        /// <summary>
        /// Candle event
        /// </summary>
        candleDataEvent = 1505,
        /// <summary>
        /// General message broadcast event
        /// </summary>
        generalMessageBroadcastEvent = 1506,
        /// <summary>
        /// Exchange trading status event
        /// </summary>
        exchangeTradingStatusEvent = 1507,
        /// <summary>
        /// Open interest event
        /// </summary>
        openInterestEvent = 1510,
        /// <summary>
        /// Instrument subscription info
        /// </summary>
        instrumentSubscriptionInfo = 5505,
        /// <summary>
        /// Level III 30 depth picture
        /// </summary>
        marketDepthEvent30 = 5014,
        /// <summary>
        /// Level III 100 depth picture
        /// </summary>
        marketDepthEvent100 = 5018
    }

    //composite market data   
    public enum PublishFormat
    {
        JSON, Binary
    }

    public enum BroadcastMode
    {
        Full, Partial
    }

    //composite
    public enum ExchangeSegment
    {
        /// <summary>
        /// NSE cash
        /// </summary>
        NSECM = 1,
        /// <summary>
        /// NSE FnO
        /// </summary>
        NSEFO = 2,
        /// <summary>
        /// NSE CDS
        /// </summary>
        NSECD = 3,
        /// <summary>
        /// BSE Cash
        /// </summary>
        BSECM = 11,
        /// <summary>
        /// BSE FnO
        /// </summary>
        BSEFO = 12,
        /// <summary>
        /// BSE CDS
        /// </summary>
        BSECD = 13,
        /// <summary>
        /// NCDEX
        /// </summary>
        NCDEX = 21,
        /// <summary>
        /// MSEI Cash
        /// </summary>
        MSEICM = 41,
        /// <summary>
        /// MSEI FnO
        /// </summary>
        MSEIFO = 42,
        /// <summary>
        /// MSEI CDS
        /// </summary>
        MSEICD = 43,
        /// <summary>
        /// MCX FnO
        /// </summary>
        MCXFO = 51
    }
}
