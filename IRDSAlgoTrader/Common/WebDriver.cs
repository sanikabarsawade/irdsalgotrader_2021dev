﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Chrome;
using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium.Remote;
using System.Net;
using System.Net.NetworkInformation;
using System.IO;
using System.Web;
using System.Threading;
using IRDSAlgoOMS.GUI;
using System.Windows;
using System.Windows.Forms;
using Microsoft.Win32;
using System.Diagnostics;
using System.IO.Compression;

namespace IRDSAlgoOMS
{
    public class WebDriver
    {
        public static Logger loggerClient;

        public string currentUrl = "";
        string username = "";
        string password = "";
        string pin = "";
        public string token = "";
        string m_Title = "";
        static readonly object _CreateWindows = new object();
        manualLoginWindowsForm manualLoginWindow = null;

        ManuallyLogin ManuallyLogin = null;
      
        bool manualLogin = false;
        string m_ExceptionErrorMessage = "";
        Kite m_Kite = null;
        Logger logger = null;
        public WebDriver(bool bmanualLogin,string susername, string spassword,string spin, Logger clogger)
        {
            manualLogin = bmanualLogin;
            username = susername;
            password = spassword;
            pin = spin;
            logger = clogger;
        }

        public void getChromeDriver(string login, string apiKey,string title)
        {
            try
            {
                int counter = 0;
                m_Title = title;
                logger.LogMessage("getChromeDriver : manualLogin flag value = " + manualLogin,MessageType.Informational);
                if (!manualLogin)
                {
                    logger.LogMessage("getChromeDriver : Doing login with selenium(automatic)", MessageType.Informational);
                    m_Kite = new Kite();

                    //IRDS::04-Sep-2020::Jyoti::added for auto update chrome driver
                    IWebDriver driver = getDriver();
                    if (driver != null)
                    {
                        // implicitly wait for web browser to open
                        driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
                        driver.Url = m_Kite.GetLoginURL(login, apiKey);
                        logger.LogMessage("getChromeDriver : driver.Url = "+ driver.Url, MessageType.Informational);
                        // get username and password to login 

                        driver.FindElement(By.XPath("(//input[@type='text'])")).SendKeys(username);
                        Thread.Sleep(1000);
                        logger.LogMessage("getChromeDriver : entered user name", MessageType.Informational);
                        driver.FindElement(By.XPath("(//input[@type='password'])")).SendKeys(password);
                        Thread.Sleep(1000);
                        logger.LogMessage("getChromeDriver : entered password", MessageType.Informational);
                        driver.FindElement(By.CssSelector("button[type='submit']")).Click();
                        logger.LogMessage("getChromeDriver : Clicked on submit", MessageType.Informational);
                        Thread.Sleep(3000);     //IRDS::Bhagyashri::7-June-2018::incremented ms to resolve trouble login issues

                        /*string ans1 = "None";
                        string ans2 = "None";

                        driver.FindElement(By.XPath("(//input[@type='password'])[1]")).SendKeys(ans1);
                        Thread.Sleep(1000);

                        driver.FindElement(By.XPath("(//input[@type='password'])[2]")).SendKeys(ans2);
                        Thread.Sleep(1000);

                        driver.FindElement(By.XPath("//button[@type='submit']")).Click();

                        Thread.Sleep(3000);*/

                        driver.FindElement(By.XPath("(//input[@type='password'])")).SendKeys(pin);
                        Thread.Sleep(1000);
                        logger.LogMessage("getChromeDriver : entered pin", MessageType.Informational);
                        driver.FindElement(By.CssSelector("button[type='submit']")).Click();
                        Thread.Sleep(1000);
                        logger.LogMessage("getChromeDriver : Clicked on login", MessageType.Informational);                        
                        currentUrl = driver.Url;
                        //sanika::02-Aug-2021::added code for request token error 
                        while ((!currentUrl.Contains("request_token")) && counter < 15)
                        {
                            counter++;
                            Thread.Sleep(1000);
                            currentUrl = driver.Url;
                        }
                        logger.LogMessage("getChromeDriver : closing chrome", MessageType.Informational);
                        logger.LogMessage("getChromeDriver : Got token url as "+ currentUrl, MessageType.Informational);
                        driver.Close();
                        driver.Quit();
                    }
                    else
                    {
                        if (apiKey.Contains("XXXXXX") || username.Contains("XXXXXX") || password.Contains("XXXXXX") || pin.Contains("XXXXXX"))
                        {
                            logger.LogMessage("getChromeDriver : credentials are not valid", MessageType.Informational);
                        }
                        else
                        {
                            logger.LogMessage("getChromeDriver : driver is null going to ManualLoginProcess", MessageType.Informational);
                            ManualLoginProcess(login, apiKey, m_Title);
                        }
                        
                    }
                }
                else
                {
                    if (apiKey.Contains("XXXXXX") || username.Contains("XXXXXX") || password.Contains("XXXXXX") || pin.Contains("XXXXXX"))
                    {
                        logger.LogMessage("getChromeDriver : credentials are not valid", MessageType.Informational);
                    }
                    else
                    {
                        logger.LogMessage("getChromeDriver : manualLogin is true going to ManualLoginProcess", MessageType.Informational);
                        ManualLoginProcess(login, apiKey, m_Title);
                    }
                }
            }
            catch(Exception e)
            {
                m_ExceptionErrorMessage = e.Message;
                logger.LogMessage("getChromeDriver : Exception Error Message = "+e.Message, MessageType.Exception);
            }
            finally //Sanika::26-Aug-2020::Added finally block for chrome version changing issue (manual login form will be open)
            {
                if(m_ExceptionErrorMessage.Contains("version") && m_ExceptionErrorMessage.Contains("ChromeDriver")&& m_ExceptionErrorMessage.Contains("supports") && m_ExceptionErrorMessage.Contains("Chrome") && m_ExceptionErrorMessage.Contains("Driver"))
                {

                    System.Windows.Forms.MessageBox.Show(new Form { TopMost = true }, "Chrome version is updated Please download latest version!\n\r For Now, Please do manual login");
                    logger.LogMessage("getChromeDriver : Chrome version is updated going to ManualLoginProcess", MessageType.Informational);
                    ManualLoginProcess(login, apiKey, m_Title);
                }
            }
        }

        //IRDS::04-Sep-2020::Jyoti::added for auto update chrome driver
        public IWebDriver getDriver()
        {            
            IWebDriver driver = null;
            try
            {
                driver = new ChromeDriver();
                logger.LogMessage("getDriver :returning driver", MessageType.Informational);
                return driver;
            }
            catch (Exception e)
            {
                if (e.Message.Contains("version of ChromeDriver"))
                {
                    logger.LogMessage("getDriver : Downloading latest updated version of chrome", MessageType.Informational);
                    try
                    {
                        string version = "";
                        //Getting current chrome version
                        //HKEY_CURRENT_USER
                        object path = Registry.GetValue(@"HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\App Paths\chrome.exe", "", null);
                        if (path != null)
                            version = FileVersionInfo.GetVersionInfo(path.ToString()).FileVersion;

                        //Request for getting available chrome driver released version
                        string url = "https://chromedriver.storage.googleapis.com/LATEST_RELEASE_";
                        string file_name = "chromedriver_win32.zip";
                        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url + version.Split('.')[0]);
                        request.AllowAutoRedirect = true;
                        request.Method = "GET";
                        string response = new StreamReader(request.GetResponse().GetResponseStream()).ReadToEnd();

                        //Request for getting available chrome driver
                        url = "https://chromedriver.storage.googleapis.com/" + response + "/" + file_name;
                        request = (HttpWebRequest)WebRequest.Create(url);
                        request.Method = "GET";
                        using (var s = request.GetResponse().GetResponseStream())
                        using (var w = File.OpenWrite(@".\" + file_name))
                            s.CopyTo(w);
                        logger.LogMessage("getDriver :Downloaded latest version of chromedriver", MessageType.Informational);
                        //Killing already running processes for chromedriver
                        Process[] localByName = Process.GetProcessesByName("chromedriver");
                        foreach (Process p in localByName)
                        {
                            p.Kill();
                            Thread.Sleep(10);
                        }
                        logger.LogMessage("getDriver :Killed running old chromedriver process", MessageType.Informational);
                        //Deleting old chromedriver version
                        File.Delete(@".\chromedriver.exe");

                        //Extracting downloaded zip file
                        ZipFile.ExtractToDirectory(@".\" + file_name, @".\");
                        logger.LogMessage("getDriver : Downloaded!!", MessageType.Informational);
                        driver = new ChromeDriver();
                        logger.LogMessage("getDriver :returning downloaded driver", MessageType.Informational);
                        return driver;
                    }
                    catch (Exception ex)
                    {
                        return driver;
                    }
                }
                else
                    return driver;
            }
        }

        public void ManualLoginProcess(string login,string apiKey, string m_Title)
        {
            logger.LogMessage("ManualLoginProcess :Going to do manual login", MessageType.Informational);
            if (m_Kite == null)
            {
                m_Kite = new Kite();
            }
            string url = m_Kite.GetLoginURL(login, apiKey);
            logger.LogMessage("ManualLoginProcess :got login url as "+ url, MessageType.Informational);
            lock (_CreateWindows)
            {
                if (manualLoginWindow == null)
                //if (ManuallyLogin == null)
                {
                    //ManuallyLogin = new ManuallyLogin(m_Kite, url);
                    //ManuallyLogin.ShowDialog();
                    //token = ManuallyLogin.Token;
                    //Thread.Sleep(1000);
                    manualLoginWindow = new manualLoginWindowsForm(m_Kite, url,m_Title);
                    manualLoginWindow.ShowDialog();
                    token = manualLoginWindow.Token;
                    if(token!="")
                        logger.LogMessage("ManualLoginProcess :Got token", MessageType.Informational);
                    else
                        logger.LogMessage("ManualLoginProcess :Not got token from manual process something went wrong", MessageType.Informational);
                }
            }
        }
    }
}
