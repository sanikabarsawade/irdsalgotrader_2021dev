﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRDSAlgoOMS.Common
{
    class Notification
    {
        ConcurrentQueue<string> m_queueNotification = new ConcurrentQueue<string>();
        public string GetMessageforNotification()
        {
            string message = "NA";
            m_queueNotification.TryDequeue(out message);
            return message;
        }

        public void AddNotificationInQueue(String message)
        {
            m_queueNotification.Enqueue(message);
        }
    }
}
