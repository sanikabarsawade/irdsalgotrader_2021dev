﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRDSAlgoOMS
{
    public abstract class LogBase
    {
        public abstract void LogMessage(string message, MessageType msgType);
    }

    public class SymbolLogs : LogBase
    {
        public  string symbol;
        public  string currentSymbol;
        public string logFilePath = "";
        public string directoryName = "";

        private  SymbolLogs instanceSymbol = null;
        private  readonly object padlock = new object();

        public  AlgoOMS signal = new AlgoOMS();

        public SymbolLogs()
        {
       
        }

        public override void LogMessage(string message, MessageType msgType)
        {
            using (StreamWriter streamWriter = new StreamWriter(logFilePath, true))
            {
                //IRDS::Jyoti::04-Aug-2021::Added for writing memory in logs
                
                streamWriter.WriteLine(GetFormattedString(message, msgType));
                //streamWriter.Close();
            }
        }
        //04-August-2021: sandip:memory write.
        public void LogMemoryMessage(string message, MessageType msgType, bool BrokerAPIDebugMemoryLog)
        {
            using (StreamWriter streamWriter = new StreamWriter(logFilePath, true))
            {
                //IRDS::Jyoti::04-Aug-2021::Added for writing memory in logs

                if (BrokerAPIDebugMemoryLog)
                {
                    Process myProcesses = Process.GetCurrentProcess();
                    string strMemory = Convert.ToString(myProcesses.PrivateMemorySize64 / (1024 * 1024));
                    message = message + ", Memory:- " + strMemory.Trim();
                    myProcesses.Dispose();
                    myProcesses = null;
                }
                streamWriter.WriteLine(GetFormattedString(message, msgType));
                //streamWriter.Close();
            }
        }

        public void createFile(string symbol)
        {
            string todaysDate = string.Format("{0:dd-MM-yyyy}", DateTime.Now);
            string todaysDateTime = string.Format("{0:dd-MM-yyyy:hh-mm-ss-tt}", DateTime.Now);
            string fileName = symbol + ".txt";

            var path = Directory.GetCurrentDirectory();
            directoryName = path + "\\" + "Logs" + "\\" + todaysDate;

            if (!Directory.Exists(directoryName))
                Directory.CreateDirectory(directoryName);

            logFilePath = directoryName + "\\" + fileName;
        }

        public  SymbolLogs InstanceSymbolLogs
        {
            get
            {
                if (instanceSymbol == null)
                {
                    lock (padlock)
                    {
                        if (instanceSymbol == null)
                        {
                            instanceSymbol = new SymbolLogs();
                        }
                    }
                }
                return instanceSymbol;
            }
        }

        public string GetFormattedString(string message, MessageType msgType)
        {
            string msgTypeString = "";
            switch (msgType)
            {
                case MessageType.Informational:
                    {
                        msgTypeString = "Informational";
                    }
                    break;

                case MessageType.Error:
                    {
                        msgTypeString = "Error";
                    }
                    break;

                case MessageType.Exception:
                    {
                        msgTypeString = "Exception";
                    }
                    break;
            }

            var formattetString = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff") + Constants.LogFileSeperator + msgTypeString + Constants.LogFileSeperator + message;
            return formattetString;
        }

        ~SymbolLogs()
        {
            //sw.Close();
        }
    }
}
