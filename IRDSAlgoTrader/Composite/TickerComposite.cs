﻿//using Quobject.SocketIoClientDotNet.Client;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
//using System.Net.Sockets;
using System.Net.WebSockets;
using System.Text;
using System.Threading.Tasks;
using static IRDSAlgoOMS.StructuresComposite;
using Quobject.SocketIoClientDotNet.Client;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace IRDSAlgoOMS
{
    public class TickerComposite
    {
        // If set to true will print extra debug information
        private bool _debug = false;

        // Root domain for ticker. Can be changed with Root parameter in the constructor.
        private string _accessToken;
        private string userID;
        private string _socketUrl = "";
        private bool _isReconnect = false;
        private int _interval = 20;
        private int _retries = 50;
        private int _retryCount = 0;

        // A watchdog timer for monitoring the connection of ticker.
        System.Timers.Timer _timer;
        int _timerTick = 20;
        Tick tick;
        string filePath = "";
        // Instance of WebSocket class that wraps .Net version
        private WebSocket _ws;

        //Default ticker
        public TickerComposite()
        {
            //Console..WriteLine("\nTicker Initialized");
        }

        // Dictionary that keeps instrument_token -> mode data
        private Dictionary<string, string> _subscribedTokens;

        // Delegates for callbacks

        /// <summary>
        /// Delegate for OnConnect event
        /// </summary>
        public delegate void OnConnectHandler();

        /// <summary>
        /// Delegate for OnClose event
        /// </summary>
        public delegate void OnCloseHandler();

        /// <summary>
        /// Delegate for OnTick event
        /// </summary>
        /// <param name="TickData">Tick data</param>
        public delegate void OnTickHandler(Tick TickData);

        /// <summary>
        /// Delegate for OnOrderUpdate event
        /// </summary>
        /// <param name="OrderData">Order data</param>
        public delegate void OnOrderUpdateHandler(OrderComposite OrderData);

        /// <summary>
        /// Delegate for OnError event
        /// </summary>
        /// <param name="Message">Error message</param>
        public delegate void OnErrorHandler(string Message);

        /// <summary>
        /// Delegate for OnReconnect event
        /// </summary>
        public delegate void OnReconnectHandler();

        /// <summary>
        /// Delegate for OnNoReconnect event
        /// </summary>
        public delegate void OnNoReconnectHandler();

        // Events that can be subscribed
        /// <summary>
        /// Event triggered when ticker is connected
        /// </summary>
        public event OnConnectHandler OnConnect;

        /// <summary>
        /// Event triggered when ticker is disconnected
        /// </summary>
        public event OnCloseHandler OnClose;

        /// <summary>
        /// Event triggered when ticker receives a tick
        /// </summary>
        public event OnTickHandler OnTick;

        /// <summary>
        /// Event triggered when ticker receives an order update
        /// </summary>
        public event OnOrderUpdateHandler OnOrderUpdate;

        /// <summary>
        /// Event triggered when ticker encounters an error
        /// </summary>
        public event OnErrorHandler OnError;

        /// <summary>
        /// Event triggered when ticker is reconnected
        /// </summary>
        public event OnReconnectHandler OnReconnect;

        /// <summary>
        /// Event triggered when ticker is not reconnecting after failure
        /// </summary>
        public event OnNoReconnectHandler OnNoReconnect;

        protected HttpClient HttpClient { get; set; } = null;

        protected Socket Socket { get; set; } = null;

        /// <summary>
        /// Initialize websocket client instance.
        /// </summary>
        /// <param name="APIKey">API key issued to you</param>
        /// <param name="UserID">Zerodha client id of the authenticated user</param>
        /// <param name="AccessToken">Token obtained after the login flow in 
        /// exchange for the `request_token`.Pre-login, this will default to None,
        /// but once you have obtained it, you should
        /// persist it in a database or session to pass
        /// to the Kite Connect class initialisation for subsequent requests.</param>
        /// <param name="Root">Websocket API end point root. Unless you explicitly 
        /// want to send API requests to a non-default endpoint, this can be ignored.</param>
        /// <param name="Reconnect">Enables WebSocket autreconnect in case of network failure/disconnection.</param>
        /// <param name="ReconnectInterval">Interval (in seconds) between auto reconnection attemptes. Defaults to 5 seconds.</param>
        /// <param name="ReconnectTries">Maximum number reconnection attempts. Defaults to 50 attempts.</param>
        public TickerComposite(string userId, string token, string path)
        //string APIKey, string AccessToken, string Root = null, bool Reconnect = false, int ReconnectInterval = 5, int ReconnectTries = 50, bool Debug = false)
        {
            if (Uri.TryCreate(path, UriKind.Absolute, out Uri result))
            {
                string authority = result.GetLeftPart(UriPartial.Authority);

                this.HttpClient = new HttpClient();
                this.HttpClient.BaseAddress = new Uri(authority);
                this.HttpClient.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            }
            _accessToken = token;
            userID = userId;
            _subscribedTokens = new Dictionary<string, string>();
            tick = new Tick();
            Trace.WriteLine("Sandip creating timer");
            // initializing  watchdog timer
            _timer = new System.Timers.Timer();
            _timer.Elapsed += _onTimerTick;
            _timer.Interval = 60000; // checks connection every second
            //logger.LogMessage("tickercomposite : started new System.Timers.Timer()", MessageType.Informational);

            //_ws.Connect();
            //_ws.Connect(new Dictionary<string, string>() { ["token"] = _accessToken, ["userID"] = "KAP70", ["source"] = "WEB", ["publishFormat"] = "JSON", ["broadcastMode"] = "Full" });
        }

        private void _onError(string Message)
        {
            // pipe the error message from ticker to the events
            OnError?.Invoke(Message);
        }

        private void _onClose()
        {
            // stop the timer while normally closing the connection
            _timer.Stop();
            OnClose?.Invoke();
        }

        /// <summary>
        /// Reads 2 byte short int from byte stream
        /// </summary>
        private ushort ReadShort(byte[] b, ref int offset)
        {
            ushort data = (ushort)(b[offset + 1] + (b[offset] << 8));
            offset += 2;
            return data;
        }

        /// <summary>
        /// Reads 4 byte int32 from byte stream
        /// </summary>
        private UInt32 ReadInt(byte[] b, ref int offset)
        {
            UInt32 data = (UInt32)BitConverter.ToUInt32(new byte[] { b[offset + 3], b[offset + 2], b[offset + 1], b[offset + 0] }, 0);
            offset += 4;
            return data;
        }

        /// <summary>
        /// Reads an ltp mode tick from raw binary data
        /// </summary>
        private Tick ReadLTP(byte[] b, ref int offset)
        {
            Tick tick = new Tick();
            tick.Mode = Constants.MODE_LTP;
            tick.InstrumentToken = ReadInt(b, ref offset);

            decimal divisor = (tick.InstrumentToken & 0xff) == 3 ? 10000000.0m : 100.0m;

            tick.Tradable = (tick.InstrumentToken & 0xff) != 9;
            tick.LastPrice = ReadInt(b, ref offset) / divisor;
            return tick;
        }

        /// <summary>
        /// Reads a index's quote mode tick from raw binary data
        /// </summary>
        private Tick ReadIndexQuote(byte[] b, ref int offset)
        {
            Tick tick = new Tick();
            tick.Mode = Constants.MODE_QUOTE;
            tick.InstrumentToken = ReadInt(b, ref offset);

            decimal divisor = (tick.InstrumentToken & 0xff) == 3 ? 10000000.0m : 100.0m;

            tick.Tradable = (tick.InstrumentToken & 0xff) != 9;
            tick.LastPrice = ReadInt(b, ref offset) / divisor;
            tick.High = ReadInt(b, ref offset) / divisor;
            tick.Low = ReadInt(b, ref offset) / divisor;
            tick.Open = ReadInt(b, ref offset) / divisor;
            tick.Close = ReadInt(b, ref offset) / divisor;
            tick.Change = ReadInt(b, ref offset) / divisor;
            return tick;
        }

        private Tick ReadIndexFull(byte[] b, ref int offset)
        {
            Tick tick = new Tick();
            tick.Mode = Constants.MODE_FULL;
            tick.InstrumentToken = ReadInt(b, ref offset);

            decimal divisor = (tick.InstrumentToken & 0xff) == 3 ? 10000000.0m : 100.0m;

            tick.Tradable = (tick.InstrumentToken & 0xff) != 9;
            tick.LastPrice = ReadInt(b, ref offset) / divisor;
            tick.High = ReadInt(b, ref offset) / divisor;
            tick.Low = ReadInt(b, ref offset) / divisor;
            tick.Open = ReadInt(b, ref offset) / divisor;
            tick.Close = ReadInt(b, ref offset) / divisor;
            tick.Change = ReadInt(b, ref offset) / divisor;
            uint time = ReadInt(b, ref offset);
            tick.Timestamp = Utils.UnixToDateTime(time);
            return tick;
        }

        /// <summary>
        /// Reads a quote mode tick from raw binary data
        /// </summary>
        private Tick ReadQuote(byte[] b, ref int offset)
        {
            Tick tick = new Tick();
            tick.Mode = Constants.MODE_QUOTE;
            tick.InstrumentToken = ReadInt(b, ref offset);

            decimal divisor = (tick.InstrumentToken & 0xff) == 3 ? 10000000.0m : 100.0m;

            tick.Tradable = (tick.InstrumentToken & 0xff) != 9;
            tick.LastPrice = ReadInt(b, ref offset) / divisor;
            tick.LastQuantity = ReadInt(b, ref offset);
            tick.AveragePrice = ReadInt(b, ref offset) / divisor;
            tick.Volume = ReadInt(b, ref offset);
            tick.BuyQuantity = ReadInt(b, ref offset);
            tick.SellQuantity = ReadInt(b, ref offset);
            tick.Open = ReadInt(b, ref offset) / divisor;
            tick.High = ReadInt(b, ref offset) / divisor;
            tick.Low = ReadInt(b, ref offset) / divisor;
            tick.Close = ReadInt(b, ref offset) / divisor;

            return tick;
        }

        /// <summary>
        /// Reads a full mode tick from raw binary data
        /// </summary>
        private Tick ReadFull(byte[] b, ref int offset)
        {
            Tick tick = new Tick();
            tick.Mode = Constants.MODE_FULL;
            tick.InstrumentToken = ReadInt(b, ref offset);

            decimal divisor = (tick.InstrumentToken & 0xff) == 3 ? 10000000.0m : 100.0m;

            tick.Tradable = (tick.InstrumentToken & 0xff) != 9;
            tick.LastPrice = ReadInt(b, ref offset) / divisor;
            tick.LastQuantity = ReadInt(b, ref offset);
            tick.AveragePrice = ReadInt(b, ref offset) / divisor;
            tick.Volume = ReadInt(b, ref offset);
            tick.BuyQuantity = ReadInt(b, ref offset);
            tick.SellQuantity = ReadInt(b, ref offset);
            tick.Open = ReadInt(b, ref offset) / divisor;
            tick.High = ReadInt(b, ref offset) / divisor;
            tick.Low = ReadInt(b, ref offset) / divisor;
            tick.Close = ReadInt(b, ref offset) / divisor;

            // KiteConnect 3 fields
            tick.LastTradeTime = Utils.UnixToDateTime(ReadInt(b, ref offset));
            tick.OI = ReadInt(b, ref offset);
            tick.OIDayHigh = ReadInt(b, ref offset);
            tick.OIDayLow = ReadInt(b, ref offset);
            tick.Timestamp = Utils.UnixToDateTime(ReadInt(b, ref offset));


            tick.Bids = new DepthItem[5];
            for (int i = 0; i < 5; i++)
            {
                tick.Bids[i].Quantity = ReadInt(b, ref offset);
                tick.Bids[i].Price = ReadInt(b, ref offset) / divisor;
                tick.Bids[i].Orders = ReadShort(b, ref offset);
                offset += 2;
            }

            tick.Offers = new DepthItem[5];
            for (int i = 0; i < 5; i++)
            {
                tick.Offers[i].Quantity = ReadInt(b, ref offset);
                tick.Offers[i].Price = ReadInt(b, ref offset) / divisor;
                tick.Offers[i].Orders = ReadShort(b, ref offset);
                offset += 2;
            }
            return tick;
        }

        private void _onData(byte[] Data, int Count, WebSocketMessageType MessageType)
        {
            _timerTick = _interval;
            if (MessageType == WebSocketMessageType.Binary)
            {
                if (Count == 1)
                {
                    //if (_debug) Console..WriteLine(DateTime.Now.ToLocalTime() + " Heartbeat");
                }
                else
                {
                    int offset = 0;
                    ushort count = ReadShort(Data, ref offset); //number of packets
                    //if (_debug) Console..WriteLine("No of packets: " + count);
                    //if (_debug) Console..WriteLine("No of bytes: " + Count);

                    for (ushort i = 0; i < count; i++)
                    {
                        ushort length = ReadShort(Data, ref offset); // length of the packet
                        //if (_debug) Console..WriteLine("Packet Length " + length);
                        Tick tick = new Tick();
                        if (length == 8) // ltp
                            tick = ReadLTP(Data, ref offset);
                        else if (length == 28) // index quote
                            tick = ReadIndexQuote(Data, ref offset);
                        else if (length == 32) // index quote
                            tick = ReadIndexFull(Data, ref offset);
                        else if (length == 44) // quote
                            tick = ReadQuote(Data, ref offset);
                        else if (length == 184) // full with marketdepth and timestamp
                            tick = ReadFull(Data, ref offset);
                        // If the number of bytes got from stream is less that that is required
                        // data is invalid. This will skip that wrong tick
                        if (tick.InstrumentToken != 0 && IsConnected && offset <= Count)
                        {
                            OnTick(tick);
                        }
                    }
                }
            }
            else if (MessageType == WebSocketMessageType.Text)
            {
                string message = Encoding.UTF8.GetString(Data.Take(Count).ToArray());
                //if (_debug) Console..WriteLine("WebSocket Message: " + message);

                Dictionary<string, dynamic> messageDict = Utils.JsonDeserialize(message);
                /*if (messageDict["type"] == "order")
                {
                    OnOrderUpdate?.Invoke(new Order(messageDict["data"]));
                }
                else if (messageDict["type"] == "error")
                {
                    OnError?.Invoke(messageDict["data"]);
                }*/
                Dictionary<string, dynamic> dataDict = messageDict["response"]["data"];
                Tick tick = new Tick();
                //changeinper, openintchange, lasttradetimeutc, 52weekhigh, 52weeklow
                //tick.Mode = Constants.MODE_FULL;
                //tick.InstrumentToken = ReadInt(b, ref offset);

                //decimal divisor = (tick.InstrumentToken & 0xff) == 3 ? 10000000.0m : 100.0m;

                //tick.Tradable = (tick.InstrumentToken & 0xff) != 9;
                tick.LastPrice = (decimal)Convert.ToDouble(dataDict["ltp"]);
                tick.LastQuantity = Convert.ToUInt32(dataDict["ltq"]);
                tick.AveragePrice = (decimal)Convert.ToDouble(dataDict["avgPr"]);
                tick.BuyQuantity = Convert.ToUInt32(dataDict["tBQ"]);
                tick.SellQuantity = Convert.ToUInt32(dataDict["tSQ"]);
                tick.Open = Convert.ToDecimal(dataDict["o"]);
                tick.High = Convert.ToDecimal(dataDict["h"]);
                tick.Low = Convert.ToDecimal(dataDict["l"]);
                tick.Close = Convert.ToDecimal(dataDict["c"]);
                tick.Volume = Convert.ToUInt32(dataDict["vol"]);
                tick.Change = Convert.ToDecimal(dataDict["ch"]);
                // KiteConnect 3 fields
                tick.LastTradeTime = Convert.ToDateTime(dataDict["ltt"]);
                if (dataDict["oI"] != "")
                tick.OI = Convert.ToUInt32(dataDict["oI"]);
                //tick.OIDayHigh = Convert.ToInt32(dataDict["vol"]);
                //tick.OIDayLow = ReadInt(b, ref offset);
                //tick.Timestamp = Utils.UnixToDateTime(ReadInt(b, ref offset));


                tick.Bids = new DepthItem[1];
                tick.Offers = new DepthItem[1];
                /*for (int i = 0; i < 5; i++)
                {
                    tick.Bids[i].Quantity = ReadInt(b, ref offset);
                    tick.Bids[i].Price = ReadInt(b, ref offset) / divisor;
                    tick.Bids[i].Orders = ReadShort(b, ref offset);
                    offset += 2;
                }

                tick.Offers = new DepthItem[5];
                for (int i = 0; i < 5; i++)
                {
                    tick.Offers[i].Quantity = ReadInt(b, ref offset);
                    tick.Offers[i].Price = ReadInt(b, ref offset) / divisor;
                    tick.Offers[i].Orders = ReadShort(b, ref offset);
                    offset += 2;
                }*/
                tick.Bids[0].Quantity = Convert.ToUInt32(dataDict["bSz"]);
                tick.Bids[0].Price = Convert.ToDecimal(dataDict["bPr"]);
                tick.Offers[0].Quantity = Convert.ToUInt32(dataDict["aSz"]);
                tick.Offers[0].Price = Convert.ToDecimal(dataDict["aPr"]);
                tick.Mode = dataDict["sym"];
                tick.Timestamp = Convert.ToDateTime(dataDict["ltt"]);
                string csv = String.Join(Environment.NewLine, dataDict["ltt"] + "," + dataDict["sym"] + "," + dataDict["o"] + "," + dataDict["h"] + "," + dataDict["l"] + "," + dataDict["c"] + "," + dataDict["ltp"] + "," + dataDict["vol"]);
                /*using (StreamWriter streamWriter = File.AppendText(filePath))
                {
                    //csv = String.Join(Environment.NewLine, "DateTime: " + dataDict["ltt"] + " Symbol: " + dataDict["sym"] + " Open: " + dataDict["o"] + " High: " + dataDict["h"] + " Low: " + dataDict["l"] + " Close: " + dataDict["c"] + " LTP: " + dataDict["ltp"] + " Volume: " + dataDict["vol"]);
                    streamWriter.WriteLine(csv);
                }*/
                Trace.WriteLine(csv);
                OnTick(tick);
            }
            else if (MessageType == WebSocketMessageType.Close)
            {
                Close();
            }

        }

        private void _onTimerTick(object sender, System.Timers.ElapsedEventArgs e)
        {
            // For each timer tick count is reduced. If count goes below 0 then reconnection is triggered.
            _timerTick--;
            if (_timerTick < 0)
            {
                _timer.Stop();
                if (_isReconnect)
                    Reconnect();
            }
            //if (_debug) Console..WriteLine(_timerTick);
        }

        private void _onConnect()
        {
            // Reset timer and retry counts and resubscribe to tokens.
            _retryCount = 0;
            _timerTick = _interval;
            //_timer.Start();
            if (_subscribedTokens.Count > 0)
                ReSubscribe();
            OnConnect?.Invoke();
        }

        /// <summary>
        /// Tells whether ticker is connected to server not.
        /// </summary>
        public bool IsConnected
        {
            get {
                try
                {
                    return true; // _ws.IsConnected();
                }
                catch (Exception e)
                {
                    return false;
                }
              }
        }

        /// <summary>
        /// Start a WebSocket connection
        /// </summary>
        public void Connect()
        {
            _timerTick = _interval;
            //_timer.Start();
            //if (!IsConnected)
            //{
            //    _ws.Connect(new Dictionary<string, string>() { ["x-session-token"] = _accessToken });
            //}
            try
            {
                MarketDataPorts[] ports = { MarketDataPorts.touchlineEvent, MarketDataPorts.indexDataEvent };
                //(MarketDataPorts[])Enum.GetValues(typeof(MarketDataPorts))
                ConnectToSocket(userID, (MarketDataPorts[])Enum.GetValues(typeof(MarketDataPorts)), PublishFormat.JSON, BroadcastMode.Partial);
            }
            catch (Exception e)
            {

            }

        }

        /// <summary>
        /// Close a WebSocket connection
        /// </summary>
        public void Close()
        {
            if(_timer!=null)
            _timer.Stop();
            if (_ws != null)
            _ws.Close();
        }

        /// <summary>
        /// Reconnect WebSocket connection in case of failures
        /// </summary>
        private void Reconnect()
        {
            if (IsConnected)
               // _ws.Close(true);

            if (_retryCount > _retries)
            {
                _ws.Close(true);
                DisableReconnect();
                OnNoReconnect?.Invoke();
            }
            else
            {
                OnReconnect?.Invoke();
                _retryCount += 1;
                //_ws.Close(true);
                Connect();
                _timerTick = (int)Math.Min(Math.Pow(2, _retryCount) * _interval, 60);
                //if (_debug) Console..WriteLine("New interval " + _timerTick);
                //_timer.Start();
            }
        }

        /// <summary>
        /// Subscribe to a list of instrument_tokens.
        /// </summary>
        /// <param name="Tokens">List of instrument instrument_tokens to subscribe</param>
        public void Subscribe(string[] Tokens)
        {
            //string msg = "{\"a\":\"subscribe\",\"v\":[" + String.Join(",", Tokens) + "]}";
            //if (_debug) Console..WriteLine(msg.Length);
            string dataStr = "";
            foreach (var token in Tokens) {
                dataStr = dataStr + "{\"symbol\":\""+token+"\"},";
            }
            //dataStr = dataStr + "{\"symbol\":\"" + "-21" + "\"},";
            dataStr = dataStr.Remove(dataStr.Length - 1);

            if (IsConnected)
            {
                Console.WriteLine("Sending json");
                Trace.WriteLine("Sending json");
                //string data = '{"request":{"streaming_type":"quote", "data":{"symbols":[{"symbol":"2885_NSE"}]}, "request_type":"subscribe", "response_format":"json"}}'
                //"{\"a\":\"subscribe\",\"v\":[" + String.Join(",", Tokens) + "]}";
                //218621_MFO for ZINC20JULFUT, 2885_NSE for RELIANCE
                //string data = "{\"request\":{\"streaming_type\": \"quote\",\"data\":{\"symbols\": [{\"symbol\":\"2885_NSE\"},{\"symbol\":\"218621_MFO\"}]},\"request_type\": \"subscribe\",\"response_format\": \"JSON\"}}";
                string data = "{\"request\":{\"streaming_type\": \"quote\",\"data\":{\"symbols\": ["+dataStr+"]},\"request_type\": \"subscribe\",\"response_format\": \"JSON\"}}";
                //string data = "{'request':{'streaming_type': 'quote','data':{'symbols': [{'symbol':'218621_MFO'}]},'request_type': 'subscribe','response_format': 'JSON'}}";
                //string data = "";
                Console.WriteLine(data);
                Trace.WriteLine(data);
                _ws.Send(data);
                _ws.Send("\n");
            }
            try
            {
                foreach (string token in Tokens)
                    if (!_subscribedTokens.ContainsKey(token))
                    {
                        _subscribedTokens.Add(token, "quote");
                    }
            }
            catch(KeyNotFoundException e)
            {
                Console.WriteLine(e.Message);
            }
               
        }

        /// <summary>
        /// Unsubscribe the given list of instrument_tokens.
        /// </summary>
        /// <param name="Tokens">List of instrument instrument_tokens to unsubscribe</param>
        public void UnSubscribe(string[] Tokens)
        {
            //string msg = "{\"a\":\"unsubscribe\",\"v\":[" + String.Join(",", Tokens) + "]}";
            //if (_debug) Console..WriteLine(msg);
            string dataStr = "";
            foreach (var token in Tokens)
            {
                dataStr = dataStr + "{\"symbol\":\"" + token + "\"},";
            }
            dataStr = dataStr.Remove(dataStr.Length - 1);

                string msg = "{\"request\":{\"streaming_type\": \"quote\",\"data\":{\"symbols\": [" + dataStr + "]},\"request_type\": \"unsubscribe\",\"response_format\": \"JSON\"}}";
                if (IsConnected)
                _ws.Send(msg);
            foreach (string token in Tokens)
                if (_subscribedTokens.ContainsKey(token))
                    _subscribedTokens.Remove(token);
        }

        /// <summary>
        /// Set streaming mode for the given list of tokens.
        /// </summary>
        /// <param name="Tokens">List of instrument tokens on which the mode should be applied</param>
        /// <param name="Mode">Mode to set. It can be one of the following: ltp, quote, full.</param>
        public void SetMode(string[] Tokens, string Mode)
        {
            string msg = "{\"a\":\"mode\",\"v\":[\"" + Mode + "\", [" + String.Join(",", Tokens) + "]]}";
            //if (_debug) Console..WriteLine(msg);

            if (IsConnected)
                //_ws.Send(msg);
            foreach (string token in Tokens)
                if (_subscribedTokens.ContainsKey(token))
                    _subscribedTokens[token] = Mode;
        }

        /// <summary>
        /// Resubscribe to all currently subscribed tokens. Used to restore all the subscribed tokens after successful reconnection.
        /// </summary>
        public void ReSubscribe()
        {
            //if (_debug) Console..WriteLine("Resubscribing");
            string[] all_tokens = _subscribedTokens.Keys.ToArray();

            string[] ltp_tokens = all_tokens.Where(key => _subscribedTokens[key] == "ltp").ToArray();
            string[] quote_tokens = all_tokens.Where(key => _subscribedTokens[key] == "quote").ToArray();
            string[] full_tokens = all_tokens.Where(key => _subscribedTokens[key] == "full").ToArray();

            UnSubscribe(all_tokens);
            Subscribe(all_tokens);

            SetMode(ltp_tokens, "ltp");
            SetMode(quote_tokens, "quote");
            SetMode(full_tokens, "full");
        }

        /// <summary>
        /// Enable WebSocket autreconnect in case of network failure/disconnection.
        /// </summary>
        /// <param name="Interval">Interval between auto reconnection attemptes. `onReconnect` callback is triggered when reconnection is attempted.</param>
        /// <param name="Retries">Maximum number reconnection attempts. Defaults to 50 attempts. `onNoReconnect` callback is triggered when number of retries exceeds this value.</param>
        public void EnableReconnect(int Interval = 5, int Retries = 50)
        {
            _isReconnect = true;
            _interval = Math.Max(Interval, 5);
            _retries = Retries;

            _timerTick = _interval;
            //if (IsConnected)
                //_timer.Start();
        }

        /// <summary>
        /// Disable WebSocket autreconnect.
        /// </summary>
        public void DisableReconnect()
        {
            _isReconnect = false;
            if (IsConnected)
                _timer.Stop();
            _timerTick = _interval;
        }

        public bool ConnectToSocket(string UserId, MarketDataPorts[] marketDataPorts, PublishFormat publishFormat = PublishFormat.JSON, BroadcastMode broadcastMode = BroadcastMode.Partial, string source = "WebAPI")
        {
            if (marketDataPorts == null || marketDataPorts.Length == 0)
                return false;

            if (string.IsNullOrEmpty(this._accessToken))
                return false;

            if (string.IsNullOrWhiteSpace(UserId))
                return false;

            HttpClient httpClient = this.HttpClient;

            if (httpClient == null && httpClient.BaseAddress == null)
                return false;

            //Connect to the socket
            Quobject.SocketIoClientDotNet.Client.IO.Options options = new Quobject.SocketIoClientDotNet.Client.IO.Options()
            {
                IgnoreServerCertificateValidation = true,
                Path = "/marketdata/socket.io",
                Query = new Dictionary<string, string>()
                    {
                        { "token", _accessToken },
                        { "userID", UserId },
                        { "source", string.IsNullOrEmpty(source) ? OrderSource.WebAPI : source },
                        { "publishFormat", publishFormat.ToString() },
                        { "broadcastMode", broadcastMode.ToString() }
                    }
            };

            this.Socket = IO.Socket(httpClient.BaseAddress, options);

            //subscribe to the base methods
            if (!SubscribeToConnectionEvents())
                return false;

            string format = publishFormat.ToString().ToLower();
            string mode = broadcastMode.ToString().ToLower();

            if (Array.IndexOf<MarketDataPorts>(marketDataPorts, MarketDataPorts.touchlineEvent) >= 0)
            {
                this.Socket.On($"1501-{format}-{mode}", (data) =>
                {
                    //var json = json.str
                    //var values = JsonConvert.DeserializeObject<Dictionary<string, string>>("{"+data.ToString()+"}");
                    Dictionary<string, string> dataDict = new Dictionary<string, string>();
                    string[] values = data.ToString().Split(',');
                    if (values.Length > 1)
                    {
                    foreach (string val in values)
                    {
                        string[] vals = val.Split(':');
                        dataDict.Add(vals[0], vals[1]);
                    }
                    //var dictionary = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
                    Tick tick = new Tick();
                    tick.LastPrice = (decimal)Convert.ToDouble(dataDict["ltp"]);
                    tick.LastQuantity = Convert.ToUInt32(dataDict["ltq"]);
                    tick.AveragePrice = (decimal)Convert.ToDouble(dataDict["ap"]);
                    tick.BuyQuantity = Convert.ToUInt32(dataDict["tb"]);
                    tick.SellQuantity = Convert.ToUInt32(dataDict["ts"]);
                    tick.Open = Convert.ToDecimal(dataDict["o"]);
                    tick.High = Convert.ToDecimal(dataDict["h"]);
                    tick.Low = Convert.ToDecimal(dataDict["l"]);
                    tick.Close = Convert.ToDecimal(dataDict["c"]);
                    tick.Volume = Convert.ToUInt32(dataDict["v"]);
                    string currTime = (new DateTime(1980, 1, 1)).AddSeconds(Convert.ToDouble(dataDict["ltt"])).ToString("yyyy-MM-dd HH:mm:ss");
                    tick.LastTradeTime = DateTime.Parse(currTime);
                    tick.Bids = new DepthItem[1];
                    tick.Offers = new DepthItem[1];
                    string[] bidDet = dataDict["bi"].Split('|');
                    tick.Bids[0].Quantity = Convert.ToUInt32(bidDet[1]);
                    tick.Bids[0].Price = Convert.ToDecimal(bidDet[2]);
                    string[] askDet = dataDict["ai"].Split('|');
                    tick.Offers[0].Quantity = Convert.ToUInt32(askDet[1]);
                    tick.Offers[0].Price = Convert.ToDecimal(askDet[2]);
                    //tick.Mode = dataDict["sym"];
                    tick.Timestamp = DateTime.Parse(currTime);
                    tick.InstrumentToken = (uint)Convert.ToDouble(dataDict["t"].Split('_')[1]);
                    //string csv = String.Join(Environment.NewLine, dataDict["ltt"] + "," + dataDict["sym"] + "," + dataDict["o"] + "," + dataDict["h"] + "," + dataDict["l"] + "," + dataDict["c"] + "," + dataDict["ltp"] + "," + dataDict["vol"]);
                    //Trace.WriteLine(csv);
                    OnTick(tick); 
                    Console.WriteLine(data);
                    //OnTick.Invoke(data);
                    //OnData<Touchline>(MarketDataPorts.touchlineEvent, data, publishFormat, broadcastMode);
                    }
                });
            }

            if (Array.IndexOf<MarketDataPorts>(marketDataPorts, MarketDataPorts.marketDepthEvent) >= 0)
            {
                //this.Socket.On($"1502-{format}-{mode}", (data) =>
                //{
                //    //OnData<MarketDepth>(MarketDataPorts.marketDepthEvent, data, publishFormat, broadcastMode);
                //    Console.WriteLine(data);
                //});
            }

            if (Array.IndexOf<MarketDataPorts>(marketDataPorts, MarketDataPorts.indexDataEvent) >= 0)
            {
                this.Socket.On($"1504-json-full", (data) =>
                {
                    Console.WriteLine(data);
            });
                this.Socket.On($"1504-{format}-{mode}", (data) =>
                { 
                    Dictionary<string, string> dataDict = new Dictionary<string, string>();
                    string[] values = data.ToString().Split(',');
                    if (values.Length > 1)
                    {
                    foreach (string val in values)
                    {
                        string[] vals = val.Split(':');
                        dataDict.Add(vals[0], vals[1]);
                    }
                    //var dictionary = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
                    Tick tick = new Tick();
                    tick.LastPrice = (decimal)Convert.ToDouble(dataDict["ltp"]);
                    //tick.LastQuantity = Convert.ToUInt32(dataDict["ltq"]);
                    //tick.AveragePrice = (decimal)Convert.ToDouble(dataDict["ap"]);
                    //tick.BuyQuantity = Convert.ToUInt32(dataDict["tb"]);
                    //tick.SellQuantity = Convert.ToUInt32(dataDict["ts"]);
                    tick.Open = Convert.ToDecimal(dataDict["o"]);
                    tick.High = Convert.ToDecimal(dataDict["h"]);
                    tick.Low = Convert.ToDecimal(dataDict["l"]);
                    tick.Close = Convert.ToDecimal(dataDict["c"]);
                    //tick.Volume = Convert.ToUInt32(dataDict["v"]);
                    string currTime = (new DateTime(1980, 1, 1)).AddSeconds(Convert.ToDouble(dataDict["lut"])).ToString("yyyy-MM-dd HH:mm:ss");
                    tick.LastTradeTime = DateTime.Parse(currTime);
                    //tick.Bids = new DepthItem[1];
                    //tick.Offers = new DepthItem[1];
                    //string[] bidDet = dataDict["bi"].Split('|');
                    //tick.Bids[0].Quantity = Convert.ToUInt32(bidDet[1]);
                    //tick.Bids[0].Price = Convert.ToDecimal(bidDet[2]);
                    //string[] askDet = dataDict["ai"].Split('|');
                    //tick.Offers[0].Quantity = Convert.ToUInt32(askDet[1]);
                    //tick.Offers[0].Price = Convert.ToDecimal(askDet[2]);
                    tick.Timestamp = DateTime.Parse(currTime);
                    tick.Mode = dataDict["t"].Split('_')[1];
                    tick.InstrumentToken = 0;
                    //string csv = String.Join(Environment.NewLine, dataDict["ltt"] + "," + dataDict["sym"] + "," + dataDict["o"] + "," + dataDict["h"] + "," + dataDict["l"] + "," + dataDict["c"] + "," + dataDict["ltp"] + "," + dataDict["vol"]);
                    //Trace.WriteLine(csv);
                    OnTick(tick);
                    Console.WriteLine("data is " + data);
                    }
                });
            }

            //if (Array.IndexOf<MarketDataPorts>(marketDataPorts, MarketDataPorts.candleDataEvent) >= 0)
            //{
            //    this.Socket.On($"1505-{format}-{mode}", (data) =>
            //    {
            //        OnData<Candle>(MarketDataPorts.candleDataEvent, data, publishFormat, broadcastMode);
            //    });
            //}

            /*

            this.Socket.On($"1506-{publishFormat}", (data) =>
            {
                OnJsonData(MarketDataPorts.generalMessageBroadcastEvent, data);
            });

            this.Socket.On($"1507-{publishFormat}", (data) =>
            {
                OnJsonData(MarketDataPorts.exchangeTradingStatusEvent, data);
            });
            */

            //if (Array.IndexOf<MarketDataPorts>(marketDataPorts, MarketDataPorts.openInterestEvent) >= 0)
            //{
            //    this.Socket.On($"1510-{format}-{mode}", (data) =>
            //    {
            //        OnData<OI>(MarketDataPorts.openInterestEvent, data, publishFormat, broadcastMode);
            //    });
            //}

            /*

            this.Socket.On($"5505-{publishFormat}", (data) =>
            {
                OnJsonData(MarketDataPorts.instrumentSubscriptionInfo, data);
            });

            this.Socket.On($"5018-{publishFormat}", (data) =>
            {
                OnJsonData(MarketDataPorts.marketDepthEvent100, data);
            });
            */

            return true;
        }

        protected virtual bool SubscribeToConnectionEvents()
        {

            Socket socket = this.Socket;
            if (socket == null)
                return false;


            socket.On(Socket.EVENT_CONNECT, (data) =>
            {
                //this.OnConnectionState(ConnectionEvents.connect, data);
                Console.WriteLine("Connected: " + data);
            });

            socket.On("joined", (data) =>
            {
                //this.isConnectedToSocket = true;
                //this.OnConnectionState(ConnectionEvents.joined, data);
            });

            socket.On("success", (data) =>
            {
                //this.OnConnectionState(ConnectionEvents.success, data);
                Console.WriteLine("Success: " + data);
            });

            socket.On("warning", (data) =>
            {
                //OnConnectionState(ConnectionEvents.warning, data);
                Console.WriteLine("warning: " + data);
            });

            socket.On("error", (data) =>
            {
                //OnConnectionState(ConnectionEvents.error, data);
                Console.WriteLine("error: " + data);
            });

            socket.On("logout", (data) =>
            {
                //this.isConnectedToSocket = false;
                //OnConnectionState(ConnectionEvents.logout, data);
                Console.WriteLine("logout: " + data);
            });

            socket.On("disconnect", (data) =>
            {
                //OnConnectionState(ConnectionEvents.logout, data);
                Console.WriteLine("disconnect: " + data);
            });

            return true;
        }

        private void OnData<T>(MarketDataPorts port, object data, PublishFormat publishFormat, BroadcastMode broadcastMode)
        {
            Console.WriteLine("OnData: " + data);
            //if (data == null)
            //    return;

            //try
            //{
            //    T quote = null;

            //    if (broadcastMode == BroadcastMode.Partial)
            //    {
            //        quote = new T();
            //        quote.AssignValue(data);
            //    }
            //    else
            //    {
            //        string str = data.ToString();
            //        if (string.IsNullOrEmpty(str))
            //            return;

            //        quote = ParseString<T>(str, triggerJsonEvent: false);
            //    }

            //    if (quote == null)
            //        return;

            //    OnMarketData(port, quote, data);
            //}
            //catch (Exception ex)
            //{
            //    //OnException(typeof(T), ex);
            //}

        }

        private void OnMarketData(MarketDataPorts port, ListQuotesBase data, object sourceData)
        {
            //this.MarketData?.Invoke(null, new MarketDataEventArgs(port, data, sourceData));
            Console.WriteLine("OnMarketData: " + data);
        }
    }
}
