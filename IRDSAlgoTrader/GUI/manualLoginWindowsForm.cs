﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IRDSAlgoOMS.GUI
{
    public partial class manualLoginWindowsForm : Form
    {
        public manualLoginWindowsForm(Kite kite, string url, string m_Title)
        {
            InitializeComponent(kite, url,this, m_Title);
        }
        public string Token
        {
            get
            {               
                return this.wpfManualLogin2.Token;
            }
        }      
    }
}
