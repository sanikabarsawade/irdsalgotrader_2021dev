﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IRDSAlgoOMS.GUI
{
	//IRDSPM::Pratiksha::08-02-2021::Added parameter
    public partial class AllSymbolSettingWindowsForm : Form
    {
        public AllSymbolSettingWindowsForm(dynamic kiteConnectWrapper, string title)
        {
            this.TopMost = true;
            InitializeComponent(kiteConnectWrapper, this, title);
            this.Load += AllSymbolSettingWindowsForm_Load;
        }

        private void AllSymbolSettingWindowsForm_Load(object sender, EventArgs e)
        {
            this.TopMost = false;
        }
    }
}
