﻿using System.IO;
using System.Reflection;
namespace IRDSAlgoOMS.GUI
{
    partial class SubscriptionDetWindowsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /// 
        //IRDSPM::Pratiksha::12-02-2021::dynamically set icon
        Assembly myAssembly = Assembly.GetExecutingAssembly();
        Stream IconStream = null;
        private void InitializeComponent(SubscriptionDetWindowsForm obj, dynamic ObjkiteConnectWrapper, string title)
        {
            this.elementHost2 = new System.Windows.Forms.Integration.ElementHost();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.subscriptionDetWPFForm2 = new IRDSAlgoOMS.GUI.SubscriptionDetWPFForm(obj, ObjkiteConnectWrapper);
            this.SuspendLayout();
            // 
            // elementHost2
            // 
            this.elementHost2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.elementHost2.Location = new System.Drawing.Point(0, 0);
            this.elementHost2.Name = "elementHost2";
            this.elementHost2.Size = new System.Drawing.Size(600, 380);
            this.elementHost2.TabIndex = 0;
            this.elementHost2.Text = "elementHost2";
            this.elementHost2.Child = this.subscriptionDetWPFForm2;
            // 
            // SubscriptionDetWindowsForm
            // 
            IconStream = myAssembly.GetManifestResourceStream("IRDSAlgoOMS.Resources." + title + ".ico");
            this.Icon = new System.Drawing.Icon(IconStream);
            this.ClientSize = new System.Drawing.Size(600, 380);
            this.Controls.Add(this.elementHost2);
            this.Name = "SubscriptionDetWindowsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.ResumeLayout(false);
            this.Text = "Subscription Details";
            this.MaximizeBox = false;
        }

        #endregion

        private System.Windows.Forms.Integration.ElementHost elementHost1;
        private SubscriptionDetWPFForm subscriptionDetWPFForm1;
        private System.Windows.Forms.Integration.ElementHost elementHost2;
        private SubscriptionDetWPFForm subscriptionDetWPFForm2;
    }
}