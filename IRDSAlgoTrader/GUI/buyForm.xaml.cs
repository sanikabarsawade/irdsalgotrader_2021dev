﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.IO;

namespace IRDSAlgoOMS.GUI
{
    public partial class buyForm : System.Windows.Controls.UserControl
    {
        BrushConverter bc = new BrushConverter();
        //sanika::11-sep-2020::Changed to dyanamic for samco and zerodha
        dynamic m_kiteConnectWrapper = null;
        string Exchange = "";
        string Price = "";
        string DisClosedQuantity = "";
        string TradingSymbol = "";
        string TransactionType = "";
        string Quantity = "";
        string OrderType = "";
        string Product = "";
        string Validity = "";
        string Variety = "";
        string TriggerPrice = "";
        string StopLossValue = "";
        string TrailingStoploss = "";
        string inisymbolList;
        ReadSettings Settings;
        string typeofOrderBuyForm;
        string selectedSymbolFromUser = "";
        Logger logger = Logger.Instance;
        public List<string> RealtimeSymbolfromsymbolIni;
        string path = Directory.GetCurrentDirectory();
        List<string> FinalSymbolList = new List<string>();
        BuyWindowsForm m_BuyWindowsForm = null;
        string m_title= "";
        //sanika::11-sep-2020::Changed to dyanamic for samco and zerodha
        public buyForm(dynamic objKiteConnectWrapper, BuyWindowsForm obj, string title)
        {
            InitializeComponent();
            m_title = title;
            m_BuyWindowsForm = obj;
            m_kiteConnectWrapper = objKiteConnectWrapper;
            CncRadio.IsChecked = true;
            marketRadio.IsChecked = true;
            regularradio.IsChecked = true;
            dayradio.IsChecked = true;
            NSERadio.IsChecked = true;
            m_SearchTextBox.Text = "";
            FinalSymbolList = m_kiteConnectWrapper.ReturnAllSymbolsFromDBdata();
            ReadSettingFile();
        }
        //IRDSPM::Pratiksha::07-12-2020::For Reading which broker is set and according to that disable the zerodha links if samco is selected--start
        private void ReadSettingFile()
        {
            ConfigSettings fileObj = new ConfigSettings();
            fileObj.ReadSettingConfigFile(path + @"\Configuration\Setting.ini");
            string brokername = fileObj.m_brokerName;
            if(brokername.ToLower() == "samco" || brokername.ToLower() == "composite")
            {
                image1.Visibility = Visibility.Hidden;
                image2.Visibility = Visibility.Hidden;
            }
            else if(brokername.ToLower() == "zerodha")
            {
                image1.Visibility = Visibility.Visible;
                image2.Visibility = Visibility.Visible;
            }
        }
        //IRDSPM::Pratiksha::07-12-2020::For Reading which broker is set and according to that disable the zerodha links if samco is selected--end
        public void orderType(string typeOfOrder)
        {
            typeofOrderBuyForm = typeOfOrder;
            if ((typeofOrderBuyForm.Length > 3) && (typeofOrderBuyForm.Contains("Buy")))
            {
                string s = typeofOrderBuyForm;
                int index = s.IndexOf(' ');
                string first = s.Substring(0, index);
                string second = s.Substring(index + 1);
                selectedSymbolFromUser = second;
                SymbolName.Content = selectedSymbolFromUser + "  " + NSERadio.Content + " x " + textQty.Text + " Qty";
                //m_SearchTextBox.IsEnabled = false;
            }
            else if ((typeofOrderBuyForm.Length > 4) && (typeofOrderBuyForm.Contains("Sell")))
            {
                string s = typeofOrderBuyForm;
                int index = s.IndexOf(' ');
                string first = s.Substring(0, index);
                string second = s.Substring(index + 1);
                selectedSymbolFromUser = second;
                SymbolName.Content = selectedSymbolFromUser + "  " + NSERadio.Content + " x " + textQty.Text + " Qty";
                //m_SearchTextBox.IsEnabled = false;
            }
            else
            {
                SymbolName.Content = "  " + NSERadio.Content + " x " + textQty.Text + " Qty";
                m_SearchTextBox.IsEnabled = true;
            }
            if (typeOfOrder.Contains("Buy"))
            {
                buyradio.IsChecked = true;
                grid1.Background = (Brush)bc.ConvertFrom("#4185F4");
                btnBuy.Content = "Buy";
                btnBuy.Background = (Brush)bc.ConvertFrom("#4185F4");
                btnBuy.BorderBrush = (Brush)bc.ConvertFrom("#4185F4");
                grid2.Background = (Brush)bc.ConvertFrom("#FFF5F8FD");
                transactionTypeLbl.Content = "Buy";
            }
            //IRDSPM::Pratiksha::09-10-2020::For Placing sell order
            else if (typeOfOrder.Contains("Sell"))
            {
                sellradio.IsChecked = true;
                grid1.Background = (Brush)bc.ConvertFrom("#FF561E");
                btnBuy.Content = "Sell";
                btnBuy.Background = (Brush)bc.ConvertFrom("#FF561E");
                btnBuy.BorderBrush = (Brush)bc.ConvertFrom("#FF561E");
                grid2.Background = (Brush)bc.ConvertFrom("#FFF5F3");
                transactionTypeLbl.Content = "Sell";
            }
        }
        private void marketRadio_checked(object sender, RoutedEventArgs e)
        {
        	//pratiksha::16-oct-2020::call to close search box
            ForSearchlistClose();
            sltextBox.IsEnabled = false;
            StoplossgroupBox.IsEnabled = false;
            StoplossgroupBox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
            StoplossgroupBox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");

            targettextBox.IsEnabled = false;
            TargetgroupBox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
            TargetgroupBox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
            TargetgroupBox.IsEnabled = false;

            tstextBox.IsEnabled = false;
            tsgroupBox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
            tsgroupBox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
            tsgroupBox.IsEnabled = false;

            pricegroupbox.IsEnabled = false;
            pricegroupbox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
            pricegroupbox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
            tpgroupbox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
            tpgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
            textTP.IsEnabled = false;
            dqgroupbox.IsEnabled = true;
            dqgroupbox.Foreground = (Brush)bc.ConvertFrom("#FF777777");
            dqgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#FF777777");
            textdp.IsEnabled = true;
            if (coradio.IsChecked == true)
            {
                pricegroupbox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                pricegroupbox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                pricegroupbox.IsEnabled = false;
                textTP.IsEnabled = false;
                dqgroupbox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                dqgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                dqgroupbox.IsEnabled = false;
                textdp.IsEnabled = false;
                tpgroupbox.IsEnabled = true;
                tpgroupbox.Foreground = (Brush)bc.ConvertFrom("#FF777777");
                tpgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#FF777777");
                textTP.IsEnabled = true;
            }
            else if (amoradio.IsChecked == true)
            {
                pricegroupbox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                pricegroupbox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                pricegroupbox.IsEnabled = false;
                textTP.IsEnabled = false;
                dqgroupbox.Foreground = (Brush)bc.ConvertFrom("#FF777777");
                dqgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#FF777777");
                dqgroupbox.IsEnabled = true;
                textdp.IsEnabled = true;
                tpgroupbox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                tpgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                textTP.IsEnabled = false;
                tpgroupbox.IsEnabled = false;
            }
            else if (regularradio.IsChecked == true)
            {
                pricegroupbox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                pricegroupbox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                pricegroupbox.IsEnabled = false;
                textTP.IsEnabled = false;
                dqgroupbox.Foreground = (Brush)bc.ConvertFrom("#FF777777");
                dqgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#FF777777");

                dqgroupbox.IsEnabled = true;
                textdp.IsEnabled = true;
                tpgroupbox.IsEnabled = false;
                tpgroupbox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                tpgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                textTP.IsEnabled = false;
            }
            if (iocradio.IsChecked == true)
            {
                dqgroupbox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                dqgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                textdp.IsEnabled = false;
                dqgroupbox.IsEnabled = false;
            }
        }
        private void LimitRadio_checked(object sender, RoutedEventArgs e)
        {
        	//pratiksha::16-oct-2020::call to close search box
            ForSearchlistClose();
            pricegroupbox.IsEnabled = true;
            textPrice.IsEnabled = true;
            tpgroupbox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
            tpgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
            textTP.IsEnabled = false;
            dqgroupbox.IsEnabled = true;
            pricegroupbox.Foreground = (Brush)bc.ConvertFrom("#FF777777");
            pricegroupbox.BorderBrush = (Brush)bc.ConvertFrom("#FF777777");
            dqgroupbox.IsEnabled = true;
            dqgroupbox.Foreground = (Brush)bc.ConvertFrom("#FF777777");
            dqgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#FF777777");
            textdp.IsEnabled = true;
            if (boradio.IsChecked == true)
            {
                tpgroupbox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                tpgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                tpgroupbox.IsEnabled = false;
                dqgroupbox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                dqgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                textdp.IsEnabled = false;
                dqgroupbox.IsEnabled = false;
                textTP.IsEnabled = false;
            }
            if (coradio.IsChecked == true)
            {
                pricegroupbox.Foreground = (Brush)bc.ConvertFrom("#FF777777");
                pricegroupbox.BorderBrush = (Brush)bc.ConvertFrom("#FF777777");
                pricegroupbox.IsEnabled = true;
                textTP.IsEnabled = true;
                dqgroupbox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                dqgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                dqgroupbox.IsEnabled = false;
                textdp.IsEnabled = false;
                tpgroupbox.IsEnabled = true;
                tpgroupbox.Foreground = (Brush)bc.ConvertFrom("#FF777777");
                tpgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#FF777777");
                textTP.IsEnabled = true;
                StoplossgroupBox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                StoplossgroupBox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                StoplossgroupBox.IsEnabled = false;
                TargetgroupBox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                TargetgroupBox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                TargetgroupBox.IsEnabled = false;
                tsgroupBox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                tsgroupBox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                tsgroupBox.IsEnabled = false;
            }
            if (iocradio.IsChecked == true)
            {
                dqgroupbox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                dqgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                textdp.IsEnabled = false;
                dqgroupbox.IsEnabled = false;
            }
        }
        private void SlRadio_checked(object sender, RoutedEventArgs e)
        {
        	//pratiksha::16-oct-2020::call to close search box
            ForSearchlistClose();
            pricegroupbox.IsEnabled = true;
            tpgroupbox.IsEnabled = true;
            dqgroupbox.IsEnabled = true;
            textPrice.IsEnabled = true;
            textTP.IsEnabled = true;
            textdp.IsEnabled = true;
            pricegroupbox.Foreground = (Brush)bc.ConvertFrom("#FF777777");
            pricegroupbox.BorderBrush = (Brush)bc.ConvertFrom("#FF777777");
            tpgroupbox.Foreground = (Brush)bc.ConvertFrom("#FF777777");
            tpgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#FF777777");
            textTP.IsEnabled = true;
            dqgroupbox.IsEnabled = true;
            dqgroupbox.Foreground = (Brush)bc.ConvertFrom("#FF777777");
            dqgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#FF777777");
            textdp.IsEnabled = true;
            if (boradio.IsChecked == true)
            {
                tpgroupbox.Foreground = (Brush)bc.ConvertFrom("#FF777777");
                tpgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#FF777777");
                tpgroupbox.IsEnabled = true;
                dqgroupbox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                dqgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                dqgroupbox.IsEnabled = false;
                textdp.IsEnabled = false;
                textTP.IsEnabled = true;
            }
            if (iocradio.IsChecked == true)
            {
                dqgroupbox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                dqgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                textdp.IsEnabled = false;
                dqgroupbox.IsEnabled = false;
            }
        }
        private void slmRadio_checked(object sender, RoutedEventArgs e)
        {
        	//pratiksha::16-oct-2020::call to close search box
            ForSearchlistClose();
            pricegroupbox.IsEnabled = false;
            pricegroupbox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
            pricegroupbox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
            tpgroupbox.Foreground = (Brush)bc.ConvertFrom("#FF777777");
            tpgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#FF777777");
            textTP.IsEnabled = true;
            dqgroupbox.IsEnabled = true;
            dqgroupbox.Foreground = (Brush)bc.ConvertFrom("#FF777777");
            dqgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#FF777777");
            textdp.IsEnabled = true;
            tpgroupbox.IsEnabled = true;
            textTP.IsEnabled = true;
            dqgroupbox.IsEnabled = true;
            if (iocradio.IsChecked == true)
            {
                dqgroupbox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                dqgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                textdp.IsEnabled = false;
                dqgroupbox.IsEnabled = false;
            }
        }
        private void misradioChecked(object sender, RoutedEventArgs e)
        {
        	//pratiksha::16-oct-2020::call to close search box
            ForSearchlistClose();
        }
        private void cncradioChecked(object sender, RoutedEventArgs e)
        {
        	//pratiksha::16-oct-2020::call to close search box
            ForSearchlistClose();
            StoplossgroupBox.IsEnabled = false;
            StoplossgroupBox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
            StoplossgroupBox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
            TargetgroupBox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
            TargetgroupBox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
            tstextBox.IsEnabled = false;
            tsgroupBox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
            tsgroupBox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
            pricegroupbox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
            pricegroupbox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
            pricegroupbox.IsEnabled = false;
            tpgroupbox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
            tpgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
            tpgroupbox.IsEnabled = false;
        }

        private void buyradio_Checked(object sender, RoutedEventArgs e)
        {
        	//pratiksha::16-oct-2020::call to close search box
            ForSearchlistClose();
            grid1.Background = (Brush)bc.ConvertFrom("#4185F4");
            btnBuy.Content = "Buy";
            btnBuy.Background = (Brush)bc.ConvertFrom("#4185F4");
            btnBuy.BorderBrush = (Brush)bc.ConvertFrom("#4185F4");
            grid2.Background = (Brush)bc.ConvertFrom("#FFF5F8FD");
            transactionTypeLbl.Content = "Buy";
            if (CncRadio.IsChecked == true)
            {
                StoplossgroupBox.IsEnabled = false;
                StoplossgroupBox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                StoplossgroupBox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                TargetgroupBox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                TargetgroupBox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                tstextBox.IsEnabled = false;
                tsgroupBox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                tsgroupBox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                pricegroupbox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                pricegroupbox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                pricegroupbox.IsEnabled = false;
                tpgroupbox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                tpgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                tpgroupbox.IsEnabled = false;
            }
        }

        private void sellradio_Checked(object sender, RoutedEventArgs e)
        {
        	//pratiksha::16-oct-2020::call to close search box
            ForSearchlistClose();
            grid1.Background = (Brush)bc.ConvertFrom("#FF561E");
            btnBuy.Content = "Sell";
            btnBuy.Background = (Brush)bc.ConvertFrom("#FF561E");
            btnBuy.BorderBrush = (Brush)bc.ConvertFrom("#FF561E");
            grid2.Background = (Brush)bc.ConvertFrom("#FFF5F3");
            transactionTypeLbl.Content = "Sell";
        }
        private void buyMouseEnter(object sender, RoutedEventArgs e)
        {
            btnBuy.Foreground = Brushes.Black;
        }
        private void buyMouseLeave(object sender, RoutedEventArgs e)
        {
            btnBuy.Foreground = Brushes.White;
        }
        private void btnSearch_MouseDown(object sender, RoutedEventArgs e)
        {//pratiksha::16-oct-2020::call to close search box
            ForSearchlistClose();
            System.Diagnostics.Process.Start("https://support.zerodha.com/category/trading-and-markets/kite-web-and-mobile/articles/margins-on-kite-order-window");
        }
        private void transaction_Click(object sender, RoutedEventArgs e)
        {
        	//pratiksha::16-oct-2020::call to close search box
            ForSearchlistClose();
            System.Diagnostics.Process.Start("https://kite.trade/docs/kite/orders/#orders");
        }

        //IRDSPM::Pratiksha::08-09-2020::For closing form
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
        	//pratiksha::16-oct-2020::call to close search box
            ForSearchlistClose();
            m_BuyWindowsForm.Close();
            selectedSymbolFromUser = "";
        }
        private void regular_Click(object sender, RoutedEventArgs e)
        {
        	//pratiksha::16-oct-2020::call to close search box
            ForSearchlistClose();
            tpgroupbox.Header = "Trigger price";
            SlRadio.IsEnabled = true;
            if (marketRadio.IsChecked == true)
            {
                pricegroupbox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                pricegroupbox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                pricegroupbox.IsEnabled = false;
                textPrice.IsEnabled = false;
                tpgroupbox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                tpgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                tpgroupbox.IsEnabled = false;
                textTP.IsEnabled = false;
                dqgroupbox.IsEnabled = true;
                dqgroupbox.Foreground = (Brush)bc.ConvertFrom("#FF777777");
                dqgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#FF777777");
                textdp.IsEnabled = true;
            }
            else if (LimitRadio.IsChecked == true)
            {
                pricegroupbox.Foreground = (Brush)bc.ConvertFrom("#FF777777");
                pricegroupbox.BorderBrush = (Brush)bc.ConvertFrom("#FF777777");
                pricegroupbox.IsEnabled = true;
                textPrice.IsEnabled = true;
                tpgroupbox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                tpgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                tpgroupbox.IsEnabled = false;
                textTP.IsEnabled = false;
                dqgroupbox.IsEnabled = true;
                dqgroupbox.Foreground = (Brush)bc.ConvertFrom("#FF777777");
                dqgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#FF777777");
                textdp.IsEnabled = true;
            }
            else if (SlRadio.IsChecked == true)
            {
                pricegroupbox.Foreground = (Brush)bc.ConvertFrom("#FF777777");
                pricegroupbox.BorderBrush = (Brush)bc.ConvertFrom("#FF777777");
                pricegroupbox.IsEnabled = true;
                textPrice.IsEnabled = true;
                tpgroupbox.Foreground = (Brush)bc.ConvertFrom("#FF777777");
                tpgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#FF777777");
                tpgroupbox.IsEnabled = true;
                textTP.IsEnabled = true;
                dqgroupbox.IsEnabled = true;
                dqgroupbox.Foreground = (Brush)bc.ConvertFrom("#FF777777");
                dqgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#FF777777");
                textdp.IsEnabled = true;
            }
            else if (slmRadio.IsChecked == true)
            {
                pricegroupbox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                pricegroupbox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                pricegroupbox.IsEnabled = false;
                textPrice.IsEnabled = false;
                tpgroupbox.Foreground = (Brush)bc.ConvertFrom("#FF777777");
                tpgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#FF777777");
                tpgroupbox.IsEnabled = true;
                textTP.IsEnabled = true;
                dqgroupbox.IsEnabled = true;
                dqgroupbox.Foreground = (Brush)bc.ConvertFrom("#FF777777");
                dqgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#FF777777");
                textdp.IsEnabled = true;
            }
            sltextBox.IsEnabled = false;
            StoplossgroupBox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
            StoplossgroupBox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
            targettextBox.IsEnabled = false;
            TargetgroupBox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
            TargetgroupBox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
            tstextBox.IsEnabled = false;
            tsgroupBox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
            tsgroupBox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
            dayradio.IsEnabled = true;
            iocradio.IsEnabled = true;

            Misradio.IsEnabled = true;
            CncRadio.IsEnabled = true;
            marketRadio.IsEnabled = true;
            LimitRadio.IsEnabled = true;
            slmRadio.IsEnabled = true;
            slmRadio.IsEnabled = true;
            textTP.IsEnabled = false;
            textPrice.IsEnabled = false;
        }
        private void bo_Click(object sender, RoutedEventArgs e)
        {
        	//pratiksha::16-oct-2020::call to close search box
            ForSearchlistClose();
            if (LimitRadio.IsChecked == true)
            {
                tpgroupbox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                tpgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                tpgroupbox.IsEnabled = false;
                dqgroupbox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                dqgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                dqgroupbox.IsEnabled = false;
                textTP.IsEnabled = false;
                textdp.IsEnabled = false;
                textPrice.IsEnabled = true;
            }
            else if (SlRadio.IsChecked == true)
            {
                tpgroupbox.Foreground = (Brush)bc.ConvertFrom("#FF777777");
                tpgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#FF777777");
                tpgroupbox.IsEnabled = true;
                dqgroupbox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                dqgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                dqgroupbox.IsEnabled = false;
                textdp.IsEnabled = false;
                textTP.IsEnabled = true;
            }
            StoplossgroupBox.Foreground = (Brush)bc.ConvertFrom("#FF777777");
            StoplossgroupBox.BorderBrush = (Brush)bc.ConvertFrom("#FF777777");
            StoplossgroupBox.IsEnabled = true;
            targettextBox.IsEnabled = true;
            sltextBox.IsEnabled = true;
            TargetgroupBox.Foreground = (Brush)bc.ConvertFrom("#FF777777");
            TargetgroupBox.BorderBrush = (Brush)bc.ConvertFrom("#FF777777");
            TargetgroupBox.IsEnabled = true;
            tstextBox.IsEnabled = true;
            tsgroupBox.Foreground = (Brush)bc.ConvertFrom("#FF777777");
            tsgroupBox.BorderBrush = (Brush)bc.ConvertFrom("#FF777777");
            tsgroupBox.IsEnabled = true;
            dayradio.IsEnabled = false;
            iocradio.IsEnabled = false;
            marketRadio.IsEnabled = false;
            marketRadio.IsChecked = false;
            LimitRadio.IsChecked = true;
            slmRadio.IsEnabled = false;
            SlRadio.IsEnabled = true;
            Misradio.IsEnabled = false;
            CncRadio.IsEnabled = false;
            tpgroupbox.IsEnabled = false;


            textTP.IsEnabled = false;
            textdp.IsEnabled = false;
            tpgroupbox.Header = "Trigger price";
        }
        private void co_Click(object sender, RoutedEventArgs e)
        {
        	//pratiksha::16-oct-2020::call to close search box
            ForSearchlistClose();
            sltextBox.IsEnabled = false;
            StoplossgroupBox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
            targettextBox.IsEnabled = false;
            TargetgroupBox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
            tstextBox.IsEnabled = false;
            tsgroupBox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
            dayradio.IsEnabled = false;
            iocradio.IsEnabled = false;
            Misradio.IsEnabled = false;
            CncRadio.IsEnabled = false;
            slmRadio.IsEnabled = false;
            SlRadio.IsEnabled = false;
            marketRadio.IsEnabled = true;
            tpgroupbox.Header = "Stoploss Trigger price";
            marketRadio.IsChecked = true;
            textTP.IsEnabled = true;
            textTP.IsEnabled = true;
            if (marketRadio.IsChecked == true)
            {
                pricegroupbox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                pricegroupbox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                pricegroupbox.IsEnabled = false;
                textTP.IsEnabled = false;
                dqgroupbox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                dqgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                dqgroupbox.IsEnabled = false;
                textdp.IsEnabled = false;
                tpgroupbox.IsEnabled = true;
                tpgroupbox.Foreground = (Brush)bc.ConvertFrom("#FF777777");
                tpgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#FF777777");
                textTP.IsEnabled = true;
                StoplossgroupBox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                StoplossgroupBox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                StoplossgroupBox.IsEnabled = false;
                TargetgroupBox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                TargetgroupBox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                TargetgroupBox.IsEnabled = false;
                tsgroupBox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                tsgroupBox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                tsgroupBox.IsEnabled = false;
            }
            if (LimitRadio.IsChecked == true)
            {
                pricegroupbox.Foreground = (Brush)bc.ConvertFrom("#FF777777");
                pricegroupbox.BorderBrush = (Brush)bc.ConvertFrom("#FF777777");
                pricegroupbox.IsEnabled = true;
                textTP.IsEnabled = true;
                dqgroupbox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                dqgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                dqgroupbox.IsEnabled = false;
                textdp.IsEnabled = false;
                tpgroupbox.IsEnabled = true;
                tpgroupbox.Foreground = (Brush)bc.ConvertFrom("#FF777777");
                tpgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#FF777777");
                textTP.IsEnabled = true;
                StoplossgroupBox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                StoplossgroupBox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                StoplossgroupBox.IsEnabled = false;
                TargetgroupBox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                TargetgroupBox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                TargetgroupBox.IsEnabled = false;
                tsgroupBox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                tsgroupBox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                tsgroupBox.IsEnabled = false;
            }
        }
        private void amo_Click(object sender, RoutedEventArgs e)
        {
        	//pratiksha::16-oct-2020::call to close search box
            ForSearchlistClose();
            StoplossgroupBox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
            StoplossgroupBox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
            StoplossgroupBox.IsEnabled = false;
            TargetgroupBox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
            TargetgroupBox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
            TargetgroupBox.IsEnabled = false;
            tsgroupBox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
            tsgroupBox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
            tsgroupBox.IsEnabled = false;

            Misradio.IsEnabled = true;
            CncRadio.IsEnabled = true;
            dayradio.IsEnabled = true;
            iocradio.IsEnabled = true;
            marketRadio.IsEnabled = true;
            LimitRadio.IsEnabled = true;
            SlRadio.IsEnabled = true;
            slmRadio.IsEnabled = true;
            if (marketRadio.IsChecked == true)
            {
                pricegroupbox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                pricegroupbox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                pricegroupbox.IsEnabled = false;
                textTP.IsEnabled = false;

                tpgroupbox.IsEnabled = false;
                tpgroupbox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                tpgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                // textTP.IsEnabled = false;

                dqgroupbox.Foreground = (Brush)bc.ConvertFrom("#FF777777");
                dqgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#FF777777");
                dqgroupbox.IsEnabled = true;
                textdp.IsEnabled = true;
            }
            else if (LimitRadio.IsChecked == true)
            {
                pricegroupbox.Foreground = (Brush)bc.ConvertFrom("#FF777777");
                pricegroupbox.BorderBrush = (Brush)bc.ConvertFrom("#FF777777");
                pricegroupbox.IsEnabled = true;
                textTP.IsEnabled = true;

                tpgroupbox.IsEnabled = false;
                tpgroupbox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                tpgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                textTP.IsEnabled = false;

                dqgroupbox.Foreground = (Brush)bc.ConvertFrom("#FF777777");
                dqgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#FF777777");
                dqgroupbox.IsEnabled = true;
                textdp.IsEnabled = true;
            }
            else if (SlRadio.IsChecked == true)
            {
                pricegroupbox.Foreground = (Brush)bc.ConvertFrom("#FF777777");
                pricegroupbox.BorderBrush = (Brush)bc.ConvertFrom("#FF777777");
                pricegroupbox.IsEnabled = true;
                textTP.IsEnabled = true;

                tpgroupbox.IsEnabled = true;
                tpgroupbox.Foreground = (Brush)bc.ConvertFrom("#FF777777");
                tpgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#FF777777");
                textTP.IsEnabled = true;

                dqgroupbox.Foreground = (Brush)bc.ConvertFrom("#FF777777");
                dqgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#FF777777");
                dqgroupbox.IsEnabled = true;
                textdp.IsEnabled = true;
            }
            if (slmRadio.IsChecked == true)
            {
                pricegroupbox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                pricegroupbox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                pricegroupbox.IsEnabled = false;
                textTP.IsEnabled = false;

                tpgroupbox.IsEnabled = true;
                tpgroupbox.Foreground = (Brush)bc.ConvertFrom("#FF777777");
                tpgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#FF777777");
                textTP.IsEnabled = true;

                dqgroupbox.Foreground = (Brush)bc.ConvertFrom("#FF777777");
                dqgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#FF777777");
                dqgroupbox.IsEnabled = true;
                textdp.IsEnabled = true;
            }
            sltextBox.IsEnabled = false;
            StoplossgroupBox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
            StoplossgroupBox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
            targettextBox.IsEnabled = false;
            TargetgroupBox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
            TargetgroupBox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
            tstextBox.IsEnabled = false;
            tsgroupBox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
            tsgroupBox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");

            tpgroupbox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
            tpgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
            tpgroupbox.Header = "Trigger price";
            textTP.IsEnabled = false;
            dqgroupbox.Foreground = (Brush)bc.ConvertFrom("#FF777777");
            dqgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#FF777777");
        }
        private void dayradio_Checked(object sender, RoutedEventArgs e)
        {
        	//pratiksha::16-oct-2020::call to close search box
            ForSearchlistClose();
            StoplossgroupBox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
            StoplossgroupBox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
            targettextBox.IsEnabled = false;
            TargetgroupBox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
            TargetgroupBox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
            tstextBox.IsEnabled = false;
            tsgroupBox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
            tsgroupBox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
            if (marketRadio.IsChecked == true)
            {
                pricegroupbox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                pricegroupbox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                pricegroupbox.IsEnabled = false;
                textTP.IsEnabled = false;

                tpgroupbox.IsEnabled = true;
                tpgroupbox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                tpgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                textTP.IsEnabled = false;

                dqgroupbox.Foreground = (Brush)bc.ConvertFrom("#FF777777");
                dqgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#FF777777");
                dqgroupbox.IsEnabled = true;
                textdp.IsEnabled = false;
                if (amoradio.IsChecked == true && regularradio.IsChecked == true)
                {
                    dqgroupbox.IsEnabled = true;
                    textdp.IsEnabled = true;
                }
            }
            else if (LimitRadio.IsChecked == true)
            {
                pricegroupbox.Foreground = (Brush)bc.ConvertFrom("#FF777777");
                pricegroupbox.BorderBrush = (Brush)bc.ConvertFrom("#FF777777");
                pricegroupbox.IsEnabled = true;
                textTP.IsEnabled = true;

                tpgroupbox.IsEnabled = true;
                tpgroupbox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                tpgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                textTP.IsEnabled = false;

                dqgroupbox.Foreground = (Brush)bc.ConvertFrom("#FF777777");
                dqgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#FF777777");
                dqgroupbox.IsEnabled = true;
                textdp.IsEnabled = true;
            }
            else if (slmRadio.IsChecked == true)
            {
                pricegroupbox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                pricegroupbox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                pricegroupbox.IsEnabled = false;
                textTP.IsEnabled = false;

                tpgroupbox.IsEnabled = true;
                tpgroupbox.Foreground = (Brush)bc.ConvertFrom("#FF777777");
                tpgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#FF777777");
                textTP.IsEnabled = true;

                dqgroupbox.IsEnabled = true;
                dqgroupbox.Foreground = (Brush)bc.ConvertFrom("#FF777777");
                dqgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#FF777777");
                textdp.IsEnabled = true;
            }
        }
        private void Icoradio_Checked(object sender, RoutedEventArgs e)
        {
        	//pratiksha::16-oct-2020::call to close search box
            ForSearchlistClose();
            StoplossgroupBox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
            StoplossgroupBox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
            targettextBox.IsEnabled = false;
            TargetgroupBox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
            TargetgroupBox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
            tstextBox.IsEnabled = false;
            tsgroupBox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
            tsgroupBox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
            if (marketRadio.IsChecked == true)
            {
                pricegroupbox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                pricegroupbox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                pricegroupbox.IsEnabled = false;
                textTP.IsEnabled = false;

                tpgroupbox.IsEnabled = false;
                tpgroupbox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                tpgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                textTP.IsEnabled = false;

                dqgroupbox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                dqgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                dqgroupbox.IsEnabled = false;
                textdp.IsEnabled = false;
            }
            else if (SlRadio.IsChecked == true)
            {
                pricegroupbox.Foreground = (Brush)bc.ConvertFrom("#FF777777");
                pricegroupbox.BorderBrush = (Brush)bc.ConvertFrom("#FF777777");
                //pricegroupbox.Cursor = System.Windows.Input.Cursors.Arrow;
                //textPrice.Cursor = System.Windows.Input.Cursors.Arrow;
                pricegroupbox.IsEnabled = true;
                textTP.IsEnabled = true;

                tpgroupbox.IsEnabled = true;
                tpgroupbox.Foreground = (Brush)bc.ConvertFrom("#FF777777");
                tpgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#FF777777");
                //tpgroupbox.Cursor = System.Windows.Input.Cursors.Arrow;
                //textTP.Cursor = System.Windows.Input.Cursors.Arrow;
                textTP.IsEnabled = true;

                dqgroupbox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                dqgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                dqgroupbox.IsEnabled = false;
                textdp.IsEnabled = false;
            }
            else if (LimitRadio.IsChecked == true)
            {
                pricegroupbox.Foreground = (Brush)bc.ConvertFrom("#FF777777");
                pricegroupbox.BorderBrush = (Brush)bc.ConvertFrom("#FF777777");
                //pricegroupbox.Cursor = System.Windows.Input.Cursors.Arrow;
                //textPrice.Cursor = System.Windows.Input.Cursors.Arrow;
                pricegroupbox.IsEnabled = true;
                textTP.IsEnabled = true;

                tpgroupbox.IsEnabled = false;
                tpgroupbox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                tpgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                textTP.IsEnabled = false;

                dqgroupbox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                dqgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                dqgroupbox.IsEnabled = false;
                textdp.IsEnabled = false;
            }
            else if (SlRadio.IsChecked == true)
            {
                pricegroupbox.Foreground = (Brush)bc.ConvertFrom("#FF777777");
                pricegroupbox.BorderBrush = (Brush)bc.ConvertFrom("#FF777777");
                //pricegroupbox.Cursor = System.Windows.Input.Cursors.Arrow;
                //textPrice.Cursor = System.Windows.Input.Cursors.Arrow;
                pricegroupbox.IsEnabled = true;
                textTP.IsEnabled = true;

                tpgroupbox.IsEnabled = true;
                tpgroupbox.Foreground = (Brush)bc.ConvertFrom("#FF777777");
                tpgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#FF777777");
                //tpgroupbox.Cursor = System.Windows.Input.Cursors.Arrow;
                //textTP.Cursor = System.Windows.Input.Cursors.Arrow;
                textTP.IsEnabled = true;

                dqgroupbox.Foreground = (Brush)bc.ConvertFrom("#FF777777");
                dqgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#FF777777");
                //dqgroupbox.Cursor = System.Windows.Input.Cursors.Arrow;
                //textdp.Cursor = System.Windows.Input.Cursors.Arrow;
                dqgroupbox.IsEnabled = true;
                textdp.IsEnabled = true;
            }
            else if (slmRadio.IsChecked == true)
            {
                pricegroupbox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                pricegroupbox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                pricegroupbox.IsEnabled = false;
                textPrice.IsEnabled = false;

                tpgroupbox.IsEnabled = true;
                tpgroupbox.Foreground = (Brush)bc.ConvertFrom("#FF777777");
                tpgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#FF777777");
                //tpgroupbox.Cursor = System.Windows.Input.Cursors.Arrow;
                //textTP.Cursor = System.Windows.Input.Cursors.Arrow;
                textTP.IsEnabled = true;

                dqgroupbox.Foreground = (Brush)bc.ConvertFrom("#E7E7E7");
                dqgroupbox.BorderBrush = (Brush)bc.ConvertFrom("#E7E7E7");
                dqgroupbox.IsEnabled = false;
                textdp.IsEnabled = false;
            }
        }
        private void btnBuy_Click(object sender, RoutedEventArgs e)
        {
            try
            {
            	//pratiksha::16-oct-2020::call to close search box
                ForSearchlistClose();
                //IRDSPM::PRatiksha::25-9-2020::For taking only symbol name
                TradingSymbol = selectedSymbolFromUser;
                if (BSERadio.IsChecked == true)
                {
                    Exchange = BSERadio.Content.ToString();
                }
                else if (NSERadio.IsChecked == true)
                {
                    Exchange = NSERadio.Content.ToString();
                }

                if (marketRadio.IsChecked == true)
                {
                    OrderType = marketRadio.Content.ToString();
                }
                else if (LimitRadio.IsChecked == true)
                {
                    OrderType = LimitRadio.Content.ToString();
                }
                else if (SlRadio.IsChecked == true)
                {
                    OrderType = SlRadio.Content.ToString();
                }
                else if (slmRadio.IsChecked == true)
                {
                    OrderType = slmRadio.Content.ToString();
                }
                

                if (buyradio.IsChecked == true)
                {
                    TransactionType = buyradio.Content.ToString();
                }
                else if (sellradio.IsChecked == true)
                {
                    TransactionType = sellradio.Content.ToString();
                }
                Quantity = textQty.Text;
                if (Misradio.IsChecked == true)
                {
                    Product = Misradio.Content.ToString();
                }
                else if (CncRadio.IsChecked == true)
                {
                    Product = CncRadio.Content.ToString();
                }

                if (dayradio.IsChecked == true)
                {
                    Validity = dayradio.Content.ToString();
                }
                else if (iocradio.IsChecked == true)
                {
                    Validity = iocradio.Content.ToString();
                }

                if (regularradio.IsChecked == true)
                {
                    Variety = regularradio.Content.ToString();
                }
                else if (boradio.IsChecked == true)
                {
                    Variety = boradio.Content.ToString();
                }
                else if (coradio.IsChecked == true)
                {
                    Variety = coradio.Content.ToString();
                }
                else if (amoradio.IsChecked == true)
                {
                    Variety = amoradio.Content.ToString();
                }
                Price = textPrice.Text.ToString();                
                DisClosedQuantity = textdp.Text.ToString();
                TriggerPrice = textTP.Text.ToString();
                StopLossValue = sltextBox.Text.ToString();
                TrailingStoploss = tstextBox.Text.ToString();
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                if ((Exchange.Length > 0) && (TradingSymbol.Length > 0) && (TransactionType.Length > 0) && (Quantity.Length > 0) && (OrderType.Length > 0) && (Product.Length > 0) && (Validity.Length > 0) && (Variety.Length > 0))
                {
                    string message = "Are you sure you want to place order for below Details:\nExchange : " + Exchange + "\nTradingSymbol : " + TradingSymbol +
                        "\nTransactionType : " + TransactionType + "\nQuantity : " + Quantity + "\nOrderType : " + OrderType +
                        "\nProduct : " + Product + "\nValidity : " + Validity + "\nVariety : " + Variety;

                    DialogResult dialog = System.Windows.Forms.MessageBox.Show(new Form { TopMost = true },message, m_title, buttons, MessageBoxIcon.Information);
                    if (dialog == DialogResult.Yes)
                    {
                        if (m_kiteConnectWrapper != null)
                        {
                            //sanika::11-sep-2020::write seprate function for place order for combinations for ordertype and variety
                            PlaceOrder();
                           // System.Windows.Forms.MessageBox.Show("Code commented");
                        }
                        m_BuyWindowsForm.Close();
                    }
                }
                else
                {
                    string message = "Please enter proper values.";
                    DialogResult dialog = System.Windows.Forms.MessageBox.Show(message, m_title, buttons, MessageBoxIcon.Error);
                }
            }
            catch(Exception er)
            {
                logger.LogMessage("btnBuy_Click : Exception Error Message = "+er.Message, MessageType.Exception);
            }
        }

        private void BSERadio_Click(object sender, RoutedEventArgs e)
        {
        	//pratiksha::16-oct-2020::call to close search box
            ForSearchlistClose();
            SymbolName.Content = selectedSymbolFromUser + "  " + BSERadio.Content + " x " + textQty.Text + " Qty";
        }

        private void NSERadio_Click(object sender, RoutedEventArgs e)
        {
        	//pratiksha::16-oct-2020::call to close search box
            ForSearchlistClose();
            SymbolName.Content = selectedSymbolFromUser + "  " + NSERadio.Content + " x " + textQty.Text + " Qty";
        }

        private void textQty_TextChanged(object sender, TextChangedEventArgs e)
        {
        	//pratiksha::16-oct-2020::call to close search box
            ForSearchlistClose();
            if (BSERadio.IsChecked == true)
            {
                SymbolName.Content = selectedSymbolFromUser + "  " + BSERadio.Content + " x " + textQty.Text + " Qty";
        }
            if (NSERadio.IsChecked == true)
        {
                SymbolName.Content = selectedSymbolFromUser + "  " + NSERadio.Content + " x " + textQty.Text + " Qty";
        }
        }
        private void m_SearchTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            listBox.Items.Clear();
            try
        {
                listBox.Visibility = Visibility.Visible;
                foreach (var symbolname in FinalSymbolList)
            {
                    if (symbolname.ToString().ToLower().Contains(m_SearchTextBox.Text.ToLower()))
                {
                        listBox.Items.Add(symbolname);
                }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            if(m_SearchTextBox.Text.Length == 0)
        {
                listBox.Visibility = Visibility.Hidden;
        }
        }
        private void listBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (listBox.SelectedItem != null)
                {
                selectedSymbolFromUser = listBox.SelectedItem.ToString();
                m_SearchTextBox.Text = selectedSymbolFromUser;
                if (BSERadio.IsChecked == true)
                {
                    SymbolName.Content = selectedSymbolFromUser + "  " + BSERadio.Content + " x " + textQty.Text + " Qty";
                }
                if (NSERadio.IsChecked == true)
                {
                    SymbolName.Content = selectedSymbolFromUser + "  " + NSERadio.Content + " x " + textQty.Text + " Qty";
                }
                listBox.Visibility = Visibility.Hidden;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        //sanika::11-sep-2020::write seprate function for place order for combinations for ordertype and variety
        public void  PlaceOrder()
        {           
            if (OrderType == Constants.ORDER_TYPE_MARKET && Variety.ToLower() == Constants.VARIETY_REGULAR)
            {
                m_kiteConnectWrapper.PlaceOrder(Exchange: Exchange,
                                                TradingSymbol: TradingSymbol,
                                                TransactionType: TransactionType,
                                                Quantity: Convert.ToInt32(Quantity),
                                                OrderType: OrderType,
                                                Price: null,
                                                Product: Product,
                                                Validity: Validity,
                                                DisclosedQuantity: null,
                                                TriggerPrice: null,
                                                SquareOffValue: null,
                                                StoplossValue: null,
                                                TrailingStoploss: null,
                                                Variety: Variety.ToLower());
            }
            else if(OrderType == Constants.ORDER_TYPE_LIMIT && Variety.ToLower() == Constants.VARIETY_REGULAR)
            {
                m_kiteConnectWrapper.PlaceOrder(Exchange: Exchange,
                                                TradingSymbol: TradingSymbol,
                                                TransactionType: TransactionType,
                                                Quantity: Convert.ToInt32(Quantity),
                                                OrderType: OrderType,
                                                Price: Convert.ToDecimal(Price),
                                                Product: Product,
                                                Validity: Validity,
                                                DisclosedQuantity: null,
                                                TriggerPrice: null,
                                                SquareOffValue: null,
                                                StoplossValue: null,
                                                TrailingStoploss: null,
                                                Variety: Variety.ToLower());
            }
            else if(OrderType == Constants.ORDER_TYPE_SL && Variety.ToLower() == Constants.VARIETY_REGULAR)
            {
                m_kiteConnectWrapper.PlaceOrder(Exchange: Exchange,
                                                TradingSymbol: TradingSymbol,
                                                TransactionType: TransactionType,
                                                Quantity: Convert.ToInt32(Quantity),
                                                OrderType: OrderType,
                                                Price: Convert.ToDecimal(Price),
                                                Product: Product,
                                                Validity: Validity,
                                                DisclosedQuantity: null,
                                                TriggerPrice: Convert.ToDecimal(TriggerPrice),
                                                SquareOffValue: null,
                                                StoplossValue: null,
                                                TrailingStoploss: null,
                                                Variety: Variety.ToLower());
            }
            else if(OrderType == Constants.ORDER_TYPE_SLM && Variety.ToLower() == Constants.VARIETY_REGULAR)
            {
                m_kiteConnectWrapper.PlaceOrder(Exchange: Exchange,
                                                TradingSymbol: TradingSymbol,
                                                TransactionType: TransactionType,
                                                Quantity: Convert.ToInt32(Quantity),
                                                OrderType: OrderType,
                                                Price: null,
                                                Product: Product,
                                                Validity: Validity,
                                                DisclosedQuantity: null,
                                                TriggerPrice: Convert.ToDecimal(TriggerPrice),
                                                SquareOffValue: null,
                                                StoplossValue: null,
                                                TrailingStoploss: null,
                                                Variety: Variety.ToLower());
            }
            else if (OrderType == Constants.ORDER_TYPE_LIMIT && Variety.ToLower() == Constants.VARIETY_BO)
            {
                m_kiteConnectWrapper.PlaceOrder(Exchange: Exchange,
                                                TradingSymbol: TradingSymbol,
                                                TransactionType: TransactionType,
                                                Quantity: Convert.ToInt32(Quantity),
                                                OrderType: OrderType,
                                                Price: Convert.ToDecimal(Price),
                                                Product: Product,                                               
                                                DisclosedQuantity: null,
                                                TriggerPrice: null,
                                                SquareOffValue: null,
                                                StoplossValue: Convert.ToDecimal(StopLossValue),
                                                TrailingStoploss: Convert.ToDecimal(TrailingStoploss),
                                                Variety: Variety.ToLower());
            }
            else if (OrderType == Constants.ORDER_TYPE_SL && Variety.ToLower() == Constants.VARIETY_BO)
            {
                m_kiteConnectWrapper.PlaceOrder(Exchange: Exchange,
                                                TradingSymbol: TradingSymbol,
                                                TransactionType: TransactionType,
                                                Quantity: Convert.ToInt32(Quantity),
                                                OrderType: OrderType,
                                                Price: Convert.ToDecimal(Price),
                                                Product: Product,
                                                DisclosedQuantity: null,
                                                TriggerPrice: Convert.ToDecimal(TriggerPrice),
                                                SquareOffValue: null,
                                                StoplossValue: Convert.ToDecimal(StopLossValue),
                                                TrailingStoploss: Convert.ToDecimal(TrailingStoploss),
                                                Variety: Variety.ToLower());
            }

            else if (OrderType == Constants.ORDER_TYPE_MARKET && Variety.ToLower() == Constants.VARIETY_CO)
            {
                m_kiteConnectWrapper.PlaceOrder(Exchange: Exchange,
                                                TradingSymbol: TradingSymbol,
                                                TransactionType: TransactionType,
                                                Quantity: Convert.ToInt32(Quantity),
                                                OrderType: OrderType,
                                                Price: null,
                                                Product: Product,
                                                DisclosedQuantity: null,
                                                TriggerPrice: Convert.ToDecimal(TriggerPrice),
                                                SquareOffValue: null,
                                                StoplossValue: null,
                                                TrailingStoploss: null,
                                                Variety: Variety.ToLower());
            }
            else if (OrderType == Constants.ORDER_TYPE_LIMIT && Variety.ToLower() == Constants.VARIETY_CO)
            {
                m_kiteConnectWrapper.PlaceOrder(Exchange: Exchange,
                                                TradingSymbol: TradingSymbol,
                                                TransactionType: TransactionType,
                                                Quantity: Convert.ToInt32(Quantity),
                                                OrderType: OrderType,
                                                Price: Convert.ToDecimal(Price),
                                                Product: Product,
                                                DisclosedQuantity: null,
                                                TriggerPrice: Convert.ToDecimal(TriggerPrice),
                                                SquareOffValue: null,
                                                StoplossValue: null,
                                                TrailingStoploss: null,
                                                Variety: Variety.ToLower());
            }

        }
        
        //IRDSPM::Pratiksha::08-10-2020::Close the form
        private void Grid_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if(e.Key == Key.Escape)
            {
                m_BuyWindowsForm.Close();
            }
        }
        //IRDSPM::Pratiksha::08-10-2020::oto solve issue 2nd time click on working, so add tab event
        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            //IRDSPM::Pratiksha::08-10-2020::Should clear all values
            CncRadio.IsChecked = true;
            marketRadio.IsChecked = true;
            regularradio.IsChecked = true;
            dayradio.IsChecked = true;
           // buyradio.IsChecked = true;
            NSERadio.IsChecked = true;
            textQty.Text = "1";
            textdp.Text = "0";
            m_SearchTextBox.Text = "";
            //IRDSPM::Pratiksha::23-12-2020::For clearing all values
            textPrice.Text = "0";
            textTP.Text = "0";
            textdp.Text = "0";
            sltextBox.Text = "0";
            targettextBox.Text = "0";
            tstextBox.Text = "0";
            SendKeys.Send("{Tab}");
        }

        //IRDSPM::Pratiksha::16-10-2020::did change for searchlist should disable when click on other places
        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (listBox.Visibility == Visibility.Visible)
            {
                if (listBox.SelectedItem == null)
                {
                    listBox.Visibility = Visibility.Hidden;
                }
            }
        }
        public void ForSearchlistClose()
        {
            try
            {
                if (listBox != null)
                {
                    if (listBox.Visibility == Visibility.Visible)
                    {
                        listBox.Visibility = Visibility.Hidden;
                    }
                }
            }
            catch(Exception e)
            {
                logger.LogMessage("ForSearchlistClose : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }
    }
}
