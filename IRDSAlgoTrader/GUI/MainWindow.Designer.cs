﻿using IRDSAlgoOMS.GUI;
using System;
using System.IO;
using System.Reflection;

namespace IRDSAlgoOMS
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent(string title)
        {
            //System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            //IRDSPM::Pratiksha::12-02-2021::dynamically set icon
            Assembly myAssembly = Assembly.GetExecutingAssembly();
            Stream IconStream = null;
            try
            {
                if (title != "")
                {
                    IconStream = myAssembly.GetManifestResourceStream("IRDSAlgoOMS.Resources." + title + ".ico");
                    if (title.ToLower() == "irds algo trader")
                    {
                        this.Text = title + " 1.0.1";
                    }
                    else
                    {
                        this.Text = title;
                    }
                }
                else
                {
                    title = "IRDS Algo Trader";
                    IconStream = myAssembly.GetManifestResourceStream("IRDSAlgoOMS.Resources." + title + ".ico");
                }
                this.Icon = new System.Drawing.Icon(IconStream);
            }
            catch (Exception ex)
            {
            }
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorker2 = new System.ComponentModel.BackgroundWorker();
            this.elementHost1 = new System.Windows.Forms.Integration.ElementHost();
            this.wpfMainControl1 = new IRDSAlgoOMS.WPFMainControl(title);
            this.SuspendLayout();
            // 
            // elementHost1
            // 
            this.elementHost1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.elementHost1.Location = new System.Drawing.Point(0, 0);
            this.elementHost1.Name = "elementHost1";
            this.elementHost1.Size = new System.Drawing.Size(884, 561);
            this.elementHost1.TabIndex = 0;
            this.elementHost1.Text = "elementHost1";
            this.elementHost1.Child = this.wpfMainControl1;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 561);
            this.Controls.Add(this.elementHost1);
            this.Name = "MainWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            
            this.Resize += new System.EventHandler(this.MainWindow_Resize);
            this.ResumeLayout(false);

        }

        #endregion

        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.ComponentModel.BackgroundWorker backgroundWorker2;
        private System.Windows.Forms.Integration.ElementHost elementHost1;
        private WPFMainControl wpfMainControl1;
    }
}