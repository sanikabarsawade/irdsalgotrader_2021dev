﻿using System.IO;
using System.Reflection;

namespace IRDSAlgoOMS.GUI
{
    partial class TradingSymbolWindowForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /// 
        //IRDSPM::Pratiksha::12-02-2021::dynamically set icon
        Assembly myAssembly = Assembly.GetExecutingAssembly();
        Stream IconStream = null;
        private void InitializeComponent(dynamic kiteConnectWrapper, TradingSymbolWindowForm obj, string title)
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.elementHost2 = new System.Windows.Forms.Integration.ElementHost();
            this.tradingSymbolWPFForm2 = new IRDSAlgoOMS.GUI.TradingSymbolWPFForm(kiteConnectWrapper, obj, title);
            this.SuspendLayout();
            // 
            // elementHost2
            // 
            this.elementHost2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.elementHost2.Location = new System.Drawing.Point(0, 0);
            this.elementHost2.Name = "elementHost2";
            this.elementHost2.Size = new System.Drawing.Size(525, 575);
            this.elementHost2.TabIndex = 0;
            this.elementHost2.Text = "elementHost2";
            this.elementHost2.Child = this.tradingSymbolWPFForm2;
            // 
            // TradingSymbolWindowForm
            // 
            this.ClientSize = new System.Drawing.Size(525, 575);
            //IRDSPM::PRatiksha::12-02-2020::Icon changes
            if (title.ToLower() == "irds algo trader")
            {
                this.Text = title + " 1.0.1";
            }
            else
            {
                this.Text = title;
            }
            IconStream = myAssembly.GetManifestResourceStream("IRDSAlgoOMS.Resources." + title + ".ico");
            this.Icon = new System.Drawing.Icon(IconStream);
            this.Controls.Add(this.elementHost2);
            this.Name = "TradingSymbolWindowForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Trading Setting";
            //IRDSPM::Pratiksha::1-10-2020::For maximized box
            this.MaximizeBox = false;
            this.ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Integration.ElementHost elementHost1;
        private TradingSymbolWPFForm tradingSymbolWPFForm1;
        private System.Windows.Forms.Integration.ElementHost elementHost2;
        private TradingSymbolWPFForm tradingSymbolWPFForm2;
    }
}