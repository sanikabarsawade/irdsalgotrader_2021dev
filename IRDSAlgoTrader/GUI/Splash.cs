﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IRDSAlgoOMS.GUI
{
    public partial class Splash : Form
    {
        public Splash()
        {
            InitializeComponent();
            //IRDSPM::Pratiksha::03-12-2020::Reading values from text file
            string m_FilePath = Directory.GetCurrentDirectory() + @"\Configuration\" + "buildDet.txt";
            if (File.Exists(m_FilePath))
            {
                string lastLine = System.IO.File.ReadLines(m_FilePath).Last();
                if (lastLine.Length > 0)
                {
                    string[] splittedLine = lastLine.Split(' ');
                    label4.Text = "Build Date: "+ splittedLine[0];
                    label3.Text = "Version: "+splittedLine[1];
                }
            }
            else
            {
                //WriteUniquelogs("ExecutionForm" + " " + m_UserID, "LoadInformationStatus : " + m_FilePath + " file does not exist", MessageType.Informational);
            }
            label3.Hide();
            label4.Hide();
        }
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x80;
                return cp;
            }
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            label2.Location = new Point(label2.Location.X, label2.Location.Y - 5);
            if (this.label2.Location.Y < 129)
            {
                label2.Hide();
            }
            if (this.label2.Location.Y < 250)
            {
                label3.Show();
                label3.Location = new Point(label3.Location.X, label3.Location.Y - 5);
            }
            if (this.label3.Location.Y < 129)
            {
                label3.Hide();
            }
            if (this.label3.Location.Y < 250)
            {
                label4.Show();
                label4.Location = new Point(label4.Location.X, label4.Location.Y - 5);
            }
            if (this.label4.Location.Y < 129)
            {
                label4.Hide();
            }
        }

        private void Splash_Load(object sender, EventArgs e)
        {
            timer1.Start();
        }
    }
}
