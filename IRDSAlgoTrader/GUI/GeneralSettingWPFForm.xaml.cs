﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Media;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace IRDSAlgoOMS.GUI
{
    public partial class GeneralSettingWPFForm : System.Windows.Controls.UserControl
    {
        GeneralSettingWindowForm m_generalSettingObj;
        string iniFileSetting;
        bool toastNotificationVal = false;
        bool toastAudioVal = false;
        bool showGUIVal = false;
        ConfigSettings Settings;
        Logger logger = Logger.Instance;
        string maximizedScreenVal = "";
        string message = "";
        MessageBoxButtons buttons;
        DialogResult dialog;
        //IRDSPM::Pratiksha::16-09-2020::For MArket watch remember me
        string remembermemwVal = "";
        string mwsymbolnameVal = "";
        string mwltpval = "";
        string mwbidval = "";
        string mwaskval = "";
        string mwvolumeval = "";
        //IRDSPM::Pratiksha::24-09-2020::for newly added variable
        string mwOpen = "";
        string mwHigh = "";
        string mwLow = "";
        string mwClose = "";
        string mwchangeinper = "";
        //IRDSPM::Pratiksha::08-10-2020::Added new column "Net Change"
        string mwNetChange = "";

        //IRDSPM::Pratiksha::18-11-2020::for current values
        string CurrentremembermemwVal = "";
        string CurrentmwsymbolnameVal = "";
        string Currentmwltpval = "";
        string Currentmwbidval = "";
        string Currentmwaskval = "";
        string Currentmwvolumeval = "";
        string CurrentmwOpen = "";
        string CurrentmwHigh = "";
        string CurrentmwLow = "";
        string CurrentmwClose = "";
        string Currentmwchangeinper = "";
        string CurrentmwNetChange = "";

        //IRDSPM::Pratiksha::16-09-2020::For reading Open position
        string opExchange = "";
        string opSymbol = "";
        string opProduct = "";
        string opQuantity = "";
        string opLtp = "";
        string opPrice = "";
        string opPL = "";
        //IRDSPM::Pratiksha::10-11-2020::for current values
        string CurrentopExchange = "";
        string CurrentopSymbol = "";
        string CurrentopProduct = "";
        string CurrentopQuantity = "";
        string CurrentopLtp = "";
        string CurrentopPrice = "";
        string CurrentopPL = "";

        //IRDSPM::Pratiksha::16-09-2020::For reading Order Book
        string obEXG = "";
        string obSymbol = "";
        string obOrderId = "";
        string obTime = "";
        string obType = "";
        string obOrderType = "";
        string obProduct = "";
        string obPrice = "";
        string obTriggerPrice = "";
        string obQTY = "";
        string obStatus = "";
        string obFilledQty = "";
        string obFilledPrice = "";
        string obRejectionOrderRemark = "";

        //IRDSPM::Pratiksha::10-11-2020::for current values
        string CurrentobEXG = "";
        string CurrentobSymbol = "";
        string CurrentobOrderId = "";
        string CurrentobTime = "";
        string CurrentobType = "";
        string CurrentobOrderType = "";
        string CurrentobProduct = "";
        string CurrentobPrice = "";
        string CurrentobTriggerPrice = "";
        string CurrentobQTY = "";
        string CurrentobStatus = "";
        string CurrentobFilledQty = "";
        string CurrentobFilledPrice = "";
        string CurrentobRejectionOrderRemark = "";

        //Open Holdings
        string ohEXG = "";
        string ohSymbol = "";
        string ohProduct = "";
        string ohQuantity = "";
        string ohcloseprice = "";
        string ohavgprice = "";
        string ohPL = "";

        //IRDSPM::Pratiksha::10-11-2020::for current values
        string CurrentohEXG = "";
        string CurrentohSymbol = "";
        string CurrentohProduct = "";
        string CurrentohQuantity = "";
        string Currentohcloseprice = "";
        string Currentohavgprice = "";
        string CurrentohPL = "";

        //IRDSPM::Pratiksha::21-10-2020::For getting audio file path
        string path = Directory.GetCurrentDirectory();
        SoundPlayer simpleSound = null;
        string getNotificationSound = "";
        //IRDSPM::Pratiksha::18-11-2020::For cancel click
        bool CurrentShowGUI = false;
        bool currenttoastNoti = false;
        bool currentToastAudio = false;
        string currentmaximizedScreen = "";
        string currentNotificationTone = "";

        //IRDSPM::Pratiksha::01-02-2021::For notifications
        bool isSend = false;
        bool isSmsEnable = false;
        bool isEmailEnable = false;
        string smsNumber = "";
        string emailid = "";
        string interval = "";
        //03-AUG-2021::Pratiksha::FOr other notification field
        bool OtherNotification = false;
        bool currentOtherNotification = false;
        bool CurrentisSend = false;
        bool CurrentisSmsEnable = false;
        bool CurrentisEmailEnable = false;
        dynamic m_ObjkiteConnectWrapper = null;
        string title = "";

        string senderName = "";
        string senderemail = "";
        string senderpassword = "";
        string senderhost = "";
        string senderPort = "";

        string CurrentSendername = "";
        string CurrentSenderemail = "";
        string CurrentSenderpassword = "";
        string CurrentSenderhost = "";
        string CurrentSenderport = "";

        public GeneralSettingWPFForm(GeneralSettingWindowForm generalObj, dynamic m_ObjkiteConnectWrapper1)
        {
            InitializeComponent();
            m_generalSettingObj = generalObj;
            m_ObjkiteConnectWrapper = m_ObjkiteConnectWrapper1;
            var path = Directory.GetCurrentDirectory();
            radioButtonMarketWatch.IsChecked = true;
            iniFileSetting = path + @"\Configuration\" + "Setting.ini";
            try
            {
                if (File.Exists(iniFileSetting))
                {
                    Settings = new ConfigSettings(logger);
                    Settings.ReadSettingConfigFile(iniFileSetting);
                    toastNotificationVal = Settings.toastNotification;
                    currenttoastNoti = toastNotificationVal;
                    toastAudioVal = Settings.toastAudio;
                    currentToastAudio = toastAudioVal;
                    showGUIVal = Settings.isShowGUI;
                    CurrentShowGUI = showGUIVal;
                    maximizedScreenVal = Settings.maximizedScreen;
                    currentmaximizedScreen = maximizedScreenVal;
                    OtherNotification = Settings.isOtherNotification;
                    currentOtherNotification = OtherNotification;
                    RefreshColumnSetupSettinginiFile();
                }
                if(OtherNotification == true)
                {
                    RBOtherNotificationTrue.IsChecked = true;
                }
                else
                {
                    RBOtherNotificationFalse.IsChecked = true;
                }
                if (toastNotificationVal == true)
                {
                    radioButtonnotificationTrue.IsChecked = true;
                    radioButtonAudioTrue.IsEnabled = true;
                    radioButtonAudioFalse.IsEnabled = true;
                }
                else
                {
                    radioButtonnotificationFalse.IsChecked = true;
                    radioButtonAudioTrue.IsEnabled = false;
                    radioButtonAudioFalse.IsEnabled = false;
                }
                if (toastAudioVal == true)
                {
                    radioButtonAudioTrue.IsChecked = true;
                    //IRDSPM::PRatiksha::21-10-2020::For sound grid check
                    lblSoundStatus.Content = "ON";
                    lblDesc.Visibility = Visibility.Hidden;
                    lbldesc2.Visibility = Visibility.Hidden;
                    gridRing1.IsEnabled = true;
                    gridRing2.IsEnabled = true;
                    gridRing3.IsEnabled = true;
                    gridRing4.IsEnabled = true;
                }
                else
                {
                    radioButtonAudioFalse.IsChecked = true;
                    //IRDSPM::PRatiksha::21-10-2020::For sound grid check
                    lblSoundStatus.Content = "OFF";
                    lblDesc.Visibility = Visibility.Visible;
                    lbldesc2.Visibility = Visibility.Visible;
                    gridRing1.IsEnabled = false;
                    gridRing2.IsEnabled = false;
                    gridRing3.IsEnabled = false;
                    gridRing4.IsEnabled = false;
                }
                if (CurrentShowGUI == true)
                {
                    radioButtonshowuiTrue.IsChecked = true;
                }
                else
                {
                    radioButtonshowuiFalse.IsChecked = true;
                }

                if (gridScreenRight.Visibility == Visibility.Visible)
                {
                    if (currentmaximizedScreen == "Maximized")
                    {
                        radioButtonMaximizedTrue.IsChecked = true;
                    }
                    else
                    {
                        radioButtonMAximizedFalse.IsChecked = true;
                    }
                }
                //IRDSPM::PRatiksha::21-10-2020::For sound path
                SoundDetails();
                //IRDSPM::Pratiksha::01-02-2021::For notifications
                GetNotificationDetails();
                listInterval.Items.Add("1");
                listInterval.Items.Add("2");
                listInterval.Items.Add("3");
                listInterval.Items.Add("5");
                listInterval.Items.Add("10");
                listInterval.Items.Add("30");
                listInterval.Items.Add("60");
                SMTPDetails();
                WriteUniquelogs("settingLogs" + " ", "GeneralSettingWPFForm :  Details loaded properly from settings.ini file.", MessageType.Informational);
            }
            catch (Exception ex)
            {
                Trace.WriteLine("exception gen ********* " + ex.Message);
                WriteUniquelogs("settingLogs" + " ", "GeneralSettingWPFForm :  Exception Error Message =  " + ex.Message, MessageType.Informational);
            }
        }

        //IRDSPM::Pratiksha::01-02-2021::For notifications
        public void GetNotificationDetails()
        {
            isSend = Settings.isSend;
            CurrentisSend = isSend;
            isSmsEnable = Settings.isSmsEnable;
            CurrentisSmsEnable = isSmsEnable;
            isEmailEnable = Settings.isEmailEnable;
            CurrentisEmailEnable = isEmailEnable;
            smsNumber = Settings.smsNumber;
            emailid = Settings.emailid;
            interval = Settings.sendinterval;
            if (smsNumber.Contains("+91"))
            {
                string num = smsNumber.Substring(3);
                txtNumber.Text = num;
            }
            else
            {
                txtNumber.Text = smsNumber;
            }
            txtEmail.Text = emailid;
            listInterval.SelectedValue = interval;
            if (isSend == true)
            {
                radioButtonserviceTrue.IsChecked = true;
                if (isSmsEnable == true)
                {
                    CheckSms.IsChecked = true;
                    smsGroupbox.IsEnabled = true;
                }
                else
                {
                    CheckSms.IsChecked = false;
                    smsGroupbox.IsEnabled = false;
                }
                if (isEmailEnable == true)
                {
                    CheckEmail.IsChecked = true;
                    EmailGroupbox.IsEnabled = true;
                }
                else
                {
                    CheckEmail.IsChecked = false;
                    EmailGroupbox.IsEnabled = false;
                }
            }
            else
            {
                radioButtonserviceFalse.IsChecked = true;
                smsGroupbox.IsEnabled = false;
                EmailGroupbox.IsEnabled = false;
                IntervalGroupbox.IsEnabled = false;
                CheckSms.IsEnabled = false;
                CheckEmail.IsEnabled = false;
            }
            title = Settings.m_Title;
        }
        private void SMTPDetails()
        {
            if (File.Exists(iniFileSetting))
            {
                try
                {
                    if (Settings == null)
                    {
                        Settings = new ConfigSettings(logger);
                    }
                    Settings.ReadSettingConfigFile(iniFileSetting);
                    senderName = Settings.m_senderName;
                    senderemail = Settings.m_senderemail;
                    senderpassword = Settings.m_senderpassword;
                    senderhost = Settings.m_senderhost;
                    senderPort = Settings.m_senderPort;
                    txtsenderName.Text = senderName;
                    txtsenderemail.Text = senderemail;
                    txtsenderpassword.Text = senderpassword;
                    txtsenderhost.Text = senderhost;
                    txtsenderPort.Text = senderPort;
                }
                catch (Exception ex)
                {
                    WriteUniquelogs("ExecutionForm" + " ", "SMTPDetails :Exception Error Message: " + ex.Message, MessageType.Exception);
                }
            }

            else
            {
                WriteUniquelogs("ExecutionForm" + " ", "SMTPDetails : Setting.ini file not found.", MessageType.Informational);
            }
        }
        public void SoundDetails()
        {
            getNotificationSound = Settings.toastAudioPath;
            currentNotificationTone = getNotificationSound;
            if (getNotificationSound.Contains("Windows Foreground"))
            {
                Ring1PlaySelected.Visibility = Visibility.Visible;
                Ring2PlaySelected.Visibility = Visibility.Hidden;
                Ring3PlaySelected.Visibility = Visibility.Hidden;
                Ring4PlaySelected.Visibility = Visibility.Hidden;
            }
            else if (getNotificationSound.Contains("Windows Ding"))
            {
                Ring1PlaySelected.Visibility = Visibility.Hidden;
                Ring2PlaySelected.Visibility = Visibility.Visible;
                Ring3PlaySelected.Visibility = Visibility.Hidden;
                Ring4PlaySelected.Visibility = Visibility.Hidden;
            }
            else if (getNotificationSound.Contains("Windows Notify Messaging"))
            {
                Ring1PlaySelected.Visibility = Visibility.Hidden;
                Ring2PlaySelected.Visibility = Visibility.Hidden;
                Ring3PlaySelected.Visibility = Visibility.Visible;
                Ring4PlaySelected.Visibility = Visibility.Hidden;
            }
            else if (getNotificationSound.Contains("Windows Proximity Notification"))
            {
                Ring1PlaySelected.Visibility = Visibility.Hidden;
                Ring2PlaySelected.Visibility = Visibility.Hidden;
                Ring3PlaySelected.Visibility = Visibility.Hidden;
                Ring4PlaySelected.Visibility = Visibility.Visible;
            }
        }
        public void RefreshColumnSetupSettinginiFile()
        {
            if (File.Exists(iniFileSetting))
            {
                try
                {
                    Settings = new ConfigSettings(logger);
                    Settings.ReadSettingConfigFile(iniFileSetting);
                    //market watch
                    mwsymbolnameVal = Settings.mwsymbolname;
                    mwltpval = Settings.mwltp;
                    mwbidval = Settings.mwbid;
                    mwaskval = Settings.mwask;
                    mwvolumeval = Settings.mwvolume;
                    mwOpen = Settings.mwOpen;
                    mwHigh = Settings.mwHigh;
                    mwLow = Settings.mwLow;
                    mwClose = Settings.mwClose;
                    mwchangeinper = Settings.mwchangeinper;
                    //IRDSPM::Pratiksha::08-10-2020::Added new column "Net Change"
                    mwNetChange = Settings.mwNetChange;

                    CurrentmwsymbolnameVal = mwsymbolnameVal;
                    Currentmwltpval = mwltpval;
                    Currentmwbidval = mwbidval;
                    Currentmwaskval = mwaskval;
                    Currentmwvolumeval = mwvolumeval;
                    CurrentmwOpen = mwOpen;
                    CurrentmwHigh = mwHigh;
                    CurrentmwLow = mwLow;
                    CurrentmwClose = mwClose;
                    Currentmwchangeinper = mwchangeinper;
                    CurrentmwNetChange = mwNetChange;

                    //open position
                    opExchange = Settings.opExchange;
                    opSymbol = Settings.opSymbol;
                    opProduct = Settings.opProduct;
                    opQuantity = Settings.opQuantity;
                    opLtp = Settings.opLTP;
                    opPrice = Settings.opPrice;
                    opPL = Settings.opPL;
                    //IRDSPM::Pratiksha::10-11-2020::for current values
                    CurrentopExchange = opExchange;
                    CurrentopSymbol = opSymbol;
                    CurrentopProduct = opProduct;
                    CurrentopQuantity = opQuantity;
                    CurrentopLtp = opLtp;
                    CurrentopPrice = opPrice;
                    CurrentopPL = opPL;

                    //Order book
                    obEXG = Settings.obEXG;
                    obSymbol = Settings.obSymbol;
                    obOrderId = Settings.obOrderId;
                    obTime = Settings.obTime;
                    obType = Settings.obType;
                    obOrderType = Settings.obOrderType;
                    obProduct = Settings.obProduct;
                    obPrice = Settings.obPrice;
                    obTriggerPrice = Settings.obTriggerPrice;
                    obQTY = Settings.obQTY;
                    obStatus = Settings.obStatus;
                    obFilledQty = Settings.obFilledQty;
                    obFilledPrice = Settings.obFilledPrice;
                    obRejectionOrderRemark = Settings.obRejectionOrderRemark;

                    CurrentobEXG = obEXG;
                    CurrentobSymbol = obSymbol;
                    CurrentobOrderId = obOrderId;
                    CurrentobTime = obTime;
                    CurrentobType = obType;
                    CurrentobOrderType = obOrderType;
                    CurrentobProduct = obProduct;
                    CurrentobPrice = obPrice;
                    CurrentobTriggerPrice = obTriggerPrice;
                    CurrentobQTY = obQTY;
                    CurrentobStatus = obStatus;
                    CurrentobFilledQty = obFilledQty;
                    CurrentobFilledPrice = obFilledPrice;
                    CurrentobRejectionOrderRemark = obRejectionOrderRemark;

                    //open Holdings
                    ohEXG = Settings.ohEXG;
                    ohSymbol = Settings.ohSymbol;
                    ohProduct = Settings.ohProduct;
                    ohQuantity = Settings.ohQuantity;
                    ohcloseprice = Settings.ohcloseprice;
                    ohavgprice = Settings.ohavgprice;
                    ohPL = Settings.ohPL;

                    CurrentohEXG = ohEXG;
                    CurrentohSymbol = ohSymbol;
                    CurrentohProduct = ohProduct;
                    CurrentohQuantity = ohQuantity;
                    Currentohcloseprice = ohcloseprice;
                    Currentohavgprice = ohavgprice;
                    CurrentohPL = ohPL;

                    MadeChangesOnCheckboxes();
                }
                catch (Exception ex)
                {
                    WriteUniquelogs("ExecutionForm" + " ", "RefreshColumnSetupSettinginiFile :Exception Error Message: " + ex.Message, MessageType.Exception);
                }
            }
            else
            {
                WriteUniquelogs("ExecutionForm" + " ", "RefreshColumnSetupSettinginiFile : Setting.ini file not found.", MessageType.Informational);
            }
        }

        public void MadeChangesOnCheckboxes()
        {
            MarketWatchTableReflectionChanges();
            OpenPositionTableReflectionChanges();
            OrderBookTableReflectionChanges();
            OpenHoldingsTableReflectionChanges();
        }
        public void MarketWatchTableReflectionChanges()
        {
            if (CurrentmwsymbolnameVal == "false")
            {
                MW1.IsChecked = false;
            }
            else
            {
                MW1.IsChecked = true;
            }

            if (Currentmwltpval == "false")
            {
                MW2.IsChecked = false;
            }
            else
            {
                MW2.IsChecked = true;
            }

            if (Currentmwbidval == "false")
            {
                MW3.IsChecked = false;
            }
            else
            {
                MW3.IsChecked = true;
            }

            if (Currentmwaskval == "false")
            {
                MW4.IsChecked = false;
            }
            else
            {
                MW4.IsChecked = true;
            }

            if (Currentmwvolumeval == "false")
            {
                MW5.IsChecked = false;
            }
            else
            {
                MW5.IsChecked = true;
            }
            //IRDSPM::PRattiksha::24-09-2020::For new symbols
            if (CurrentmwOpen == "false")
            {
                MW6.IsChecked = false;
            }
            else
            {
                MW6.IsChecked = true;
            }

            if (CurrentmwHigh == "false")
            {
                MW7.IsChecked = false;
            }
            else
            {
                MW7.IsChecked = true;
            }

            if (CurrentmwLow == "false")
            {
                MW8.IsChecked = false;
            }
            else
            {
                MW8.IsChecked = true;
            }

            if (CurrentmwClose == "false")
            {
                MW9.IsChecked = false;
            }
            else
            {
                MW9.IsChecked = true;
            }

            if (Currentmwchangeinper == "false")
            {
                MW10.IsChecked = false;
            }
            else
            {
                MW10.IsChecked = true;
            }
            //IRDSPM::Pratiksha::08-10-2020::Added new column "Net Change"
            if (CurrentmwNetChange == "false")
            {
                MW11.IsChecked = false;
            }
            else
            {
                MW11.IsChecked = true;
            }
        }
        public void OpenPositionTableReflectionChanges()
        {
            if (CurrentopExchange == "false")
            {
                OP1.IsChecked = false;
            }
            else
            {
                OP1.IsChecked = true;
            }

            if (CurrentopSymbol == "false")
            {
                OP2.IsChecked = false;
            }
            else
            {
                OP2.IsChecked = true;
            }

            if (CurrentopProduct == "false")
            {
                OP3.IsChecked = false;
            }
            else
            {
                OP3.IsChecked = true;
            }

            if (CurrentopQuantity == "false")
            {
                OP4.IsChecked = false;
            }
            else
            {
                OP4.IsChecked = true;
            }

            if (CurrentopLtp == "false")
            {
                OP5.IsChecked = false;
            }
            else
            {
                OP5.IsChecked = true;
            }
            if (CurrentopPrice == "false")
            {
                OP6.IsChecked = false;
            }
            else
            {
                OP6.IsChecked = true;
            }

            if (CurrentopPL == "false")
            {
                OP7.IsChecked = false;
            }
            else
            {
                OP7.IsChecked = true;
            }

        }
        public void OrderBookTableReflectionChanges()
        {
            if (CurrentobEXG == "false")
            {
                OB1.IsChecked = false;
            }
            else
            {
                OB1.IsChecked = true;
            }

            if (CurrentobSymbol == "false")
            {
                OB2.IsChecked = false;
            }
            else
            {
                OB2.IsChecked = true;
            }

            if (CurrentobOrderId == "false")
            {
                OB3.IsChecked = false;
            }
            else
            {
                OB3.IsChecked = true;
            }

            if (CurrentobTime == "false")
            {
                OB4.IsChecked = false;
            }
            else
            {
                OB4.IsChecked = true;
            }

            if (CurrentobType == "false")
            {
                OB5.IsChecked = false;
            }
            else
            {
                OB5.IsChecked = true;
            }
            if (CurrentobOrderType == "false")
            {
                OB6.IsChecked = false;
            }
            else
            {
                OB6.IsChecked = true;
            }

            if (CurrentobProduct == "false")
            {
                OB7.IsChecked = false;
            }
            else
            {
                OB7.IsChecked = true;
            }

            if (CurrentobPrice == "false")
            {
                OB8.IsChecked = false;
            }
            else
            {
                OB8.IsChecked = true;
            }

            if (CurrentobTriggerPrice == "false")
            {
                OB9.IsChecked = false;
            }
            else
            {
                OB9.IsChecked = true;
            }

            if (CurrentobQTY == "false")
            {
                OB10.IsChecked = false;
            }
            else
            {
                OB10.IsChecked = true;
            }
            if (CurrentobStatus == "false")
            {
                OB11.IsChecked = false;
            }
            else
            {
                OB11.IsChecked = true;
            }

            if (CurrentobFilledQty == "false")
            {
                OB12.IsChecked = false;
            }
            else
            {
                OB12.IsChecked = true;
            }

            if (CurrentobFilledPrice == "false")
            {
                OB13.IsChecked = false;
            }
            else
            {
                OB13.IsChecked = true;
            }

            if (CurrentobRejectionOrderRemark == "false")
            {
                OB14.IsChecked = false;
            }
            else
            {
                OB14.IsChecked = true;
            }
        }
        public void OpenHoldingsTableReflectionChanges()
        {
            if (CurrentohEXG == "false")
            {
                OH1.IsChecked = false;
            }
            else
            {
                OH1.IsChecked = true;
            }

            if (CurrentohSymbol == "false")
            {
                OH2.IsChecked = false;
            }
            else
            {
                OH2.IsChecked = true;
            }

            if (CurrentohProduct == "false")
            {
                OH3.IsChecked = false;
            }
            else
            {
                OH3.IsChecked = true;
            }

            if (CurrentohQuantity == "false")
            {
                OH4.IsChecked = false;
            }
            else
            {
                OH4.IsChecked = true;
            }

            if (Currentohcloseprice == "false")
            {
                OH5.IsChecked = false;
            }
            else
            {
                OH5.IsChecked = true;
            }
            if (Currentohavgprice == "false")
            {
                OH6.IsChecked = false;
            }
            else
            {
                OH6.IsChecked = true;
            }

            if (CurrentohPL == "false")
            {
                OH7.IsChecked = false;
            }
            else
            {
                OH7.IsChecked = true;
            }
        }
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            string notificationChange = NofificationChange();
            string SMTPChange = SmtpChange();
            string marketWatchChange = MarketWatchUpdated();
            string OpenPositionChange = OpenPositionUpdated();
            string OrderBookChange = OrderBookUpdated();
            string openHoldingChange = OpenHoldingUpdated();
            if (CurrentShowGUI != showGUIVal || currenttoastNoti != toastNotificationVal || toastAudioVal != currentToastAudio ||
                currentmaximizedScreen != maximizedScreenVal || currentNotificationTone != getNotificationSound || currentOtherNotification != OtherNotification
                || marketWatchChange == "true" || OpenPositionChange == "true" || OrderBookChange == "true" || openHoldingChange == "true" ||
                CurrentisSmsEnable != isSmsEnable || CurrentisEmailEnable != isEmailEnable || SMTPChange == "true" || notificationChange == "true")
            {
                btnApply_Click(sender, e);
            }
            else
            {
                m_generalSettingObj.Close();
            }
        }

        private string NofificationChange()
        {
            string changed = "";
            for (int i = 0; i < 1; i++)
            {
                if (smsNumber != txtNumber.Text)
                {
                    changed = "true";
                    break;
                }
                else
                {
                    changed = "false";
                }

                if (emailid != txtEmail.Text)
                {
                    changed = "true";
                    break;
                }
                else
                {
                    changed = "false";
                }
                if (CurrentisEmailEnable != isEmailEnable)
                {
                    changed = "true";
                    break;
                }
                else
                {
                    changed = "false";
                }
                if (CurrentisSmsEnable != isSmsEnable)
                {
                    changed = "true";
                    break;
                }
                else
                {
                    changed = "false";
                }
                if (CurrentisSend != isSend)
                {
                    changed = "true";
                    break;
                }
                else
                {
                    changed = "false";
                }
                if (interval != listInterval.SelectedItem.ToString())
                {
                    changed = "true";
                    break;
                }
                else
                {
                    changed = "false";
                }
            }
            return changed;
        }

        private string SmtpChange()
        {
            CurrentSendername = txtsenderName.Text;
            CurrentSenderemail = txtsenderemail.Text;
            CurrentSenderpassword = txtsenderpassword.Text;
            CurrentSenderhost = txtsenderhost.Text;
            CurrentSenderport = txtsenderPort.Text;
            string changed = "";
            for (int i = 0; i < 1; i++)
            {
                if (senderName != CurrentSendername)
                {
                    changed = "true";
                    break;
                }
                else
                {
                    changed = "false";
                }
                if (senderemail != CurrentSenderemail)
                {
                    changed = "true";
                    break;
                }
                else
                {
                    changed = "false";
                }
                if (senderpassword != CurrentSenderpassword)
                {
                    changed = "true";
                    break;
                }
                else
                {
                    changed = "false";
                }
                if (senderhost != CurrentSenderhost)
                {
                    changed = "true";
                    break;
                }
                else
                {
                    changed = "false";
                }
                if (senderPort != CurrentSenderport)
                {
                    changed = "true";
                    break;
                }
                else
                {
                    changed = "false";
                }
            }
            return changed;
        }

        private string OpenHoldingUpdated()
        {
            string changed = "";
            for (int i = 0; i < 2; i++)
            {
                if (ohEXG != CurrentohEXG) { changed = "true"; break; }
                else { changed = "false"; }

                if (ohSymbol != CurrentohSymbol) { changed = "true"; break; }
                else { changed = "false"; }

                if (ohProduct != CurrentohProduct) { changed = "true"; break; }
                else { changed = "false"; }

                if (ohQuantity != CurrentohQuantity) { changed = "true"; break; }
                else { changed = "false"; }

                if (ohcloseprice != Currentohcloseprice) { changed = "true"; break; }
                else { changed = "false"; }

                if (ohavgprice != Currentohavgprice) { changed = "true"; break; }
                else { changed = "false"; }

                if (ohPL != CurrentohPL) { changed = "true"; break; }
                else { changed = "false"; }
            }
            return changed;
        }
        private string OrderBookUpdated()
        {
            string Changed = "";
            for (int i = 0; i < 2; i++)
            {
                if (obEXG != CurrentobEXG) { Changed = "true"; break; }
                else { Changed = "false"; }

                if (obSymbol != CurrentobSymbol) { Changed = "true"; break; }
                else { Changed = "false"; }

                if (obOrderId != CurrentobOrderId) { Changed = "true"; break; }
                else { Changed = "false"; }

                if (obTime != CurrentobTime) { Changed = "true"; break; }
                else { Changed = "false"; }

                if (obType != CurrentobType) { Changed = "true"; break; }
                else { Changed = "false"; }

                if (obOrderType != CurrentobOrderType) { Changed = "true"; break; }
                else { Changed = "false"; }

                if (obProduct != CurrentobProduct) { Changed = "true"; break; }
                else { Changed = "false"; }

                if (obPrice != CurrentobPrice) { Changed = "true"; break; }
                else { Changed = "false"; }

                if (obTriggerPrice != CurrentobTriggerPrice) { Changed = "true"; break; }
                else { Changed = "false"; }

                if (obQTY != CurrentobQTY) { Changed = "true"; break; }
                else { Changed = "false"; }

                if (obStatus != CurrentobStatus) { Changed = "true"; break; }
                else { Changed = "false"; }

                if (obFilledQty != CurrentobFilledQty) { Changed = "true"; break; }
                else { Changed = "false"; }

                if (obFilledPrice != CurrentobFilledPrice) { Changed = "true"; break; }
                else { Changed = "false"; }

                if (obRejectionOrderRemark != CurrentobRejectionOrderRemark) { Changed = "true"; break; }
                else { Changed = "false"; }
            }
            return Changed;
        }
        private string OpenPositionUpdated()
        {
            string changed = "";
            for (int i = 0; i < 2; i++)
            {
                if (opSymbol != CurrentopSymbol) { changed = "true"; break; }
                else { changed = "false"; }

                if (opExchange != CurrentopExchange) { changed = "true"; break; }
                else { changed = "false"; }

                if (opProduct != CurrentopProduct) { changed = "true"; break; }
                else { changed = "false"; }

                if (opQuantity != CurrentopQuantity) { changed = "true"; break; }
                else { changed = "false"; }

                if (opLtp != CurrentopLtp) { changed = "true"; break; }
                else { changed = "false"; }

                if (opPrice != CurrentopPrice) { changed = "true"; break; }
                else { changed = "false"; }

                if (opPL != CurrentopPL) { changed = "true"; break; }
                else { changed = "false"; }
            }
            return changed;
        }
        private string MarketWatchUpdated()
        {
            string changed = "";
            for (int i = 0; i < 1; i++)
            {
                if (mwsymbolnameVal != CurrentmwsymbolnameVal)
                {
                    changed = "true";
                    break;
                }
                else
                {
                    changed = "false";
                }
                if (mwltpval != Currentmwltpval)
                {
                    changed = "true";
                    break;
                }
                else
                {
                    changed = "false";
                }
                if (mwbidval != Currentmwbidval)
                {
                    changed = "true";
                    break;
                }
                else
                {
                    changed = "false";
                }
                if (mwaskval != Currentmwaskval)
                {
                    changed = "true";
                    break;
                }
                else
                {
                    changed = "false";
                }
                if (mwvolumeval != Currentmwvolumeval)
                {
                    changed = "true";
                    break;
                }
                else
                {
                    changed = "false";
                }
                if (mwOpen != CurrentmwOpen)
                {
                    changed = "true";
                    break;
                }
                else
                {
                    changed = "false";
                }
                if (mwHigh != CurrentmwHigh)
                {
                    changed = "true";
                    break;
                }
                else
                {
                    changed = "false";
                }
                if (mwLow != CurrentmwLow)
                {
                    changed = "true";
                    break;
                }
                else
                {
                    changed = "false";
                }
                if (mwClose != CurrentmwClose)
                {
                    changed = "true";
                    break;
                }
                else
                {
                    changed = "false";
                }
                if (mwchangeinper != Currentmwchangeinper)
                {
                    changed = "true";
                    break;
                }
                else
                {
                    changed = "false";
                }
                if (mwNetChange != CurrentmwNetChange)
                {
                    changed = "true";
                    break;
                }
                else
                {
                    changed = "false";
                }
            }
            return changed;
        }

        private void btnApply_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Validation();
                if (validemail == true && validMobile == true)
                {
                    message = "Are you sure you want to save the changes?";
                    buttons = MessageBoxButtons.YesNo;
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                    if (dialog == DialogResult.Yes)
                    {
                        string[] key = new string[4];
                        string[] value = new string[4];

                        string key_val;
                        string value_value;
                        key[0] = "toastNotification";
                        key[1] = "toastAudio";
                        key[2] = "showGUIForOpenPositions";
                        key[3] = "screenMode";

                        if (currenttoastNoti == true)
                        {
                            value[0] = "true";
                        }
                        else
                        {
                            value[0] = "false";
                            value[1] = "false";
                        }

                        if (currentToastAudio == true)
                        {
                            value[1] = "true";
                        }
                        else
                        {
                            value[1] = "false";
                        }

                        value[2] = CurrentShowGUI.ToString().ToLower();

                        for (int i = 0; i < 4; i++)
                        {
                            if (i < 2)
                            {
                                key_val = key[i];
                                value_value = value[i];
                                Settings.writeINIFile("TOASTSETTING", key_val, value_value);
                            }
                            else if (i == 2)
                            {
                                key_val = key[i];
                                value_value = value[i];
                                Settings.writeINIFile("GUI", key_val, value_value);
                            }
                            //IRDSPM::PRatiksha::18-11-2020::uncommented
                            else if (i == 3)
                            {
                                key_val = key[i];
                                value_value = currentmaximizedScreen;
                                Settings.writeINIFile("screenMode", key_val, value_value);
                            }
                        }
                        Settings.writeINIFile("TOASTSETTING", "toastAudioPath", currentNotificationTone);
                        marketWatchSave();
                        openPositionSave();
                        orderbookSave();
                        openHoldingSave();
                        //IRDSPM::Pratiksha::14-05-2021::For saving notification details
                        NotificationsSave();

                        //IRDSPM::Pratiksha::14-07-2021::For SMTP changes
                        SMTPSaveDet();
                        //IRDSPM::Pratiksha::16-02-2021::To add into guilogs for toast

                        if (txtsenderName.Text == "XXXXXX" && txtsenderemail.Text == "XXXXXX" && txtsenderhost.Text == "XXXXXX" && txtsenderPort.Text == "XXXXXX" && txtsenderpassword.Text == "XXXXXX")
                        {
                            message = "You need to set the SMTP details also to get the emails from IRDS.\nYou can set those details by clicking on SMTP tab.";
                            buttons = MessageBoxButtons.OK;
                            System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                        }
                        WriteGUIlogs(m_ObjkiteConnectWrapper.m_ExceptionCounter, DateTime.Now + " General setting's changes saved successfully!!!");
                        m_ObjkiteConnectWrapper.m_ExceptionCounter++;

                        m_generalSettingObj.Close();
                        WriteUniquelogs("settingLogs" + " ", "btnApply_Click :  Details saved in ini file.", MessageType.Informational);
                    }
                    else
                    {
                        m_generalSettingObj.Close();
                    }
                }
            }

            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "btnApply_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        bool validemail = false;
        bool validMobile = false;
        private void Validation()
        {
            bool showmsgforEmail = false;
            bool showmsgforMobile = false;
            try
            {
                if (radioButtonserviceFalse.IsChecked == true)
                {
                    validemail = true;
                    validMobile = true;
                }
                else
                {
                    if(CheckEmail.IsChecked == true) 
                    {
                        if (txtEmail.Text.Length == 0)
                        {
                            validemail = false;
                        }
                        else
                        {
                            if (txtEmail.Text == "XXXXXX")
                            {
                                validemail = true;
                            }
                            else
                            {
                                bool[] isValidEmail = null;
                                Regex regexemailid = new Regex(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
                                if (txtEmail.Text.ToString().Contains(','))
                                {
                                    string[] emailDet = txtEmail.Text.ToString().Split(',');
                                    isValidEmail = new bool[emailDet.Length];
                                    for (int i = 0; i < emailDet.Length; i++)
                                    {
                                        isValidEmail[i] = regexemailid.IsMatch((emailDet[i]));
                                    }
                                    for (int i = 0; i < isValidEmail.Length; i++)
                                    {
                                        if (isValidEmail[i] == false)
                                        {
                                            validemail = false;
                                            showmsgforEmail = true;
                                            break;
                                        }
                                        else
                                        {
                                            validemail = true;
                                        }
                                    }
                                }
                                else
                                {
                                    isValidEmail = new bool[1];
                                    isValidEmail[0] = regexemailid.IsMatch(txtEmail.Text);
                                    if (isValidEmail[0] == false)
                                    {
                                        validemail = false;
                                        showmsgforEmail = true;
                                    }
                                    else
                                    {
                                        validemail = true;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        validemail = true;
                    }

                    if(CheckSms.IsChecked == true)
                    {
                        if (txtNumber.Text.Length == 0)
                        {
                            validMobile = false;
                        }
                        else
                        {
                            if (txtNumber.Text == "XXXXXX")
                            {
                                validMobile = true;
                            }
                            else
                            {
                                bool[] isValidNumber = null;
                                Regex regexMobNum = new Regex(@"^\d{10}$");
                                if (txtNumber.Text.ToString().Contains(','))
                                {
                                    string[] numberDet = txtNumber.Text.ToString().Split(',');
                                    isValidNumber = new bool[numberDet.Length];
                                    for (int i = 0; i < numberDet.Length; i++)
                                    {
                                        isValidNumber[i] = regexMobNum.IsMatch((numberDet[i]));
                                    }
                                    for (int i = 0; i < isValidNumber.Length; i++)
                                    {
                                        if (isValidNumber[i] == false)
                                        {
                                            validMobile = false;
                                            showmsgforMobile = true;
                                            break;
                                        }
                                        else
                                        {
                                            validMobile = true;
                                        }
                                    }
                                }
                                else
                                {
                                    isValidNumber = new bool[1];
                                    isValidNumber[0] = regexMobNum.IsMatch(txtNumber.Text);
                                    if (isValidNumber[0] == false)
                                    {
                                        validMobile = false;
                                        showmsgforMobile = true;
                                    }
                                    else
                                    {
                                        validMobile = true;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        validMobile = true;
                    }
                }
                if(showmsgforEmail == true || validemail == false || validMobile == false || showmsgforMobile == true)
                {
                    string message = "Please enter valid ";
                    if(showmsgforEmail == true || validemail == false)
                    {
                        message += "Emailid\n";
                    }
                    if (showmsgforMobile == true || validMobile == false)
                    {
                        message += "Mobile number";
                    }
                    buttons = MessageBoxButtons.OK;
                    System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "Validation :  Exception Error Message " + ex.Message, MessageType.Exception);
            }
        }

        List<string> PlainDetList;
        List<string> EncDetList;
        private void SMTPSaveDet()
        {
            try
            {
                PlainDetList = new List<string>();
                string[] Key = new string[5];
                string[] value = new string[5];
                Key[0] = "senderName";
                Key[1] = "senderemail";
                Key[2] = "senderpassword";
                Key[3] = "senderhost";
                Key[4] = "senderPort";


                value[0] = txtsenderName.Text;
                value[1] = txtsenderemail.Text;
                value[2] = txtsenderpassword.Text;
                value[3] = txtsenderhost.Text;
                value[4] = txtsenderPort.Text;

                PlainDetList.Add(value[0]);
                PlainDetList.Add(value[1]);
                PlainDetList.Add(value[2]);
                PlainDetList.Add(value[3]);
                PlainDetList.Add(value[4]);

                if (PlainDetList[0] != "XXXXXX" || PlainDetList[1] != "XXXXXX" || PlainDetList[2] != "XXXXXX" || PlainDetList[3] != "XXXXXX" || PlainDetList[4] != "XXXXXX")
                {
                    encryptDetails(PlainDetList);
                    for (int k = 0; k < 5; k++)
                    {
                        Settings.writeINIFile("SMTP", Key[k], EncDetList[k]);
                    }
                    WriteUniquelogs("settingLogs" + " ", "SMTPSaveDet :  SMTPSaveDet details saved in settings.ini file.", MessageType.Informational);

                }
                else if(PlainDetList[0] == "XXXXXX" || PlainDetList[1] == "XXXXXX" || PlainDetList[2] == "XXXXXX" || PlainDetList[3] == "XXXXXX" || PlainDetList[4] == "XXXXXX")
                {
                    for (int k = 0; k < 5; k++)
                    {
                        Settings.writeINIFile("SMTP", Key[k], value[k]);
                    }
                    WriteUniquelogs("settingLogs" + " ", "SMTPSaveDet :  SMTPSaveDet details saved in settings.ini file.", MessageType.Informational);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "SMTPSaveDet :  Exception Error Message " + ex.Message, MessageType.Exception);
            }
        }
        public void encryptDetails(List<string> plainDet)
        {
            try
            {
                EncDetList = new List<string>();
                for (int i = 0; i < plainDet.Count; i++)
                {
                    string enc = EncryptData(plainDet[i], "IrdsalgoTrader");
                    EncDetList.Add(enc);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "encryptDetails : Exception Error Message =  " + ex.Message, MessageType.Exception);
            }
        }
        public static string EncryptData(string strInText, string strKey)
        {
            byte[] bytesBuff = Encoding.Unicode.GetBytes(strInText);

            using (Aes aes__1 = Aes.Create())
            {
                System.Security.Cryptography.Rfc2898DeriveBytes crypto = new Rfc2898DeriveBytes(strKey, new byte[] {
                0x49,
                0x76,
                0x61,
                0x6e,
                0x20,
                0x4d,
                0x65,
                0x64,
                0x76,
                0x65,
                0x64,
                0x65,
                0x76
            });
                aes__1.Key = crypto.GetBytes(32);
                aes__1.IV = crypto.GetBytes(16);

                using (MemoryStream mStream = new MemoryStream())
                {
                    using (CryptoStream cStream = new CryptoStream(mStream, aes__1.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cStream.Write(bytesBuff, 0, bytesBuff.Length);
                        cStream.Close();
                    }

                    strInText = Convert.ToBase64String(mStream.ToArray());
                }
            }

            return strInText;
        }
        public void NotificationsSave()
        {
            try
            {
                string[] Key = new string[8];
                string[] Val = new string[8];
                Key[0] = "isSend";
                Key[1] = "isSmsEnable";
                Key[2] = "isEmailEnable";
                Key[3] = "smsNumber";
                Key[4] = "emailid";
                Key[5] = "interval";
                //03-AUG-2021::Pratiksha::FOr other notification field
                Key[6] = "OtherNotification";
                if (radioButtonserviceTrue.IsChecked == true)
                { Val[0] = "true"; }
                else { Val[0] = "false"; }

                if (CheckSms.IsChecked == true)
                { Val[1] = "true"; }
                else { Val[1] = "false"; }

                if (CheckEmail.IsChecked == true)
                { Val[2] = "true"; }
                else { Val[2] = "false"; }


                Val[3] = txtNumber.Text;
                Val[4] = txtEmail.Text;
                if (listInterval.SelectedItem != null)
                    Val[5] = listInterval.SelectedItem.ToString();
                //03-AUG-2021::Pratiksha::FOr other notification field
                if (RBOtherNotificationTrue.IsChecked == true)
                { Val[6] = "true"; }
                else { Val[6] = "false"; }
                for (int k = 0; k < 7; k++)
                {
                    Settings.writeINIFile("Notifications", Key[k], Val[k]);
                }
                WriteUniquelogs("settingLogs" + " ", "NotificationsSave :  Notifications details saved in settings.ini file.", MessageType.Informational);
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "NotificationsSave :  Exception Error Message " + ex.Message, MessageType.Exception);
            }
        }
        //IRDSPM::Pratiksha::16-02-2021::For toast notification
        public void WriteGUIlogs(int Counter, string message)
        {
            try
            {
                SymbolLogs log = new SymbolLogs();
                log.createFile("GUILogs");
                log.LogMessage(Counter + "|" + message, MessageType.Informational);
                log = null;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : " + e.Message);
                Trace.WriteLine("Exception : " + e.Message);
            }
        }
        private void HomeGrid_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            gridHomeRight.Visibility = Visibility.Visible;
            gridScreenRight.Visibility = Visibility.Hidden;
            gridcolumnSetupRight.Visibility = Visibility.Hidden;
            //IRDSPM::Pratiksha::For new sound grid
            gridSoundRight.Visibility = Visibility.Hidden;
            //Pratiksha::01-02-2021::For notification datagrid
            gridNotificationRight.Visibility = Visibility.Hidden;
            //IRDSPM::Pratiksha::14-07-2021::For SMTP changes
            gridsmtpRight.Visibility = Visibility.Hidden;
        }

        private void radioButtonnotificationTrue_Checked(object sender, RoutedEventArgs e)
        {
            radioButtonAudioTrue.IsEnabled = true;
            radioButtonAudioFalse.IsEnabled = true;
        }

        private void radioButtonnotificationFalse_Checked(object sender, RoutedEventArgs e)
        {
            radioButtonAudioTrue.IsEnabled = false;
            radioButtonAudioFalse.IsEnabled = false;
            //IRDSPM::PRatiksha::02-10-2020::Default set to false
            radioButtonAudioFalse.IsChecked = true;
        }

        private void HomeGrid_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            HomeGrid.Background = Brushes.LightBlue;
        }

        private void HomeGrid_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            try
            {
                var bc = new BrushConverter();
                HomeGrid.Background = (Brush)bc.ConvertFrom("#FFE4EEFF");
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "HomeGrid_MouseLeave :  Exception Error Message =  " + ex.Message, MessageType.Exception);
            }
        }

        private void ScreenGrid_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                gridHomeRight.Visibility = Visibility.Hidden;
                gridScreenRight.Visibility = Visibility.Visible;
                gridcolumnSetupRight.Visibility = Visibility.Hidden;
                //IRDSPM::Pratiksha::For new sound grid
                gridSoundRight.Visibility = Visibility.Hidden;
                if (gridScreenRight.Visibility == Visibility.Visible)
                {
                    if (currentmaximizedScreen == "Maximized")
                    {
                        radioButtonMaximizedTrue.IsChecked = true;
                    }
                    else
                    {
                        radioButtonMAximizedFalse.IsChecked = true;
                    }
                }
                //Pratiksha::01-02-2021::For notification datagrid
                gridNotificationRight.Visibility = Visibility.Hidden;
                //IRDSPM::Pratiksha::14-07-2021::For SMTP changes
                gridsmtpRight.Visibility = Visibility.Hidden;
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "ScreenGrid_MouseLeftButtonUp :  Exception Error Message =  " + ex.Message, MessageType.Exception);
            }
        }

        private void ScreenGrid_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            ScreenGrid.Background = Brushes.LightBlue;
        }

        private void ScreenGrid_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            try
            {
                var bc = new BrushConverter();
                ScreenGrid.Background = (Brush)bc.ConvertFrom("#FFE4EEFF");
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "ScreenGrid_MouseLeave :  Exception Error Message = " + ex.Message, MessageType.Informational);
            }
        }

        private void columnSetupGrid_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                gridHomeRight.Visibility = Visibility.Hidden;
                gridScreenRight.Visibility = Visibility.Hidden;
                gridcolumnSetupRight.Visibility = Visibility.Visible;
                //IRDSPM::Pratiksha::For new sound grid
                gridSoundRight.Visibility = Visibility.Hidden;
                radioButtonMarketWatch_Checked(sender, e);
                //Pratiksha::01-02-2021::For notification datagrid
                gridNotificationRight.Visibility = Visibility.Hidden;
                //IRDSPM::Pratiksha::14-07-2021::For SMTP changes
                gridsmtpRight.Visibility = Visibility.Hidden;
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "columnSetupGrid_MouseLeftButtonUp :  Exception Error Message =  " + ex.Message, MessageType.Exception);
            }
        }

        private void columnSetupGrid_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            columnSetupGrid.Background = Brushes.LightBlue;
        }

        private void columnSetupGrid_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            try
            {
                var bc = new BrushConverter();
                columnSetupGrid.Background = (Brush)bc.ConvertFrom("#FFE4EEFF");
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "columnSetupGrid_MouseLeave :  Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void radioButtonMarketWatch_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                marketWatchGrid.Visibility = Visibility.Visible;
                OpenPositionGrid.Visibility = Visibility.Hidden;
                OrderBookGrid.Visibility = Visibility.Hidden;
                OpenHoldingsGrid.Visibility = Visibility.Hidden;
                MarketWatchTableReflectionChanges();
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "radioButtonMarketWatch_Checked :  Exception Error Message =  " + ex.Message, MessageType.Exception);
            }
        }

        private void radioButtonOrderBook_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                marketWatchGrid.Visibility = Visibility.Hidden;
                OpenPositionGrid.Visibility = Visibility.Hidden;
                OrderBookGrid.Visibility = Visibility.Visible;
                OpenHoldingsGrid.Visibility = Visibility.Hidden;
                OrderBookTableReflectionChanges();
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "radioButtonOrderBook_Checked :  Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void radioButtonOpenPosition_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                marketWatchGrid.Visibility = Visibility.Hidden;
                OpenPositionGrid.Visibility = Visibility.Visible;
                OrderBookGrid.Visibility = Visibility.Hidden;
                OpenHoldingsGrid.Visibility = Visibility.Hidden;
                OpenPositionTableReflectionChanges();
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "radioButtonOpenPosition_Checked :  Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        public void orderbookSave()
        {
            try
            {
                string[] obkey = new string[14];
                string[] obVal = new string[14];
                obkey[0] = "EXG";
                obkey[1] = "Symbol";
                obkey[2] = "OrderId";
                obkey[3] = "Time";
                obkey[4] = "Type";
                obkey[5] = "OrderType";
                obkey[6] = "Product";
                obkey[7] = "Price";
                obkey[8] = "TriggerPrice";
                obkey[9] = "QTY";
                obkey[10] = "Status";
                obkey[11] = "FilledQty";
                obkey[12] = "FilledPrice";
                obkey[13] = "RejectionOrderRemark";

                if (OB1.IsChecked == true)
                { obVal[0] = "true"; }
                else { obVal[0] = "false"; }

                if (OB2.IsChecked == true)
                { obVal[1] = "true"; }
                else { obVal[1] = "false"; }

                if (OB3.IsChecked == true)
                { obVal[2] = "true"; }
                else { obVal[2] = "false"; }

                if (OB4.IsChecked == true)
                { obVal[3] = "true"; }
                else { obVal[3] = "false"; }

                if (OB5.IsChecked == true)
                { obVal[4] = "true"; }
                else { obVal[4] = "false"; }

                if (OB6.IsChecked == true)
                { obVal[5] = "true"; }
                else { obVal[5] = "false"; }

                if (OB7.IsChecked == true)
                { obVal[6] = "true"; }
                else { obVal[6] = "false"; }

                if (OB8.IsChecked == true)
                { obVal[7] = "true"; }
                else { obVal[7] = "false"; }

                if (OB9.IsChecked == true)
                { obVal[8] = "true"; }
                else { obVal[8] = "false"; }

                if (OB10.IsChecked == true)
                { obVal[9] = "true"; }
                else { obVal[9] = "false"; }

                if (OB11.IsChecked == true)
                { obVal[10] = "true"; }
                else { obVal[10] = "false"; }

                if (OB12.IsChecked == true)
                { obVal[11] = "true"; }
                else { obVal[11] = "false"; }

                if (OB13.IsChecked == true)
                { obVal[12] = "true"; }
                else { obVal[12] = "false"; }

                if (OB14.IsChecked == true)
                { obVal[13] = "true"; }
                else { obVal[13] = "false"; }
                for (int k = 0; k < 14; k++)
                {
                    Settings.writeINIFile("OrderBookColumn", obkey[k], obVal[k]);
                }
                WriteUniquelogs("settingLogs" + " ", "orderbookSave : OrderBook details saved in settings.ini file.", MessageType.Informational);
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "orderbookSave : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }
        public void openPositionSave()
        {
            try
            {
                string[] opkey = new string[8];
                string[] opVal = new string[8];
                opkey[0] = "Exchange";
                opkey[1] = "Symbol";
                opkey[2] = "Product";
                opkey[3] = "Quantity";
                opkey[4] = "LTP";
                opkey[5] = "Price";
                opkey[6] = "P&L";


                if (OP1.IsChecked == true)
                { opVal[0] = "true"; }
                else { opVal[0] = "false"; }

                if (OP2.IsChecked == true)
                { opVal[1] = "true"; }
                else { opVal[1] = "false"; }

                if (OP3.IsChecked == true)
                { opVal[2] = "true"; }
                else { opVal[2] = "false"; }

                if (OP4.IsChecked == true)
                { opVal[3] = "true"; }
                else { opVal[3] = "false"; }

                if (OP5.IsChecked == true)
                { opVal[4] = "true"; }
                else { opVal[4] = "false"; }

                if (OP6.IsChecked == true)
                { opVal[5] = "true"; }
                else { opVal[5] = "false"; }

                if (OP7.IsChecked == true)
                { opVal[6] = "true"; }
                else { opVal[6] = "false"; }

                for (int k = 0; k < 7; k++)
                {
                    Settings.writeINIFile("OpenPositionColumn", opkey[k], opVal[k]);
                }
                WriteUniquelogs("settingLogs" + " ", "openPositionSave : OpenPosition details saved in settings.ini file.", MessageType.Informational);
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "openPositionSave :  Exception Error Message = " + ex.Message, MessageType.Informational);
            }
        }
        public void marketWatchSave()
        {
            try
            {
                //IRDSPM::Pratiksha::08-10-2020::Added new column "Net Change"
                string[] mwkey = new string[11];
                string[] mwVal = new string[11];
                mwkey[0] = "Symbol";
                mwkey[1] = "LTP";
                mwkey[2] = "BidPrice";
                mwkey[3] = "AskPrice";
                mwkey[4] = "Volume";
                mwkey[5] = "Open";
                mwkey[6] = "High";
                mwkey[7] = "Low";
                mwkey[8] = "Close";
                mwkey[9] = "Changein%";
                //IRDSPM::Pratiksha::08-10-2020::Added new column "Net Change"
                mwkey[10] = "NetChange";

                if (MW1.IsChecked == true)
                { mwVal[0] = "true"; }
                else { mwVal[0] = "false"; }

                if (MW2.IsChecked == true)
                { mwVal[1] = "true"; }
                else { mwVal[1] = "false"; }

                if (MW3.IsChecked == true)
                { mwVal[2] = "true"; }
                else { mwVal[2] = "false"; }

                if (MW4.IsChecked == true)
                { mwVal[3] = "true"; }
                else { mwVal[3] = "false"; }

                if (MW5.IsChecked == true)
                { mwVal[4] = "true"; }
                else { mwVal[4] = "false"; }
                //IRDSPM:Pratiksha::24-09-2020::Added for new symbols
                if (MW6.IsChecked == true)
                { mwVal[5] = "true"; }
                else { mwVal[5] = "false"; }

                if (MW7.IsChecked == true)
                { mwVal[6] = "true"; }
                else { mwVal[6] = "false"; }

                if (MW8.IsChecked == true)
                { mwVal[7] = "true"; }
                else { mwVal[7] = "false"; }

                if (MW9.IsChecked == true)
                { mwVal[8] = "true"; }
                else { mwVal[8] = "false"; }

                if (MW10.IsChecked == true)
                { mwVal[9] = "true"; }
                else { mwVal[9] = "false"; }

                //IRDSPM::Pratiksha::08-10-2020::Added new column "Net Change"
                if (MW11.IsChecked == true)
                { mwVal[10] = "true"; }
                else { mwVal[10] = "false"; }
                for (int j = 0; j < 11; j++)
                {
                    Settings.writeINIFile("MarketWatchColumn", mwkey[j], mwVal[j]);
                }
                WriteUniquelogs("settingLogs" + " ", "marketWatchSave :  MarketWatch details updated in settings.ini file.", MessageType.Informational);
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "marketWatchSave :  Exception Error Message : " + ex.Message, MessageType.Exception);
            }
        }

        public void openHoldingSave()
        {
            try
            {
                string[] oHkey = new string[8];
                string[] oHVal = new string[8];
                oHkey[0] = "Exchange";
                oHkey[1] = "Symbol";
                oHkey[2] = "Product";
                oHkey[3] = "Quantity";
                oHkey[4] = "closePrice";
                oHkey[5] = "avgPrice";
                oHkey[6] = "P&L";

                if (OH1.IsChecked == true)
                { oHVal[0] = "true"; }
                else { oHVal[0] = "false"; }

                if (OH2.IsChecked == true)
                { oHVal[1] = "true"; }
                else { oHVal[1] = "false"; }

                if (OH3.IsChecked == true)
                { oHVal[2] = "true"; }
                else { oHVal[2] = "false"; }

                if (OH4.IsChecked == true)
                { oHVal[3] = "true"; }
                else { oHVal[3] = "false"; }

                if (OH5.IsChecked == true)
                { oHVal[4] = "true"; }
                else { oHVal[4] = "false"; }

                if (OH6.IsChecked == true)
                { oHVal[5] = "true"; }
                else { oHVal[5] = "false"; }

                if (OH7.IsChecked == true)
                { oHVal[6] = "true"; }
                else { oHVal[6] = "false"; }
                for (int k = 0; k < 7; k++)
                {
                    Settings.writeINIFile("OpenHoldingsColumn", oHkey[k], oHVal[k]);
                }
                WriteUniquelogs("settingLogs" + " ", "openHoldingSave :  OpenPosition details saved in settings.ini file.", MessageType.Informational);
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "openHoldingSave :  Exception Error Message " + ex.Message, MessageType.Exception);
            }
        }
        public void WriteUniquelogs(string symbol, string message, MessageType msgType)
        {
            try
            {
                SymbolLogs log = new SymbolLogs();
                log.createFile(symbol);
                log.LogMessage(message, MessageType.Informational);
                log = null;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : " + e.Message);
                Trace.WriteLine("Exception : " + e.Message);
            }
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            FontDialog f1 = new FontDialog();
            f1.ShowDialog();
        }

        private void btnMarkall_Click(object sender, RoutedEventArgs e)
        {
            if (radioButtonMarketWatch.IsChecked == true)
            {
                MW1.IsChecked = true;
                MW1.IsEnabled = false;
                MW2.IsChecked = true;
                MW3.IsChecked = true;
                MW4.IsChecked = true;
                MW5.IsChecked = true;
                MW6.IsChecked = true;
                MW7.IsChecked = true;
                MW8.IsChecked = true;
                MW9.IsChecked = true;
                MW10.IsChecked = true;
                //IRDSPM::Pratiksha::08-10-2020::Added new column "Net Change"
                MW11.IsChecked = true;

                CurrentmwsymbolnameVal = "true";
                Currentmwltpval = "true";
                Currentmwbidval = "true";
                Currentmwaskval = "true";
                Currentmwvolumeval = "true";
                CurrentmwOpen = "true";
                CurrentmwHigh = "true";
                CurrentmwLow = "true";
                CurrentmwClose = "true";
                Currentmwchangeinper = "true";
                CurrentmwNetChange = "true";
            }
            else if (radioButtonOpenPosition.IsChecked == true)
            {
                OP1.IsChecked = true;
                OP2.IsEnabled = false;
                OP2.IsChecked = true;
                OP3.IsChecked = true;
                OP4.IsChecked = true;
                OP5.IsChecked = true;
                OP6.IsChecked = true;
                OP7.IsChecked = true;

                CurrentopExchange = "true";
                CurrentopSymbol = "true";
                CurrentopLtp = "true";
                CurrentopPL = "true";
                CurrentopPrice = "true";
                CurrentopProduct = "true";
                CurrentopQuantity = "true";
            }
            else if (radioButtonOrderBook.IsChecked == true)
            {
                OB1.IsChecked = true;
                OB2.IsEnabled = false;
                OB2.IsChecked = true;
                OB3.IsChecked = true;
                OB4.IsChecked = true;
                OB5.IsChecked = true;
                OB6.IsChecked = true;
                OB7.IsChecked = true;
                OB8.IsChecked = true;
                OB9.IsChecked = true;
                OB10.IsChecked = true;
                OB11.IsChecked = true;
                OB12.IsChecked = true;
                OB13.IsChecked = true;
                OB14.IsChecked = true;

                CurrentobEXG = "true";
                CurrentobFilledPrice = "true";
                CurrentobFilledQty = "true";
                CurrentobOrderId = "true";
                CurrentobOrderType = "true";
                CurrentobPrice = "true";
                CurrentobProduct = "true";
                CurrentobQTY = "true";
                CurrentobRejectionOrderRemark = "true";
                CurrentobStatus = "true";
                CurrentobSymbol = "true";
                CurrentobTime = "true";
                CurrentobTriggerPrice = "true";
                CurrentobType = "true";
            }
            else if (radioButtonOpenHoldings.IsChecked == true)
            {
                OH1.IsChecked = true;
                OH2.IsEnabled = false;
                OH2.IsChecked = true;
                OH3.IsChecked = true;
                OH4.IsChecked = true;
                OH5.IsChecked = true;
                OH6.IsChecked = true;
                OH7.IsChecked = true;
                Currentohavgprice = "true";
                Currentohcloseprice = "true";
                CurrentohEXG = "true";
                CurrentohPL = "true";
                CurrentohProduct = "true";
                CurrentohQuantity = "true";
                CurrentohSymbol = "true";
            }
        }

        private void btnResetMW_Click(object sender, RoutedEventArgs e)
        {
            if (radioButtonMarketWatch.IsChecked == true)
            {
                MW1.IsChecked = true;
                MW1.IsEnabled = false;
                MW2.IsChecked = false;
                MW3.IsChecked = false;
                MW4.IsChecked = false;
                MW5.IsChecked = false;
                MW6.IsChecked = false;
                MW7.IsChecked = false;
                MW8.IsChecked = false;
                MW9.IsChecked = false;
                MW10.IsChecked = false;
                //IRDSPM::Pratiksha::08-10-2020::Added new column "Net Change"
                MW11.IsChecked = false;

                CurrentmwsymbolnameVal = "true";
                Currentmwltpval = "false";
                Currentmwbidval = "false";
                Currentmwaskval = "false";
                Currentmwvolumeval = "false";
                CurrentmwOpen = "false";
                CurrentmwHigh = "false";
                CurrentmwLow = "false";
                CurrentmwClose = "false";
                Currentmwchangeinper = "false";
                CurrentmwNetChange = "false";
            }
            else if (radioButtonOpenPosition.IsChecked == true)
            {
                OP1.IsChecked = false;
                OP2.IsEnabled = false;
                OP2.IsChecked = true;
                OP3.IsChecked = false;
                OP4.IsChecked = false;
                OP5.IsChecked = false;
                OP6.IsChecked = false;
                OP7.IsChecked = false;

                CurrentopExchange = "false";
                CurrentopSymbol = "true";
                CurrentopLtp = "false";
                CurrentopPL = "false";
                CurrentopPrice = "false";
                CurrentopProduct = "false";
                CurrentopQuantity = "false";
            }
            else if (radioButtonOrderBook.IsChecked == true)
            {
                OB1.IsChecked = false;
                OB2.IsEnabled = false;
                OB2.IsChecked = true;
                OB3.IsChecked = false;
                OB4.IsChecked = false;
                OB5.IsChecked = false;
                OB6.IsChecked = false;
                OB7.IsChecked = false;
                OB8.IsChecked = false;
                OB9.IsChecked = false;
                OB10.IsChecked = false;
                OB11.IsChecked = false;
                OB12.IsChecked = false;
                OB13.IsChecked = false;
                OB14.IsChecked = false;

                CurrentobEXG = "false";
                CurrentobFilledPrice = "false";
                CurrentobFilledQty = "false";
                CurrentobOrderId = "false";
                CurrentobOrderType = "false";
                CurrentobPrice = "false";
                CurrentobProduct = "false";
                CurrentobQTY = "false";
                CurrentobRejectionOrderRemark = "false";
                CurrentobStatus = "false";
                CurrentobSymbol = "true";
                CurrentobTime = "false";
                CurrentobTriggerPrice = "false";
                CurrentobType = "false";
            }
            else if (radioButtonOpenHoldings.IsChecked == true)
            {
                OH1.IsChecked = false;
                OH2.IsEnabled = false;
                OH2.IsChecked = true;
                OH3.IsChecked = false;
                OH4.IsChecked = false;
                OH5.IsChecked = false;
                OH6.IsChecked = false;
                OH7.IsChecked = false;

                Currentohavgprice = "false";
                Currentohcloseprice = "false";
                CurrentohEXG = "false";
                CurrentohPL = "false";
                CurrentohProduct = "false";
                CurrentohQuantity = "false";
                CurrentohSymbol = "true";
            }
        }

        private void radioButtonOpenHoldings_Checked(object sender, RoutedEventArgs e)
        {
            marketWatchGrid.Visibility = Visibility.Hidden;
            OpenPositionGrid.Visibility = Visibility.Hidden;
            OrderBookGrid.Visibility = Visibility.Hidden;
            OpenHoldingsGrid.Visibility = Visibility.Visible;
            OpenHoldingsTableReflectionChanges();
        }

        //IRDSPM::Pratiksha::21-10-2020::For new sound grid changes --start
        private void SoundGrid_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                gridHomeRight.Visibility = Visibility.Hidden;
                gridScreenRight.Visibility = Visibility.Hidden;
                gridcolumnSetupRight.Visibility = Visibility.Hidden;
                gridSoundRight.Visibility = Visibility.Visible;
                //Pratiksha::01-02-2021::For notification datagrid
                gridNotificationRight.Visibility = Visibility.Hidden;
                //IRDSPM::Pratiksha::14-07-2021::For SMTP changes
                gridsmtpRight.Visibility = Visibility.Hidden;
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "SoundGrid_MouseLeftButtonUp :  Exception Error Message =  " + ex.Message, MessageType.Exception);
            }
        }

        private void SoundGrid_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            SoundGrid.Background = Brushes.LightBlue;
        }

        private void SoundGrid_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            try
            {
                var bc = new BrushConverter();
                SoundGrid.Background = (Brush)bc.ConvertFrom("#FFE4EEFF");
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "SoundGrid_MouseLeave :  Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }
        private void radioButtonAudioTrue_Checked(object sender, RoutedEventArgs e)
        {
            lblSoundStatus.Content = "ON";
            lblDesc.Visibility = Visibility.Hidden;
            lbldesc2.Visibility = Visibility.Hidden;
            gridRing1.IsEnabled = true;
            gridRing2.IsEnabled = true;
            gridRing3.IsEnabled = true;
            gridRing4.IsEnabled = true;
        }

        private void radioButtonAudioFalse_Checked(object sender, RoutedEventArgs e)
        {
            lblSoundStatus.Content = "OFF";
            lblDesc.Visibility = Visibility.Visible;
            lbldesc2.Visibility = Visibility.Visible;
            gridRing1.IsEnabled = false;
            gridRing2.IsEnabled = false;
            gridRing3.IsEnabled = false;
            gridRing4.IsEnabled = false;
        }

        private void Ring1Play_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (File.Exists(path + "\\AudioFiles" + "\\Windows Foreground.wav"))
            {
                simpleSound = new SoundPlayer(path + "\\AudioFiles" + "\\Windows Foreground.wav");
                simpleSound.Play();
            }
        }

        private void Ring2Play_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (File.Exists(path + "\\AudioFiles" + "\\Windows Ding.wav"))
            {
                simpleSound = new SoundPlayer(path + "\\AudioFiles" + "\\Windows Ding.wav");
                simpleSound.Play();
            }
        }

        private void Ring3Play_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (File.Exists(path + "\\AudioFiles" + "\\Windows Notify Messaging.wav"))
            {
                simpleSound = new SoundPlayer(path + "\\AudioFiles" + "\\Windows Notify Messaging.wav");
                simpleSound.Play();
            }
        }

        private void Ring4Play_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (File.Exists(path + "\\AudioFiles" + "\\Windows Proximity Notification.wav"))
            {
                simpleSound = new SoundPlayer(path + "\\AudioFiles" + "\\Windows Proximity Notification.wav");
                simpleSound.Play();
            }
        }
        private void btnRing1_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                currentNotificationTone = "\\AudioFiles" + "\\Windows Foreground.wav";
                // Settings.writeINIFile("TOASTSETTING", "toastAudioPath", "\\AudioFiles" + "\\Windows Foreground.wav");
                Ring1PlaySelected.Visibility = Visibility.Visible;
                Ring2PlaySelected.Visibility = Visibility.Hidden;
                Ring3PlaySelected.Visibility = Visibility.Hidden;
                Ring4PlaySelected.Visibility = Visibility.Hidden;
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "btnRing1_Click :  Exception Error Message : " + ex.Message, MessageType.Exception);
            }
        }

        private void btnRing2_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                currentNotificationTone = "\\AudioFiles" + "\\Windows Ding.wav";
                // Settings.writeINIFile("TOASTSETTING", "toastAudioPath", "\\AudioFiles" + "\\Windows Ding.wav");
                Ring1PlaySelected.Visibility = Visibility.Hidden;
                Ring2PlaySelected.Visibility = Visibility.Visible;
                Ring3PlaySelected.Visibility = Visibility.Hidden;
                Ring4PlaySelected.Visibility = Visibility.Hidden;
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "btnRing2_Click :  Exception Error Message : " + ex.Message, MessageType.Exception);
            }
        }

        private void btnRing3_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                currentNotificationTone = "\\AudioFiles" + "\\Windows Notify Messaging.wav";
                //Settings.writeINIFile("TOASTSETTING", "toastAudioPath", "\\AudioFiles" + "\\Windows Notify Messaging.wav");
                Ring1PlaySelected.Visibility = Visibility.Hidden;
                Ring2PlaySelected.Visibility = Visibility.Hidden;
                Ring3PlaySelected.Visibility = Visibility.Visible;
                Ring4PlaySelected.Visibility = Visibility.Hidden;
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "btnRing3_Click :  Exception Error Message : " + ex.Message, MessageType.Exception);
            }
        }

        private void btnRing4_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                currentNotificationTone = "\\AudioFiles" + "\\Windows Proximity Notification.wav";
                // Settings.writeINIFile("TOASTSETTING", "toastAudioPath", "\\AudioFiles" + "\\Windows Proximity Notification.wav");
                Ring1PlaySelected.Visibility = Visibility.Hidden;
                Ring2PlaySelected.Visibility = Visibility.Hidden;
                Ring3PlaySelected.Visibility = Visibility.Hidden;
                Ring4PlaySelected.Visibility = Visibility.Visible;
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "btnRing4_Click :  Exception Error Message : " + ex.Message, MessageType.Exception);
            }
        }

        private void btnApply_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            btnApply.Foreground = Brushes.Black;
        }

        private void btnApply_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            btnApply.Foreground = Brushes.White;
        }

        private void btnCancel_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            btnCancel.Foreground = Brushes.Black;
        }

        private void btnCancel_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            btnCancel.Foreground = Brushes.White;
        }
        //IRDSPM::Praatiksha::21-10-2020::For new sound grid changes --stop

        private void radioButtonshowuiTrue_Click(object sender, RoutedEventArgs e)
        {
            CurrentShowGUI = true;
        }

        private void radioButtonshowuiFalse_Click(object sender, RoutedEventArgs e)
        {
            CurrentShowGUI = false;
        }

        private void radioButtonnotificationTrue_Click(object sender, RoutedEventArgs e)
        {
            currenttoastNoti = true;
        }

        private void radioButtonnotificationFalse_Click(object sender, RoutedEventArgs e)
        {
            currenttoastNoti = false;
        }

        private void radioButtonAudioTrue_Click(object sender, RoutedEventArgs e)
        {
            currentToastAudio = true;
        }

        private void radioButtonAudioFalse_Click(object sender, RoutedEventArgs e)
        {
            currentToastAudio = false;
        }

        private void radioButtonMaximizedTrue_Click(object sender, RoutedEventArgs e)
        {
            currentmaximizedScreen = "Maximized";
        }

        private void radioButtonMAximizedFalse_Click(object sender, RoutedEventArgs e)
        {
            currentmaximizedScreen = "Normal";
        }

        private void MW1_Click(object sender, RoutedEventArgs e)
        {
            if (MW1.IsChecked == true)
            {
                CurrentmwsymbolnameVal = "true";
            }
            else
            {
                CurrentmwsymbolnameVal = "false";
            }
        }

        private void MW2_Click(object sender, RoutedEventArgs e)
        {
            if (MW2.IsChecked == true)
            {
                Currentmwltpval = "true";
            }
            else
            {
                Currentmwltpval = "false";
            }
        }

        private void MW3_Click(object sender, RoutedEventArgs e)
        {
            if (MW3.IsChecked == true)
            {
                Currentmwbidval = "true";
            }
            else
            {
                Currentmwbidval = "false";
            }
        }

        private void MW4_Click(object sender, RoutedEventArgs e)
        {
            if (MW4.IsChecked == true)
            {
                Currentmwaskval = "true";
            }
            else
            {
                Currentmwaskval = "false";
            }
        }

        private void MW5_Click(object sender, RoutedEventArgs e)
        {
            if (MW5.IsChecked == true)
            {
                Currentmwvolumeval = "true";
            }
            else
            {
                Currentmwvolumeval = "false";
            }
        }

        private void MW6_Click(object sender, RoutedEventArgs e)
        {
            if (MW6.IsChecked == true)
            {
                CurrentmwOpen = "true";
            }
            else
            {
                CurrentmwOpen = "false";
            }
        }

        private void MW7_Click(object sender, RoutedEventArgs e)
        {
            if (MW7.IsChecked == true)
            {
                CurrentmwHigh = "true";
            }
            else
            {
                CurrentmwHigh = "false";
            }
        }

        private void MW8_Click(object sender, RoutedEventArgs e)
        {
            if (MW8.IsChecked == true)
            {
                CurrentmwLow = "true";
            }
            else
            {
                CurrentmwLow = "false";
            }
        }

        private void MW9_Click(object sender, RoutedEventArgs e)
        {
            if (MW9.IsChecked == true)
            {
                CurrentmwClose = "true";
            }
            else
            {
                CurrentmwClose = "false";
            }
        }

        private void MW10_Click(object sender, RoutedEventArgs e)
        {
            if (MW10.IsChecked == true)
            {
                Currentmwchangeinper = "true";
            }
            else
            {
                Currentmwchangeinper = "false";
            }
        }

        private void MW11_Click(object sender, RoutedEventArgs e)
        {
            if (MW11.IsChecked == true)
            {
                CurrentmwNetChange = "true";
            }
            else
            {
                CurrentmwNetChange = "false";
            }
        }

        private void OP1_Click(object sender, RoutedEventArgs e)
        {
            if (OP1.IsChecked == true)
            {
                CurrentopExchange = "true";
            }
            else { CurrentopExchange = "false"; }
        }

        private void OP2_Click(object sender, RoutedEventArgs e)
        {
            if (OP2.IsChecked == true)
            {
                CurrentopSymbol = "true";
            }
            else { CurrentopSymbol = "false"; }
        }

        private void OP3_Click(object sender, RoutedEventArgs e)
        {
            if (OP3.IsChecked == true)
            {
                CurrentopProduct = "true";
            }
            else { CurrentopProduct = "false"; }
        }

        private void OP4_Click(object sender, RoutedEventArgs e)
        {
            if (OP4.IsChecked == true)
            {
                CurrentopQuantity = "true";
            }
            else { CurrentopQuantity = "false"; }
        }

        private void OP5_Click(object sender, RoutedEventArgs e)
        {
            if (OP5.IsChecked == true)
            {
                CurrentopLtp = "true";
            }
            else { CurrentopLtp = "false"; }
        }

        private void OP6_Click(object sender, RoutedEventArgs e)
        {
            if (OP6.IsChecked == true)
            {
                CurrentopPrice = "true";
            }
            else { CurrentopPrice = "false"; }
        }

        private void OP7_Click(object sender, RoutedEventArgs e)
        {
            if (OP7.IsChecked == true)
            {
                CurrentopPL = "true";
            }
            else { CurrentopPL = "false"; }
        }

        private void OB1_Click(object sender, RoutedEventArgs e)
        {
            if (OB1.IsChecked == true)
            { CurrentobEXG = "true"; }
            else { CurrentobEXG = "false"; }
        }

        private void OB2_Click(object sender, RoutedEventArgs e)
        {
            if (OB2.IsChecked == true)
            { CurrentobSymbol = "true"; }
            else { CurrentobSymbol = "false"; }
        }

        private void OB3_Click(object sender, RoutedEventArgs e)
        {
            if (OB3.IsChecked == true)
            { CurrentobOrderId = "true"; }
            else { CurrentobOrderId = "false"; }
        }

        private void OB4_Click(object sender, RoutedEventArgs e)
        {
            if (OB4.IsChecked == true)
            { CurrentobTime = "true"; }
            else { CurrentobTime = "false"; }
        }

        private void OB5_Click(object sender, RoutedEventArgs e)
        {
            if (OB5.IsChecked == true)
            { CurrentobType = "true"; }
            else { CurrentobType = "false"; }
        }

        private void OB6_Click(object sender, RoutedEventArgs e)
        {
            if (OB6.IsChecked == true)
            { CurrentobOrderType = "true"; }
            else { CurrentobOrderType = "false"; }
        }

        private void OB7_Click(object sender, RoutedEventArgs e)
        {
            if (OB7.IsChecked == true)
            { CurrentobProduct = "true"; }
            else { CurrentobProduct = "false"; }
        }

        private void OB8_Click(object sender, RoutedEventArgs e)
        {
            if (OB8.IsChecked == true)
            { CurrentobPrice = "true"; }
            else { CurrentobPrice = "false"; }
        }

        private void OB9_Click(object sender, RoutedEventArgs e)
        {
            if (OB9.IsChecked == true)
            { CurrentobTriggerPrice = "true"; }
            else { CurrentobTriggerPrice = "false"; }
        }

        private void OB10_Click(object sender, RoutedEventArgs e)
        {
            if (OB10.IsChecked == true)
            { CurrentobQTY = "true"; }
            else { CurrentobQTY = "false"; }
        }

        private void OB11_Click(object sender, RoutedEventArgs e)
        {
            if (OB11.IsChecked == true)
            { CurrentobStatus = "true"; }
            else { CurrentobStatus = "false"; }
        }

        private void OB12_Click(object sender, RoutedEventArgs e)
        {
            if (OB12.IsChecked == true)
            { CurrentobFilledQty = "true"; }
            else { CurrentobFilledQty = "false"; }
        }

        private void OB13_Click(object sender, RoutedEventArgs e)
        {
            if (OB13.IsChecked == true)
            { CurrentobFilledPrice = "true"; }
            else { CurrentobFilledPrice = "false"; }
        }

        private void OB14_Click(object sender, RoutedEventArgs e)
        {
            if (OB14.IsChecked == true)
            { CurrentobRejectionOrderRemark = "true"; }
            else { CurrentobRejectionOrderRemark = "false"; }
        }

        private void OH1_Click(object sender, RoutedEventArgs e)
        {
            if (OH1.IsChecked == true)
            { CurrentohEXG = "true"; }
            else { CurrentohEXG = "false"; }
        }

        private void OH2_Click(object sender, RoutedEventArgs e)
        {
            if (OH2.IsChecked == true)
            { CurrentohSymbol = "true"; }
            else { CurrentohSymbol = "false"; }
        }

        private void OH3_Click(object sender, RoutedEventArgs e)
        {
            if (OH3.IsChecked == true)
            { CurrentohProduct = "true"; }
            else { CurrentohProduct = "false"; }
        }

        private void OH4_Click(object sender, RoutedEventArgs e)
        {
            if (OH4.IsChecked == true)
            { CurrentohQuantity = "true"; }
            else { CurrentohQuantity = "false"; }
        }

        private void OH5_Click(object sender, RoutedEventArgs e)
        {
            if (OH5.IsChecked == true)
            { Currentohcloseprice = "true"; }
            else { Currentohcloseprice = "false"; }
        }

        private void OH6_Click(object sender, RoutedEventArgs e)
        {
            if (OH6.IsChecked == true)
            { Currentohavgprice = "true"; }
            else { Currentohavgprice = "false"; }
        }

        private void OH7_Click(object sender, RoutedEventArgs e)
        {
            if (OH7.IsChecked == true)
            { CurrentohPL = "true"; }
            else { CurrentohPL = "false"; }
        }

        private void NotificationGrid_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            gridHomeRight.Visibility = Visibility.Hidden;
            gridScreenRight.Visibility = Visibility.Hidden;
            gridcolumnSetupRight.Visibility = Visibility.Hidden;
            gridSoundRight.Visibility = Visibility.Hidden;
            gridNotificationRight.Visibility = Visibility.Visible;
            //IRDSPM::Pratiksha::14-07-2021::For SMTP changes
            gridsmtpRight.Visibility = Visibility.Hidden;
        }

        private void NotificationGrid_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            NotificationGrid.Background = Brushes.LightBlue;
        }

        private void NotificationGrid_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            try
            {
                var bc = new BrushConverter();
                NotificationGrid.Background = (Brush)bc.ConvertFrom("#FFE4EEFF");
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "ScreenGrid_MouseLeave :  Exception Error Message = " + ex.Message, MessageType.Informational);
            }
        }

        private void radioButtonSMSTrue_Click(object sender, RoutedEventArgs e)
        {
            CurrentisSmsEnable = true;
        }

        private void radioButtonSMSFalse_Click(object sender, RoutedEventArgs e)
        {
            CurrentisSmsEnable = false;
        }

        private void radioButtonEmailTrue_Click(object sender, RoutedEventArgs e)
        {
            CurrentisEmailEnable = true;
        }

        private void radioButtonEmailFalse_Click(object sender, RoutedEventArgs e)
        {
            CurrentisEmailEnable = false;
        }

        private void radioButtonserviceTrue_Click(object sender, RoutedEventArgs e)
        {
            if (radioButtonserviceTrue.IsChecked == true)
            {
                CurrentisSend = true;
            }

            if (CheckSms.IsChecked == true)
            {
                smsGroupbox.IsEnabled = true;
            }
            else
            {
                smsGroupbox.IsEnabled = false;
            }
            if (CheckEmail.IsChecked == true)
            {
                EmailGroupbox.IsEnabled = true;
            }
            else
            {
                EmailGroupbox.IsEnabled = false;
            }
            CheckSms.IsEnabled = true;
            CheckEmail.IsEnabled = true;
            IntervalGroupbox.IsEnabled = true;
        }

        private void radioButtonserviceFalse_Click(object sender, RoutedEventArgs e)
        {
            if (radioButtonserviceFalse.IsChecked == true)
            {
                CurrentisSend = false;
            }
            CheckSms.IsEnabled = false;
            CheckEmail.IsEnabled = false;
            smsGroupbox.IsEnabled = false;
            EmailGroupbox.IsEnabled = false;
            IntervalGroupbox.IsEnabled = false;
        }

        private void CheckSms_Checked(object sender, RoutedEventArgs e)
        {
            if (CheckSms.IsChecked == true)
            {
                smsGroupbox.IsEnabled = true;
                CurrentisSmsEnable = true;
            }
        }

        private void CheckSms_Unchecked(object sender, RoutedEventArgs e)
        {
            if (CheckSms.IsChecked == false)
            {
                smsGroupbox.IsEnabled = false;
                CurrentisSmsEnable = false;
            }
        }
        private void CheckEmail_Checked(object sender, RoutedEventArgs e)
        {
            if (CheckEmail.IsChecked == true)
            {
                EmailGroupbox.IsEnabled = true;
                CurrentisEmailEnable = true;
            }
        }

        private void CheckEmail_Unchecked(object sender, RoutedEventArgs e)
        {
            if (CheckEmail.IsChecked == false)
            {
                EmailGroupbox.IsEnabled = false;
                CurrentisEmailEnable = false;
            }
        }

        //IRDSPM::Pratiksha::14-07-2021::For SMTP changes
        private void SmtpGrid_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            gridHomeRight.Visibility = Visibility.Hidden;
            gridScreenRight.Visibility = Visibility.Hidden;
            gridcolumnSetupRight.Visibility = Visibility.Hidden;
            gridSoundRight.Visibility = Visibility.Hidden;
            gridNotificationRight.Visibility = Visibility.Hidden;
            gridsmtpRight.Visibility = Visibility.Visible;
        }

        private void SmtpGrid_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            SmtpGrid.Background = Brushes.LightBlue;
        }

        private void SmtpGrid_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            try
            {
                var bc = new BrushConverter();
                SmtpGrid.Background = (Brush)bc.ConvertFrom("#FFE4EEFF");
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "SmtpGrid_MouseLeave :  Exception Error Message =  " + ex.Message, MessageType.Exception);
            }
        }

        private void RBOtherNotificationFalse_Click(object sender, RoutedEventArgs e)
        {
            if(RBOtherNotificationFalse.IsChecked == true)
            {
                currentOtherNotification = false;
            }
        }

        private void RBOtherNotificationTrue_Click(object sender, RoutedEventArgs e)
        {
            if (RBOtherNotificationTrue.IsChecked == true)
            {
                currentOtherNotification = true;
            }
        }
    }
}
