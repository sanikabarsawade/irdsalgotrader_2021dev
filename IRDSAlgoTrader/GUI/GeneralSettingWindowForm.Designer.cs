﻿using System.IO;
using System.Reflection;
namespace IRDSAlgoOMS.GUI
{
    partial class GeneralSettingWindowForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /// 
        //IRDSPM::Pratiksha::12-02-2021::dynamically set icon
        Assembly myAssembly = Assembly.GetExecutingAssembly();
        Stream IconStream = null;
        private void InitializeComponent(GeneralSettingWindowForm obj, dynamic m_ObjkiteConnectWrapper, string title)
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.elementHost1 = new System.Windows.Forms.Integration.ElementHost();
            this.generalSettingWPFForm1 = new IRDSAlgoOMS.GUI.GeneralSettingWPFForm(obj, m_ObjkiteConnectWrapper);
            this.SuspendLayout();
            // 
            // elementHost1
            // 
            this.elementHost1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.elementHost1.Location = new System.Drawing.Point(0, 0);
            this.elementHost1.Name = "elementHost1";
            this.elementHost1.Size = new System.Drawing.Size(604, 411);
            this.elementHost1.TabIndex = 0;
            this.elementHost1.Text = "elementHost1";
            this.elementHost1.Child = this.generalSettingWPFForm1;
            // 
            // GeneralSettingWindowForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(604, 411);
            this.Controls.Add(this.elementHost1);

            IconStream = myAssembly.GetManifestResourceStream("IRDSAlgoOMS.Resources." + title + ".ico");
            this.Icon = new System.Drawing.Icon(IconStream);
            this.Name = "GeneralSettingWindowForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.MaximizeBox = false;
            this.Text = "General Setting";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Integration.ElementHost elementHost1;
        private GeneralSettingWPFForm generalSettingWPFForm1;
    }
}