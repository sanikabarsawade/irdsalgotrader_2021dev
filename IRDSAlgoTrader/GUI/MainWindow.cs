﻿using IRDSAlgoOMS.GUI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;

namespace IRDSAlgoOMS
{
    public partial class MainWindow : Form
    {
        bool m_isFromEXE = true;
        dynamic m_ObjkiteConnectWrapper = null;
        //IRDSPM::Pratiksha::16-09-2020::For resize window from ini
        ConfigSettings Settings = null;
        static string path = Directory.GetCurrentDirectory();
        string iniFileSetting = path + @"\Configuration\" + "Setting.ini";
        Logger logger = Logger.Instance;
        string ScreenVal = "";
        string title = "";
        public MainWindow()
        {
            try
            {
                //IRDSPM::PRatiksha::03-12-2020::To open SplashScreen in beginning
                //Thread t = new Thread(new ThreadStart(StartForm));
                //t.Start();
                //Thread.Sleep(5000);
                
                //t.Abort();
                this.Closing += MainWindow_Closing;
                if (File.Exists(iniFileSetting))
                {
                    if (Settings == null)
                    {
                        Settings = new ConfigSettings(logger);
                    }
                    Settings.ReadSettingConfigFile(iniFileSetting);
                    ScreenVal = Settings.maximizedScreen;
                    title = Settings.m_Title;
                    if(title == "")
                    {
                        title = "IRDS Algo Trader";
                    }
                }                
                if (ScreenVal == "Normal")
                {
                    this.CenterWindowOnScreen();
                    this.WindowState = FormWindowState.Normal;
                }
                //IRDSPM::Pratiksha::10-12-2020::For maximized
                else if (ScreenVal == "Maximized")
                {
                    this.WindowState = FormWindowState.Maximized;
                }
                this.TopMost = true;
                InitializeComponent(title);
                this.Load += MainWindow_Load1;
            }
            catch (Exception e)
            {
                File.AppendAllText("StartGUI.txt", "exception " + e.Message);
            }
        }

        private void MainWindow_Load1(object sender, EventArgs e)
        {
            this.TopMost = false;
        }
        //IRDSPM::PRatiksha::03-12-2020::For SplashScreen
        //public void StartForm()
        //{
        //    try
        //    {
        //        System.Windows.Forms.Application.Run(new Splash());
        //    }
        //    catch (Exception e)
        //    {
        //        File.AppendAllText("StartGUI.txt", "exception " + e.Message);
        //    }
        //}

        //IRDS::Jyoti::08-09-2020::Samco Integration changes
        public void loadValues(dynamic ObjkiteConnectWrapper,string userId,List<string>symbols,bool isFromEXE)
        {
            try
            {
                this.wpfMainControl1.m_ObjkiteConnectWrapper = ObjkiteConnectWrapper;
                this.wpfMainControl1.m_UserID = userId;
                this.wpfMainControl1.SymbolList = symbols;
                m_isFromEXE = isFromEXE;
                m_ObjkiteConnectWrapper = ObjkiteConnectWrapper;
                this.wpfMainControl1.m_MainWindow = this;
                this.wpfMainControl1.m_TickDataDownloader = m_ObjkiteConnectWrapper.GetTickDataDownloaderFlag();
                this.wpfMainControl1.StartTimer();
            }
            catch(Exception e)
            {
                File.AppendAllText("StartGUI.txt", "exception " + e.Message);
            }
        }

        public void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                string message = "Do you really want to exit from application?";
                //IRDSPM::Pratiksha::29-07-2020::Change the title
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                DialogResult dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                if (dialog == DialogResult.Yes)
                {
                    if (m_isFromEXE)
                    {
                        if (m_ObjkiteConnectWrapper != null)
                        {
                            //m_ObjkiteConnectWrapper.StopThread();
                            //sanika::29-sep-2020::stop thread brfore exist
                            this.wpfMainControl1.StopThread();
                        }
                        Thread.Sleep(2000);
                        Environment.Exit(0);
                    }
                }
                else
                {
                    e.Cancel = true;
                }
            }
            catch(Exception er)
            {
                //Environment.Exit(0);
            }
            //this.MyUserControl.MethodToBeCalledWhenUnloaded().
        }

        //IRDSPM::PRatiksha::15-08-2020::on minimize alignment center, not worked yet
        private void MainWindow_Resize(object sender, EventArgs e)
        {
            //IRDSPM::PRatiksha::17-08-2020::For minimized change
            //if ((this.WindowState.ToString() == "Normal") || (this.WindowState.ToString() == "Minimized"))
            if ((this.WindowState.ToString() == "Normal"))
            {
                this.CenterWindowOnScreen();
                this.WindowState = FormWindowState.Normal;
            }
            //if (File.Exists(iniFileSetting))
            //{
            //    //IRDSPM::Pratiksha::10-12-2020::Not saved minimized value
            //    if (this.WindowState.ToString() != "Minimized" && ScreenVal != this.WindowState.ToString())
            //    {
            //        //sanika::1-dec-2020::create object if it is null 
            //        if (Settings == null)
            //        {
            //            Settings = new ConfigSettings();
            //        }
            //        Settings.writeINIFile("screenMode", "screenMode", this.WindowState.ToString());
            //    }
            //}
        }
        //IRDSPM::PRatiksha::17-08-2020::125% and 100% Screen resolution change for minimized window --start
        private void CenterWindowOnScreen()
        {
            double screenWidth = System.Windows.SystemParameters.PrimaryScreenWidth;
            double screenHeight = System.Windows.SystemParameters.PrimaryScreenHeight;
            double windowWidth = this.Width;
            double windowHeight = this.Height;
            if ((screenWidth > 1100) && (screenHeight > 650))
            {
                this.Left = Convert.ToInt32((screenWidth / 2) - (windowWidth / 2));
                this.Left = Convert.ToInt32((screenWidth - windowWidth) / 2);
                this.Top = Convert.ToInt32((screenHeight - windowHeight) / 2);
            }
            else
            {
                this.Top = Convert.ToInt32((screenHeight / 2) - (windowHeight / 0.2));
                this.Top = Convert.ToInt32((screenHeight - windowHeight) / 0.2);
                this.Left = Convert.ToInt32((screenWidth - windowWidth) / 0.9);
            }
        }
        //IRDSPM::PRatiksha::17-08-2020::125% and 100% Screen resolution change for minimized window --end
    }
}
