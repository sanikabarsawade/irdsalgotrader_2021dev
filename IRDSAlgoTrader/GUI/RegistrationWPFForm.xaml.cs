﻿using IRDSAlgoOMS.Common;
using RegistrationProcess;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace IRDSAlgoOMS.GUI
{
    public partial class RegistrationWPFForm : System.Windows.Controls.UserControl
    {
        public bool IsRegisterSuccess = false;
        string message = "";
        //IRDSPM::Pratiksha::08-02-2021::Dynamic name
        string title = "";
        MessageBoxButtons buttons;
        DialogResult dialog;
        RegistrationWindowForm m_registerFormObj = null;
        string path = Directory.GetCurrentDirectory();
        string strIniFilePath = null;
        INIFile m_iniFileObj = null;
        public double trialPeriod = 0;
        //Pratiksha::26-02-2021::For server request
        string token;
        Register m_obj;
        public RegistrationWPFForm(RegistrationWindowForm obj, double trialdays, string m_title, string m_token)
        {
            InitializeComponent();
            title = m_title;
            token = m_token;
            m_registerFormObj = obj;
            trialPeriod = trialdays;
            NetworkChange.NetworkAvailabilityChanged += new NetworkAvailabilityChangedEventHandler(OnNetworkAvailabilityChanged);
            strIniFilePath = path + @"\Configuration\" + "RegCredentials.INFO";
            //sanika::09-oct-2020::get hardware id
            string id = GetHardwareID();
            //System.Windows.MessageBox.Show(id);
            //sanika::09-oct-2020::set hardware id
            textBox4.Text = id;
            textBox4.IsEnabled = false;
            try
            {
                lblTitle.Content = "Free Trial Registration for " + trialPeriod + " Days!";
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : " + e.Message);
            }
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            WriteUniquelogs("RegistrationProcess", "Fields are cleared.", MessageType.Informational);
        }
        public void WriteUniquelogs(string fileName, string message, MessageType msgType)
        {
            try
            {
                SymbolLogs log = new SymbolLogs();
                //log = SymbolLogs.InstanceSymbolLogs;
                log.createFile(fileName);
                log.LogMessage(message, MessageType.Informational);
                log = null;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : " + e.Message);
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            //IRDSPM::Pratiksha::18-11-2020::If made any change then it should ask for exit
            if (textBox1.Text.Length != 0 || textBox2.Text.Length != 0 || textBox3.Text.Length != 0 || textBox5.Text.Length != 0)
            {
                message = "Are you sure you want to exit from AlgoTrader?";
                buttons = MessageBoxButtons.YesNo;
                DialogResult result = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {
                    Environment.Exit(0);
                }
            }
            else
            {
                Environment.Exit(0);
            }
        }

        private void btnApply_Click(object sender, RoutedEventArgs e)
        {
            Boolean bolUpdateuserinformation = false;
            // string strIniFilePath = Environment.CurrentDirectory + "\\" + "iTableManager.INFO";
            string strNameInfo = textBox1.Text.Trim();
            string strEmailId = textBox2.Text.Trim();
            string strMobileNo = textBox3.Text.Trim();
            string strUserName = "";
            string strPassword = "";
            string strAddress = "";
            string strCity = "";
            string strsState = "";
            string strRemark = "";
            string strClientID = textBox4.Text.Trim();
            string strUserType = "User";
            string strStatus = "";
            string strOnline = "";
            string strSessionAdminId = "admin";
            string strAffiliateId = textBox5.Text;
            DateTime dtCurrentDate = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));
            DateTime dtExpiryDate = dtCurrentDate.AddDays(trialPeriod);//sanika::3-nov-2020::changed 15 to 7 days
            double dexpirydate = dtExpiryDate.ToOADate();

            double dLastlogin = dtCurrentDate.ToOADate();
            try
            {
                bool bolAdduserinformation = true;

                //validation for name of user
                if (textBox1.Text.Trim() == "")
                {
                    message = "Please enter name.";
                    buttons = MessageBoxButtons.OK;
                    System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                    textBox1.Focus();
                    return;
                }
                else
                {
                    Match nameValidation = Regex.Match(textBox1.Text, @"^[a-zA-z ]*$");
                    if (!nameValidation.Success)
                    {
                        message = "Please enter valid name.";
                        buttons = MessageBoxButtons.OK;
                        System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                        textBox1.Focus();
                        return;
                    }
                }

                //validation for EMailID of user
                if ((textBox2.Text != "") || (textBox2.Text != null))
                {
                    Match emailValidation = Regex.Match(textBox2.Text, @"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$");
                    if (!emailValidation.Success)
                    {
                        message = "Please enter valid email id.";
                        buttons = MessageBoxButtons.OK;
                        System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                        textBox2.Focus();
                        return;
                    }
                }

                //Validation for Mobile number of user
                if ((textBox3.Text != "") || (textBox3.Text != null))
                {
                    Match emailValidation = Regex.Match(textBox3.Text, @"^\s*\+?[0-9]{0,3}\d?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$");
                    if (!emailValidation.Success)
                    {
                        message = "Please enter valid mobile number.";
                        buttons = MessageBoxButtons.OK;
                        System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                        textBox3.Focus();
                        return;
                    }
                }

                //Validation for Client ID
                if ((textBox4.Text == "") || (textBox4.Text == null))
                {
                    //Match clientidValidation = Regex.Match(textBox4.Text, @"^(\w{4}-\w{4}-\w{4}-\w{4}-\w{4})$");
                    //if (!clientidValidation.Success)
                    {
                        message = "Please enter valid clientID.";
                        buttons = MessageBoxButtons.OK;
                        System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                        textBox4.Focus();
                        return;
                    }
                }

                // for affiliate 
                if ((textBox5.Text == "") || (textBox5.Text == null))
                {
                    message = "Please enter valid affiliateID.";
                    buttons = MessageBoxButtons.OK;
                    System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                    textBox5.Focus();
                    return;
                }
                //IRDSPM::PRatiksha::06-03-2021::For retrive user details from server
                if (m_obj == null)
                {
                    m_obj = new Register();
                }
                //if (m_obj.RetrieveUserdetails(strClientID, token) == 0)
                //{
                //    WriteUniquelogs("RegistrationProcess", "Going for adding new user", MessageType.Informational);
                //    //IRDSPM::PRatiksha::26-02-2021::For adding new user details from server
                //    bolAdduserinformation = m_obj.AddUserInformation(token, strNameInfo, strUserType, strEmailId, strMobileNo, strUserName, strPassword, strAddress, strCity, strsState, strClientID, dexpirydate, strRemark, strStatus, strOnline, strSessionAdminId, strAffiliateId, dLastlogin, false);
                //    if (bolAdduserinformation)
                //    {
                //        //sanika::9-oct-2020::added parameter mobile no
                //        WritingintoINI(strClientID, dexpirydate);//, strMobileNo);
                //        WriteUniquelogs("RegistrationProcess", "set_Click : Register successfully.", MessageType.Exception);
                //        //IRDSPM::PRatiksha::26-02-2021::For close form condition
                //        IsRegisterSuccess = true;
                //        message = "Registration successful!!!";
                //        buttons = MessageBoxButtons.OK;
                //        dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                //        if (dialog == DialogResult.OK)
                //        {
                //            m_registerFormObj.Close();
                //        }
                //    }
                //    else
                //    {
                //        //IRDSPM::PRatiksha::26-02-2021::For close form
                //        m_obj.IsRegisterSuccess = false;
                //        IsRegisterSuccess = false;
                //        WriteUniquelogs("RegistrationProcess", "set_Click : Registration failed.", MessageType.Exception);
                //        message = "Registration failed!!!";
                //        buttons = MessageBoxButtons.OK;
                //        dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                //    }
                //}
                System.Windows.MessageBox.Show("Code commented");
            }
            catch (Exception ex)
            {
                WriteUniquelogs("RegistrationProcess", "set_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        //sanika::9-oct-2020::added parameter mobile no
        public void WritingintoINI(string strClientID, double dexpirydate)//,string strMobileNo)
        {
            try
            {
                if (System.IO.File.Exists(strIniFilePath))
                {
                    m_iniFileObj = new INIFile(strIniFilePath);
                    m_iniFileObj.TradingIniWriteValue("X1eaJ0kA6ril", "LFY8V3Y0YQe", EncryptData(dexpirydate.ToString(), "IrdsalgoTrader"), strIniFilePath);
                    //sanika::1-oct-2020::strClientID converted to upper while saving
                    m_iniFileObj.TradingIniWriteValue("X1eaJ0kA6ril", "0xiW6AjUO8", EncryptData(strClientID, "IrdsalgoTrader"), strIniFilePath);
                    //added for write date into file
                    m_iniFileObj.TradingIniWriteValue("X1eaJ0kA6ril", "0xiW6Ajwsa", EncryptData(DateTime.Now.ToString("M-d-yyyy"), "IrdsalgoTrader"), strIniFilePath);

                }
                else
                {
                    m_iniFileObj = new INIFile(strIniFilePath);
                    FileStream Fs = new FileStream(strIniFilePath, FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite);
                    System.IO.StreamWriter Sw = new System.IO.StreamWriter(Fs);
                    m_iniFileObj.TradingIniWriteValue("X1eaJ0kA6ril", "LFY8V3Y0YQe", EncryptData(dexpirydate.ToString(), "IrdsalgoTrader"), strIniFilePath);
                    //sanika::1-oct-2020::strClientID converted to upper while saving
                    m_iniFileObj.TradingIniWriteValue("X1eaJ0kA6ril", "0xiW6AjUO8", EncryptData(strClientID, "IrdsalgoTrader"), strIniFilePath);
                    //added for write date into file
                    m_iniFileObj.TradingIniWriteValue("X1eaJ0kA6ril", "0xiW6Ajwsa", EncryptData(DateTime.Now.ToString("M-d-yyyy"), "IrdsalgoTrader"), strIniFilePath);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs("RegistrationProcess", "WritingintoINI : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        public static string EncryptData(string strInText, string strKey)
        {
            byte[] bytesBuff = Encoding.Unicode.GetBytes(strInText);

            using (Aes aes__1 = Aes.Create())
            {
                Rfc2898DeriveBytes crypto = new Rfc2898DeriveBytes(strKey, new byte[] {
                0x49,
                0x76,
                0x61,
                0x6e,
                0x20,
                0x4d,
                0x65,
                0x64,
                0x76,
                0x65,
                0x64,
                0x65,
                0x76
            });
                aes__1.Key = crypto.GetBytes(32);
                aes__1.IV = crypto.GetBytes(16);

                using (MemoryStream mStream = new MemoryStream())
                {
                    using (CryptoStream cStream = new CryptoStream(mStream, aes__1.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cStream.Write(bytesBuff, 0, bytesBuff.Length);
                        cStream.Close();
                    }

                    strInText = Convert.ToBase64String(mStream.ToArray());
                }
            }

            return strInText;
        }
        private void btnApply_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            btnApply.Foreground = Brushes.Black;
        }

        private void btnApply_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            btnApply.Foreground = Brushes.White;
        }

        private void btnCancel_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            btnCancel.Foreground = Brushes.Black;
        }

        private void btnCancel_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            btnCancel.Foreground = Brushes.White;
        }
        static void OnNetworkAvailabilityChanged(object sender, NetworkAvailabilityEventArgs e)
        {
            ConfigSettings m_configObj = null;
            if (e.IsAvailable)
            {
                Console.WriteLine("Network has become available");
            }
            else
            {
                bool flag = false;
                try
                {
                    using (var client = new WebClient())
                    using (client.OpenRead("http://clients3.google.com/generate_204"))
                    {
                        flag = true;
                    }
                }
                catch
                {
                    flag = false;
                }
                if (!flag)
                {
                    if (m_configObj == null)
                    {
                        m_configObj = new ConfigSettings();
                    }
                    string m_path = Directory.GetCurrentDirectory();
                    m_configObj.ReadSettingConfigFile(m_path + @"\Configuration\" + "Setting.ini");
                    string title = m_configObj.m_Title;
                    string message = "Please check internet connection!!";
                    MessageBoxButtons buttons = MessageBoxButtons.OK;
                    DialogResult dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                }
            }
        }

        public string GetHardwareID()
        {
            string hardwareId = "";
            hardwareId = License.Status.HardwareID;
            return hardwareId;
        }
    }
}
