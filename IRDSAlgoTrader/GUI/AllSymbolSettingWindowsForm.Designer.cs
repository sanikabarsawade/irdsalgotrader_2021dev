﻿using System.IO;
using System.Reflection;

namespace IRDSAlgoOMS.GUI
{
    partial class AllSymbolSettingWindowsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        /// 
        //IRDSPM::Pratiksha::12-02-2021::dynamically set icon
        Assembly myAssembly = Assembly.GetExecutingAssembly();
        Stream IconStream = null;
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
		//IRDSPM::Pratiksha::08-02-2021::Added parameter
        private void InitializeComponent(dynamic kiteConnectWrapper, AllSymbolSettingWindowsForm obj, string title)
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.elementHost1 = new System.Windows.Forms.Integration.ElementHost();
            this.allSymbolSettingWPFForm1 = new IRDSAlgoOMS.GUI.AllSymbolSettingWPFForm(kiteConnectWrapper, obj, title);
            this.SuspendLayout();
            // 
            // elementHost1
            // 
            this.elementHost1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.elementHost1.Location = new System.Drawing.Point(0, 0);
            this.elementHost1.Name = "elementHost1";
            this.elementHost1.Size = new System.Drawing.Size(550, 550);
            this.elementHost1.TabIndex = 0;
            this.elementHost1.Text = "elementHost1";
            this.elementHost1.Child = this.allSymbolSettingWPFForm1;
            // 
            // AllSymbolSettingWindowsForm
            // 
            this.ClientSize = new System.Drawing.Size(550, 550);
            this.Controls.Add(this.elementHost1);
            this.Name = "AllSymbolSettingWindowsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.MaximizeBox = false;
            this.Text = "Symbol Setting";
            this.ResumeLayout(false);
            IconStream = myAssembly.GetManifestResourceStream("IRDSAlgoOMS.Resources." + title + ".ico");
            this.Icon = new System.Drawing.Icon(IconStream);
        }

        #endregion

        private System.Windows.Forms.Integration.ElementHost elementHost1;
        private AllSymbolSettingWPFForm allSymbolSettingWPFForm1;
    }
}