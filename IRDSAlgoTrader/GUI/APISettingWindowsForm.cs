﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IRDSAlgoOMS.GUI
{
    public partial class APISettingWindowsForm : Form
    {
       // KiteConnectWrapper m_objKiteConnectWrapper = null;
        public APISettingWindowsForm(dynamic kiteConnectWrapper, string loginStatus, string title)
        {
            this.TopMost = true;
            InitializeComponent(kiteConnectWrapper, loginStatus, this, title);
            this.Load += APISettingWindowsForm_Load;
        }

        private void APISettingWindowsForm_Load(object sender, EventArgs e)
        {
            this.TopMost = false;
        }

        public void LoadKiteConnectObject(dynamic kiteConnectWrapper, string loginStatus)
        {
            apiSettingWPFForm1.LoadKiteConnectObject(kiteConnectWrapper, loginStatus);
        }
    }
}
