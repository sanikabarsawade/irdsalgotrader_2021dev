﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IRDSAlgoOMS.GUI
{
    public partial class ManuallyLogin : Form
    {
        Kite m_Kite = null;
        public string m_token = "";
        string m_url = "";
        public ManuallyLogin(Kite kite,string url)
        {
            InitializeComponent();
            m_Kite = kite;
            m_url = url;
            //txtUrl.Text = url;
            linkLabel1.Text = url;
            lblToken.BackColor = Color.Yellow;
        }
       

        private void button1_Click(object sender, EventArgs e)
        {
            string token = txtToken.Text;
            this.Close();
        }

        public string Token
        {
            get
            {
                return txtToken.Text;
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start(m_url);
        }

        private void ManuallyLogin_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Close();
        }
    }
}
