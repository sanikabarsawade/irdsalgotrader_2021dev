﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IRDSAlgoOMS.GUI
{
    public partial class SubscriptionDetWindowsForm : Form
    {
        dynamic ObjkiteConnectWrapper = null;
        public SubscriptionDetWindowsForm(dynamic ObjkiteConnectWrapper, string title)
        {
            this.TopMost = true;
            InitializeComponent(this, ObjkiteConnectWrapper, title);
            this.Load += SubscriptionDetWindowsForm_Load;
        }

        private void SubscriptionDetWindowsForm_Load(object sender, EventArgs e)
        {
            this.TopMost = false;
        }
    }
}
