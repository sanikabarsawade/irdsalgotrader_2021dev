﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IRDSAlgoOMS
{
    public partial class RegistrationForm : Form
    {
        //IRDSPM::Pratiksha::03-07-2020::For setting ini
        string bgcolor = "";
        string fontcolor = "";
        string fontDet = "";
        string fontStyle = "";
        Color bColor;
        Color fColor;
        string iniFileSetting;
        string path = Directory.GetCurrentDirectory();
        ConfigSettings Settings1;
        Logger logger = Logger.Instance;
        bool bolAdduserinformation = true;
        Boolean bolUpdateuserinformation = false;
        public RegistrationForm()
        {
            InitializeComponent();
            //IRDSPM::Pratiksha::03-07-2020::For reading and assigning color to background --start
            iniFileSetting = path + @"\Configuration\" + "Setting.ini";
            if (File.Exists(iniFileSetting))
            {
                Settings1 = new ConfigSettings(logger);
                Settings1.ReadSettingConfigFile(iniFileSetting);
                bgcolor = Settings1.bgcolorname.ToString();
                fontcolor = Settings1.fontcolorname.ToString();
                fontDet = Settings1.fontdet.ToString();
                fontStyle = Settings1.fontstyle.ToString();
            }
            string[] colorDetfromINI = { bgcolor, fontcolor };
            for (int i = 0; i < colorDetfromINI.Length; i++)
            {
                Match m = Regex.Match(colorDetfromINI[i], @"A=(?<Alpha>\d+),\s*R=(?<Red>\d+),\s*G=(?<Green>\d+),\s*B=(?<Blue>\d+)");
                if (m.Success)
                {
                    int alpha = int.Parse(m.Groups["Alpha"].Value);
                    int red = int.Parse(m.Groups["Red"].Value);
                    int green = int.Parse(m.Groups["Green"].Value);
                    int blue = int.Parse(m.Groups["Blue"].Value);
                    if (i == 0)
                    {
                        bColor = Color.FromArgb(alpha, red, green, blue);
                    }
                    else
                    {
                        fColor = Color.FromArgb(alpha, red, green, blue);
                    }
                }
                else
                {
                    if (i == 0)
                    {
                        bColor = Color.FromName(colorDetfromINI[i]);
                    }
                    else
                    {
                        fColor = Color.FromName(colorDetfromINI[i]);
                    }
                }
            }
            //IRDSPM::Pratiksha::29 - 07 - 2020::not to change the back and front color
            //panel1.BackColor = bColor;
            //groupBox1.BackColor = bColor;
            //groupBox2.BackColor = bColor;
            //groupBox3.BackColor = bColor;
            //BackColor = bColor;

            //Name.ForeColor = fColor;
            //Email.ForeColor = fColor;
            //Mobileno.ForeColor = fColor;
            //ClientID.ForeColor = fColor;
            //Adminid.ForeColor = fColor;
            //groupBox4.ForeColor = fColor;
            try
            {
                string fontfamilyAndSize = fontDet.Substring(fontDet.IndexOf("=") + 1, fontDet.IndexOf(",") - 1);
                string fontname = fontfamilyAndSize.Substring(0, fontfamilyAndSize.IndexOf(","));
                int pFrom = fontfamilyAndSize.IndexOf("=") + "=".Length;
                int pTo = fontfamilyAndSize.LastIndexOf(",");
                string result = fontfamilyAndSize.Substring(pFrom, fontfamilyAndSize.Length - pFrom);
                float fontSize = float.Parse(result);

                Name.Font = new Font(fontname, fontSize, FontStyle.Bold, GraphicsUnit.Point);
                Email.Font = new Font(fontname, fontSize, FontStyle.Bold, GraphicsUnit.Point);
                Mobileno.Font = new Font(fontname, fontSize, FontStyle.Bold, GraphicsUnit.Point);
                ClientID.Font = new Font(fontname, fontSize, FontStyle.Bold, GraphicsUnit.Point);
                Adminid.Font = new Font(fontname, fontSize, FontStyle.Bold, GraphicsUnit.Point);

                textBox1.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                textBox2.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                textBox3.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                textBox4.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                textBox5.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                groupBox4.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                WriteUniquelogs("RegistrationForm" + " ", "Colors and font changes applies properly.", MessageType.Exception);
            }
            catch (Exception ex)
            {
                WriteUniquelogs("RegistrationForm" + " ", "RegistrationForm : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
            NetworkChange.NetworkAvailabilityChanged += new NetworkAvailabilityChangedEventHandler(OnNetworkAvailabilityChanged);
        }

        private void reset_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            //textBox4.Text = "";
            textBox5.Text = "";
            WriteUniquelogs("RegistrationForm" + " ", "Reset click, All fields are clear now.", MessageType.Exception);
        }

        private void set_Click(object sender, EventArgs e)
        {
            ////IRDSPM::Pratiksha::31-08-2020::Added for 30 days expiry date
            ////current date for Indian timezone
            //DateTime dtCurrentDate = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));

            //DateTime dtExpiryDate = dtCurrentDate.AddDays(30);
            //double dexpirydate = dtExpiryDate.ToOADate();
            //KiteConnectWrapper kiteConn = new KiteConnectWrapper();
            //try
            //{
            //    //validation for name of user
            //    if (textBox1.Text.Trim() == "")
            //    {
            //        string message = "Please enter name.";
            //        //IRDSPM::Pratiksha::29-07-2020::Change the message box layout made it standard
            //        MessageBoxButtons buttons = MessageBoxButtons.OK;
            //        DialogResult dialog = MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
            //        textBox1.Focus();
            //        return;
            //    }
            //    else
            //    {
            //        Match nameValidation = Regex.Match(textBox1.Text, @"^[a-zA-z ]*$");
            //        if (!nameValidation.Success)
            //        {
            //            //IRDSPM::Pratiksha::29-07-2020::Change the message box layout made it standard
            //            string message = "Please enter valid name.";
            //            MessageBoxButtons buttons = MessageBoxButtons.OK;
            //            DialogResult dialog = MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
            //            textBox1.Focus();
            //            return;
            //        }
            //    }

            //    //validation for EMailID of user
            //    if ((textBox2.Text != "") || (textBox2.Text != null))
            //    {
            //        Match emailValidation = Regex.Match(textBox2.Text, @"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$");
            //        if (!emailValidation.Success)
            //        {
            //            //IRDSPM::Pratiksha::29-07-2020::Change the message box layout made it standard
            //            string message = "Please enter valid email id.";
            //            MessageBoxButtons buttons = MessageBoxButtons.OK;
            //            DialogResult dialog = MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
            //            textBox2.Focus();
            //            return;
            //        }
            //    }

            //    //Validation for Mobile number of user
            //    if ((textBox3.Text != "") || (textBox3.Text != null))
            //    {
            //        Match emailValidation = Regex.Match(textBox3.Text, @"^\s*\+?[0-9]{0,3}\d?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$");
            //        if (!emailValidation.Success)
            //        {
            //            //IRDSPM::Pratiksha::29-07-2020::Change the message box layout made it standard
            //            string message = "Please enter valid mobile number.";
            //            MessageBoxButtons buttons = MessageBoxButtons.OK;
            //            DialogResult dialog = MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
            //            textBox3.Focus();
            //            return;
            //        }
            //    }

            //    //Validation for Client ID
            //    if ((textBox4.Text == "") || (textBox4.Text == null))
            //    {
            //        //Match clientidValidation = Regex.Match(textBox4.Text, @"^(\w{4}-\w{4}-\w{4}-\w{4}-\w{4})$");
            //        //if (!clientidValidation.Success)
            //        {
            //            //IRDSPM::Pratiksha::29-07-2020::Change the message box layout made it standard
            //            string message = "Please enter valid clientID.";
            //            MessageBoxButtons buttons = MessageBoxButtons.OK;
            //            DialogResult dialog = MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
            //            textBox4.Focus();
            //            return;
            //        }
            //    }
            //    // for affiliate 
            //    if ((textBox5.Text == "") || (textBox5.Text == null))
            //    {
            //        //IRDSPM::Pratiksha::29-07-2020::Change the message box layout made it standard
            //        string message = "Please enter valid affiliateID.";
            //        MessageBoxButtons buttons = MessageBoxButtons.OK;
            //        DialogResult dialog = MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
            //        textBox5.Focus();
            //        return;
            //    }
            //    //if (kiteConn.RetrieveUserdetails(textBox4.Text, textBox1.Text) == 1)
            //    if (kiteConn.RetrieveUserdetails(textBox4.Text) == 1)
            //    {
            //        //query for updating user record for 30 days trial
            //        bolUpdateuserinformation = kiteConn.UpdateUserInformation(textBox1.Text, textBox2.Text, textBox3.Text, textBox4.Text, textBox5.Text, dexpirydate);

            //        if (bolUpdateuserinformation)
            //        {
            //            WriteUniquelogs("RegistrationForm" + " ", "User registred successfully.", MessageType.Exception);
            //            //IRDSPM::Pratiksha::29-07-2020::Change the message box layout made it standard
            //            string message = "User details updated Successfully!!!";
            //            MessageBoxButtons buttons = MessageBoxButtons.OK;
            //            DialogResult dialog = MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
            //            if (dialog == DialogResult.OK)
            //            {
            //                this.Close();
            //            }
            //        }
            //        else
            //        {
            //            WriteUniquelogs("RegistrationForm" + " ", "Unable to update record.", MessageType.Exception);
            //            //IRDSPM::Pratiksha::29-07-2020::Change the message box layout made it standard
            //            string message = "Unable to update record.";
            //            MessageBoxButtons buttons = MessageBoxButtons.OK;
            //            DialogResult dialog = MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
            //            if (dialog == DialogResult.OK)
            //            {
            //                this.Close();
            //            }
            //        }
            //    }
            //    else
            //    {
            //        //if (kiteConn.RetrieveUserdetails(textBox4.Text, textBox1.Text) == 1)
            //        if (kiteConn.RetrieveUserdetails(textBox4.Text) == 1)
            //        {
            //            WriteUniquelogs("RegistrationForm" + " ", "Already register user.", MessageType.Exception);
            //            //IRDSPM::Pratiksha::29-07-2020::Change the message box layout made it standard
            //            string message = "Already registered user...";
            //            MessageBoxButtons buttons = MessageBoxButtons.OK;
            //            DialogResult dialog = MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
            //            if (dialog == DialogResult.OK)
            //            {
            //                this.Close();
            //            }
            //        }
            //        else
            //        {
            //            bolAdduserinformation = kiteConn.dbConnectionAndInsert(textBox1.Text, textBox2.Text, textBox3.Text, textBox4.Text, textBox5.Text, dexpirydate);
            //            if (bolAdduserinformation)
            //            {
            //                WriteUniquelogs("RegistrationForm" + " ", "Register successfully.", MessageType.Exception);
            //                //IRDSPM::Pratiksha::29-07-2020::Change the message box layout made it standard
            //                string message = "Registration Successful!!!";
            //                MessageBoxButtons buttons = MessageBoxButtons.OK;
            //                DialogResult dialog = MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
            //                if (dialog == DialogResult.OK)
            //                {
            //                    this.Close();
            //                }
            //            }
            //            else
            //            {
            //                WriteUniquelogs("RegistrationForm" + " ", "Registration failed.", MessageType.Exception);
            //                //IRDSPM::Pratiksha::29-07-2020::Change the message box layout made it standard
            //                string message = "Registration Failed!!!";
            //                MessageBoxButtons buttons = MessageBoxButtons.OK;
            //                DialogResult dialog = MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
            //            }
            //        }
            //    }
            //    //}
            //    //catch (Exception ex)
            //    //{
            //    //    WriteUniquelogs("RegistrationForm" + " ", "set_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            //    //}
                //}
                //catch (Exception ex)
                //{
                //    WriteUniquelogs("RegistrationForm" + " ", "set_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
                //}
            MessageBox.Show("Code Commentted");
        }
        public void WriteUniquelogs(string fileName, string message, MessageType msgType)
        {
            try
            {
                SymbolLogs log = new SymbolLogs();
                //log = SymbolLogs.InstanceSymbolLogs;
                log.createFile(fileName);
                log.LogMessage(message, MessageType.Informational);
                log = null;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : " + e.Message);
            }
        }
        //IRDSPM::Pratiksha::29-07-2020::Added cancel and clear all button
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            //textBox4.Text = "";
            textBox5.Text = "";
            WriteUniquelogs("RegistrationForm" + " ", "Reset click, All fields are clear now.", MessageType.Exception);
        }
        static void OnNetworkAvailabilityChanged(object sender, NetworkAvailabilityEventArgs e)
        {
            if (e.IsAvailable)
            {
                Console.WriteLine("Network has become available");
            }
            else
            {
                bool flag = false;
                try
                {
                    using (var client = new WebClient())
                    using (client.OpenRead("http://clients3.google.com/generate_204"))
                    {
                        flag = true;
                    }
                }
                catch
                {
                    flag = false;
                }
                if (!flag)
                {
                    string title = "IRDS Algo Trader";
                    string message = "Please check internet connection!!";
                    MessageBoxButtons buttons = MessageBoxButtons.OK;
                    DialogResult dialog = MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                    //MessageBox.Show("Please check internet connection!!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }
    }
}
