﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace IRDSAlgoOMS.GUI
{

    public partial class APISettingWPFForm : System.Windows.Controls.UserControl
    {
        string passwordValue = "";
        APISettingWindowsForm m_apiSettingObj = null;
        dynamic m_objKiteConnectWrapper = null;
        SamcoConnectWrapper m_objSamcoConnectWrapper = null;
        string m_loginStatus = null;
        ConfigSettings m_objconfigSetting = null;
        ReadSettings m_objReadSetting = null;
        string iniFileApiSetting;
        string iniFileCompositeSetting;
        string iniFileSamco;
        string path = Directory.GetCurrentDirectory();
        string m_IniFilePath = "";
        Logger logger = Logger.Instance;
        //IRDSPM::Pratiksha::15-09-2020::For Getting values from ini api setting
        string userID = " ";
        string rootVal = " ";
        string loginVal = " ";
        string MyAPIKeyVal = " ";
        string MySecretVal = " ";
        string MyUserIdVal = " ";
        string MyPasswordVal = " ";
        string PinVal = " ";
        bool autoLoginVal = false;
        bool currentValManualLogin = false;
        bool CurrentValCShowGUI = false;
        //IRDSPM::Pratiksha::15-09-2020::For Getting values from ini api setting
        string userIDSamco = " ";
        string rootValSamco = " ";
        string MyPasswordValSamco = " ";
        string YOBValSamco = "";
        bool manualLoginValSamco = false;
        bool currentValManualLoginSamco = false;
        string message = "";
        //IRDSPM::Pratiksha::08-02-2021::Dynamic name
        string title = "";
        MessageBoxButtons buttons;
        DialogResult dialog;
        List<string> PlainDetList;
        List<string> EncDetList;

        //IRDSPM::PRatiksha::25-11-2020::For Samco encryption
        List<string> PlainDetListSamco;
        List<string> EncDetListSamco;
        string brokername = "";

        //IRDSPM::Pratiksha::28-01-2021::For Composite variables
        string CRoot = "";
        string CUsername = "";
        string CAppKey = "";
        string CSecretKey = "";
        string CMarketDataappKey = "";
        string CMarketDatasecretKey = "";
        bool CshowGUIForOpenPositions = false;
        List<string> PlainDetListComposite;
        List<string> EncDetListComposite;
        string iniSetting = Directory.GetCurrentDirectory() + @"\Configuration\Setting.ini";
        //sanika::27-Nov-2020::changed to dynamic for samco
        public APISettingWPFForm(dynamic kiteConnectWrapper, string loginStatus, APISettingWindowsForm apiObj)
        {
            InitializeComponent();
            m_apiSettingObj = apiObj;
            try
            {
                m_objconfigSetting = new ConfigSettings();
                m_objconfigSetting.ReadSettingConfigFile(path + @"\Configuration\Setting.ini");
                brokername = m_objconfigSetting.m_brokerName;
                selectBrokerCombobox.Items.Add("Zerodha");
                selectBrokerCombobox.Items.Add("Samco");
                selectBrokerCombobox.Items.Add("Composite");
                if (brokername.ToLower() == "zerodha")
                {
                    selectBrokerCombobox.SelectedItem = "Zerodha";
                }
                else if (brokername.ToLower() == "samco")
                {
                    selectBrokerCombobox.SelectedItem = "Samco";
                }
                else if (brokername.ToLower() == "composite")
                {
                    selectBrokerCombobox.SelectedItem = "Composite";
                }
                title = m_objconfigSetting.m_Title;
            }
            catch (Exception e)
            {
                WriteUniquelogs("settingLogs" + " ", "APISettingWPFForm :  Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        //sanika::27-Nov-2020::changed to dynamic for samco
        public void LoadKiteConnectObject(dynamic kiteConnectWrapper, string loginStatus)
        {
            try
            {
            m_objKiteConnectWrapper = kiteConnectWrapper;
            m_loginStatus = loginStatus;
            if (m_loginStatus == "Connected")
            {
                btnApply.IsEnabled = false;
                btnApply.Foreground = Brushes.Gray;
            }
            else
            {
                btnApply.IsEnabled = true;
                btnApply.Foreground = Brushes.White;
            }
        }
            catch (Exception e)
            {
                WriteUniquelogs("settingLogs" + " ", "LoadKiteConnectObject :  Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }
        public void LoadSamcoConnectObject(SamcoConnectWrapper samcoConnectWrapper, string loginStatus)
        {
            try
            {
            m_objSamcoConnectWrapper = samcoConnectWrapper;
            m_loginStatus = loginStatus;
        }
            catch (Exception e)
            {
                WriteUniquelogs("settingLogs" + " ", "LoadSamcoConnectObject :  Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        public void refreshDataFromSamcoINIfile()
        {
            iniFileSamco = path + @"\Configuration\" + "SamcoSettings.ini";
            try
            {
                if (File.Exists(iniFileSamco))
                {
                    m_objReadSetting = new ReadSettings(logger);

                    userID = m_objReadSetting.readUserId(iniFileSamco);
                    m_objReadSetting.readConfigFileSAMCO(iniFileSamco);
                    rootVal = m_objReadSetting.root;
                    MyUserIdVal = m_objReadSetting.MyUserId;
                    MyPasswordVal = m_objReadSetting.MyPassword;
                    PinVal = m_objReadSetting.YOB;

                    userIDSamco = MyUserIdVal;
                    rootValSamco = rootVal;
                    MyPasswordValSamco = MyPasswordVal;
                    YOBValSamco = PinVal;

                    WriteUniquelogs("settingLogs" + " ", "refreshDataFromSamcoINIfile :  userIDSamco "+ userIDSamco + " rootValSamco "+ rootValSamco+ " MyPasswordValSamco "+ MyPasswordValSamco + " YOBValSamco "+ YOBValSamco, MessageType.Informational);
                }
                else
                {
                    message = "INI file does not exit!!";
                    buttons = MessageBoxButtons.OK;
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                }
                textRoot1Samco.Text = rootVal;
                textuserid2Samco.Text = MyUserIdVal;
                textPassword3Samco.Password = MyPasswordVal;
                textPassword31Samco.Text = MyPasswordVal;
                textYOB4Samco.Text = YOBValSamco;
                WriteUniquelogs("settingLogs" + " ", "refreshDataFromSamcoINIfile :  userIDSamco " + textuserid2Samco.Text + " rootValSamco " + textRoot1Samco.Text + " MyPasswordValSamco " + textPassword3Samco.Password + " YOBValSamco " + textYOB4Samco.Text, MessageType.Informational);
                WriteUniquelogs("settingLogs" + " ", "refreshDataFromSamcoINIfile :  Refresh data from samco ini file on API setting.", MessageType.Informational);
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "refreshDataFromSamcoINIfile :  Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            try
            {
            if (selectBrokerCombobox.SelectedItem.ToString() == "Samco")
            {
                if ((brokername.ToLower() != selectBrokerCombobox.SelectedItem.ToString().ToLower()) || (textRoot1Samco.Text != rootValSamco) || (textuserid2Samco.Text != userIDSamco) || (textPassword3Samco.Password != MyPasswordValSamco) || (textPassword31Samco.Text != MyPasswordValSamco) || (textYOB4Samco.Text != YOBValSamco) )//|| (currentValManualLoginSamco != manualLoginValSamco))
                {
                    btnApply_Click(sender, e);
                }
                else { m_apiSettingObj.Close(); }
            }
            else if (selectBrokerCombobox.SelectedItem.ToString() == "Zerodha")
            {
                if (AutoLoginTrue.IsChecked == true)
                {
                    currentValManualLogin = true;
                }
                else
                {
                    currentValManualLogin = false;
                }
                if ((brokername.ToLower() != selectBrokerCombobox.SelectedItem.ToString().ToLower()) || (textRoot1.Text != rootVal) || (textLogin2.Text != loginVal) || (textApiKey3.Text != MyAPIKeyVal) || (textSecret4.Text != MySecretVal) || (textUserId5.Text != MyUserIdVal) || (textpassword6.Password != MyPasswordVal)|| (textpassword61.Text != MyPasswordVal) || (textPin7.Password != PinVal)|| (textPin71.Text != PinVal) || (currentValManualLogin != autoLoginVal))
                {
                    btnApply_Click(sender, e);
                }
                else { m_apiSettingObj.Close(); }
            }
                //IRDSPM::Pratiksha::28-01-2021::For Composite
                else if (selectBrokerCombobox.SelectedItem.ToString() == "Composite")
                {
                    if (ShowGUITrue.IsChecked == true)
                    {
                        CurrentValCShowGUI = true;
                    }
                    else
                    {
                        CurrentValCShowGUI = false;
                    }
                    if ((brokername.ToLower() != selectBrokerCombobox.SelectedItem.ToString().ToLower()) || (textRoot3.Text != CRoot) || (textUsername3.Text != CUsername) || (textAppKey3.Text != CAppKey) || (textSecretKey3.Text != CSecretKey) || (textMarketDataappKey3.Text != CMarketDataappKey) || (textpasswordMarketDatasecretKey3.Text != CMarketDatasecretKey) || (CurrentValCShowGUI != CshowGUIForOpenPositions))
                    {
                        btnApply_Click(sender, e);
                    }
                    else { m_apiSettingObj.Close(); }
                }
            else
            {
                m_apiSettingObj.Close();
            }
        }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "btnCancel_Click :  Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void selectBrokerCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
            if (selectBrokerCombobox.SelectedItem.ToString() == "Samco")
            {
                groupBoxZerodha.Visibility = Visibility.Hidden;
                groupBoxSamco.Visibility = Visibility.Visible;
                    groupBoxComposite.Visibility = Visibility.Hidden;
                refreshDataFromSamcoINIfile();
            }
            else if (selectBrokerCombobox.SelectedItem.ToString() == "Zerodha")
            {
                groupBoxSamco.Visibility = Visibility.Hidden;
                groupBoxZerodha.Visibility = Visibility.Visible;
                    groupBoxComposite.Visibility = Visibility.Hidden;
                refreshDataFromINIfile();
            }
                //IRDSPM::Pratiksha::28-21-2021::For composite
                else if (selectBrokerCombobox.SelectedItem.ToString() == "Composite")
                {
                    groupBoxSamco.Visibility = Visibility.Hidden;
                    groupBoxZerodha.Visibility = Visibility.Hidden;
                    groupBoxComposite.Visibility = Visibility.Visible;
                    refreshDataFromCompositeINIfile();
                }
            if ((brokername.ToLower() != selectBrokerCombobox.SelectedItem.ToString().ToLower()))
            {
                btnApply.IsEnabled = true;
                btnApply.Foreground = Brushes.White;
            }
            else
            {
                btnApply.IsEnabled = false;
                btnApply.Foreground = Brushes.Gray;
            }
        }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "selectBrokerCombobox_SelectionChanged :  Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        //IRDSPM::Pratiksha::28-21-2021::For composite
        public void refreshDataFromCompositeINIfile()
        {
            iniFileCompositeSetting = path + @"\Configuration\" + "CompositeSettings.ini";
            try
            {
                if (File.Exists(iniFileCompositeSetting))
                {
                    if (m_objconfigSetting == null)
                    {
                        m_objconfigSetting = new ConfigSettings(logger);
                    }
                    m_objconfigSetting.readCompositeConfigFile(iniFileCompositeSetting);
                    CRoot = m_objconfigSetting.root;
                    CUsername = m_objconfigSetting.username;
                    CAppKey = m_objconfigSetting.appKey;
                    CSecretKey = m_objconfigSetting.secretKey;
                    CMarketDataappKey = m_objconfigSetting.marketDataAppKey;
                    CMarketDatasecretKey = m_objconfigSetting.marketDataSecretKey;
                    CshowGUIForOpenPositions = m_objconfigSetting.showGUIForOpenPositions;
                }
                else
                {
                    message = "INI file does not exit!!";
                    buttons = MessageBoxButtons.OK;
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                }
                textRoot3.Text = CRoot;
                textUsername3.Text = CUsername;
                textAppKey3.Text = CAppKey;
                textSecretKey3.Text = CSecretKey;
                textMarketDataappKey3.Text = CMarketDataappKey;
                textpasswordMarketDatasecretKey3.Text = CMarketDatasecretKey;

                if (CshowGUIForOpenPositions == true)
                {
                    ShowGUITrue.IsChecked = true;
                }
                else
                {
                    ShowGUIFalse.IsChecked = true;
                }
                WriteUniquelogs("settingLogs" + " ", "refreshDataFromCompositeINIfile : Refresh data from Composite ini file on API setting.", MessageType.Informational);
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "refreshDataFromCompositeINIfile : Exception Error message = " + ex.Message, MessageType.Exception);
            }
        }
        public void refreshDataFromINIfile()
        {
            iniFileApiSetting = path + @"\Configuration\" + "APISetting.ini";
            try
            {
                if (File.Exists(iniFileApiSetting))
                {
                    m_objReadSetting = new ReadSettings(logger);
                    userID = m_objReadSetting.readUserId(iniFileApiSetting);
                    m_objReadSetting.readConfigFileOROMS(iniFileApiSetting);
                    rootVal = m_objReadSetting.root;
                    loginVal = m_objReadSetting.login;
                    MyAPIKeyVal = m_objReadSetting.MyAPIKey;
                    MySecretVal = m_objReadSetting.MySecret;
                    MyUserIdVal = m_objReadSetting.MyUserId;
                    MyPasswordVal = m_objReadSetting.MyPassword;
                    PinVal = m_objReadSetting.Pin;
                    autoLoginVal = m_objReadSetting.autoLogin;
                }
                else
                {
                    message = "INI file does not exit!!";
                    buttons = MessageBoxButtons.OK;
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                }
                textRoot1.Text = rootVal;
                textLogin2.Text = loginVal;
                textApiKey3.Text = MyAPIKeyVal;
                textSecret4.Text = MySecretVal;
                textUserId5.Text = MyUserIdVal;
                textpassword6.Password = MyPasswordVal;
                textpassword61.Text = MyPasswordVal;
                textPin7.Password = PinVal;
                textPin71.Text = PinVal;
                if (autoLoginVal == true)
                {
                    AutoLoginTrue.IsChecked = true;
                }
                else
                {
                    AutoLoginFalse.IsChecked = true;
                }
                WriteUniquelogs("settingLogs" + " ", "refreshDataFromINIfile : Refresh data from zerodha ini file on API setting.", MessageType.Informational);
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "refreshDataFromINIfile : Exception Error message = " + ex.Message, MessageType.Exception);
            }
        }
        public void WriteUniquelogs(string symbol, string message, MessageType msgType)
        {
            try
            {
                SymbolLogs log = new SymbolLogs();
                log.createFile(symbol);
                log.LogMessage(message, MessageType.Informational);
                log = null;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : " + e.Message);
                Trace.WriteLine("Exception : " + e.Message);
            }
        }

        private void btnApply_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (selectBrokerCombobox.SelectedItem.ToString() == "Samco")
                {
                    SamcoDetailsSave();
                }
                else if (selectBrokerCombobox.SelectedItem.ToString() == "Zerodha")
                {
                    ZerodhaDetailsSave();
                }
                else if (selectBrokerCombobox.SelectedItem.ToString() == "Composite")
                {
                    CompositeDetailsSave();
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "btnApply_Click :  Exception  Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void CompositeDetailsSave()
        {
            try
            {
                message = "Do you want to save the changes?";
                buttons = MessageBoxButtons.YesNo;
                dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                if (dialog == DialogResult.Yes)
                {
                    PlainDetListComposite = new List<string>();
                    string key_val;
                    string value_value;
                    string[] key = new string[7];
                    string[] value = new string[7];
                    key[0] = "root";
                    key[1] = "MyUserId"; //Sanika::15-Feb-2021::changed name because Same for all broker
                    key[2] = "appKey";
                    key[3] = "secretKey";
                    key[4] = "MarketDataappKey";
                    key[5] = "MarketDatasecretKey";
                    key[6] = "showGUIForOpenPositions";

                    value[0] = textRoot3.Text;
                    value[1] = textUsername3.Text;
                    value[2] = textAppKey3.Text;
                    value[3] = textSecretKey3.Text;
                    value[4] = textMarketDataappKey3.Text;
                    value[5] = textpasswordMarketDatasecretKey3.Text;
                    if (ShowGUITrue.IsChecked == true)
                    {
                        value[6] = "true";
                    }
                    if (ShowGUIFalse.IsChecked == true)
                    {
                        value[6] = "false";
                    }

                    PlainDetListComposite.Add(value[1]);
                    PlainDetListComposite.Add(value[2]);
                    PlainDetListComposite.Add(value[3]);
                    PlainDetListComposite.Add(value[4]);
                    PlainDetListComposite.Add(value[5]);

                    if (m_objReadSetting == null)
                    {
                        m_objReadSetting = new ReadSettings(logger);
                    }
                    if (PlainDetListComposite[1] != "XXXXXX" || PlainDetListComposite[2] != "XXXXXX" || PlainDetListComposite[3] != "XXXXXX" || PlainDetListComposite[4] != "XXXXXX" || PlainDetListComposite[5] != "XXXXXX")
                    {
                        encryptDetails(PlainDetListComposite);

                        for (int i = 0; i < 7; i++)
                        {
                            if (i == 1 || i == 2 || i == 3 || i == 4 || i == 5)
            {
                                m_objReadSetting.writeINIFileWithPath("credentials", key[i], EncDetList[i - 1], iniFileCompositeSetting);
            }
                            else
            {
                                m_objReadSetting.writeINIFileWithPath("credentials", key[i], value[i], iniFileCompositeSetting);
            }
        }

                        m_objReadSetting.writeINIFileWithPath("BROKER", "brokername", selectBrokerCombobox.SelectedItem.ToString(), iniSetting);
                        WriteUniquelogs("settingLogs" + " ", "CompositeDetailsSave: set broker name in ini file as " + selectBrokerCombobox.SelectedItem.ToString(), MessageType.Informational);

                        if ((!CRoot.Contains("XXXXXX")) || (!CAppKey.Contains("XXXXXX")) || (!CMarketDataappKey.Contains("XXXXXX")) || (!CSecretKey.Contains("XXXXXX")) || (!CMarketDatasecretKey.Contains("XXXXXX")))
                        {
                            WriteUniquelogs("settingLogs" + " ", "CompositeDetailsSave: Modified the details", MessageType.Informational);
                            message = "Saved changes successfully!!! If you want to reflect those changes please restart " + title+".\n Do you want to restart now?";
                            buttons = MessageBoxButtons.YesNo;
                            dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                            if (dialog == DialogResult.No)
                            {
                                m_apiSettingObj.Close();
                                WriteUniquelogs("settingLogs" + " ", "CompositeDetailsSave: Clicked on No to retstart the application", MessageType.Informational);
                            }
                            else if (dialog == DialogResult.Yes)
                            {
                                WriteUniquelogs("settingLogs" + " ", "CompositeDetailsSave: Clicked on Yes to retstart the application", MessageType.Informational);
                                RestartApplication();
                            }
                        }
                        else
                        {
                            //IRDSPM::Pratiksha::24-11-2020::Save encrypted details in ini file --end
                            string userid = textUserId5.Text;
                            refreshDataFromINIfile();
                            m_objKiteConnectWrapper.ChangeUserID(userid);
                            WriteUniquelogs("settingLogs" + " ", "CompositeDetailsSave: Saved changes successfully", MessageType.Informational);
                            //IRDSPM::Pratiksha::16-02-2021::To add into guilogs for toast
                            WriteGUIlogs(m_objKiteConnectWrapper.m_ExceptionCounter, DateTime.Now + " API setting's changes saved successfully!!!");
                            m_objKiteConnectWrapper.m_ExceptionCounter++;
                                m_apiSettingObj.Close();
                        }
                        WriteUniquelogs("settingLogs" + " ", "CompositeDetailsSave: Data updated from broker setting to apisetting.ini file.", MessageType.Informational);
                    }
                    else
                    {
                        message = "You can not save these changes, please add proper values.";
                        buttons = MessageBoxButtons.OK;
                        dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    m_apiSettingObj.Close();
                    WriteUniquelogs("settingLogs" + " ", "CompositeDetailsSave : You have clicked No, no data updated in apisetting.ini file.", MessageType.Informational);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "CompositeDetailsSave : " + ex.Message, MessageType.Informational);
            }
        }
        //IRDSPM::Pratiksha::16-02-2021::For toast notification
        public void WriteGUIlogs(int Counter, string message)
        {
            try
            {
                SymbolLogs log = new SymbolLogs();
                log.createFile("GUILogs");
                log.LogMessage(Counter + "|" + message, MessageType.Informational);
                log = null;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : " + e.Message);
                Trace.WriteLine("Exception : " + e.Message);
            }
        }
        private void ZerodhaDetailsSave()
        {
            try
            {
            message = "Do you want to save the changes?";
            buttons = MessageBoxButtons.YesNo;
            dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
            if (dialog == DialogResult.Yes)
            {
                PlainDetList = new List<string>();
                string key_val;
                string value_value;
                string[] key = new string[8];
                string[] value = new string[8];
                key[0] = "root";
                key[1] = "login";
                //IRDSPM::Pratiksha::24-11-2020::Save encrypted details in ini file --start
                key[2] = "MyAPIKey";
                key[3] = "MySecret";
                key[4] = "MyUserId";
                key[5] = "MyPassword";
                key[6] = "Pin";
                key[7] = "autoLogin";

                value[0] = textRoot1.Text;
                value[1] = textLogin2.Text;
                value[2] = textApiKey3.Text;
                PlainDetList.Add(value[2]);
                value[3] = textSecret4.Text;
                PlainDetList.Add(value[3]);
                value[4] = textUserId5.Text;
                PlainDetList.Add(value[4]);
                if(PasswordEyeClose.Visibility == Visibility.Visible)
                {
                value[5] = textpassword6.Password;
                }
                else
                {
                    value[5] = textpassword61.Text;
                }
                PlainDetList.Add(value[5]);
                if (PinEyeClose.Visibility == Visibility.Visible)
                {
                value[6] = textPin7.Password;
                }
                else
                {
                    value[6] = textPin71.Text;
                }
                //value[6] = textPin7.Password;
                PlainDetList.Add(value[6]);
                if (AutoLoginTrue.IsChecked == true)
                {
                    value[7] = "true";
                }
                else
                {
                    value[7] = "false";
                }
                if (AutoLoginTrue.IsChecked == true)
                {
                    currentValManualLogin = true;
                }
                else
                {
                    currentValManualLogin = false;
                }
                //IRDSPM::Pratiksha::27-11-2020::For saving details it should not contain XXXXX values
                if(PlainDetList[0]!= "XXXXXX"|| PlainDetList[1] != "XXXXXX" || PlainDetList[2] != "XXXXXX" || PlainDetList[3] != "XXXXXX" || PlainDetList[4] != "XXXXXX")
                {
                    encryptDetails(PlainDetList);

                    for (int i = 0; i < 8; i++)
                    {
                        if (i == 2 || i == 3 || i == 4 || i == 5 || i == 6)
                        {
                            m_objReadSetting.writeINIFile("credentials", key[i], EncDetList[i - 2]);
                        }
                        else
                        {
                            m_objReadSetting.writeINIFile("credentials", key[i], value[i]);
                        }
                    }

                        m_objReadSetting.writeINIFileWithPath("BROKER", "brokername", selectBrokerCombobox.SelectedItem.ToString(), iniSetting);
                    WriteUniquelogs("settingLogs" + " ", "ZerodhaDetailsSave: set broker name in ini file as "+ selectBrokerCombobox.SelectedItem.ToString(), MessageType.Informational);

                    //sanika::28-Nov-2020::Added check for first time no need to restart the application and restarted application if there data is modified
                    if ((!MyAPIKeyVal.Contains("XXXXXX")) || (!MyUserIdVal.Contains("XXXXXX")) || (!MyPasswordVal.Contains("XXXXXX")) || (!PinVal.Contains("XXXXXX")) || (!MySecretVal.Contains("XXXXXX")))
                    {
                        WriteUniquelogs("settingLogs" + " ", "ZerodhaDetailsSave: Modified the details", MessageType.Informational);
                            message = "Saved changes successfully!!! If you want to reflect those changes please restart " + title + ".\n Do you want to restart now?";
                        buttons = MessageBoxButtons.YesNo;
                        dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                        if (dialog == DialogResult.No)
                        {
                            //IRDSPM::Pratiksha::14-12-2020::If click no then frm should be close
                            m_apiSettingObj.Close();
                            WriteUniquelogs("settingLogs" + " ", "ZerodhaDetailsSave: Clicked on No to retstart the application", MessageType.Informational);
                        }
                        else if(dialog == DialogResult.Yes)
                        {
                            WriteUniquelogs("settingLogs" + " ", "ZerodhaDetailsSave: Clicked on Yes to retstart the application", MessageType.Informational);
                            RestartApplication();
                        }
                    }
                    else
                    {
                        //IRDSPM::Pratiksha::24-11-2020::Save encrypted details in ini file --end
                        string userid = textUserId5.Text;
                        refreshDataFromINIfile();
                        m_objKiteConnectWrapper.ChangeUserID(userid);
                        WriteUniquelogs("settingLogs" + " ", "ZerodhaDetailsSave: Saved changes successfully", MessageType.Informational);
                            //IRDSPM::Pratiksha::16-02-2021::To add into guilogs for toast
                            WriteGUIlogs(m_objKiteConnectWrapper.m_ExceptionCounter, DateTime.Now + " API setting's changes saved successfully!!!");
                            m_objKiteConnectWrapper.m_ExceptionCounter++;
                            m_apiSettingObj.Close();
                    }
                    WriteUniquelogs("settingLogs" + " ", "ZerodhaDetailsSave: Data updated from broker setting to apisetting.ini file.", MessageType.Informational);
                }
                else
                {
                        message = "You can not save these changes, please add proper values.";
                        buttons = MessageBoxButtons.OK;
                        dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                }
            }
            else
            {
                m_apiSettingObj.Close();
                WriteUniquelogs("settingLogs" + " ", "ZerodhaDetailsSave : You have clicked No, no data updated in apisetting.ini file.", MessageType.Informational);
            }
        }
            catch (Exception e)
            {
                WriteUniquelogs("settingLogs" + " ", "ZerodhaDetailsSave :  Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        private void SamcoDetailsSave()
        {
            try
            {
                //IRDSPM::Pratiksha::24-11-2020::add data in samco file
                message = "Do you want to save the changes?";
                buttons = MessageBoxButtons.YesNo;
                dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                if (dialog == DialogResult.Yes)
                {
                    PlainDetListSamco = new List<string>();
                    string key_val;
                    string value_value;
                    string[] key = new string[6];
                    string[] value = new string[6];
                    key[0] = "root";
                    key[1] = "MyUserId";
                    key[2] = "MyPassword";
                    key[3] = "Pin";                    
                    value[0] = textRoot1Samco.Text;
                    value[1] = textuserid2Samco.Text;
                    PlainDetListSamco.Add(value[1]);
                    if (PasswordEyeCloseSamco.Visibility == Visibility.Visible)
                    {
                    value[2] = textPassword3Samco.Password;
                    }
                    else
                    {
                        value[2] = textPassword31Samco.Text;
                    }
                    PlainDetListSamco.Add(value[2]);
                    value[3] = textYOB4Samco.Text;
                    PlainDetListSamco.Add(value[3]);
                   
                    //IRDSPM::Pratiksha::25-11-2020::convert in encrypted text
                    if (PlainDetListSamco[0] != "XXXXXX" || PlainDetListSamco[1] != "XXXXXX" || PlainDetListSamco[2] != "XXXXXX")
                    {
                    encryptDetailsSamco(PlainDetListSamco);

                    //IRDSPM::Pratiksha::24-11-2020::Saving in ini file
                    for (int i = 0; i < 4; i++)
                    {
                        if (i == 1 || i == 2 || i == 3)
                        {
                            m_objReadSetting.writeINIFile("credentials", key[i], EncDetListSamco[i - 1]);
                        }
                        else
                        {
                            m_objReadSetting.writeINIFile("credentials", key[i], value[i]);
                        }
                    }


                        m_objReadSetting.writeINIFileWithPath("BROKER", "brokername", selectBrokerCombobox.SelectedItem.ToString(), iniSetting);
                    WriteUniquelogs("settingLogs" + " ", "ZerodhaDetailsSave: set broker name in ini file as " + selectBrokerCombobox.SelectedItem.ToString(), MessageType.Informational);



                    //sanika::28-Nov-2020::Added check for first time no need to restart the application and restarted application if there data is modified
                    if ((!userIDSamco.Contains("XXXXXX")) || (!MyPasswordValSamco.Contains("XXXXXX")) || (!YOBValSamco.Contains("XXXXXX")))
                    {
                        WriteUniquelogs("settingLogs" + " ", "SamcoDetailsSave: Modified the details", MessageType.Informational);
                            message = "Saved changes successfully!!! If you want to reflect those changes please restart " + title + ".\n Do you want to restart now?";
                        buttons = MessageBoxButtons.YesNo;
                        dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                        if (dialog == DialogResult.No)
                        {
                            m_apiSettingObj.Close();
                            WriteUniquelogs("settingLogs" + " ", "SamcoDetailsSave: Clicked on No to retstart the application", MessageType.Informational);
                        }
                        else if (dialog == DialogResult.Yes)
                        {
                            WriteUniquelogs("settingLogs" + " ", "SamcoDetailsSave: Clicked on Yes to retstart the application", MessageType.Informational);
                            RestartApplication();
                        }
                    }
                    else
                    {
                        string userid = textuserid2Samco.Text;
                        refreshDataFromSamcoINIfile();
                        m_objKiteConnectWrapper.ChangeUserID(userid);//sanika::28-Nov-2020::Missing to change user id after chnaging from xxxxxx to valid user id
                        WriteUniquelogs("settingLogs" + " ", "ZerodhaDetailsSave: Saved changes successfully", MessageType.Informational);
                            //IRDSPM::Pratiksha::16-02-2021::To add into guilogs for toast
                            WriteGUIlogs(m_objKiteConnectWrapper.m_ExceptionCounter, DateTime.Now + " API setting's changes saved successfully!!!");
                            m_objKiteConnectWrapper.m_ExceptionCounter++;
                            m_apiSettingObj.Close();
                    }                 
                   
                    WriteUniquelogs("settingLogs" + " ", "SamcoDetailsSave :  Data updated to samcoSetting.ini file.", MessageType.Informational);
                }
                else
                {
                        message = "You can not save these changes, please add proper values.";
                        buttons = MessageBoxButtons.OK;
                        dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    m_apiSettingObj.Close();
                    WriteUniquelogs("settingLogs" + " ", "SamcoDetailsSave :  You have clicked No, no data updated in samcoSetting.ini file.", MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs("settingLogs" + " ", "SamcoDetailsSave :  Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        //IRDSPM::Pratiksha::24-11-2020::Encrypt detials for saving samco
        public void encryptDetails(List<string> plainDet)
        {
            try
            {
                EncDetList = new List<string>();
                for (int i = 0; i < plainDet.Count; i++)
                {
                    string enc = EncryptData(plainDet[i], "IrdsalgoTrader");
                    EncDetList.Add(enc);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "encryptDetails : Exception Error Message =  " + ex.Message, MessageType.Exception);
            }
        }

        //IRDSPM::Pratiksha::25-11-2020::Encrypt detials for saving samco
        public void encryptDetailsSamco(List<string> plainDetSamco)
        {
            try
            {
                EncDetListSamco = new List<string>();
                for (int i = 0; i < plainDetSamco.Count; i++)
                {
                    string enc = EncryptData(plainDetSamco[i], "IrdsalgoTrader");
                    EncDetListSamco.Add(enc);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "encryptDetailsSamco : Exception Error Message =  " + ex.Message, MessageType.Exception);
            }
        }

        private void btnApply_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            btnApply.Foreground = Brushes.Black;
        }

        private void btnApply_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            btnApply.Foreground = Brushes.White;
        }

        private void btnCancel_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            btnCancel.Foreground = Brushes.Black;
        }

        private void btnCancel_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            btnCancel.Foreground = Brushes.White;
        }
        //IRDSPM::Pratiksha::24-11-2020::Encrpt code
        public static string EncryptData(string strInText, string strKey)
        {
            byte[] bytesBuff = Encoding.Unicode.GetBytes(strInText);

            using (Aes aes__1 = Aes.Create())
            {
                System.Security.Cryptography.Rfc2898DeriveBytes crypto = new Rfc2898DeriveBytes(strKey, new byte[] {
                0x49,
                0x76,
                0x61,
                0x6e,
                0x20,
                0x4d,
                0x65,
                0x64,
                0x76,
                0x65,
                0x64,
                0x65,
                0x76
            });
                aes__1.Key = crypto.GetBytes(32);
                aes__1.IV = crypto.GetBytes(16);

                using (MemoryStream mStream = new MemoryStream())
                {
                    using (CryptoStream cStream = new CryptoStream(mStream, aes__1.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cStream.Write(bytesBuff, 0, bytesBuff.Length);
                        cStream.Close();
                    }

                    strInText = Convert.ToBase64String(mStream.ToArray());
                }
            }

            return strInText;
        }


        public void RestartApplication()
        {
            try
            {
                // Wait for the process to terminate
                Process process = null;
                string applicationName = Process.GetCurrentProcess().ProcessName;
                if (applicationName.Contains('.'))
                {
                    applicationName = applicationName.Split('.')[0];
                }
                try
                {

                    int nProcessID = Process.GetCurrentProcess().Id;
                    process = Process.GetProcessById(nProcessID);
                    Process.Start(applicationName, "");
                    process.Kill();
                }
                catch (ArgumentException ex)
                {
                    // ArgumentException to indicate that the 
                    // process doesn't exist?   LAME!!
                }

            }
            catch (Exception er)
            {

            }
        }
        //IRDSPM::Pratiksha::14-12-2020::When text changes it should enable the changes
        private void text_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextChangedEffectOnApplyButton();
        }

        private void TextChangedEffectOnApplyButton()
        {
            try
            {
                string currentValPassword = "";
                string currentValPin = "";
                if (selectBrokerCombobox.SelectedItem.ToString() == "Zerodha")
                {
                    if (PasswordEyeClose.Visibility == Visibility.Visible)
                    {
                        currentValPassword = textpassword6.Password;
                    }
                    else
                    {
                        currentValPassword = textpassword61.Text;
                    }
                    if (PinEyeClose.Visibility == Visibility.Visible)
                    {
                        currentValPin = textPin7.Password;
                    }
                    else
                    {
                        currentValPin = textPin71.Text;
                    }
                    if (AutoLoginTrue.IsChecked == true)
                    {
                        currentValManualLogin = true;
                    }
                    else
                    {
                        currentValManualLogin = false;
                    }
                    if ((brokername.ToLower() != selectBrokerCombobox.SelectedItem.ToString().ToLower()) || (textRoot1.Text != rootVal) || (textLogin2.Text != loginVal) || (textApiKey3.Text != MyAPIKeyVal) || (textSecret4.Text != MySecretVal) || (textUserId5.Text != MyUserIdVal) || (currentValPassword != MyPasswordVal) || (currentValPassword != MyPasswordVal) || (currentValPin != PinVal) || (currentValPin != PinVal) || (currentValManualLogin != autoLoginVal))
                    {
                        btnApply.IsEnabled = true;
                        btnApply.Foreground = Brushes.White;
                    }
                    else
                    {
                        btnApply.IsEnabled = false;
                        btnApply.Foreground = Brushes.Gray;
                    }
                }
                else if (selectBrokerCombobox.SelectedItem.ToString() == "Samco")
                {
                    string currentValPasswordSamco = "";
                    if (PasswordEyeCloseSamco.Visibility == Visibility.Visible)
                    {
                        currentValPasswordSamco = textPassword3Samco.Password;
                    }
                    else
                    {
                        currentValPasswordSamco = textPassword31Samco.Text;
                    }
                    if ((brokername.ToLower() != selectBrokerCombobox.SelectedItem.ToString().ToLower()) || (textRoot1Samco.Text != rootValSamco) || (textuserid2Samco.Text != userIDSamco) || (currentValPasswordSamco != MyPasswordValSamco) || (currentValPasswordSamco != MyPasswordValSamco) || (textYOB4Samco.Text != YOBValSamco))
                    {
                        btnApply.IsEnabled = true;
                        btnApply.Foreground = Brushes.White;
                    }
                    else
                    {
                        btnApply.IsEnabled = false;
                        btnApply.Foreground = Brushes.Gray;
                    }
                }
                else if (selectBrokerCombobox.SelectedItem.ToString() == "Composite")
                {
                    if (ShowGUITrue.IsChecked == true)
                    {
                        CurrentValCShowGUI = true;
                    }
                    else
                    {
                        CurrentValCShowGUI = false;
                    }
                    if ((brokername.ToLower() != selectBrokerCombobox.SelectedItem.ToString().ToLower()) || (textRoot3.Text != CRoot) || (textUsername3.Text != CUsername) || (textAppKey3.Text != CAppKey) || (textSecretKey3.Text != CSecretKey) || (textMarketDataappKey3.Text != CMarketDataappKey) || (textpasswordMarketDatasecretKey3.Text != CMarketDatasecretKey) || (CurrentValCShowGUI != CshowGUIForOpenPositions))
                    {
                        btnApply.IsEnabled = true;
                        btnApply.Foreground = Brushes.White;
                    }
                    else
                    {
                        btnApply.IsEnabled = false;
                        btnApply.Foreground = Brushes.Gray;
                    }
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "TextChangedEffectOnApplyButton : Exception Error Message =  " + ex.Message, MessageType.Exception);
            }
        }

        private void text_PasswordChanged(object sender, RoutedEventArgs e)
        {
            TextChangedEffectOnApplyButton();
        }

        private void AutoLoginTrue_Checked(object sender, RoutedEventArgs e)
        {
            bool currentVal = false;
            if(AutoLoginTrue.IsChecked == true)
            {
                currentVal = true;
            }
            else { currentVal = false; }

            if(autoLoginVal != currentVal || (brokername.ToLower() != selectBrokerCombobox.SelectedItem.ToString().ToLower()))
            {
                btnApply.IsEnabled = true;
                btnApply.Foreground = Brushes.White;
            }
            else
            {
                btnApply.IsEnabled = false;
                btnApply.Foreground = Brushes.Gray;
            }
        }

        private void AutoLoginFalse_Checked(object sender, RoutedEventArgs e)
        {
            bool currentVal = false;
            if (AutoLoginTrue.IsChecked == true)
            {
                currentVal = true;
            }
            else { currentVal = false; }

            if (autoLoginVal != currentVal || (brokername.ToLower() != selectBrokerCombobox.SelectedItem.ToString().ToLower()))
            {
                btnApply.IsEnabled = true;
                btnApply.Foreground = Brushes.White;
            }
            else
            {
                btnApply.IsEnabled = false;
                btnApply.Foreground = Brushes.Gray;
            }
        }
        private void ShowGUITrue_Checked(object sender, RoutedEventArgs e)
        {
            bool currentVal = false;
            if (ShowGUITrue.IsChecked == true)
            {
                currentVal = true;
            }
            else { currentVal = false; }

            if (CshowGUIForOpenPositions != currentVal || (brokername.ToLower() != selectBrokerCombobox.SelectedItem.ToString().ToLower()))
            {
                btnApply.IsEnabled = true;
                btnApply.Foreground = Brushes.White;
            }
            else
            {
                btnApply.IsEnabled = false;
                btnApply.Foreground = Brushes.Gray;
            }
        }
        private void ShowGUIFalse_Checked(object sender, RoutedEventArgs e)
        {
            bool currentVal = false;
            if (ShowGUITrue.IsChecked == true)
            {
                currentVal = true;
            }
            else { currentVal = false; }

            if (CshowGUIForOpenPositions != currentVal || (brokername.ToLower() != selectBrokerCombobox.SelectedItem.ToString().ToLower()))
            {
                btnApply.IsEnabled = true;
                btnApply.Foreground = Brushes.White;
            }
            else
            {
                btnApply.IsEnabled = false;
                btnApply.Foreground = Brushes.Gray;
            }
        }
        private void PasswordEyeClose_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (selectBrokerCombobox.SelectedItem.ToString() == "Zerodha")
            {
                textpassword61.Text = textpassword6.Password;
                textpassword61.Visibility = Visibility.Visible;
                PasswordEyeOpen.Visibility = Visibility.Visible;
                textpassword6.Visibility = Visibility.Hidden;
                PasswordEyeClose.Visibility = Visibility.Hidden;
            }
            else if (selectBrokerCombobox.SelectedItem.ToString() == "Samco")
            {
                textPassword31Samco.Text = textPassword3Samco.Password;
                textPassword31Samco.Visibility = Visibility.Visible;
                PasswordEyeOpenSamco.Visibility = Visibility.Visible;
                textPassword3Samco.Visibility = Visibility.Hidden;
                PasswordEyeCloseSamco.Visibility = Visibility.Hidden;
            }
        }

        private void PasswordEyeOpen_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (selectBrokerCombobox.SelectedItem.ToString() == "Zerodha")
            {
                textpassword6.Password = textpassword61.Text;
                textpassword61.Visibility = Visibility.Hidden;
                PasswordEyeOpen.Visibility = Visibility.Hidden;
                textpassword6.Visibility = Visibility.Visible;
                PasswordEyeClose.Visibility = Visibility.Visible;
            }
            else if (selectBrokerCombobox.SelectedItem.ToString() == "Samco")
            {
                textPassword3Samco.Password = textPassword31Samco.Text;
                textPassword31Samco.Visibility = Visibility.Hidden;
                PasswordEyeOpenSamco.Visibility = Visibility.Hidden;
                textPassword3Samco.Visibility = Visibility.Visible;
                PasswordEyeCloseSamco.Visibility = Visibility.Visible;
            }
        }

        private void PinEyeClose_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            textPin71.Text = textPin7.Password;
            textPin71.Visibility = Visibility.Visible;
            PinEyeOpen.Visibility = Visibility.Visible;
            textPin7.Visibility = Visibility.Hidden;
            PinEyeClose.Visibility = Visibility.Hidden;
        }

        private void PinEyeOpen_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            textPin7.Password = textPin71.Text;
            textPin71.Visibility = Visibility.Hidden;
            PinEyeOpen.Visibility = Visibility.Hidden;
            textPin7.Visibility = Visibility.Visible;
            PinEyeClose.Visibility = Visibility.Visible;
        }
    }
}
