﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Threading;

namespace IRDSAlgoOMS.GUI
{
    public partial class ProgressBarForm : UserControl, INotifyPropertyChanged
    {
        private BackgroundWorker _bgworker = new BackgroundWorker();
        ProgressBarWindowsForm m_pbObj = null;
        private int _WorkerState;
        int m_counter = 0;
        public event PropertyChangedEventHandler PropertyChanged;
        int i = 0;
        int j = 0;
        int lastCount = 0;
        public int WorkerState
        {
            get { return _WorkerState; }
            set
            {
                _WorkerState = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("WorkerState"));
            }
        }

        public ProgressBarForm(ProgressBarWindowsForm obj)
        {
            InitializeComponent();
            m_pbObj = obj;
            lastCount = 500;

            DataContext = this;
            _bgworker.DoWork += (s, e) =>
            {
                if (WorkerState <= 100 || m_counter <= 100)
                {
                    for (i = 0; i <= lastCount; i++)
                    {
                        Thread.Sleep(200);
                        if (m_counter > 0)
                        {
                            WorkerState = m_counter;
                            i = m_counter;
                        }
                        else { WorkerState = 20; }
                        for (j = 0; j < 3; j++)
                        {
                            Thread.Sleep(200);
                            if (j == 2)
                            {
                                WorkerState = WorkerState + 29;
                                Thread.Sleep(200);
                                if (WorkerState > 100)
                                {
                                    break;
                                }
                            }
                            else
                            {
                                WorkerState = WorkerState + 25;
                                if (WorkerState > 100)
                                {
                                    break;
                                }
                            }
                        }
                    }
                }
            };

            _bgworker.RunWorkerAsync();
        }
        
        private void ShowProgress()
        {
            _bgworker.DoWork += (s, e) =>
            {
                for (i = 0; i < 100; i++)
                {
                    Thread.Sleep(200);
                    WorkerState = i;
                    this.Dispatcher.Invoke(() =>
                    {
                        WorkerState = i;
                        //Percent.Content = i + "%";
                    });
                }
            };

            _bgworker.RunWorkerAsync();
        }
        public void setWorkstate(int counter)
        {
            lastCount = counter;
                this.Dispatcher.Invoke(() =>
                {
                WorkerState = lastCount;
                i = lastCount;
                });
        }
    }
}
