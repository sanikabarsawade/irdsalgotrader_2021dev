﻿using IRDSAlgoOMS;
using IRDSAlgoOMS.Common;
using IRDSAlgoOMS.GUI;
using IRDSAlgoOMS.Zerodha;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
//IRDSPM::Pratiksha::11-05-2021::for sending sms
using Timer = System.Windows.Forms.Timer;
using SendEmailSMS;
namespace IRDSAlgoOMS
{
    /// <summary>
    /// Interaction logic for WPFMainControl.xaml
    /// </summary>
    public partial class WPFMainControl : System.Windows.Controls.UserControl
    {
        string typeofOrder = "Buy";

        BuyWindowsForm m_ObjBuyForm = null;
        BrushConverter bc = new BrushConverter();
        public DataView DataViewMarketWatch { get; set; }
        public DataView DataViewOpenPosition { get; set; }
        public DataView DataViewOrderBook { get; set; }
        public DataView DataViewOpenHoldings { get; set; }
        public DataTable mydtOpenPosition;
        public DataTable mydtMarketWatch;
        public DataTable mydtOrderBook;
        public DataTable mydtOpenHolding;
        public dynamic m_ObjkiteConnectWrapper = null;
        public static bool m_isSqrOff = false;
        public string m_UserID = "";
        public string m_futName = "";
        public List<string> SymbolList = new List<string>();
        Dictionary<string, string> m_RealTimeSymbols = new Dictionary<string, string>();
        List<string> ListForSymbolName = new List<string>();
        List<string> ListForMarketWtchGridView = new List<string>();
        List<string> ListForOpenPositionGridView = new List<string>();
        List<string> ListForUpdatedOpenPositionGridView = new List<string>();
        List<string> ListForOrderBookGridView = new List<string>();
        List<string> ListForOpenHoldingsGridView = new List<string>();
        int m_PreviousOrderCount = 0;
        bool m_AllPositions = false;
        bool m_OpenPositions = false;
        bool m_ClosePosition = false;
        bool m_AllOrders = false;
        bool m_CompleteOrders = false;
        bool m_CancelOrders = false;
        bool m_PendingOrders = false;
        bool m_usePNLFromPosition = false;
        public bool m_TickDataDownloader = false;
        public static string m_BridgeConnection = "Waiting";

        static string path = Directory.GetCurrentDirectory();
        static string todaysDate = string.Format("{0:dd-MM-yyyy}", DateTime.Now);
        string m_DirectoryName = path + "\\" + "Logs" + "\\" + todaysDate;
        private static readonly object orderbookLock = new object();
        private static readonly object m_bridgeStatusLock = new object();
        private static readonly object m_LogLock = new object();
        static List<string> m_Messages = new List<string>();
        AlgoOMS signalProcessor = new AlgoOMS();
        bool toastNotificationVal = false;
        bool toastAudioVal = false;
        string m_FilePath = path + @"\Configuration\" + "LastPrice.txt";
        string pnlFilePath = path + "\\PNL.csv";
        Thread m_LoadAllValuesOnGUI = null;
        Thread m_LoadMarginValuesOnGUI = null;
        Thread m_UpdatePNL = null;
        Thread m_ReadResponseFromNamedPipe = null;
        Thread m_SendNotification = null;
        System.Timers.Timer m_Timer = null;
        System.Timers.Timer m_TimerForBridgeStatus = null;
        static NamedPipeServerStream m_NamedPipeServer = null;
        bool m_StopThread = false;
        static bool m_StopBridgeThread = false;
        string iniFileSetting = path + @"\Configuration\" + "Setting.ini";
        Logger logger = null;
        ConfigSettings Settings = null;
        int counter = 0;
        static char NEWLINE = '\n';
        static string SPACE_SEPARATOR = " ";
        static string SPLIT_REGEXP = "\\s+";
        static bool m_bRefreshDone = false;
        //IRDSPM::Pratiksha::15-09-2020::Globa object for spi setting form
        APISettingWindowsForm m_apiSettingobj = null;
        AllSymbolSettingWindowsForm m_symbolSettingWindowObj = null;
        TradingSymbolWindowForm m_objTradingSymbol = null;
        public MainWindow m_MainWindow = null;
        //IRDSPM::Pratiksha::16-09-2020::For MArket watch remember me
        string mwsymbolnameVal = "";
        string mwltpval = "";
        string mwbidval = "";
        string mwaskval = "";
        string mwvolumeval = "";
        //IRDSPM::Pratiksha::24-09-2020::for newly added variable
        string mwOpen = "";
        string mwHigh = "";
        string mwLow = "";
        string mwClose = "";
        string mwchangeinper = "";
        //IRDSPM::Pratiksha::08-10-2020::Added new column "Net Change"
        string mwNetChange = "";
        //IRDSPM::Pratiksha::16-09-2020::For reading Open position
        string opExchange = "";
        string opSymbol = "";
        string opProduct = "";
        string opQuantity = "";
        string opLtp = "";
        string opPrice = "";
        string opPL = "";

        //IRDSPM::Pratiksha::16-09-2020::For reading Order Book
        string obEXG = "";
        string obSymbol = "";
        string obOrderId = "";
        string obTime = "";
        string obType = "";
        string obOrderType = "";
        string obProduct = "";
        string obPrice = "";
        string obTriggerPrice = "";
        string obQTY = "";
        string obStatus = "";
        string obFilledQty = "";
        string obFilledPrice = "";
        string obRejectionOrderRemark = "";

        //Open Holdings
        string ohEXG = "";
        string ohSymbol = "";
        string ohProduct = "";
        string ohQuantity = "";
        string ohcloseprice = "";
        string ohavgprice = "";
        string ohPL = "";

        //IRDSPM::PRatiksha::23-09-2020::For name to arrange based on column
        string m_ColumnSelectionForAscOrder = null;
        //IRDSPM::Pratiksha::24-09-2020::For context menu strip
        ContextMenuStrip mymenu = null;
        //IRDSPM::Pratiksha::30-09-2020:: for Ascending and descending order check
        //IRDSPM::Pratiksha::08-10-2020::Rename close to Prev Close -"Client suggestion" , also added new column Net Change
        string[] MWColumnnameArray = { "Symbol", "LTP", "Bid Price", "Ask Price", "Volume", "Open", "High", "Low", "Prev Close", "Change in %", "Net Change" };
        string[] OPColumnnameArray = { "Exchange", "Symbol", "Product", "Quantity", "LTP", "Price", "P&L" };
        string[] OBColumnnameArray = { "EXG", "Symbol", "OrderId", "Time", "Type", "Order Type", "Product", "Price", "Trigger Price", "QTY", "Status", "Filled Qty", "Filled Price", "Rejection Order Remark" };
        string[] OHColumnnameArray = { "Exchange", "Symbol", "Product", "Quantity", "Close Price", "Avg Price", "P&L" };

        //IRDSPM::Pratiksha::09-10-2020::For current symbol to buy, sell, or remove order
        string m_CurrentSymbolForMWRow = "";
        string m_CurrentSymbolForModifyOrder = "";
        private Dictionary<string, TickData> symbolWiseTickInfo = new Dictionary<string, TickData>();
        List<string> ListForMarketWtchGridViewDeletedSymbols = new List<string>();
        string inisymbolList = path + @"\Configuration\" + "symbollist.ini";

        //IRDSPM::Pratiksha::17-10-2020::Created obj for subsription details
        SubscriptionDetWindowsForm m_subDetObj = null;
        //IRDSPM::Pratiksha::20-10-2020::For progress bar
        bool m_IsAlredyDownloadedInstruments = false;

        //IRDSPM::PRatiksha::21-10-2020::For sound path
        string AudioFilePath = "";
        SoundPlayer simpleSound = null;
        //IRDSPM::PRatiksha::22-10-2020::For esc close messagebox
        DialogResult Deletedialog;
        bool isupdatelastloginInDb = false;
        string title = "";

        //IRDSPM::PRatiksha::05-11-2021
        SendInfo obj = new SendInfo();
        int sendsmscount = 0;
        //IRDSPM::Pratiksha::14-05-2021::For sending notifications
        bool m_issend = false;
        bool m_smssend = false;
        bool m_emailsend = false;
        string m_mobNum = "";
        string m_email = "";
        string m_interval = "";
        bool m_issendUpdatesNotification = false;
        Timer SendsmsMail = new Timer();

        double m_LowestMA = 0;
        double m_HighestMA = 0;
        bool m_isloaded = false;
        double m_LowestPNL = 0;
        double m_HighestPNL = 0;
        DateTime m_CurrentDate = DateTime.Now;
        DateTime m_TimeToExitAllNSEOrder;
        int m_PendingOrderCount = 0;
        bool m_IsOrderBookUpdated = false;
        bool m_SendAttachment = false;

        string m_senderName = "";
        string m_senderemail = "";
        string m_senderpassword = "";
        string m_senderhost = "";
        string m_senderPort = "";
        //29-Jully-2021::Pratiksha::For Selecting row on 1st time
        bool m_SelectRowFirstTime = true;
        bool isConnected = false;
        bool isDisconnected = false;
        public WPFMainControl(string m_title)
        {
            InitializeComponent();
            logger = Logger.Instance;
            title = m_title;
            lblValueStatus.Content = "Waiting";
            lblValueStatus.Background = Brushes.Blue;
            lblValueStatus.Foreground = Brushes.White;

            //IRDSPM::Pratiksha::26-08-2020::For bridge status
            lblBridgeStatus.Content = "Waiting";
            lblBridgeStatus.Background = Brushes.Blue;
            lblBridgeStatus.Foreground = Brushes.White;

            //to add columns in dataTable
            AddColumns();

            //sanika::5-sep-2020::Started thread
            if (m_LoadAllValuesOnGUI == null)
            {
                m_LoadAllValuesOnGUI = new Thread(() => LoadAllValuesOnGUI());
                m_LoadAllValuesOnGUI.Start();
            }

            //Jyoti::11-Feb-2021::Started thread for margin
            if (m_LoadMarginValuesOnGUI == null)
            {
                m_LoadMarginValuesOnGUI = new Thread(() => LoadInformationStatus());
                m_LoadMarginValuesOnGUI.Start();
            }
            //20-July-2021: sandip:optimization of objects,namepipe commented.
            //if (m_ReadResponseFromNamedPipe == null)
            //{
            //    m_ReadResponseFromNamedPipe = new Thread(() => ReceiveResponse());
            //    m_ReadResponseFromNamedPipe.Start();
            //}

            if (m_SendNotification == null)
            {
                m_SendNotification = new Thread(() => SendNotification());
                m_SendNotification.Start();
            }

            //sanika::24-sep-2020::Added new thread for Update PNL
            if (m_UpdatePNL == null)
            {
                m_UpdatePNL = new Thread(() => UpdatePNL());
                m_UpdatePNL.Start();
            }
            //IRDSPM::Pratiksha::24-09-2020::Contextstripmenu
            mymenu = new ContextMenuStrip();
            mymenu.Items.Add("Ascending").Name = "Asc";
            mymenu.Items.Add("Descending").Name = "Des";
            mymenu.Items.Add("Column Setup").Name = "setup";
            RefreshSettinginiFile();
        }

        //private void AddValuesInitially()
        //{
        //    string symbolnameVal = "";
        //    string ltpval = "";
        //    string bidval = "";
        //    string askval = "";
        //    string volumeval = "";
        //    string Open = "";
        //    string High = "";
        //    string Low = "";
        //    string Close = "";
        //    string changeinper = "";
        //    if (File.Exists(m_FilePath))
        //    {
        //        string[] lines = System.IO.File.ReadAllLines(@m_FilePath);
        //        foreach (var line in lines)
        //        {
        //            string[] arr = line.Split(',');
        //            if (arr.Length > 3)
        //            {
        //                symbolnameVal = arr[0];
        //                ltpval = arr[1];
        //                bidval = arr[2];
        //                askval = arr[3];
        //                volumeval = arr[4];
        //                Open = arr[5];
        //                High = arr[6];
        //                Low = arr[7];
        //                Close = arr[8];
        //                changeinper = arr[9];
        //                this.Dispatcher.Invoke(() =>
        //                {
        //                    mydtMarketWatch.Rows.Add(new object[] { symbolnameVal, ltpval, bidval, askval, volumeval, Open, High, Low, Close, changeinper });
        //                    DataContext = this;
        //                });
        //            }
        //        }
        //    }
        //}
        public void LoadAllValuesOnGUI()
        {
            while (true && m_StopThread == false)
            {
                try
                {
                    if (m_ObjkiteConnectWrapper != null)
                    {
                        bool m_LoginStatus = m_ObjkiteConnectWrapper.GetConnectionStatus();
                        if ((m_LoginStatus == false && m_ObjkiteConnectWrapper.m_waitingStatus == true) || (m_LoginStatus == false && m_ObjkiteConnectWrapper.m_waitingStatus == false))
                        {
                            this.Dispatcher.Invoke(() =>
                            {
                                lblValueStatus.Content = "Waiting";
                            });
                        }
                        else
                        {
                            ChangeFlag(m_LoginStatus);
                        }
                        LoadLogs();
                        if (m_LoginStatus)
                        {
                            //IRDSPM::pratiksha::28-11-2020::Added for opening progressbar
                            if (m_ObjkiteConnectWrapper.isDownloadInstrumentsNow == true)
                            {
                                m_ObjkiteConnectWrapper.isDownloadInstrumentsNow = false;
                                this.Dispatcher.Invoke(() =>
                                {
                                    OpenProgressBar();
                                });
                            }
                            else if (m_ObjkiteConnectWrapper.m_CounterProgressBar == 101)
                            {
                                this.Dispatcher.Invoke(() =>
                                {
                                    if (obj_progressbar != null)
                                    {
                                        //IRDSPM::Pratiksha::05-02-2021::To close loop
                                        obj_progressbar.setWorkstate(500);
                                        obj_progressbar.Close();
                                        m_ObjkiteConnectWrapper.m_CounterProgressBar = 102;
                                    }
                                });
                            }
                            if (m_futName == "" || SymbolList.Count != m_RealTimeSymbols.Count)
                            {
                                InsertSymbolsIntoList();
                            }
                            lock (orderbookLock)
                            {
                                this.Dispatcher.Invoke(() =>
                                {
                                    if (borderMW.Visibility == Visibility.Hidden && borderOB.Visibility == Visibility.Hidden && borderOP.Visibility == Visibility.Hidden && borderOH.Visibility == Visibility.Hidden)
                                    {
                                        MadeChangesOnGridandcheckboxes();
                                    }
                                });
                                if (isClearAll == false)
                                {
                                    LoadWatchList();
                                }
                                LoadOpenPositions();
                                LoadOrderBook();
                                LoadOrderHoldings();
                                LoadLogs();
                                //LoadInformationStatus();
                                this.Dispatcher.Invoke(() =>
                                {

                                    UpdateChangedValues();
                                    //IRDSPM::PRatiksha::29-06-2021::For download csv changes
                                    // TimeSpan time = DateTime.ParseExact(m_ObjkiteConnectWrapper.m_TimeToExitAllNSEOrders);
                                    m_TimeToExitAllNSEOrder = DateTime.ParseExact(m_ObjkiteConnectWrapper.m_TimeToExitAllNSEOrders, "HH:mm:ss", null);
                                    DateTime maximumTime = m_TimeToExitAllNSEOrder.AddMinutes(1);
                                    if (m_TimeToExitAllNSEOrder != null && m_SendAttachment == false)
                                    {
                                        DateTime currentDateTime = DateTime.Now;
                                        if ((currentDateTime.ToOADate() >= m_TimeToExitAllNSEOrder.ToOADate()) && (currentDateTime.ToOADate() <= maximumTime.ToOADate()))
                                        {
                                            string sub = "RealStock Strategy Report";
                                            string body = "Sharing today's report, please find in attachment.\nThanks and Regards\nIRDS Support Team.";
                                            string filename = DateTime.Now.ToString("dd-MM-yyyy--HH-mm-ss") + "-" + m_UserID + "-OpenPosition" + ".csv";
                                            string filename1 = DateTime.Now.ToString("dd-MM-yyyy--HH-mm-ss") + "-" + m_UserID + "-OrderBook" + ".csv";
                                            string pathname = path + @"\Reports\" + filename;
                                            string pathname1 = path + @"\Reports\" + filename1;
                                            ToCSV(mydtOpenPosition, pathname, filename, true);
                                            ToCSV(mydtOrderBook, pathname1, filename1, true);
                                            SendInfo obj = new SendInfo();
                                            if (m_senderName != "XXXXXX" && m_senderemail != "XXXXXX" && m_senderhost != "XXXXXX" && m_senderpassword != "XXXXXX" && m_senderPort != "XXXXXX")
                                            {
                                                obj.SendEmailViaMailkit(m_email, sub, body, pathname, pathname1);
                                            }
                                            m_SendAttachment = true;
                                        }
                                    }
                                });
                                m_bRefreshDone = true;

                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    WriteUniquelogs("ExecutionForm" + " " + m_UserID, "LoadAllValuesOnGUI : Exception Error Message = " + e.Message, MessageType.Exception);
                }
                finally
                {
                    m_bRefreshDone = true;
                }
                Thread.Sleep(500);
            }
            WriteUniquelogs("ExecutionForm" + " " + m_UserID, "LoadAllValuesOnGUI : Stopped thread m_StopThread " + m_StopThread, MessageType.Informational);
        }

        private void btnRollOver_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string message = "Are you sure to Roll over orders? ";
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                DialogResult dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                if (dialog == DialogResult.Yes)
                {
                    Dictionary<string, PositionInfo> positionResponseForOverall = new Dictionary<string, PositionInfo>();
                    m_ObjkiteConnectWrapper.GetAllPosition(out positionResponseForOverall);
                    if (positionResponseForOverall.Count != 0)
                    {
                        foreach (var valuePair in positionResponseForOverall)
                        {
                            var Position = valuePair.Value.netPosition;
                            if (Position.Quantity != 0)
                            {
                                m_ObjkiteConnectWrapper.RollOver(Position.TradingSymbol, Position.Exchange, Position.Product);
                            }
                        }
                    }
                }
                if (dialog == DialogResult.No)
                {
                    //nothing to do
                }
            }
            catch (Exception er)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "btnRollOver_Click : Exception Error Message = " + er.Message, MessageType.Exception);

            }
            finally
            {
                // btnRollOver.IsEnabled = true;
            }
        }

        public void UpdatePNL()
        {
            while (true && m_StopThread == false)
            {
                try
                {
                    LoadPNL();
                }
                catch (Exception e)
                {
                    WriteUniquelogs("ExecutionForm" + " " + m_UserID, "UpdatePNL : Exception Error Message = " + e.Message, MessageType.Exception);
                }
                Thread.Sleep(100);
            }
            WriteUniquelogs("ExecutionForm" + " " + m_UserID, "UpdatePNL : Stopped thread m_StopThread " + m_StopThread, MessageType.Informational);
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            //IRDSPM::PRatiksha::28-07-2020::For successful login it should open popup of already login
            if (lblValueStatus.Content.ToString() == "Connected")
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "btnLogin_Click : You are already logged in.", MessageType.Informational);
                toastDisplay("You are already logged in.");
            }
            else
            {
                //sanika::2-dec-2020::Reload the all ini values
                m_ObjkiteConnectWrapper.ChangeUserID(m_UserID);
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "btnLogin_Click : Going to login", MessageType.Informational);
                m_ObjkiteConnectWrapper.Connect();
            }
        }

        private void btnFlogin_Click(object sender, RoutedEventArgs e)
        {
            m_ObjkiteConnectWrapper.ForceFulReLogin();
        }

        public void WriteUniquelogs(string fileName, string message, MessageType msgType)
        {
            try
            {
                SymbolLogs log = new SymbolLogs();
                log.createFile(fileName);
                log.LogMessage(message, MessageType.Informational);
                log = null;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : " + e.Message);
            }
        }

        public void StartTimer()
        {
            try
            {
                //sanika::moved code from conrtuctor to this function
                //IRDSPM::PRatiksha::03-09-2020::To enable or disable MCX symbol setting --start
                //m_TickDataDownloader = m_ObjkiteConnectWrapper.GetTickDataDownloaderFlag();
                try
                {
                    if (m_TickDataDownloader == true)
                    {
                        MCXMenuItem.IsEnabled = true;
                    }
                    else
                    {
                        MCXMenuItem.IsEnabled = false;
                    }
                }
                catch (Exception e)
                {
                    WriteUniquelogs("ExecutionForm" + " " + m_UserID, "StartTimer : Exception Error Message = " + e.Message, MessageType.Exception);
                }
                //IRDSPM::PRatiksha::03-09-2020::To enable or disable MCX symbol setting --end

                m_Timer = new System.Timers.Timer();
                m_Timer.Elapsed += _OnTimerExecute;
                //04-August-2021: sandip:memory issue
                m_Timer.Interval = 60000; // checks connection every second
                m_Timer.Enabled = true;
                logger.LogMessage("starttimer wpfmain : started new System.Timers.Timer()", MessageType.Informational);
                //20-July-2021: sandip:optimization of objects,namepipe commnted.
                //Sanika::3-Sep-2020::Added timer for read name pipe
                //m_TimerForBridgeStatus = new System.Timers.Timer();
                //m_TimerForBridgeStatus.Elapsed += _OnTimerExecuteForBridgeStatus;
                //m_TimerForBridgeStatus.Interval = 10000; // checks connection every second
                //m_TimerForBridgeStatus.Enabled = true;

                //this.Dispatcher.Invoke(() =>
                //{
                //    tabList.Items.Add(DateTime.Now + "  " + "Initially " + lblValueStatus.Content);
                //});

                //sanika::16-July::2021::commented as per sir's suggestion for odin client 
                //IRDSPM::Pratiksha::11-05-2021::Sending sms and email
                //SendsmsMail.Tick += new EventHandler(SendsmsMail_Tick);
                //SendsmsMail.Start();
            }
            catch (Exception e)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "StartTimer : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }
        //20-July-2021: sandip:optimization of objects,name pipe commnted
        //private void _OnTimerExecuteForBridgeStatus(object sender, System.Timers.ElapsedEventArgs e)
        //{
        //try
        //{
        //    if (m_ReadResponseFromNamedPipe != null)
        //    {
        //        if (m_ReadResponseFromNamedPipe.IsAlive)
        //        {
        //            Thread.Sleep(1000);
        //        }
        //        else
        //        {
        //            m_ReadResponseFromNamedPipe.Start();
        //            WriteUniquelogs("ExecutionForm" + " " + m_UserID, "_OnTimerExecuteForBridgeStatus : Started Thread of receivedResponse", MessageType.Informational);
        //        }
        //    }
        //    else
        //    {
        //        if (m_ReadResponseFromNamedPipe == null)
        //        {
        //            m_ReadResponseFromNamedPipe = new Thread(() => ReceiveResponse());
        //            m_ReadResponseFromNamedPipe.Start();
        //            WriteUniquelogs("ExecutionForm" + " " + m_UserID, "_OnTimerExecuteForBridgeStatus : Started Thread of receivedResponse", MessageType.Informational);
        //        }
        //    }
        //}
        //catch (Exception er)
        //{
        //    WriteUniquelogs("ExecutionForm" + " " + m_UserID, "_OnTimerExecuteForBridgeStatus : Exception Error Message = " + er.Message, MessageType.Exception);
        //}
        //}

        private void _OnTimerExecute(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                if (m_LoadAllValuesOnGUI != null)
                {
                    if (m_LoadAllValuesOnGUI.IsAlive)
                    {
                        Thread.Sleep(1000);
                    }
                    else
                    {
                        m_LoadAllValuesOnGUI.Start();
                        WriteUniquelogs("ExecutionForm" + " " + m_UserID, "_OnTimerExecute : Started Thread of loadValues", MessageType.Informational);
                    }
                }
                else
                {
                    if (m_LoadAllValuesOnGUI == null)
                    {
                        m_LoadAllValuesOnGUI = new Thread(() => LoadAllValuesOnGUI());
                        m_LoadAllValuesOnGUI.Start();
                        WriteUniquelogs("ExecutionForm" + " " + m_UserID, "_OnTimerExecute : Started Thread of loadValues", MessageType.Informational);
                    }
                }
                if (m_LoadMarginValuesOnGUI != null)
                {
                    if (m_LoadMarginValuesOnGUI.IsAlive)
                    {
                        Thread.Sleep(1000);
                    }
                    else
                    {
                        m_LoadMarginValuesOnGUI.Start();
                        WriteUniquelogs("ExecutionForm" + " " + m_UserID, "_OnTimerExecute : Started Thread of loadMarginValues", MessageType.Informational);
                    }
                }
                else
                {
                    if (m_LoadMarginValuesOnGUI == null)
                    {
                        m_LoadMarginValuesOnGUI = new Thread(() => LoadInformationStatus());
                        m_LoadMarginValuesOnGUI.Start();
                        WriteUniquelogs("ExecutionForm" + " " + m_UserID, "_OnTimerExecute : Started Thread of loadMarginValues", MessageType.Informational);
                    }
                }
            }
            catch (Exception er)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "_OnTimerExecute : Exception Error Message = " + er.Message, MessageType.Exception);
            }
        }


        //sanika::16-sep-2020::remove order and time column, added ltp column
        public void LoadOpenPositions()
        {
            int counter = 0;
            ListForUpdatedOpenPositionGridView.Clear();
            try
            {
                //Dictionary<string, PositionInfo> positionResponseForOverall = new Dictionary<string, PositionInfo>();
                //Sanika::11-sep-2020::Remove out variable as per latest design
                dynamic positionResponseForOverall = m_ObjkiteConnectWrapper.GetAllPosition();
                //m_ObjkiteConnectWrapper.GetAllPosition(out positionResponseForOverall);
                if (positionResponseForOverall.Count != 0)
                {
                    foreach (var valuePair in positionResponseForOverall)
                    {
                        var Position = valuePair.Value.netPosition;
                        string transactionType = "";
                        string price = "";
                        if (Position.Exchange == Constants.EXCHANGE_NFO && !Position.TradingSymbol.Contains(m_futName))
                        {
                            //sanika::6-May-2021::Added check for option symbols
                            if (!((Position.TradingSymbol.EndsWith("PE") || Position.TradingSymbol.EndsWith("CE")) && Position.TradingSymbol.Contains(m_CurrentDate.ToString("yy"))))
                                Position.TradingSymbol = Position.TradingSymbol + m_futName;
                        }
                        string lastPrice = m_ObjkiteConnectWrapper.GetLastTradedPrice(Position.TradingSymbol);
                        if (lastPrice == "")
                        {
                            lastPrice = Position.LastPrice.ToString();
                        }
                        double p_l = 0;
                        //use pnl from position
                        if (m_usePNLFromPosition)
                        {
                            //sanika::23-sep-2020::added condition for trasaction type
                            if (Position.Quantity > 0)
                            {
                                transactionType = Constants.TRANSACTION_TYPE_BUY;
                            }
                            else
                            {
                                transactionType = Constants.TRANSACTION_TYPE_SELL;
                            }
                            double openPrice = m_ObjkiteConnectWrapper.GetOpenPostionPrice(Position.TradingSymbol, Position.Exchange, Position.Product);
                            if (openPrice == 0)
                            {
                                openPrice = Math.Round(Convert.ToDouble(Position.AveragePrice), 2);
                            }
                            price = openPrice.ToString();

                            double pl = Convert.ToDouble(Math.Round(Position.PNL, 2));
                            p_l = pl;
                            if (Position.Quantity != 0)
                            {
                                counter++;
                            }
                        }
                        //calculate pnl 
                        else
                        {
                            //sanika::19-Nov-2020::Added price pass as empty
                            double openPrice = m_ObjkiteConnectWrapper.GetOpenPostionPrice(Position.TradingSymbol, Position.Exchange, Position.Product);
                            if (openPrice == 0)
                            {
                                openPrice = Math.Round(Convert.ToDouble(Position.AveragePrice), 2);
                            }
                            price = openPrice.ToString();

                            //var net = valuePair.Value.netPosition;
                            var net = Position;
                            string ltp = m_ObjkiteConnectWrapper.GetLastTradedPrice(net.TradingSymbol);
                            double sellValue = Convert.ToDouble(net.SellValue);
                            double buyValue = Convert.ToDouble(net.BuyValue);
                            if (ltp == "")
                            {
                                ltp = net.LastPrice.ToString();
                            }
                            double openValue = Math.Abs(Convert.ToDouble(ltp) * net.Quantity);
                            //WriteUniquelogs(net.TradingSymbol + " " + m_UserID, "ltp = " + ltp + " sellValue = " + sellValue + " buyValue = " + buyValue + " openValue = " + openValue,MessageType.Informational);
                            if (net.Quantity > 0)
                            {
                                sellValue += openValue;
                                double mtm = sellValue - buyValue;
                                //WriteUniquelogs(net.TradingSymbol + " " + m_UserID, "mtm = " + mtm, MessageType.Informational);
                                //Trace.WriteLine(net.TradingSymbol + " " + mtm); 
                                p_l = Math.Round(mtm, 2);
                                counter++;
                            }
                            else if (net.Quantity < 0)
                            {
                                buyValue += openValue;
                                double mtm = sellValue - buyValue;
                                //WriteUniquelogs(net.TradingSymbol + " " + m_UserID, "mtm = " + mtm, MessageType.Informational);
                                // Trace.WriteLine(net.TradingSymbol + " " + mtm);   
                                p_l = Math.Round(mtm, 2);
                                counter++;
                            }
                            else
                            {
                                double mtm = Convert.ToDouble(net.PNL);
                                //WriteUniquelogs(net.TradingSymbol + " " + m_UserID, "mtm = " + mtm + " mtmTest = "+ mtmTest, MessageType.Informational);
                                //Trace.WriteLine(net.TradingSymbol + " " + mtm);
                                p_l = Math.Round(mtm, 2);
                            }
                        }
                        double MTM = p_l;
                        string symbolName = Position.TradingSymbol + "|" + Position.Product;

                        if (!ListForUpdatedOpenPositionGridView.Contains(symbolName))
                        {
                            ListForUpdatedOpenPositionGridView.Add(symbolName);
                        }

                        if (m_AllPositions)
                        {
                            if (!ListForOpenPositionGridView.Contains(symbolName))
                            {
                                addIntoDataGridView(Position.Exchange, Position.TradingSymbol, Position.Product, Position.Quantity.ToString(), lastPrice, price, MTM);
                            }
                            else
                            {
                                updateIntoDataGridView(Position.Exchange, Position.TradingSymbol, Position.Product, Position.Quantity.ToString(), lastPrice, price, MTM);
                            }
                        }
                        else if (m_OpenPositions && Position.Quantity != 0)
                        {
                            if (!ListForOpenPositionGridView.Contains(symbolName))
                            {
                                addIntoDataGridView(Position.Exchange, Position.TradingSymbol, Position.Product, Position.Quantity.ToString(), lastPrice, price, MTM);
                            }
                            else
                            {
                                updateIntoDataGridView(Position.Exchange, Position.TradingSymbol, Position.Product, Position.Quantity.ToString(), lastPrice, price, MTM);
                            }
                        }
                        else if (m_ClosePosition && Position.Quantity == 0)
                        {
                            if (!ListForOpenPositionGridView.Contains(symbolName))
                            {
                                addIntoDataGridView(Position.Exchange, Position.TradingSymbol, Position.Product, Position.Quantity.ToString(), lastPrice, price, MTM);
                            }
                            else
                            {
                                updateIntoDataGridView(Position.Exchange, Position.TradingSymbol, Position.Product, Position.Quantity.ToString(), lastPrice, price, MTM);
                            }
                        }
                    }
                }
                //sanika::23-sep-2020::commented - code not working for remove row
                if (ListForOpenPositionGridView.Count > ListForUpdatedOpenPositionGridView.Count)
                {
                    for (int i = 0; i < ListForOpenPositionGridView.Count - 1; i++)
                    {
                        string sym = ListForOpenPositionGridView[i];
                        if (!ListForUpdatedOpenPositionGridView.Contains(sym))
                        {
                            RemoveIntoDataGridView(sym);
                            ListForOpenPositionGridView.Remove(sym);
                        }
                    }
                }


                int totalOPenPosition = counter;
                this.Dispatcher.Invoke(() =>
                {
                    lblOpenPositionCount.Content = totalOPenPosition;
                    lblLastupdatedTime.Content = DateTime.Now.ToString();
                });
            }
            catch (Exception er)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "loadOpenPositions : Exception Error Message = " + er.Message, MessageType.Exception);
            }

        }
        public void LoadPNL()
        {
            try
            {
                //sanika::25-sep-2020::Added condition for null exception
                if (m_ObjkiteConnectWrapper != null)
                {
                    double totalMTM = m_ObjkiteConnectWrapper.GetTotalMTM();
                    //Trace.WriteLine("*****Displaying PNL on gui " + totalMTM);
                    this.Dispatcher.Invoke(() =>
                    {
                        if (totalMTM > 0)
                        {
                            lblTotalPNL.Foreground = Brushes.Green;
                        }
                        else
                        {
                            lblTotalPNL.Foreground = Brushes.Red;
                        }
                        lblTotalPNL.Content = totalMTM.ToString();
                        //Trace.WriteLine("*****After Displaying PNL on gui " + totalMTM);

                       //sanika::6-Aug-2021::added condition for exception
                        if (lblMarginAvailable.Content.ToString() != "NA" && lblMarginAvailable.Content.ToString() != "NA" && lblTotalPNL.Content.ToString() != "NA")
                        {
                            //sanika::6-Aug-2021::added code for margin gets 0 in sms
                            double ma = Convert.ToDouble(lblMarginAvailable.Content.ToString());
                            double mu = Convert.ToDouble(lblMarginAvailable.Content.ToString());
                            double pnl = Convert.ToDouble(lblTotalPNL.Content.ToString());
                            if (m_isloaded == false)
                            {
                                m_LowestMA = ma;
                                m_HighestMA = m_LowestMA;
                                m_LowestPNL = pnl;
                                m_HighestPNL = m_LowestPNL;
                                m_isloaded = true;
                            }

                            if (m_LowestMA > ma)
                            {
                                m_LowestMA = ma;
                            }
                            if (m_HighestMA < ma)
                            {
                                m_HighestMA = ma;
                            }

                            if (m_LowestPNL > pnl)
                            {
                                m_LowestPNL = pnl;
                            }
                            if (m_HighestPNL < pnl)
                            {
                                m_HighestPNL = pnl;
                            }
                        }
                    });
                    //if(totalMTM!=0)
                    //    WritePNLIntoFile(totalMTM);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "LoadPNL : Exception Error Message = " + e.Message, MessageType.Exception);
            }

        }

        //sanika::16-sep-2020::remove order and time column, added ltp column
        public void addIntoDataGridView(string Exchange, string TradingSymbol, string Product, string Quantity, string Ltp, string Price, double MTM)
        {
            double openPrice = Math.Round(Convert.ToDouble(Price), 2);
            try
            {
                this.Dispatcher.Invoke(() =>
                {
                    mydtOpenPosition.Rows.Add(new object[] { Exchange, TradingSymbol, Product, Quantity, Ltp, openPrice.ToString(), MTM });
                    DataContext = this;
                });

                ListForOpenPositionGridView.Add(TradingSymbol + "|" + Product);
            }
            catch (Exception e)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "addIntoDataGridView : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        //sanika::16-sep-2020::remove order and time column, added ltp column
        public void updateIntoDataGridView(string Exchange, string TradingSymbol, string Product, string Quantity, string Ltp, string Price, double MTM)
        {
            try
            {
                foreach (DataRow dr in mydtOpenPosition.Rows)
                {
                    if (((dr["Symbol"]).ToString() == TradingSymbol) && (dr["Exchange"].ToString() == Exchange) && (dr["Product"].ToString() == Product))
                    {
                        double openPrice = Math.Round(Convert.ToDouble(Price), 2);
                        dr["Exchange"] = Exchange;
                        dr["Product"] = Product;
                        dr["Quantity"] = Quantity;
                        dr["LTP"] = Ltp.ToString();
                        dr["Price"] = openPrice.ToString();
                        dr["P&L"] = MTM;
                        ChangeinCellColorOpenPosition(MTM, dr);
                        break;
                    }

                }
            }
            catch (Exception e)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "updateIntoDataGridView : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        //IRDSPM::PRatiksha::29-09-2020::For cell color change of P&L
        private void ChangeinCellColorOpenPosition(double MTM, DataRow dr)
        {
            this.Dispatcher.Invoke(() =>
            {
                if (opPL == "true")
                {
                    DataGridRow firstRow = marketOpenPriceGrid.ItemContainerGenerator.ContainerFromItem(marketOpenPriceGrid.Items[mydtOpenPosition.Rows.IndexOf(dr)]) as DataGridRow;
                    if (firstRow != null)
                    {
                        System.Windows.Controls.DataGridCell firstColumnInFirstRow = marketOpenPriceGrid.Columns[7].GetCellContent(firstRow).Parent as System.Windows.Controls.DataGridCell;
                        if (firstColumnInFirstRow != null)
                        {
                            firstColumnInFirstRow.FontWeight = FontWeights.Bold;
                            firstColumnInFirstRow.FontSize = 14;
                            if (MTM < 0)
                            {
                                firstColumnInFirstRow.Background = (Brush)bc.ConvertFrom("#FE0001");
                                firstColumnInFirstRow.Foreground = Brushes.White;
                            }
                            else if (MTM > 0)
                            {
                                firstColumnInFirstRow.Background = Brushes.LimeGreen;
                                firstColumnInFirstRow.Foreground = Brushes.White;
                            }
                            else if (MTM == 0)
                            {
                                firstColumnInFirstRow.Foreground = Brushes.Black;
                            }
                        }
                    }
                }
            });
        }

        public void RemoveIntoDataGridView(string symbolName)
        {
            try
            {
                string[] stringArray = symbolName.Split('|');

                for (int i = mydtOpenPosition.Rows.Count - 1; i >= 0; i--)
                {
                    DataRow dr = mydtOpenPosition.Rows[i];
                    if (((dr["Symbol"]).ToString() == stringArray[0]) && (dr["Product"].ToString() == stringArray[1]))
                    {
                        this.Dispatcher.Invoke(() =>
                        {
                            mydtOpenPosition.Rows.Remove(dr);
                            DataViewOpenPosition = mydtOpenPosition.DefaultView;
                            DataContext = this;
                        });
                        break;
                        //marketOpenPriceGrid.Items.RemoveAt(i);
                    }
                }


                //DataRow dataRow = null;
                //foreach (DataRow dr in mydtOpenPosition.Rows)
                //{
                //    if (((dr["Symbol"]).ToString() == stringArray[0]) && (dr["Product"].ToString() == stringArray[1]))
                //    {
                //        dataRow = dr;
                //        //dr.Delete();
                //        //dr.AcceptChanges();
                //        break;
                //    }

                //}
                //dataRow.Delete();               
                //mydtOpenPosition.AcceptChanges();
            }
            catch (Exception e)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "RemoveIntoDataGridView : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        //IRDSPM::Pratiksha::08-12-2020::For Marlet watch delete symbol
        public void RemoveFromMarketWatch(string symbolName)
        {
            try
            {
                for (int i = mydtMarketWatch.Rows.Count - 1; i >= 0; i--)
                {
                    DataRow dr = mydtMarketWatch.Rows[i];
                    if ((dr["Symbol"]).ToString() == symbolName)
                    {
                        this.Dispatcher.Invoke(() =>
                        {
                            mydtMarketWatch.Rows.Remove(dr);
                            DataViewMarketWatch = mydtMarketWatch.DefaultView;
                            DataContext = this;
                        });
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "RemoveFromMarketWatch : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }
        public void LoadOrderBook()
        {
            try
            {
                int counter = 0;
                //IRDS::Jyoti::08-09-2020::Samco Integration changes
                //List<Order> Orders = new List<Order>();
                //Sanika::11-sep-2020::Remove out variable as per latest design
                dynamic Orders = m_ObjkiteConnectWrapper.GetAllOrders();
                if (Orders.Count > 0)
                {
                    //var newList = Orders.OrderByDescending(x => x.OrderTimestamp).ToList();
                    if (m_PreviousOrderCount < Orders.Count)
                    {
                        mydtOrderBook.Clear();
                    }
                    foreach (var order in Orders)
                    {
                        if (order.Status == Constants.ORDER_STATUS_PENDING)
                        {
                            counter++;
                        }

                        if (m_AllOrders)
                        {
                            double openPrice = Math.Round(m_ObjkiteConnectWrapper.GetOpenPostionPricebyOrderID(order.Tradingsymbol, order.Exchange, order.OrderId), 2);

                            if (!ListForOrderBookGridView.Contains(order.OrderId))
                            {
                                addOrdersIntoDataGridView(order, openPrice);
                            }
                            else
                            {
                                updateOrderIntoDataGridView(order, openPrice);
                            }
                        }
                        else if (m_CompleteOrders && order.Status == Constants.ORDER_STATUS_COMPLETE)
                        {
                            double openPrice = Math.Round(m_ObjkiteConnectWrapper.GetOpenPostionPricebyOrderID(order.Tradingsymbol, order.Exchange, order.OrderId), 2);

                            if (!ListForOrderBookGridView.Contains(order.OrderId))
                            {
                                addOrdersIntoDataGridView(order, openPrice);
                            }
                            else
                            {
                                updateOrderIntoDataGridView(order, openPrice);
                            }
                        }
                        else if (m_CancelOrders && order.Status == Constants.ORDER_STATUS_CANCELLED)
                        {
                            double openPrice = Math.Round(m_ObjkiteConnectWrapper.GetOpenPostionPricebyOrderID(order.Tradingsymbol, order.Exchange, order.OrderId), 2);

                            if (!ListForOrderBookGridView.Contains(order.OrderId))
                            {
                                addOrdersIntoDataGridView(order, openPrice);
                            }
                            else
                            {
                                updateOrderIntoDataGridView(order, openPrice);
                            }
                        }
                        //sanika::8-oct-2020::Changed condition for displaying open orders
                        else if (m_PendingOrders && (order.Status == Constants.ORDER_STATUS_PENDING || order.Status == Constants.ORDER_STATUS_OPEN))
                        {
                            double openPrice = Math.Round(m_ObjkiteConnectWrapper.GetOpenPostionPricebyOrderID(order.Tradingsymbol, order.Exchange, order.OrderId), 2);

                            if (!ListForOrderBookGridView.Contains(order.OrderId))
                            {
                                addOrdersIntoDataGridView(order, openPrice);
                            }
                            else
                            {
                                updateOrderIntoDataGridView(order, openPrice);
                            }
                        }
                        if (m_PreviousOrderCount < Orders.Count)
                        {
                            if (!m_RealTimeSymbols.ContainsKey(order.Tradingsymbol) && (!ListForMarketWtchGridViewDeletedSymbols.Contains(order.Tradingsymbol)))
                            {
                                //IRDS::Jyoti::9-Sept-20::Added for options symbols
                                if (order.Exchange == "NFO" && (order.Tradingsymbol.EndsWith("CE") || order.Tradingsymbol.EndsWith("PE")))
                                    m_RealTimeSymbols.Add(order.Tradingsymbol, "NFO-OPT");
                                else
                                {
                                    if ((!ListForMarketWtchGridViewDeletedSymbols.Contains(order.Tradingsymbol)))
                                        m_RealTimeSymbols.Add(order.Tradingsymbol, order.Exchange);
                                }
                            }
                        }
                    }
                }
                m_PreviousOrderCount = Orders.Count;
                this.Dispatcher.Invoke(() =>
                {
                    lblOrdersCount.Content = Orders.Count;
                    lblOrderLastUpdatedTime.Content = DateTime.Now.ToString();
                });
                m_PendingOrderCount = counter;
                m_IsOrderBookUpdated = true;
            }
            catch (Exception e)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "LoadOrderBook : Exception Error Message = " + e.Message, MessageType.Exception);
            }

        }

        public void LoadOrderHoldings()
        {
            try
            {
                //IRDS::Jyoti::08-09-2020::Samco Integration changes
                //Dictionary<string,Holding> holdings = new  Dictionary<string, Holding>();
                dynamic holdings;
                m_ObjkiteConnectWrapper.GetAllHoldings(out holdings);
                if (m_ObjkiteConnectWrapper.GetAllHoldings(out holdings))
                {
                    foreach (var order in holdings)
                    {
                        if (!ListForOpenHoldingsGridView.Contains(order.Value.TradingSymbol))
                        {
                            this.Dispatcher.Invoke(() =>
                            {
                                mydtOpenHolding.Rows.Add(new object[] { order.Value.Exchange, order.Value.TradingSymbol, order.Value.Product, order.Value.Quantity, Math.Round(order.Value.ClosePrice, 2).ToString(), Math.Round(order.Value.AveragePrice, 2).ToString(), Math.Round(order.Value.PNL, 2).ToString() });
                                DataContext = this;
                            });

                            ListForOpenHoldingsGridView.Add(order.Value.TradingSymbol);
                        }
                        else
                        {
                            foreach (DataRow dr in mydtOpenHolding.Rows)
                            {
                                if ((dr["Symbol"]).ToString() == order.Value.TradingSymbol)
                                {
                                    dr["Exchange"] = order.Value.Exchange;
                                    dr["Symbol"] = order.Value.TradingSymbol;
                                    dr["Product"] = order.Value.Product;
                                    dr["Quantity"] = order.Value.Quantity;
                                    dr["Close Price"] = Math.Round(order.Value.ClosePrice, 2);
                                    dr["Avg Price"] = Math.Round(order.Value.AveragePrice, 2);
                                    dr["P&L"] = Math.Round(order.Value.PNL, 2);
                                    break;
                                }
                            }
                        }
                    }
                }

                this.Dispatcher.Invoke(() =>
                {
                    lblOpenHoldingsCount.Content = holdings.Count.ToString();
                    lblLastupdatedTime1.Content = DateTime.Now.ToString();
                });



            }
            catch (Exception e)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "LoadOrderBook : Exception Error Message = " + e.Message, MessageType.Exception);
            }

        }

        public void LoadWatchList()
        {
            try
            {
                if (m_TickDataDownloader)
                {
                    LoadTickDataSymbols();
                }
                else
                {
                    LoadRealTimeSymbols();
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "LoadWatchList : Exception Error Message = " + e.Message, MessageType.Exception);
            }

        }

        //public int m_CounterForDeleteMarketWatchList = 0;
        public void LoadRealTimeSymbols()
        {
            //IRDSPM::Pratiksha::08-12-2020::For removing symbol if we delete from symbol setting
            if (m_ObjkiteConnectWrapper.marketWatchdeletelistmain.Count > 0)// && m_CounterForDeleteMarketWatchList != m_ObjkiteConnectWrapper.marketWatchdeletelistmain.Count)
            {
                // if (m_CounterForDeleteMarketWatchList != m_ObjkiteConnectWrapper.marketWatchdeletelistmain.Count)
                {
                    for (int m = 0; m < m_ObjkiteConnectWrapper.marketWatchdeletelistmain.Count; m++)
                    {
                        //IRDSPM::PRatiksha::14-12-2020::Add becasue CDS symbol was unable to remove
                        string symbol = "";
                        string[] parts;
                        parts = m_ObjkiteConnectWrapper.marketWatchdeletelistmain[m].Split('.');
                        if (parts.Length == 2)
                        {
                            symbol = m_ObjkiteConnectWrapper.marketWatchdeletelistmain[m].Split('.')[0];
                            if (m_RealTimeSymbols.ContainsKey(symbol))
                            {
                                m_RealTimeSymbols.Remove(symbol);
                            }
                            if (ListForMarketWtchGridView.Contains(symbol))
                            {
                                ListForMarketWtchGridView.Remove(symbol);
                            }
                        }
                        else if (parts.Length == 3)
                        {
                            symbol = parts[0] + "." + parts[1];
                            if (m_RealTimeSymbols.ContainsKey(symbol))
                            {
                                m_RealTimeSymbols.Remove(symbol);
                            }
                            if (ListForMarketWtchGridView.Contains(symbol))
                            {
                                ListForMarketWtchGridView.Remove(symbol);
                            }
                        }
                        RemoveFromMarketWatch(symbol);
                    }
                    // m_CounterForDeleteMarketWatchList = m_ObjkiteConnectWrapper.marketWatchdeletelistmain.Count;
                }
            }

            foreach (var symbol in m_RealTimeSymbols)
            {
                //sanika::23-sep-2020::added try-catch
                try
                {
                    string tradingSymbol = symbol.Key;
                    if (tradingSymbol == "NIFTY 50" || tradingSymbol == "NIFTY BANK")
                    {
                        continue;
                    }
                    string exchange = symbol.Value;
                    //sanika::15-oct-2020::Added condirion for option symbols
                    if (exchange != Constants.EXCHANGE_NSE && exchange != Constants.EXCHANGE_NFO_OPT)
                    {
                        //IRDS::05-Sep-2020::Jyoti::Commented for saving original symbol
                        //sanika::3-dec-2020::Added condition if symbol already contains future name
                        if (!tradingSymbol.Contains(m_futName) && !tradingSymbol.Contains("-I") && exchange != Constants.EXCHANGE_CDS && !tradingSymbol.EndsWith("FUT"))
                            tradingSymbol = tradingSymbol + m_futName;
                    }

                    //IRDSPM::Pratiksha::02-10-2020::Change the datatype for ascnding and descending order
                    string ltpValue = m_ObjkiteConnectWrapper.GetLastTradedPrice(tradingSymbol);
                    //sanika::1-oct-2020::added condition for input string error
                    if (ltpValue == "")
                        ltpValue = "0";
                    double ltp = Convert.ToDouble(ltpValue);
                    double bid = Convert.ToDouble(m_ObjkiteConnectWrapper.GetBidPrice(tradingSymbol));
                    double ask = Convert.ToDouble(m_ObjkiteConnectWrapper.GetAskPrice(tradingSymbol));
                    double volume = Convert.ToDouble(m_ObjkiteConnectWrapper.GetVolumePrice(tradingSymbol));
                    //sanika::23-sep-2020::added new column
                    double open = Convert.ToDouble(m_ObjkiteConnectWrapper.GetOpenPrice(tradingSymbol));
                    double high = Convert.ToDouble(m_ObjkiteConnectWrapper.GetHighPrice(tradingSymbol));
                    double low = Convert.ToDouble(m_ObjkiteConnectWrapper.GetLowPrice(tradingSymbol));
                    double close = Convert.ToDouble(m_ObjkiteConnectWrapper.GetClosePrice(tradingSymbol));
                    double changeInPercent = Convert.ToDouble(m_ObjkiteConnectWrapper.GetChangeInPercent(tradingSymbol));
                    double netChange = Convert.ToDouble(m_ObjkiteConnectWrapper.GetChangeInAbsolute(tradingSymbol));

                    if (ListForMarketWtchGridView.Contains(tradingSymbol))
                    {
                        foreach (DataRow dr in mydtMarketWatch.Rows)
                        {
                            if ((dr["Symbol"]).ToString() == tradingSymbol)
                            {
                                dr["Volume"] = volume;
                                //sanika::23-sep-2020::added new column
                                dr["Open"] = open;
                                dr["High"] = high;
                                dr["Low"] = low;
                                //IRDSPM::Pratiksha::08-10-2020::Rename close to Prev Close -"Client suggestion"
                                dr["Prev Close"] = close;
                                dr["Change in %"] = changeInPercent;
                                dr["LTP"] = ltp;
                                dr["Bid Price"] = bid;
                                dr["Ask Price"] = ask;
                                //IRDSPM::Pratiksha::08-10-2020::Added new column "Net Change"
                                dr["Net Change"] = netChange;
                                //IRDSPM::Pratiksha::10-10-2020::to chnage color of net change
                                CellColorChangeForMarketWatch(tradingSymbol, ltp, bid, ask, changeInPercent, netChange, dr);
                                break;
                            }
                        }
                        //}
                    }
                    else
                    {
                        this.Dispatcher.Invoke(() =>
                        {
                            //sanika::23-sep-2020::added new column
                            //if (!ListForMarketWtchGridViewDeletedSymbols.Contains(tradingSymbol))
                            //{
                            mydtMarketWatch.Rows.Add(new object[] { tradingSymbol, ltp, bid, ask, volume, open, high, low, close, changeInPercent });
                            DataContext = this;
                            //}
                        });

                        ListForMarketWtchGridView.Add(tradingSymbol);
                    }

                    //IRDS::30-SEP-20::Jyoti::Added for public ltp, ask, bid changes
                    if (symbolWiseTickInfo.ContainsKey(tradingSymbol))
                    {
                        symbolWiseTickInfo[tradingSymbol].LTP = Convert.ToDecimal(ltp);
                        symbolWiseTickInfo[tradingSymbol].AskPrice = Convert.ToDecimal(ask);
                        symbolWiseTickInfo[tradingSymbol].BidPrice = Convert.ToDecimal(bid);
                    }
                    else
                    {
                        TickData tickData = new TickData();
                        tickData.LTP = Convert.ToDecimal(ltp);
                        tickData.AskPrice = Convert.ToDecimal(ask);
                        tickData.BidPrice = Convert.ToDecimal(bid);
                        symbolWiseTickInfo.Add(tradingSymbol, tickData);
                    }
                }
                catch (Exception e)
                {
                    WriteUniquelogs("ExecutionForm" + " " + m_UserID, "LoadRealTimeSymbols : Exception Error Message = " + e.Message, MessageType.Exception);
                }
            }
        }

        //IRDSPM::Pratiksha::29-09-2020::For background cell color change
        private void CellColorChangeForMarketWatch(string symbol, double ltp, double bid, double ask, double changeInPercent, double netChange, DataRow dr)
        {
            try
            {
                this.Dispatcher.Invoke(() =>
                {
                    if (marketWatchGrid.Items.Count > 0)
                    {
                        DataGridRow firstRow = marketWatchGrid.ItemContainerGenerator.ContainerFromItem(marketWatchGrid.Items[mydtMarketWatch.Rows.IndexOf(dr)]) as DataGridRow;

                        if (firstRow != null)
                        {
                            //IRDSPM::PRatiksha::21-10-2020::If values are zero then do not change the styles --start

                            if (ltp == 0 && bid == 0 && ask == 0 && changeInPercent == 0 && netChange == 0)
                            {
                                if (mwltpval == "true")
                                {
                                    System.Windows.Controls.DataGridCell CellLTP = marketWatchGrid.Columns[1].GetCellContent(firstRow).Parent as System.Windows.Controls.DataGridCell;
                                    CellLTP.FontWeight = FontWeights.Normal;
                                    CellLTP.FontSize = 12;
                                    CellLTP.Background = Brushes.Transparent;
                                    CellLTP.Foreground = Brushes.Black;
                                }
                                if (mwbidval == "true")
                                {
                                    System.Windows.Controls.DataGridCell CellBidPrice = marketWatchGrid.Columns[2].GetCellContent(firstRow).Parent as System.Windows.Controls.DataGridCell;
                                    CellBidPrice.FontWeight = FontWeights.Normal;
                                    CellBidPrice.FontSize = 12;
                                    CellBidPrice.Background = Brushes.Transparent;
                                    CellBidPrice.Foreground = Brushes.Black;
                                }

                                if (mwchangeinper == "true")
                                {
                                    System.Windows.Controls.DataGridCell CellChangeInPercent = marketWatchGrid.Columns[9].GetCellContent(firstRow).Parent as System.Windows.Controls.DataGridCell;
                                    CellChangeInPercent.FontWeight = FontWeights.Normal;
                                    CellChangeInPercent.FontSize = 12;
                                    CellChangeInPercent.Background = Brushes.Transparent;
                                    CellChangeInPercent.Foreground = Brushes.Black;
                                }
                                if (mwaskval == "true")
                                {

                                    System.Windows.Controls.DataGridCell CellAsk = marketWatchGrid.Columns[3].GetCellContent(firstRow).Parent as System.Windows.Controls.DataGridCell;
                                    CellAsk.FontWeight = FontWeights.Normal;
                                    CellAsk.FontSize = 12;
                                    CellAsk.Background = Brushes.Transparent;
                                    CellAsk.Foreground = Brushes.Black;
                                }
                                if (mwNetChange == "true")
                                {
                                    System.Windows.Controls.DataGridCell cellNetChange = marketWatchGrid.Columns[10].GetCellContent(firstRow).Parent as System.Windows.Controls.DataGridCell;
                                    cellNetChange.FontWeight = FontWeights.Normal;
                                    cellNetChange.FontSize = 12;
                                    cellNetChange.Background = Brushes.Transparent;
                                    cellNetChange.Foreground = Brushes.Black;
                                }
                            }
                            //IRDSPM::PRatiksha::21-10-2020::If values are zero then do not change the styles --end
                            else
                            {
                                //Change in Percent
                                if (mwchangeinper == "true")
                                {
                                    System.Windows.Controls.DataGridCell CellChangeInPercent = marketWatchGrid.Columns[9].GetCellContent(firstRow).Parent as System.Windows.Controls.DataGridCell;
                                    double changeInPer = Convert.ToDouble(changeInPercent.ToString());
                                    if (CellChangeInPercent != null)
                                    {
                                        CellChangeInPercent.FontWeight = FontWeights.Bold;
                                        CellChangeInPercent.FontSize = 14;
                                        if (changeInPer < 0)
                                        {
                                            CellChangeInPercent.Background = (Brush)bc.ConvertFrom("#FE0001");
                                            CellChangeInPercent.Foreground = Brushes.White;
                                        }
                                        else if (changeInPer > 0)
                                        {
                                            CellChangeInPercent.Background = Brushes.LimeGreen;
                                            CellChangeInPercent.Foreground = Brushes.White;
                                        }
                                        else if (changeInPer == 0)
                                        {
                                            CellChangeInPercent.Background = Brushes.White;
                                            CellChangeInPercent.Foreground = Brushes.Black;
                                        }
                                    }
                                }

                                //IRDS::30-SEP-20::Jyoti::Added for public ltp, ask, bid changes
                                //LTP
                                if (mwltpval == "true")
                                {
                                    System.Windows.Controls.DataGridCell CellLTP = marketWatchGrid.Columns[1].GetCellContent(firstRow).Parent as System.Windows.Controls.DataGridCell;
                                    if (symbolWiseTickInfo.ContainsKey(symbol))
                                    {
                                        double oldLTP = (double)symbolWiseTickInfo[symbol].LTP;
                                        double doubleLTP = Convert.ToDouble(ltp);
                                        if (CellLTP != null)
                                        {
                                            CellLTP.FontWeight = FontWeights.Bold;
                                            CellLTP.FontSize = 14;
                                            if (oldLTP > doubleLTP)
                                            {
                                                CellLTP.Background = (Brush)bc.ConvertFrom("#FE0001");
                                                CellLTP.Foreground = Brushes.White;
                                            }
                                            else if (oldLTP < doubleLTP)
                                            {
                                                CellLTP.Background = Brushes.LimeGreen;
                                                CellLTP.Foreground = Brushes.White;
                                            }
                                            else if (doubleLTP == oldLTP)
                                            {
                                                CellLTP.Background = Brushes.White;
                                                CellLTP.Foreground = Brushes.Black;
                                            }
                                        }
                                    }
                                }

                                //Bid Price
                                if (mwbidval == "true")
                                {
                                    System.Windows.Controls.DataGridCell CellBidPrice = marketWatchGrid.Columns[2].GetCellContent(firstRow).Parent as System.Windows.Controls.DataGridCell;
                                    if (symbolWiseTickInfo.ContainsKey(symbol))
                                    {
                                        double oldBid = (double)symbolWiseTickInfo[symbol].BidPrice;
                                        double doubleBid = Convert.ToDouble(bid);
                                        if (CellBidPrice != null)
                                        {
                                            CellBidPrice.FontWeight = FontWeights.Bold;
                                            CellBidPrice.FontSize = 14;
                                            if (doubleBid < oldBid)
                                            {
                                                CellBidPrice.Background = (Brush)bc.ConvertFrom("#FE0001");
                                                CellBidPrice.Foreground = Brushes.White;
                                            }
                                            else if (doubleBid > oldBid)
                                            {
                                                CellBidPrice.Background = Brushes.LimeGreen;
                                                CellBidPrice.Foreground = Brushes.White;
                                            }
                                            else if (doubleBid == oldBid)
                                            {
                                                CellBidPrice.Background = Brushes.White;
                                                CellBidPrice.Foreground = Brushes.Black;
                                            }
                                        }
                                    }
                                }

                                //Ask Price
                                if (mwaskval == "true")
                                {
                                    System.Windows.Controls.DataGridCell CellAsk = marketWatchGrid.Columns[3].GetCellContent(firstRow).Parent as System.Windows.Controls.DataGridCell;
                                    if (symbolWiseTickInfo.ContainsKey(symbol))
                                    {
                                        double oldAsk = (double)symbolWiseTickInfo[symbol].AskPrice;
                                        double doubleask = Convert.ToDouble(ask);
                                        if (CellAsk != null)
                                        {
                                            CellAsk.FontWeight = FontWeights.Bold;
                                            CellAsk.FontSize = 14;
                                            if (doubleask < oldAsk)
                                            {
                                                CellAsk.Background = (Brush)bc.ConvertFrom("#FE0001");
                                                CellAsk.Foreground = Brushes.White;
                                            }
                                            else if (doubleask > oldAsk)
                                            {
                                                CellAsk.Background = Brushes.LimeGreen;
                                                CellAsk.Foreground = Brushes.White;
                                            }
                                            else if (doubleask == oldAsk)
                                            {
                                                CellAsk.Background = Brushes.White;
                                                CellAsk.Foreground = Brushes.Black;
                                            }
                                        }
                                    }
                                }

                                //IRDSPM::PRatiksha::10-10-2020::Color change for net Change
                                if (mwNetChange == "true")
                                {
                                    System.Windows.Controls.DataGridCell cellNetChange = marketWatchGrid.Columns[10].GetCellContent(firstRow).Parent as System.Windows.Controls.DataGridCell;
                                    if (cellNetChange != null)
                                    {
                                        cellNetChange.FontWeight = FontWeights.Bold;
                                        cellNetChange.FontSize = 14;
                                        if (netChange < 0)
                                        {
                                            cellNetChange.Background = (Brush)bc.ConvertFrom("#FE0001");
                                            cellNetChange.Foreground = Brushes.White;
                                        }
                                        else if (netChange > 0)
                                        {
                                            cellNetChange.Background = Brushes.LimeGreen;
                                            cellNetChange.Foreground = Brushes.White;
                                        }
                                        else if (netChange == 0)
                                        {
                                            cellNetChange.Background = Brushes.White;
                                            cellNetChange.Foreground = Brushes.Black;
                                        }
                                    }
                                }
                            }

                        }
                    }
                });
            }
            catch (Exception e)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "CellColorChangeForMarketWatch : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        public void LoadTickDataSymbols()
        {
            foreach (var symbol in ListForSymbolName)
            {
                //sanika::23-sep-2020::added try-catch
                try
                {
                    string tradingSymbol = symbol;
                    if (tradingSymbol == "NIFTY 50" || tradingSymbol == "NIFTY BANK")
                    {
                        continue;
                    }
                    /*string exchange = symbol.Value;
                    if (exchange != Constants.EXCHANGE_NSE)
                    {
                        if (!tradingSymbol.Contains(m_futName))
                            tradingSymbol = tradingSymbol + m_futName;
                    }*/
                    //IRDSPM::PRatiksha::02-10-2020::Change the datatype
                    //IRDSPM::PRatiksha::02-10-2020::Change told by sanika
                    string ltpValue = m_ObjkiteConnectWrapper.GetLastTradedPrice(tradingSymbol);
                    if (ltpValue == "")
                        ltpValue = "0";
                    double ltp = Convert.ToDouble(ltpValue);
                    double bid = Convert.ToDouble(m_ObjkiteConnectWrapper.GetBidPrice(tradingSymbol));
                    double ask = Convert.ToDouble(m_ObjkiteConnectWrapper.GetAskPrice(tradingSymbol));
                    double volume = Convert.ToDouble(m_ObjkiteConnectWrapper.GetVolumePrice(tradingSymbol));
                    //sanika::23-sep-2020::Added new columns
                    double open = Convert.ToDouble(m_ObjkiteConnectWrapper.GetOpenPrice(tradingSymbol));
                    double high = Convert.ToDouble(m_ObjkiteConnectWrapper.GetHighPrice(tradingSymbol));
                    double low = Convert.ToDouble(m_ObjkiteConnectWrapper.GetLowPrice(tradingSymbol));
                    double close = Convert.ToDouble(m_ObjkiteConnectWrapper.GetClosePrice(tradingSymbol));
                    double changeInPercent = Convert.ToDouble(m_ObjkiteConnectWrapper.GetChangeInPercent(tradingSymbol));
                    //IRDSPM::Pratiksha::08-10-2020::Added new column "Net Change"
                    double netChange = Convert.ToDouble(m_ObjkiteConnectWrapper.GetChangeInAbsolute(tradingSymbol));

                    if (ListForMarketWtchGridView.Contains(tradingSymbol))
                    {
                        foreach (DataRow dr in mydtMarketWatch.Rows)
                        {
                            if ((dr["Symbol"]).ToString() == tradingSymbol)
                            {
                                dr["LTP"] = ltp;
                                dr["Bid Price"] = bid;
                                dr["Ask Price"] = ask;
                                dr["Volume"] = volume;
                                //sanika::23-sep-2020::Added new columns
                                dr["Open"] = open;
                                dr["High"] = high;
                                dr["Low"] = low;
                                //IRDSPM::Pratiksha::08-10-2020::Rename close to Prev Close -"Client suggestion"
                                dr["Prev Close"] = close;
                                dr["Change in %"] = changeInPercent;
                                //IRDSPM::Pratiksha::08-10-2020::Added new column "Net Change"
                                dr["Net Change"] = netChange;
                                //IRDSPM::Pratiksha::16-10-2020::For background cell color change
                                CellColorChangeForMarketWatch(tradingSymbol, ltp, bid, ask, changeInPercent, netChange, dr);
                                break;
                            }
                        }
                    }
                    else
                    {
                        this.Dispatcher.Invoke(() =>
                        {
                            //sanika::23-sep-2020::Added new columns
                            mydtMarketWatch.Rows.Add(new object[] { tradingSymbol, ltp, bid, ask, volume, open, high, low, close, changeInPercent });
                            DataContext = this;
                        });

                        ListForMarketWtchGridView.Add(tradingSymbol);
                    }

                    //sanika::2-oct-2020::added for color changing 
                    //IRDS::30-SEP-20::Jyoti::Added for public ltp, ask, bid changes
                    if (symbolWiseTickInfo.ContainsKey(tradingSymbol))
                    {
                        symbolWiseTickInfo[tradingSymbol].LTP = Convert.ToDecimal(ltp);
                        symbolWiseTickInfo[tradingSymbol].AskPrice = Convert.ToDecimal(ask);
                        symbolWiseTickInfo[tradingSymbol].BidPrice = Convert.ToDecimal(bid);
                    }
                    else
                    {
                        TickData tickData = new TickData();
                        tickData.LTP = Convert.ToDecimal(ltp);
                        tickData.AskPrice = Convert.ToDecimal(ask);
                        tickData.BidPrice = Convert.ToDecimal(bid);
                        symbolWiseTickInfo.Add(tradingSymbol, tickData);
                    }
                }
                catch (Exception e)
                {
                    WriteUniquelogs("ExecutionForm" + " " + m_UserID, "LoadTickDataSymbols : Exception Error Message = " + e.Message, MessageType.Exception);
                }
            }
        }

        public void AddColumns()
        {
            try
            {
                //open position
                mydtOpenPosition = new DataTable();
                mydtOpenPosition.Columns.Add("Exchange");
                mydtOpenPosition.Columns.Add("Symbol");
                mydtOpenPosition.Columns.Add("Product");
                //IRDSPM::PRatiksha::02-10-2020::For Ascending order working properly
                mydtOpenPosition.Columns.Add("Quantity", typeof(Double));
                mydtOpenPosition.Columns.Add("LTP", typeof(Double));
                mydtOpenPosition.Columns.Add("Price", typeof(Double));
                mydtOpenPosition.Columns.Add("P&L", typeof(Double));
                DataViewOpenPosition = mydtOpenPosition.DefaultView;
                DataContext = this;

                //Market watch
                mydtMarketWatch = new DataTable();
                mydtMarketWatch.Columns.Add("Symbol");
                //IRDSPM::PRatiksha::02-10-2020::For Ascending order working properly
                mydtMarketWatch.Columns.Add("LTP", typeof(Double));
                mydtMarketWatch.Columns.Add("Bid Price", typeof(Double));
                mydtMarketWatch.Columns.Add("Ask Price", typeof(Double));
                mydtMarketWatch.Columns.Add("Volume", typeof(Double));
                //sanika::23-sep-2020::added new column
                mydtMarketWatch.Columns.Add("Open", typeof(Double));
                mydtMarketWatch.Columns.Add("High", typeof(Double));
                mydtMarketWatch.Columns.Add("Low", typeof(Double));
                //IRDSPM::Pratiksha::08-10-2020::Rename close to Prev Close -"Client suggestion"
                mydtMarketWatch.Columns.Add("Prev Close", typeof(Double));
                mydtMarketWatch.Columns.Add("Change in %", typeof(Double));
                //IRDSPM::Pratiksha::08-10-2020::Added new column "Net Change"
                mydtMarketWatch.Columns.Add("Net Change", typeof(Double));

                DataViewMarketWatch = mydtMarketWatch.DefaultView;
                DataContext = this;


                //order book
                mydtOrderBook = new DataTable();
                mydtOrderBook.Columns.Add("EXG");
                mydtOrderBook.Columns.Add("Symbol");
                //IRDSPM::PRatiksha::02-10-2020::For Ascending order working properly
                mydtOrderBook.Columns.Add("OrderId", typeof(Double));
                mydtOrderBook.Columns.Add("Time");
                mydtOrderBook.Columns.Add("Type");
                mydtOrderBook.Columns.Add("Order Type");
                mydtOrderBook.Columns.Add("Product");
                //IRDSPM::Pratiksha::29-07-2020::Replace order price to price
                mydtOrderBook.Columns.Add("Price", typeof(Double));
                mydtOrderBook.Columns.Add("Trigger Price", typeof(Double));
                mydtOrderBook.Columns.Add("QTY", typeof(Double));
                mydtOrderBook.Columns.Add("Status");
                mydtOrderBook.Columns.Add("Filled Qty", typeof(Double));
                mydtOrderBook.Columns.Add("Filled Price", typeof(Double));
                mydtOrderBook.Columns.Add("Rejection Order Remark");
                DataViewOrderBook = mydtOrderBook.DefaultView;
                DataContext = this;

                //open Holdings
                mydtOpenHolding = new DataTable();
                mydtOpenHolding.Columns.Add("Exchange");
                mydtOpenHolding.Columns.Add("Symbol");
                mydtOpenHolding.Columns.Add("Product");
                //IRDSPM::PRatiksha::02-10-2020::For Ascending order working properly
                mydtOpenHolding.Columns.Add("Quantity", typeof(Double));
                mydtOpenHolding.Columns.Add("Close Price", typeof(Double));
                mydtOpenHolding.Columns.Add("Avg Price", typeof(Double));
                mydtOpenHolding.Columns.Add("P&L", typeof(Double));
                DataViewOpenHoldings = mydtOpenHolding.DefaultView;
                DataContext = this;

                //by default all check box check for open position tab -- sanika::12-Aug-2020
                chkBoxAllPosition.IsChecked = true;
                stsAll.IsChecked = true;
            }
            catch (Exception e)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "AddColumns : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        private void OpenBrokerForm_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                m_apiSettingobj = null;
                if (m_apiSettingobj == null)
                {
                    m_apiSettingobj = new APISettingWindowsForm(m_ObjkiteConnectWrapper, lblValueStatus.Content.ToString(), title);
                }
                m_apiSettingobj.LoadKiteConnectObject(m_ObjkiteConnectWrapper, lblValueStatus.Content.ToString());
                m_apiSettingobj.ShowDialog();
            }
            catch (Exception er)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "OpenBrokerForm_Click : Exception Error Message = " + er.Message, MessageType.Exception);
            }
        }
        //IRDSPM::Pratiksha::27-07-2020::For Open new trading symbol form
        private void tradingSymbolSetting_Click(object sender, RoutedEventArgs e)
        {
            //IRDSPM::Pratiksha::18-09-2020::For trading setting
            //if(m_objTradingSymbol == null)
            {
                m_objTradingSymbol = new TradingSymbolWindowForm(m_ObjkiteConnectWrapper, title);
            }
            m_objTradingSymbol.LoadKiteConnectObject(m_ObjkiteConnectWrapper);
            m_objTradingSymbol.ShowDialog();
            //ReadDownloadTokenFromIni();
        }

        private void OpenSymbolSetting_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bool m_LoginStatus = m_ObjkiteConnectWrapper.GetConnectionStatus();
                if (m_LoginStatus)
                {
                    if (m_symbolSettingWindowObj == null)
                    {
                        m_symbolSettingWindowObj = new AllSymbolSettingWindowsForm(m_ObjkiteConnectWrapper, title);
                    }

                    m_symbolSettingWindowObj.ShowDialog();
                    isClearAll = false;
                }
                else
                {
                    string message = "User not logged in.";
                    MessageBoxButtons buttons = MessageBoxButtons.OK;
                    DialogResult dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                }
                //string message = "User not logged in.";
                //string title = "IRDS Algo Trader";
                //MessageboxOk obj = new MessageboxOk(title, message);
                //obj.ShowDialog();
            }
            catch (Exception er)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "opensymnolsetting_Click : Exception Error Message = " + er.Message, MessageType.Exception);
            }
        }

        private void openGeneralSetting_Click(object sender, RoutedEventArgs e)
        {
            GeneralSettingWindowForm obj = new GeneralSettingWindowForm(m_ObjkiteConnectWrapper, title);
            obj.ShowDialog();
            RefreshSettinginiFile();
            //Pratiksha::Fetch data when changed from form
            FetchNotificationsDet();
            MadeChangesOnGridandcheckboxes();
        }

        public void LoadInformationStatus()
        {
            while (true && m_StopThread == false)
            {
                if (m_ObjkiteConnectWrapper != null && m_ObjkiteConnectWrapper.GetConnectionStatus())
                {
                    Trace.WriteLine("LoadInformationStatus started: " + DateTime.Now);
                    try
                    {
                        decimal usedMargin = 0;
                        decimal marginAvailable = 0;
                        decimal openingBalance = 0;
                        //IRDS::Jyoti::08-09-2020::Samco Integration changes
                        Trace.WriteLine("GetMargin started: " + DateTime.Now);
                        dynamic userMargin = m_ObjkiteConnectWrapper.GetMarginFromStructure();
                        Trace.WriteLine("GetMargin ended: " + DateTime.Now);
                        //sanika::6-oct-2020::Added condition for excpetion of userMargin
                        if (userMargin != null && userMargin.Count != 0)
                        {
                            usedMargin = Math.Round(userMargin["Utilised"], 2);
                            marginAvailable = Math.Round(userMargin["Net"], 2);
                            openingBalance = userMargin["Available"];
                        }
                        //sanika::16-sep-2020::get changed userid bacause on GUI displaying xxxxxx
                        m_UserID = m_ObjkiteConnectWrapper.GetUserID();
                        Trace.WriteLine("GetUserID ended: " + DateTime.Now);
                        this.Dispatcher.Invoke(() =>
                        {
                            lblClientIDVal.Content = m_UserID;
                            lblClientID.Content = m_UserID;
                            //IRDSPM::PRatiksha::03-09-2020::LAble padding change
                            lblBridgeStatus.Content = m_BridgeConnection;
                            //IRDSPM::Pratiksha::26-08-2020::For bridge status background
                            if (m_BridgeConnection == "Connected")
                            {
                                lblBridgeStatus.Background = Brushes.LimeGreen;
                            }
                            else if (m_BridgeConnection == "Disconnected")
                            {
                                lblBridgeStatus.Background = Brushes.Red;
                            }
                            //sanika::6-oct-2020::Added condition for excpetion of userMargin
                            if (userMargin != null && userMargin.Count != 0)
                            {
                                lblMarginAvailable.Content = marginAvailable.ToString();
                                lblMarginAvailabletab.Content = marginAvailable.ToString();
                                lblMarginUsed.Content = usedMargin.ToString();
                                lblMarginUsedtab.Content = usedMargin.ToString();
                                lblOpeningBalance.Content = openingBalance.ToString();
                                lblOpeningBalancetab.Content = openingBalance.ToString();
                            }
                            Trace.WriteLine("UpdateChangedValues started: " + DateTime.Now);
                            //UpdateChangedValues();
                        });

                    }
                    catch (Exception e)
                    {
                        WriteUniquelogs("ExecutionForm" + " " + m_UserID, "LoadInformationStatus : Exception Error Message = " + e.Message, MessageType.Exception);
                    }
                    Trace.WriteLine("LoadInformationStatus ended: " + DateTime.Now);
                }
                //sanika::26-Jul-2021::added sleep to reduce cpu usage
                Thread.Sleep(1000);
            }
        }

        //sanika::24-sep-2020::added seperate function for nifty 50 nifty bank
        public void UpdateChangedValues()
        {
            try
            {
                //IRDSPM::Pratiksha::31-07-2020::Change the font color if -ve then red and if positive is green
                //Please enter the nifty50 status value inside the round  backets
                string nifty50LastPrice = m_ObjkiteConnectWrapper.GetLastTradedPrice("NIFTY 50");
                string bankniftyLastPrice = m_ObjkiteConnectWrapper.GetLastTradedPrice("NIFTY BANK");
                if (nifty50LastPrice == "" && bankniftyLastPrice == "")
                {
                    if (File.Exists(m_FilePath))
                    {
                        string lastLine = System.IO.File.ReadLines(m_FilePath).Last();
                        if (lastLine.Length > 0)
                        {
                            string[] splittedLine = lastLine.Split(' ');
                            nifty50LastPrice = splittedLine[0];
                            bankniftyLastPrice = splittedLine[1];
                        }
                    }
                    else
                    {
                        WriteUniquelogs("ExecutionForm" + " " + m_UserID, "LoadInformationStatus : " + m_FilePath + " file does not exist", MessageType.Informational);
                    }
                }
                string nifty50Close = m_ObjkiteConnectWrapper.GetClosePrice("NIFTY 50");
                string bankniftyClose = m_ObjkiteConnectWrapper.GetClosePrice("NIFTY BANK");
                if (nifty50LastPrice != "" && nifty50Close != "0")//sanika::26-sep-2020::changed condition of close value for issue of 100%
                {
                    double changeInValues = Convert.ToDouble(nifty50LastPrice) - Convert.ToDouble(nifty50Close);
                    double roundupchangeInValues = Math.Round(changeInValues, 2);
                    string basePrice;
                    if (Convert.ToDouble(nifty50LastPrice) > Convert.ToDouble(nifty50Close))
                    {
                        basePrice = nifty50LastPrice;
                    }
                    else
                    {
                        basePrice = nifty50Close;
                    }
                    double changeInPercentage = Math.Round(((changeInValues / Convert.ToDouble(basePrice)) * 100), 2);
                    if (changeInPercentage == 100)
                    {

                    }
                    //IRDSPM::Pratiksha::03-08-2020::For dynamic size of nifty value --start
                    if (nifty50LastPrice.Length == 5)
                    {
                        string sample = nifty50LastPrice + ".00";
                        statusNifty50.Content = sample;
                    }

                    if (nifty50LastPrice.Length == 7)
                    {
                        string sample = nifty50LastPrice + "0";
                        statusNifty50.Content = sample;
                    }
                    if (nifty50LastPrice.Length > 7)
                    {
                        statusNifty50.Content = nifty50LastPrice;
                    }
                    lblniftyVal.Content = roundupchangeInValues.ToString() + " (" + changeInPercentage.ToString() + "%)";
                    if (Convert.ToDouble(changeInPercentage) > 0)
                    {
                        statusNifty50.Foreground = Brushes.Green;
                    }
                    else
                    {
                        statusNifty50.Foreground = Brushes.Red;
                    }

                }
                else if (nifty50LastPrice != "")
                {
                    if (nifty50LastPrice.Length == 5)
                    {
                        string sample = nifty50LastPrice + ".00";
                        statusNifty50.Content = sample;
                    }

                    if (nifty50LastPrice.Length == 7)
                    {
                        string sample = nifty50LastPrice + "0";
                        statusNifty50.Content = sample;
                    }
                    if (nifty50LastPrice.Length > 7)
                    {
                        statusNifty50.Content = nifty50LastPrice;
                    }
                }

                //Please enter the banknifty status value inside the round  backets
                if (bankniftyLastPrice != "" && bankniftyClose != "0") //sanika::26-sep-2020::changed condition of close value for issue of 100%
                {
                    double changeInValues = Convert.ToDouble(bankniftyLastPrice) - Convert.ToDouble(bankniftyClose);
                    double roundupchangeInValues = Math.Round(changeInValues, 2);
                    string basePrice;
                    if (Convert.ToDouble(bankniftyLastPrice) > Convert.ToDouble(bankniftyClose))
                    {
                        basePrice = bankniftyLastPrice;
                    }
                    else
                    {
                        basePrice = bankniftyClose;
                    }
                    double changeInPercentage = Math.Round(((changeInValues / Convert.ToDouble(basePrice)) * 100), 2);
                    statusBankNifty.Content = bankniftyLastPrice;
                    lblbankniftypnlval.Content = roundupchangeInValues.ToString() + " (" + changeInPercentage.ToString() + "%)";
                    if (Convert.ToDouble(changeInPercentage) > 0)
                    {
                        statusBankNifty.Foreground = Brushes.Green;
                    }
                    else
                    {
                        statusBankNifty.Foreground = Brushes.Red;
                    }
                    if (bankniftyLastPrice.Length == 5)
                    {
                        string sample = bankniftyLastPrice + ".00";
                        statusBankNifty.Content = sample;
                    }

                    if (bankniftyLastPrice.Length == 7)
                    {
                        string sample = bankniftyLastPrice + "0";
                        statusBankNifty.Content = sample;
                    }
                    if (bankniftyLastPrice.Length > 7)
                    {
                        statusBankNifty.Content = bankniftyLastPrice;
                    }
                }
                else if (bankniftyLastPrice != "")
                {
                    if (bankniftyLastPrice.Length == 5)
                    {
                        string sample = bankniftyLastPrice + ".00";
                        statusBankNifty.Content = sample;
                    }

                    if (bankniftyLastPrice.Length == 7)
                    {
                        string sample = bankniftyLastPrice + "0";
                        statusBankNifty.Content = sample;
                    }
                    if (bankniftyLastPrice.Length > 7)
                    {
                        statusBankNifty.Content = bankniftyLastPrice;
                    }
                }

                //Sanika::03-Sep-2020::Added to write last price into file
                using (StreamWriter streamWriter = new StreamWriter(m_FilePath, false))
                {
                    streamWriter.WriteLine(nifty50LastPrice + " " + bankniftyLastPrice);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "UpdateChangedValues : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        public void LoadLogs()
        {
            try
            {
                //read file for logs
                string[] Lines = null;

                if (File.Exists(m_DirectoryName + "//GUILogs.txt"))
                {
                    Lines = File.ReadAllLines(m_DirectoryName + "//GUILogs.txt");
                }
                if (Lines != null)
                {
                    foreach (var line in Lines)
                    {
                        if (line.Contains('|'))
                        {
                            string log = line.Split('|')[3];

                            if (!tabList.Items.Contains(log))
                            {
                                this.Dispatcher.Invoke(() =>
                                {
                                    tabList.Items.Add(log);
                                    if (!log.Contains("Connected"))
                                    {
                                        string[] time = log.Split(' ');
                                        DateTime t1 = Convert.ToDateTime(time[1]);
                                        DateTime t2 = Convert.ToDateTime(DateTime.Now.ToString("HH:mm:ss"));
                                        TimeSpan t = t2.Subtract(t1);
                                        double minutes = t.TotalMinutes;
                                        if (minutes < 1)
                                        {
                                            toastDisplay(log);
                                        }
                                    }
                                    else
                                    {
                                        string[] time = log.Split(' ');
                                        DateTime t1 = Convert.ToDateTime(time[1]);
                                        DateTime t2 = Convert.ToDateTime(DateTime.Now.ToString("HH:mm:ss"));
                                        TimeSpan t = t2.Subtract(t1);
                                        double minutes = t.TotalMinutes;
                                        if (minutes < 1)
                                        {
                                            toastDisplay(log);
                                        }
                                    }

                                });
                            }
                        }
                    }
                    //sanika::5-oct-2020::Added code for display toast after connect and disconnect bridge
                    if (m_BridgeConnection == "Connected" && isConnected == false)
                    {
                        //sanika::6-oct-2020::move dispatcher line
                        this.Dispatcher.Invoke(() =>
                        {
                            toastDisplay("Bridge Connected!!");
                        });
                        isConnected = true;
                        isDisconnected = false;
                    }
                    else if (m_BridgeConnection == "Disconnected" && isDisconnected == false)
                    {
                        //sanika::6-oct-2020::move dispatcher line
                        this.Dispatcher.Invoke(() =>
                        {
                            toastDisplay("Bridge Disconnected!!");
                        });
                        isConnected = false;
                        isDisconnected = true;
                    }

                }
            }
            catch (Exception e)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "LoadLogs : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        public void ChangeFlag(bool loginStatus)
        {
            try
            {
                if (loginStatus)
                {
                    this.Dispatcher.Invoke(() =>
                    {
                        lblValueStatus.Content = "Connected";
                        //sanika::28-Jul-2021::Did change for bridge connection
                        bool value = m_ObjkiteConnectWrapper.GetFromEXEValue();
                        if (value == true)
                            m_BridgeConnection = "Connected";
                        lblValueStatus.Background = Brushes.LimeGreen;
                        //IRDSPM::Pratiksha::21-12-2020::To update last login date
                        if (isupdatelastloginInDb == false)
                        {
                            string licenceID = GetHardwareID();
                            isupdatelastloginInDb = m_ObjkiteConnectWrapper.UpdateDateinDB(licenceID);
                        }
                        //IRDSPM::Pratiksha::24-04-2020::reflect all the changes
                        if (borderMW.Visibility == Visibility.Hidden && borderOB.Visibility == Visibility.Hidden && borderOP.Visibility == Visibility.Hidden && borderOH.Visibility == Visibility.Hidden)
                        {
                            MadeChangesOnGridandcheckboxes();
                        }
                        //IRDSPM::Pratiksha::12-10-2020::Change the column width
                        if (marketWatchGrid.Columns.Count > 0)
                        {
                            marketWatchGrid.Columns[0].MinWidth = 200;
                            m_SelectRowFirstTime = false;
                            marketWatchGrid.SelectedIndex = 0;
                            marketWatchGrid.Focusable = true;
                            //Keyboard.Focus(marketWatchGrid);
                        }
                    });

                }
                else if (loginStatus == false)
                {
                    this.Dispatcher.Invoke(() =>
                    {
                        lblValueStatus.Content = "Failure";
                        lblValueStatus.Background = Brushes.Red;
                    });
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "ChangeFlag : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        public void InsertSymbolsIntoList()
        {
            try
            {
                m_futName = m_ObjkiteConnectWrapper.FetchTableName();
                m_TickDataDownloader = m_ObjkiteConnectWrapper.GetTickDataDownloaderFlag();
                if (m_TickDataDownloader)
                {
                    foreach (var sym in SymbolList)
                    {
                        //IRDS::03-Jul-2020::Jyoti::added for Ajay Sir Changes
                        string exg = sym.Split('.')[1].Split(',')[0];
                        if (exg != Constants.EXCHANGE_NSE)
                        {
                            string expDate = sym.Split('.')[1].Split(',')[1];
                            m_futName = expDate.Substring(expDate.Length - 2) + expDate.Substring(expDate.Length - 5, 3).ToUpper() + "FUT";
                            // ListForSymbolName.Add(sym.Split('.')[0] + m_futName);
                        }
                        //if (!ListForMarketWtchGridViewDeletedSymbols.Contains(sym.Split('.')[0]))
                        {
                            if (!m_RealTimeSymbols.ContainsKey(sym.Split('.')[0] + m_futName) && (!ListForMarketWtchGridViewDeletedSymbols.Contains(sym.Split('.')[0] + m_futName)))
                            {
                                //sanika::15-oct-2020::Added condirion for option symbols
                                if (sym.Split('.')[1] != Constants.EXCHANGE_NSE && sym.Split('.')[1] != Constants.EXCHANGE_NFO_OPT)
                                {
                                    m_RealTimeSymbols.Add(sym.Split('.')[0] + m_futName, sym.Split('.')[1]);
                                }
                                else
                                {
                                    m_RealTimeSymbols.Add(sym.Split('.')[0], sym.Split('.')[1]);
                                }
                            }
                            else
                            {
                                //sanika::15-oct-2020::Added condirion for option symbols
                                if (sym.Split('.')[1] != Constants.EXCHANGE_NSE && sym.Split('.')[1] != Constants.EXCHANGE_NFO_OPT)
                                {
                                    if (!m_RealTimeSymbols.ContainsKey(sym.Split('.')[0] + m_futName) && (!ListForMarketWtchGridViewDeletedSymbols.Contains(sym.Split('.')[0] + m_futName)))
                                        m_RealTimeSymbols.Add(sym.Split('.')[0] + m_futName, sym.Split('.')[1]);
                                }
                                else
                                {
                                    if (!m_RealTimeSymbols.ContainsKey(sym.Split('.')[0]) && (!ListForMarketWtchGridViewDeletedSymbols.Contains(sym.Split('.')[0])))
                                        m_RealTimeSymbols.Add(sym.Split('.')[0], sym.Split('.')[1]);
                                }
                            }
                        }
                        if (exg != Constants.EXCHANGE_NSE)
                        {
                            string expDate = sym.Split('.')[1].Split(',')[1];
                            m_futName = expDate.Substring(expDate.Length - 2) + expDate.Substring(expDate.Length - 5, 3).ToUpper() + "FUT";
                            ListForSymbolName.Add(sym.Split('.')[0] + m_futName);
                        }
                        else
                        {
                            ListForSymbolName.Add(sym.Split('.')[0]);
                        }
                    }
                }
                else
                {
                    foreach (var sym in SymbolList)
                    {
                        //if (!ListForMarketWtchGridViewDeletedSymbols.Contains(sym.Split('.')[0]))
                        {
                            if (!m_RealTimeSymbols.ContainsKey(sym.Split('.')[0]) && (!ListForMarketWtchGridViewDeletedSymbols.Contains(sym.Split('.')[0])))
                            {
                                //sanika::15-oct-2020::Added condition for option symbols
                                if (sym.Split('.')[1] != Constants.EXCHANGE_NSE && sym.Split('.')[1] != Constants.EXCHANGE_NFO_OPT)
                                {
                                    if ((!m_RealTimeSymbols.ContainsKey(sym.Split('.')[0] + m_futName)) && (!m_RealTimeSymbols.ContainsKey(sym.Split('.')[0])) && (!ListForMarketWtchGridViewDeletedSymbols.Contains(sym.Split('.')[0] + m_futName)) && ((!ListForMarketWtchGridViewDeletedSymbols.Contains(sym.Split('.')[0]))))
                                    {
                                        //sanika::19-oct-2020::added condition to solve double concatination
                                        if (!sym.Contains(m_futName) && (!sym.Split('.')[0].EndsWith("FUT")))//sanika::3-dec-2020::Added condition if symbol already contains future name
                                        {
                                            if (sym.Split('.').Length > 2)
                                            {
                                                //IRDSPM::Pratiksha::03-12-2020::For symbols havinf more than 2 dots in it
                                                if (sym.Split('.')[2].Contains(Constants.EXCHANGE_CDS) && (!ListForMarketWtchGridViewDeletedSymbols.Contains((sym.Split('.')[0]) + "." + (sym.Split('.')[1]))))
                                                {
                                                    m_RealTimeSymbols.Add(sym.Split('.')[0] + "." + sym.Split('.')[1], sym.Split('.')[2]);
                                                }
                                            }
                                            else
                                            {
                                                if (sym.Split('.')[1].Contains(Constants.EXCHANGE_CDS))
                                                {
                                                    m_RealTimeSymbols.Add(sym.Split('.')[0], sym.Split('.')[1]);
                                                }
                                                else if (!sym.Split('.')[0].EndsWith("FUT"))//sanika::3-dec-2020::Added condition if symbol already contains future name
                                                {
                                                    m_RealTimeSymbols.Add(sym.Split('.')[0] + m_futName, sym.Split('.')[1]);
                                                }
                                            }
                                        }
                                        else
                                            m_RealTimeSymbols.Add(sym.Split('.')[0], sym.Split('.')[1]);

                                    }
                                }
                                else
                                {
                                    if (!m_RealTimeSymbols.ContainsKey(sym.Split('.')[0]) && (!ListForMarketWtchGridViewDeletedSymbols.Contains(sym.Split('.')[0])))
                                        m_RealTimeSymbols.Add(sym.Split('.')[0], sym.Split('.')[1]);
                                }
                            }
                            else
                            {
                                //sanika::15-oct-2020::Added condirion for option symbols
                                if (sym.Split('.')[1] != Constants.EXCHANGE_NSE && sym.Split('.')[1] != Constants.EXCHANGE_NFO_OPT)
                                {
                                    //IRDSPM::Pratiksha::15-12-2020::For Checking exchange matches or not
                                    if ((!m_RealTimeSymbols.ContainsKey(sym.Split('.')[0] + m_futName)) && (!m_RealTimeSymbols.ContainsKey(sym.Split('.')[0] + ", " + sym.Split('.')[1])) && (!ListForMarketWtchGridViewDeletedSymbols.Contains(sym.Split('.')[0] + m_futName)) && ((!ListForMarketWtchGridViewDeletedSymbols.Contains(sym.Split('.')[0]))))
                                    {
                                        //sanika::19-oct-2020::added condition to solve double concatination
                                        if (!sym.Contains(m_futName))
                                            m_RealTimeSymbols.Add(sym.Split('.')[0] + m_futName, sym.Split('.')[1]);
                                        else
                                        {
                                            //IRDSPM::PRatiksha::26-10-2020::Added condition for if symbol is present in deleted list then no need to add it
                                            if (!(ListForMarketWtchGridViewDeletedSymbols.Contains(sym.Split('.')[0])) && !(m_RealTimeSymbols.ContainsKey(sym.Split('.')[0])))
                                                m_RealTimeSymbols.Add(sym.Split('.')[0], sym.Split('.')[1]);
                                        }
                                    }
                                }
                                else
                                {
                                    if (!m_RealTimeSymbols.ContainsKey(sym.Split('.')[0]) && (!ListForMarketWtchGridViewDeletedSymbols.Contains(sym.Split('.')[0])))
                                        m_RealTimeSymbols.Add(sym.Split('.')[0], sym.Split('.')[1]);
                                }
                            }
                        }//sanika::3-dec-2020::Added condition if symbol already contains future name
                        if (sym.Split('.')[1] == Constants.EXCHANGE_NFO && (!sym.Contains(m_futName)) && (!sym.Split('.')[0].EndsWith("FUT")))
                        {
                            ListForSymbolName.Add(sym.Split('.')[0] + m_futName);
                        }
                        else
                        {//sanika::3-dec-2020::Added for cds options symbols
                            if (sym.Split('.').Length > 2)
                                ListForSymbolName.Add(sym.Split('.')[0] + "." + sym.Split('.')[1]);
                            else
                                ListForSymbolName.Add(sym.Split('.')[0]);
                        }
                    }
                }
                this.Dispatcher.Invoke(() =>
                {
                    //IRDSPM::PRatiksha::25-08-2020::Because need to hide status box
                    lblClientIDVal.Content = m_UserID;
                    lblClientID.Content = m_UserID;
                    //IRDSPM::Pratiksha::10-09-2020::Removed unwanted code
                });
            }
            catch (Exception e)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "InsertSymbolsIntoList : Exception Error Message = " + e.Message, MessageType.Exception);
            }

        }
        private void OnMouseEnterbtnCloseAll(object sender, RoutedEventArgs e)
        {
            btnClose.Foreground = Brushes.Black;
        }
        private void OnMouseLeavebtnCloseAll(object sender, RoutedEventArgs e)
        {
            btnClose.Foreground = Brushes.White;
        }

        private void OnMouseEnterbtnCancelAll(object sender, RoutedEventArgs e)
        {
            btnCancel.Foreground = Brushes.Black;
        }
        private void OnMouseLeavebtnCancelAll(object sender, RoutedEventArgs e)
        {
            btnCancel.Foreground = Brushes.White;
        }

        private void OnMouseEnterbtnsqrOfAllOrder(object sender, RoutedEventArgs e)
        {
            btnsqrOfAllOrder.Foreground = Brushes.Black;
        }
        private void OnMouseLeavebtnsqrOfAllOrder(object sender, RoutedEventArgs e)
        {
            btnsqrOfAllOrder.Foreground = Brushes.White;
        }
        private void showPopupforSqroff(object sender, RoutedEventArgs e)
        {
            try
            {
                string message = "Are you sure you want to close this order?";
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                DialogResult dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                if (dialog == DialogResult.Yes)
                {
                    DataRowView row = (DataRowView)marketOpenPriceGrid.SelectedItems[0];
                    string symbol = row["Symbol"].ToString();
                    string exchange = row["Exchange"].ToString();
                    string product = row["Product"].ToString();
                    m_ObjkiteConnectWrapper.CloseOrder(symbol, exchange, product);
                }
                if (dialog == DialogResult.No)
                {
                    //nothing to do
                }
            }
            catch (Exception er)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "showPopupforSqroff : Exception Error Message = " + er.Message, MessageType.Exception);
            }
        }

        private void showPopupforcancelGrid(object sender, RoutedEventArgs e)
        {
            try
            {
                string message = "Are you sure you want to cancel this order?";
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                DialogResult dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                if (dialog == DialogResult.Yes)
                {
                    if (marketOrderBookGrid.SelectedItems.Count > 0)
                    {
                        DataRowView row = (DataRowView)marketOrderBookGrid.SelectedItems[0];
                        string orderid = row["OrderId"].ToString();
                        string product = row["Product"].ToString();
                        string symbol = row["Symbol"].ToString();//Sanika::3-sep-2020::added for log purpose
                        m_ObjkiteConnectWrapper.CancelOrder(orderid, product, symbol);
                    }
                }
                if (dialog == DialogResult.No)
                {
                    //nothing to do
                }
            }
            catch (Exception er)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "showPopupforcancelGrid : Exception Error Message = " + er.Message, MessageType.Exception);
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //IRDSPM::Pratiksha::31-07-2020::If user not lofin in then not give access           
                bool m_LoginStatus = m_ObjkiteConnectWrapper.GetConnectionStatus();
                if (m_LoginStatus)
                {
                    //IRDSPM::Pratiksha::31-07-2020::Added for message change
                    string message = "Are you sure you want to cancel only pending order?";
                    MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                    DialogResult dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                    if (dialog == DialogResult.Yes)
                    {
                        MessageBoxButtons buttons1 = MessageBoxButtons.OK;
                        bool result = m_ObjkiteConnectWrapper.CancelAllPendingOrders();
                        if (result == true)
                        {
                            string message1 = "All orders cancelled successfully.";
                            System.Windows.Forms.MessageBox.Show(message1, title, buttons1, MessageBoxIcon.Information);
                        }
                        else
                        {
                            string message1 = "Failed to cancel all orders.";
                            System.Windows.Forms.MessageBox.Show(message1, title, buttons1, MessageBoxIcon.Information);
                        }
                    }
                }
                else
                {
                    string message = "User not logged in.";
                    MessageBoxButtons buttons = MessageBoxButtons.OK;
                    DialogResult dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                }
            }
            catch (Exception er)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "btnCancel_Click : Exception Error Message = " + er.Message, MessageType.Exception);
            }

        }
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //IRDSPM::Pratiksha::31-07-2020::If user not lofin in then not give access           
                bool m_LoginStatus = m_ObjkiteConnectWrapper.GetConnectionStatus();
                if (m_LoginStatus)
                {
                    //IRDSPM::Pratiksha::31-07-2020::Added for message change
                    string message = "Are you sure you want to close only open position?";
                    MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                    DialogResult dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                    if (dialog == DialogResult.Yes)
                    {
                        MessageBoxButtons buttons1 = MessageBoxButtons.OK;
                        bool result = m_ObjkiteConnectWrapper.CloseAllOpenOrders();

                        if (result == true)
                        {
                            string message1 = "All orders closed successfully.";
                            System.Windows.Forms.MessageBox.Show(message1, title, buttons1, MessageBoxIcon.Information);
                        }
                        else
                        {
                            string message1 = "Failed to closed all orders.";
                            System.Windows.Forms.MessageBox.Show(message1, title, buttons1, MessageBoxIcon.Information);
                        }
                    }
                }
                else
                {
                    string message = "User not logged in.";
                    MessageBoxButtons buttons = MessageBoxButtons.OK;
                    DialogResult dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                }
            }
            catch (Exception er)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "btnClose_Click : Exception Error Message = " + er.Message, MessageType.Exception);
            }
        }
        //IRDSPM::Pratiksha::28-07-2020::For Messagebox
        private void btnsqrOfAllOrder_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //IRDSPM::Pratiksha::31-07-2020::If user not lofin in then not give access           
                bool m_LoginStatus = m_ObjkiteConnectWrapper.GetConnectionStatus();
                if (m_LoginStatus)
                {
                    //sanika::21-sep-2020::Added condition if sqroff only if bridge is connected
                    //IRDSPM::Pratiksha::11-05-2021::Commented changes for 
                    //if (m_BridgeConnection == "Connected")
                    //{
                    string message = "Are you sure you want to sqroff all orders and stop thread?";
                    MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                    DialogResult dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                    if (dialog == DialogResult.Yes)
                    {
                        MessageBoxButtons buttons1 = MessageBoxButtons.OK;
                        bool result = m_ObjkiteConnectWrapper.CloseAllOpenOrders();
                        bool resultCancelOrder = m_ObjkiteConnectWrapper.CancelAllPendingOrders();
                        //sanika::14-oct-2020::checked sqroff flag while displaying messagebox
                        if (m_isSqrOff == false)
                        {
                            if (result == true || resultCancelOrder == true)
                            {
                                string message1 = "Sqroff all orders and stop thread successfully.";
                                m_isSqrOff = true;
                                m_ObjkiteConnectWrapper.SetSqroffFlag(m_isSqrOff);
                                System.Windows.Forms.MessageBox.Show(message1, title, buttons1, MessageBoxIcon.Information);
                            }
                            else
                            {
                                string message1 = "No open positions to close.";
                                System.Windows.Forms.MessageBox.Show(message1, title, buttons1, MessageBoxIcon.Information);
                            }
                        }
                    }
                    //}
                    //else
                    //{
                    //    string message = "Bridge is not connected";
                    //    MessageBoxButtons buttons = MessageBoxButtons.OK;
                    //    DialogResult dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                    //}
                }
                else
                {
                    string message = "User not logged in.";
                    MessageBoxButtons buttons = MessageBoxButtons.OK;
                    DialogResult dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                }
            }
            catch (Exception er)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "btnsqrOfAllOrder_Click : Exception Error Message = " + er.Message, MessageType.Exception);
            }
        }
        private void checkOrderAll_Click(object sender, RoutedEventArgs e)
        {
            m_AllPositions = true;
            m_OpenPositions = false;
            m_ClosePosition = false;
            chkBoxAllPosition.IsChecked = true;
            chkBoxAllOpenPosition.IsChecked = false;
            chkBoxAllClosePosition.IsChecked = false;
            ListForOpenPositionGridView.Clear();
            mydtOpenPosition.Clear();
        }
        private void checkOrderOpen_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                m_AllPositions = false;
                m_OpenPositions = true;
                m_ClosePosition = false;
                chkBoxAllPosition.IsChecked = false;
                chkBoxAllOpenPosition.IsChecked = true;
                chkBoxAllClosePosition.IsChecked = false;
                ListForOpenPositionGridView.Clear();
                mydtOpenPosition.Clear();
            }
            catch (Exception er)
            {

            }
        }
        private void checkOrderClose_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                m_AllPositions = false;
                m_OpenPositions = false;
                m_ClosePosition = true;
                chkBoxAllPosition.IsChecked = false;
                chkBoxAllOpenPosition.IsChecked = false;
                chkBoxAllClosePosition.IsChecked = true;
                ListForOpenPositionGridView.Clear();
                mydtOpenPosition.Clear();
            }
            catch (Exception ex)
            {

            }
        }
        private void checkStatusAll_Click(object sender, RoutedEventArgs e)
        {
            //System.Windows.MessageBox.Show("checkStatusAll_Click, it changed!");
            m_AllOrders = true;
            m_CompleteOrders = false;
            m_CancelOrders = false;
            m_PendingOrders = false;
            stsAll.IsChecked = true;
            stsComplete.IsChecked = false;
            stsPending.IsChecked = false;
            stsCancel.IsChecked = false;
            ListForOrderBookGridView.Clear();
            mydtOrderBook.Clear();
        }
        private void checkStatusComplete_Click(object sender, RoutedEventArgs e)
        {
            //System.Windows.MessageBox.Show("checkStatusComplete_Click, it changed!");
            m_AllOrders = false;
            m_CompleteOrders = true;
            m_CancelOrders = false;
            m_PendingOrders = false;
            stsAll.IsChecked = false;
            stsComplete.IsChecked = true;
            stsPending.IsChecked = false;
            stsCancel.IsChecked = false;
            ListForOrderBookGridView.Clear();
            mydtOrderBook.Clear();
        }
        private void checkStatusPending_Click(object sender, RoutedEventArgs e)
        {
            // System.Windows.MessageBox.Show("checkStatusPending_Click, it changed!");
            m_AllOrders = false;
            m_CompleteOrders = false;
            m_CancelOrders = false;
            m_PendingOrders = true;
            stsAll.IsChecked = false;
            stsComplete.IsChecked = false;
            stsPending.IsChecked = true;
            stsCancel.IsChecked = false;
            ListForOrderBookGridView.Clear();
            mydtOrderBook.Clear();
        }
        private void checkStatusCancel_Click(object sender, RoutedEventArgs e)
        {
            // System.Windows.MessageBox.Show("checkStatusCancel_Click, it changed!");
            m_AllOrders = false;
            m_CompleteOrders = false;
            m_CancelOrders = true;
            m_PendingOrders = false;
            stsAll.IsChecked = false;
            stsComplete.IsChecked = false;
            stsPending.IsChecked = false;
            stsCancel.IsChecked = true;
            ListForOrderBookGridView.Clear();
            mydtOrderBook.Clear();
        }
        //IRDSPM::PRatiksha::31-07-2020::Added to exit application
        private void btnexit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string message = "Do you really want to exit from application?";
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                DialogResult dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                if (dialog == DialogResult.Yes)
                {
                    StopThread();
                    Thread.Sleep(2000);
                    Environment.Exit(0);
                }
                if (dialog == DialogResult.No)
                {
                    //nothing
                }
            }
            catch (Exception er)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "btnexit_Click : Exception Error Message = " + er.Message, MessageType.Exception);
            }
        }

        //IRDS::Jyoti::08-09-2020::Samco Integration changes
        public void addOrdersIntoDataGridView(dynamic order, double openPrice)
        {
            try
            {
                //sanika::30-sep-2020::fetched price accoording to status
                decimal price = 0;
                if (order.Status == Constants.ORDER_STATUS_COMPLETE)
                {
                    price = order.AveragePrice;
                }
                else
                {
                    price = order.Price;
                }
                this.Dispatcher.Invoke(() =>
                {
                    //sanika::29-sep-2020::faced issue by jyoti - for pending order display avg price 0
                    mydtOrderBook.Rows.Add(new object[] { order.Exchange, order.Tradingsymbol, order.OrderId, order.OrderTimestamp.ToString(), order.TransactionType, order.OrderType, order.Product, Math.Round(price, 2).ToString(), Math.Round(order.TriggerPrice, 2).ToString(), order.Quantity.ToString(), order.Status, order.FilledQuantity.ToString(), openPrice, order.StatusMessage });
                    DataContext = this;
                });

                ListForOrderBookGridView.Add(order.OrderId);
            }
            catch (Exception e)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "addOrdersIntoDataGridView : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        //IRDS::Jyoti::08-09-2020::Samco Integration changes
        public void updateOrderIntoDataGridView(dynamic order, double openPrice)
        {
            bool isIdFound = false;
            try
            {
                foreach (DataRow dr in mydtOrderBook.Rows)
                {
                    if ((dr["OrderId"]).ToString() == order.OrderId)
                    {
                        //sanika::30-sep-2020::fetched price accoording to status
                        decimal price = 0;
                        if (order.Status == Constants.ORDER_STATUS_COMPLETE)
                        {
                            price = order.AveragePrice;
                        }
                        else
                        {
                            price = order.Price;
                        }

                        dr["EXG"] = order.Exchange;
                        dr["Time"] = order.OrderTimestamp.ToString();
                        dr["Symbol"] = order.Tradingsymbol;
                        dr["Type"] = order.TransactionType;
                        dr["Order Type"] = order.OrderType;
                        dr["Product"] = order.Product;
                        dr["QTY"] = order.Quantity.ToString();
                        //IRDSPM::Pratiksha::29 - 07 - 2020::Replace price order to price
                        //sanika::30-sep-2020::fetched price accoording to status
                        dr["Price"] = Math.Round(price, 2).ToString();
                        dr["Trigger Price"] = Math.Round(order.TriggerPrice, 2).ToString();
                        dr["Status"] = order.Status;
                        dr["Filled Qty"] = order.FilledQuantity.ToString();
                        dr["Filled Price"] = openPrice.ToString();
                        dr["Rejection Order Remark"] = order.StatusMessage;
                        isIdFound = true;
                        break;
                    }
                }
                if (ListForOrderBookGridView.Contains(order.OrderId) && isIdFound == false)
                {
                    addOrdersIntoDataGridView(order, openPrice);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "updateIntoDataGridView : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }
        //IRDSPM::Pratiksha::22-10-2020::If symbol not selected
        public void GetSymbolIfNoneSelected(string type)
        {
            DataRowView row = null;
            if (marketWatchGrid.SelectedItem != null)
            {
                row = (DataRowView)marketWatchGrid.SelectedItems[0];
                string symbol = row["Symbol"].ToString();
                typeofOrder = type + " " + symbol;
            }
            else
            {
                if (marketWatchGrid.Items.Count != 0)//sanika::2-Dec-2020::Added condition if no symbols present in market watch then exception occur and crashed
                {
                    row = (DataRowView)marketWatchGrid.Items[0];
                    string symbol = row["Symbol"].ToString();
                    typeofOrder = type + " " + symbol;
                }
            }
        }
        public void OpenBuyForm_Click(object sender, RoutedEventArgs e)
        {
            //IRDSPM::Pratksha::07-12-2020::For not show if user not logged in
            bool m_LoginStatus = m_ObjkiteConnectWrapper.GetConnectionStatus();
            if (m_LoginStatus)
            {
                //IRDSPM::Pratiksha::22-10-2020::If symbol not selected
                String type = "Buy";
                GetSymbolIfNoneSelected(type);

                // typeofOrder = "Buy";
                if (m_ObjBuyForm == null)
                {
                    m_ObjBuyForm = new BuyWindowsForm(m_ObjkiteConnectWrapper, title);
                }
                m_ObjBuyForm.orderType(typeofOrder);
                m_ObjBuyForm.ShowDialog();
            }
            else
            {
                string message = "User not logged in.";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                DialogResult dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
            }
        }
        public void OpensellForm_Click(object sender, RoutedEventArgs e)
        {
            //IRDSPM::Pratksha::07-12-2020::For not show if user not logged in
            bool m_LoginStatus = m_ObjkiteConnectWrapper.GetConnectionStatus();
            if (m_LoginStatus)
            {
                //IRDSPM::Pratiksha::22-10-2020::If symbol not selected
                String type = "Sell";
                GetSymbolIfNoneSelected(type);

                if (m_ObjBuyForm == null)
                {
                    m_ObjBuyForm = new BuyWindowsForm(m_ObjkiteConnectWrapper, title);
                }
                //IRDSPM::Pratiksha::14-09-2020::Added type of order parameter
                m_ObjBuyForm.orderType(typeofOrder);
                m_ObjBuyForm.ShowDialog();
            }
            else
            {
                string message = "User not logged in.";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                DialogResult dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
            }
        }

        public void OpenSymbolForm_Click(object sender, RoutedEventArgs e)
        {
            SymbolSettingWindowForm obj = new SymbolSettingWindowForm(title);
            obj.ShowDialog();
        }

        //20-July-2021: sandip:optimization of objects,namepip commneted.
        //Sanika::27-Aug-2020::Added method to read logs from named pipe
        //private static void ReceiveResponse()
        //{
        //    //Sanika::2-oct-2020::add seperate class for pipe
        //    try
        //    {
        //        PipeConnection pipeConnection = null;
        //        while (!m_StopBridgeThread)
        //        {
        //            //Trace.WriteLine("Going to create server obj");
        //            if (pipeConnection == null)
        //            {
        //                pipeConnection = new PipeConnection();
        //                pipeConnection.serverStartMethod();
        //            }
        //            //sanika::8-oct-2020::Added sleep to reduced cpu usage 
        //            Thread.Sleep(1000);
        //        }
        //    }
        //    catch (Exception e)
        //    {

        //    }
        //}

        private static void ReceiveResponse_Log()
        {
            try
            {
                Trace.WriteLine("ReceiveResponse started");
                string errorMessage = "";
                while (true && m_StopBridgeThread == false)
                {
                    try
                    {
                        if (m_NamedPipeServer == null)
                        {
                            Trace.WriteLine("ReceiveResponse : created new obj");
                            m_NamedPipeServer = new NamedPipeServerStream("AlgoTraderBridgePipe123456", PipeDirection.InOut, 254, PipeTransmissionMode.Message);
                        }
                        Trace.WriteLine("ReceiveResponse : WaitForConnection");
                        m_NamedPipeServer.WaitForConnection();
                        StreamReader reader = new StreamReader(m_NamedPipeServer);
                        while (true && m_StopBridgeThread == false)
                        {
                            string line = "";
                            line = reader.ReadLine();
                            if (line != null)
                            {
                                m_BridgeConnection = line;
                                Trace.WriteLine("#####ReceiveResponse :  m_BridgeConnection = " + m_BridgeConnection);
                            }
                            else
                            {
                                m_BridgeConnection = "Disconnected";
                                Trace.WriteLine("#####ReceiveResponse :  m_BridgeConnection = " + m_BridgeConnection);
                                m_NamedPipeServer.WaitForPipeDrain();
                                m_NamedPipeServer.Disconnect();
                                m_NamedPipeServer.Close();
                                m_NamedPipeServer.Dispose();
                                m_NamedPipeServer = null;
                                break;
                            }

                        }
                    }
                    catch (Exception e)
                    {
                        errorMessage = e.Message;
                        Trace.WriteLine("ReceiveResponse : exception = " + e.Message);
                        //File.AppendAllText("Pipe.txt", "exception = "+e.Message);
                    }
                    finally
                    {
                        if (m_NamedPipeServer != null)
                        {
                            if (m_NamedPipeServer.IsConnected)
                            {
                                m_NamedPipeServer.WaitForPipeDrain();
                                m_NamedPipeServer.Disconnect();
                            }
                            m_NamedPipeServer.Close();
                            m_NamedPipeServer.Dispose();
                            m_NamedPipeServer = null;
                        }
                    }
                    Thread.Sleep(1000);
                }

                if (m_NamedPipeServer != null)
                {
                    if (m_NamedPipeServer.IsConnected)
                    {
                        m_NamedPipeServer.WaitForPipeDrain();
                        m_NamedPipeServer.Disconnect();
                    }
                    m_NamedPipeServer.Close();
                    m_NamedPipeServer.Dispose();
                    m_NamedPipeServer = null;
                }
            }
            catch (Exception e)
            {
                Trace.WriteLine("ReceiveResponse : exception123 = " + e.Message);
            }

            // WriteUniquelogs("ExecutionForm" + " " + m_UserID, "ReceiveResponse : Stopped thread m_StopBridgeThread " + m_StopBridgeThread, MessageType.Informational);
        }

        //IRDSPM::PRatiksha::02-09-2020::to display toast notification
        public void toastDisplay(string message)
        {
            try
            {
                ToastNotifications tn = new ToastNotifications(title);
                //IRDSPM::PRatiksha::02-09-2020::Toast Notification --start
                if (File.Exists(iniFileSetting))
                {
                    if (Settings == null)
                    {
                        Settings = new ConfigSettings(logger);
                    }
                    Settings.ReadSettingConfigFile(iniFileSetting);
                    toastNotificationVal = Settings.toastNotification;
                    toastAudioVal = Settings.toastAudio;
                }
                //IRDSPM::PRatiksha::02-09-2020::Toast Notification -end

                if (toastNotificationVal == true)
                {
                    if (toastAudioVal == true)
                    {
                        //IRDSPM::Pratiksha::21-10-2020::Change the audio file path
                        if (File.Exists(path + AudioFilePath))
                        {
                            simpleSound = new SoundPlayer(path + AudioFilePath);
                            simpleSound.Play();
                        }
                        else
                        {
                            WriteUniquelogs("ExecutionForm" + " " + m_UserID, "ToastNotification - Audio file Not Exist ", MessageType.Informational);
                        }
                    }
                    string toastnotificationMessage = breakLines(message, 30);

                    //IRDSPM::Pratiksha::08-09-2020::to open 5 forms at a time
                    if (System.Windows.Forms.Application.OpenForms.Count <= 7)
                    {
                        tn.showAlert(toastnotificationMessage);
                    }
                    // this.tabControl.SelectedIndex = 5;
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "ToastNotification : Exception Error Message " + ex.Message, MessageType.Exception);
            }
        }
        public static String breakLines(String input, int maxLineLength)
        {
            string[] tokens = input.Split(' ');
            StringBuilder output = new StringBuilder(input.Length);
            try
            {
                int lineLen = 0;
                for (int i = 0; i < tokens.Length; i++)
                {
                    string word = tokens[i];

                    if (lineLen + (SPACE_SEPARATOR + word).Length > maxLineLength)
                    {
                        if (i > 0)
                        {
                            output.Append(NEWLINE);
                        }
                        lineLen = 0;
                    }
                    if (i < tokens.Length - 1 && (lineLen + (word + SPACE_SEPARATOR).Length + tokens[i + 1].Length <= maxLineLength))
                    {
                        word += SPACE_SEPARATOR;
                    }
                    output.Append(word);
                    lineLen += word.Length;
                }
            }
            catch (Exception ex)
            {
                //WriteUniquelogs("ExecutionForm" + " " + m_UserID, "breakLines : Exception Error Message " + ex.Message, MessageType.Exception);
            }
            return output.ToString();
        }

        private void DataGridRow_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            string symbol = "";
            if (tabControl.SelectedValue == tabMarketWatch)
            {
                //IRDSPM::Pratiksha::09-10-2020::Added change if row not selected then on right click that perticular symbol name shoul open
                DependencyObject dep = (DependencyObject)e.OriginalSource;
                while ((dep != null) && !(dep is System.Windows.Controls.DataGridCell))
                {
                    dep = VisualTreeHelper.GetParent(dep);
                }
                if (dep == null) return;

                if (dep is System.Windows.Controls.DataGridCell)
                {
                    System.Windows.Controls.DataGridCell cell = dep as System.Windows.Controls.DataGridCell;
                    cell.Focus();

                    while ((dep != null) && !(dep is DataGridRow))
                    {
                        dep = VisualTreeHelper.GetParent(dep);
                    }
                    DataGridRow row = dep as DataGridRow;
                    marketWatchGrid.SelectedItem = row.DataContext;
                    DataRowView row1 = (DataRowView)marketWatchGrid.SelectedItems[0];
                    symbol = row1["Symbol"].ToString();
                    m_CurrentSymbolForMWRow = symbol;
                }
            }
            else if (tabControl.SelectedValue == tabOrderBook)
            {
                DependencyObject dep = (DependencyObject)e.OriginalSource;
                while ((dep != null) && !(dep is System.Windows.Controls.DataGridCell))
                {
                    dep = VisualTreeHelper.GetParent(dep);
                }
                if (dep == null) return;

                if (dep is System.Windows.Controls.DataGridCell)
                {
                    System.Windows.Controls.DataGridCell cell = dep as System.Windows.Controls.DataGridCell;
                    cell.Focus();

                    while ((dep != null) && !(dep is DataGridRow))
                    {
                        dep = VisualTreeHelper.GetParent(dep);
                    }
                    DataGridRow row = dep as DataGridRow;
                    marketOrderBookGrid.SelectedItem = row.DataContext;
                    DataRowView row1 = (DataRowView)marketOrderBookGrid.SelectedItems[0];
                    string status = row1["Status"].ToString();
                    if (status == Constants.ORDER_STATUS_PENDING || status == Constants.ORDER_STATUS_OPEN)
                    {
                        string TransactionType = "";
                        string TriggerPrice = "";
                        string OrderId = "";
                        string Quantity = "";
                        string Exchange = Constants.EXCHANGE_NSE;
                        string Product = Constants.PRODUCT_MIS;
                        symbol = row1["Symbol"].ToString();
                        TransactionType = row1["Type"].ToString();
                        TriggerPrice = row1["Trigger Price"].ToString();
                        OrderId = row1["OrderId"].ToString();
                        Quantity = row1["QTY"].ToString();
                        Exchange = row1["EXG"].ToString();
                        Product = row1["Product"].ToString();
                        m_CurrentSymbolForModifyOrder = symbol + "," + TransactionType + "," + TriggerPrice + "," + OrderId + "," + Quantity + "," + Exchange + "," + Product;
                    }
                    else
                    {
                        symbol = "";
                    }
                }
            }
            if (symbol != "")
            {
                OpenOrderMenuStrip();
            }
        }

        //IRDSPM::Pratiksha::09-10-2020::Contextstripmenu
        private void OpenOrderMenuStrip()
        {
            try
            {
                ContextMenuStrip orderMenuStrip = new ContextMenuStrip();
                if (tabControl.SelectedValue == tabMarketWatch)
                {
                    orderMenuStrip.Items.Add("Place buy order").Name = "buy";
                    orderMenuStrip.Items.Add("Place sell order").Name = "sell";
                    //IRDSPM::Pratiksha::07-12-2020::changed the option name to delete
                    orderMenuStrip.Items.Add("Delete symbol").Name = "delete";
                }
                else if (tabControl.SelectedValue == tabOrderBook)
                {
                    orderMenuStrip.Items.Add("Modify Order").Name = "ModifyOrder";
                }

                var point = System.Windows.Forms.Control.MousePosition;
                int x = Convert.ToInt32(point.X);
                int y = Convert.ToInt32(point.Y);
                orderMenuStrip.Show(new System.Drawing.Point(x, y));
                orderMenuStrip.ItemClicked += OrderMenuStrip_ItemClicked;
            }
            catch (Exception ex)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "OpenOrderMenuStrip : Exception Error Message " + ex.Message, MessageType.Exception);
            }
        }

        //IRDSPM::Pratiksha::09-10-2020::To check for user selected which buy/sell/delete
        private void OrderMenuStrip_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                string selectedItem = e.ClickedItem.Name.ToString();
                if (selectedItem == "buy")
                {
                    PlaceBuyOrder();
                }
                else if (selectedItem == "sell")
                {
                    PlaceSellOrder();
                }
                else if (selectedItem == "delete")
                {
                    DeleteSymbol();
                }
                else if (selectedItem == "ModifyOrder")
                {
                    //ModifyOrder();
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "OrderMenuStrip_ItemClicked : Exception Error Message " + ex.Message, MessageType.Exception);
            }
        }

        private void ModifyOrder()
        {
            try
            {
                typeofOrder = "Modify " + m_CurrentSymbolForModifyOrder;
                if (m_ObjBuyForm == null)
                {
                    m_ObjBuyForm = new BuyWindowsForm(m_ObjkiteConnectWrapper, title);
                }
                m_ObjBuyForm.orderType(typeofOrder);
                m_ObjBuyForm.ShowDialog();
            }
            catch (Exception ex)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "PlaceBuyOrder : Exception Error Message " + ex.Message, MessageType.Exception);
            }
        }

        //IRDSPM::Pratiksha::09-10-2020::Placing buy order on symbol click
        private void PlaceBuyOrder()
        {
            try
            {
                typeofOrder = "Buy" + " " + m_CurrentSymbolForMWRow;
                if (m_ObjBuyForm == null)
                {
                    m_ObjBuyForm = new BuyWindowsForm(m_ObjkiteConnectWrapper, title);
                }
                m_ObjBuyForm.orderType(typeofOrder);
                m_ObjBuyForm.ShowDialog();
            }
            catch (Exception ex)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "PlaceBuyOrder : Exception Error Message " + ex.Message, MessageType.Exception);
            }
        }
        //IRDSPM::Pratiksha::09-10-2020::Placing sell order on symbol click
        private void PlaceSellOrder()
        {
            try
            {
                typeofOrder = "Sell" + " " + m_CurrentSymbolForMWRow;
                if (m_ObjBuyForm == null)
                {
                    m_ObjBuyForm = new BuyWindowsForm(m_ObjkiteConnectWrapper, title);
                }
                m_ObjBuyForm.orderType(typeofOrder);
                m_ObjBuyForm.ShowDialog();
            }
            catch (Exception ex)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "PlaceSellOrder : Exception Error Message " + ex.Message, MessageType.Exception);
            }
        }
        //IRDSPM::Pratiksha::09-10-2020::delete selected symbol
        private void DeleteSymbol()
        {
            try
            {
                //IRDSPM::Pratiksha::22-10-2020::For setting focus to previous
                int item = marketWatchGrid.SelectedIndex - 1;
                //IRDSPM::PRatiksha::26-10-2020::To solve out of bound array exception
                if (item == -1)
                {
                    item = 0;
                }
                string message = "Are you sure you want to delete " + m_CurrentSymbolForMWRow + " symbol?";
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                Deletedialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);

                if (Deletedialog == DialogResult.Yes)
                {
                    if (ListForMarketWtchGridView.Count > 0)
                    {
                        if (ListForMarketWtchGridView.Contains(m_CurrentSymbolForMWRow))
                        {
                            string sym = m_CurrentSymbolForMWRow;
                            RemoveIntoMarketWathDataGridView(sym);
                            ListForMarketWtchGridView.Remove(sym);
                            //if (sym.Contains(m_futName))
                            //{
                            //    int index = sym.IndexOf(m_futName);
                            //    Console.WriteLine(sym.Substring(0, index));
                            //    sym = sym.Substring(0, index);
                            //}
                            if (m_RealTimeSymbols.ContainsKey(sym))
                            {
                                m_RealTimeSymbols.Remove(sym);
                            }
                            ListForMarketWtchGridViewDeletedSymbols.Add(sym);
                        }

                        if (File.Exists(inisymbolList))
                        {
                            List<string> Finallist = new List<string>();
                            List<string> SymbolListFromIni = new List<string>();
                            Settings = new ConfigSettings(logger);
                            Settings.ReadSymbolList();
                            INIFileForSetting iniObj = new INIFileForSetting();
                            SymbolListFromIni = Settings.Symbol;
                            for (int i = 0; i < SymbolListFromIni.Count; i++)
                            {
                                string[] symParts = SymbolListFromIni[i].Split('.');

                                string symbolName = symParts[0];

                                if (symParts[1] != "NSE" && symParts[1] != Constants.EXCHANGE_NFO_OPT)
                                {
                                    symbolName += m_futName;
                                }
                                if (m_RealTimeSymbols.ContainsKey(symbolName))
                                {
                                    Finallist.Add(SymbolListFromIni[i]);
                                }
                            }
                            //for (int i = 0; i < SymbolListFromIni.Count; i++)
                            //{
                            iniObj.clearTestingSymbol("RealTimeSymbols", null, null, inisymbolList);
                            //}
                            for (int m = 0; m < Finallist.Count; m++)
                            {
                                Settings.WriteSymbolList("RealTimeSymbols", m.ToString(), Finallist[m]);
                            }
                        }
                    }
                    //IRDSPM::Pratiksha::22-10-2020::For setting focus to previous
                    if (marketWatchGrid.Items.Count != 0)//sanika::2-Dec-2020::Added condition if all symbols remove then exception occur and crashed
                    {
                        var row = (DataGridRow)marketWatchGrid.ItemContainerGenerator.ContainerFromIndex(item);
                        row.MoveFocus(new TraversalRequest(FocusNavigationDirection.Previous));
                    }
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "DeleteSymbol : Exception Error Message " + ex.Message, MessageType.Exception);
            }
        }

        //IRDSPM::Pratiksha::09-10-2020::Rmove row from market watch
        public void RemoveIntoMarketWathDataGridView(string symbolName)
        {
            try
            {
                for (int i = mydtMarketWatch.Rows.Count - 1; i >= 0; i--)
                {
                    DataRow dr = mydtMarketWatch.Rows[i];
                    if ((dr["Symbol"]).ToString() == symbolName)
                    {
                        this.Dispatcher.Invoke(() =>
                        {
                            mydtMarketWatch.Rows.Remove(dr);
                            DataViewMarketWatch = mydtMarketWatch.DefaultView;
                            DataContext = this;
                        });
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "RemoveIntoDataGridView : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        private void OpenContextmenuStrip()
        {
            var point = System.Windows.Forms.Control.MousePosition;
            int x = Convert.ToInt32(point.X);
            int y = Convert.ToInt32(point.Y);
            mymenu.Show(new System.Drawing.Point(x, y));
            mymenu.ItemClicked += Mymenu_ItemClicked;
        }
        private void Mymenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            string selectedItem = e.ClickedItem.Name.ToString();
            if (selectedItem == "Asc")
            {
                SelectionAscCase();
            }
            else if (selectedItem == "Des")
            {
                SelectionDesCase();
            }
            else if (selectedItem == "setup")
            {
                ColumnSelectionCase();
            }
        }

        private void ColumnSelectionCase()
        {
            if (tabControl.SelectedIndex == 0)
            {
                borderMW.Visibility = Visibility.Visible;
                bordersymbolname.Visibility = Visibility.Visible;
                rememberMeCheckboxMW.Visibility = Visibility.Visible;
                symbolNameMWGrid.Visibility = Visibility.Visible;
                lblColumnSetup.Visibility = Visibility.Visible;
                gridButtonsMW.Visibility = Visibility.Visible;
                MW1.IsChecked = true;
                MW1.IsEnabled = false;
                MadeChangesOnGridandcheckboxes();
            }
            else if (tabControl.SelectedIndex == 1)
            {
                borderOP.Visibility = Visibility.Visible;
                bordersymbolnameOP.Visibility = Visibility.Visible;
                rememberMeCheckboxOP.Visibility = Visibility.Visible;
                symbolNameOPGrid.Visibility = Visibility.Visible;
                bordersymbolnameOP.Visibility = Visibility.Visible;
                rememberMeCheckboxOP.Visibility = Visibility.Visible;
                symbolNameOPGrid.Visibility = Visibility.Visible;
                lblColumnSetupOP.Visibility = Visibility.Visible;
                gridButtonsOP.Visibility = Visibility.Visible;
                OP2.IsEnabled = false;
                OP2.IsChecked = true;
            }
            else if (tabControl.SelectedIndex == 2)
            {
                borderOB.Visibility = Visibility.Visible;
                rememberMeCheckboxOB.Visibility = Visibility.Visible;
                symbolNameOBGrid.Visibility = Visibility.Visible;
                bordersymbolnameOB.Visibility = Visibility.Visible;
                lblColumnSetupOB.Visibility = Visibility.Visible;
                gridButtonsOB.Visibility = Visibility.Visible;
                OB2.IsChecked = true;
                OB2.IsEnabled = false;
            }
            else if (tabControl.SelectedIndex == 3)
            {
                borderOH.Visibility = Visibility.Visible;
                rememberMeCheckboxOH.Visibility = Visibility.Visible;
                symbolNameOHGrid.Visibility = Visibility.Visible;
                bordersymbolnameOH.Visibility = Visibility.Visible;
                lblcolumnSetupOH.Visibility = Visibility.Visible;
                gridButtonsOH.Visibility = Visibility.Visible;
                OH2.IsChecked = true;
                OH2.IsEnabled = false;
            }
        }

        //IRDSPM::Pratiksha::14-10-2020::For solving color change issue on table, need to sort DataTable
        public DataTable resort(DataTable dt, string colName, string direction)
        {
            dt.DefaultView.Sort = colName + " " + direction;
            dt = dt.DefaultView.ToTable();
            return dt;
        }

        public void SelectionAscCase()
        {
            try
            {
                if (tabControl.SelectedIndex == 0 || tabControl.SelectedIndex == -1)
                {
                    for (int i = 0; i < MWColumnnameArray.Length; i++)
                    {
                        string currentstr = MWColumnnameArray[i].ToString();
                        if (m_ColumnSelectionForAscOrder.Contains(currentstr))
                        {
                            marketWatchGrid.Items.SortDescriptions.Clear();
                            marketWatchGrid.Items.SortDescriptions.Add(new SortDescription(currentstr, ListSortDirection.Ascending));
                            mydtMarketWatch = resort(mydtMarketWatch, currentstr, "ASC");
                            break;
                        }
                    }
                }
                else if (tabControl.SelectedIndex == 1)
                {
                    for (int i = 0; i < OPColumnnameArray.Length; i++)
                    {
                        string currentstr = OPColumnnameArray[i].ToString();
                        if (m_ColumnSelectionForAscOrder.Contains(currentstr))
                        {
                            marketOpenPriceGrid.Items.SortDescriptions.Clear();
                            marketOpenPriceGrid.Items.SortDescriptions.Add(new SortDescription(currentstr, ListSortDirection.Ascending));
                            mydtOpenPosition = resort(mydtOpenPosition, currentstr, "ASC");
                            break;
                        }

                    }
                }
                else if (tabControl.SelectedIndex == 2)
                {
                    // IRDSPM::Pratiksha::26 - 10 - 2020::Not worked for some column names properly
                    string[] test = m_ColumnSelectionForAscOrder.Split(':');
                    var indexOfFirstSpace = test[1].IndexOf(" ");
                    var first = test[1].Substring(0, indexOfFirstSpace);
                    var compararedString = test[1].Substring(indexOfFirstSpace + 1);
                    for (int i = 0; i < OBColumnnameArray.Length; i++)
                    {
                        string currentstr = OBColumnnameArray[i].ToString();
                        if (compararedString.Length == currentstr.Length && (m_ColumnSelectionForAscOrder.Contains(currentstr)))
                        {
                            marketOrderBookGrid.Items.SortDescriptions.Clear();
                            marketOrderBookGrid.Items.SortDescriptions.Add(new SortDescription(currentstr, ListSortDirection.Ascending));
                            break;
                        }
                    }
                }
                else if (tabControl.SelectedIndex == 3)
                {
                    for (int i = 0; i < OHColumnnameArray.Length; i++)
                    {
                        string currentstr = OHColumnnameArray[i].ToString();
                        if (m_ColumnSelectionForAscOrder.Contains(currentstr))
                        {
                            marketOpenHoldingsGrid.Items.SortDescriptions.Clear();
                            marketOpenHoldingsGrid.Items.SortDescriptions.Add(new SortDescription(currentstr, ListSortDirection.Ascending));
                            break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "SelectionAscCase : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        public void SelectionDesCase()
        {
            try
            {
                if (tabControl.SelectedIndex == 0 || tabControl.SelectedIndex == -1)
                {

                    for (int i = 0; i < MWColumnnameArray.Length; i++)
                    {
                        string currentstr = MWColumnnameArray[i].ToString();
                        if (m_ColumnSelectionForAscOrder.Contains(currentstr))
                        {
                            marketWatchGrid.Items.SortDescriptions.Clear();
                            marketWatchGrid.Items.SortDescriptions.Add(new SortDescription(currentstr, ListSortDirection.Descending));
                            mydtMarketWatch = resort(mydtMarketWatch, currentstr, "DESC");
                            break;
                        }
                    }
                }
                else if (tabControl.SelectedIndex == 1)
                {
                    for (int i = 0; i < OPColumnnameArray.Length; i++)
                    {
                        string currentstr = OPColumnnameArray[i].ToString();
                        if (m_ColumnSelectionForAscOrder.Contains(currentstr))
                        {
                            marketOpenPriceGrid.Items.SortDescriptions.Clear();
                            marketOpenPriceGrid.Items.SortDescriptions.Add(new SortDescription(currentstr, ListSortDirection.Descending));
                            mydtOpenPosition = resort(mydtOpenPosition, currentstr, "DESC");
                            break;
                        }
                    }
                }
                else if (tabControl.SelectedIndex == 2)
                {
                    //IRDSPM::Pratiksha::26-10-2020::Not worked for some column names properly
                    string[] test = m_ColumnSelectionForAscOrder.Split(':');
                    var indexOfFirstSpace = test[1].IndexOf(" ");
                    var first = test[1].Substring(0, indexOfFirstSpace);
                    var compararedString = test[1].Substring(indexOfFirstSpace + 1);
                    for (int i = 0; i < OBColumnnameArray.Length; i++)
                    {
                        string currentstr = OBColumnnameArray[i].ToString();
                        if (compararedString.Length == currentstr.Length && (m_ColumnSelectionForAscOrder.Contains(currentstr)))
                        {
                            marketOrderBookGrid.Items.SortDescriptions.Clear();
                            marketOrderBookGrid.Items.SortDescriptions.Add(new SortDescription(currentstr, ListSortDirection.Descending));
                            break;
                        }
                    }
                }
                else if (tabControl.SelectedIndex == 3)
                {
                    for (int i = 0; i < OHColumnnameArray.Length; i++)
                    {
                        string currentstr = OHColumnnameArray[i].ToString();
                        if (m_ColumnSelectionForAscOrder.Contains(currentstr))
                        {
                            marketOpenHoldingsGrid.Items.SortDescriptions.Clear();
                            marketOpenHoldingsGrid.Items.SortDescriptions.Add(new SortDescription(currentstr, ListSortDirection.Descending));
                            break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "SelectionDesCase : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        //IRDSPM::PRatiksha::14-09-2020::Selection should reflect on button click
        private void btnOkMW_Click(object sender, RoutedEventArgs e)
        {
            if (tabControl.SelectedIndex == 0 || tabControl.SelectedIndex == -1)
            {
                MarketWatchCheckedandSave();
            }
            else if (tabControl.SelectedIndex == 1)
            {
                OpenPositionCheckedandSave();
            }
            else if (tabControl.SelectedIndex == 2)
            {
                OrderBookCheckedandSave();
            }
            else if (tabControl.SelectedIndex == 3)
            {
                OpenHoldingCheckedandSave();
            }
            MadeChangesOnGridandcheckboxes();
            RefreshSettinginiFile();
        }

        //IRDSPM::PRatiksha::14-09-2020::listbox close event on cancel button click
        private void btnCancelMW_Click(object sender, RoutedEventArgs e)
        {
            if (tabControl.SelectedIndex == 0)
            {
                borderMW.Visibility = Visibility.Hidden;
                bordersymbolname.Visibility = Visibility.Hidden;
                rememberMeCheckboxMW.Visibility = Visibility.Hidden;
                symbolNameMWGrid.Visibility = Visibility.Hidden;
                gridButtonsMW.Visibility = Visibility.Hidden;
                lblColumnSetup.Visibility = Visibility.Hidden;
            }
            else if (tabControl.SelectedIndex == 1)
            {
                borderOP.Visibility = Visibility.Hidden;
                bordersymbolnameOP.Visibility = Visibility.Hidden;
                rememberMeCheckboxOP.Visibility = Visibility.Hidden;
                symbolNameOPGrid.Visibility = Visibility.Hidden;
                gridButtonsOP.Visibility = Visibility.Hidden;
                lblColumnSetupOP.Visibility = Visibility.Hidden;
            }
            else if (tabControl.SelectedIndex == 2)
            {
                borderOB.Visibility = Visibility.Hidden;
                rememberMeCheckboxOB.Visibility = Visibility.Hidden;
                symbolNameOBGrid.Visibility = Visibility.Hidden;
                bordersymbolnameOB.Visibility = Visibility.Hidden;
                gridButtonsOB.Visibility = Visibility.Hidden;
                lblColumnSetupOB.Visibility = Visibility.Hidden;
            }
            else if (tabControl.SelectedIndex == 3)
            {
                borderOH.Visibility = Visibility.Hidden;
                rememberMeCheckboxOH.Visibility = Visibility.Hidden;
                symbolNameOHGrid.Visibility = Visibility.Hidden;
                bordersymbolnameOH.Visibility = Visibility.Hidden;
                gridButtonsOH.Visibility = Visibility.Hidden;
                lblcolumnSetupOH.Visibility = Visibility.Hidden;
            }
        }

        //IRDSPM::PRatiksha::14-09-2020::checkbox list should clear on reset click
        private void btnResetMW_Click(object sender, RoutedEventArgs e)
        {
            if (tabControl.SelectedIndex == 0)
            {
                MW1.IsChecked = true;
                MW1.IsEnabled = false;
                MW2.IsChecked = false;
                MW3.IsChecked = false;
                MW4.IsChecked = false;
                MW5.IsChecked = false;
                MW6.IsChecked = false;
                MW7.IsChecked = false;
                MW8.IsChecked = false;
                MW9.IsChecked = false;
                MW10.IsChecked = false;
                MW11.IsChecked = false;
            }
            else if (tabControl.SelectedIndex == 1)
            {
                OP1.IsChecked = false;
                OP2.IsEnabled = false;
                OP2.IsChecked = true;
                OP3.IsChecked = false;
                OP4.IsChecked = false;
                OP5.IsChecked = false;
                OP6.IsChecked = false;
                OP7.IsChecked = false;
            }
            else if (tabControl.SelectedIndex == 2)
            {
                OB1.IsChecked = false;
                OB2.IsEnabled = false;
                OB2.IsChecked = true;
                OB3.IsChecked = false;
                OB4.IsChecked = false;
                OB5.IsChecked = false;
                OB6.IsChecked = false;
                OB7.IsChecked = false;
                OB8.IsChecked = false;
                OB9.IsChecked = false;
                OB10.IsChecked = false;
                OB11.IsChecked = false;
                OB12.IsChecked = false;
                OB13.IsChecked = false;
                OB14.IsChecked = false;
            }
            else if (tabControl.SelectedIndex == 3)
            {
                OH1.IsChecked = false;
                OH2.IsEnabled = false;
                OH2.IsChecked = true;
                OH3.IsChecked = false;
                OH4.IsChecked = false;
                OH5.IsChecked = false;
                OH6.IsChecked = false;
                OH7.IsChecked = false;
            }
        }

        //IRDSPM::PRatiksha::14-09-2020::on mark all checkboxes shuld checked
        private void btnMarkall_Click(object sender, RoutedEventArgs e)
        {
            if (tabControl.SelectedIndex == 0)
            {
                MW1.IsChecked = true;
                MW1.IsEnabled = false;
                MW2.IsChecked = true;
                MW3.IsChecked = true;
                MW4.IsChecked = true;
                MW5.IsChecked = true;
                MW6.IsChecked = true;
                MW7.IsChecked = true;
                MW8.IsChecked = true;
                MW9.IsChecked = true;
                MW10.IsChecked = true;
                MW11.IsChecked = true;
            }
            else if (tabControl.SelectedIndex == 1)
            {
                OP1.IsChecked = true;
                OP2.IsEnabled = false;
                OP2.IsChecked = true;
                OP3.IsChecked = true;
                OP4.IsChecked = true;
                OP5.IsChecked = true;
                OP6.IsChecked = true;
                OP7.IsChecked = true;
            }
            else if (tabControl.SelectedIndex == 2)
            {
                OB1.IsChecked = true;
                OB2.IsEnabled = false;
                OB2.IsChecked = true;
                OB3.IsChecked = true;
                OB4.IsChecked = true;
                OB5.IsChecked = true;
                OB6.IsChecked = true;
                OB7.IsChecked = true;
                OB8.IsChecked = true;
                OB9.IsChecked = true;
                OB10.IsChecked = true;
                OB11.IsChecked = true;
                OB12.IsChecked = true;
                OB13.IsChecked = true;
                OB14.IsChecked = true;
            }
            else if (tabControl.SelectedIndex == 3)
            {
                OH1.IsChecked = true;
                OH2.IsEnabled = false;
                OH2.IsChecked = true;
                OH3.IsChecked = true;
                OH4.IsChecked = true;
                OH5.IsChecked = true;
                OH6.IsChecked = true;
                OH7.IsChecked = true;
            }
        }

        public void MadeChangesOnGridandcheckboxes()
        {
            try
            {
                if (tabControl.SelectedIndex == -1 || tabControl.SelectedIndex == 0)
                {
                    MarketWatchTableReflectionChanges();
                }
                else if (tabControl.SelectedIndex == 1)
                {
                    OpenPositionTableReflectionChanges();
                }
                else if (tabControl.SelectedIndex == 2)
                {
                    OrderBookTableReflectionChanges();
                }
                else if (tabControl.SelectedIndex == 3)
                {
                    OpenHoldingsTableReflectionChanges();
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "Api Setting | Exception : in samco refresh : " + ex, MessageType.Informational);
            }
        }

        private void OrderBookTableReflectionChanges()
        {
            if (obEXG == "false")
            {
                OB1.IsChecked = false;
                marketOrderBookGrid.Columns[1].Visibility = Visibility.Hidden;
            }
            else
            {
                OB1.IsChecked = true;
                marketOrderBookGrid.Columns[1].Visibility = Visibility.Visible;
            }

            if (obSymbol == "false")
            {
                OB2.IsChecked = false;
                marketOrderBookGrid.Columns[2].Visibility = Visibility.Hidden;
            }
            else
            {
                OB2.IsChecked = true;
                marketOrderBookGrid.Columns[2].Visibility = Visibility.Visible;
            }

            if (obOrderId == "false")
            {
                OB3.IsChecked = false;
                marketOrderBookGrid.Columns[3].Visibility = Visibility.Hidden;
            }
            else
            {
                OB3.IsChecked = true;
                marketOrderBookGrid.Columns[3].Visibility = Visibility.Visible;
            }

            if (obTime == "false")
            {
                OB4.IsChecked = false;
                marketOrderBookGrid.Columns[4].Visibility = Visibility.Hidden;
            }
            else
            {
                OB4.IsChecked = true;
                marketOrderBookGrid.Columns[4].Visibility = Visibility.Visible;
            }

            if (obType == "false")
            {
                OB5.IsChecked = false;
                marketOrderBookGrid.Columns[5].Visibility = Visibility.Hidden;
            }
            else
            {
                OB5.IsChecked = true;
                marketOrderBookGrid.Columns[5].Visibility = Visibility.Visible;
            }
            if (obOrderType == "false")
            {
                OB6.IsChecked = false;
                marketOrderBookGrid.Columns[6].Visibility = Visibility.Hidden;
            }
            else
            {
                OB6.IsChecked = true;
                marketOrderBookGrid.Columns[6].Visibility = Visibility.Visible;
            }

            if (obProduct == "false")
            {
                OB7.IsChecked = false;
                marketOrderBookGrid.Columns[7].Visibility = Visibility.Hidden;
            }
            else
            {
                OB7.IsChecked = true;
                marketOrderBookGrid.Columns[7].Visibility = Visibility.Visible;
            }

            if (obPrice == "false")
            {
                OB8.IsChecked = false;
                marketOrderBookGrid.Columns[8].Visibility = Visibility.Hidden;
            }
            else
            {
                OB8.IsChecked = true;
                marketOrderBookGrid.Columns[8].Visibility = Visibility.Visible;
            }

            if (obTriggerPrice == "false")
            {
                OB9.IsChecked = false;
                marketOrderBookGrid.Columns[9].Visibility = Visibility.Hidden;
            }
            else
            {
                OB9.IsChecked = true;
                marketOrderBookGrid.Columns[9].Visibility = Visibility.Visible;
            }

            if (obQTY == "false")
            {
                OB10.IsChecked = false;
                marketOrderBookGrid.Columns[10].Visibility = Visibility.Hidden;
            }
            else
            {
                OB10.IsChecked = true;
                marketOrderBookGrid.Columns[10].Visibility = Visibility.Visible;
            }
            if (obStatus == "false")
            {
                OB11.IsChecked = false;
                marketOrderBookGrid.Columns[11].Visibility = Visibility.Hidden;
            }
            else
            {
                OB11.IsChecked = true;
                marketOrderBookGrid.Columns[11].Visibility = Visibility.Visible;
            }

            if (obFilledQty == "false")
            {
                OB12.IsChecked = false;
                marketOrderBookGrid.Columns[12].Visibility = Visibility.Hidden;
            }
            else
            {
                OB12.IsChecked = true;
                marketOrderBookGrid.Columns[12].Visibility = Visibility.Visible;
            }

            if (obFilledPrice == "false")
            {
                OB13.IsChecked = false;
                marketOrderBookGrid.Columns[13].Visibility = Visibility.Hidden;
            }
            else
            {
                OB13.IsChecked = true;
                marketOrderBookGrid.Columns[13].Visibility = Visibility.Visible;
            }

            if (obRejectionOrderRemark == "false")
            {
                OB14.IsChecked = false;
                marketOrderBookGrid.Columns[14].Visibility = Visibility.Hidden;
            }
            else
            {
                OB14.IsChecked = true;
                marketOrderBookGrid.Columns[14].Visibility = Visibility.Visible;
            }
            DataViewOrderBook = mydtOrderBook.DefaultView;
            DataContext = this;
        }

        private void OpenPositionTableReflectionChanges()
        {
            if (opExchange == "false")
            {
                OP1.IsChecked = false;
                marketOpenPriceGrid.Columns[1].Visibility = Visibility.Hidden;
            }
            else
            {
                OP1.IsChecked = true;
                marketOpenPriceGrid.Columns[1].Visibility = Visibility.Visible;
            }

            if (opSymbol == "false")
            {
                OP2.IsChecked = false;
                marketOpenPriceGrid.Columns[2].Visibility = Visibility.Hidden;
            }
            else
            {
                OP2.IsChecked = true;
                marketOpenPriceGrid.Columns[2].Visibility = Visibility.Visible;
            }

            if (opProduct == "false")
            {
                OP3.IsChecked = false;
                marketOpenPriceGrid.Columns[3].Visibility = Visibility.Hidden;
            }
            else
            {
                OP3.IsChecked = true;
                marketOpenPriceGrid.Columns[3].Visibility = Visibility.Visible;
            }

            if (opQuantity == "false")
            {
                OP4.IsChecked = false;
                marketOpenPriceGrid.Columns[4].Visibility = Visibility.Hidden;
            }
            else
            {
                OP4.IsChecked = true;
                marketOpenPriceGrid.Columns[4].Visibility = Visibility.Visible;
            }

            if (opLtp == "false")
            {
                OP5.IsChecked = false;
                marketOpenPriceGrid.Columns[5].Visibility = Visibility.Hidden;
            }
            else
            {
                OP5.IsChecked = true;
                marketOpenPriceGrid.Columns[5].Visibility = Visibility.Visible;
            }
            if (opPrice == "false")
            {
                OP6.IsChecked = false;
                marketOpenPriceGrid.Columns[6].Visibility = Visibility.Hidden;
            }
            else
            {
                OP6.IsChecked = true;
                marketOpenPriceGrid.Columns[6].Visibility = Visibility.Visible;
            }

            if (opPL == "false")
            {
                OP7.IsChecked = false;
                marketOpenPriceGrid.Columns[7].Visibility = Visibility.Hidden;
            }
            else
            {
                OP7.IsChecked = true;
                marketOpenPriceGrid.Columns[7].Visibility = Visibility.Visible;
            }

            DataViewOpenPosition = mydtOpenPosition.DefaultView;
            DataContext = this;
        }

        private void OpenHoldingsTableReflectionChanges()
        {
            if (ohEXG == "false")
            {
                OH1.IsChecked = false;
                marketOpenHoldingsGrid.Columns[0].Visibility = Visibility.Hidden;
            }
            else
            {
                OH1.IsChecked = true;
                marketOpenHoldingsGrid.Columns[0].Visibility = Visibility.Visible;
            }

            if (ohSymbol == "false")
            {
                OH2.IsChecked = false;
                marketOpenHoldingsGrid.Columns[1].Visibility = Visibility.Hidden;
            }
            else
            {
                OH2.IsChecked = true;
                marketOpenHoldingsGrid.Columns[1].Visibility = Visibility.Visible;
            }

            if (ohProduct == "false")
            {
                OH3.IsChecked = false;
                marketOpenHoldingsGrid.Columns[2].Visibility = Visibility.Hidden;
            }
            else
            {
                OH3.IsChecked = true;
                marketOpenHoldingsGrid.Columns[2].Visibility = Visibility.Visible;
            }

            if (ohQuantity == "false")
            {
                OH4.IsChecked = false;
                marketOpenHoldingsGrid.Columns[3].Visibility = Visibility.Hidden;
            }
            else
            {
                OH4.IsChecked = true;
                marketOpenHoldingsGrid.Columns[3].Visibility = Visibility.Visible;
            }

            if (ohcloseprice == "false")
            {
                OH5.IsChecked = false;
                marketOpenHoldingsGrid.Columns[4].Visibility = Visibility.Hidden;
            }
            else
            {
                OH5.IsChecked = true;
                marketOpenHoldingsGrid.Columns[4].Visibility = Visibility.Visible;
            }
            if (ohavgprice == "false")
            {
                OH6.IsChecked = false;
                marketOpenHoldingsGrid.Columns[5].Visibility = Visibility.Hidden;
            }
            else
            {
                OH6.IsChecked = true;
                marketOpenHoldingsGrid.Columns[5].Visibility = Visibility.Visible;
            }

            if (ohPL == "false")
            {
                OH7.IsChecked = false;
                marketOpenHoldingsGrid.Columns[6].Visibility = Visibility.Hidden;
            }
            else
            {
                OH7.IsChecked = true;
                marketOpenHoldingsGrid.Columns[6].Visibility = Visibility.Visible;
            }

            DataViewOpenHoldings = mydtOpenHolding.DefaultView;
            DataContext = this;
        }
        private void MarketWatchTableReflectionChanges()
        {
            if (mwsymbolnameVal == "false")
            {
                MW1.IsChecked = false;
                marketWatchGrid.Columns[0].Visibility = Visibility.Hidden;
            }
            else
            {
                MW1.IsChecked = true;
                marketWatchGrid.Columns[0].Visibility = Visibility.Visible;
            }

            if (mwltpval == "false")
            {
                MW2.IsChecked = false;
                marketWatchGrid.Columns[1].Visibility = Visibility.Hidden;
            }
            else
            {
                MW2.IsChecked = true;
                marketWatchGrid.Columns[1].Visibility = Visibility.Visible;
            }

            if (mwbidval == "false")
            {
                MW3.IsChecked = false;
                marketWatchGrid.Columns[2].Visibility = Visibility.Hidden;
            }
            else
            {
                MW3.IsChecked = true;
                marketWatchGrid.Columns[2].Visibility = Visibility.Visible;
            }

            if (mwaskval == "false")
            {
                MW4.IsChecked = false;
                marketWatchGrid.Columns[3].Visibility = Visibility.Hidden;
            }
            else
            {
                MW4.IsChecked = true;
                marketWatchGrid.Columns[3].Visibility = Visibility.Visible;
            }

            if (mwvolumeval == "false")
            {
                MW5.IsChecked = false;
                marketWatchGrid.Columns[4].Visibility = Visibility.Hidden;
            }
            else
            {
                MW5.IsChecked = true;
                marketWatchGrid.Columns[4].Visibility = Visibility.Visible;
            }
            //IRDSPM::PRattiksha::24-09-2020::For new symbols
            if (mwOpen == "false")
            {
                MW6.IsChecked = false;
                marketWatchGrid.Columns[5].Visibility = Visibility.Hidden;
            }
            else
            {
                MW6.IsChecked = true;
                marketWatchGrid.Columns[5].Visibility = Visibility.Visible;
            }

            if (mwHigh == "false")
            {
                MW7.IsChecked = false;
                marketWatchGrid.Columns[6].Visibility = Visibility.Hidden;
            }
            else
            {
                MW7.IsChecked = true;
                marketWatchGrid.Columns[6].Visibility = Visibility.Visible;
            }

            if (mwLow == "false")
            {
                MW8.IsChecked = false;
                marketWatchGrid.Columns[7].Visibility = Visibility.Hidden;
            }
            else
            {
                MW8.IsChecked = true;
                marketWatchGrid.Columns[7].Visibility = Visibility.Visible;
            }

            if (mwClose == "false")
            {
                MW9.IsChecked = false;
                marketWatchGrid.Columns[8].Visibility = Visibility.Hidden;
            }
            else
            {
                MW9.IsChecked = true;
                marketWatchGrid.Columns[8].Visibility = Visibility.Visible;
            }

            if (mwchangeinper == "false")
            {
                MW10.IsChecked = false;
                marketWatchGrid.Columns[9].Visibility = Visibility.Hidden;
            }
            else
            {
                MW10.IsChecked = true;
                marketWatchGrid.Columns[9].Visibility = Visibility.Visible;
            }
            //IRDSPM::Pratiksha::08-10-2020::Added new column "Net Change"
            if (mwNetChange == "false")
            {
                MW11.IsChecked = false;
                marketWatchGrid.Columns[10].Visibility = Visibility.Hidden;
            }
            else
            {
                MW11.IsChecked = true;
                marketWatchGrid.Columns[10].Visibility = Visibility.Visible;
            }

            DataViewMarketWatch = mydtMarketWatch.DefaultView;
            DataContext = this;
        }

        public void MarketWatchCheckedandSave()
        {
            try
            {
                borderMW.Visibility = Visibility.Hidden;
                bordersymbolname.Visibility = Visibility.Hidden;
                rememberMeCheckboxMW.Visibility = Visibility.Hidden;
                symbolNameMWGrid.Visibility = Visibility.Hidden;
                gridButtonsMW.Visibility = Visibility.Hidden;
                lblColumnSetup.Visibility = Visibility.Hidden;
                if (tabControl.SelectedIndex == -1 || tabControl.SelectedIndex == 0)
                {
                    string[] mwkey = new string[11];
                    string[] mwVal = new string[11];
                    mwkey[0] = "Symbol";
                    mwkey[1] = "LTP";
                    mwkey[2] = "BidPrice";
                    mwkey[3] = "AskPrice";
                    mwkey[4] = "Volume";
                    mwkey[5] = "Open";
                    mwkey[6] = "High";
                    mwkey[7] = "Low";
                    mwkey[8] = "Close";
                    mwkey[9] = "Changein%";
                    //IRDSPM::Pratiksha::08-10-2020::Added new column "Net Change"
                    mwkey[10] = "NetChange";

                    if (MW1.IsChecked == true)
                    { mwVal[0] = "true"; }
                    else { mwVal[0] = "false"; }

                    if (MW2.IsChecked == true)
                    { mwVal[1] = "true"; }
                    else { mwVal[1] = "false"; }

                    if (MW3.IsChecked == true)
                    { mwVal[2] = "true"; }
                    else { mwVal[2] = "false"; }

                    if (MW4.IsChecked == true)
                    { mwVal[3] = "true"; }
                    else { mwVal[3] = "false"; }

                    if (MW5.IsChecked == true)
                    { mwVal[4] = "true"; }
                    else { mwVal[4] = "false"; }
                    //IRDSPM:Pratiksha::24-09-2020::Added for new symbols
                    if (MW6.IsChecked == true)
                    { mwVal[5] = "true"; }
                    else { mwVal[5] = "false"; }

                    if (MW7.IsChecked == true)
                    { mwVal[6] = "true"; }
                    else { mwVal[6] = "false"; }

                    if (MW8.IsChecked == true)
                    { mwVal[7] = "true"; }
                    else { mwVal[7] = "false"; }

                    if (MW9.IsChecked == true)
                    { mwVal[8] = "true"; }
                    else { mwVal[8] = "false"; }

                    if (MW10.IsChecked == true)
                    { mwVal[9] = "true"; }
                    else { mwVal[9] = "false"; }

                    //IRDSPM::Pratiksha::08-10-2020::Added new column "Net Change"
                    if (MW11.IsChecked == true)
                    { mwVal[10] = "true"; }
                    else { mwVal[10] = "false"; }
                    for (int j = 0; j < 11; j++)
                    {
                        Settings.writeINIFile("MarketWatchColumn", mwkey[j], mwVal[j]);
                    }
                    RefreshSettinginiFile();
                    MarketWatchTableReflectionChanges();
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "RememberMe Setting | Exception " + ex.Message, MessageType.Informational);
            }
        }
        public void OpenPositionCheckedandSave()
        {
            try
            {
                DataViewOpenPosition = mydtOpenPosition.DefaultView;
                DataContext = this;
                borderOP.Visibility = Visibility.Hidden;
                bordersymbolnameOP.Visibility = Visibility.Hidden;
                rememberMeCheckboxOP.Visibility = Visibility.Hidden;
                symbolNameOPGrid.Visibility = Visibility.Hidden;
                gridButtonsOP.Visibility = Visibility.Hidden;
                lblColumnSetupOP.Visibility = Visibility.Hidden;
                if (tabControl.SelectedIndex == 1)
                {
                    string[] opkey = new string[8];
                    string[] opVal = new string[8];
                    opkey[0] = "Exchange";
                    opkey[1] = "Symbol";
                    opkey[2] = "Product";
                    opkey[3] = "Quantity";
                    opkey[4] = "LTP";
                    opkey[5] = "Price";
                    opkey[6] = "P&L";


                    if (OP1.IsChecked == true)
                    { opVal[0] = "true"; }
                    else { opVal[0] = "false"; }

                    if (OP2.IsChecked == true)
                    { opVal[1] = "true"; }
                    else { opVal[1] = "false"; }

                    if (OP3.IsChecked == true)
                    { opVal[2] = "true"; }
                    else { opVal[2] = "false"; }

                    if (OP4.IsChecked == true)
                    { opVal[3] = "true"; }
                    else { opVal[3] = "false"; }

                    if (OP5.IsChecked == true)
                    { opVal[4] = "true"; }
                    else { opVal[4] = "false"; }

                    if (OP6.IsChecked == true)
                    { opVal[5] = "true"; }
                    else { opVal[5] = "false"; }

                    if (OP7.IsChecked == true)
                    { opVal[6] = "true"; }
                    else { opVal[6] = "false"; }
                    for (int k = 0; k < 7; k++)
                    {
                        Settings.writeINIFile("OpenPositionColumn", opkey[k], opVal[k]);
                    }
                    RefreshSettinginiFile();
                    OpenPositionTableReflectionChanges();
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "RememberMe Setting | Exception " + ex, MessageType.Informational);
            }
        }
        public void OrderBookCheckedandSave()
        {
            try
            {
                DataViewOrderBook = mydtOrderBook.DefaultView;
                DataContext = this;
                borderOB.Visibility = Visibility.Hidden;
                rememberMeCheckboxOB.Visibility = Visibility.Hidden;
                symbolNameOBGrid.Visibility = Visibility.Hidden;
                bordersymbolnameOB.Visibility = Visibility.Hidden;
                gridButtonsOB.Visibility = Visibility.Hidden;
                lblColumnSetupOB.Visibility = Visibility.Hidden;
                if (tabControl.SelectedIndex == 2)
                {
                    string[] obkey = new string[14];
                    string[] obVal = new string[14];
                    obkey[0] = "EXG";
                    obkey[1] = "Symbol";
                    obkey[2] = "OrderId";
                    obkey[3] = "Time";
                    obkey[4] = "Type";
                    obkey[5] = "OrderType";
                    obkey[6] = "Product";
                    obkey[7] = "Price";
                    obkey[8] = "TriggerPrice";
                    obkey[9] = "QTY";
                    obkey[10] = "Status";
                    obkey[11] = "FilledQty";
                    obkey[12] = "FilledPrice";
                    obkey[13] = "RejectionOrderRemark";


                    if (OB1.IsChecked == true)
                    { obVal[0] = "true"; }
                    else { obVal[0] = "false"; }

                    if (OB2.IsChecked == true)
                    { obVal[1] = "true"; }
                    else { obVal[1] = "false"; }

                    if (OB3.IsChecked == true)
                    { obVal[2] = "true"; }
                    else { obVal[2] = "false"; }

                    if (OB4.IsChecked == true)
                    { obVal[3] = "true"; }
                    else { obVal[3] = "false"; }

                    if (OB5.IsChecked == true)
                    { obVal[4] = "true"; }
                    else { obVal[4] = "false"; }

                    if (OB6.IsChecked == true)
                    { obVal[5] = "true"; }
                    else { obVal[5] = "false"; }

                    if (OB7.IsChecked == true)
                    { obVal[6] = "true"; }
                    else { obVal[6] = "false"; }

                    if (OB8.IsChecked == true)
                    { obVal[7] = "true"; }
                    else { obVal[7] = "false"; }

                    if (OB9.IsChecked == true)
                    { obVal[8] = "true"; }
                    else { obVal[8] = "false"; }

                    if (OB10.IsChecked == true)
                    { obVal[9] = "true"; }
                    else { obVal[9] = "false"; }

                    if (OB11.IsChecked == true)
                    { obVal[10] = "true"; }
                    else { obVal[10] = "false"; }

                    if (OB12.IsChecked == true)
                    { obVal[11] = "true"; }
                    else { obVal[11] = "false"; }

                    if (OB13.IsChecked == true)
                    { obVal[12] = "true"; }
                    else { obVal[12] = "false"; }

                    if (OB14.IsChecked == true)
                    { obVal[13] = "true"; }
                    else { obVal[13] = "false"; }
                    for (int k = 0; k < 14; k++)
                    {
                        Settings.writeINIFile("OrderBookColumn", obkey[k], obVal[k]);
                    }
                    RefreshSettinginiFile();
                    OrderBookTableReflectionChanges();
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "RememberMe Setting | Exception " + ex, MessageType.Informational);
            }
        }

        public void OpenHoldingCheckedandSave()
        {
            try
            {
                DataViewOpenHoldings = mydtOpenHolding.DefaultView;
                DataContext = this;
                borderOH.Visibility = Visibility.Hidden;
                bordersymbolnameOH.Visibility = Visibility.Hidden;
                rememberMeCheckboxOH.Visibility = Visibility.Hidden;
                symbolNameOHGrid.Visibility = Visibility.Hidden;
                gridButtonsOH.Visibility = Visibility.Hidden;
                lblcolumnSetupOH.Visibility = Visibility.Hidden;
                if (tabControl.SelectedIndex == 3)
                {
                    string[] oHkey = new string[8];
                    string[] oHVal = new string[8];
                    oHkey[0] = "Exchange";
                    oHkey[1] = "Symbol";
                    oHkey[2] = "Product";
                    oHkey[3] = "Quantity";
                    oHkey[4] = "closePrice";
                    oHkey[5] = "avgPrice";
                    oHkey[6] = "P&L";


                    if (OH1.IsChecked == true)
                    { oHVal[0] = "true"; }
                    else { oHVal[0] = "false"; }

                    if (OH2.IsChecked == true)
                    { oHVal[1] = "true"; }
                    else { oHVal[1] = "false"; }

                    if (OH3.IsChecked == true)
                    { oHVal[2] = "true"; }
                    else { oHVal[2] = "false"; }

                    if (OH4.IsChecked == true)
                    { oHVal[3] = "true"; }
                    else { oHVal[3] = "false"; }

                    if (OH5.IsChecked == true)
                    { oHVal[4] = "true"; }
                    else { oHVal[4] = "false"; }

                    if (OH6.IsChecked == true)
                    { oHVal[5] = "true"; }
                    else { oHVal[5] = "false"; }

                    if (OH7.IsChecked == true)
                    { oHVal[6] = "true"; }
                    else { oHVal[6] = "false"; }
                    for (int k = 0; k < 7; k++)
                    {
                        Settings.writeINIFile("OpenHoldingsColumn", oHkey[k], oHVal[k]);
                    }
                    RefreshSettinginiFile();
                    OpenHoldingsTableReflectionChanges();
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "RememberMe Setting | Exception " + ex, MessageType.Informational);
            }
        }

        private void FetchNotificationsDet()
        {
            try
            {
                m_issend = Settings.isSend;
                m_smssend = Settings.isSmsEnable;
                m_emailsend = Settings.isEmailEnable;
                m_mobNum = Settings.smsNumber;
                m_email = Settings.emailid;
                m_interval = Settings.sendinterval;
                m_issendUpdatesNotification = Settings.isOtherNotification;
            }
            catch (Exception ex)
            {
                WriteUniquelogs("ExecutionForm" + " ", "FetchNotificationsDet :Exception Error Message: " + ex.Message, MessageType.Informational);
            }
        }

        //IRDSPM::Pratiksha::21-06-2021::for lacks and thousand conversion
        private string FormatNumber(double num)
        {
            try
            {
                if (num >= 100000000)
                {
                    return (num / 1000000D).ToString("0.#L");
                }
                if (num >= 1000000)
                {
                    return (num / 1000000D).ToString("0.##L");
                }
                if (num >= 100000)
                {
                    return (num / 1000D).ToString("0.#k");
                }
                if (num >= 10000)
                {
                    return (num / 1000D).ToString("0.##k");
                }
                if (num >= 1000)
                {
                    return (num / 1000D).ToString("0.###k");
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "FormatNumber : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
            return num.ToString();
        }

        //IRDSPM::PRatiksha::11-05-2021::For sending sms and email
        private void SendsmsMail_Tick(object sender, EventArgs e)
        {
            try
            {
                if ((TimeSpan.Parse(DateTime.Now.ToString("HH:mm")) > TimeSpan.Parse("09:15")) && (TimeSpan.Parse(DateTime.Now.ToString("HH:mm")) < TimeSpan.Parse("15:30")))
                {
                    if (lblValueStatus.Content.ToString() == "Connected" && lblMarginAvailable.Content.ToString() != "NA" && lblMarginUsed.Content.ToString() != "NA" && lblTotalPNL.Content.ToString() != "NA" && lblOpenPositionCount.Content.ToString() != "NA")
                    {
                        if (obj == null)
                        {
                            obj = new SendInfo();
                        }
                        SendsmsMail.Interval = Convert.ToInt32(m_interval) * 60 * 1000;

                        if (m_issend == true)
                        {//IRDSPM::PRatiksha::18-05-2021::For sms and email send
                            string sub = "Updated Value for " + lblClientID.Content.ToString() + " at " + DateTime.Now.ToString("HH:mm:ss");
                            //sanika::22-Jul-2021::call format function for margin
                            string ma = FormatNumber(Convert.ToDouble(lblMarginAvailable.Content.ToString()));
                            string mu = FormatNumber(Convert.ToDouble(lblMarginAvailable.Content.ToString()));
                            string pl = FormatNumber(Convert.ToDouble(lblTotalPNL.Content.ToString()));
                            string LMA = FormatNumber(m_LowestMA);
                            string HMA = FormatNumber(m_HighestMA);
                            string LPNL = FormatNumber(m_LowestPNL);
                            string HPNL = FormatNumber(m_HighestPNL);
                            string message = lblClientID.Content.ToString() + " PL " + pl + " MA " + ma + " MU " + mu +" Open Positions: " + lblOpenPositionCount.Content.ToString()+"  LMA: " + LMA + "  HMA: " + HMA + "  LPNL: " + LPNL + "  HPNL: " + HPNL ;
                            //string message = lblClientID.Content.ToString() + " PL " + lblTotalPNL.Content.ToString() + " MA " + ma + " MU" + mu + " "+DateTime.Now.ToString("HH:mm:ss");

                            if (lblOpenPositionCount.Content.ToString() != "NA" && m_PendingOrderCount < Convert.ToInt32(lblOpenPositionCount.Content.ToString()) && m_IsOrderBookUpdated == true)
                            {
                                message += "Open Positions: " + lblOpenPositionCount.Content.ToString() + "  pending orders : " + m_PendingOrderCount;
                            }
                            message += " " + DateTime.Now.ToString("HH:mm:ss");
                            SendSMSAndEmail(message);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "SendsmsMail_Tick : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        public void SendSMSAndEmail(string message)
        {
            string sub = "Information from IRDS";
            if (m_emailsend == true)
            {
                if (m_email.Contains(","))
                {
                    string[] emailDet = m_email.Split(',');
                    for (int i = 0; i < emailDet.Length; i++)
                    {
                        WriteUniquelogs("SendEmailSms", "SendSMSAndEmail : sending email ", MessageType.Informational);
                        bool res = false;
                        if (m_senderName != "XXXXXX" && m_senderemail != "XXXXXX" && m_senderhost != "XXXXXX" && m_senderpassword != "XXXXXX" && m_senderPort != "XXXXXX")
                        {
                            res = obj.SendEmailViaMailkit(emailDet[i], sub, message);
                        }
                        if (res == false)
                        {
                            if (m_senderName != "XXXXXX" && m_senderemail != "XXXXXX" && m_senderhost != "XXXXXX" && m_senderpassword != "XXXXXX" && m_senderPort != "XXXXXX")
                            {
                                obj.SendEmailViaMailkit(emailDet[i], sub, message);
                            }

                        }
                    }
                }
                else
                {
                    WriteUniquelogs("SendEmailSms", "SendSMSAndEmail : sending email ", MessageType.Informational);
                    bool res = false;
                    if (m_senderName != "XXXXXX" && m_senderemail != "XXXXXX" && m_senderhost != "XXXXXX" && m_senderpassword != "XXXXXX" && m_senderPort != "XXXXXX")
                    {
                        res = obj.SendEmailViaMailkit(m_email, sub, message);
                    }
                    if (res == false)
                    {
                        if (m_senderName != "XXXXXX" && m_senderemail != "XXXXXX" && m_senderhost != "XXXXXX" && m_senderpassword != "XXXXXX" && m_senderPort != "XXXXXX")
                        {
                            obj.SendEmailViaMailkit(m_email, sub, message);
                        }
                    }
                }
            }
            if (m_smssend == true)
            {
                if (m_mobNum.Contains(","))
                {
                    string[] mobNumDet = m_mobNum.Split(',');
                    for (int i = 0; i < mobNumDet.Length; i++)
                    {
                        if (mobNumDet[i].Contains("+91"))
                        {
                            obj.SendSMS(mobNumDet[i], message);
                        }
                        else
                        {
                            obj.SendSMS("+91" + mobNumDet[i], message);
                        }
                    }
                }
                else
                {
                    if (m_mobNum.Contains("+91"))
                    {
                        obj.SendSMS(m_mobNum, message);
                        WriteUniquelogs("SendEmailSms", "SendSMSAndEmail : sending sms ", MessageType.Informational);
                    }
                    else
                    {
                        obj.SendSMS("+91" + m_mobNum, message);
                        WriteUniquelogs("SendEmailSms", "SendSMSAndEmail : sending sms ", MessageType.Informational);
                    }
                }
            }
        }
        //sanika::23-sep-2020::Read data from setting.ini file
        public void RefreshSettinginiFile()
        {
            if (File.Exists(iniFileSetting))
            {
                try
                {
                    if (Settings == null)
                    {
                        Settings = new ConfigSettings(logger);
                    }
                    Settings.ReadSettingConfigFile(iniFileSetting);
                    FetchColumnSetupDet();
                    FetchNotificationsDet();
                    FetchSMTPDet();

                    AudioFilePath = Settings.toastAudioPath;
                    title = Settings.m_Title;
                    helpTitle.Header = title + " Help";

                    //IRDSPM::Pratiksha::11-05-2021::Sending sms and email
                    if (m_issendUpdatesNotification == true)
                    {
                        SendsmsMail.Tick += new EventHandler(SendsmsMail_Tick);
                        SendsmsMail.Start();
                        SendsmsMail.Enabled = true;
                    }
                    else
                    {
                        SendsmsMail.Enabled = false;
                    }
                }
                catch (Exception ex)
                {
                    WriteUniquelogs("ExecutionForm" + " ", "RememberMe :Exception Error Message: " + ex.Message, MessageType.Informational);
                }
            }
            else
            {
                WriteUniquelogs("ExecutionForm" + " ", "Setting.ini file not found.", MessageType.Informational);
            }
        }

        private void FetchSMTPDet()
        {
            try
            {
                m_senderName = Settings.m_senderName;
                m_senderemail = Settings.m_senderemail;
                m_senderpassword = Settings.m_senderpassword;
                m_senderhost = Settings.m_senderhost;
                m_senderPort = Settings.m_senderPort;
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "FetchSMTPDet :Exception Error Message: " + ex.Message, MessageType.Informational);
            }
        }

        private void FetchColumnSetupDet()
        {
            try
            {
                //market watch
                mwsymbolnameVal = Settings.mwsymbolname;
                mwltpval = Settings.mwltp;
                mwbidval = Settings.mwbid;
                mwaskval = Settings.mwask;
                mwvolumeval = Settings.mwvolume;
                mwOpen = Settings.mwOpen;
                mwHigh = Settings.mwHigh;
                mwLow = Settings.mwLow;
                mwClose = Settings.mwClose;
                mwchangeinper = Settings.mwchangeinper;
                //IRDSPM::Pratiksha::08-10-2020::Added new column "Net Change"
                mwNetChange = Settings.mwNetChange;
                //open position
                opExchange = Settings.opExchange;
                opSymbol = Settings.opSymbol;
                opProduct = Settings.opProduct;
                opQuantity = Settings.opQuantity;
                opLtp = Settings.opLTP;
                opPrice = Settings.opPrice;
                opPL = Settings.opPL;

                //Order book
                obEXG = Settings.obEXG;
                obSymbol = Settings.obSymbol;
                obOrderId = Settings.obOrderId;
                obTime = Settings.obTime;
                obType = Settings.obType;
                obOrderType = Settings.obOrderType;
                obProduct = Settings.obProduct;
                obPrice = Settings.obPrice;
                obTriggerPrice = Settings.obTriggerPrice;
                obQTY = Settings.obQTY;
                obStatus = Settings.obStatus;
                obFilledQty = Settings.obFilledQty;
                obFilledPrice = Settings.obFilledPrice;
                obRejectionOrderRemark = Settings.obRejectionOrderRemark;

                //open Holdings
                ohEXG = Settings.ohEXG;
                ohSymbol = Settings.ohSymbol;
                ohProduct = Settings.ohProduct;
                ohQuantity = Settings.ohQuantity;
                ohcloseprice = Settings.ohcloseprice;
                ohavgprice = Settings.ohavgprice;
                ohPL = Settings.ohPL;
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "FetchColumnSetupDet :Exception Error Message: " + ex.Message, MessageType.Informational);
            }
        }

        //IRDSPM::PRatiksha::02-10-2020::For refresh image click
        private void imagerefresh_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            RefreshAllData();
        }

        private void RefreshAllData()
        {
            try
            {
                this.Dispatcher.Invoke(() =>
                {
                    imagerefresh.IsEnabled = false;
                });
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                m_bRefreshDone = false;
                int counter = 0;
                while (m_bRefreshDone == false && counter < 20)
                {
                    Thread.Sleep(100);
                    counter++;
                }
                if (m_bRefreshDone == false)
                {
                    WriteUniquelogs("ExecutionForm" + " " + m_UserID, "btnRefresh_Click : m_bRefreshDone is false", MessageType.Informational);
                }
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
                this.Dispatcher.Invoke(() =>
                {
                    imagerefresh.IsEnabled = true;
                });
                toastDisplay(DateTime.Now.ToString() + " Refresh done sucessfully!!");
                tabList.Items.Add(DateTime.Now.ToString() + " Refresh done sucessfully!!");
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "btnRefresh_Click : Refresh Done Sucessfully", MessageType.Informational);
            }
            catch (Exception er)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "btnRefresh_Click : Exception Error Message = " + er.Message, MessageType.Exception);
            }
            return;
        }
        //IRDSPM::PRatiksha::12-09-2020::On column header right click should open sorting
        private void columnHeader_Click(object sender, MouseButtonEventArgs e)
        {
            m_ColumnSelectionForAscOrder = sender.ToString();
            OpenContextmenuStrip();
        }

        //sanika::29-sep-2020::Added function to stop thread
        public void StopThread()
        {
            try
            {
                //sanika::2-oct-2020::added to stop thread of pipe
                PipeConnection.stopThread = true;
                StopTimer();
                m_StopThread = true;
                m_StopBridgeThread = true;
                if (m_ObjkiteConnectWrapper != null)
                {
                    m_ObjkiteConnectWrapper.StopThread();
                }
                //Thread.Sleep(5000);
            }
            catch (Exception e)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "StopThread : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        //sanika::29-sep-2020::Added function to stop timer
        public void StopTimer()
        {
            try
            {
                if (m_Timer != null)
                {
                    //sanika::23-Nov-2020::First close then timer then dispose
                    m_Timer.Stop();
                    m_Timer.Close();
                    m_Timer.Dispose();
                    WriteUniquelogs("ExecutionForm" + " " + m_UserID, "StopTimer : Closed timer of pnl and load values on gui ", MessageType.Informational);
                }

                if (m_TimerForBridgeStatus != null)
                {
                    //sanika::23-Nov-2020::First close then timer then dispose
                    m_TimerForBridgeStatus.Stop();
                    m_TimerForBridgeStatus.Close();
                    m_TimerForBridgeStatus.Dispose();
                    WriteUniquelogs("ExecutionForm" + " " + m_UserID, "StopTimer : Closed timer of bridge", MessageType.Informational);
                }
                Thread.Sleep(100);
            }
            catch (Exception e)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "StopTimer : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        //IRDSPM::PRatiksha::02-10-2020::For refresh button click
        private void refreshbtn_Click(object sender, RoutedEventArgs e)
        {
            RefreshAllData();
        }

        private void Grid_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            KeypressEvents(e);
        }

        private void KeypressEvents(System.Windows.Input.KeyEventArgs e)
        {
            if (((e.Key == Key.F1) || (e.Key == Key.VolumeMute)) && (!Keyboard.IsKeyDown(Key.LeftCtrl)))
            {
                //IRDSPM::Pratiksha::22-10-2020::If symbol not selected
                String type = "Buy";
                GetSymbolIfNoneSelected(type);
                if (m_ObjBuyForm == null)
                {
                    m_ObjBuyForm = new BuyWindowsForm(m_ObjkiteConnectWrapper, title);
                }
                m_ObjBuyForm.orderType(typeofOrder);
                m_ObjBuyForm.ShowDialog();
            }
            else if ((e.Key == Key.F2) || (e.Key == Key.VolumeDown))
            {
                //IRDSPM::Pratiksha::22-10-2020::If symbol not selected
                String type = "Sell";
                GetSymbolIfNoneSelected(type);
                if (m_ObjBuyForm == null)
                {
                    m_ObjBuyForm = new BuyWindowsForm(m_ObjkiteConnectWrapper, title);
                }
                m_ObjBuyForm.orderType(typeofOrder);
                m_ObjBuyForm.ShowDialog();
            }
            else if ((e.Key == Key.F5) || (e.Key == Key.MediaPlayPause))
            {
                RefreshAllData();
            }
            else if ((e.Key == Key.Add) || ((Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift)) && e.Key == Key.OemPlus))
            {
                m_symbolSettingWindowObj = new AllSymbolSettingWindowsForm(m_ObjkiteConnectWrapper, title);
                m_symbolSettingWindowObj.ShowDialog();
            }
            else if (e.Key == Key.Escape)
            {
                if (m_ObjBuyForm.Visible == true)
                {
                    m_ObjBuyForm.Close();
                }
            }
            else if (e.SystemKey == Key.Escape)
            {
                Deletedialog = DialogResult.No;
            }
            //IRDSPM::Pratiksha::27-11-2020::Open help manal
            else if ((Keyboard.IsKeyDown(Key.LeftCtrl) && e.Key == Key.F1) || (Keyboard.IsKeyDown(Key.RightCtrl) && e.Key == Key.F1))
            {
                try
                {
                    if (File.Exists(path + "\\" + title + " Help.pdf"))
                    {
                        Process.Start(path + "\\" + title + " Help.pdf");
                    }
                    else
                    {
                        WriteUniquelogs("ExecutionForm" + " " + m_UserID, "KeypressEvents : File not found", MessageType.Informational);
                    }
                }
                catch (Exception ex)
                {
                    WriteUniquelogs("ExecutionForm" + " " + m_UserID, "KeypressEvents : Exception Error Message = " + ex.Message, MessageType.Informational);
                }
            }

            //IRDSPM::Pratiksha::10-10-2020::On delete click row should remove
            else if (e.Key == Key.Delete || (Keyboard.IsKeyDown(Key.LeftShift) && e.Key == Key.Delete) || (Keyboard.IsKeyDown(Key.RightShift) && e.Key == Key.Delete))
            {
                if (tabControl.SelectedIndex == -1 || tabControl.SelectedIndex == 0)
                {
                    DataRowView row1 = (DataRowView)marketWatchGrid.SelectedItems[0];
                    m_CurrentSymbolForMWRow = row1["Symbol"].ToString();
                    DeleteSymbol();
                }
            }
        }
        //sanika::29-sep-2020::Added function to check thread
        //public bool CheckAllThreadExistOrNot()
        //{
        //    bool isStopAllThread = false;
        //    try
        //    {
        //        if(m_LoadAllValuesOnGUI != null)
        //        {
        //            if (!m_LoadAllValuesOnGUI.IsAlive)
        //            {
        //                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "CheckAllThreadExistOrNot : m_LoadAllValuesOnGUI Thread closed", MessageType.Informational);
        //                isStopAllThread = true;
        //            }
        //            else
        //            {
        //                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "CheckAllThreadExistOrNot : m_LoadAllValuesOnGUI Not able to close", MessageType.Informational);
        //                return false;
        //            }
        //        }
        //        if(m_UpdatePNL !=null)
        //        {
        //            if (!m_UpdatePNL.IsAlive)
        //            {
        //                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "CheckAllThreadExistOrNot : m_UpdatePNL Thread closed", MessageType.Informational);
        //                isStopAllThread = true;
        //            }
        //            else
        //            {
        //                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "CheckAllThreadExistOrNot : m_UpdatePNL Not able to close", MessageType.Informational);
        //                return false;
        //            }
        //        }
        //        if(m_ReadResponseFromNamedPipe!= null)
        //        {
        //            if (!m_ReadResponseFromNamedPipe.IsAlive)
        //            {
        //                isStopAllThread = true;
        //                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "CheckAllThreadExistOrNot : m_ReadResponseFromNamedPipe Thread closed", MessageType.Informational);
        //            }
        //            else
        //            {
        //                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "CheckAllThreadExistOrNot : m_ReadResponseFromNamedPipe Not able to close", MessageType.Informational);
        //                return false;
        //            }
        //        }

        //        if (m_ObjkiteConnectWrapper != null)
        //            isStopAllThread = m_ObjkiteConnectWrapper.CheckAllThreadExistOrNot();
        //    }
        //    catch (Exception e)
        //    {
        //        WriteUniquelogs("ExecutionForm" + " " + m_UserID, "CheckAllThreadExistOrNot : Exception Error Message = " + e.Message, MessageType.Exception);
        //    }
        //    return isStopAllThread;
        //}

        //IRDSPM::PRatiksha::16-10-2020::Display progressbar
        //IRDSPM::Pratiksha::20-10-2020::Added for Opening progressbar to download instruments
        ProgressBarWindowsForm obj_progressbar = null;
        public void OpenProgressBar()
        {
            try
            {
                if (obj_progressbar == null)
                {
                    obj_progressbar = new ProgressBarWindowsForm(title);
                }
                obj_progressbar.Show();
            }
            catch (Exception ex)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "OpenProgressBar : Exception Error Message = " + ex.Message, MessageType.Informational);
            }
        }

        //IRDSPM::Pratiksha::17-10-2020::Added new form to display subscription details
        public void OpenSubDetForm_Click(object sender, RoutedEventArgs e)
        {
            if (m_subDetObj == null)
            {
                m_subDetObj = new SubscriptionDetWindowsForm(m_ObjkiteConnectWrapper, title);
            }
            m_subDetObj.ShowDialog();
        }

        //IRDSPM::Pratiksha::22-10-2020::Added for renew option in menu
        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Process.Start("https://rzp.io/i/dxIZMgn");
            }
            catch (Exception ex)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "MenuItem_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }
        //IRDSPM::Pratiksha::27-10-2020::For opening pdf document
        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {
                Console.WriteLine(path + "\\" + title + " Help.pdf");
                if (File.Exists(path + "\\" + title + " Help.pdf"))
                {
                    Process.Start(path + "\\" + title + " Help.pdf");
                }
                else
                {
                    WriteUniquelogs("ExecutionForm" + " " + m_UserID, "MenuItem_Click_1 : File not found", MessageType.Informational);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "MenuItem_Click_1 : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }
        //IRDSPM::Pratiksha::27-10-2020::For opening pdf document
        AboutUsWindowsForm obj_aboutUs = null;
        private void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {
            try
            {
                if (obj_aboutUs == null)
                {
                    obj_aboutUs = new AboutUsWindowsForm(m_ObjkiteConnectWrapper, title);
                }
                obj_aboutUs.ShowDialog();
            }
            catch (Exception ex)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "MenuItem_Click_2 : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }
        public void WritePNLIntoFile(double pnl)
        {
            try
            {
                if (!File.Exists(pnlFilePath))
                {
                    string header = String.Join(Environment.NewLine, "Time,PNL");
                    header += Environment.NewLine;
                    System.IO.File.AppendAllText(pnlFilePath, header);
                }

                using (StreamWriter streamWriter = File.AppendText(pnlFilePath))
                {
                    string time = DateTime.Now.ToString("HH:mm:ss");
                    String csv = String.Join(Environment.NewLine, time + "," + pnl);
                    streamWriter.WriteLine(csv);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "WritePNLIntoFile : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        private void DownloadAllTokenMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Thread downloadinstruments = new Thread(() => m_ObjkiteConnectWrapper.DownloadTokenFromuserManualClick());
                downloadinstruments.SetApartmentState(ApartmentState.STA);
                downloadinstruments.Start();
            }
            catch (Exception ex)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "DownloadAllTokenMenu_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        //Pratiksha::Added ot get harware id
        public string GetHardwareID()
        {
            string hardwareId = "";
            try
            {
                hardwareId = License.Status.HardwareID;
            }
            catch (Exception e)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "GetHardwareID : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return hardwareId;
        }
        //IRDSPM::Pratiksha::03-06-2021::Download CSV and create name for file
        private void btnDownloadCsv_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                string filename = "";
                string pathname = "";

                if (tabControl.SelectedIndex == 1)
                {
                    DataTable dt = mydtOpenPosition;
                    if (dt != null)
                    {
                        filename = DateTime.Now.ToString("dd-MM-yyyy--HH-mm-ss") + "-" + m_UserID + "-OpenPosition" + ".csv";
                        pathname = path + @"\Reports\" + filename;
                        ToCSV(dt, pathname, filename, false);
                    }
                }
                else if (tabControl.SelectedIndex == 2)
                {
                    DataTable dt = mydtOrderBook;
                    if (dt != null)
                    {
                        filename = DateTime.Now.ToString("dd-MM-yyyy--HH-mm-ss") + "-" + m_UserID + "-OrderBook" + ".csv";
                        pathname = path + @"\Reports\" + filename;
                        ToCSV(dt, pathname, filename, false);
                    }
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "btnDownloadCsv_MouseLeftButtonDown : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }
        //IRDSPM::Pratiksha::03-06-2021::Create excel sheet
        private void ToCSV(DataTable dtDataTable, string pathname, string filename, bool timewise)
        {
            try
            {
                System.IO.Directory.CreateDirectory("Reports");
                if (timewise == false)
                {
                    StreamWriter sw = new StreamWriter(pathname, false);
                    for (int i = 0; i < dtDataTable.Columns.Count; i++)
                    {
                        sw.Write(dtDataTable.Columns[i]);
                        if (i < dtDataTable.Columns.Count - 1)
                        {
                            sw.Write(",");
                        }
                    }
                    sw.Write(sw.NewLine);
                    foreach (DataRow dr in dtDataTable.Rows)
                    {
                        for (int i = 0; i < dtDataTable.Columns.Count; i++)
                        {
                            if (!Convert.IsDBNull(dr[i]))
                            {
                                string value = dr[i].ToString();
                                if (value.Contains(','))
                                {
                                    value = String.Format("\"{0}\"", value);
                                    sw.Write(value.ToString());
                                }
                                else
                                {
                                    sw.Write(dr[i].ToString());
                                }
                            }
                            if (i < dtDataTable.Columns.Count - 1)
                            {
                                sw.Write(",");
                            }
                        }
                        sw.Write(sw.NewLine);
                    }
                    sw.Close();
                    if (!tabList.Items.Contains(filename + " downloaded at " + pathname + "."))
                    {
                        toastDisplay("File downloaded with name " + filename + " !!!");
                        tabList.Items.Add(DateTime.Now.ToString() + " File downloaded with name " + filename + " at " + pathname + ".");
                    }
                    Process.Start(@pathname);
                }
                else
                {
                    StreamWriter sw = new StreamWriter(pathname, false);
                    sw.Write("Account Number: " + lblClientID.Content.ToString() + "\n");
                    if (filename.Contains("OpenPosition"))
                    {
                        for (int i = 0; i < dtDataTable.Columns.Count; i++)
                        {
                            if (dtDataTable.Columns[i].ToString() != "Quantity")
                            {
                                sw.Write(dtDataTable.Columns[i]);
                                if (i < dtDataTable.Columns.Count - 1)
                                {
                                    sw.Write(",");
                                }
                            }
                        }
                        sw.Write(sw.NewLine);
                        foreach (DataRow dr in dtDataTable.Rows)
                        {
                            for (int i = 0; i < dtDataTable.Columns.Count; i++)
                            {
                                if (i != 3)
                                {
                                    if (!Convert.IsDBNull(dr[i]))
                                    {
                                        string value = dr[i].ToString();
                                        if (value.Contains(','))
                                        {
                                            value = String.Format("\"{0}\"", value);
                                            sw.Write(value.ToString());
                                        }
                                        else
                                        {
                                            sw.Write(dr[i].ToString());
                                        }
                                    }
                                    if (i < dtDataTable.Columns.Count - 1)
                                    {
                                        sw.Write(",");
                                    }
                                }
                            }
                            sw.Write(sw.NewLine);
                        }
                    }
                    else
                    {
                        for (int i = 0; i < dtDataTable.Columns.Count; i++)
                        {
                            sw.Write(dtDataTable.Columns[i]);
                            if (i < dtDataTable.Columns.Count - 1)
                            {
                                sw.Write(",");
                            }
                        }
                        sw.Write(sw.NewLine);
                        foreach (DataRow dr in dtDataTable.Rows)
                        {
                            for (int i = 0; i < dtDataTable.Columns.Count; i++)
                            {
                                if (!Convert.IsDBNull(dr[i]))
                                {
                                    string value = dr[i].ToString();
                                    if (value.Contains(','))
                                    {
                                        value = String.Format("\"{0}\"", value);
                                        sw.Write(value.ToString());
                                    }
                                    else
                                    {
                                        sw.Write(dr[i].ToString());
                                    }
                                }
                                if (i < dtDataTable.Columns.Count - 1)
                                {
                                    sw.Write(",");
                                }
                            }
                            sw.Write(sw.NewLine);
                        }
                    }
                    sw.Close();
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "ToCSV : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }
        bool isClearAll = false;
        private void btnClearAll_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                isClearAll = true;
                INIFileForSetting iniObj = new INIFileForSetting();
                iniObj.clearTestingSymbol("RealTimeSymbols", null, null, inisymbolList);
                if (Settings == null)
                {
                    Settings = new ConfigSettings();
                }
                Settings.WriteSymbolList("RealTimeSymbols", "0", "NIFTY 50.NSE");
                Settings.WriteSymbolList("RealTimeSymbols", "1", "NIFTY BANK.NSE");
                ListForMarketWtchGridView.Clear();
                mydtMarketWatch.Clear();
                m_RealTimeSymbols.Clear();
                SymbolList.Clear();
                ListForMarketWtchGridViewDeletedSymbols.Clear();
                m_ObjkiteConnectWrapper.ClearTradingSymbols();
            }
            catch (Exception ex)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "btnClearAll_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        public void SendNotification()
        {
            try
            {
                while (true && m_StopThread == false)
                {
                    if (m_ObjkiteConnectWrapper != null && m_ObjkiteConnectWrapper.GetConnectionStatus())
                    {
                        if ((TimeSpan.Parse(DateTime.Now.ToString("HH:mm")) > TimeSpan.Parse("09:15")) && (TimeSpan.Parse(DateTime.Now.ToString("HH:mm")) < TimeSpan.Parse("15:30")))
                        {
                            string message = m_ObjkiteConnectWrapper.GetMessageforNotification();
                            if (message != "NA" && message != "" && message != null)
                            {
                                if (m_issend == true)
                                {
                                    string sub = "Information from IRDS";
                                    if (m_emailsend == true)
                                    {
                                        if (m_email.Contains(","))
                                        {
                                            string[] emailDet = m_email.Split(',');
                                            for (int i = 0; i < emailDet.Length; i++)
                                            {
                                                if (m_senderName != "XXXXXX" && m_senderemail != "XXXXXX" && m_senderhost != "XXXXXX" && m_senderpassword != "XXXXXX" && m_senderPort != "XXXXXX")
                                                {
                                                    obj.SendEmailViaMailkit(emailDet[i], sub, message);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (m_senderName != "XXXXXX" && m_senderemail != "XXXXXX" && m_senderhost != "XXXXXX" && m_senderpassword != "XXXXXX" && m_senderPort != "XXXXXX")
                                            {
                                                obj.SendEmailViaMailkit(m_email, sub, message);
                                            }
                                        }
                                    }
                                    if (m_smssend == true)
                                    {
                                        if (m_mobNum.Contains(","))
                                        {
                                            string[] mobNumDet = m_mobNum.Split(',');
                                            for (int i = 0; i < mobNumDet.Length; i++)
                                            {
                                                if (mobNumDet[i].Contains("+91"))
                                                {
                                                    obj.SendSMS(mobNumDet[i], message);
                                                }
                                                else
                                                {
                                                    obj.SendSMS("+91" + mobNumDet[i], message);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (m_mobNum.Contains("+91"))
                                            {
                                                WriteUniquelogs("SendEmailSms", "SendNotification : sending sms ", MessageType.Informational);
                                                obj.SendSMS(m_mobNum, message);
                                            }
                                            else
                                            {
                                                WriteUniquelogs("SendEmailSms", "SendNotification : sending sms ", MessageType.Informational);
                                                obj.SendSMS("+91" + m_mobNum, message);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //sanika::26-Jul-2021::chnaged position of sleep to reduce cpu usage
                    Thread.Sleep(5000);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "SendNotification : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        public string GetFormattedValues(string value)
        {
            double num = Convert.ToDouble(value);
            if (num >= 100000)
                return GetFormattedValues((num / 100000).ToString()) + "L";
            if (num >= 10000)
            {
                return (num / 1000D).ToString("0.#") + "K";
            }
            return num.ToString("#,0");

        }
    }
}