﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IRDSAlgoOMS
{
    public partial class TradingSymbol : Form
    {
        Logger logger = Logger.Instance;
        string profit = " ";
        string loss = " ";
        string startTime2 = " ";
        string endTime2 = " ";
        string startTimeNFO2 = " ";
        string startTimeMCX2 = " ";
        string TimeLimitToPlaceOrder2 = " ";
        string TimeForExitAllOpenPosition2 = " ";
        ConfigSettings Settings;
        ReadSettings readSetting;
        string iniFileTradingSymbol;
        string iniFileSetting;
        string path = Directory.GetCurrentDirectory();
        string fontDet = "";
        string fontStyle = "";
        //IRDSPM::14-08-2020::For temporary storing value
        bool currentStoreDataMysqlDBVal;
        bool currentstoreDataSqliteDB;
        bool currentstoreDataCSV;
        bool currentValShowGUI;
        bool currentstoreDataMysqlDB;
        bool currentcalculateProfitLoss;
        //IRDSPM::PRatiksha::14-08-2020:: newly added
        bool calculateProfitLossVal;
        bool storeDataMysqlDBVal;
        bool storeDataSqliteDBVal;
        bool storeDataCSVVal;
        bool historicalDataInMysqlDBVal;
        string StartTimeNFOVal = "";
        string StartTimeMCXVal = "";
        string endTimeVal = "";
        //sanika::14-sep-2020::changed to dyanamic
        dynamic m_objKiteConnectWrapper = null;
        public TradingSymbol()
        {
            InitializeComponent();
            iniFileSetting = path + @"\Configuration\" + "Setting.ini";
            if (File.Exists(iniFileSetting))
            {
                Settings = new ConfigSettings(logger);
                Settings.ReadSettingConfigFile(iniFileSetting);
                fontDet = Settings.fontdet.ToString();
                fontStyle = Settings.fontstyle.ToString();
            }
           
            //IRDSPM::Pratiksha::09-07-2020::Accessing font family and font size --start
            try
            {
                string fontfamilyAndSize = fontDet.Substring(fontDet.IndexOf("=") + 1, fontDet.IndexOf(",") - 1);
                string fontname = fontfamilyAndSize.Substring(0, fontfamilyAndSize.IndexOf(","));
                int pFrom = fontfamilyAndSize.IndexOf("=") + "=".Length;
                int pTo = fontfamilyAndSize.LastIndexOf(",");
                //string result = fontfamilyAndSize.Substring(pFrom, pTo - pFrom);
                string result = fontfamilyAndSize.Substring(pFrom, fontfamilyAndSize.Length - pFrom);
                float fontSize = float.Parse(result);
               
                startTime.Font = new Font(fontname, fontSize, FontStyle.Bold, GraphicsUnit.Point);
               // endTime.Font = new Font(fontname, fontSize, FontStyle.Bold, GraphicsUnit.Point);
                overallProfit.Font = new Font(fontname, fontSize, FontStyle.Bold, GraphicsUnit.Point);
                overallLoss.Font = new Font(fontname, fontSize, FontStyle.Bold, GraphicsUnit.Point);
                TimeLimitToPlaceOrder.Font = new Font(fontname, fontSize, FontStyle.Bold, GraphicsUnit.Point);
                TimeForExitAllOpenPosition.Font = new Font(fontname, fontSize, FontStyle.Bold, GraphicsUnit.Point);
                //IRDSPM::Pratiksha::14-08-2020::have dynamic font size for all variables
               // calculateProfitLoss.Font = new Font(fontname, fontSize, FontStyle.Bold, GraphicsUnit.Point);
               // storeDataMysqlDB.Font = new Font(fontname, fontSize, FontStyle.Bold, GraphicsUnit.Point);
               // storeDataSqliteDB.Font = new Font(fontname, fontSize, FontStyle.Bold, GraphicsUnit.Point);
               // storeDataCSV.Font = new Font(fontname, fontSize, FontStyle.Bold, GraphicsUnit.Point);
                endTime.Font = new Font(fontname, fontSize, FontStyle.Bold, GraphicsUnit.Point);
                //historicalDataInMysqlDB.Font = new Font(fontname, fontSize, FontStyle.Bold, GraphicsUnit.Point);
                startTimeMCX.Font = new Font(fontname, fontSize, FontStyle.Bold, GraphicsUnit.Point);
                startTimeNFO.Font = new Font(fontname, fontSize, FontStyle.Bold, GraphicsUnit.Point);

                txtLoss.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                txtProfit.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                startTimePicker.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                TTPOTimePicker.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                TOEOTimePicker.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                startTimeNFOTimePicker.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                startTimeMCXTimePicker.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);

            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception : " + ex);
            }
            LoadTradingSettingDet();
        }
        private void LoadTradingSettingDet()
        {
            iniFileTradingSymbol = path + @"\Configuration\" + "TradingSetting.ini";
            if (File.Exists(iniFileTradingSymbol))
            {
                Settings = new ConfigSettings();
                Settings.readTradingINIFile(iniFileTradingSymbol);
                profit = Settings.m_overallProfit.ToString();
                loss = Settings.m_overallLoss.ToString();
                startTime2 = Settings.StartTimeNSE;
                DateTime start = DateTime.ParseExact(startTime2, "HH:mm:ss",CultureInfo.InvariantCulture);
                startTimePicker.Value = start;
                //  endTime2 = Settings.EndTime;

                TimeLimitToPlaceOrder2 = Settings.TimeLimitToPlaceOrder;
                DateTime Place = DateTime.ParseExact(TimeLimitToPlaceOrder2, "HH:mm:ss", CultureInfo.InvariantCulture);
                TTPOTimePicker.Value = Place;

                TimeForExitAllOpenPosition2 = Settings.ExitTimeNSE;
                DateTime exit = DateTime.ParseExact(TimeForExitAllOpenPosition2, "HH:mm:ss", CultureInfo.InvariantCulture);
                TOEOTimePicker.Value = exit;

                txtProfit.Text = profit;
                txtLoss.Text = loss;
                // txtEndTime.Text = endTime2;
                calculateProfitLossVal = Settings.isCalculateProfitLoss;
                storeDataMysqlDBVal = Settings.storeDataMysqlDB;
                storeDataSqliteDBVal = Settings.storeDataSqliteDB;
                storeDataCSVVal = Settings.storeDataCSV;
                historicalDataInMysqlDBVal = Settings.historicalDataInMysqlDB;
                endTime2 = Settings.EndTimeNSE;
                DateTime end = DateTime.ParseExact(endTime2, "HH:mm:ss", CultureInfo.InvariantCulture);
                endTimePicker.Value = end;

                startTimeNFO2 = Settings.StartTimeNFO;
                DateTime startTimeNFO = DateTime.ParseExact(startTimeNFO2, "HH:mm:ss", CultureInfo.InvariantCulture);
                startTimeNFOTimePicker.Value = startTimeNFO;

                startTimeMCX2 = Settings.StartTimeMCX;
                DateTime startTimeMCX = DateTime.ParseExact(startTimeMCX2, "HH:mm:ss", CultureInfo.InvariantCulture);
                startTimeMCXTimePicker.Value = startTimeMCX;
                //IRDSPM::Pratiksha::14-08-2020::For newly added variables
                if (calculateProfitLossVal == true)
                {
                    radioButton1.Checked = true;
                    txtProfit.Enabled = true;
                    txtLoss.Enabled = true;
                }
                else
                {
                    radioButton2.Checked = true;
                    txtProfit.Enabled = false;
                    txtLoss.Enabled = false;
                }

                if (storeDataMysqlDBVal == true)
                {
                    sdimdTrue.Checked = true;
                }
                else
                {
                    sdimdFalse.Checked = true;
                }

                if (storeDataSqliteDBVal == true)
                {
                    sdisdTrue.Checked = true;
                }
                else
                {
                    sdisdFalse.Checked = true;
                }

                if (storeDataCSVVal == true)
                {
                    sdcTrue.Checked = true;
                }
                else
                {
                    sdcFalse.Checked = true;
                }
                if (historicalDataInMysqlDBVal == true)
                {
                    hdimdTrue.Checked = true;
                }
                else
                {
                    hdimdFalse.Checked = true;
                }
            }
            else
            {
                MessageBox.Show("INI file does not exit!!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void set_Click(object sender, EventArgs e)
        {
            string key_val;
            string value_value;
            string[] key = new string[13];
            string[] value = new string[13];
            key[0] = startTime.Name;
            key[1] = startTimeNFO.Name;
            key[2] = startTimeMCX.Name;
            key[3] = TimeLimitToPlaceOrder.Name;
            key[4] = TimeForExitAllOpenPosition.Name;
            key[5] = endTime.Name;
            key[6] = overallProfit.Name;
            key[7] = overallLoss.Name;
            //key[8] = historicalDataInMysqlDB.Name;
            key[8] = "calculateProfitLoss";
            //key[9] = storeDataMysqlDB.Name;
            //key[10] = storeDataSqliteDB.Name;
            //key[11] = storeDataCSV.Name;


            value[0] = startTimePicker.Text;
            value[1] = startTimeNFOTimePicker.Text;
            value[2] = startTimeMCXTimePicker.Text;
            value[3] = TTPOTimePicker.Text;
            value[4] = TOEOTimePicker.Text;
            value[5] = endTimePicker.Text;
            value[6] = txtProfit.Text;
            value[7] = txtLoss.Text;
            if (radioButton1.Checked)
            {
                value[8] = "true";
            }
            else
            {
                value[8] = "false";
            }

            //if (sdimdTrue.Checked)
            //{
            //    value[9] = "true";
            //}
            //else
            //{
            //    value[9] = "false";
            //}


            //if (sdisdTrue.Checked)
            //{
            //    value[10] = "true";
            //}
            //else
            //{
            //    value[10] = "false";
            //}

            //if (sdcTrue.Checked)
            //{
            //    value[11] = "true";
            //}
            //else
            //{
            //    value[11] = "false";
            //}

            //if (hdimdTrue.Checked)
            //{
            //    value[8] = "true";
            //}
            //else
            //{
            //    value[8] = "false";
            //}
            for (int i = 0; i < 9; i++)
            {
                key_val = key[i];
                value_value = value[i];
                Settings.writeINIFile("TRADINGSETTING", key_val, value_value);
            }
            m_objKiteConnectWrapper.isReloadTradingSettingINI();
            string message = "Saved changes successfully!!!";
            //IRDSPM::Pratiksha::29-07-2020::Change the title
            string title = "IRDS Algo Trader";
            MessageBoxButtons buttons = MessageBoxButtons.OK;
            DialogResult dialog = MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
            if (dialog == DialogResult.OK)
            {
                this.Close();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            txtProfit.Text = "";
            txtLoss.Text = "";
            startTimePicker.Text = "";
            //txtEndTime.Text = "";
            TTPOTimePicker.Text = "";
            TOEOTimePicker.Text = "";
        }

        //sanika::14-sep-2020::changed to dyanamic and added try-catch
        public void LoadKiteConnectObject(dynamic kiteConnectWrapper)
        {
            try
            {
                m_objKiteConnectWrapper = kiteConnectWrapper;
            }
            catch(Exception e)
            {

            }
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            txtLoss.Enabled = true;
            txtProfit.Enabled = true;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            txtLoss.Enabled = false;
            txtProfit.Enabled = false;
        }
    }
}
