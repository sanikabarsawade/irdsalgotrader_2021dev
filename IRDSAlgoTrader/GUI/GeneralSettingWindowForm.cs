﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IRDSAlgoOMS.GUI
{
    public partial class GeneralSettingWindowForm : Form
    {
        public GeneralSettingWindowForm(dynamic m_ObjkiteConnectWrapper, string title)
        {
            this.TopMost = true;
            InitializeComponent(this, m_ObjkiteConnectWrapper, title);
            this.Load += GeneralSettingWindowForm_Load;
        }

        private void GeneralSettingWindowForm_Load(object sender, EventArgs e)
        {
            this.TopMost = false;
        }
    }
}
