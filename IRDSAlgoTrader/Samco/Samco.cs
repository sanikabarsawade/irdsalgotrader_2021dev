﻿using IRDSAlgoOMS.Zerodha;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.SQLite;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace IRDSAlgoOMS
{
    public class Samco
    {
        // instance of ticker
        static TickerSamco ticker;
        public bool bOnExpiry = false;
        public string apiKey;
        public string apiSecret;
        public string username;
        public string password;
        public string root;
        public string login;
        public string scripts;
        public string yob;
        string[] value;
        public static string volumeLimit = "";

        public string accessToken = "t4qslbmicuz8newegffp5s2jxvds58e8";
        // intialize the token
        public string MyPublicToken = "t4qslbmicuz8newegffp5s2jxvds58e8";

        public string m_accessTokenDownloadHistoricalData = "";
        public string m_MyPublicTokenDownloadHistoricalData = "";
        public string apiKeyDownloadHistoricalData;
        public string apiSecretDownloadHistoricalData;
        public string usernameDownloadHistoricalData;
        public string passwordDownloadHistoricalData;
        public string rootDownloadHistoricalData;
        public string loginDownloadHistoricalData;
        public string PinDownloadHistoricalData;

        //IRDS::Bhagyashri::26-Mar-2018::Empty string to get request token
        public string requestToken = "";
        //IRDS::Bhagyashri::27-Mar-2018::Manual Login
        public bool manualLogin = false;

        private OrderStoreSamco m_GlobalOrderStore;
        public bool debug;
        public WebProxy proxy;
        public int timeout;
        public Action sessionHook;
        public bool enableLogging;

        //IRDS::03-Jul-2020::Jyoti::added for database settings from ini file
        public string dbServer = "";
        public string dbUserid = "";
        public string dbPassword = "";
        public string databaseName = "";
        public Dictionary<string, List<string>> FinalZerodhaTickData = new Dictionary<string, List<string>>();
        //public Dictionary<string, DateTime> TickStartTime = new Dictionary<string, DateTime>();

        public static Logger loggerClient;
        private static readonly object databaselock = new object();

        //IRDS::29-Jul-2020::Jyoti::added for queue changes
        //IRDS::04-August-2020::Sandip::Change concurrent queue
        ConcurrentQueue<Tick> m_queueTick = new ConcurrentQueue<Tick>();

        Thread m_SubscribeTokens = null;
        bool isStopSubscribeTokenThread = false;
        public struct Quote1
        {

        }
        public Dictionary<string, Quote> quotes = new Dictionary<string, Quote>();
        public Dictionary<UInt32, RealTimeData> DictRealTimeData = new Dictionary<UInt32, RealTimeData>();

        private System.Collections.Concurrent.BlockingCollection<Tick> _Queue;
        Task[] _sessionStartTask = new Task[1];
        public bool bolAsyncFinish = false;


        //IRDS::Sanika::26-Jun-2019::Added for real time data from kite      


        public static DateTime Start;
        public static DateTime StartNSE;
        public static DateTime StartNFO;
        public static DateTime StartMCX;
        public static DateTime StartCDS;
        public static DateTime EndTime;
        public static DateTime TmpStart;
        public static DateTime End;
        public static string Interval = "";
        public static string EndInterval = "";
        public static DateTime TickEnd;
        public static DateTime _1MinTickEnd;
        public static DateTime FirstTickEnd;
        public bool LoginFlag = false;
        public static bool VolumeFlag = false;
        public List<string> RealTimeSymbols = new List<string>();
        public List<string> TradingSymbol = new List<string>();
        public ConcurrentDictionary<string, List<string>> ZerodhaData = new ConcurrentDictionary<string, List<string>>();
        //public ConcurrentDictionary<string, List<string>> ZerodhaDataFromCSV = new ConcurrentDictionary<string, List<string>>();
        public Dictionary<string, List<string>> ZerodhaDatatmp;
        public Dictionary<string, List<string>> FinalZerodhaData = new Dictionary<string, List<string>>();
        public Dictionary<string, List<string>> TmpZerodhaData;// = new Dictionary<string, List<string>>();
        public Dictionary<string, string> SymbolList = new Dictionary<string, string>();
        public Dictionary<string, string> SymbolListWithFutureName = new Dictionary<string, string>();
        //public Dictionary<string, string> LastTradedPrice = new Dictionary<string, string>();
        //public Dictionary<string, string> BidPrice = new Dictionary<string, string>();
        //public Dictionary<string, string> AskPrice = new Dictionary<string, string>();
        //public Dictionary<string, string> Volume = new Dictionary<string, string>();
        //public Dictionary<string, string> Close = new Dictionary<string, string>();
        public Dictionary<string, string> LastTickTime = new Dictionary<string, string>();
        public List<string> TmpList;
        SQLiteConnection Sql_conFetchSymbolName;
        SQLiteConnection Sql_conTableName;
        SQLiteConnection Sql_Datacon;
        SQLiteConnection Sql_conForCreateandreadTable;

        SQLiteDataReader Sql_read;
        SQLiteTransaction Tr;
        public int orderCounter = 1;
        public static bool DownloadDataFlag = false;
        public static string FilePath = "";
        public List<string> ListOfSymbolsForStartValues = new List<string>();
        public List<string> ListStartOpenValues = new List<string>();
        public List<string> ListForFirstDataFrom5min = new List<string>();
        public Dictionary<string, string> OpenValues = new Dictionary<string, string>();
        public Dictionary<string, string> TmpOpenValues = new Dictionary<string, string>();
        public Dictionary<string, List<string>> dictionaryForCloseValues = new Dictionary<string, List<string>>();
        public Dictionary<string, List<string>> dictionaryForLastTickTime = new Dictionary<string, List<string>>();
        public Dictionary<string, string> dictionaryForInitialVolume = new Dictionary<string, string>();
        public List<string> listOfNSESymbols = new List<string>();
        public List<string> listOfNFOSymbols = new List<string>();
        public List<string> listOfMCXSymbols = new List<string>();
        //public Tick tick;
        public string tickCurrentTime = "";
        public string tickCurrent = "";
        public DateTime endTimeForTick;
        public string pathFile = Directory.GetCurrentDirectory();
        Logger logger;
        public string DBName = "samcodata.db";
        MySQLConnectZerodha m_Mysql;
        bool m_isConnected = false;
        bool m_isConnectedToSqlite = false;
        int m_previousTradingSymbolListCount = 0;
        public string m_ExceptionError = "";

        List<string> m_NSESymbolsInstruements = new List<string>();
        List<string> m_NFOSymbolsInstruements = new List<string>();
        List<string> m_MCXSymbolsInstruements = new List<string>();
        List<string> m_CDSSymbolsInstruements = new List<string>();

        public static DateTime EndTimeNSE;
        public static DateTime EndTimeNFO;
        public static DateTime EndTimeMCX;
        //sanika::2-dec-2020::Added for cds
        public static DateTime EndTimeCDS;

        //mysql flag by default false
        bool m_useMysql = true;
        bool m_useSqlite = false;
        bool m_useCSV = false;

        SqliteConnectZerodha sqliteConnectZerodha = null;

        //sanika::14-Dec-2020::Changed name of class because of crash issue
        TickDataSamco m_TickData = null;

        bool m_StopThread = false;

        private readonly Dictionary<string, string> _routes = new Dictionary<string, string>
        {
            ["login"] = "/login",
            ["quote"] = "/quote/getQuote?symbolName=IDEA",
            ["orders.place"] = "/order/placeOrder",
            ["orders.placeBO"] = "/order/placeOrderBO",
            ["orders.placeCO"] = "/order/placeOrderCO",
            ["orders.cancel"] = "/order/cancelOrder{orderNumber}",
            ["orders.getAll"] = "/order/orderBook",
            ["data.historical"] = "/history/candleData{symbolName}{fromDate}{toDate}{exchange}",
            ["data.intraday"] = "/intraday/candleData{symbolName}{fromDate}{toDate}{exchange}{interval}",
            ["positions.getAll"] = "/position/getPositions{positionType}",
            ["orders.closeBO"] = "/order/exitBO{orderNumber}",
            ["orders.closeCO"] = "/order/exitCO{orderNumber}",
            ["orders.modify"] = "/order/modifyOrder/{orderNumber}",
            ["holdings.getAll"] = "/holding/getHoldings",
            ["market.instruments"] = "https://developers.stocknote.com/doc/ScripMaster.csv",
            ["user.segment_margins"] = "/limit/getLimits",
            ["indices.quote"] = "/quote/indexQuote{indexName}"
        };
        //IRDS::04-August-2020::Sandip::new variable.
        DateTime startDateTime = DateTime.Now;

        public Samco()
        {

            ////Console.WriteLine("Kite Class Object Initialize\n");
        }

        /// <summary>
        /// Initialize a new Kite Connect client instance.
        /// </summary>
        /// <param name="APIKey">API Key issued to you</param>
        /// <param name="AccessToken">The token obtained after the login flow in exchange for the `RequestToken` . 
        /// Pre-login, this will default to None,but once you have obtained it, you should persist it in a database or session to pass 
        /// to the Kite Connect class initialisation for subsequent requests.</param>
        /// <param name="Root">API end point root. Unless you explicitly want to send API requests to a non-default endpoint, this can be ignored.</param>
        /// <param name="Debug">If set to True, will serialise and print requests and responses to stdout.</param>
        /// <param name="Timeout">Time in milliseconds for which  the API client will wait for a request to complete before it fails</param>
        /// <param name="Proxy">To set proxy for http request. Should be an object of WebProxy.</param>
        /// <param name="Pool">Number of connections to server. Client will reuse the connections if they are alive.</param>
        public Samco(ConfigSettings objConfigSettings, string AccessToken = null, string Root = null, bool Debug = false, int Timeout = 7000, WebProxy Proxy = null, int Pool = 2)
        {
            accessToken = AccessToken;
            //apiKey = APIKey;
            if (!String.IsNullOrEmpty(Root)) this.root = objConfigSettings.root;
            enableLogging = Debug;

            timeout = Timeout;
            proxy = Proxy;

            ServicePointManager.DefaultConnectionLimit = Pool;
            //apiKey = objConfigSettings.apiKey;
            //apiSecret = objConfigSettings.apiSecret;
            username = objConfigSettings.username;
            password = objConfigSettings.password;
            root = objConfigSettings.root;
            //login = objConfigSettings.login;
            yob = objConfigSettings.yob;
            if (objConfigSettings.ManualLogin == "false")
                manualLogin = false;
            else
                manualLogin = true;
            RealTimeSymbols = objConfigSettings.Symbol;
            //sanika::8-dec-2020::interval dynamic
            Interval = objConfigSettings.interval.ToString();
            string currentTime = DateTime.Now.ToString("dd-MM-yyyy");
             DateTime CurrentDateTime = DateTime.ParseExact(currentTime + " " + objConfigSettings.StartTimeNSE, "dd-MM-yyyy HH:mm:ss", null);
           // DateTime CurrentDateTime = DateTime.ParseExact(currentTime + " " + "13:31:00", "dd-MM-yyyy HH:mm:ss", null);
            StartNSE = CurrentDateTime;
            CurrentDateTime = DateTime.ParseExact(currentTime + " " + objConfigSettings.StartTimeNFO, "dd-MM-yyyy HH:mm:ss", null);
            StartNFO = CurrentDateTime;
            CurrentDateTime = DateTime.ParseExact(currentTime + " " + objConfigSettings.StartTimeMCX, "dd-MM-yyyy HH:mm:ss", null);
            StartMCX = CurrentDateTime;
            //sanika::2-dec-2020::added for cds
            CurrentDateTime = DateTime.ParseExact(currentTime + " " + objConfigSettings.StartTimeCDS, "dd-MM-yyyy HH:mm:ss", null);
            StartCDS = CurrentDateTime;
            currentTime = DateTime.Now.ToString("dd-MM-yyyy HH:mm") + ":00";
            CurrentDateTime = DateTime.ParseExact(currentTime, "dd-MM-yyyy HH:mm:ss", null);
           

            //sanika::9-dec-2020::To calculate start time as per interval
            currentTime = DateTime.Now.ToString("dd-MM-yyyy HH:mm") + ":00";
            CurrentDateTime = DateTime.ParseExact(currentTime, "dd-MM-yyyy HH:mm:ss", null);
            if (CurrentDateTime < StartNSE)
                Start = StartNSE;
            else
                Start = RoundUp(CurrentDateTime, TimeSpan.FromMinutes(Convert.ToInt32(Interval)));

            EndInterval = (Convert.ToInt32(Interval) + 1).ToString();
            End = Start.AddSeconds(60 * Convert.ToInt32(EndInterval));
            TickEnd = Start.AddSeconds(60 * Convert.ToInt32(Interval));
            FirstTickEnd = TickEnd;
            _1MinTickEnd = Start.AddSeconds(60);
            TmpStart = Start;

            //sanika::18-Nov-2020::changed end time exchange wise
            string endT = DateTime.Now.ToString("dd-MM-yyyy") + " " + objConfigSettings.EndTimeNSE;
            EndTimeNSE = DateTime.ParseExact(endT, "dd-MM-yyyy HH:mm:ss", null);
            endT = DateTime.Now.ToString("dd-MM-yyyy") + " " + objConfigSettings.EndTimeNFO;
            EndTimeNFO = DateTime.ParseExact(endT, "dd-MM-yyyy HH:mm:ss", null);
            endT = DateTime.Now.ToString("dd-MM-yyyy") + " " + objConfigSettings.EndTimeMCX;
            EndTimeMCX = DateTime.ParseExact(endT, "dd-MM-yyyy HH:mm:ss", null);
            //sanika::2-dec-2020::added for cds
            endT = DateTime.Now.ToString("dd-MM-yyyy") + " " + objConfigSettings.EndTimeCDS;
            EndTimeCDS = DateTime.ParseExact(endT, "dd-MM-yyyy HH:mm:ss", null);

           
            endTimeForTick = Start.AddSeconds(60 * Convert.ToInt32(Interval));
            m_useMysql = objConfigSettings.storeDataMysqlDB;
            m_useSqlite = objConfigSettings.storeDataSqliteDB;
            m_useCSV = objConfigSettings.storeDataCSV;
            //IRDS::03-Jul-2020::Jyoti::added for database settings from ini file
            dbServer = objConfigSettings.dbServer;
            dbUserid = objConfigSettings.dbUserid;
            dbPassword = objConfigSettings.dbPassword;
            databaseName = objConfigSettings.databaseName;

            //sanika::11-Jun-2021::Added thread to subscribed tokens
            if (m_SubscribeTokens == null)
            {
                Trace.WriteLine("Started ProcessTickDataToDatabase ");
                m_SubscribeTokens = new Thread(() => SubscribeTokens());
                m_SubscribeTokens.Start();
            }
        }

        //sanika::9-dec-2020::To calculate start time as per interval
        DateTime RoundUp(DateTime dt, TimeSpan d)
        {
            return new DateTime((dt.Ticks + d.Ticks - 1) / d.Ticks * d.Ticks, dt.Kind);
        }
        public void EnableLogging(bool _enableLogging)
        {
            enableLogging = _enableLogging;
        }

        public void initSeesion()
        {
            try
            {
                /*WebDriver webDriver = new WebDriver(manualLogin, username, password, Pin);
                webDriver.getChromeDriver(login, apiKey); //Automation login using Selenium Webdriver with c#

                //IRDS::Bhagyashri::27-Mar-2018::Check the manual login is true or false   
                if (!manualLogin)
                {
                    //string requestToken = HttpUtility.ParseQueryString(webDriver.currentUrl).Get("request_token");

                    //IRDS::Bhagyashri::26-Mar-2018::Change the code to get request token from url   
                    string reqToken = webDriver.currentUrl;
                    Uri tmp = new Uri(reqToken);
                    //Console.WriteLine("Query: {0}", tmp.Query);
                    NameValueCollection Parms = HttpUtility.ParseQueryString(tmp.Query);
                    //Console.WriteLine("Parms: {0}", Parms.Count);

                    foreach (string x in Parms.AllKeys)
                    {
                        if (x.Contains("request_token"))
                        {
                            requestToken = Parms[x];
                        }
                    }

                    //Console.Write("\nRequest Token : "+ requestToken);

                    User user = GenerateSession(requestToken, apiSecret);
                    //Console.WriteLine(Utils.JsonSerialize(user));

                    accessToken = user.AccessToken;
                    MyPublicToken = user.PublicToken;
                }
                else
                {
                    //Manually insert request token
                    Console.Write("Enter request token : ");
                    requestToken = Console.ReadLine();

                    User user = GenerateSession(requestToken, apiSecret);
                    accessToken = user.AccessToken;
                    MyPublicToken = user.PublicToken;
                }*/
                bool issendNotification = false;
                while(!(DateTime.Now > StartNSE))
                {
                    if(issendNotification == false)
                    {
                        SamcoConnectWrapper.WriteGUIlogs(SamcoConnectWrapper.m_ExceptionCounter, "Waiting to start market");
                        SamcoConnectWrapper.m_ExceptionCounter++;
                        issendNotification = true;
                    }
                    Thread.Sleep(1000);
                }

                var param = new Dictionary<string, dynamic>();
                Utils.AddIfNotNull(param, "userId", username);
                Utils.AddIfNotNull(param, "password", password);
                Utils.AddIfNotNull(param, "yob", yob);
                Dictionary<string, dynamic> response = new Dictionary<string, dynamic>();
                response = Post("login", param);

                //sanika::28-Nov-2020::Added condition for exception
                if (response.Count != 0)
                {
                    Console.WriteLine("sessionToken: " + response["sessionToken"]);
                    accessToken = response["sessionToken"];
                    LoginFlag = true;
                    writeCredentialIntoFile(accessToken);
                }
                else
                {
                    //sanika::14-dec-2020::Added to know the user login not successful 
                    m_ExceptionError = "Please check login credentials.Not get response from server!";
                    logger.LogMessage("initSeesion : Response count comes as 0", MessageType.Informational);
                    LoginFlag = false;
                }

            }
            catch (Exception e)
            {
                logger.LogMessage("initSeesion : Exception Error Message = " + e.Message, MessageType.Exception);
                m_ExceptionError = e.Message;
                LoginFlag = false;
            }
        }

        public void writeCredentialIntoFile(string accessToken)
        {
            string currentDate = DateTime.Now.ToString("M-d-yyyy");
            var path_File = Directory.GetCurrentDirectory();
            string FilePath = path_File + @"\Configuration\" + "credentials_" + username + ".txt";
            string line = currentDate + " " + username + " " + accessToken;
            using (StreamWriter streamWriter = new StreamWriter(FilePath, false))
            {
                streamWriter.WriteLine(line);
            }
        }

        /// Adds extra headers to request
        public void AddExtraHeaders(ref HttpWebRequest Req)
        {
            /*if (Assembly.GetEntryAssembly() != null)
                Req.UserAgent = "KiteConnect.Net/" + Assembly.GetEntryAssembly().GetName().Version;

            Req.Headers.Add("X-Kite-Version", "3");
            Req.Headers.Add("Authorization", "token " + apiKey + ":" + accessToken);*/
            if (accessToken != null)
            {
                Req.Headers.Add("x-session-token", accessToken);
            }

            Req.Timeout = timeout;
            if (proxy != null) Req.Proxy = proxy;

            if (enableLogging)
            {
                foreach (string header in Req.Headers.Keys)
                {
                    //Console.WriteLine("DEBUG: " + header + ": " + Req.Headers.GetValues(header)[0]);
                }
            }
        }

        /// Make an HTTP request.
        public dynamic Request(string Route, string Method, Dictionary<string, dynamic> Params = null)
        {
            if (Params == null)
                Params = new Dictionary<string, dynamic>();
            string url = "";
            if (Route != "market.instruments")
                url = root + _routes[Route];
            else
                url = _routes[Route];
            if (url.Contains("/{"))
            {
                var urlparams = Params.ToDictionary(entry => entry.Key, entry => entry.Value);
                foreach (KeyValuePair<string, dynamic> item in urlparams)
                    if (url.Contains("{" + item.Key + "}"))
                    {
                        url = url.Replace("{" + item.Key + "}", (string)item.Value);
                        //url = url.Replace((string)item.Value);
                        Params.Remove(item.Key);
                    }
            }
            else if (url.Contains("{"))
            {
                int index = url.IndexOf("{");
                url = url.Insert(index, "?");
                var urlparams = Params.ToDictionary(entry => entry.Key, entry => entry.Value);

                foreach (KeyValuePair<string, dynamic> item in urlparams)
                    if (url.Contains("{" + item.Key + "}"))
                    {
                        //url = url.Replace("{" + item.Key + "}", (string)item.Value);
                        url = url.Replace("{" + item.Key + "}", item.Key + "=" + (string)item.Value + "&");
                        Params.Remove(item.Key);
                    }
                url = url.Remove(url.Length - 1);
            }

            //if (!Params.ContainsKey("api_key"))
            //    Params.Add("api_key", _apiKey);

            //if (!Params.ContainsKey("access_token") && !String.IsNullOrEmpty(_accessToken))
            //    Params.Add("access_token", _accessToken);

            HttpWebRequest request;
            //string paramString = String.Join("&", Params.Select(x => Utils.BuildParam(x.Key, x.Value)));
            var entries = Params.Select(d => string.Format("\"{0}\": \"{1}\"", d.Key, string.Join(", ", d.Value)));
            string paramString = "";
            if (entries.ToList().Count > 0)
            {
                paramString = "{" + string.Join(",", entries) + "}";
            }

            if (Method == "POST" || Method == "PUT")
            {
                request = (HttpWebRequest)WebRequest.Create(url);
                request.AllowAutoRedirect = true;
                request.Method = Method;
                //Commented by Jyoti due to {"serverTime":"15/07/20 19:23:47","statusMessage":"HTTP 415 Unsupported Media Type"}
                //request.ContentType = "application/x-www-form-urlencoded";
                request.ContentType = "application/json";
                request.ContentLength = paramString.Length;
                //if (enableLogging) Console.WriteLine("DEBUG: " + Method + " " + url + "\n" + paramString);
                AddExtraHeaders(ref request);

                using (Stream webStream = request.GetRequestStream())
                using (StreamWriter requestWriter = new StreamWriter(webStream))
                    requestWriter.Write(paramString);
            }
            else
            {
                if (paramString != "")
                {
                    url = url + "?" + paramString;
                }
                request = (HttpWebRequest)WebRequest.Create(url);
                request.AllowAutoRedirect = true;
                request.Method = Method;
                //if (enableLogging) Console.WriteLine("DEBUG: " + Method + " " + url + "?" + paramString);
                AddExtraHeaders(ref request);
            }

            WebResponse webResponse;
            try
            {
                webResponse = request.GetResponse();
            }
            catch (WebException e)
            {
                if (e.Response == null)
                    throw e;

                webResponse = e.Response;
                //System.Diagnostics.Debug.WriteLine(e.ToStringg());
                WebResponse errResp = e.Response;
                using (Stream respStream = errResp.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(respStream);
                    string text = reader.ReadToEnd();
                    //"{\"serverTime\":\"13/07/20 16:12:34\",\"statusMessage\":\"HTTP 405 Method Not Allowed\"}"
                    Trace.WriteLine(Route + " :: " + text);
                    WriteUniquelogs("SamcoLogs", text, MessageType.Exception);
                    if (text.Contains("Session Expired"))
                    {
                        try
                        {
                            initSeesion();
                            logger.LogMessage("Forceful Relogin initSeesion successful.", MessageType.Informational);

                            //IRDS::9-oct-2019::Sanika::Added if login failed then also call this two methods
                            SetAccessToken(accessToken);
                            logger.LogMessage("SetAccessToken successful.", MessageType.Informational);

                            // initialize ticker
                            if (ticker != null)
                            {
                                ticker = new TickerSamco();
                            }
                            initTicker();

                            logger.LogMessage("initTicker successful.", MessageType.Informational);
                        }
                        catch (Exception ex)
                        {
                            logger.LogMessage("Forceful Relogin initSeesion unsuccessful.", MessageType.Exception);
                        }
                    }
                }
                return new Dictionary<string, dynamic>();
            }

            using (Stream webStream = webResponse.GetResponseStream())
            {
                using (StreamReader responseReader = new StreamReader(webStream))
                {
                    string response = responseReader.ReadToEnd();
                    //if (enableLogging) Console.WriteLine("DEBUG: " + (int)((HttpWebResponse)webResponse).StatusCode + " " + response + "\n");
                    Trace.WriteLine("response: " + response);
                    //WriteUniquelogs("SamcoLogs", response, MessageType.Informational);
                    HttpStatusCode status = ((HttpWebResponse)webResponse).StatusCode;

                    if (webResponse.ContentType == "application/json")
                    {
                        Dictionary<string, dynamic> responseDictionary = Utils.JsonDeserialize(response);

                        if (status != HttpStatusCode.OK)
                        {
                            string errorType = "GeneralException";
                            string message = "";

                            if (responseDictionary.ContainsKey("error_type"))
                                errorType = responseDictionary["error_type"];

                            if (responseDictionary.ContainsKey("message"))
                                message = responseDictionary["message"];

                            switch (errorType)
                            {
                                case "GeneralException": throw new GeneralException(message, status);
                                case "TokenException":
                                    {
                                        //sessionHook?.Invoke();
                                        throw new TokenException(message, status);
                                    }
                                case "PermissionException": throw new PermissionException(message, status);
                                case "OrderException": throw new OrderException(message, status);
                                case "InputException": throw new InputException(message, status);
                                case "DataException": throw new DataException(message, status);
                                case "NetworkException": throw new NetworkException(message, status);
                                default: throw new GeneralException(message, status);
                            }
                        }

                        return responseDictionary;
                    }
                    else if ((webResponse.ContentType == "text/csv") || (webResponse.ContentType == "application/octet-stream"))
                        return Utils.ParseCSV(response);
                    else
                        throw new DataException("Unexpected content type " + webResponse.ContentType + " " + response);
                }
            }
        }

        // Get the login url to which a user should be redirected to initiate the login flow.
        public string GetLoginURL(string login, string apiKey)
        {
            return String.Format("{0}?api_key={1}", login, apiKey);
        }

        /// Do the token exchange with the `RequestToken` obtained after the login flow,
        /// and retrieve the `AccessToken` required for all subsequent requests.The
        /// response contains not just the `AccessToken`, but metadata for
        /// the user who has authenticated.
        public User GenerateSession(string RequestToken, string AppSecret)
        {
            string checksum = Utils.SHA256(apiKey + RequestToken + AppSecret);

            var param = new Dictionary<string, dynamic>
            {
                {"api_key", apiKey},
                {"request_token", RequestToken},
                {"checksum", checksum}
            };

            var userData = Post("api.token", param);

            return new User(userData);
        }

        public void SetAccessToken(string AccessToken)
        {
            accessToken = AccessToken;
            //Console.WriteLine("Access Token : " + AccessToken);
        }

        /// Helper function to add parameter to the request only if it is not null or empty
        private void AddIfNotNull(Dictionary<string, dynamic> Params, string Key, string Value)
        {
            if (!String.IsNullOrEmpty(Value))
                Params.Add(Key, Value);
        }

        /// Alias for sending a DELETE request.
        private dynamic Delete(string Route, Dictionary<string, dynamic> Params = null)
        {
            return Request(Route, "DELETE", Params);
        }

        /// Alias for sending a GET request.
        public dynamic Get(string Route, Dictionary<string, dynamic> Params = null)
        {
            return Request(Route, "GET", Params);
        }

        /// Alias for sending a POST request.
        public dynamic Post(string Route, Dictionary<string, dynamic> Params = null)
        {
            return Request(Route, "POST", Params);
        }

        /// Retrieve the list of market instruments available to trade.
        public List<InstrumentSamco> GetInstruments(string Exchange = null)
        {
            List<Dictionary<string, dynamic>> instrumentsData;

            instrumentsData = Get("market.instruments");
            List<InstrumentSamco> instruments = new List<InstrumentSamco>();

            foreach (Dictionary<string, dynamic> item in instrumentsData)
            {
                instruments.Add(new InstrumentSamco(item));

            }
            return instruments;
        }

        /// <summary>
        /// Retrieve the list of positions.
        /// </summary>
        /// <returns>Day and net positions.</returns>
        public PositionResponseSamco GetPositions()
        {
            //net pending
            //var positionsdata = Get("portfolio.positions");
            //return new PositionResponseSamco(positionsdata["data"], "DAY");
            //string positionType = "NET";
            var param = new Dictionary<string, dynamic>();
            Utils.AddIfNotNull(param, "positionType", "NET");
            Dictionary<string, dynamic> positionsDataNet = Get("positions.getAll", param);
            param = new Dictionary<string, dynamic>();
            Utils.AddIfNotNull(param, "positionType", "DAY");
            //Dictionary<string, dynamic> positionsDataDay = Get("positions.getAll", param);
            Dictionary<string, dynamic> positionsDataDay = new Dictionary<string, dynamic>();
            return new PositionResponseSamco(positionsDataNet, positionsDataDay);
        }

        /// Gets the collection of orders from the orderbook.
        public List<Order> GetOrders()
        {
            var ordersData = Get("orders");

            List<Order> orders = new List<Order>();

            foreach (Dictionary<string, dynamic> item in ordersData["data"])
                orders.Add(new Order(item));

            return orders;
        }

        /// Gets information about given OrderId.

        public List<Order> GetOrderHistory(string OrderId)
        {
            var param = new Dictionary<string, dynamic>();
            param.Add("order_id", OrderId);

            var orderData = Get("orders.history", param);

            List<Order> orderhistory = new List<Order>();

            foreach (Dictionary<string, dynamic> item in orderData["data"])
                orderhistory.Add(new Order(item));

            return orderhistory;
        }

        //IRDS::Bhagyashri::27-Apr-2018::Implemented the GetOrderHistoryBySymbols() to get symbol wise latestOrder
        public List<OrderSamco> GetOrderHistoryBySymbols(string tradingsymbol)
        {
            var param = new Dictionary<string, dynamic>();
            param.Add("tradingsymbol", tradingsymbol);

            var orderData = Get("orders", param);

            List<OrderSamco> orderhistorybysymbol = new List<OrderSamco>();
            List<OrderSamco> previousOrderBySymbol = new List<OrderSamco>();

            foreach (Dictionary<string, dynamic> item in orderData["data"])
                orderhistorybysymbol.Add(new OrderSamco(item));

            previousOrderBySymbol = orderhistorybysymbol.Where(o => o.Tradingsymbol == tradingsymbol).ToList();

            return previousOrderBySymbol;
        }

        public List<OrderSamco> GetOrderHistory()
        {
            var orderData = Get("orders.getAll");

            List<OrderSamco> orderhistorybysymbol = new List<OrderSamco>();
            List<OrderSamco> previousOrderBySymbol = new List<OrderSamco>();

            if (orderData.Count > 0)
            {
                foreach (Dictionary<string, dynamic> item in orderData["orderBookDetails"])
                    orderhistorybysymbol.Add(new OrderSamco(item));
            }
            //previousOrderBySymbol = orderhistorybysymbol.Where(o => o.Tradingsymbol == tradingsymbol).ToList();

            return orderhistorybysymbol;
        }

        //IRDS::Bhagyashri::7-May-2018::Implemented the kite method for Cancel Order
        /// <summary>
        /// Cancel an order
        /// </summary>
        /// <param name="OrderId">Id of the order to be cancelled</param>
        /// <param name="Variety">You can place orders of varieties; regular orders, after market orders, cover orders etc. </param>
        /// <param name="ParentOrderId">Id of the parent order (obtained from the /orders call) as BO is a multi-legged order</param>
        /// <returns>Json response in the form of nested string dictionary.</returns>
        public Dictionary<string, dynamic> CancelOrder(string OrderId, string Variety = Constants.VARIETY_REGULAR, string ParentOrderId = null)
        {
            var param = new Dictionary<string, dynamic>();
            param.Add("orderNumber", OrderId);
            Dictionary<string, dynamic> cancelData = Delete("orders.cancel", param);
            return cancelData;
        }

        /// Place an order
        public Dictionary<string, dynamic> PlaceOrder(
             string Exchange,
             string TradingSymbol,
             string TransactionType,
             int Quantity,
             decimal? Price = null,
             string Product = null,
             string OrderType = null,
             string Validity = null,
             int? DisclosedQuantity = null,
             decimal? TriggerPrice = null,
             decimal? SquareOffValue = null,
             decimal? StoplossValue = null,
             decimal? TrailingStoploss = null,
             string Variety = Constants.VARIETY_REGULAR,
             string Tag = "")
        {
            var param = new Dictionary<string, dynamic>();

            Utils.AddIfNotNull(param, "symbolName", TradingSymbol);
            Utils.AddIfNotNull(param, "exchange", Exchange);
            Utils.AddIfNotNull(param, "transactionType", TransactionType);
            Utils.AddIfNotNull(param, "orderType", OrderType);
            Utils.AddIfNotNull(param, "quantity", Quantity.ToString());
            Utils.AddIfNotNull(param, "disclosedQuantity", DisclosedQuantity.ToString());
            Utils.AddIfNotNull(param, "price", Price.ToString());
            Utils.AddIfNotNull(param, "orderValidity", Validity);
            Utils.AddIfNotNull(param, "productType", Product);
            Utils.AddIfNotNull(param, "triggerPrice", TriggerPrice.ToString());
            Utils.AddIfNotNull(param, "squareOffValue", SquareOffValue.ToString());
            Utils.AddIfNotNull(param, "stopLossValue", StoplossValue.ToString());
            /*Utils.AddIfNotNull(param, "trailing_stoploss", TrailingStoploss.ToString());
            Utils.AddIfNotNull(param, "variety", Variety);
            Utils.AddIfNotNull(param, "tag", Tag);*/

            string route = "orders.place";
            if (Product == "BO")
            {
                route = "orders.placeBO";
            }
            else if (Product == "CO")
            {
                route = "orders.placeCO";
            }
            return Post(route, param);
        }

        /// <summary>
        /// Modify an open position's product type.
        /// </summary>
        /// <param name="Exchange">Name of the exchange</param>
        /// <param name="TradingSymbol">Tradingsymbol of the instrument</param>
        /// <param name="TransactionType">BUY or SELL</param>
        /// <param name="PositionType">overnight or day</param>
        /// <param name="Quantity">Quantity to convert</param>
        /// <param name="OldProduct">Existing margin product of the position</param>
        /// <param name="NewProduct">Margin product to convert to</param>
        /// <returns>Json response in the form of nested string dictionary.</returns>
        public Dictionary<string, dynamic> ConvertPosition(
            string Exchange,
            string TradingSymbol,
            string TransactionType,
            string PositionType,
            int? Quantity,
            string OldProduct,
            string NewProduct)
        {
            var param = new Dictionary<string, dynamic>();

            Utils.AddIfNotNull(param, "exchange", Exchange);
            Utils.AddIfNotNull(param, "tradingsymbol", TradingSymbol);
            Utils.AddIfNotNull(param, "transaction_type", TransactionType);
            Utils.AddIfNotNull(param, "position_type", PositionType);
            Utils.AddIfNotNull(param, "quantity", Quantity.ToString());
            Utils.AddIfNotNull(param, "old_product", OldProduct);
            Utils.AddIfNotNull(param, "new_product", NewProduct);

            return Put("portfolio.positions.modify", param);
        }


        //IRDS::16-July-2018::Bhagyashri::Added kiteConnect API method
        /// <summary>
        /// Modify an open order.
        /// </summary>
        /// <param name="OrderId">Id of the order to be modified</param>
        /// <param name="ParentOrderId">Id of the parent order (obtained from the /orders call) as BO is a multi-legged order</param>
        /// <param name="Exchange">Name of the exchange</param>
        /// <param name="TradingSymbol">Tradingsymbol of the instrument</param>
        /// <param name="TransactionType">BUY or SELL</param>
        /// <param name="Quantity">Quantity to transact</param>
        /// <param name="Price">For LIMIT orders</param>
        /// <param name="Product">Margin product applied to the order (margin is blocked based on this)</param>
        /// <param name="OrderType">Order type (MARKET, LIMIT etc.)</param>
        /// <param name="Validity">Order validity</param>
        /// <param name="DisclosedQuantity">Quantity to disclose publicly (for equity trades)</param>
        /// <param name="TriggerPrice">For SL, SL-M etc.</param>
        /// <param name="Variety">You can place orders of varieties; regular orders, after market orders, cover orders etc. </param>
        /// <returns>Json response in the form of nested string dictionary.</returns>
        public Dictionary<string, dynamic> ModifyOrder(
            string OrderId,
            string ParentOrderId = null,
            string Exchange = null,
            string TradingSymbol = null,
            string TransactionType = null,
            string Quantity = null,
            decimal? Price = null,
            string Product = null,
            string OrderType = null,
            string Validity = Constants.VALIDITY_DAY,
            int? DisclosedQuantity = null,
            decimal? TriggerPrice = null,
            string Variety = Constants.VARIETY_REGULAR)
        {
            var param = new Dictionary<string, dynamic>();

            string VarietyString = Variety;
            string ProductString = Product;

            if ((ProductString == "bo" || ProductString == "co") && VarietyString != ProductString)
                throw new Exception(String.Format("Invalid variety. It should be: {0}", ProductString));

            Utils.AddIfNotNull(param, "orderNumber", OrderId);
            Utils.AddIfNotNull(param, "parentOrderId", ParentOrderId);
            Utils.AddIfNotNull(param, "triggerPrice", TriggerPrice.ToString());
            //Utils.AddIfNotNull(param, "variety", Variety);

            if (ProductString == "bo")
            {
                Utils.AddIfNotNull(param, "quantity", Quantity);
                Utils.AddIfNotNull(param, "price", Price.ToString());
                Utils.AddIfNotNull(param, "disclosedQuantity", DisclosedQuantity.ToString());
            }
            else if (ProductString != "co")
            {
                Utils.AddIfNotNull(param, "exchange", Exchange);
                Utils.AddIfNotNull(param, "tradingSymbol", TradingSymbol);
                Utils.AddIfNotNull(param, "transactionType", TransactionType);
                Utils.AddIfNotNull(param, "quantity", Quantity);
                Utils.AddIfNotNull(param, "price", Price.ToString());
                Utils.AddIfNotNull(param, "productType", Product);
                Utils.AddIfNotNull(param, "orderType", OrderType);
                Utils.AddIfNotNull(param, "orderValidity", Validity);
                Utils.AddIfNotNull(param, "disclosedQuantity", DisclosedQuantity.ToString());
            }
            else
            {

            }

            return Put("orders.modify", param);
        }

        /// <summary>
        /// Retrieve LTP of upto 200 instruments
        /// </summary>
        /// <param name="InstrumentId">Indentification of instrument in the form of EXCHANGE:TRADINGSYMBOL (eg: NSE:INFY) or InstrumentToken (eg: 408065)</param>
        /// <returns>Dictionary with InstrumentId as key and LTP as value.</returns>
        public Dictionary<string, LTP> GetLTP(string[] InstrumentId)
        {
            var param = new Dictionary<string, dynamic>();
            param.Add("i", InstrumentId);
            Dictionary<string, dynamic> ltpData = Get("market.ltp", param)["data"];

            Dictionary<string, LTP> ltps = new Dictionary<string, LTP>();
            foreach (string item in ltpData.Keys)
                ltps.Add(item, new LTP(ltpData[item]));

            return ltps;
        }

        public decimal get_LTP_Price(string symbol)
        {
            decimal LTP_Price = 0.0m;
            Dictionary<string, LTP> get_ltps = new Dictionary<string, LTP>();

            get_ltps = GetLTP(InstrumentId: new string[] { symbol });
            Console.WriteLine(Utils.JsonSerialize(get_ltps));
            if (get_ltps.ContainsKey(symbol))
            {
                foreach (var item in get_ltps.Values)
                {
                    LTP_Price = item.LastPrice;
                }
                return LTP_Price;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Alias for sending a PUT request.
        /// </summary>
        /// <param name="Route">URL route of API</param>
        /// <param name="Params">Additional paramerters</param>
        /// <returns>Varies according to API endpoint</returns>
        private dynamic Put(string Route, Dictionary<string, dynamic> Params = null)
        {
            return Request(Route, "PUT", Params);
        }

        // set a callback hook for session
        public void SetSessionExpiryHook(Action Method)
        {
            Console.WriteLine("Inside SetSessionExpiryHook");
            sessionHook = Method;
        }

        //sanika::14-Dec-2020::Changed name of class because of crash issue
        public void initializeLoggerObject(Logger logger, OrderStoreSamco orderStore, TickDataSamco tickData)
        {
            this.logger = logger;
            this.m_GlobalOrderStore = orderStore;
            this.m_TickData = tickData;
        }

        // Ticker Initiated
        Thread m_addInTableThread = null;
        public void initTicker()
        {
            try
            {
                //IRDS::03-Jul-2020::Jyoti::added for queue changes
                //IRDS::04-August-2020::Sandip::lock to create thread.
                lock (this)
                {
                    if (m_useMysql)
                    {
                        if (m_Mysql == null)
                        {
                            m_Mysql = new MySQLConnectZerodha();
                            //IRDS::03-Jul-2020::Jyoti::added for database settings from ini file
                            if (m_Mysql.Connect(logger, dbServer, dbUserid, dbPassword, databaseName))
                            {
                                m_isConnected = true;
                            }
                        }

                    }

                    //sqlite
                    //sanika::14-sep-2020::wrote seperate class for sqlite db
                    if (m_useSqlite)
                    {
                        if (sqliteConnectZerodha == null)
                        {
                            sqliteConnectZerodha = new SqliteConnectZerodha();
                        }
                        if (sqliteConnectZerodha.Connect("samcodata.db"))
                        {
                            m_isConnectedToSqlite = true;
                        }
                    }
                }

                string symbolName = "";
                foreach (var symbol in RealTimeSymbols)
                {
                    symbolName = symbol;

                    if (symbolName.Split('.')[1] == Constants.EXCHANGE_NSE && (!listOfNSESymbols.Contains(symbolName.Split('.')[0])))
                    {
                        listOfNSESymbols.Add(symbolName.Split('.')[0]);
                    }
                    else if (symbolName.Split('.')[1] == Constants.EXCHANGE_NFO && (!listOfNFOSymbols.Contains(symbolName.Split('.')[0])))
                    {
                        listOfNFOSymbols.Add(symbolName.Split('.')[0]);
                    }
                    else if (symbolName.Split('.')[1] == Constants.EXCHANGE_MCX && (!listOfMCXSymbols.Contains(symbolName.Split('.')[0])))
                    {
                        listOfMCXSymbols.Add(symbolName.Split('.')[0]);
                    }
                }

                if (TradingSymbol.Count > 0)
                {
                    foreach (var symbol in TradingSymbol)
                    {
                        symbolName = symbol;

                        if (symbolName.Split('.')[1] == Constants.EXCHANGE_NSE && (!listOfNSESymbols.Contains(symbolName.Split('.')[0])))
                        {
                            listOfNSESymbols.Add(symbolName.Split('.')[0]);
                        }
                        else if (symbolName.Split('.')[1] == Constants.EXCHANGE_NFO && (!listOfNFOSymbols.Contains(symbolName.Split('.')[0])))
                        {
                            listOfNFOSymbols.Add(symbolName.Split('.')[0]);
                        }
                        else if (symbolName.Split('.')[1] == Constants.EXCHANGE_MCX && (!listOfMCXSymbols.Contains(symbolName.Split('.')[0])))
                        {
                            listOfMCXSymbols.Add(symbolName.Split('.')[0]);
                        }
                    }
                }

                //get token from symbol name
                string instrumentToken = "";
                string[] instrumentList = new string[listOfNSESymbols.Count() + listOfNFOSymbols.Count() + listOfMCXSymbols.Count()];
                if (listOfNSESymbols.Count > 0)
                {
                    for (int i = 0; i < listOfNSESymbols.Count(); i++)
                    {
                        //sanika::7-Dec-2020::Added to get token of indices 
                        if (listOfNSESymbols[i].Contains("NIFTY"))
                        {
                            if (listOfNSESymbols[i] == "NIFTY 50")
                                instrumentToken = GetTokenOfIndices("NIFTY 50");
                            else if (listOfNSESymbols[i] == "NIFTY BANK")
                                instrumentToken = GetTokenOfIndices("NIFTY BANK");
                        }
                        else
                        {
                            instrumentToken = getInstrumentToken(listOfNSESymbols[i]);
                        }
                        if (instrumentToken != "")
                        {
                            //instrumentList[i] = Convert.ToUInt32(instrumentToken);
                            instrumentList[i] = instrumentToken;
                            if (!SymbolList.ContainsKey(instrumentToken))
                            {
                                SymbolList.Add(instrumentToken, listOfNSESymbols[i]);
                                SymbolListWithFutureName.Add(instrumentToken, listOfNSESymbols[i]);
                                m_NSESymbolsInstruements.Add(listOfNSESymbols[i]);
                            }
                        }
                        else
                        {
                            logger.LogMessage("initTicker : " + listOfNSESymbols[i] + " this ignore because not present in db", MessageType.Informational);
                        }
                    }
                }
                if (listOfNFOSymbols.Count > 0)
                {
                    string concatString = FetchTableName();
                    for (int i = listOfNSESymbols.Count(), j = 0; i < listOfNFOSymbols.Count() + listOfNSESymbols.Count() && j < listOfNFOSymbols.Count(); i++, j++)
                    {
                        string name = listOfNFOSymbols[j];
                        instrumentToken = getInstrumentToken(listOfNFOSymbols[j] + concatString, concatString);
                        if (instrumentToken != "")
                        {
                            instrumentList[i] = instrumentToken;
                            if (!SymbolList.ContainsKey(instrumentToken))
                            {
                                SymbolList.Add(instrumentToken, name);
                                SymbolListWithFutureName.Add(instrumentToken, listOfNFOSymbols[j] + concatString);
                                m_NFOSymbolsInstruements.Add(listOfNFOSymbols[j] + concatString);
                            }
                        }
                        else
                        {
                            logger.LogMessage("initTicker " + listOfNFOSymbols[j] + " this ignore because not present in db", MessageType.Informational);
                        }
                    }
                }

                if (listOfMCXSymbols.Count > 0)
                {
                    string concatString = FetchTableName();
                    for (int i = listOfNFOSymbols.Count() + listOfNSESymbols.Count(), j = 0; i < listOfNFOSymbols.Count() + listOfNSESymbols.Count() + listOfMCXSymbols.Count() && j < listOfMCXSymbols.Count(); i++, j++)
                    {
                        string name = listOfMCXSymbols[j];
                        instrumentToken = getInstrumentToken(listOfMCXSymbols[j] + concatString, concatString);
                        if (instrumentToken != "")
                        {
                            instrumentList[i] = instrumentToken;
                            if (!SymbolList.ContainsKey(instrumentToken))
                            {
                                SymbolList.Add(instrumentToken, name);
                                SymbolListWithFutureName.Add(instrumentToken, listOfMCXSymbols[j] + concatString);
                                m_MCXSymbolsInstruements.Add(listOfMCXSymbols[j] + concatString);
                            }
                        }
                        else
                        {
                            logger.LogMessage("initTicker " + listOfMCXSymbols[j] + " this ignore because not present in db", MessageType.Informational);
                        }
                    }
                }

                try
                {
                    //sanika::14-sep-2020::wrote seperate class for sqlite db
                    if (m_useSqlite)
                    {
                        if (m_isConnectedToSqlite)
                        {
                            foreach (var data in SymbolListWithFutureName)
                            {
                                string tableName = data.Value;
                                if (!sqliteConnectZerodha.IsTableExists(tableName))
                                {
                                    sqliteConnectZerodha.ExecuteNonQueryCommand("CREATE TABLE '" + tableName + "'(TickDateTime DateTime PRIMARY KEY, Open real NOT NULL, High real NOT NULL,Low real NOT NULL,Close real NOT NULL,Volume real NOT NULL,DoubleDate real NOT NULL)");
                                }
                            }
                        }
                    }

                    if (m_useMysql)
                    {
                        //IRDS::03-Jul-2020::Jyoti::added for cleaning and creating tables
                        try
                        {
                            //to create db file and tables                
                            if (m_isConnected)
                            {
                                foreach (var data in SymbolListWithFutureName)
                                {
                                    string tableName = data.Value;
                                    tableName = tableName.Replace("-", "");
                                    tableName = tableName.Replace("&", "");
                                    tableName = tableName.Replace(" ", "");
                                    //m_Mysql.CreateZerodhaTable(tableName, databaseName);
                                    if (!m_Mysql.IsTableExists(tableName, databaseName))
                                    {
                                        //Jyoti5s
                                        string fields = "id INT NOT NULL AUTO_INCREMENT,Symbol VARCHAR(50) NOT NULL, LastTradeTime DATETIME NOT NULL,Bid DOUBLE NOT NULL,Ask DOUBLE NOT NULL,LTP DOUBLE NOT NULL,ChangeInValues DOUBLE NOT NULL,ChangeInPercentage DOUBLE NOT NULL,Open DOUBLE NOT NULL,High DOUBLE NOT NULL,Low DOUBLE NOT NULL,Close DOUBLE NOT NULL,Average DOUBLE NOT NULL,Volume BIGINT NOT NULL,OpenInterest BIGINT NOT NULL,Buyers BIGINT NOT NULL,Sellers BIGINT NOT NULL";
                                        //m_Mysql.CreateTable(tableName, fields, "id");
                                        m_Mysql.CreateTable(tableName, "Symbol varchar(50) NOT NULL, LastTradeTime datetime NOT NULL, Open double NOT NULL, High double NOT NULL,Low double NOT NULL,Close double NOT NULL,Volume BIGINT NOT NULL", "LastTradeTime");
                                    }
                                    //TickStartTime.Add(data.Value, Start);
                                }
                            }

                        }
                        catch (Exception e)
                        {
                            logger.LogMessage("Exception while creating table " + e.Message, MessageType.Exception);
                        }
                    }

                    if (m_addInTableThread == null)//As per sandip sir suggestion start thread after creating tables -- sanika
                    {
                        Trace.WriteLine("Started ProcessTickDataToDatabase ");
                        m_addInTableThread = new Thread(() => ProcessTickDataToDatabase());
                        m_addInTableThread.Start();
                    }                   

                }
                catch (Exception e)
                {
                    logger.LogMessage("Exception while creating table " + e.Message, MessageType.Exception);
                }

                Console.WriteLine("Inside initTicker");
                ticker = new TickerSamco(accessToken, "data.txt");

                ticker.OnTick += OnTick;
                ticker.OnReconnect += OnReconnect;
                ticker.OnNoReconnect += OnNoReconnect;
                ticker.OnError += OnError;
                ticker.OnClose += OnClose;
                ticker.OnConnect += OnConnect;
                ticker.OnOrderUpdate += OnOrderUpdate;

                ticker.EnableReconnect(Interval: 5, Retries: 50);
                ticker.Connect();
                logger.LogMessage("Subscribing the token " + instrumentList.Count(), MessageType.Informational);
                //dynamic list
                ticker.Subscribe(Tokens: instrumentList);
                //ticker.SetMode(Tokens: instrumentList, Mode: Constants.MODE_FULL);
            }
            catch (Exception e)
            {
                logger.LogMessage("Initicker : Exception Error Message = " + e.Message, MessageType.Exception);
            }

        }

        public bool OpenConnectionForCreateTable()
        {
            try
            {
                //db created at absolute path
                var path_DB = Directory.GetCurrentDirectory();
                string directoryName = path_DB + "\\" + "Database";

                if (!Directory.Exists(directoryName))
                    Directory.CreateDirectory(directoryName);

                string DBFilePath = directoryName + "\\" + DBName;

                if (!File.Exists(DBFilePath))
                {
                    SQLiteConnection.CreateFile(DBFilePath);
                    Thread.Sleep(100);
                }

                string connectionString = String.Format("Data Source={0};Version=3;New=False;Compress=True;PRAGMA locking_mode=EXCLUSIVE;", DBFilePath);
                Sql_conForCreateandreadTable = new SQLiteConnection(connectionString);
                //open sqlite connection
                Sql_conForCreateandreadTable.Open();
            }
            catch (Exception e)
            {
                logger.LogMessage("OpenConnectionForCreateTable :  Exception Error Message = " + e.Message, MessageType.Exception);
                return false;
            }

            return true;
        }

        public bool OpenConnection()
        {
            try
            {
                //db created at absolute path
                var path_DB = Directory.GetCurrentDirectory();
                string directoryName = path_DB + "\\" + "Database";

                if (!Directory.Exists(directoryName))
                    Directory.CreateDirectory(directoryName);

                string DBFilePath = directoryName + "\\" + DBName;

                if (!File.Exists(DBFilePath))
                {
                    SQLiteConnection.CreateFile(DBFilePath);
                    Thread.Sleep(100);
                }

                string connectionString = String.Format("Data Source={0};Version=3;New=False;Compress=True;", DBFilePath);
                Sql_Datacon = new SQLiteConnection(connectionString);
                //open sqlite connection
                Sql_Datacon.Open();
            }
            catch (Exception e)
            {
                logger.LogMessage("OpenConnection :  Exception Error Message = " + e.Message, MessageType.Exception);
                return false;
            }

            return true;
        }

        public void CheckTradingSymbolPresentOrNot(List<string> list)
        {
            foreach (var value in list)
            {
                if ((!RealTimeSymbols.Contains(value)) && (!TradingSymbol.Contains(value)))
                {
                    TradingSymbol.Add(value);
                    //IRDS::Jyoti::12-Sept-20::Added exchange for symbol settings form
                    RealTimeSymbols.Add(value);
                }
            }
            if (TradingSymbol.Count > 0 && TradingSymbol.Count > m_previousTradingSymbolListCount)
            {
                initTicker();
                m_previousTradingSymbolListCount = TradingSymbol.Count;
            }
        }

        public bool stopThread()
        {
            if (m_addInTableThread != null)
            {
                if (m_addInTableThread.IsAlive)
                {
                    m_addInTableThread.Abort();
                    Thread.Sleep(1000);
                    if (!m_addInTableThread.IsAlive)
                        return true;
                }
            }
            return false;
        }

        //sanika::29-sep-2020::changed method to stop thread
        public void StopThread()
        {
            m_StopThread = true;
        }


        //to create symbol name for future symbols
        public string FetchTableName()
        {
            string concatString = "";
            string year = DateTime.Now.ToString("yy");
            string currentMonth = DateTime.Now.ToString("MMM");
            List<string> tableName = new List<string>();
            string tablename = "";

            try
            {
                if (Sql_conTableName == null)
                {
                    var path_DB = Directory.GetCurrentDirectory();
                    string directoryName = path_DB + "\\" + "Database";

                    string DBFilePath = directoryName + "\\" + "samco.db";
                    string connectionString = String.Format("Data Source={0};Version=3;New=False;Compress=True;", DBFilePath);
                    Sql_conTableName = new SQLiteConnection(connectionString);
                    Sql_conTableName.Open();
                }
                SQLiteCommand Sql_cmdTableName;
                SQLiteDataReader Sql_readTableName;

                string query = "SELECT name FROM sqlite_master WHERE type = 'table' AND name NOT LIKE 'sqlite_%'";
                Sql_cmdTableName = new SQLiteCommand(query, Sql_conTableName);
                Sql_readTableName = Sql_cmdTableName.ExecuteReader();
                if (Sql_readTableName != null && Sql_readTableName.HasRows)
                {
                    while (Sql_readTableName.Read())
                    {
                        tablename = (string)Sql_readTableName["name"];
                        tableName.Add(tablename);
                    }
                }
                for (int i = 0; i < tableName.Count(); i++)
                {
                    if (tableName[i].Any(char.IsDigit))
                    {
                        if (tableName[i].Substring(0, 2) == year)
                        {
                            string month = tableName[i].Substring(2, 3);
                            if (month == DateTime.Now.ToString("MMM").ToUpper())
                            {
                                DateTime lastThusday = GetLastThusdayDate();
                                if (DateTime.Now < lastThusday)
                                {
                                    concatString = tableName[i];
                                    break;
                                }
                                else
                                {
                                    //IRDS::27-dec-2019::Sanika::Added change year if current month is dec
                                    if (currentMonth.Equals("Dec"))
                                    {
                                        int yr = Convert.ToInt32(year) + 1;
                                        year = yr.ToString();
                                    }
                                    concatString = year + DateTime.Now.AddMonths(1).ToString("MMM").ToUpper() + "FUT";

                                    if (tableName.Contains(concatString))
                                    {
                                        break;
                                    }
                                    else
                                    {
                                        return null;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                logger.LogMessage("FetchTableName : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return concatString;
        }

        //for fetch date of last thusday of month
        public DateTime GetLastThusdayDate()
        {
            DateTime date = DateTime.Now;
            int year = DateTime.Now.Year;
            int month = DateTime.Now.Month;
            try
            {
                var tmpDate = new DateTime(year, month, 1).AddMonths(1).AddDays(-1);
                while (tmpDate.DayOfWeek != DayOfWeek.Thursday)
                {
                    tmpDate = tmpDate.AddDays(-1);
                }
                date = tmpDate.AddDays(1);
            }
            catch (Exception e)
            {
                logger.LogMessage("GetLastThusdayDate : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return date;
        }


        //IRDS::Sanika::14-Aug-2019::Function to get instrument token from symbol name (from database)
        public string getInstrumentToken(string symbolName, string tableName = "instruments")
        {
            string token = "";
            try
            {
                if (Sql_conFetchSymbolName == null)
                {
                    var path_DB = Directory.GetCurrentDirectory();
                    string directoryName = path_DB + "\\" + "Database";

                    string DBFilePath = directoryName + "\\" + "samco.db";
                    string connectionString = String.Format("Data Source={0};Version=3;New=False;Compress=True;", DBFilePath);
                    Sql_conFetchSymbolName = new SQLiteConnection(connectionString);
                    Sql_conFetchSymbolName.Open();
                }
                SQLiteCommand Sql_cmdToken;
                SQLiteDataReader Sql_readToken;

                string query = "select InstrumentToken from '" + tableName + "' where symbol = '" + symbolName + "'";
                Sql_cmdToken = new SQLiteCommand(query, Sql_conFetchSymbolName);
                Sql_readToken = Sql_cmdToken.ExecuteReader();
                if (Sql_readToken != null && Sql_readToken.HasRows)
                {
                    if (Sql_readToken.Read())
                    {
                        token = (Sql_readToken["InstrumentToken"]).ToString();
                    }
                }

            }
            catch (Exception e)
            {
                logger.LogMessage("getInstrumentToken : Exception Error Message = " + e.Message, MessageType.Exception);

            }
            return token;
        }

        public void OnTokenExpire()
        {
            bOnExpiry = true;
            Console.WriteLine("Fire Event OnTokenExpire : Need to login again");
        }

        public void OnConnect()
        {
            Console.WriteLine("Connected ticker");
            logger.LogMessage("OnConnect:: Connected ticker", MessageType.Informational);

        }

        public void OnClose()
        {
            Console.WriteLine("Closed ticker");
            logger.LogMessage("OnClose:: Closed ticker", MessageType.Informational);
        }

        public void OnError(string Message)
        {
            Console.WriteLine("Error: " + Message);
            logger.LogMessage("OnError:: " + Message, MessageType.Informational);
        }

        public void OnNoReconnect()
        {
            Console.WriteLine("Not reconnecting");
            logger.LogMessage("OnNoReconnect:: Not reconnecting", MessageType.Informational);
        }

        private void OnReconnect()
        {
            Console.WriteLine("Reconnecting");
            logger.LogMessage("OnReconnect:: Reconnecting", MessageType.Informational);
        }

        public string GetToken(string symbol)
        {
            string tradingSymbol = symbol.Split('.')[0];
            string exchange = symbol.Split('.')[1];
            string tableName = "";
            if (exchange == Constants.EXCHANGE_NSE)
            {
                tableName = "instruments";
            }
            else if (exchange == Constants.EXCHANGE_NFO)
            {
                tableName = FetchTableName();
                tradingSymbol += tableName;
            }

            return getInstrumentToken(tradingSymbol, tableName);
        }

        public bool orderPlaced = true;

        private static readonly object tickLock = new object();
        DateTime myTickLastDateTime;
        public void OnTick(Tick TickData)
        {
            Tick tick = TickData;
            //IRDS::03-Jul-2020::Jyoti::added for queue changes
            //lock (m_queueTick)

            {
                try
                {
                    //IRDS::04-August-2020::Sandip::removed lock as we using thread safe queue.
                    if (tick.Timestamp != null)
                    {
                        m_queueTick.Enqueue(tick);
                        myTickLastDateTime = Convert.ToDateTime(tick.Timestamp);
                        //sanika::7-Dec-2020::added as per kite.cs changes 
                        //sanika::22-sep-2020::remove dictionary and added structure
                        if (m_TickData != null)
                        {
                            string symbolName = "";
                            if (tick.InstrumentToken != 0)
                                symbolName = SymbolListWithFutureName[tick.InstrumentToken.ToString()];
                            else
                                symbolName = SymbolListWithFutureName[tick.Mode.ToString()];
                            Trace.WriteLine("##TICKStatus : started to update structure");
                            m_TickData.AddOrUpdateTickData(tick, symbolName);
                            Trace.WriteLine("##TICKStatus : stopped to update structure");
                        }
                        //sanika::19-Nov-2020::Added condition to avoid expception

                        tickCurrentTime = (Convert.ToDateTime(tick.Timestamp.ToString())).ToString("HH:mm:ss");
                    }
                }
                catch (Exception e)
                {
                    logger.LogMessage("OnTick : Exception Error Message = " + e.Message, MessageType.Exception);
                }
            }
            // try
            // {
            //string FilePath = pathFile + "\\" + "TickData.txt";
            //using (StreamWriter streamWriter = File.AppendText(FilePath))
            //{
            //    streamWriter.WriteLine(Utils.JsonSerialize(TickData));
            //}

            /*if (tick.Timestamp != null)
            {
                string currentDate = DateTime.Now.ToString("M/d/yyyy");
                string tickDate = (Convert.ToDateTime(tick.Timestamp.ToString())).ToString("M/d/yyyy");
                if ((tickDate == currentDate))
                {
                    //lock (tickLock)
                    {
                        DateTime tickDateTime = (DateTime)tick.Timestamp;
                        tickCurrentTime = (Convert.ToDateTime(tick.Timestamp.ToString())).ToString("HH:mm:ss");
                        string symbolName = SymbolListWithFutureName[tick.InstrumentToken.ToString()];
                        lock (tickLock)
                        {
                            if (LastTradedPrice.ContainsKey(symbolName))
                            {
                                LastTradedPrice[symbolName] = tick.LastPrice.ToString();
                            }
                            else
                            {
                                LastTradedPrice.Add(symbolName, tick.LastPrice.ToString());
                            }
                        }

                      //  createBar(tick, tickDateTime);
                    }
                }
            }*/

            //}
            //catch (Exception e)
            //{
            //    Trace.WriteLine("from ontick : " + e.Message + " " + tick.Timestamp.ToString());
            //}
        }


        //0th index = StartTime
        //1th index = EndTime
        //2nd index = open
        //3rd index = high
        //4th index = low
        //5th index = close
        //6th index = volume
        //dictionary = initial volume

        int i = 0, j = 0;
        string startTime = "";


        public void createBar(Tick data, DateTime time)
        {
            string symbolName = SymbolListWithFutureName[data.Mode.ToString()];
            try
            {
                if ((time.ToOADate() >= Start.ToOADate()) && (time.ToOADate() < endTimeForTick.ToOADate()))
                {
                    createArray(data, Start, endTimeForTick, symbolName);
                }
                if (time.ToOADate() >= endTimeForTick.ToOADate() || myTickLastDateTime.ToOADate() >= endTimeForTick.ToOADate())
                {
                    FinalZerodhaData = new Dictionary<string, List<string>>(ZerodhaData);
                    ListOfSymbolsForStartValues.Clear();
                    Start = endTimeForTick;
                    endTimeForTick = Start.AddSeconds(60 * Convert.ToInt32(Interval));//.ToString("HH:mm:ss"); 
                    //ListStartOpenValues.Clear();
                    ZerodhaData.Clear();
                    Trace.WriteLine("##TICKStatus Store Data in MySQL******************* time " + time + " myTickLastDateTime " + myTickLastDateTime.ToString());

                    insertIntoTableTick(Start);
                    Trace.WriteLine("##TICKStatus Store Data in MySQL Finished ******************* time " + time + " myTickLastDateTime " + myTickLastDateTime.ToString());
                    WriteTickDatainCSVFile();
                    InsertCandleinSQLLiteDatabase();
                    //Trace.WriteLine("##TICKStatus after create arrayL******************* " + time + " symbol " + symbolName);
                    createArray(data, Start, endTimeForTick, symbolName);
                }

                if (m_isConnected && m_useMysql)
                {
                    DateTime endDateTime = DateTime.Now;
                    TimeSpan difference = endDateTime - startDateTime;
                    if (difference.Milliseconds > 500)//time.ToOADate() >= TickStartTime[symbolName].ToOADate())
                    {
                        //Trace.WriteLine("##TICKStatus : &&&&&&&&&&&&&&&& Starts 1 sec Data &&&&&&&&&&&&&&&&&&&&& " + m_queueTick.Count.ToString() + " symbolName " + symbolName + time.ToLongTimeString());                       
                        startDateTime = DateTime.Now;
                        FinalZerodhaTickData = new Dictionary<string, List<string>>(ZerodhaData);
                        // ZerodhaDataForMarketWatch = new Dictionary<string, List<string>>(ZerodhaData);                       
                        insertIntoTableTick(symbolName);
                        //TickStartTime[symbolName] = TickStartTime[symbolName].AddMilliseconds(10);
                        //Trace.WriteLine("##TICKStatus : &&&&&&&&&&&&&&&& ENDENDENDENDN 1 sec Data &&&&&&&&&&&&&&&&&&&&&");
                    }
                }
            }

            catch (Exception e)
            {
                logger.LogMessage("CreateBar : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            //IRDS::04-August-2020::Sandip::no need to write tickby tick data
            //IRDS::11-oct-2019::Sanika::Added for write tick data in csv file
            //try
            //{
            //    var path_DB = Directory.GetCurrentDirectory();
            //    string todayDate = DateTime.Now.ToString("M-d-yyyy");
            //    string today = DateTime.Now.ToString("M/d/yyyy");
            //    System.IO.Directory.CreateDirectory(path_DB + "\\" + "Data\\TickData\\" + todayDate);
            //    string dName = path_DB + "\\" + "Data\\TickData\\" + todayDate + "\\" + symbolName + ".csv";
            //    if (!File.Exists(dName))
            //    {
            //        string header = String.Join(Environment.NewLine, "Date,Time,Last Price,Volume,BuyQuantity,SellQuantity,Open,High,Low,Close,Timestamp,LastTreadedTime");
            //        header += Environment.NewLine;
            //        System.IO.File.AppendAllText(dName, header);
            //    }
            //    using (StreamWriter streamWriter = File.AppendText(dName))
            //    {
            //        String csv = String.Join(Environment.NewLine, today + "," + time + "," + data.LastPrice + "," + data.Volume + "," + data.BuyQuantity + "," + data.SellQuantity + "," + data.Open + "," + data.High + "," + data.Low + "," + data.Close + "," + data.Timestamp + "," + data.LastTradeTime);
            //        streamWriter.WriteLine(csv);
            //    }
            //}
            //catch (Exception e)
            //{
            //    logger.LogMessage("Exception from csv writing " + e.Message, MessageType.Exception);
            //    Trace.WriteLine("exception : " + e.Message);
            //}
        }
        public void insertIntoTableTick(DateTime tmpDate)
        {
            if (m_useMysql)
            {
                //logger.LogMessage("Inside insertIntoTableTick", MessageType.Informational);
                string todayDate = DateTime.Now.ToString("yyyy-MM-dd");
                Dictionary<string, List<string>> dictionary = new Dictionary<string, List<string>>(FinalZerodhaTickData);
                try
                {

                    m_Mysql.beginTran();
                    string start = tmpDate.ToString("HH:mm:ss");
                    Parallel.ForEach(dictionary, d =>
                    {
                        //foreach (var d in dictionary)
                        //{
                        string tableName = d.Key;
                        string time = null;
                        int lastIndex = d.Value[0].LastIndexOf(":");
                        if (lastIndex != -1)
                        {
                            time = d.Value[0].Substring(0, lastIndex);
                            time = start;// time + ":00";
                        }
                        //tableName = tableName.Replace("-", "");
                        //tableName = tableName.Replace("&", "");
                        //tableName = tableName.Replace(" ", "");
                        ///Trace.WriteLine("##TICKStatus Write in my sql " + time + " symbol " + symbol);
                        m_Mysql.InsertorUpdate(tableName, todayDate + " " + time, d.Value[2], d.Value[3], d.Value[4], d.Value[5], d.Value[6]);

                    });
                    //m_Mysql.CommitTran();

                }
                catch (Exception e)
                {
                    logger.LogMessage("insertIntoTableTick : Exception Error Message = " + e.Message, MessageType.Exception);
                }
                finally
                {
                    m_Mysql.CommitTran();
                }
            }
        }


        public void insertIntoTableTick(string SymbolName)
        {
            if (m_useMysql)
            {
                //logger.LogMessage("Inside insertIntoTableTick", MessageType.Informational);
                string todayDate = DateTime.Now.ToString("yyyy-MM-dd");
                Dictionary<string, List<string>> dictionary = new Dictionary<string, List<string>>(FinalZerodhaTickData);
                try
                {
                    m_Mysql.beginTran();
                    foreach (var d in dictionary)
                    {
                        string tableName = d.Key;
                        string time = null;
                        int lastIndex = d.Value[0].LastIndexOf(":");
                        if (lastIndex != -1)
                        {
                            time = d.Value[0].Substring(0, lastIndex);
                            time = time + ":00";
                        }
                        //tableName = tableName.Replace("-", "");
                        //tableName = tableName.Replace("&", "");
                        //tableName = tableName.Replace(" ", "");
                        //if (SymbolName == tableName)
                        {
                            //Trace.WriteLine("##TICKStatus Write in my sql " + time + " tableName " + tableName);
                            m_Mysql.InsertorUpdate(tableName, todayDate + " " + time, d.Value[2], d.Value[3], d.Value[4], d.Value[5], d.Value[6]);
                        }
                    }
                }
                catch (Exception e)
                {
                    logger.LogMessage("insertIntoTableTick : Exception Error Message =  " + e.Message, MessageType.Exception);
                }
                finally
                {
                    m_Mysql.CommitTran();
                }
            }
        }

        public bool TableExists(string Table_Name)
        {
            using (SQLiteCommand Sql_cmd = new SQLiteCommand(Sql_conForCreateandreadTable))
            {
                Sql_cmd.CommandText = "SELECT * FROM sqlite_master WHERE type='table' AND name=@name";
                Sql_cmd.Parameters.AddWithValue("@name", Table_Name);

                using (Sql_read = Sql_cmd.ExecuteReader())
                {
                    if (Sql_read != null && Sql_read.HasRows)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }
        public void InsertCandleinSQLLiteDatabase()
        {
            if (m_useSqlite)
            {
                if (m_isConnectedToSqlite)
                {
                    string todayDate = DateTime.Now.ToString("yyyy-MM-dd");
                    Dictionary<string, List<string>> dictionary = new Dictionary<string, List<string>>(FinalZerodhaData);
                    try
                    {
                        lock (databaselock)
                        {
                            //sanika::18-sep-2020::changed parallel loop with foreach and added list
                            List<string> dbQueries = new List<string>();
                            //Parallel.ForEach(dictionary, d =>
                            foreach (var d in dictionary)
                            {
                                string tableName = d.Key;
                                string time = null;
                                int lastIndex = d.Value[0].LastIndexOf(":");
                                if (lastIndex != -1)
                                {
                                    time = d.Value[0].Substring(0, lastIndex);
                                    time = time + ":00";
                                }
                                int trial = 0;
                                DateTime dateTime = DateTime.Parse(todayDate + " " + time);
                                try
                                {
                                    //sanika::14-sep-2020::wrote seperate class for sqlite db
                                    string insertSQLQuery = "INSERT OR IGNORE into '" + tableName + "' (TickDateTime, Open, High, Low, Close, Volume,DoubleDate) values ('" + dateTime + "','" + d.Value[2] + "','" + d.Value[3] + "','" + d.Value[4] + "','" + d.Value[5] + "','" + d.Value[6] + "','" + dateTime.ToOADate() + "')";
                                    //sqliteConnectZerodha.ExecuteNonQueryCommand(insertSQLQuery);
                                    //sanika::18-sep-2020::added queries into list
                                    if (!dbQueries.Contains(insertSQLQuery))
                                        dbQueries.Add(insertSQLQuery);
                                }
                                catch (Exception e)
                                {
                                    logger.LogMessage(tableName + " InsertCandleinSQLLiteDatabase : Exception Error Message = " + e.Message, MessageType.Exception);
                                }
                            }
                            //sanika::18-sep-2020::call function which insert multiple values
                            sqliteConnectZerodha.InsertDataMultipleValues("", dbQueries);
                        }
                    }
                    catch (Exception e)
                    {
                        logger.LogMessage("InsertCandleinSQLLiteDatabase:Exception Error Message =  " + e.Message, MessageType.Exception);
                    }
                }
            }
        }

        public bool GetHighLow(string symbol, int barCount, out double dHigh, out double dLow)
        {
            bool isValueOut = false;
            dHigh = 0;
            dLow = 0;
            if (m_useSqlite)
            {
                if (m_isConnectedToSqlite)
                {
                    try
                    {
                        string tableName = symbol;
                        int itry = 0;
                        lock (databaselock)
                        {
                            //sanika::14-sep-2020::wrote seperate class for sqlite db
                            if (sqliteConnectZerodha.GetHighLow(symbol, barCount, out dHigh, out dLow))
                            {
                                isValueOut = true;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        isValueOut = false;
                        logger.LogMessage("GetHighLow : Exception Error Message = " + e.Message, MessageType.Exception);
                    }
                }
            }
            return isValueOut;
        }



        public void InsertCandleinSQLLiteDatabaseOld()
        {
            if (m_useSqlite)
            {
                if (Sql_Datacon == null)
                {
                    OpenConnection();
                }
                string todayDate = DateTime.Now.ToString("yyyy-MM-dd");
                Dictionary<string, List<string>> dictionary = new Dictionary<string, List<string>>(FinalZerodhaData);
                try
                {

                    lock (databaselock)
                    {
                        //Parallel.ForEach(d, dictionary) =>
                        //{
                        //Parallel.ForEach(dictionary, d =>
                        //{

                        //while (cnt <= 1000)
                        //{
                        foreach (var d in dictionary)
                        {
                            string tableName = d.Key;
                            string time = null;
                            int lastIndex = d.Value[0].LastIndexOf(":");
                            if (lastIndex != -1)
                            {
                                time = d.Value[0].Substring(0, lastIndex);
                                time = time + ":00";
                            }
                            int trial = 0;
                            DateTime dateTime = DateTime.Parse(todayDate + " " + time);
                            {
                                try
                                {
                                    //logger.LogMessage(tableName + "going to insert data", MessageType.Informational);
                                    SQLiteCommand cmdInsert;
                                    string insertSQLQuery = "INSERT OR IGNORE into '" + tableName + "' (TickDateTime, Open, High, Low, Close, Volume,DoubleDate) values ('" + dateTime + "','" + d.Value[2] + "','" + d.Value[3] + "','" + d.Value[4] + "','" + d.Value[5] + "','" + d.Value[6] + "','" + dateTime.ToOADate() + "')";
                                    cmdInsert = new SQLiteCommand(insertSQLQuery, Sql_Datacon);
                                    //File.AppendAllText("exe.txt", "start excute = " + DateTime.Now + "\n");
                                    cmdInsert.ExecuteNonQuery();
                                    //logger.LogMessage(tableName + "inserted data insert query " + insertSQLQuery, MessageType.Informational);
                                }
                                catch (Exception e)
                                {
                                    logger.LogMessage(tableName + " insertIntoTable : Exception from insert data into DB " + e.Message, MessageType.Exception);
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    logger.LogMessage("InsertCandleinSQLLiteDatabase:Exception from insert data into DB " + e.Message, MessageType.Exception);
                }
            }
        }

        public bool GetHighLowOld(string symbol, int barCount, out double dHigh, out Double dLow)
        {
            dHigh = 0;
            dLow = 0;
            Trace.WriteLine("********trying to req**********");
            //if (m_useSqlite)
            {
                try
                {
                    if (Sql_conForCreateandreadTable == null)
                    {
                        OpenConnectionForCreateTable();
                    }
                    string tableName = symbol;

                    int itry = 0;
                    //SQLiteDataReader Sql_readNow;
                    //SQLiteCommand Sql_cmdSelectMaxAndMin;
                    lock (databaselock)
                    {
                        string insertSQLQuery = "SELECT max(High) as high, min(low) as low FROM '" + tableName + "' WHERE DoubleDate IN(SELECT DoubleDate FROM '" + tableName + "' order by DoubleDate DESC LIMIT " + barCount + ")";
                        using (SQLiteCommand Sql_cmdSelectMaxAndMin = new SQLiteCommand(insertSQLQuery, Sql_conForCreateandreadTable))
                        {
                            SQLiteDataReader Sql_readNow = Sql_cmdSelectMaxAndMin.ExecuteReader();
                            if (Sql_readNow != null && Sql_readNow.HasRows)
                            {
                                if (Sql_readNow.Read())
                                {
                                    dHigh = (double)Sql_readNow["high"];
                                    dLow = (double)Sql_readNow["low"];
                                    Trace.WriteLine("********fetched high/close**********" + dHigh + " " + dLow + " " + symbol);
                                }
                                if (dHigh == 0 || dLow == 0)
                                {
                                    logger.LogMessage("GetHighLow : dHigh 0  = " + tableName, MessageType.Exception);
                                }
                            }
                        }
                    }
                    return true;
                }
                catch (Exception e)
                {
                    logger.LogMessage("GetHighLow : Exception Error Message = " + e.Message, MessageType.Exception);
                }
            }
            return false;
        }

        public bool GetCloseValuesList(string symbol, int barCount, out List<double> closeValues, out List<string> dateTimeList)
        {
            List<double> ListOfCloseValues = new List<double>();
            List<string> dateTime = new List<string>();
            //if (m_useSqlite)
            {
                if (Sql_Datacon == null)
                {
                    OpenConnection();
                }
                string tableName = symbol;
                try
                {
                    int itry = 0;
                    SQLiteDataReader Sql_readNow;
                    SQLiteCommand Sql_cmdSelectMaxAndMin;
                    string insertSQLQuery = "SELECT Close, DoubleDate FROM '" + tableName + "' WHERE DoubleDate IN(SELECT DoubleDate FROM '" + tableName + "' order by DoubleDate DESC LIMIT " + barCount + ")";
                    Sql_cmdSelectMaxAndMin = new SQLiteCommand(insertSQLQuery, Sql_Datacon);
                    Sql_readNow = Sql_cmdSelectMaxAndMin.ExecuteReader();
                    if (Sql_readNow != null && Sql_readNow.HasRows)
                    {
                        while (Sql_readNow.Read())
                        {
                            ListOfCloseValues.Add((double)Sql_readNow["Close"]);
                        }
                    }
                    if (ListOfCloseValues.Count() == 0)
                    {
                        logger.LogMessage("GetCloseValuesList : Not able to get close values", MessageType.Informational);
                    }
                }
                catch (Exception e)
                {
                    logger.LogMessage("GetCloseValuesList : Exception Error Message = " + e.Message, MessageType.Exception);
                }
            }
            closeValues = ListOfCloseValues;
            dateTimeList = new List<string>();//   dateTime;
            return true;
        }

        public void WriteTickDatainCSVFile()
        {
            if (m_useCSV)
            {
                try
                {
                    lock (this)
                    {
                        var path_DB = Directory.GetCurrentDirectory();
                        string todayDate = DateTime.Now.ToString("M-d-yyyy");
                        System.IO.Directory.CreateDirectory(path_DB + "\\" + "Data\\SamcoData");
                        foreach (var d in FinalZerodhaData)
                        {
                            string dName = path_DB + "\\" + "Data\\SamcoData\\" + d.Key + ".csv";
                            //IRDS::26-Aug-2019::Sanika::Added to write header into csv file
                            if (!File.Exists(dName))
                            {
                                string header = String.Join(Environment.NewLine, "Date,Time,Open,High,Low,Close,Volume");
                                header += Environment.NewLine;
                                System.IO.File.AppendAllText(dName, header);
                            }
                            using (StreamWriter streamWriter = File.AppendText(dName))
                            {
                                string time = null;
                                int lastIndex = d.Value[0].LastIndexOf(":");
                                if (lastIndex != -1)
                                {
                                    time = d.Value[0].Substring(0, lastIndex);
                                }
                                String csv = String.Join(Environment.NewLine, todayDate + "," + time + "," + d.Value[2] + "," + d.Value[3] + "," + d.Value[4] + "," + d.Value[5] + "," + d.Value[6]);

                                streamWriter.WriteLine(csv);
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    logger.LogMessage("WriteTickDatainCSVFile : Exception Error Message = " + e.Message, MessageType.Exception);
                }
            }
        }

        public void createArray(Tick data, DateTime dstart, DateTime dend, string symbolName)
        {
            try
            {
                string currentTime = (Convert.ToDateTime(data.Timestamp.ToString())).ToString("HH:mm:ss");
                string start = dstart.ToString("HH:mm:ss");
                string end = dend.ToString("HH:mm:ss");
                if (dictionaryForLastTickTime.ContainsKey(symbolName))
                {
                    dictionaryForLastTickTime[symbolName][0] = dictionaryForLastTickTime[symbolName][1];
                    dictionaryForLastTickTime[symbolName][1] = currentTime;
                }
                else
                {
                    List<string> tmplist = new List<string>();
                    tmplist.Insert(0, currentTime);
                    tmplist.Insert(1, currentTime);
                    dictionaryForLastTickTime.Add(symbolName, new List<string>(tmplist));
                }

                //for every 1st data of tick
                if (!ListOfSymbolsForStartValues.Contains(symbolName))
                {
                    List<string> list = new List<string>();
                    list.Insert(0, start);
                    list.Insert(1, end);
                    list.Insert(2, data.LastPrice.ToString());
                    list.Insert(3, data.LastPrice.ToString());
                    list.Insert(4, data.LastPrice.ToString());
                    list.Insert(5, data.LastPrice.ToString());
                    list.Insert(6, data.Volume.ToString());
                    list.Insert(7, data.Volume.ToString()); // for intial volume

                    if (ZerodhaData.ContainsKey(symbolName))
                    {
                        ZerodhaData[symbolName] = new List<string>(list);
                    }
                    else
                    {
                        ZerodhaData.TryAdd(symbolName, new List<string>(list));
                    }

                    ListOfSymbolsForStartValues.Add(symbolName);
                }
                else
                {
                    //IRDS::27-dec-2019::Commented now we are taking first open instead of latest one
                    ////for latest open
                    //if (!ListStartOpenValues.Contains(symbolName))
                    //{
                    //    if (dictionaryForLastTickTime.ContainsKey(symbolName))
                    //    {
                    //        if (dictionaryForLastTickTime[symbolName][0] == dictionaryForLastTickTime[symbolName][1])
                    //        {
                    //            ZerodhaData[symbolName][2] = data.LastPrice.ToString();
                    //        }
                    //    }
                    //    ListStartOpenValues.Add(symbolName);
                    //}

                    if (ZerodhaData.ContainsKey(symbolName))
                    {
                        ZerodhaData[symbolName][0] = start;
                        ZerodhaData[symbolName][1] = end;
                        if (Convert.ToDecimal(ZerodhaData[symbolName][3]) < Convert.ToDecimal(data.LastPrice))
                        {
                            ZerodhaData[symbolName][3] = data.LastPrice.ToString();
                        }

                        if (Convert.ToDecimal(ZerodhaData[symbolName][4]) > Convert.ToDecimal(data.LastPrice))
                        {
                            ZerodhaData[symbolName][4] = data.LastPrice.ToString();
                        }

                        ZerodhaData[symbolName][5] = data.LastPrice.ToString();
                        decimal volume = Convert.ToDecimal(data.Volume) - Convert.ToDecimal(ZerodhaData[symbolName][7]);
                        ZerodhaData[symbolName][6] = volume.ToString();
                    }
                    else
                    {
                        List<string> list = new List<string>();
                        list.Insert(0, start);
                        list.Insert(1, end);
                        list.Insert(2, data.LastPrice.ToString());
                        list.Insert(3, data.LastPrice.ToString());
                        list.Insert(4, data.LastPrice.ToString());
                        list.Insert(5, data.LastPrice.ToString());
                        list.Insert(6, data.Volume.ToString());
                        list.Insert(7, data.Volume.ToString()); // for intial volume

                        if (ZerodhaData.ContainsKey(symbolName))
                        {
                            ZerodhaData[symbolName] = new List<string>(list);
                        }
                        else
                        {
                            ZerodhaData.TryAdd(symbolName, new List<string>(list));
                        }
                    }
                }
            }
            catch (Exception e)
            {
                logger.LogMessage("CreateArray : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }


        //public string getName(string token)
        //{
        //    string symbolName = "";
        //    var path_DB = Directory.GetCurrentDirectory();
        //    string directoryName = path_DB + "\\" + "Database";

        //    string DBFilePath = directoryName + "\\" + "kite.db";
        //    try
        //    {
        //        string connectionString = String.Format("Data Source={0};Version=3;New=False;Compress=True;", DBFilePath);
        //        if (Sql_con == null)
        //        {

        //        }
        //        using (Sql_con = new SQLiteConnection(connectionString))
        //        {
        //            Sql_con.Open();

        //            using (Tr = Sql_con.BeginTransaction())
        //            {
        //                string query = "select symbol FROM instruments where InstrumentToken = " + token;
        //                SQLiteCommand Sql_cmd = new SQLiteCommand(query, Sql_con);
        //                Sql_read = Sql_cmd.ExecuteReader();
        //                if (Sql_read != null && Sql_read.HasRows)
        //                {
        //                    while (Sql_read.Read())
        //                    {
        //                        symbolName = (string)Sql_read["symbol"];
        //                    }
        //                }
        //                Tr.Commit();
        //            }

        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine("Exception : " + e.Message);
        //    }
        //    return symbolName;
        //}

        bool m_OrderUpdate = false;
        public void OnOrderUpdate(OrderSamco OrderData)
        {
            //if (TradingSymbol.Contains(OrderData.Tradingsymbol + "." + OrderData.Exchange) || RealTimeSymbols.Contains(OrderData.Tradingsymbol + "." + OrderData.Exchange))
            {
                OrderExtSamco objOrderExt = new OrderExtSamco(OrderData);
                m_GlobalOrderStore.AddOrUpdateOrder(objOrderExt);
                m_GlobalOrderStore.AddOrder(objOrderExt);
                Trace.WriteLine("OrderUpdate " + Utils.JsonSerialize(OrderData));
                // Console.WriteLine("OrderUpdate " + Utils.JsonSerialize(OrderData));
                // testc(objOrderExt.order.OrderId.ToString());
            }
        }

        public void SetOrderUpdateStatus(bool isUpdated)
        {
            WriteUniquelogs("OnOrderUpdate", "SetOrderUpdateStatus : m_OrderUpdate set to true ", MessageType.Informational);
            m_OrderUpdate = isUpdated;
        }
        public bool GetOrderUpdateStatus()
        {
            return m_OrderUpdate;
        }

        //public void testc(string orderid)
        //{
        //    OrderExt orderExt;
        //    m_GlobalOrderStore.GetOpenOrderbyID("IOC", "NSE", orderid, out orderExt);
        //    List<Order> orderInfo = m_GlobalOrderStore.GetOpenOrderbySymbol("IOC", "NSE");
        //    int i = 0;
        //    i++;
        //}

        /// <summary>
        /// Retrieve historical data (candles) for an instrument.
        /// </summary>
        /// <param name="InstrumentToken">Identifier for the instrument whose historical records you want to fetch. This is obtained with the instrument list API.</param>
        /// <param name="FromDate">Date in format yyyy-MM-dd for fetching candles between two days. Date in format yyyy-MM-dd hh:mm:ss for fetching candles between two timestamps.</param>
        /// <param name="ToDate">Date in format yyyy-MM-dd for fetching candles between two days. Date in format yyyy-MM-dd hh:mm:ss for fetching candles between two timestamps.</param>
        /// <param name="Interval">The candle record interval. Possible values are: minute, day, 3minute, 5minute, 10minute, 15minute, 30minute, 60minute</param>
        /// <param name="Continuous">Pass true to get continous data of expired instruments.</param>
        /// <returns>List of Historical objects.</returns>
        public List<HistoricalSamco> GetHistoricalData(
            string symbolName,
            DateTime FromDate,
            DateTime ToDate,
            string Interval = "1",
            string exchange = "NSE")
        {
            var param = new Dictionary<string, dynamic>();

            //param.Add("from", FromDate.ToString("yyyy-MM-dd HH:mm:ss"));
            //param.Add("to", ToDate.ToString("yyyy-MM-dd HH:mm:ss"));
            //param.Add("interval", Interval);
            //param.Add("continuous", Continuous ? "1" : "0");
            Utils.AddIfNotNull(param, "exchange", exchange);
            Utils.AddIfNotNull(param, "symbolName", symbolName);
            Utils.AddIfNotNull(param, "fromDate", FromDate.ToString("yyyy-MM-dd HH:mm:ss"));
            Utils.AddIfNotNull(param, "toDate", ToDate.ToString("yyyy-MM-dd HH:mm:ss"));
            Utils.AddIfNotNull(param, "interval", Interval);
            var respData = Get("data.intraday", param);
            ArrayList historicalData = respData["intradayCandleData"];
            List<HistoricalSamco> historicals = new List<HistoricalSamco>();

            foreach (var item in historicalData)
            {
                Dictionary<string, dynamic> record = (Dictionary<string, dynamic>)item;
                historicals.Add(new HistoricalSamco(record));
            }

            return historicals;
        }


        /// <summary>
        /// Get account balance and cash margin details for a particular segment.
        /// </summary>
        /// <param name="Segment">Trading segment (eg: equity or commodity)</param>
        /// <returns>Margins for specified segment.</returns>
        public UserMarginSamco GetMargins(string Segment)
        {
            var userMarginData = Get("user.segment_margins");
            UserMarginsResponseSamco response = new UserMarginsResponseSamco(userMarginData);
            return response.Equity;
        }

        /// <summary>
        /// Retrieve LTP and OHLC of upto 200 instruments
        /// </summary>
        /// <param name="InstrumentId">Indentification of instrument in the form of EXCHANGE:TRADINGSYMBOL (eg: NSE:INFY) or InstrumentToken (eg: 408065)</param>
        /// <returns>Dictionary of all OHLC objects with keys as in InstrumentId</returns>
        public Dictionary<string, OHLC> GetOHLC(string[] InstrumentId)
        {
            var param = new Dictionary<string, dynamic>();
            param.Add("i", InstrumentId);
            Dictionary<string, dynamic> ohlcData = Get("market.ohlc", param)["data"];

            Dictionary<string, OHLC> ohlcs = new Dictionary<string, OHLC>();
            foreach (string item in ohlcData.Keys)
                ohlcs.Add(item, new OHLC(ohlcData[item]));

            return ohlcs;
        }

        public Dictionary<string, Quote> GetQuote(string[] InstrumentId)
        {
            var param = new Dictionary<string, dynamic>();
            param.Add("i", InstrumentId);
            Dictionary<string, dynamic> quoteData = Get("market.quote", param)["data"];

            Dictionary<string, Quote> quotes = new Dictionary<string, Quote>();
            foreach (string item in quoteData.Keys)
                quotes.Add(item, new Quote(quoteData[item]));

            return quotes;
        }

        public string GetTokenOfIndices(string indexName)
        {
            var param = new Dictionary<string, dynamic>();
            param.Add("indexName", indexName);
            Dictionary<string, dynamic> quoteData = Get("indices.quote", param);
            string listingId = quoteData["listingId"];
            return listingId;
        }

        //IRDS ::Nayana :: 15-Apr-2019 :: Added to get realtime data and add that data into the dictionary 
        public void GetRealTimeData(Tick Data)
        {
            if (!DictRealTimeData.ContainsKey(Data.InstrumentToken))
            {
                DictRealTimeData.Add(Data.InstrumentToken, new RealTimeData(Data));
            }
            else
            {
                DictRealTimeData[Data.InstrumentToken] = new RealTimeData(Data);
            }


            foreach (var kvp in DictRealTimeData)
            {
                orderPlaced = false;
                uint key = kvp.Key;
                string timestamp = DictRealTimeData[key].Timestamp;
                PlaceOrderWithRealTimeData(timestamp, key);
                orderPlaced = true;
            }

        }

        
        private void Append(Tick action)
        {
            _Queue.Add(action);
            System.Diagnostics.Debug.WriteLine(" Append " + _Queue.Count.ToString());
        }

        public void PlaceOrderWithRealTimeData(string DateTime, uint InstrumentToken)
        {
            if (DictRealTimeData.ContainsKey(InstrumentToken))
            {
                DictRealTimeData.TryGetValue(InstrumentToken, out RealTimeData value);

                string TimeStamp = value.Timestamp;

                if (TimeStamp == DateTime)
                {
                    decimal Open = value.Open;
                    decimal Close = value.Close;
                    decimal high = value.High;
                    double volume = value.Volume;
                    decimal Low = value.Low;

                    if (true)
                    {
                        string error = "";
                        AlgoOMS signal = new AlgoOMS();
                    }
                }
            }
            else
            {

            }
        }
        public List<HoldingSamco> GetHoldings()
        {
            var holdingsData = Get("holdings.getAll");

            List<HoldingSamco> holdings = new List<HoldingSamco>();
            if (holdingsData.ContainsKey("holdingDetails"))
            {
                foreach (Dictionary<string, dynamic> item in holdingsData["holdingDetails"])
                    holdings.Add(new HoldingSamco(item));
            }
            return holdings;
        }
        //IRDSPM::Pratiksha::-01-07-2020::For fetching symbol names from db --start
        public List<string> GetDataforTradingsymbol(string exchange)
        {
            List<string> symbolDet = new List<string>();
            string token = "";
            try
            {
                if (Sql_conFetchSymbolName == null)
                {
                    var path_DB = Directory.GetCurrentDirectory();
                    string directoryName = path_DB + "\\" + "Database";

                    string DBFilePath = directoryName + "\\" + "samco.db";
                    string connectionString = String.Format("Data Source={0};Version=3;New=False;Compress=True;", DBFilePath);
                    Sql_conFetchSymbolName = new SQLiteConnection(connectionString);
                    Sql_conFetchSymbolName.Open();
                }
                SQLiteTransaction TrToken;
                SQLiteCommand Sql_cmdToken;
                SQLiteDataReader Sql_readToken;

                string condition = "";
                string tableName = "instruments";
                if (exchange == "NFO")
                {
                    condition = " where exchange = 'NFO';";
                    tableName = FetchTableName();
                }
                else if (exchange == "MCX")
                {
                    condition = " where exchange = 'MFO';";
                    tableName = FetchTableName();
                }
                else if (exchange == "OPTIONS")
                {
                    tableName = "options";
                }
                else if (exchange == "CDS")
                {
                    tableName = "Currency";
                }
                string query = "select symbol from '" + tableName + "'" + condition;
                Sql_cmdToken = new SQLiteCommand(query, Sql_conFetchSymbolName);
                Sql_readToken = Sql_cmdToken.ExecuteReader();
                if (Sql_readToken != null && Sql_readToken.HasRows)
                {
                    while (Sql_readToken.Read())
                    {
                        token = (Sql_readToken["symbol"]).ToString();
                        symbolDet.Add(token);
                    }
                }
            }
            catch (Exception e)
            {
                logger.LogMessage("getInstrumentToken : Exception Error Message = " + e.Message, MessageType.Exception);
                //Console.WriteLine("Exception : " + e.Message);
            }
            return symbolDet;
        }

        private static readonly object m_WriteLog = new object();
        public void WriteUniquelogs(string symbol, string message, MessageType msgType)
        {
            try
            {
                lock (m_WriteLog)
                {
                    SymbolLogs log = new SymbolLogs();
                    //log = SymbolLogs.InstanceSymbolLogs;
                    log.createFile(symbol);
                    log.LogMessage(message, MessageType.Informational);
                    log = null;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : " + e.Message);
                //Trace.WriteLine("Exception : " + e.Message);
            }
        }


        //IRDS::04-August-2020::Sandip::process tick data
        public void ProcessTickDataToDatabase()
        {
            Trace.WriteLine("ProcessTickDataToDatabase");
            long myprocesstick = 0;
            long Totalmyprocesstick = 0;
            while (true && m_StopThread == false)
            {
                if (m_useMysql == false && m_useCSV == false && m_useSqlite == false)
                {
                    Trace.WriteLine("ProcessTickDataToDatabase");
                    Thread.Sleep(1000);
                    continue;
                }
                Thread.Sleep(1);
                if (DateTime.Now.ToOADate() <= EndTimeMCX.ToOADate())
                {
                    Tick tick;
                    while (m_queueTick.TryDequeue(out tick) && m_StopThread == false)
                    {

                        myprocesstick++;
                        Totalmyprocesstick++;
                        try
                        {
                            if (tick.Timestamp != null)
                            {
                                string currentDate = DateTime.Now.ToString("M/d/yyyy");
                                string tickDate = (Convert.ToDateTime(tick.Timestamp.ToString())).ToString("M/d/yyyy");
                                string symbolName = "";
                                if (tick.InstrumentToken != 0)
                                    symbolName = SymbolListWithFutureName[tick.InstrumentToken.ToString()];
                                else
                                    symbolName = SymbolListWithFutureName[tick.Mode.ToString()];
                                if ((tickDate == currentDate))
                                {
                                    DateTime tickDateTime = (DateTime)tick.Timestamp;                                  
                                    if (myprocesstick % 100 == 0)
                                    {
                                        Trace.WriteLine("##TICKStatus : 100 Ticks process  " + m_queueTick.Count.ToString() + " myprocesstick " + tickCurrentTime + " Totalmyprocesstick " + Totalmyprocesstick.ToString());
                                        myprocesstick = 0;//
                                        Thread.Sleep(1);
                                        if (Totalmyprocesstick > 100000) Totalmyprocesstick = 0;
                                    }
                                    DateTime EndTime = GetEndTime(symbolName);
                                    DateTime startDateTime = GetStartTime(symbolName);
                                    if (tickDateTime.ToOADate() <= EndTime.ToOADate())
                                    {
                                        if (tickDateTime.ToOADate() >= startDateTime.ToOADate())
                                        {
                                            createBar(tick, tickDateTime);
                                        }
                                    }
                                }
                            }
                        }

                        catch (Exception e)
                        {
                            Trace.WriteLine("##TICKStatus : Exception from ProcessTickDataToDatabas " + e.Message);
                            logger.LogMessage("ProcessTickDataToDatabase : Exception Error Message " + e.Message, MessageType.Exception);
                            //Trace.WriteLine("from insertFromQueueInTable : " + e.Message + " " + tick.Timestamp.ToString());
                        }
                        Thread.Sleep(5);//sleep 1sec to recive more ticks
                    }
                }
                else
                {
                    StopThread();
                }
            }
            logger.LogMessage("ProcessTickDataToDatabase : Thread stopped m_StopThread " + m_StopThread, MessageType.Informational);
        }

        public DateTime GetStartTime(string symbolName)
        {
            DateTime dateTime = new DateTime();
            if (m_NSESymbolsInstruements.Contains(symbolName))
            {
                dateTime = StartNSE;
            }
            else if (m_NFOSymbolsInstruements.Contains(symbolName))
            {
                dateTime = StartNFO;
            }
            else if (m_MCXSymbolsInstruements.Contains(symbolName))
            {
                dateTime = StartMCX;
            }
            return dateTime;
        }

        public DateTime GetEndTime(string symbolName)
        {
            DateTime dateTime = new DateTime();
            if (m_NSESymbolsInstruements.Contains(symbolName))
            {
                dateTime = EndTimeNSE;
            }
            else if (m_NFOSymbolsInstruements.Contains(symbolName))
            {
                dateTime = EndTimeNFO;
            }
            else if (m_MCXSymbolsInstruements.Contains(symbolName))
            {
                dateTime = EndTimeMCX;
            }
            //sanika::2-dec-2020::Added for cds
            else if (m_CDSSymbolsInstruements.Contains(symbolName))
            {
                dateTime = EndTimeCDS;
            }
            return dateTime;
        }
        public List<string> fetchTablenamesForsearchboxList()
        {
            List<string> MasterSymbolList = new List<string>();
            List<string> tableNames = new List<string>();
            string concatString = "";
            string year = DateTime.Now.ToString("yy");
            string nextYear = DateTime.Now.AddYears(1).ToString("yy");
            string currentMonth = DateTime.Now.ToString("MMM");
            string tablename = "";
            try
            {
                if (Sql_conTableName == null)
                {
                    var path_DB = Directory.GetCurrentDirectory();
                    string directoryName = path_DB + "\\" + "Database";
                    string DBFilePath = directoryName + "\\" + "samco.db";
                    string connectionString = String.Format("Data Source={0};Version=3;New=False;Compress=True;", DBFilePath);
                    Sql_conTableName = new SQLiteConnection(connectionString);
                    Sql_conTableName.Open();
                }
                SQLiteTransaction TrTableName;
                SQLiteCommand Sql_cmdTableName;
                SQLiteDataReader Sql_readTableName;
                string query = "SELECT name FROM sqlite_master WHERE type = 'table' AND name NOT LIKE 'sqlite_%'";
                Sql_cmdTableName = new SQLiteCommand(query, Sql_conTableName);
                Sql_readTableName = Sql_cmdTableName.ExecuteReader();
                if (Sql_readTableName != null && Sql_readTableName.HasRows)
                {
                    while (Sql_readTableName.Read())
                    {
                        tablename = (string)Sql_readTableName["name"];
                        tableNames.Add(tablename);
                    }
                }
                Console.WriteLine(tableNames.Count());
                string currMonthinStr = DateTime.Now.ToString("MMM");
                int currMonthinInt = DateTime.ParseExact(currMonthinStr, "MMM", CultureInfo.CurrentCulture).Month;
                for (int i = 0; i < tableNames.Count(); i++)
                {
                    {
                        if (tableNames[i].Substring(0, 2).Any(char.IsDigit))
                        {
                            string currMonth = tableNames[i].Substring(2, 3);
                            int month = DateTime.ParseExact(currMonth, "MMM", CultureInfo.CurrentCulture).Month;
                            if (currMonthinInt <= month)
                            {
                                int yearint = Convert.ToInt32(tableNames[i].Substring(0, 2));
                                if (yearint >= Convert.ToInt32(year))
                                {
                                    concatString = tableNames[i];
                                    MasterSymbolList.Add(concatString);
                                }
                            }
                            else
                            {
                                int yearint = Convert.ToInt32(tableNames[i].Substring(0, 2));
                                if (yearint > Convert.ToInt32(year))
                                {
                                    concatString = tableNames[i];
                                    MasterSymbolList.Add(concatString);
                                }
                            }
                        }
                        else if ((tableNames[i].Substring(0, 3).Any(char.IsLetter)) && (tableNames[i].Substring(3, 2).Any(char.IsDigit)))
                        {
                            string currMonth = tableNames[i].Substring(0, 3);
                            int month = DateTime.ParseExact(currMonth, "MMM", CultureInfo.CurrentCulture).Month;
                            if (currMonthinInt <= month)
                            {
                                int yearint = Convert.ToInt32(tableNames[i].Substring(3, 2));
                                if (yearint >= Convert.ToInt32(year))
                                {
                                    concatString = tableNames[i];
                                    MasterSymbolList.Add(concatString);
                                }
                            }
                        }
                        else
                        {
                            MasterSymbolList.Add(tableNames[i].ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return MasterSymbolList;
        }

        public bool GetChangeInPercent(string TradingSymbol, string tickTime, out double highPercent, out double lowPercent)
        {
            //sanika::5-Apr-2021::Added condition as table name changed for future symbols
            if (TradingSymbol.Contains(FetchTableName()))
                TradingSymbol = TradingSymbol.Replace(FetchTableName(), "-I");
            // File.AppendAllText("time.txt", "**** "+ TradingSymbol +" GetChangeInPercent - getting change in percent for end time "+endTime+"\n");
            highPercent = 0;
            lowPercent = 0;
            // if (m_useSqlite)
            {
                //  if (m_isConnectedToSqlite)
                {
                    try
                    {
                        /// string tableName = TradingSymbol;                       
                        //lock (databaselock)
                        {
                            double dHigh = 0, dLow = 0;
                            double close = GetYesterdayClose(TradingSymbol);
                            //sanika::5-Apr-2021::Added to take high and low of last bar
                            string sTime = DateTime.ParseExact(tickTime, "HH:mm:ss", null).AddMinutes(-1).ToString("HH:mm") + ":00";
                            string eTime = DateTime.ParseExact(sTime, "HH:mm:ss", null).AddMinutes(1).ToString("HH:mm:ss");
                            GetHighLowFromDB(TradingSymbol, sTime, eTime, out dHigh, out dLow);
                            if (dHigh == 0)
                                dHigh = Convert.ToDouble(m_TickData.GetHighPrice(TradingSymbol));
                            if (dLow == 0)
                                dLow = Convert.ToDouble(m_TickData.GetLowPrice(TradingSymbol));

                            WriteUniquelogs(TradingSymbol + " " + username, "GetChangeInPercent : TradingSymbol = " + TradingSymbol + " dHigh = " + dHigh + " dLow = " + dLow + " close " + close, MessageType.Informational);
                            highPercent = ((dHigh - close) * 100) / dHigh;
                            lowPercent = ((close - dLow) * 100) / dLow;
                            WriteUniquelogs(TradingSymbol + " " + username, "GetChangeInPercent :  highPercent = " + highPercent + " lowPercent = " + lowPercent, MessageType.Informational);
                            // "GetChangeInPercent :
                        }
                        return true;
                    }
                    catch (Exception e)
                    {
                        WriteUniquelogs(TradingSymbol + " " + username, "GetChangeInPercent : Exception Error Message = " + e.Message, MessageType.Exception);
                    }
                }
            }
            return false;
        }


        public bool GetChangeInPercent(string TradingSymbol, out double highPercent, out double lowPercent)
        {
            // File.AppendAllText("time.txt", "**** "+ TradingSymbol +" GetChangeInPercent - getting change in percent for end time "+endTime+"\n");
            highPercent = 0;
            lowPercent = 0;
            // if (m_useSqlite)
            {
                //  if (m_isConnectedToSqlite)
                {
                    try
                    {
                        /// string tableName = TradingSymbol;                       
                        //lock (databaselock)
                        {
                            double dHigh = 0, dLow = 0;
                            double close = GetYesterdayClose(TradingSymbol);
                            dHigh = Convert.ToDouble(m_TickData.GetHighPrice(TradingSymbol));
                            dLow = Convert.ToDouble(m_TickData.GetLowPrice(TradingSymbol));
                            //sqliteConnectZerodha.GetMaxHighLow(tableName, startTime,endTime, out dHigh, out dLow);
                            logger.LogMessage("GetChangeInPercent : TradingSymbol = " + TradingSymbol + " dHigh = " + dHigh + " dLow = " + dLow, MessageType.Informational);
                            highPercent = ((dHigh - close) * 100) / dHigh;
                            lowPercent = ((close - dLow) * 100) / dLow;
                            logger.LogMessage("GetChangeInPercent :  highPercent = " + highPercent + " lowPercent = " + lowPercent, MessageType.Informational);
                            // "GetChangeInPercent :
                        }
                        return true;
                    }
                    catch (Exception e)
                    {
                        logger.LogMessage("GetChangeInPercent : Exception Error Message = " + e.Message, MessageType.Exception);
                    }
                }
            }
            return false;
        }


        //IRDS::sanika::30-oct-2020::Added to fetch yesterday close from db
        public double GetYesterdayClose(string TradingSymbol)
        {
            double close = 0;
            if (m_useSqlite)
            {
                if (m_isConnectedToSqlite)
                {
                    try
                    {
                        string tableName = TradingSymbol;
                        close = Convert.ToDouble(m_TickData.GetYesterdayClose(TradingSymbol));
                        logger.LogMessage("GetClose : TradingSymbol = " + TradingSymbol + " GetYesterdayClose = " + close, MessageType.Informational);
                        if (close == 0)
                        {
                            lock (databaselock)
                            {
                                close = sqliteConnectZerodha.GetYesterdayClose(tableName);
                                logger.LogMessage("GetClose : TradingSymbol = " + TradingSymbol + " close = " + close, MessageType.Informational);
                                if (close != 0)
                                {
                                    m_TickData.AddYesterdayClose(close, TradingSymbol);
                                }
                                else
                                {
                                    logger.LogMessage("GetClose : Close retrive as 0 from db TradingSymbol = " + TradingSymbol + " close = " + close, MessageType.Informational);
                                    close = Convert.ToDouble(m_TickData.GetClosePrice(TradingSymbol));
                                }
                            }
                        }
                        return close;
                    }
                    catch (Exception e)
                    {
                        logger.LogMessage("GetClose : Exception Error Message = " + e.Message, MessageType.Exception);
                    }
                }
            }
            return close;
        }

        //sanika::5-nov-2020::Added for ajay sir's startegy 
        public bool GetHighLowFromDB(string symbol, string StartTime, string EndTime, out double dHigh, out double dLow)
        {
            bool isValueOut = false;
            dHigh = 0;
            dLow = 0;
            if (m_useSqlite)
            {
                if (m_isConnectedToSqlite)
                {
                    try
                    {
                        string tableName = symbol;
                        string currentdate = DateTime.Now.ToString("dd-MM-yyyy");
                        double start = DateTime.ParseExact(currentdate + " " + StartTime, "dd-MM-yyyy HH:mm:ss", null).ToOADate();
                        double end = DateTime.ParseExact(currentdate + " " + EndTime, "dd-MM-yyyy HH:mm:ss", null).ToOADate();
                        lock (databaselock)
                        {
                            //sanika::14-sep-2020::wrote seperate class for sqlite db
                            if (sqliteConnectZerodha.GetMaxHighLow(symbol, start, end, out dHigh, out dLow))
                            {
                                isValueOut = true;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        isValueOut = false;
                        logger.LogMessage("GetHighLow : Exception Error Message = " + e.Message, MessageType.Exception);
                    }
                }
            }
            return isValueOut;
        }
        string m_Title = "";
        //IRDSPM::PRatiksha:: To set title
        public void SetTitle(string Title)
        {
            m_Title = Title;
        }
        //sanika::11-Jun-2021::Added thread to subscribe tokens after market start or after start time from ini settings
        public void SubscribeTokens()
        {
            while(isStopSubscribeTokenThread == false)
            {
                if (DateTime.Now.ToOADate() >= StartNSE.ToOADate())
                {
                    initTicker();
                    isStopSubscribeTokenThread = true;
                }
            }
        }
    }
}
