﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRDSAlgoOMS
{
    public class OrderStoreSamco
    {
        private Dictionary<string, SymbolWiseOrderStoreSamco> orderStoreBySymbolTT;
        private Dictionary<string, OrderSamco> AllOrder;
        private static readonly object OrdertickLock = new object();
        public OrderStoreSamco()
        {
            orderStoreBySymbolTT = new Dictionary<string, SymbolWiseOrderStoreSamco>();
            AllOrder = new Dictionary<string, OrderSamco>();
        }

        public OrderStoreSamco(List<Instrument> instruments)
        {
            foreach (var instrument in instruments)
                orderStoreBySymbolTT.Add(GetKey(instrument.TradingSymbol, instrument.Exchange), new SymbolWiseOrderStoreSamco());
        }

        //private Dictionary<Tkey2, TValue> dic2 = new Dictionary<Tkey2, TValue>();

        public string GetKey(string tradingSymbol, string exchange)
        {
            return tradingSymbol + "|" + exchange;
        }

        public void AddOrUpdateOrder(OrderExtSamco newOrder)
        {
            lock (OrdertickLock)
            {
                var key = GetKey(newOrder.order.Tradingsymbol, newOrder.order.Exchange);
                if (orderStoreBySymbolTT.ContainsKey(key))
                {
                    SymbolWiseOrderStoreSamco symbolWiseOrderStore;
                    orderStoreBySymbolTT.TryGetValue(key, out symbolWiseOrderStore);
                    if (symbolWiseOrderStore != null)
                        symbolWiseOrderStore.AddOrUpdateOrder(newOrder);
                }
                else
                {
                    orderStoreBySymbolTT.Add(GetKey(newOrder.order.Tradingsymbol, newOrder.order.Exchange), new SymbolWiseOrderStoreSamco());
                    if (orderStoreBySymbolTT.ContainsKey(key))
                    {
                        SymbolWiseOrderStoreSamco symbolWiseOrderStore;
                        orderStoreBySymbolTT.TryGetValue(key, out symbolWiseOrderStore);

                        symbolWiseOrderStore.AddOrUpdateOrder(newOrder);
                    }
                }
            }

        }

        public void AddOrder(OrderExtSamco newOrder)
        {
            if (!AllOrder.ContainsKey(newOrder.order.OrderId))
            {
                AllOrder.Add(newOrder.order.OrderId, newOrder.order);
            }
            else
            {
                AllOrder[newOrder.order.OrderId] = newOrder.order;
            }
        }
        public bool GetOrderbyID(string tradingSymbol, string exchange, string orderId, out OrderExtSamco existingOrder)
        {
            lock (OrdertickLock)
            {
                var key = GetKey(tradingSymbol, exchange);
                SymbolWiseOrderStoreSamco symbolWiseOrderStore;
                orderStoreBySymbolTT.TryGetValue(key, out symbolWiseOrderStore);
                if (symbolWiseOrderStore != null)
                    return symbolWiseOrderStore.GetOrder(orderId, out existingOrder);
                else
                    existingOrder = new OrderExtSamco();
                return false;
            }
        }

        public List<OrderSamco> GetOrderbySymbol(string tradingSymbol, string exchange)
        {
            lock (OrdertickLock)
            {
                var key = GetKey(tradingSymbol, exchange);
                SymbolWiseOrderStoreSamco symbolWiseOrderStore;
                orderStoreBySymbolTT.TryGetValue(key, out symbolWiseOrderStore);

                List<OrderSamco> orderhistorybysymbol = new List<OrderSamco>();
                List<OrderSamco> previousOrderBySymbol = new List<OrderSamco>();

                //SymbolWiseOrderStore symbolWiseOrderStore;
                //orderStoreBySymbolTT.TryGetValue(key, out symbolWiseOrderStore);
                if (symbolWiseOrderStore != null)
                {
                    orderhistorybysymbol = symbolWiseOrderStore.GetAllOrder();
                }

                return orderhistorybysymbol;
            }
        }

        public List<OrderSamco> GetOpenOrderbySymbol(string tradingSymbol, string exchange)
        {
            lock (OrdertickLock)
            {
                var key = GetKey(tradingSymbol, exchange);
                SymbolWiseOrderStoreSamco symbolWiseOrderStore;
                orderStoreBySymbolTT.TryGetValue(key, out symbolWiseOrderStore);

                List<OrderSamco> orderhistorybysymbol = new List<OrderSamco>();
                List<OrderSamco> previousOrderBySymbol = new List<OrderSamco>();

                if (symbolWiseOrderStore != null)
                {
                    orderhistorybysymbol = symbolWiseOrderStore.GetAllOpenOrder();
                }

                return orderhistorybysymbol;
            }
        }

        public bool GetOpenOrderbyID(string tradingSymbol, string exchange, string orderId, out OrderExtSamco existingOrder)
        {
            lock (OrdertickLock)
            {
                var key = GetKey(tradingSymbol, exchange);
                SymbolWiseOrderStoreSamco symbolWiseOrderStore;
                orderStoreBySymbolTT.TryGetValue(key, out symbolWiseOrderStore);
                if (symbolWiseOrderStore != null)
                    return symbolWiseOrderStore.GetOrder(orderId, out existingOrder);
                else
                    existingOrder = new OrderExtSamco();
                return false;
            }
        }

        public bool GetLatestOrder(string mappedSymbol, string buyOrSell, out OrderExtSamco existingOrder)
        {
            lock (OrdertickLock)
            {
                SymbolWiseOrderStoreSamco symbolWiseOrderStore;
                orderStoreBySymbolTT.TryGetValue(mappedSymbol, out symbolWiseOrderStore);
                return symbolWiseOrderStore.GetLatestOrder(buyOrSell, out existingOrder);
            }
        }

        //Sanika::11-sep-2020::Remove out variable as per latest design
        public List<OrderSamco> GetAllOrder()
        {
            List<OrderSamco>  Orders = new List<OrderSamco>();
            foreach (var order in AllOrder)
            {
                Orders.Add(order.Value);
            }
            return Orders;
        }

    }
}
