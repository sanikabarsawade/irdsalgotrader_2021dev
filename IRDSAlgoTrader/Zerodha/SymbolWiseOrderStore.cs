﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRDSAlgoOMS
{
    public class SymbolWiseOrderStore
    {
        public SymbolWiseOrderStore()
        { }

        private Dictionary<string, OrderExt> orderStoreByOrderId = new Dictionary<string, OrderExt>();
   
        public void AddOrUpdateOrder(OrderExt newOrder)
        {
            if (orderStoreByOrderId.ContainsKey(newOrder.order.OrderId))
            { 
                newOrder.LastUpdateTime = DateTime.Now;
                orderStoreByOrderId[newOrder.order.OrderId] = newOrder;
            }
            else
            {
                    orderStoreByOrderId.Add(newOrder.order.OrderId, newOrder);
            }
        }
               
        public bool GetOrder(string orderId, out OrderExt existingOrder)
        {
            orderStoreByOrderId.TryGetValue(orderId, out existingOrder);
            if(existingOrder != null)
            {
                return true;
            }
            else
            {
                existingOrder = new OrderExt();
                return false;
            }
        }
        public List<Order> GetAllOrder()
        {
            List<Order> orderhistorybysymbol = new List<Order>();
            var orderList = orderStoreByOrderId.Select(o => o.Value).OrderBy(o => o.LastUpdateTime);
            var reverseList = orderList.Reverse();

            foreach (var order1 in reverseList)
            {
                if (order1.order.Status == Constants.ORDER_STATUS_PENDING || order1.order.Status == Constants.ORDER_STATUS_OPEN)
                {
                    orderhistorybysymbol.Add(order1.order);
                }                
            }
            return orderhistorybysymbol;
        }

        public List<Order> GetAllOpenOrder()
        {
            List<Order> orderhistorybysymbol = new List<Order>();
            var orderList = orderStoreByOrderId.Select(o => o.Value).OrderBy(o => o.LastUpdateTime);
            var reverseList = orderList.Reverse();

            foreach (var order1 in reverseList)
            {
                if (order1.order.Status == Constants.ORDER_STATUS_COMPLETE && order1.order.OrderType == Constants.ORDER_TYPE_MARKET)
                {
                    orderhistorybysymbol.Add(order1.order);
                }
            }
            return orderhistorybysymbol;
        }
        
        public bool GetLatestOrder(string buyOrSell, out OrderExt existingOrder)
        {            
            var orderList = orderStoreByOrderId.Select(o => o.Value).OrderBy(o => o.LastUpdateTime);
            var reverseList = orderList.Reverse();

            foreach (var order in reverseList)
            {
                if (order.order.TransactionType == buyOrSell)
                {
                    existingOrder = order;
                    return true;
                }
            }

            existingOrder = new OrderExt();
            return false;
        }
    }
}
