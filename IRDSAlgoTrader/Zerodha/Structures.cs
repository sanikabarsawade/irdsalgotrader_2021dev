﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace IRDSAlgoOMS
{
    /// <summary>
    /// Tick data structure
    /// </summary>
    public struct Tick
    {
        public string Mode { get; set; }
        public UInt32 InstrumentToken { get; set; }
        public bool Tradable { get; set; }
        public decimal LastPrice { get; set; }
        public UInt32 LastQuantity { get; set; }
        public decimal AveragePrice { get; set; }
        public UInt32 Volume { get; set; }
        public UInt32 BuyQuantity { get; set; }
        public UInt32 SellQuantity { get; set; }
        public decimal Open { get; set; }
        public decimal High { get; set; }
        public decimal Low { get; set; }
        public decimal Close { get; set; }
        public decimal Change { get; set; }
        public DepthItem[] Bids { get; set; }
        public DepthItem[] Offers { get; set; }

        // KiteConnect 3 Fields

        public DateTime? LastTradeTime { get; set; }
        public UInt32 OI { get; set; }
        public UInt32 OIDayHigh { get; set; }
        public UInt32 OIDayLow { get; set; }
        public DateTime? Timestamp { get; set; }
    }

    /// <summary>
    /// Market depth item structure
    /// </summary>
    public struct DepthItem
    {
        public DepthItem(Dictionary<string, dynamic> data)
        {
            Quantity = Convert.ToUInt32(data["quantity"]);
            Price = data["price"];
            Orders = Convert.ToUInt32(data["orders"]);
        }

        public UInt32 Quantity { get; set; }
        public decimal Price { get; set; }
        public UInt32 Orders { get; set; }
    }

    /// <summary>
    /// Historical structure
    /// </summary>
    public struct Historical
    {
        public Historical(ArrayList data)
        {
            TimeStamp = Convert.ToDateTime(data[0]);
            Open = Convert.ToDecimal(data[1]);
            High = Convert.ToDecimal(data[2]);
            Low = Convert.ToDecimal(data[3]);
            Close = Convert.ToDecimal(data[4]);
            Volume = Convert.ToUInt32(data[5]);
        }

        public DateTime TimeStamp { get; }
        public decimal Open { get; }
        public decimal High { get; }
        public decimal Low { get; }
        public decimal Close { get; }
        public UInt32 Volume { get; }
    }

    /// <summary>
    /// Holding structure
    /// </summary>
    public struct Holding
    {
        public Holding(Dictionary<string, dynamic> data)
        {
            try
            {
                Product = Convert.ToString(data["product"]);
                Exchange = Convert.ToString(data["exchange"]);
                Price = Convert.ToDecimal(data["price"]);
                LastPrice = Convert.ToDecimal(data["last_price"]);
                CollateralQuantity = (int)data["collateral_quantity"];
                PNL = Convert.ToDecimal(data["pnl"]);
                ClosePrice = Convert.ToDecimal(data["close_price"]);
                AveragePrice = Convert.ToDecimal(data["average_price"]);
                TradingSymbol = Convert.ToString(data["tradingsymbol"]);
                CollateralType = Convert.ToString(data["collateral_type"]);
                T1Quantity = (int)data["t1_quantity"];
                InstrumentToken = Convert.ToUInt32(data["instrument_token"]);
                ISIN = Convert.ToString(data["isin"]);
                RealisedQuantity = (int)data["realised_quantity"];
                Quantity = (int)data["quantity"];
            }
            catch (Exception)
            {
                throw new DataException("Unable to parse data. " + Utils.JsonSerialize(data));
            }
        }

        public string Product { get; set; }
        public string Exchange { get; set; }
        public decimal Price { get; set; }
        public decimal LastPrice { get; set; }
        public int CollateralQuantity { get; set; }
        public decimal PNL { get; set; }
        public decimal ClosePrice { get; set; }
        public decimal AveragePrice { get; set; }
        public string TradingSymbol { get; set; }
        public string CollateralType { get; set; }
        public int T1Quantity { get; set; }
        public UInt32 InstrumentToken { get; set; }
        public string ISIN { get; set; }
        public int RealisedQuantity { get; set; }
        public int Quantity { get; set; }
    }

    /// <summary>
    /// Available margin structure
    /// </summary>
    public struct AvailableMargin
    {
        public AvailableMargin(Dictionary<string, dynamic> data)
        {
            try
            {
                AdHocMargin = data["adhoc_margin"];
                Cash = data["cash"];
                Collateral = data["collateral"];
                IntradayPayin = data["intraday_payin"];
            }
            catch (Exception)
            {
                throw new DataException("Unable to parse data. " + Utils.JsonSerialize(data));
            }
        }

        public decimal AdHocMargin { get; set; }
        public decimal Cash { get; set; }
        public decimal Collateral { get; set; }
        public decimal IntradayPayin { get; set; }
    }

    /// <summary>
    /// Utilised margin structure
    /// </summary>
    public struct UtilisedMargin
    {
        public UtilisedMargin(Dictionary<string, dynamic> data)
        {
            try
            {
                Debits = data["debits"];
                Exposure = data["exposure"];
                M2MRealised = data["m2m_realised"];
                M2MUnrealised = data["m2m_unrealised"];
                OptionPremium = data["option_premium"];
                Payout = data["payout"];
                Span = data["span"];
                HoldingSales = data["holding_sales"];
                Turnover = data["turnover"];
            }
            catch (Exception)
            {
                throw new DataException("Unable to parse data. " + Utils.JsonSerialize(data));
            }
        }

        public decimal Debits { get; set; }
        public decimal Exposure { get; set; }
        public decimal M2MRealised { get; set; }
        public decimal M2MUnrealised { get; set; }
        public decimal OptionPremium { get; set; }
        public decimal Payout { get; set; }
        public decimal Span { get; set; }
        public decimal HoldingSales { get; set; }
        public decimal Turnover { get; set; }

    }

    /// <summary>
    /// UserMargin structure
    /// </summary>
    public struct UserMargin
    {
        public UserMargin(Dictionary<string, dynamic> data)
        {
            try
            {
                Enabled = data["enabled"];
                Net = data["net"];
                Available = new AvailableMargin(data["available"]);
                Utilised = new UtilisedMargin(data["utilised"]);
            }
            catch (Exception)
            {
                throw new DataException("Unable to parse data. " + Utils.JsonSerialize(data));
            }
        }

        public bool Enabled { get; set; }
        public decimal Net { get; set; }
        public AvailableMargin Available { get; set; }
        public UtilisedMargin Utilised { get; set; }
    }

    /// <summary>
    /// User margins response structure
    /// </summary>
    public struct UserMarginsResponse
    {
        public UserMarginsResponse(Dictionary<string, dynamic> data)
        {
            try
            {
                Equity = new UserMargin(data["equity"]);
                Commodity = new UserMargin(data["commodity"]);
            }
            catch (Exception)
            {
                throw new DataException("Unable to parse data. " + Utils.JsonSerialize(data));
            }
        }
        public UserMargin Equity { get; set; }
        public UserMargin Commodity { get; set; }
    }

    /// <summary>
    /// UserMargin structure
    /// </summary>
    public struct InstrumentMargin
    {
        public InstrumentMargin(Dictionary<string, dynamic> data)
        {
            try
            {
                Margin = data["margin"];
                COLower = data["co_lower"];
                MISMultiplier = data["mis_multiplier"];
                Tradingsymbol = data["tradingsymbol"];
                COUpper = data["co_upper"];
                NRMLMargin = data["nrml_margin"];
                MISMargin = data["mis_margin"];
            }
            catch (Exception)
            {
                throw new DataException("Unable to parse data. " + Utils.JsonSerialize(data));
            }
        }

        public string Tradingsymbol { get; set; }
        public decimal Margin { get; set; }
        public decimal COLower { get; set; }
        public decimal COUpper { get; set; }
        public decimal MISMultiplier { get; set; }
        public decimal MISMargin { get; set; }
        public decimal NRMLMargin { get; set; }
    }
    /// <summary>
    /// Position structure
    /// </summary>
    public struct Position
    {
        public Position(Dictionary<string, dynamic> data)
        {
            try
            {
                Product = Convert.ToString(data["product"]);
                OvernightQuantity = (int)data["overnight_quantity"];
                Exchange = Convert.ToString(data["exchange"]);
                SellValue = Convert.ToDouble(data["sell_value"]);
                BuyM2M = Convert.ToDouble(data["buy_m2m"]);
                LastPrice = Convert.ToDouble(data["last_price"]);
                TradingSymbol = Convert.ToString(data["tradingsymbol"]);
                Realised = Convert.ToDouble(data["realised"]);
                PNL = Convert.ToDouble(data["pnl"]);
                Multiplier = Convert.ToDouble(data["multiplier"]);
                SellQuantity = (int)data["sell_quantity"];
                SellM2M = Convert.ToDouble(data["sell_m2m"]);
                BuyValue = Convert.ToDouble(data["buy_value"]);
                BuyQuantity = (int)data["buy_quantity"];
                AveragePrice = Convert.ToDouble(data["average_price"]);
                Unrealised = Convert.ToDouble(data["unrealised"]);
                Value = Convert.ToDouble(data["value"]);
                BuyPrice = Convert.ToDouble(data["buy_price"]);
                SellPrice = Convert.ToDouble(data["sell_price"]);
                M2M = Convert.ToDouble(data["m2m"]);
                InstrumentToken = Convert.ToUInt32(data["instrument_token"]);
                ClosePrice = Convert.ToDouble(data["close_price"]);
                Quantity = (int)data["quantity"];
                DayBuyQuantity = (int)(data["day_buy_quantity"]);
                DayBuyValue = Convert.ToDouble(data["day_buy_value"]);
                DayBuyPrice = Convert.ToDouble(data["day_buy_price"]);
                DaySellQuantity = (int)data["day_sell_quantity"];
                DaySellValue = Convert.ToDouble(data["day_sell_value"]);
                DaySellPrice = Convert.ToDouble(data["day_sell_price"]);
            }
            catch (Exception e)
            {
                throw new DataException("Unable to parse data. "+e.Message+" "+ Utils.JsonSerialize(data));
            }

        }

        public string Product { get; }
        public int OvernightQuantity { get; }
        public string Exchange { get; }
        public double SellValue { get; }
        public double BuyM2M { get; }
        public double LastPrice { get; }
        public string TradingSymbol { get; }
        public double Realised { get; }
        public double PNL { get; }
        public double Multiplier { get; }
        public int SellQuantity { get; }
        public double SellM2M { get; }
        public double BuyValue { get; }
        public int BuyQuantity { get; }
        public double AveragePrice { get; }
        public double Unrealised { get; }
        public double Value { get; }
        public double BuyPrice { get; }
        public double SellPrice { get; }
        public double M2M { get; }
        public UInt32 InstrumentToken { get; }
        public double ClosePrice { get; }
        public int Quantity { get; }
        public int DayBuyQuantity { get; }
        public double DayBuyPrice { get; }
        public double DayBuyValue { get; }
        public int DaySellQuantity { get; }
        public double DaySellPrice { get; }
        public double DaySellValue { get; }
    }

    /// <summary>
    /// Position response structure
    /// </summary>
    public struct PositionResponse
    {
        public PositionResponse(Dictionary<string, dynamic> data)
        {
            Day = new List<Position>();
            Net = new List<Position>();

            foreach (Dictionary<string, dynamic> item in data["day"])
                Day.Add(new Position(item));
            foreach (Dictionary<string, dynamic> item in data["net"])
                Net.Add(new Position(item));
        }

        public List<Position> Day { get; }
        public List<Position> Net { get; }
    }

    /// <summary>
    /// Order structure
    /// </summary>
    public struct Order
    {
        public Order(Dictionary<string, dynamic> data)
        {
            try
            {
                AveragePrice = Convert.ToDecimal(data["average_price"]);
                CancelledQuantity = (int)data["cancelled_quantity"];
                DisclosedQuantity = (int)data["disclosed_quantity"];
                Exchange = Convert.ToString(data["exchange"]);
                ExchangeOrderId = Convert.ToString(data["exchange_order_id"]);
                ExchangeTimestamp = Utils.StringToDate(data["exchange_timestamp"]);
                FilledQuantity = (int)data["filled_quantity"];
                InstrumentToken = Convert.ToUInt32(data["instrument_token"]);
                OrderId = Convert.ToString(data["order_id"]);
                OrderTimestamp = Utils.StringToDate(data["order_timestamp"]);
                OrderType = Convert.ToString(data["order_type"]);
                ParentOrderId = Convert.ToString(data["parent_order_id"]);
                PendingQuantity = (int)data["pending_quantity"];
                PlacedBy = Convert.ToString(data["placed_by"]);
                Price = Convert.ToDecimal(data["price"]);
                Product = Convert.ToString(data["product"]);
                Quantity = (int)data["quantity"];
                Status = Convert.ToString(data["status"]);
                StatusMessage = Convert.ToString(data["status_message"]);
                Tag = Convert.ToString(data["tag"]);
                Tradingsymbol = Convert.ToString(data["tradingsymbol"]);
                TransactionType = Convert.ToString(data["transaction_type"]);
                TriggerPrice = Convert.ToDecimal(data["trigger_price"]);
                Validity = Convert.ToString(data["validity"]);
                Variety = Convert.ToString(data["variety"]);
            }
            catch (Exception)
            {
                throw new DataException("Unable to parse data. " + Utils.JsonSerialize(data));
            }
        }

        public decimal AveragePrice { get; set; }
        public int CancelledQuantity { get; set; }
        public int DisclosedQuantity { get; set; }
        public string Exchange { get; set; }
        public string ExchangeOrderId { get; set; }
        public DateTime? ExchangeTimestamp { get; set; }
        public int FilledQuantity { get; set; }
        public UInt32 InstrumentToken { get; set; }
        public string OrderId { get; set; }
        public DateTime? OrderTimestamp { get; set; }
        public string OrderType { get; set; }
        public string ParentOrderId { get; set; }
        public int PendingQuantity { get; set; }
        public string PlacedBy { get; set; }
        public decimal Price { get; set; }
        public string Product { get; set; }
        public int Quantity { get; set; }
        public string Status { get; set; }
        public string StatusMessage { get; set; }
        public string Tag { get; set; }
        public string Tradingsymbol { get; set; }
        public string TransactionType { get; set; }
        public decimal TriggerPrice { get; set; }
        public string Validity { get; set; }
        public string Variety { get; set; }
    }

    /// <summary>
    /// Instrument structure
    /// </summary>
    public struct Instrument
    {
        public Instrument(Dictionary<string, dynamic> data)
        {
            try
            {
                InstrumentToken = Convert.ToUInt32(data["instrument_token"]);
                ExchangeToken = Convert.ToUInt32(data["exchange_token"]);
                TradingSymbol = data["tradingsymbol"];
                Name = data["name"];
                LastPrice = Convert.ToDecimal(data["last_price"]);
                TickSize = Convert.ToDecimal(data["tick_size"]);
                Expiry = Utils.StringToDate(data["expiry"]);
                InstrumentType = data["instrument_type"];
                Segment = data["segment"];
                Exchange = data["exchange"];

                if (data["strike"].Contains("e"))
                    Strike = Decimal.Parse(data["strike"], System.Globalization.NumberStyles.Float);
                else
                    Strike = Convert.ToDecimal(data["strike"]);

                LotSize = Convert.ToUInt32(data["lot_size"]);
            }
            catch (Exception)
            {
                throw new DataException("Unable to parse data. " + Utils.JsonSerialize(data));
            }

        }

        public Instrument(string v) : this()
        {
        }

        public UInt32 InstrumentToken { get; set; }
        public UInt32 ExchangeToken { get; set; }
        public string TradingSymbol { get; set; }
        public string Name { get; set; }
        public decimal LastPrice { get; set; }
        public decimal TickSize { get; set; }
        public DateTime? Expiry { get; set; }
        public string InstrumentType { get; set; }
        public string Segment { get; set; }
        public string Exchange { get; set; }
        public decimal Strike { get; set; }
        public UInt32 LotSize { get; set; }
       
    }

    /// <summary>
    /// Trade structure
    /// </summary>
    public struct Trade
    {
        public Trade(Dictionary<string, dynamic> data)
        {
            try
            {
                TradeId = data["trade_id"];
                OrderId = data["order_id"];
                ExchangeOrderId = data["exchange_order_id"];
                Tradingsymbol = data["tradingsymbol"];
                Exchange = data["exchange"];
                InstrumentToken = Convert.ToUInt32(data["instrument_token"]);
                TransactionType = data["transaction_type"];
                Product = data["product"];
                AveragePrice = data["average_price"];
                Quantity = data["quantity"];
                FillTimestamp = Utils.StringToDate(data["fill_timestamp"]);
                ExchangeTimestamp = Utils.StringToDate(data["exchange_timestamp"]);
            }
            catch (Exception)
            {
                throw new DataException("Unable to parse data. " + Utils.JsonSerialize(data));
            }

        }

        public string TradeId { get; }
        public string OrderId { get; }
        public string ExchangeOrderId { get; }
        public string Tradingsymbol { get; }
        public string Exchange { get; }
        public UInt32 InstrumentToken { get; }
        public string TransactionType { get; }
        public string Product { get; }
        public decimal AveragePrice { get; }
        public int Quantity { get; }
        public DateTime? FillTimestamp { get; }
        public DateTime? ExchangeTimestamp { get; }
    }

    /// <summary>
    /// Trigger range structure
    /// </summary>
    public struct TrigerRange
    {
        public TrigerRange(Dictionary<string, dynamic> data)
        {
            try
            {
                Start = data["start"];
                End = data["end"];
                Percent = data["percent"];
            }
            catch (Exception)
            {
                throw new DataException("Unable to parse data. " + Utils.JsonSerialize(data));
            }

        }
        public decimal Start { get; }
        public decimal End { get; }
        public decimal Percent { get; }
    }

    /// <summary>
    /// User structure
    /// </summary>
    public struct User
    {
        public User(Dictionary<string, dynamic> data)
        {
            try
            {
                APIKey = data["data"]["api_key"];
                Products = (string[])data["data"]["products"].ToArray(typeof(string));
                UserName = data["data"]["user_name"];
                UserShortName = data["data"]["user_shortname"];
                AvatarURL = data["data"]["avatar_url"];
                Broker = data["data"]["broker"];
                AccessToken = data["data"]["access_token"];
                PublicToken = data["data"]["public_token"];
                RefreshToken = data["data"]["refresh_token"];
                UserType = data["data"]["user_type"];
                UserId = data["data"]["user_id"];
                LoginTime = Utils.StringToDate(data["data"]["login_time"]);
                Exchanges = (string[])data["data"]["exchanges"].ToArray(typeof(string));
                OrderTypes = (string[])data["data"]["order_types"].ToArray(typeof(string));
                Email = data["data"]["email"];
            }
            catch (Exception)
            {
                throw new DataException("Unable to parse data. " + Utils.JsonSerialize(data));
            }

        }

        public string APIKey { get; }
        public string[] Products { get; }
        public string UserName { get; }
        public string UserShortName { get; }
        public string AvatarURL { get; }
        public string Broker { get; }
        public string AccessToken { get; }
        public string PublicToken { get; }
        public string RefreshToken { get; }
        public string UserType { get; }
        public string UserId { get; }
        public DateTime? LoginTime { get; }
        public string[] Exchanges { get; }
        public string[] OrderTypes { get; }
        public string Email { get; }
    }

    public struct TokenSet
    {
        public TokenSet(Dictionary<string, dynamic> data)
        {
            try
            {
                UserId = data["data"]["user_id"];
                AccessToken = data["data"]["access_token"];
                RefreshToken = data["data"]["refresh_token"];
            }
            catch (Exception)
            {
                throw new DataException("Unable to parse data. " + Utils.JsonSerialize(data));
            }
        }
        public string UserId { get; }
        public string AccessToken { get; }
        public string RefreshToken { get; }
    }

    /// <summary>
    /// User structure
    /// </summary>
    public struct Profile
    {
        public Profile(Dictionary<string, dynamic> data)
        {
            try
            {
                Products = (string[])data["data"]["products"].ToArray(typeof(string));
                UserName = data["data"]["user_name"];
                UserShortName = data["data"]["user_shortname"];
                AvatarURL = data["data"]["avatar_url"];
                Broker = data["data"]["broker"];
                UserType = data["data"]["user_type"];
                Exchanges = (string[])data["data"]["exchanges"].ToArray(typeof(string));
                OrderTypes = (string[])data["data"]["order_types"].ToArray(typeof(string));
                Email = data["data"]["email"];
            }
            catch (Exception)
            {
                throw new DataException("Unable to parse data. " + Utils.JsonSerialize(data));
            }

        }


        public string[] Products { get; }
        public string UserName { get; }
        public string UserShortName { get; }
        public string AvatarURL { get; }
        public string Broker { get; }
        public string UserType { get; }
        public string[] Exchanges { get; }
        public string[] OrderTypes { get; }
        public string Email { get; }
    }

    /// <summary>
    /// Quote structure
    /// </summary>
    public  struct Quote
    {
        public Quote(Dictionary<string, dynamic> data)
        {
            try
            {
                InstrumentToken = Convert.ToUInt32(data["instrument_token"]);
                Timestamp = Utils.StringToDate(data["timestamp"]);
                LastPrice = data["last_price"];
                LastQuantity = Convert.ToUInt32(data["last_quantity"]);
                LastTradeTime = Utils.StringToDate(data["last_trade_time"]);
                AveragePrice = data["average_price"];
                Volume = Convert.ToUInt32(data["volume"]);

                BuyQuantity = Convert.ToUInt32(data["buy_quantity"]);
                SellQuantity = Convert.ToUInt32(data["sell_quantity"]);

                Open = data["ohlc"]["open"];
                Close = data["ohlc"]["close"];
                Low = data["ohlc"]["low"];
                High = data["ohlc"]["high"];

                Change = data["net_change"];

                OI = Convert.ToUInt32(data["oi"]);

                OIDayHigh = Convert.ToUInt32(data["oi_day_high"]);
                OIDayLow = Convert.ToUInt32(data["oi_day_low"]);

                Bids = new List<DepthItem>();
                Offers = new List<DepthItem>();

                foreach (Dictionary<string, dynamic> bid in data["depth"]["buy"])
                    Bids.Add(new DepthItem(bid));

                foreach (Dictionary<string, dynamic> offer in data["depth"]["sell"])
                    Offers.Add(new DepthItem(offer));
            }
            catch (Exception)
            {
                throw new DataException("Unable to parse data. " + Utils.JsonSerialize(data));
            }

        }

        public UInt32 InstrumentToken { get; set; }
        public decimal LastPrice { get; set; }
        public UInt32 LastQuantity { get; set; }
        public decimal AveragePrice { get; set; }
        public UInt32 Volume { get; set; }
        public UInt32 BuyQuantity { get; set; }
        public UInt32 SellQuantity { get; set; }
        public decimal Open { get; set; }
        public decimal High { get; set; }
        public decimal Low { get; set; }
        public decimal Close { get; set; }
        public decimal Change { get; set; }
        public List<DepthItem> Bids { get; set; }
        public List<DepthItem> Offers { get; set; }

        // KiteConnect 3 Fields

        public DateTime? LastTradeTime { get; set; }
        public UInt32 OI { get; set; }
        public UInt32 OIDayHigh { get; set; }
        public UInt32 OIDayLow { get; set; }
        public DateTime? Timestamp { get; set; }
    }

    /// <summary>
    /// OHLC Quote structure
    /// </summary>
    public struct OHLC
    {
        public OHLC(Dictionary<string, dynamic> data)
        {
            try
            {
                InstrumentToken = Convert.ToUInt32(data["instrument_token"]);
                LastPrice = data["last_price"];

                Open = data["ohlc"]["open"];
                Close = data["ohlc"]["close"];
                Low = data["ohlc"]["low"];
                High = data["ohlc"]["high"];
            }
            catch (Exception)
            {
                throw new DataException("Unable to parse data. " + Utils.JsonSerialize(data));
            }

        }
        public UInt32 InstrumentToken { get; set; }
        public decimal LastPrice { get; }
        public decimal Open { get; }
        public decimal Close { get; }
        public decimal High { get; }
        public decimal Low { get; }
    }

    /// <summary>
    /// LTP Quote structure
    /// </summary>
    public struct LTP
    {
        public LTP(Dictionary<string, dynamic> data)
        {
            try
            {
                InstrumentToken = Convert.ToUInt32(data["instrument_token"]);
                LastPrice = data["last_price"];
            }
            catch (Exception)
            {
                throw new DataException("Unable to parse data. " + Utils.JsonSerialize(data));
            }

        }
        public UInt32 InstrumentToken { get; set; }
        public decimal LastPrice { get; }
    }

    /// <summary>
    /// Mutual funds holdings structure
    /// </summary>
    public struct MFHolding
    {
        public MFHolding(Dictionary<string, dynamic> data)
        {
            try
            {
                Quantity = data["quantity"];
                Fund = data["fund"];
                Folio = data["folio"];
                AveragePrice = data["average_price"];
                TradingSymbol = data["tradingsymbol"];
                LastPrice = data["last_price"];
                PNL = data["pnl"];
            }
            catch (Exception)
            {
                throw new DataException("Unable to parse data. " + Utils.JsonSerialize(data));
            }
        }

        public decimal Quantity { get; }
        public string Fund { get; }
        public string Folio { get; }
        public decimal AveragePrice { get; }
        public string TradingSymbol { get; }
        public decimal LastPrice { get; }
        public decimal PNL { get; }
    }

    /// <summary>
    /// Mutual funds instrument structure
    /// </summary>
    public struct MFInstrument
    {
        public MFInstrument(Dictionary<string, dynamic> data)
        {
            try
            {
                TradingSymbol = data["tradingsymbol"];
                AMC = data["amc"];
                Name = data["name"];

                PurchaseAllowed = data["purchase_allowed"] == "1";
                RedemtpionAllowed = data["redemption_allowed"] == "1";

                MinimumPurchaseAmount = Convert.ToDecimal(data["minimum_purchase_amount"]);
                PurchaseAmountMultiplier = Convert.ToDecimal(data["purchase_amount_multiplier"]);
                MinimumAdditionalPurchaseAmount = Convert.ToDecimal(data["minimum_additional_purchase_amount"]);
                MinimumRedemptionQuantity = Convert.ToDecimal(data["minimum_redemption_quantity"]);
                RedemptionQuantityMultiplier = Convert.ToDecimal(data["redemption_quantity_multiplier"]);
                LastPrice = Convert.ToDecimal(data["last_price"]);

                DividendType = data["dividend_type"];
                SchemeType = data["scheme_type"];
                Plan = data["plan"];
                SettlementType = data["settlement_type"];
                LastPriceDate = Utils.StringToDate(data["last_price_date"]);
            }
            catch (Exception)
            {
                throw new DataException("Unable to parse data. " + Utils.JsonSerialize(data));
            }

        }

        public string TradingSymbol { get; }
        public string AMC { get; }
        public string Name { get; }

        public bool PurchaseAllowed { get; }
        public bool RedemtpionAllowed { get; }

        public decimal MinimumPurchaseAmount { get; }
        public decimal PurchaseAmountMultiplier { get; }
        public decimal MinimumAdditionalPurchaseAmount { get; }
        public decimal MinimumRedemptionQuantity { get; }
        public decimal RedemptionQuantityMultiplier { get; }
        public decimal LastPrice { get; }

        public string DividendType { get; }
        public string SchemeType { get; }
        public string Plan { get; }
        public string SettlementType { get; }
        public DateTime? LastPriceDate { get; }
    }

    /// <summary>
    /// Mutual funds order structure
    /// </summary>
    public struct MFOrder
    {
        public MFOrder(Dictionary<string, dynamic> data)
        {
            try
            {
                StatusMessage = data["status_message"];
                PurchaseType = data["purchase_type"];
                PlacedBy = data["placed_by"];
                Amount = data["amount"];
                Quantity = data["quantity"];
                SettlementId = data["settlement_id"];
                OrderTimestamp = Utils.StringToDate(data["order_timestamp"]);
                AveragePrice = data["average_price"];
                TransactionType = data["transaction_type"];
                ExchangeOrderId = data["exchange_order_id"];
                ExchangeTimestamp = Utils.StringToDate(data["exchange_timestamp"]);
                Fund = data["fund"];
                Variety = data["variety"];
                Folio = data["folio"];
                Tradingsymbol = data["tradingsymbol"];
                Tag = data["tag"];
                OrderId = data["order_id"];
                Status = data["status"];
                LastPrice = data["last_price"];
            }
            catch (Exception)
            {
                throw new DataException("Unable to parse data. " + Utils.JsonSerialize(data));
            }

        }

        public string StatusMessage { get; }
        public string PurchaseType { get; }
        public string PlacedBy { get; }
        public decimal Amount { get; }
        public decimal Quantity { get; }
        public string SettlementId { get; }
        public DateTime? OrderTimestamp { get; }
        public decimal AveragePrice { get; }
        public string TransactionType { get; }
        public string ExchangeOrderId { get; }
        public DateTime? ExchangeTimestamp { get; }
        public string Fund { get; }
        public string Variety { get; }
        public string Folio { get; }
        public string Tradingsymbol { get; }
        public string Tag { get; }
        public string OrderId { get; }
        public string Status { get; }
        public decimal LastPrice { get; }
    }

    /// <summary>
    /// Mutual funds SIP structure
    /// </summary>
    public struct MFSIP
    {
        public MFSIP(Dictionary<string, dynamic> data)
        {
            try
            {
                DividendType = data["dividend_type"];
                PendingInstalments = data["pending_instalments"];
                Created = Utils.StringToDate(data["created"]);
                LastInstalment = Utils.StringToDate(data["last_instalment"]);
                TransactionType = data["transaction_type"];
                Frequency = data["frequency"];
                InstalmentDate = data["instalment_date"];
                Fund = data["fund"];
                SIPId = data["sip_id"];
                Tradingsymbol = data["tradingsymbol"];
                Tag = data["tag"];
                InstalmentAmount = data["instalment_amount"];
                Instalments = data["instalments"];
                Status = data["status"];
                OrderId = data.ContainsKey(("order_id")) ? data["order_id"] : "";
            }
            catch (Exception)
            {
                throw new DataException("Unable to parse data. " + Utils.JsonSerialize(data));
            }

        }

        public string DividendType { get; }
        public int PendingInstalments { get; }
        public DateTime? Created { get; }
        public DateTime? LastInstalment { get; }
        public string TransactionType { get; }
        public string Frequency { get; }
        public int InstalmentDate { get; }
        public string Fund { get; }
        public string SIPId { get; }
        public string Tradingsymbol { get; }
        public string Tag { get; }
        public int InstalmentAmount { get; }
        public int Instalments { get; }
        public string Status { get; }
        public string OrderId { get; }
    }


    public struct RealTimeData
    {
        public RealTimeData(Tick data)
        {
            try
            {
                Open = data.Open;
                High = data.High;

                Close = data.Close;
                Low = data.Low;
                Timestamp = DateTime.Now.ToString();//Convert.ToString(data.Timestamp);
                Volume = data.Volume;
            }
            catch (Exception)
            {
                throw new DataException("Unable to parse data. " + Utils.JsonSerialize(data));
            }
        }
        public decimal Open { get; set; }
        public decimal High { get; set; }
        public decimal Low { get; set; }
        public decimal Close { get; set; }
        public UInt32 Volume { get; set; }

        // KiteConnect 3 Fields

        public string Timestamp { get; set; }
    }

        //sanika::16-oct-2020::added to store position information
        public struct LocalPositionStructure
        { 
            public string Product { get; set; }
            public string Exchange { get; set; } 
            public string TradingSymbol { get; set; }
            public string TransactionType  { get; set; }            
            public int Quantity { get; set; }
            public string OrderID { get; set; }
            public DateTime PlacedOrderDateTime { get; set; }
            public string ClosedTransactionType { get; set; }
            public int ClosedQuantity { get; set; }
            public string ClosedOrderID { get; set; }
            public DateTime ClosedPlacedOrderDateTime { get; set; }
        }

    public struct OrdersPlaced
    {
        public string TradingSymbol { get; set; }
        public string Exchange { get; set; }
        public string TransactionType { get; set; }
        public decimal TriggerPrice { get; set; }
        public decimal Price { get; set; }
        public string Product { get; set; }
        public int Quantity { get; set; }
        public string OrderType { get; set; }
    }
}
