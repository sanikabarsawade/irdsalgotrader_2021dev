﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRDSAlgoOMS
{
    
    public class PositionInfo
    {
        public Position dayPosition { get; set; }
        public Position netPosition { get; set; }
        public DateTime LastUpdateTime { get; set; }
        public PositionInfo() {}
    }
}
