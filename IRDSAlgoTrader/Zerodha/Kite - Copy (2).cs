﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Security.Cryptography;
using KiteConnect;
using System.Web;
using System.Web.Script.Serialization;
using Microsoft.VisualBasic.FileIO;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Data.SQLite;
using System.Collections.Concurrent;
using System.Globalization;
using MySql.Data.MySqlClient;
using System.Windows;
using IRDSAlgoOMS.Zerodha;

namespace IRDSAlgoOMS
{
    public class Kite
    {
        // instance of ticker
        static Ticker ticker;
        public bool bOnExpiry = false;
        public string apiKey;
        public string apiSecret;
        public string username;
        public string password;
        public string root;
        public string login;
        public string scripts;
        public string Pin;
        string[] value;
        public static string volumeLimit = "";

        public string accessToken = "t4qslbmicuz8newegffp5s2jxvds58e8";
        // intialize the token
        public string MyPublicToken = "t4qslbmicuz8newegffp5s2jxvds58e8";

        public string m_accessTokenDownloadHistoricalData = "";
        public string m_MyPublicTokenDownloadHistoricalData = "";
        public string apiKeyDownloadHistoricalData;
        public string apiSecretDownloadHistoricalData;
        public string usernameDownloadHistoricalData;
        public string passwordDownloadHistoricalData;
        public string rootDownloadHistoricalData;
        public string loginDownloadHistoricalData;      
        public string PinDownloadHistoricalData;

        //IRDS::Bhagyashri::26-Mar-2018::Empty string to get request token
        public string requestToken = "";
        //IRDS::Bhagyashri::27-Mar-2018::Manual Login
        public bool manualLogin = false;

        private OrderStore m_GlobalOrderStore;
        public bool debug;
        public WebProxy proxy;
        public int timeout;
        public Action sessionHook;
        public bool enableLogging;

        //IRDS::03-Jul-2020::Jyoti::added for database settings from ini file
        public string dbServer = "";
        public string dbUserid = "";
        public string dbPassword = "";
        public string databaseName = "";
        public Dictionary<string, List<string>> FinalZerodhaTickData = new Dictionary<string, List<string>>();
        //public Dictionary<string, DateTime> TickStartTime = new Dictionary<string, DateTime>();

        public static Logger loggerClient;
        private static readonly object databaselock = new object();
        public Dictionary<string, ConcurrentQueue<Tick>> m_QueueOfAllSymbols = new Dictionary<string, ConcurrentQueue<Tick>>();
        public Dictionary<string, Thread> m_ThreadsOfAllSymbols = new Dictionary<string, Thread>();
        //IRDS::29-Jul-2020::Jyoti::added for queue changes
        //IRDS::04-August-2020::Sandip::Change concurrent queue
        ConcurrentQueue<Tick> m_queueTick = new ConcurrentQueue<Tick>();

        //IRDS::Sanika::26-Jun-2019::Added for real time data from kite

        public static DateTime Start;
        public static DateTime StartNSE;
        public static DateTime StartNFO;
        public static DateTime StartMCX;
        //sanika::2-dec-2020::Added for cds
        public static DateTime StartCDS;

        public static DateTime EndTimeNSE;
        public static DateTime EndTimeNFO;
        public static DateTime EndTimeMCX;
        //sanika::2-dec-2020::Added for cds
        public static DateTime EndTimeCDS;
        public static DateTime TmpStart;
        public static DateTime End;
        public static string Interval = "";
        public static string EndInterval = "";
        public static DateTime TickEnd;
        public static DateTime _1MinTickEnd;
        public static DateTime FirstTickEnd;
        public bool LoginFlag = false;
   
        public List<string> RealTimeSymbols = new List<string>();
        public List<string> TradingSymbol = new List<string>();
        public ConcurrentDictionary<string, List<string>> ZerodhaData = new ConcurrentDictionary<string, List<string>>();      
        public Dictionary<string, List<string>> ZerodhaDatatmp;
        public Dictionary<string, List<string>> FinalZerodhaData = new Dictionary<string, List<string>>(); 
        public Dictionary<string, List<string>> TmpZerodhaData;
        public Dictionary<string, string> SymbolList = new Dictionary<string, string>();
        public Dictionary<string, string> SymbolListWithFutureName = new Dictionary<string, string>();        
        public List<string> TmpList;
        SQLiteConnection Sql_conFetchSymbolName;
        SQLiteConnection Sql_conTableName;
        SQLiteConnection Sql_Datacon;
        SQLiteConnection Sql_conForCreateandreadTable;
        SQLiteDataReader Sql_read;
        SQLiteTransaction Tr;        
        public static bool DownloadDataFlag = false;
        public static string FilePath = "";
        public List<string> ListOfSymbolsForStartValues = new List<string>();
        public List<string> ListStartOpenValues = new List<string>();
        public List<string> ListForFirstDataFrom5min = new List<string>();
        public Dictionary<string, string> OpenValues = new Dictionary<string, string>();
        public Dictionary<string, string> TmpOpenValues = new Dictionary<string, string>();
        public Dictionary<string, List<string>> dictionaryForCloseValues = new Dictionary<string, List<string>>();
        public Dictionary<string, List<string>> dictionaryForLastTickTime = new Dictionary<string, List<string>>();
        public Dictionary<string, string> dictionaryForInitialVolume = new Dictionary<string, string>();
        public List<string> listOfNSESymbols = new List<string>();
        public List<string> listOfNFOSymbols = new List<string>();
        public List<string> listOfMCXSymbols = new List<string>();
        //IRDS::Jyoti::9-Sept-20::Added for options symbols
        public List<string> listOfOPTSymbols = new List<string>();
        //sanika::2-dec-2020::Added for cds symbols
        public List<string> listOfCDSSymbols = new List<string>();

        //public Tick tick;
        public string tickCurrentTime = "";
        public string tickCurrent = "";
        public DateTime endTimeForTick;
        public string pathFile = Directory.GetCurrentDirectory();
        Logger logger;
        public string DBName = "zerodhadata.db";
        MySQLConnectZerodha m_Mysql;
        bool m_isConnected = false;
        bool m_isConnectedToSqlite = false;
        int m_previousTradingSymbolListCount = 0;
        public string m_ExceptionError = "";

        List<string> m_NSESymbolsInstruements = new List<string>();
        List<string> m_NFOSymbolsInstruements = new List<string>();
        List<string> m_MCXSymbolsInstruements = new List<string>();
        List<string> m_CDSSymbolsInstruements = new List<string>();

        //mysql flag by default false
        bool m_useMysql = true;
        bool m_useSqlite = false;
        bool m_useCSV = false;

        SqliteConnectZerodha sqliteConnectZerodha = null;

        TickData m_TickData = null;

        bool m_StopThread = false;
        //sanika::15-oct-2020::added for wrong toast display if relogin fails
        public bool m_ForceFulLoginFlag = false;

        //sanika::15-Dec-2020::Added to restart exe for ajay sir
        DateTime m_ExeRestartStartTime;
        DateTime m_ExeRestartEndTime;
       

        private readonly Dictionary<string, string> _routes = new Dictionary<string, string>
        {
            ["parameters"] = "/parameters",
            ["api.token"] = "/session/token",
            ["api.refresh"] = "/session/refresh_token",

            ["instrument.margins"] = "/margins/{segment}",

            ["user.profile"] = "/user/profile",
            ["user.margins"] = "/user/margins",
            ["user.segment_margins"] = "/user/margins/{segment}",

            ["orders"] = "/orders",
            ["trades"] = "/trades",
            ["orders.history"] = "/orders/{order_id}",

            ["orders.place"] = "/orders/{variety}",
            ["orders.modify"] = "/orders/{variety}/{order_id}",
            ["orders.cancel"] = "/orders/{variety}/{order_id}",
            ["orders.trades"] = "/orders/{order_id}/trades",

            ["portfolio.positions"] = "/portfolio/positions",
            ["portfolio.holdings"] = "/portfolio/holdings",
            ["portfolio.positions.modify"] = "/portfolio/positions",

            ["market.instruments.all"] = "/instruments",
            ["market.instruments"] = "/instruments/{exchange}",
            ["market.quote"] = "/quote",
            ["market.ohlc"] = "/quote/ohlc",
            ["market.ltp"] = "/quote/ltp",
            ["market.historical"] = "/instruments/historical/{instrument_token}/{interval}",
            ["market.trigger_range"] = "/instruments/{exchange}/{tradingsymbol}/trigger_range",

            ["mutualfunds.orders"] = "/mf/orders",
            ["mutualfunds.order"] = "/mf/orders/{order_id}",
            ["mutualfunds.orders.place"] = "/mf/orders",
            ["mutualfunds.cancel_order"] = "/mf/orders/{order_id}",

            ["mutualfunds.sips"] = "/mf/sips",
            ["mutualfunds.sips.place"] = "/mf/sips",
            ["mutualfunds.cancel_sips"] = "/mf/sips/{sip_id}",
            ["mutualfunds.sips.modify"] = "/mf/sips/{sip_id}",
            ["mutualfunds.sip"] = "/mf/sips/{sip_id}",

            ["mutualfunds.instruments"] = "/mf/instruments",
            ["mutualfunds.holdings"] = "/mf/holdings"
        };

        //IRDS::03-Jul-2020::Jyoti::added for Ajay Sir Changes
        public Dictionary<string, string> SymbolListWithExchanges = new Dictionary<string, string>();
        public Dictionary<string, string> SymbolListWithExpDate = new Dictionary<string, string>();
        public Dictionary<string, int> SymbolListExistingRecord = new Dictionary<string, int>();
        public Dictionary<string, string> SymbolListYestRecord = new Dictionary<string, string>();
        public Dictionary<string, string> DictMysqlData = new Dictionary<string, string>();
        public Dictionary<string, string> OldDictMysqlData = new Dictionary<string, string>();
        //IRDS::04-August-2020::Sandip::new variable.
        DateTime startDateTime = DateTime.Now;
        int m_SortCounter = 1;
        public Kite()
        {

            ////Console.WriteLine("Kite Class Object Initialize\n");
        }

        /// <summary>
        /// Initialize a new Kite Connect client instance.
        /// </summary>
        /// <param name="APIKey">API Key issued to you</param>
        /// <param name="AccessToken">The token obtained after the login flow in exchange for the `RequestToken` . 
        /// Pre-login, this will default to None,but once you have obtained it, you should persist it in a database or session to pass 
        /// to the Kite Connect class initialisation for subsequent requests.</param>
        /// <param name="Root">API end point root. Unless you explicitly want to send API requests to a non-default endpoint, this can be ignored.</param>
        /// <param name="Debug">If set to True, will serialise and print requests and responses to stdout.</param>
        /// <param name="Timeout">Time in milliseconds for which  the API client will wait for a request to complete before it fails</param>
        /// <param name="Proxy">To set proxy for http request. Should be an object of WebProxy.</param>
        /// <param name="Pool">Number of connections to server. Client will reuse the connections if they are alive.</param>
        public Kite(string APIKey, ConfigSettings objConfigSettings, string AccessToken = null, string Root = null, bool Debug = false, int Timeout = 7000, WebProxy Proxy = null, int Pool = 2)
        {
            accessToken = AccessToken;
            apiKey = APIKey;
            if (!String.IsNullOrEmpty(Root)) this.root = objConfigSettings.root;
            enableLogging = Debug;

            timeout = Timeout;
            proxy = Proxy;

            ServicePointManager.DefaultConnectionLimit = Pool;
            apiKey = objConfigSettings.apiKey;
            apiSecret = objConfigSettings.apiSecret;
            username = objConfigSettings.username;
            password = objConfigSettings.password;
            root = objConfigSettings.root;
            login = objConfigSettings.login;
            Pin = objConfigSettings.Pin;
            //if (objConfigSettings.ManualLogin == "false")
            
            string autologinVal = objConfigSettings.autoLogin;
            if (autologinVal.ToLower() == "true")
                manualLogin = false;
            else
                manualLogin = true;
            RealTimeSymbols = objConfigSettings.Symbol;
            //sanika::8-dec-2020::interval dynamic
            Interval = objConfigSettings.interval.ToString();
            string currentTime = DateTime.Now.ToString("dd-MM-yyyy");
            DateTime CurrentDateTime = DateTime.ParseExact(currentTime+" "+objConfigSettings.StartTimeNSE, "dd-MM-yyyy HH:mm:ss", null);
            StartNSE = CurrentDateTime;
            CurrentDateTime = DateTime.ParseExact(currentTime + " " + objConfigSettings.StartTimeNFO, "dd-MM-yyyy HH:mm:ss", null);
            StartNFO = CurrentDateTime;
            CurrentDateTime = DateTime.ParseExact(currentTime + " " + objConfigSettings.StartTimeMCX, "dd-MM-yyyy HH:mm:ss", null);
            StartMCX = CurrentDateTime;
            //sanika::2-dec-2020::added for cds
            CurrentDateTime = DateTime.ParseExact(currentTime + " " + objConfigSettings.StartTimeCDS, "dd-MM-yyyy HH:mm:ss", null);
            StartCDS = CurrentDateTime;

            //sanika::9-dec-2020::To calculate start time as per interval
            currentTime = DateTime.Now.ToString("dd-MM-yyyy HH:mm") + ":00";
            CurrentDateTime = DateTime.ParseExact(currentTime, "dd-MM-yyyy HH:mm:ss", null);
            if (CurrentDateTime < StartNSE)
                Start = StartNSE;
            else
                Start = RoundUp(CurrentDateTime, TimeSpan.FromMinutes(Convert.ToInt32(Interval)));
            
            EndInterval = (Convert.ToInt32(Interval) + 1).ToString();
            End = Start.AddSeconds(60 * Convert.ToInt32(EndInterval));
            TickEnd = Start.AddSeconds(60 * Convert.ToInt32(Interval));
            FirstTickEnd = TickEnd;
            _1MinTickEnd = Start.AddSeconds(60);
            TmpStart = Start;

            //sanika::18-Nov-2020::changed end time exchange wise
            string endT = DateTime.Now.ToString("dd-MM-yyyy") +" "+ objConfigSettings.EndTimeNSE;
            EndTimeNSE = DateTime.ParseExact(endT, "dd-MM-yyyy HH:mm:ss", null);
            endT = DateTime.Now.ToString("dd-MM-yyyy") + " " + objConfigSettings.EndTimeNFO;
            EndTimeNFO = DateTime.ParseExact(endT, "dd-MM-yyyy HH:mm:ss", null);
            endT = DateTime.Now.ToString("dd-MM-yyyy") + " " + objConfigSettings.EndTimeMCX;
            EndTimeMCX = DateTime.ParseExact(endT, "dd-MM-yyyy HH:mm:ss", null);
            //sanika::2-dec-2020::added for cds
            endT = DateTime.Now.ToString("dd-MM-yyyy") + " " + objConfigSettings.EndTimeCDS;
            EndTimeCDS = DateTime.ParseExact(endT, "dd-MM-yyyy HH:mm:ss", null);

            endTimeForTick = Start.AddSeconds(60 * Convert.ToInt32(Interval));
            m_useMysql = objConfigSettings.storeDataMysqlDB;
            m_useSqlite = objConfigSettings.storeDataSqliteDB;
            m_useCSV = objConfigSettings.storeDataCSV;
            //IRDS::03-Jul-2020::Jyoti::added for database settings from ini file
            dbServer = objConfigSettings.dbServer;
            dbUserid = objConfigSettings.dbUserid;
            dbPassword = objConfigSettings.dbPassword;
            databaseName = objConfigSettings.databaseName;
            m_bTickMCXDataDownloader = objConfigSettings.isTickMCXDataDownloader;

            //sanika::15-Dec-2020::Added to restart exe
            string datetime = DateTime.Now.ToString("dd-MM-yyyy");
            m_ExeRestartStartTime = DateTime.ParseExact(datetime + " 13:01:00", "dd-MM-yyyy HH:mm:ss", null);
            m_ExeRestartEndTime = DateTime.ParseExact(datetime + " 13:05:00", "dd-MM-yyyy HH:mm:ss", null);

            //IRDS::Sanika::31-Aug-2020::Added for historical data
            apiKeyDownloadHistoricalData = objConfigSettings.apiKeyHistorical;
            apiSecretDownloadHistoricalData = objConfigSettings.apiSecretHistorical;
            usernameDownloadHistoricalData = objConfigSettings.usernameHistorical;
            passwordDownloadHistoricalData = objConfigSettings.passwordHistorical;
            rootDownloadHistoricalData = objConfigSettings.rootHistorical;
            loginDownloadHistoricalData = objConfigSettings.loginHistorical;
            PinDownloadHistoricalData = objConfigSettings.PinHistorical;
        }

        //sanika::9-dec-2020::To calculate start time as per interval
        DateTime RoundUp(DateTime dt, TimeSpan d)
        {
            return new DateTime((dt.Ticks + d.Ticks - 1) / d.Ticks * d.Ticks, dt.Kind);
        }

        public void EnableLogging(bool _enableLogging)
        {
            enableLogging = _enableLogging;
        }

        public void initSeesion()
        {
            try
            {
                logger.LogMessage("initSeesion : Going to webdriver to login", MessageType.Informational);
                WebDriver webDriver = new WebDriver(manualLogin, username, password, Pin, logger);
                webDriver.getChromeDriver(login, apiKey); //Automation login using Selenium Webdriver with c#

                //IRDS::Bhagyashri::27-Mar-2018::Check the manual login is true or false   
                if (!manualLogin)
                {
                    //string requestToken = HttpUtility.ParseQueryString(webDriver.currentUrl).Get("request_token");

                    if (webDriver.token != "")
                    {
                        requestToken = webDriver.token;
                        logger.LogMessage("initSeesion : Retrived token", MessageType.Informational);
                    }
                    else
                    { 
                        string reqToken = webDriver.currentUrl;
                        Uri tmp = new Uri(reqToken);
                        //Console.WriteLine("Query: {0}", tmp.Query);
                        NameValueCollection Parms = HttpUtility.ParseQueryString(tmp.Query);
                        //Console.WriteLine("Parms: {0}", Parms.Count);

                        foreach (string x in Parms.AllKeys)
                        {
                            if (x.Contains("request_token"))
                            {
                                requestToken = Parms[x];
                                logger.LogMessage("initSeesion : Retrived token", MessageType.Informational);
                            }
                        }
                    }
                    //Console.Write("\nRequest Token : "+ requestToken);

                    User user = GenerateSession(requestToken, apiSecret);
                    //Console.WriteLine(Utils.JsonSerialize(user));

                    accessToken = user.AccessToken;
                    MyPublicToken = user.PublicToken;
                    //IRDS::03-Jul-2020::Jyoti::added for Ajay Sir Changes
                    LoginFlag = true;
                    //sanika::15-oct-2020::added for wrong toast display if relogin fails
                    m_ForceFulLoginFlag = true; 
                    writeCredentialIntoFile(accessToken, MyPublicToken);
                    logger.LogMessage("initSeesion : Wrote token in file", MessageType.Informational);
                }
                else
                {
                    //Manually insert request token
                    ///Console.Write("Enter request token : ");
                    // requestToken = Console.ReadLine();
                    if (webDriver.token != "")
                    {
                        requestToken = webDriver.token;
                        logger.LogMessage("initSeesion : Retrived token", MessageType.Informational);
                        User user = GenerateSession(requestToken, apiSecret);
                        accessToken = user.AccessToken;
                        MyPublicToken = user.PublicToken;

                        List<Instrument> list = GetInstruments(Constants.EXCHANGE_NSE);
                        //IRDSPM::Pratiksha::11-12-2020::For Not display messagebox
                        //if (list != null)
                        //{
                        //    MessageBox.Show("Logged in successfull!!");
                        //}
                        LoginFlag = true;
                        //sanika::15-oct-2020::added for wrong toast display if relogin fails
                        m_ForceFulLoginFlag = true;
                        writeCredentialIntoFile(accessToken, MyPublicToken);
                        logger.LogMessage("initSeesion : Wrote token in file", MessageType.Informational);
                    }
                    else
                    {
                        LoginFlag = false;
                    }
                }
            }
            catch (Exception e)
            {
                logger.LogMessage("initSeesion : Exception Error Message = "+e.Message, MessageType.Exception);
                m_ExceptionError = e.Message;
                LoginFlag = false;
            }
        }

        public void writeCredentialIntoFile(string accessToken, string MyPublicToken)
        {
            string currentDate = DateTime.Now.ToString("M-d-yyyy");
            var path_File = Directory.GetCurrentDirectory();
            string FilePath = path_File + @"\Configuration\" + "credentials_"+ username+".txt";
            string line = currentDate + " " + username + " " + accessToken + " " + MyPublicToken;
            using (StreamWriter streamWriter = new StreamWriter(FilePath, false))
            {
                streamWriter.WriteLine(line);
            }
        }

        /// Adds extra headers to request
        public void AddExtraHeaders(ref HttpWebRequest Req)
        {
            if (Assembly.GetEntryAssembly() != null)
                Req.UserAgent = "KiteConnect.Net/" + Assembly.GetEntryAssembly().GetName().Version;

            Req.Headers.Add("X-Kite-Version", "3");
            Req.Headers.Add("Authorization", "token " + apiKey + ":" + accessToken);

            Req.Timeout = timeout;
            if (proxy != null) Req.Proxy = proxy;

            if (enableLogging)
            {
                foreach (string header in Req.Headers.Keys)
                {
                    //Console.WriteLine("DEBUG: " + header + ": " + Req.Headers.GetValues(header)[0]);
                }
            }
        }

        /// Adds extra headers to request
        public void AddExtraHeadersForDifferentAccount(ref HttpWebRequest Req)
        {
            if (Assembly.GetEntryAssembly() != null)
                Req.UserAgent = "KiteConnect.Net/" + Assembly.GetEntryAssembly().GetName().Version;

            Req.Headers.Add("X-Kite-Version", "3");
            Req.Headers.Add("Authorization", "token " + apiKeyDownloadHistoricalData + ":" + m_accessTokenDownloadHistoricalData);

            Req.Timeout = timeout;
            if (proxy != null) Req.Proxy = proxy;

            if (enableLogging)
            {
                foreach (string header in Req.Headers.Keys)
                {
                    //Console.WriteLine("DEBUG: " + header + ": " + Req.Headers.GetValues(header)[0]);
                }
            }
        }

        /// Make an HTTP request.
        public dynamic Request(string Route, string Method, Dictionary<string, dynamic> Params = null)
        {
            string url = root + _routes[Route];

            if (Params == null)
                Params = new Dictionary<string, dynamic>();

            if (url.Contains("{"))
            {
                var urlparams = Params.ToDictionary(entry => entry.Key, entry => entry.Value);

                foreach (KeyValuePair<string, dynamic> item in urlparams)
                    if (url.Contains("{" + item.Key + "}"))
                    {
                        url = url.Replace("{" + item.Key + "}", (string)item.Value);
                        Params.Remove(item.Key);
                    }
            }

            //if (!Params.ContainsKey("api_key"))
            //    Params.Add("api_key", _apiKey);

            //if (!Params.ContainsKey("access_token") && !String.IsNullOrEmpty(_accessToken))
            //    Params.Add("access_token", _accessToken);

            HttpWebRequest request;
            string paramString = String.Join("&", Params.Select(x => Utils.BuildParam(x.Key, x.Value)));

            if (Method == "POST" || Method == "PUT")
            {
                request = (HttpWebRequest)WebRequest.Create(url);
                request.AllowAutoRedirect = true;
                request.Method = Method;
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = paramString.Length;
                //if (enableLogging) Console.WriteLine("DEBUG: " + Method + " " + url + "\n" + paramString);
                AddExtraHeaders(ref request);

                using (Stream webStream = request.GetRequestStream())
                using (StreamWriter requestWriter = new StreamWriter(webStream))
                    requestWriter.Write(paramString);
            }
            else
            {
               
                    request = (HttpWebRequest)WebRequest.Create(url + "?" + paramString);
                    request.AllowAutoRedirect = true;
                    request.Method = Method;
                    //if (enableLogging) Console.WriteLine("DEBUG: " + Method + " " + url + "?" + paramString);
                    AddExtraHeaders(ref request);
                
            }

            WebResponse webResponse;
            try
            {
                webResponse = request.GetResponse();
            }
            catch (WebException e)
            {
                if (e.Response == null)
                    throw e;

                webResponse = e.Response;
            }

            using (Stream webStream = webResponse.GetResponseStream())
            {
                using (StreamReader responseReader = new StreamReader(webStream))
                {
                    string response = responseReader.ReadToEnd();
                    //if (enableLogging) Console.WriteLine("DEBUG: " + (int)((HttpWebResponse)webResponse).StatusCode + " " + response + "\n");

                    HttpStatusCode status = ((HttpWebResponse)webResponse).StatusCode;

                    if (webResponse.ContentType == "application/json")
                    {
                        Dictionary<string, dynamic> responseDictionary = Utils.JsonDeserialize(response);

                        if (status != HttpStatusCode.OK)
                        {
                            string errorType = "GeneralException";
                            string message = "";

                            if (responseDictionary.ContainsKey("error_type"))
                                errorType = responseDictionary["error_type"];

                            if (responseDictionary.ContainsKey("message"))
                                message = responseDictionary["message"];

                            switch (errorType)
                            {
                                case "GeneralException": throw new GeneralException(message, status);
                                case "TokenException":
                                    {
                                        sessionHook?.Invoke();
                                        throw new TokenException(message, status);
                                    }
                                case "PermissionException": throw new PermissionException(message, status);
                                case "OrderException": throw new OrderException(message, status);
                                case "InputException": throw new InputException(message, status);
                                case "DataException": throw new DataException(message, status);
                                case "NetworkException": throw new NetworkException(message, status);
                                default: throw new GeneralException(message, status);
                            }
                        }

                        return responseDictionary;
                    }
                    else if (webResponse.ContentType == "text/csv")
                        return Utils.ParseCSV(response);
                    else
                        throw new DataException("Unexpected content type " + webResponse.ContentType + " " + response);
                }
            }
        }

        public dynamic RequestForDifferentAccount(string Route, string Method, Dictionary<string, dynamic> Params = null)
        {
            string url = root + _routes[Route];

            if (Params == null)
                Params = new Dictionary<string, dynamic>();

            if (url.Contains("{"))
            {
                var urlparams = Params.ToDictionary(entry => entry.Key, entry => entry.Value);

                foreach (KeyValuePair<string, dynamic> item in urlparams)
                    if (url.Contains("{" + item.Key + "}"))
                    {
                        url = url.Replace("{" + item.Key + "}", (string)item.Value);
                        Params.Remove(item.Key);
                    }
            }

            //if (!Params.ContainsKey("api_key"))
            //    Params.Add("api_key", _apiKey);

            //if (!Params.ContainsKey("access_token") && !String.IsNullOrEmpty(_accessToken))
            //    Params.Add("access_token", _accessToken);

            HttpWebRequest request;
            string paramString = String.Join("&", Params.Select(x => Utils.BuildParam(x.Key, x.Value)));

            if (Method == "POST" || Method == "PUT")
            {
                request = (HttpWebRequest)WebRequest.Create(url);
                request.AllowAutoRedirect = true;
                request.Method = Method;
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = paramString.Length;
                //if (enableLogging) Console.WriteLine("DEBUG: " + Method + " " + url + "\n" + paramString);
                AddExtraHeadersForDifferentAccount(ref request);

                using (Stream webStream = request.GetRequestStream())
                using (StreamWriter requestWriter = new StreamWriter(webStream))
                    requestWriter.Write(paramString);
            }
            else
            {
                request = (HttpWebRequest)WebRequest.Create(url + "?" + paramString);
                request.AllowAutoRedirect = true;
                request.Method = Method;
                //if (enableLogging) Console.WriteLine("DEBUG: " + Method + " " + url + "?" + paramString);
                AddExtraHeadersForDifferentAccount(ref request);
            }

            WebResponse webResponse;
            try
            {
                webResponse = request.GetResponse();
            }
            catch (WebException e)
            {
                if (e.Response == null)
                    throw e;

                webResponse = e.Response;
            }

            using (Stream webStream = webResponse.GetResponseStream())
            {
                using (StreamReader responseReader = new StreamReader(webStream))
                {
                    string response = responseReader.ReadToEnd();
                    //if (enableLogging) Console.WriteLine("DEBUG: " + (int)((HttpWebResponse)webResponse).StatusCode + " " + response + "\n");

                    HttpStatusCode status = ((HttpWebResponse)webResponse).StatusCode;

                    if (webResponse.ContentType == "application/json")
                    {
                        Dictionary<string, dynamic> responseDictionary = Utils.JsonDeserialize(response);

                        if (status != HttpStatusCode.OK)
                        {
                            string errorType = "GeneralException";
                            string message = "";

                            if (responseDictionary.ContainsKey("error_type"))
                                errorType = responseDictionary["error_type"];

                            if (responseDictionary.ContainsKey("message"))
                                message = responseDictionary["message"];

                            switch (errorType)
                            {
                                case "GeneralException": throw new GeneralException(message, status);
                                case "TokenException":
                                    {
                                        sessionHook?.Invoke();
                                        throw new TokenException(message, status);
                                    }
                                case "PermissionException": throw new PermissionException(message, status);
                                case "OrderException": throw new OrderException(message, status);
                                case "InputException": throw new InputException(message, status);
                                case "DataException": throw new DataException(message, status);
                                case "NetworkException": throw new NetworkException(message, status);
                                default: throw new GeneralException(message, status);
                            }
                        }

                        return responseDictionary;
                    }
                    else if (webResponse.ContentType == "text/csv")
                        return Utils.ParseCSV(response);
                    else
                        throw new DataException("Unexpected content type " + webResponse.ContentType + " " + response);
                }
            }
        }

        // Get the login url to which a user should be redirected to initiate the login flow.
        public string GetLoginURL(string login, string apiKey)
        {
            return String.Format("{0}?api_key={1}", login, apiKey);
        }

        /// Do the token exchange with the `RequestToken` obtained after the login flow,
        /// and retrieve the `AccessToken` required for all subsequent requests.The
        /// response contains not just the `AccessToken`, but metadata for
        /// the user who has authenticated.
        public User GenerateSession(string RequestToken, string AppSecret)
        {
            string checksum = Utils.SHA256(apiKey + RequestToken + AppSecret);

            var param = new Dictionary<string, dynamic>
            {
                {"api_key", apiKey},
                {"request_token", RequestToken},
                {"checksum", checksum}
            };

            var userData = Post("api.token", param);

            return new User(userData);
        }

        public User GenerateSessionForDifferentAccount(string RequestToken, string AppSecret)
        {
            string checksum = Utils.SHA256(apiKeyDownloadHistoricalData + RequestToken + AppSecret);

            var param = new Dictionary<string, dynamic>
            {
                {"api_key", apiKeyDownloadHistoricalData},
                {"request_token", RequestToken},
                {"checksum", checksum}
            };

            var userData = PostForDifferentAccount("api.token", param);

            return new User(userData);
        }

        public void SetAccessToken(string AccessToken)
        {
            accessToken = AccessToken;
            //Console.WriteLine("Access Token : " + AccessToken);
        }

        /// Helper function to add parameter to the request only if it is not null or empty
        private void AddIfNotNull(Dictionary<string, dynamic> Params, string Key, string Value)
        {
            if (!String.IsNullOrEmpty(Value))
                Params.Add(Key, Value);
        }

        /// Alias for sending a DELETE request.
        private dynamic Delete(string Route, Dictionary<string, dynamic> Params = null)
        {
            return Request(Route, "DELETE", Params);
        }

        /// Alias for sending a GET request.
        public dynamic Get(string Route, Dictionary<string, dynamic> Params = null)
        {
            return Request(Route, "GET", Params);
        }

        public dynamic GetForDifferentAccount(string Route, Dictionary<string, dynamic> Params = null)
        {
            return RequestForDifferentAccount(Route, "GET", Params);
        }

        /// Alias for sending a POST request.
        public dynamic Post(string Route, Dictionary<string, dynamic> Params = null)
        {
            return Request(Route, "POST", Params);
        }

        public dynamic PostForDifferentAccount(string Route, Dictionary<string, dynamic> Params = null)
        {
            return RequestForDifferentAccount(Route, "POST", Params);
        }

        /// Retrieve the list of market instruments available to trade.
        public List<Instrument> GetInstruments(string Exchange = null)
        {
            var param = new Dictionary<string, dynamic>();

            List<Dictionary<string, dynamic>> instrumentsData;

            if (String.IsNullOrEmpty(Exchange))
                instrumentsData = Get("market.instruments.all", param);
            else
            {
                param.Add("exchange", Exchange);
                instrumentsData = Get("market.instruments", param);
            }

            List<Instrument> instruments = new List<Instrument>();

            foreach (Dictionary<string, dynamic> item in instrumentsData)
            {
                instruments.Add(new Instrument(item));

            }
            return instruments;
        }

        /// <summary>
        /// Retrieve the list of positions.
        /// </summary>
        /// <returns>Day and net positions.</returns>
        public PositionResponse GetPositions()
        {
            var positionsdata = Get("portfolio.positions");
            return new PositionResponse(positionsdata["data"]);
        }

        /// Gets the collection of orders from the orderbook.
        public List<Order> GetOrders()
        {
            var ordersData = Get("orders");

            List<Order> orders = new List<Order>();

            foreach (Dictionary<string, dynamic> item in ordersData["data"])
                orders.Add(new Order(item));

            return orders;
        }

        /// Gets information about given OrderId.

        public List<Order> GetOrderHistory(string OrderId)
        {
            var param = new Dictionary<string, dynamic>();
            param.Add("order_id", OrderId);

            var orderData = Get("orders.history", param);

            List<Order> orderhistory = new List<Order>();

            foreach (Dictionary<string, dynamic> item in orderData["data"])
                orderhistory.Add(new Order(item));

            return orderhistory;
        }

        //IRDS::Bhagyashri::27-Apr-2018::Implemented the GetOrderHistoryBySymbols() to get symbol wise latestOrder
        public List<Order> GetOrderHistoryBySymbols(string tradingsymbol)
        {
            var param = new Dictionary<string, dynamic>();
            param.Add("tradingsymbol", tradingsymbol);

            var orderData = Get("orders", param);

            List<Order> orderhistorybysymbol = new List<Order>();
            List<Order> previousOrderBySymbol = new List<Order>();

            foreach (Dictionary<string, dynamic> item in orderData["data"])
                orderhistorybysymbol.Add(new Order(item));

            previousOrderBySymbol = orderhistorybysymbol.Where(o => o.Tradingsymbol == tradingsymbol).ToList();

            return previousOrderBySymbol;
        }

        public List<Order> GetOrderHistory()
        {
            var orderData = Get("orders");

            List<Order> orderhistorybysymbol = new List<Order>();
            List<Order> previousOrderBySymbol = new List<Order>();

            foreach (Dictionary<string, dynamic> item in orderData["data"])
                orderhistorybysymbol.Add(new Order(item));

            //previousOrderBySymbol = orderhistorybysymbol.Where(o => o.Tradingsymbol == tradingsymbol).ToList();

            return orderhistorybysymbol;
        }

        //IRDS::Bhagyashri::7-May-2018::Implemented the kite method for Cancel Order
        /// <summary>
        /// Cancel an order
        /// </summary>
        /// <param name="OrderId">Id of the order to be cancelled</param>
        /// <param name="Variety">You can place orders of varieties; regular orders, after market orders, cover orders etc. </param>
        /// <param name="ParentOrderId">Id of the parent order (obtained from the /orders call) as BO is a multi-legged order</param>
        /// <returns>Json response in the form of nested string dictionary.</returns>
        public Dictionary<string, dynamic> CancelOrder(string OrderId, string Variety = Constants.VARIETY_REGULAR, string ParentOrderId = null)
        {
            var param = new Dictionary<string, dynamic>();

            Utils.AddIfNotNull(param, "order_id", OrderId);
            Utils.AddIfNotNull(param, "parent_order_id", ParentOrderId);
            Utils.AddIfNotNull(param, "variety", Variety);

            return Delete("orders.cancel", param);
        }

        /// Place an order
        public Dictionary<string, dynamic> PlaceOrder(
             string Exchange,
             string TradingSymbol,
             string TransactionType,
             int Quantity,
             decimal? Price = null,
             string Product = null,
             string OrderType = null,
             string Validity = null,
             int? DisclosedQuantity = null,
             decimal? TriggerPrice = null,
             decimal? SquareOffValue = null,
             decimal? StoplossValue = null,
             decimal? TrailingStoploss = null,
             string Variety = Constants.VARIETY_REGULAR,
             string Tag = "")
        {           

            var param = new Dictionary<string, dynamic>();          
            Utils.AddIfNotNull(param, "exchange", Exchange);
            Utils.AddIfNotNull(param, "tradingsymbol", TradingSymbol);
            Utils.AddIfNotNull(param, "transaction_type", TransactionType);
            Utils.AddIfNotNull(param, "quantity", Quantity.ToString());
            Utils.AddIfNotNull(param, "price", Price.ToString());
            Utils.AddIfNotNull(param, "product", Product);
            Utils.AddIfNotNull(param, "order_type", OrderType);
            Utils.AddIfNotNull(param, "validity", Validity);
            Utils.AddIfNotNull(param, "disclosed_quantity", DisclosedQuantity.ToString());
            Utils.AddIfNotNull(param, "trigger_price", TriggerPrice.ToString());
            Utils.AddIfNotNull(param, "squareoff", SquareOffValue.ToString());
            Utils.AddIfNotNull(param, "stoploss", StoplossValue.ToString());
            Utils.AddIfNotNull(param, "trailing_stoploss", TrailingStoploss.ToString());
            Utils.AddIfNotNull(param, "variety", Variety);
            Utils.AddIfNotNull(param, "tag", Tag);

            return Post("orders.place", param);
        }

        /// <summary>
        /// Modify an open position's product type.
        /// </summary>
        /// <param name="Exchange">Name of the exchange</param>
        /// <param name="TradingSymbol">Tradingsymbol of the instrument</param>
        /// <param name="TransactionType">BUY or SELL</param>
        /// <param name="PositionType">overnight or day</param>
        /// <param name="Quantity">Quantity to convert</param>
        /// <param name="OldProduct">Existing margin product of the position</param>
        /// <param name="NewProduct">Margin product to convert to</param>
        /// <returns>Json response in the form of nested string dictionary.</returns>
        public Dictionary<string, dynamic> ConvertPosition(
            string Exchange,
            string TradingSymbol,
            string TransactionType,
            string PositionType,
            int? Quantity,
            string OldProduct,
            string NewProduct)
        {
            var param = new Dictionary<string, dynamic>();

            Utils.AddIfNotNull(param, "exchange", Exchange);
            Utils.AddIfNotNull(param, "tradingsymbol", TradingSymbol);
            Utils.AddIfNotNull(param, "transaction_type", TransactionType);
            Utils.AddIfNotNull(param, "position_type", PositionType);
            Utils.AddIfNotNull(param, "quantity", Quantity.ToString());
            Utils.AddIfNotNull(param, "old_product", OldProduct);
            Utils.AddIfNotNull(param, "new_product", NewProduct);

            return Put("portfolio.positions.modify", param);
        }


        //IRDS::16-July-2018::Bhagyashri::Added kiteConnect API method
        /// <summary>
        /// Modify an open order.
        /// </summary>
        /// <param name="OrderId">Id of the order to be modified</param>
        /// <param name="ParentOrderId">Id of the parent order (obtained from the /orders call) as BO is a multi-legged order</param>
        /// <param name="Exchange">Name of the exchange</param>
        /// <param name="TradingSymbol">Tradingsymbol of the instrument</param>
        /// <param name="TransactionType">BUY or SELL</param>
        /// <param name="Quantity">Quantity to transact</param>
        /// <param name="Price">For LIMIT orders</param>
        /// <param name="Product">Margin product applied to the order (margin is blocked based on this)</param>
        /// <param name="OrderType">Order type (MARKET, LIMIT etc.)</param>
        /// <param name="Validity">Order validity</param>
        /// <param name="DisclosedQuantity">Quantity to disclose publicly (for equity trades)</param>
        /// <param name="TriggerPrice">For SL, SL-M etc.</param>
        /// <param name="Variety">You can place orders of varieties; regular orders, after market orders, cover orders etc. </param>
        /// <returns>Json response in the form of nested string dictionary.</returns>
        public Dictionary<string, dynamic> ModifyOrder(
            string OrderId,
            string ParentOrderId = null,
            string Exchange = null,
            string TradingSymbol = null,
            string TransactionType = null,
            string Quantity = null,
            decimal? Price = null,
            string Product = null,
            string OrderType = null,
            string Validity = Constants.VALIDITY_DAY,
            int? DisclosedQuantity = null,
            decimal? TriggerPrice = null,
            string Variety = Constants.VARIETY_REGULAR)
        {
            var param = new Dictionary<string, dynamic>();

            string VarietyString = Variety;
            string ProductString = Product;

            if ((ProductString == "bo" || ProductString == "co") && VarietyString != ProductString)
                throw new Exception(String.Format("Invalid variety. It should be: {0}", ProductString));

            Utils.AddIfNotNull(param, "order_id", OrderId);
            Utils.AddIfNotNull(param, "parent_order_id", ParentOrderId);
            Utils.AddIfNotNull(param, "trigger_price", TriggerPrice.ToString());
            Utils.AddIfNotNull(param, "variety", Variety);

            if (VarietyString == "bo" && ProductString == "bo")
            {
                Utils.AddIfNotNull(param, "quantity", Quantity);
                Utils.AddIfNotNull(param, "price", Price.ToString());
                Utils.AddIfNotNull(param, "disclosed_quantity", DisclosedQuantity.ToString());
            }
            else if (VarietyString != "co" && ProductString != "co")
            {
                Utils.AddIfNotNull(param, "exchange", Exchange);
                Utils.AddIfNotNull(param, "tradingsymbol", TradingSymbol);
                Utils.AddIfNotNull(param, "transaction_type", TransactionType);
                Utils.AddIfNotNull(param, "quantity", Quantity);
                Utils.AddIfNotNull(param, "price", Price.ToString());
                Utils.AddIfNotNull(param, "product", Product);
                Utils.AddIfNotNull(param, "order_type", OrderType);
                Utils.AddIfNotNull(param, "validity", Validity);
                Utils.AddIfNotNull(param, "disclosed_quantity", DisclosedQuantity.ToString());
            }

            return Put("orders.modify", param);
        }

        /// <summary>
        /// Retrieve LTP of upto 200 instruments
        /// </summary>
        /// <param name="InstrumentId">Indentification of instrument in the form of EXCHANGE:TRADINGSYMBOL (eg: NSE:INFY) or InstrumentToken (eg: 408065)</param>
        /// <returns>Dictionary with InstrumentId as key and LTP as value.</returns>
        public Dictionary<string, LTP> GetLTP(string[] InstrumentId)
        {
            var param = new Dictionary<string, dynamic>();
            param.Add("i", InstrumentId);
            Dictionary<string, dynamic> ltpData = Get("market.ltp", param)["data"];

            Dictionary<string, LTP> ltps = new Dictionary<string, LTP>();
            foreach (string item in ltpData.Keys)
                ltps.Add(item, new LTP(ltpData[item]));

            return ltps;
        }

        public decimal get_LTP_Price(string symbol)
        {
            decimal LTP_Price = 0.0m;
            Dictionary<string, LTP> get_ltps = new Dictionary<string, LTP>();

            get_ltps = GetLTP(InstrumentId: new string[] { symbol });
            Console.WriteLine(Utils.JsonSerialize(get_ltps));
            if (get_ltps.ContainsKey(symbol))
            {
                foreach (var item in get_ltps.Values)
                {
                    LTP_Price = item.LastPrice;
                }
                return LTP_Price;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Alias for sending a PUT request.
        /// </summary>
        /// <param name="Route">URL route of API</param>
        /// <param name="Params">Additional paramerters</param>
        /// <returns>Varies according to API endpoint</returns>
        private dynamic Put(string Route, Dictionary<string, dynamic> Params = null)
        {
            return Request(Route, "PUT", Params);
        }

        // set a callback hook for session
        public void SetSessionExpiryHook(Action Method)
        {
            Console.WriteLine("Inside SetSessionExpiryHook");
            sessionHook = Method;
        }

        public void initializeLoggerObject(Logger logger, OrderStore orderStore,TickData tickData)
        {
            this.logger = logger;
            this.m_GlobalOrderStore = orderStore;
            this.m_TickData = tickData;
        }

        // Ticker Initiated
        public Thread m_addInTableThread = null;
        public Thread m_createNewThread = null;
        public bool m_bTickMCXDataDownloader = true;
        public void initTicker()
        {
            if (!m_bTickMCXDataDownloader)
            {
                try
                {
                    //IRDS::03-Jul-2020::Jyoti::added for queue changes
                    //IRDS::04-August-2020::Sandip::lock to create thread.
                    lock (this)
                    {
                        //mysql
                        if (m_useMysql)
                        {
                            if (m_Mysql == null)
                            {
                                m_Mysql = new MySQLConnectZerodha();
                                //IRDS::03-Jul-2020::Jyoti::added for database settings from ini file
                                if (m_Mysql.Connect(logger, dbServer, dbUserid, dbPassword, databaseName))
                                {
                                    m_isConnected = true;
                                }
                            }

                        }

                        //sqlite
                        //sanika::14-sep-2020::wrote seperate class for sqlite db
                        if (m_useSqlite)
                        {
                            if (sqliteConnectZerodha == null)
                            {
                                sqliteConnectZerodha = new SqliteConnectZerodha();
                            }
                            if(sqliteConnectZerodha.Connect("zerodhadata.db"))
                            {
                                m_isConnectedToSqlite = true;
                            }
                        }
                    }

                    string symbolName = "";
                    foreach (var symbol in RealTimeSymbols)
                    {
                        symbolName = symbol;

                        if (symbolName.Split('.')[1] == Constants.EXCHANGE_NSE && (!listOfNSESymbols.Contains(symbolName.Split('.')[0])))
                        {
                            listOfNSESymbols.Add(symbolName.Split('.')[0]);
                        }
                        else if (symbolName.Split('.')[1] == Constants.EXCHANGE_NFO && (!listOfNFOSymbols.Contains(symbolName.Split('.')[0])))
                        {
                            listOfNFOSymbols.Add(symbolName.Split('.')[0]);
                        }
                        else if (symbolName.Split('.')[1] == Constants.EXCHANGE_MCX && (!listOfMCXSymbols.Contains(symbolName.Split('.')[0])))
                        {
                            listOfMCXSymbols.Add(symbolName.Split('.')[0]);
                        }
                        //IRDS::Jyoti::9-Sept-20::Added for options symbols
                        else if (symbolName.Split('.')[1] == Constants.EXCHANGE_NFO_OPT && (!listOfOPTSymbols.Contains(symbolName.Split('.')[0])))
                        {
                            listOfOPTSymbols.Add(symbolName.Split('.')[0]);
                        }
                        //sanika::2-dec-2020::Added for CDS symbols
                        else if (symbolName.Split('.')[1] == Constants.EXCHANGE_CDS && (!listOfCDSSymbols.Contains(symbolName.Split('.')[0])))
                        {
                            listOfCDSSymbols.Add(symbolName.Split('.')[0]);
                        }
                        else if(symbolName.Split('.').Length > 2)//sanika::3-dec-2020::Added for cds-oprions symbols
                        {
                            if(symbolName.Split('.')[2] == Constants.EXCHANGE_CDS &&  (!listOfCDSSymbols.Contains(symbolName.Split('.')[0]+"."+symbolName.Split('.')[1])))
                            {
                                listOfCDSSymbols.Add(symbolName.Split('.')[0] + "." + symbolName.Split('.')[1]);
                            }
                        }
                    }

                    if (TradingSymbol.Count > 0)
                    {
                        foreach (var symbol in TradingSymbol)
                        {
                            symbolName = symbol;

                            if (symbolName.Split('.')[1] == Constants.EXCHANGE_NSE && (!listOfNSESymbols.Contains(symbolName.Split('.')[0])))
                            {
                                listOfNSESymbols.Add(symbolName.Split('.')[0]);
                            }
                            else if (symbolName.Split('.')[1] == Constants.EXCHANGE_NFO && (!listOfNFOSymbols.Contains(symbolName.Split('.')[0])))
                            {
                                listOfNFOSymbols.Add(symbolName.Split('.')[0]);
                            }
                            else if (symbolName.Split('.')[1] == Constants.EXCHANGE_MCX && (!listOfMCXSymbols.Contains(symbolName.Split('.')[0])))
                            {
                                listOfMCXSymbols.Add(symbolName.Split('.')[0]);
                            }
                            //IRDS::Jyoti::9-Sept-20::Added for options symbols
                            else if (symbolName.Split('.')[1] == Constants.EXCHANGE_NFO_OPT && (!listOfOPTSymbols.Contains(symbolName.Split('.')[0])))
                            {
                                listOfOPTSymbols.Add(symbolName.Split('.')[0]);
                            }
                            //sanika::2-dec-2020::Added for CDS symbols
                            else if (symbolName.Split('.')[1] == Constants.EXCHANGE_CDS && (!listOfCDSSymbols.Contains(symbolName.Split('.')[0])))
                            {
                                listOfCDSSymbols.Add(symbolName.Split('.')[0]);
                            }
                            else if (symbolName.Split('.').Length > 2)//sanika::3-dec-2020::Added for cds-oprions symbols
                            {
                                if (symbolName.Split('.')[2] == Constants.EXCHANGE_CDS && (!listOfCDSSymbols.Contains(symbolName.Split('.')[0] + "." + symbolName.Split('.')[1])))
                                {
                                    listOfCDSSymbols.Add(symbolName.Split('.')[0] + "." + symbolName.Split('.')[1]);
                                }
                            }
                        }
                    }

                    //get token from symbol name
                    string instrumentToken = "";
                    //IRDS::Jyoti::9-Sept-20::Added for options symbols
                    UInt32[] instrumentList = new UInt32[listOfNSESymbols.Count() + listOfNFOSymbols.Count() + listOfMCXSymbols.Count() + listOfOPTSymbols.Count() + listOfCDSSymbols.Count()];
                    if (listOfNSESymbols.Count > 0)
                    {
                        for (int i = 0; i < listOfNSESymbols.Count(); i++)
                        {
                            instrumentToken = getInstrumentToken(listOfNSESymbols[i]);
                            if (instrumentToken != "")
                            {
                                instrumentList[i] = Convert.ToUInt32(instrumentToken);
                                if (!SymbolList.ContainsKey(instrumentToken))
                                {
                                    //sanika::11-Nov-2020::added for logss
                                    WriteUniquelogs("TickLogs", listOfNSESymbols[i]+ " fetch instrument token as "+ instrumentToken, MessageType.Informational);
                                    SymbolList.Add(instrumentToken, listOfNSESymbols[i]);
                                    SymbolListWithFutureName.Add(instrumentToken, listOfNSESymbols[i]);
                                    m_NSESymbolsInstruements.Add(listOfNSESymbols[i]);
                                }
                            }
                            else
                            {
                                //sanika::11-Nov-2020::added for logss
                                WriteUniquelogs("TickLogs", "###### "+listOfNSESymbols[i] +"  this ignore because not present in db", MessageType.Informational);                               
                            }
                        }
                    }
                    if (listOfNFOSymbols.Count > 0)
                    {
                        string concatString = FetchTableName();
                        for (int i = listOfNSESymbols.Count(), j = 0; i < listOfNFOSymbols.Count() + listOfNSESymbols.Count() && j < listOfNFOSymbols.Count(); i++, j++)
                        {
                            string name = listOfNFOSymbols[j];
                            //IRDS::31-Aug-2020::Jyoti::Added gettoken for I, II, III changes
                            //instrumentToken = getInstrumentToken(listOfNFOSymbols[j] + concatString, concatString);
                            instrumentToken = GetToken(name + "." + Constants.EXCHANGE_NFO);
                         
                            if (instrumentToken != "")
                            {
                                instrumentList[i] = Convert.ToUInt32(instrumentToken);
                                if (!SymbolList.ContainsKey(instrumentToken))
                                {
                                    SymbolList.Add(instrumentToken, name);
                                    //IRDS::05-Sep-2020::Jyoti::Added for _FUT for nfo symbols
                                    //SymbolListWithFutureName.Add(instrumentToken, listOfNFOSymbols[j] + concatString);
                                    //m_NFOSymbolsInstruements.Add(listOfNFOSymbols[j] + concatString);
                                    if (listOfNFOSymbols[j].Contains("-I"))
                                    {
                                        SymbolListWithFutureName.Add(instrumentToken, listOfNFOSymbols[j]);
                                        m_NFOSymbolsInstruements.Add(listOfNFOSymbols[j]);
                                        //sanika::11-Nov-2020::added for logss
                                        WriteUniquelogs("TickLogs", listOfNFOSymbols[j] + " fetch instrument token as " + instrumentToken, MessageType.Informational);
                                    }
                                    else if(listOfNFOSymbols[j].Contains(concatString))
                                    {
                                        SymbolListWithFutureName.Add(instrumentToken, listOfNFOSymbols[j]);
                                        if(!listOfNFOSymbols[j].EndsWith("FUT"))//sanika::3-dec-2020::Added condition if symbol already contains future name
                                            m_NFOSymbolsInstruements.Add(listOfNFOSymbols[j] + concatString);
                                        else
                                            m_NFOSymbolsInstruements.Add(listOfNFOSymbols[j]);
                                        //sanika::11-Nov-2020::added for logss
                                        WriteUniquelogs("TickLogs", listOfNFOSymbols[j] + " fetch instrument token as " + instrumentToken, MessageType.Informational);
                                    }
                                    else
                                    {
                                        if (!listOfNFOSymbols[j].EndsWith("FUT"))//sanika::3-dec-2020::Added condition if symbol already contains future name
                                        {
                                            SymbolListWithFutureName.Add(instrumentToken, listOfNFOSymbols[j] + concatString);
                                            m_NFOSymbolsInstruements.Add(listOfNFOSymbols[j] + concatString);
                                        }
                                        else
                                        {
                                            SymbolListWithFutureName.Add(instrumentToken, listOfNFOSymbols[j]);
                                            m_NFOSymbolsInstruements.Add(listOfNFOSymbols[j]);
                                        }
                                        //sanika::11-Nov-2020::added for logss
                                        WriteUniquelogs("TickLogs", listOfNFOSymbols[j] + concatString +" fetch instrument token as " + instrumentToken, MessageType.Informational);
                                    }
                                }

                                
                            }
                            else
                            {
                                //sanika::11-Nov-2020::added for logss
                                WriteUniquelogs("TickLogs", "###### " + listOfNFOSymbols[j] + "  this ignore because not present in db", MessageType.Informational);
                                //logger.LogMessage("initTicker " + listOfNFOSymbols[j] + " this ignore because not present in db", MessageType.Informational);
                            }
                        }
                    }

                    if (listOfMCXSymbols.Count > 0)
                    {
                        string concatString =  FetchTableName();
                        for (int i = (listOfNSESymbols.Count() + listOfNFOSymbols.Count()), j = 0; i < listOfNFOSymbols.Count() + listOfNSESymbols.Count() + listOfMCXSymbols.Count() && j < listOfMCXSymbols.Count(); i++, j++)
                        {
                            string name = listOfMCXSymbols[j];
                            instrumentToken = getInstrumentToken(listOfMCXSymbols[j] + concatString, concatString);
                            if (instrumentToken != "")
                            {                                
                                instrumentList[i] = Convert.ToUInt32(instrumentToken);
                                if (!SymbolList.ContainsKey(instrumentToken))
                                {
                                    SymbolList.Add(instrumentToken, name);
                                    SymbolListWithFutureName.Add(instrumentToken, listOfMCXSymbols[j] + concatString);
                                    m_MCXSymbolsInstruements.Add(listOfMCXSymbols[j] + concatString);
                                    //sanika::11-Nov-2020::added for logss
                                    WriteUniquelogs("TickLogs", listOfMCXSymbols[j] + concatString + "fetch instrument token as " + instrumentToken, MessageType.Informational);
                                }
                            }
                            else
                            {
                                //sanika::11-Nov-2020::added for logss
                                WriteUniquelogs("TickLogs", "###### " + listOfMCXSymbols[j] + "  this ignore because not present in db", MessageType.Informational);
                            }
                        }
                    }

                    //IRDS::Jyoti::9-Sept-20::Added for options symbols
                    if (listOfOPTSymbols.Count > 0)
                    {
                        for (int i = (listOfNSESymbols.Count() + listOfNFOSymbols.Count() + listOfMCXSymbols.Count()), j = 0; i < listOfNFOSymbols.Count() + listOfNSESymbols.Count() + listOfMCXSymbols.Count() + listOfOPTSymbols.Count() && j < listOfOPTSymbols.Count(); i++, j++)
                        {
                            instrumentToken = getInstrumentToken(listOfOPTSymbols[j], "options");
                            if (instrumentToken != "")
                            {
                                instrumentList[i] = Convert.ToUInt32(instrumentToken);
                                if (!SymbolList.ContainsKey(instrumentToken))
                                {
                                    SymbolList.Add(instrumentToken, listOfOPTSymbols[j]);
                                    SymbolListWithFutureName.Add(instrumentToken, listOfOPTSymbols[j]);
                                    m_NFOSymbolsInstruements.Add(listOfOPTSymbols[j]);
                                    //sanika::11-Nov-2020::added for logss
                                    WriteUniquelogs("TickLogs", listOfOPTSymbols[j]  + "fetch instrument token as " + instrumentToken, MessageType.Informational);
                                }
                            }
                            else
                            {
                                //sanika::11-Nov-2020::added for logss
                                WriteUniquelogs("TickLogs", "###### " + listOfOPTSymbols[j] + "  this ignore because not present in db", MessageType.Informational);
                               
                            }
                        }
                    }
                    //sanika::2-dec-2020::Added for cds symbols
                    if (listOfCDSSymbols.Count > 0)
                    {
                        for (int i = (listOfNSESymbols.Count() + listOfNFOSymbols.Count() + listOfMCXSymbols.Count() + listOfOPTSymbols.Count()), j = 0; i < listOfNFOSymbols.Count() + listOfNSESymbols.Count() + listOfMCXSymbols.Count() + listOfOPTSymbols.Count() + listOfCDSSymbols.Count() && j < listOfCDSSymbols.Count(); i++, j++)
                        {
                            instrumentToken = getInstrumentToken(listOfCDSSymbols[j], "Currency");
                            if (instrumentToken != "")
                            {
                                instrumentList[i] = Convert.ToUInt32(instrumentToken);
                                if (!SymbolList.ContainsKey(instrumentToken))
                                {
                                    SymbolList.Add(instrumentToken, listOfCDSSymbols[j]);
                                    SymbolListWithFutureName.Add(instrumentToken, listOfCDSSymbols[j]);
                                    m_CDSSymbolsInstruements.Add(listOfCDSSymbols[j]);
                                    //sanika::11-Nov-2020::added for logss
                                    WriteUniquelogs("TickLogs", listOfCDSSymbols[j] + "fetch instrument token as " + instrumentToken, MessageType.Informational);
                                }
                            }
                            else
                            {
                                //sanika::11-Nov-2020::added for logss
                                WriteUniquelogs("TickLogs", "###### " + listOfCDSSymbols[j] + "  this ignore because not present in db", MessageType.Informational);

                            }
                        }
                    }

                    try
                    {
                        //sanika::14-sep-2020::wrote seperate class for sqlite db
                        if (m_useSqlite)
                        {
                            if (m_isConnectedToSqlite)
                            {
                                foreach (var data in SymbolListWithFutureName)
                                {
                                    string tableName = data.Value;
                                    if (!sqliteConnectZerodha.IsTableExists(tableName))                                        
                                    {
                                        sqliteConnectZerodha.ExecuteNonQueryCommand("CREATE TABLE '" + tableName + "'(TickDateTime DateTime PRIMARY KEY, Open real NOT NULL, High real NOT NULL,Low real NOT NULL,Close real NOT NULL,Volume real NOT NULL,DoubleDate real NOT NULL)");
                                    }
                                }
                            }
                        }

                        if (m_useMysql)
                        {
                            //IRDS::03-Jul-2020::Jyoti::added for cleaning and creating tables
                            try
                            {
                                //to create db file and tables                
                                if (m_isConnected)
                                {
                                    foreach (var data in SymbolListWithFutureName)
                                    {
                                        string tableName = data.Value;
                                        //tableName = tableName.Replace("-", "");
                                        //tableName = tableName.Replace("&", "");
                                        //tableName = tableName.Replace(" ", "");
                                        //m_Mysql.CreateZerodhaTable(tableName, databaseName);
                                        if (!m_Mysql.IsTableExists(tableName, databaseName))
                                        {
                                            //Jyoti5s
                                            //string fields = "id INT NOT NULL AUTO_INCREMENT,Symbol VARCHAR(50) NOT NULL, LastTradeTime DATETIME NOT NULL,Bid DOUBLE NOT NULL,Ask DOUBLE NOT NULL,LTP DOUBLE NOT NULL,ChangeInValues DOUBLE NOT NULL,ChangeInPercentage DOUBLE NOT NULL,Open DOUBLE NOT NULL,High DOUBLE NOT NULL,Low DOUBLE NOT NULL,Close DOUBLE NOT NULL,Average DOUBLE NOT NULL,Volume BIGINT NOT NULL,OpenInterest BIGINT NOT NULL,Buyers BIGINT NOT NULL,Sellers BIGINT NOT NULL";
                                            //m_Mysql.CreateTable(tableName, fields, "id");
                                            m_Mysql.CreateTable(tableName, "Symbol varchar(50) NOT NULL, LastTradeTime datetime NOT NULL, Open double NOT NULL, High double NOT NULL,Low double NOT NULL,Close double NOT NULL,Volume BIGINT NOT NULL", "LastTradeTime");
                                        }
                                    }
                                }

                            }
                            catch (Exception e)
                            {
                                logger.LogMessage("initTicker : Exception Error Message while creating Mysql table " + e.Message, MessageType.Exception);
                            }
                        }

                        //if (m_createNewThread == null)
                        //{                            
                        //    m_createNewThread = new Thread(() => CreateNewThreadForEachSymbol());
                        //    m_createNewThread.Start();
                        //}

                        if (m_addInTableThread == null)//As per sandip sir suggestion start thread after creating tables -- sanika
                        {
                            //Trace.WriteLine("Started ProcessTickDataToDatabase ");
                            m_addInTableThread = new Thread(() => ProcessTickDataToDatabase());
                            m_addInTableThread.Start();
                        }
                    }
                    catch (Exception e)
                    {
                        logger.LogMessage("initTicker : Exception Error Message while sqlite creating table " + e.Message, MessageType.Exception);
                    }

                    //sanika::11-Nov-2020::added for logss
                    WriteUniquelogs("TickLogs", "Going to subscribe tokens "+ instrumentList.Count(), MessageType.Informational);

                    Console.WriteLine("Inside initTicker");
                    ticker = new Ticker(apiKey, accessToken);                    
                    ticker.OnTick += OnTick;
                    ticker.OnReconnect += OnReconnect;
                    ticker.OnNoReconnect += OnNoReconnect;
                    ticker.OnError += OnError;
                    ticker.OnClose += OnClose;
                    ticker.OnConnect += OnConnect;
                    ticker.OnOrderUpdate += OnOrderUpdate;

                    ticker.EnableReconnect(Interval: 5, Retries: 50);
                    ticker.Connect();                   
                    //dynamic list
                    ticker.Subscribe(Tokens: instrumentList);
                    ticker.SetMode(Tokens: instrumentList, Mode: Constants.MODE_FULL);                   
                }
                catch (Exception e)
                {
                    logger.LogMessage("Initicker : Exception Error Message = "+e.Message, MessageType.Exception);
                }
            }
            else
            {
                MCXinitTicker();   
            }
        }

        public void WriteDateOfHistoricalDataDownloadIntoFile()
        {
            string currentDate = DateTime.Now.ToString("M-d-yyyy HH:mm:ss");
            var path_File = Directory.GetCurrentDirectory();
            string FilePath = path_File + @"\Configuration\" + "historicalData.txt";
            string line = currentDate;
            using (StreamWriter streamWriter = new StreamWriter(FilePath, false))
            {
                streamWriter.WriteLine(line);
            }
        }

        public string GetLastDownloadDateOfHistoricalData()
        {
            string lastDate = "";
            var path_File = Directory.GetCurrentDirectory();
            string FilePath = path_File + @"\Configuration\" + "historicalData.txt";
            if (!File.Exists(FilePath))
            {
                logger.LogMessage("CheckDownloadInstrumentCSV : historicalData.txt file not exist", MessageType.Informational);
                return lastDate;
            }
            else
            {
                string lastLine = System.IO.File.ReadLines(FilePath).Last();
                lastDate = lastLine;
            }
            return lastDate;
        }
       
        public void CloseConnectionForCreateTable()
        {
            try
            {
                Sql_conForCreateandreadTable.Close();
            }
            catch (Exception e)
            {
                logger.LogMessage("CloseConnectionForCreateTable : Exception Error Message = " + e.Message, MessageType.Exception);
                return;
            }
        }

        public bool OpenConnectionForCreateTable()
        {
            try
            {
                //db created at absolute path
                var path_DB = Directory.GetCurrentDirectory();
                string directoryName = path_DB + "\\" + "Database";

                if (!Directory.Exists(directoryName))
                    Directory.CreateDirectory(directoryName);

                string DBFilePath = directoryName + "\\" + DBName;

                if (!File.Exists(DBFilePath))
                {
                    SQLiteConnection.CreateFile(DBFilePath);
                    Thread.Sleep(100);
                }

                string connectionString = String.Format("Data Source={0};Version=3;New=False;Compress=True;", DBFilePath);
                Sql_conForCreateandreadTable = new SQLiteConnection(connectionString);
                //open sqlite connection
                Sql_conForCreateandreadTable.Open();
            }
            catch (Exception e)
            {
                logger.LogMessage("OpenConnectionForCreateTable :  Exception Error Message = " + e.Message, MessageType.Exception);
                return false;
            }

            return true;
        }

        public bool OpenConnection()
        {
            try
            {
                //db created at absolute path
                var path_DB = Directory.GetCurrentDirectory();
                string directoryName = path_DB + "\\" + "Database";

                if (!Directory.Exists(directoryName))
                    Directory.CreateDirectory(directoryName);

                string DBFilePath = directoryName + "\\" + DBName;

                if (!File.Exists(DBFilePath))
                {
                    SQLiteConnection.CreateFile(DBFilePath);
                    Thread.Sleep(100);
                }

                string connectionString = String.Format("Data Source={0};Version=3;New=False;Compress=True;", DBFilePath);
                Sql_Datacon = new SQLiteConnection(connectionString);
                //open sqlite connection
                Sql_Datacon.Open();
            }
            catch (Exception e)
            {
                logger.LogMessage("OpenConnection :  Exception Error Message = " + e.Message, MessageType.Exception);
                return false;
            }

            return true;
        }

        public void CheckTradingSymbolPresentOrNot(List<string> list)
        {
            foreach (var value in list)
            {
                if ((!RealTimeSymbols.Contains(value)) && (!TradingSymbol.Contains(value)))
                {
                    TradingSymbol.Add(value);                    
                    //IRDS::Jyoti::12-Sept-20::Added exchange for symbol settings form
                    RealTimeSymbols.Add(value);
                }
            }
            if (TradingSymbol.Count > 0 && TradingSymbol.Count > m_previousTradingSymbolListCount)
            {
                initTicker();
                m_previousTradingSymbolListCount = TradingSymbol.Count;
            }
        }

        //public bool CheckAllThreadExistOrNot()
        //{
        //    bool isStopAllThread = false;
        //    try
        //    {
        //        if (m_addInTableThread != null)
        //        {
        //            if (!m_addInTableThread.IsAlive)
        //            {
        //                logger.LogMessage("CheckAllThreadExistOrNot : m_addInTableThread Thread closed", MessageType.Informational);
        //                isStopAllThread = true;
        //            }
        //            else
        //            {
        //                logger.LogMessage("CheckAllThreadExistOrNot : m_addInTableThread Not able to close", MessageType.Informational);
        //                return false;
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        logger.LogMessage("CheckAllThreadExistOrNot : Exception Error Message = " + e.Message, MessageType.Exception);
        //    }
        //    return isStopAllThread;
        //}

        //sanika::29-sep-2020::changed method to stop thread
        public void StopThread()
        {
            m_StopThread = true;            
        }


        //to create symbol name for future symbols
        public string FetchTableName()
        {
            string concatString = "";
            string year = DateTime.Now.ToString("yy");
            string currentMonth = DateTime.Now.ToString("MMM");
            List<string> tableName = new List<string>();
            string tablename = "";

            try
            {
                if (Sql_conTableName == null)
                {
                    var path_DB = Directory.GetCurrentDirectory();
                    string directoryName = path_DB + "\\" + "Database";

                    string DBFilePath = directoryName + "\\" + "kite.db";
                    string connectionString = String.Format("Data Source={0};Version=3;New=False;Compress=True;", DBFilePath);
                    Sql_conTableName = new SQLiteConnection(connectionString);
                    Sql_conTableName.Open();
                }                
                SQLiteCommand Sql_cmdTableName;
                SQLiteDataReader Sql_readTableName;

                string query = "SELECT name FROM sqlite_master WHERE type = 'table' AND name NOT LIKE 'sqlite_%'";
                Sql_cmdTableName = new SQLiteCommand(query, Sql_conTableName);
                Sql_readTableName = Sql_cmdTableName.ExecuteReader();
                if (Sql_readTableName != null && Sql_readTableName.HasRows)
                {
                    while (Sql_readTableName.Read())
                    {
                        tablename = (string)Sql_readTableName["name"];
                        tableName.Add(tablename);
                    }
                }
                for (int i = 0; i < tableName.Count(); i++)
                {
                    if (tableName[i].Any(char.IsDigit))
                    {
                        if (tableName[i].Substring(0, 2) == year)
                        {
                            string month = tableName[i].Substring(2, 3);
                            if (month == DateTime.Now.ToString("MMM").ToUpper())
                            {
                                DateTime lastThusday = GetLastThusdayDate();
                                if (DateTime.Now < lastThusday)
                                {
                                    concatString = tableName[i];
                                    break;
                                }
                                else
                                {
                                    //IRDS::27-dec-2019::Sanika::Added change year if current month is dec
                                    if (currentMonth.Equals("Dec"))
                                    {
                                        int yr = Convert.ToInt32(year) + 1;
                                        year = yr.ToString();
                                    }
                                    concatString = year + DateTime.Now.AddMonths(1).ToString("MMM").ToUpper() + "FUT";

                                    if (tableName.Contains(concatString))
                                    {
                                        break;
                                    }
                                    else
                                    {
                                        return null;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                logger.LogMessage("FetchTableName : Exception Error Message = " + e.Message, MessageType.Exception);                
            }
            return concatString;
        }

        //for fetch date of last thusday of month
        public DateTime GetLastThusdayDate()
        {
            DateTime date = DateTime.Now;
            int year = DateTime.Now.Year;
            int month = DateTime.Now.Month;
            try
            {
                var tmpDate = new DateTime(year, month, 1).AddMonths(1).AddDays(-1);
                while (tmpDate.DayOfWeek != DayOfWeek.Thursday)
                {
                    tmpDate = tmpDate.AddDays(-1);
                }
                date = tmpDate.AddDays(1);
            }
            catch (Exception e)
            {
                logger.LogMessage("GetLastThusdayDate : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return date;
        }


        //IRDS::Sanika::14-Aug-2019::Function to get instrument token from symbol name (from database)
        public string getInstrumentToken(string symbolName, string tableName = "instruments")
        {
            string token = "";
            try
            {
                if (Sql_conFetchSymbolName == null)
                {
                    var path_DB = Directory.GetCurrentDirectory();
                    string directoryName = path_DB + "\\" + "Database";

                    string DBFilePath = directoryName + "\\" + "kite.db";
                    string connectionString = String.Format("Data Source={0};Version=3;New=False;Compress=True;", DBFilePath);
                    Sql_conFetchSymbolName = new SQLiteConnection(connectionString);
                    Sql_conFetchSymbolName.Open();
                }               
                SQLiteCommand Sql_cmdToken;
                SQLiteDataReader Sql_readToken;

                string query = "select InstrumentToken from '" + tableName + "' where symbol = '" + symbolName + "'";
                Sql_cmdToken = new SQLiteCommand(query, Sql_conFetchSymbolName);
                Sql_readToken = Sql_cmdToken.ExecuteReader();
                if (Sql_readToken != null && Sql_readToken.HasRows)
                {
                    if (Sql_readToken.Read())
                    {
                        token = (Sql_readToken["InstrumentToken"]).ToString();
                    }
                }

            }
            catch (Exception e)
            {
                logger.LogMessage("getInstrumentToken : Exception Error Message = " + e.Message, MessageType.Exception);
                
            }
            return token;
        }

        public void OnTokenExpire()
        {
            bOnExpiry = true;
            Console.WriteLine("Fire Event OnTokenExpire : Need to login again");
        }

        public void OnConnect()
        {
            Console.WriteLine("Connected ticker");

        }

        public void OnClose()
        {
            Console.WriteLine("Closed ticker");
        }

        public void OnError(string Message)
        {
            Console.WriteLine("Error: " + Message);
        }

        public void OnNoReconnect()
        {
            Console.WriteLine("Not reconnecting");
        }

        private void OnReconnect()
        {
            Console.WriteLine("Reconnecting");
        }

        public string GetToken(string symbol)
        {
            string tradingSymbol = symbol.Split('.')[0];
            string exchange = symbol.Split('.')[1];
            string tableName = "";
            if (exchange == Constants.EXCHANGE_NFO_OPT)//sanika::18-Mar-2021::Added to get option tokens
            {
                tableName = "OPTIONS";
            }
            else if (exchange == Constants.EXCHANGE_NSE) //sanika::18-Mar-2021::Condition changed 
            {
                tableName = "instruments";
            }           
            //sanika::3-dec-2020::Added condition if symbol already contains future name of other than current month
            else if (tradingSymbol.EndsWith("FUT") && !tradingSymbol.Contains(FetchTableName()))
            {                
                tableName = tradingSymbol.Substring(tradingSymbol.Length - 8, 8);
            }
            else
            {
                tableName = FetchTableName();
                //IRDS::05-Sep-2020::Jyoti::Added changes for I, II, III changes
                if (tradingSymbol.Contains("-III"))
                {
                    tradingSymbol = tradingSymbol.Replace("-III", "");
                    string currMonth = tableName.Substring(2, 3);
                    int month = DateTime.ParseExact(currMonth, "MMM", CultureInfo.CurrentCulture).Month + 2;
                    DateTime d = new DateTime(DateTime.Now.Year, month, 1);
                    tableName = tableName.Replace(currMonth, d.ToString("MMM").ToUpper());
                }
                else if (tradingSymbol.Contains("-II"))
                {
                    tradingSymbol = tradingSymbol.Replace("-II", "");
                    string currMonth = tableName.Substring(2, 3);
                    int month = DateTime.ParseExact(currMonth, "MMM", CultureInfo.CurrentCulture).Month + 1;
                    DateTime d = new DateTime(DateTime.Now.Year, month, 1);
                    tableName =  tableName.Replace(currMonth, d.ToString("MMM").ToUpper());
                }
                else if (tradingSymbol.Contains("-I"))
                    tradingSymbol = tradingSymbol.Replace("-I", "");
                if(!tradingSymbol.Contains(tableName))
                    tradingSymbol += tableName;
            }

            return getInstrumentToken(tradingSymbol, tableName);
        }

        public bool orderPlaced = true;

        private static readonly object tickLock = new object();
        DateTime myTickLastDateTime;
        public void OnTick(Tick TickData)
        {  
            Tick tick = TickData;
            //IRDS::03-Jul-2020::Jyoti::added for queue changes
            //lock (m_queueTick)
            if (!m_bTickMCXDataDownloader)
            {
                try
                {
                    //if(m_QueueOfAllSymbols.ContainsKey(tick.InstrumentToken.ToString()))
                    //{
                    //    m_QueueOfAllSymbols[tick.InstrumentToken.ToString()].Enqueue(tick);
                    //}
                    //else
                    //{
                    //    ConcurrentQueue<Tick> queueTick = new ConcurrentQueue<Tick>();
                    //    queueTick.Enqueue(tick);
                    //    m_QueueOfAllSymbols.Add(tick.InstrumentToken.ToString(), queueTick);                      
                    //}
                    //IRDS::04-August-2020::Sandip::removed lock as we using thread safe queue.
                    m_queueTick.Enqueue(tick);
                    myTickLastDateTime = Convert.ToDateTime(tick.Timestamp);                                     
                    //sanika::22-sep-2020::remove dictionary and added structure
                    if (m_TickData != null)
                    {
                        string symbolName = SymbolListWithFutureName[tick.InstrumentToken.ToString()];
                        Trace.WriteLine("##TICKStatus : started to update structure");
                        m_TickData.AddOrUpdateTickData(tick, symbolName);
                        Trace.WriteLine("##TICKStatus : stopped to update structure");
                    }
                    //sanika::19-Nov-2020::Added condition to avoid expception
                    if(tick.Timestamp != null)
                       tickCurrentTime = (Convert.ToDateTime(tick.Timestamp.ToString())).ToString("HH:mm:ss");

                }
                catch (Exception e)
                {
                    logger.LogMessage("OnTick : Exception Error Message = " + e.Message, MessageType.Exception);                    
                }
            }
            else
            {
                try
                {
                    if (m_QueueOfAllSymbols.ContainsKey(tick.InstrumentToken.ToString()))
                    {
                        m_QueueOfAllSymbols[tick.InstrumentToken.ToString()].Enqueue(tick);
                    }
                    else
                    {
                        ConcurrentQueue<Tick> queueTick = new ConcurrentQueue<Tick>();
                        queueTick.Enqueue(tick);
                        m_QueueOfAllSymbols.Add(tick.InstrumentToken.ToString(), queueTick);
                    }
                }
                catch (Exception e)
                {
                    logger.LogMessage("OnTick : Exception Error Message =  " + e.Message, MessageType.Exception);                    
                }
                return;
            }
            // try
            // {
            //string FilePath = pathFile + "\\" + "TickData.txt";
            //using (StreamWriter streamWriter = File.AppendText(FilePath))
            //{
            //    streamWriter.WriteLine(Utils.JsonSerialize(TickData));
            //}

            /*if (tick.Timestamp != null)
            {
                string currentDate = DateTime.Now.ToString("M/d/yyyy");
                string tickDate = (Convert.ToDateTime(tick.Timestamp.ToString())).ToString("M/d/yyyy");
                if ((tickDate == currentDate))
                {
                    //lock (tickLock)
                    {
                        DateTime tickDateTime = (DateTime)tick.Timestamp;
                        tickCurrentTime = (Convert.ToDateTime(tick.Timestamp.ToString())).ToString("HH:mm:ss");
                        string symbolName = SymbolListWithFutureName[tick.InstrumentToken.ToString()];
                        lock (tickLock)
                        {
                            if (LastTradedPrice.ContainsKey(symbolName))
                            {
                                LastTradedPrice[symbolName] = tick.LastPrice.ToString();
                            }
                            else
                            {
                                LastTradedPrice.Add(symbolName, tick.LastPrice.ToString());
                            }
                        }

                      //  createBar(tick, tickDateTime);
                    }
                }
            }*/

            //}
            //catch (Exception e)
            //{
            //    Trace.WriteLine("from ontick : " + e.Message + " " + tick.Timestamp.ToString());
            //}
        }


        //0th index = StartTime
        //1th index = EndTime
        //2nd index = open
        //3rd index = high
        //4th index = low
        //5th index = close
        //6th index = volume
        //dictionary = initial volume

        int i = 0, j = 0;
        string startTime = "";


        public void createBar(Tick data, DateTime time)
        {
            string symbolName = SymbolListWithFutureName[data.InstrumentToken.ToString()];
            try
            {
                if ((time.ToOADate() >= Start.ToOADate()) && (time.ToOADate() < endTimeForTick.ToOADate()))
                {
                    createArray(data, Start, endTimeForTick, symbolName);                   
                }
                if (time.ToOADate() >= endTimeForTick.ToOADate() || myTickLastDateTime.ToOADate() >= endTimeForTick.ToOADate())
                {
                    FinalZerodhaData = new Dictionary<string, List<string>>(ZerodhaData);
                    ListOfSymbolsForStartValues.Clear();
                    Start = endTimeForTick;
                    endTimeForTick = Start.AddSeconds(60 * Convert.ToInt32(Interval));//.ToString("HH:mm:ss"); 
                    //ListStartOpenValues.Clear();
                    ZerodhaData.Clear();
                    Trace.WriteLine("##TICKStatus Store Data in MySQL******************* time " + time + " myTickLastDateTime " + myTickLastDateTime.ToString());
                  
                    insertIntoTableTick(Start);
                    Trace.WriteLine("##TICKStatus Store Data in MySQL Finished ******************* time " + time + " myTickLastDateTime " + myTickLastDateTime.ToString());
                    WriteTickDatainCSVFile();                   
                    InsertCandleinSQLLiteDatabase();
                    //Trace.WriteLine("##TICKStatus after create arrayL******************* " + time + " symbol " + symbolName);
                    createArray(data, Start, endTimeForTick, symbolName);
                }

                if (m_isConnected && m_useMysql)
                {
                    DateTime endDateTime = DateTime.Now;
                    TimeSpan difference = endDateTime - startDateTime;
                    if (difference.Milliseconds>500)//time.ToOADate() >= TickStartTime[symbolName].ToOADate())
                    {
                        //Trace.WriteLine("##TICKStatus : &&&&&&&&&&&&&&&& Starts 1 sec Data &&&&&&&&&&&&&&&&&&&&& " + m_queueTick.Count.ToString() + " symbolName " + symbolName + time.ToLongTimeString());                       
                        startDateTime = DateTime.Now;
                        FinalZerodhaTickData = new Dictionary<string, List<string>>(ZerodhaData);
                        // ZerodhaDataForMarketWatch = new Dictionary<string, List<string>>(ZerodhaData);                       
                        insertIntoTableTick(symbolName);
                        //TickStartTime[symbolName] = TickStartTime[symbolName].AddMilliseconds(10);
                        //Trace.WriteLine("##TICKStatus : &&&&&&&&&&&&&&&& ENDENDENDENDN 1 sec Data &&&&&&&&&&&&&&&&&&&&&");
                    }
                }
            }

            catch (Exception e)
            {
                logger.LogMessage("CreateBar : Exception Error Message = " + e.Message, MessageType.Exception);                
            }
			//IRDS::04-August-2020::Sandip::no need to write tickby tick data
            //IRDS::11-oct-2019::Sanika::Added for write tick data in csv file
            //try
            //{
            //    var path_DB = Directory.GetCurrentDirectory();
            //    string todayDate = DateTime.Now.ToString("M-d-yyyy");
            //    string today = DateTime.Now.ToString("M/d/yyyy");
            //    System.IO.Directory.CreateDirectory(path_DB + "\\" + "Data\\TickData\\" + todayDate);
            //    string dName = path_DB + "\\" + "Data\\TickData\\" + todayDate + "\\" + symbolName + ".csv";
            //    if (!File.Exists(dName))
            //    {
            //        string header = String.Join(Environment.NewLine, "Date,Time,Last Price,Volume,BuyQuantity,SellQuantity,Open,High,Low,Close,Timestamp,LastTreadedTime");
            //        header += Environment.NewLine;
            //        System.IO.File.AppendAllText(dName, header);
            //    }
            //    using (StreamWriter streamWriter = File.AppendText(dName))
            //    {
            //        String csv = String.Join(Environment.NewLine, today + "," + time + "," + data.LastPrice + "," + data.Volume + "," + data.BuyQuantity + "," + data.SellQuantity + "," + data.Open + "," + data.High + "," + data.Low + "," + data.Close + "," + data.Timestamp + "," + data.LastTradeTime);
            //        streamWriter.WriteLine(csv);
            //    }
            //}
            //catch (Exception e)
            //{
            //    logger.LogMessage("Exception from csv writing " + e.Message, MessageType.Exception);
            //    Trace.WriteLine("exception : " + e.Message);
            //}
        }

        public void insertIntoTableTick(DateTime tmpDate)
        {
            if (m_useMysql)
            {
                //logger.LogMessage("Inside insertIntoTableTick", MessageType.Informational);
                string todayDate = DateTime.Now.ToString("yyyy-MM-dd");
                Dictionary<string, List<string>> dictionary = new Dictionary<string, List<string>>(FinalZerodhaTickData);
                try
                {

                    m_Mysql.beginTran();
                    string start = tmpDate.ToString("HH:mm:ss");
                    Parallel.ForEach(dictionary, d =>
                    {
                        //foreach (var d in dictionary)
                    //{
                        string tableName = d.Key;
                        string time = null;
                        int lastIndex = d.Value[0].LastIndexOf(":");
                        if (lastIndex != -1)
                        {
                            time = d.Value[0].Substring(0, lastIndex);
                            time = start;// time + ":00";
                        }
                        //tableName = tableName.Replace("-", "");
                        //tableName = tableName.Replace("&", "");
                        //tableName = tableName.Replace(" ", "");
                        ///Trace.WriteLine("##TICKStatus Write in my sql " + time + " symbol " + symbol);
                        m_Mysql.InsertorUpdate(tableName, todayDate + " " + time, d.Value[2], d.Value[3], d.Value[4], d.Value[5], d.Value[6]);
                       
                    });
                    //m_Mysql.CommitTran();

                }
                catch (Exception e)
                {
                    logger.LogMessage("insertIntoTableTick : Exception Error Message = " + e.Message, MessageType.Exception);
                }
                finally
                {
                    m_Mysql.CommitTran();
                }
            }
        }

        public void insertIntoTableTick(string SymbolName)
        {
            if (m_useMysql)
            {
                //logger.LogMessage("Inside insertIntoTableTick", MessageType.Informational);
                string todayDate = DateTime.Now.ToString("yyyy-MM-dd");
                Dictionary<string, List<string>> dictionary = new Dictionary<string, List<string>>(FinalZerodhaTickData);
                try
                {
                    m_Mysql.beginTran();
                    foreach (var d in dictionary)
                    {
                        string tableName = d.Key;
                        string time = null;
                        int lastIndex = d.Value[0].LastIndexOf(":");
                        if (lastIndex != -1)
                        {
                            time = d.Value[0].Substring(0, lastIndex);
                            time = time + ":00";
                        }
                        //tableName = tableName.Replace("-", "");
                        //tableName = tableName.Replace("&", "");
                        //tableName = tableName.Replace(" ", "");
                        //if (SymbolName == tableName)
                        {
                            //Trace.WriteLine("##TICKStatus Write in my sql " + time + " tableName " + tableName);
                            m_Mysql.InsertorUpdate(tableName, todayDate + " " + time, d.Value[2], d.Value[3], d.Value[4], d.Value[5], d.Value[6]);
                        }
                    }
                }
                catch (Exception e)
                {
                    logger.LogMessage("insertIntoTableTick : Exception Error Message =  " + e.Message, MessageType.Exception);
                }
                finally
                {
                    m_Mysql.CommitTran();
                }
            }
        }

        public bool TableExists(string Table_Name)
        {
            using (SQLiteCommand Sql_cmd = new SQLiteCommand(Sql_conForCreateandreadTable))
            {
                Sql_cmd.CommandText = "SELECT * FROM sqlite_master WHERE type='table' AND name=@name";
                Sql_cmd.Parameters.AddWithValue("@name", Table_Name);

                using (Sql_read = Sql_cmd.ExecuteReader())
                {
                    if (Sql_read != null && Sql_read.HasRows)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }

        public void InsertCandleinSQLLiteDatabase()
        {
            if (m_useSqlite)
            {
                if (m_isConnectedToSqlite)
                {
                    string todayDate = DateTime.Now.ToString("yyyy-MM-dd");
                    Dictionary<string, List<string>> dictionary = new Dictionary<string, List<string>>(FinalZerodhaData);
                    try
                    {
                        lock (databaselock)
                        {
                            //sanika::18-sep-2020::changed parallel loop with foreach and added list
                            List<string> dbQueries = new List<string>();
                            //Parallel.ForEach(dictionary, d =>
                            foreach(var d in dictionary)
                            {
                                string tableName = d.Key;
                                string time = null;
                                int lastIndex = d.Value[0].LastIndexOf(":");
                                if (lastIndex != -1)
                                {
                                    time = d.Value[0].Substring(0, lastIndex);
                                    time = time + ":00";
                                }
                                int trial = 0;
                                DateTime dateTime = DateTime.Parse(todayDate + " " + time);
                                try
                                {
                                    //sanika::14-sep-2020::wrote seperate class for sqlite db
                                    string insertSQLQuery = "INSERT OR IGNORE into '" + tableName + "' (TickDateTime, Open, High, Low, Close, Volume,DoubleDate) values ('" + dateTime + "','" + d.Value[2] + "','" + d.Value[3] + "','" + d.Value[4] + "','" + d.Value[5] + "','" + d.Value[6] + "','" + dateTime.ToOADate() + "')";
                                    //sqliteConnectZerodha.ExecuteNonQueryCommand(insertSQLQuery);
                                    //sanika::18-sep-2020::added queries into list
                                    if (!dbQueries.Contains(insertSQLQuery))
                                         dbQueries.Add(insertSQLQuery);
                                }
                                catch (Exception e)
                                {                                    
                                    logger.LogMessage(tableName + " InsertCandleinSQLLiteDatabase : Exception Error Message = " + e.Message, MessageType.Exception);
                                }
                            }
                            //sanika::18-sep-2020::call function which insert multiple values
                            sqliteConnectZerodha.InsertDataMultipleValues("", dbQueries);
                        }
                    }
                    catch (Exception e)
                    {
                        logger.LogMessage("InsertCandleinSQLLiteDatabase:Exception Error Message =  " + e.Message, MessageType.Exception);
                    }
                }
            }
        }

        public bool GetHighLow(string symbol, int barCount, out double dHigh, out double dLow)
        {
            bool isValueOut = false;
            dHigh = 0;
            dLow = 0;           
            if (m_useSqlite)
            {
                if (m_isConnectedToSqlite)
                {
                    try
                    {
                        string tableName = symbol;
                        int itry = 0;                        
                        lock (databaselock)
                        {
                            //sanika::14-sep-2020::wrote seperate class for sqlite db
                            if (sqliteConnectZerodha.GetHighLow(symbol, barCount, out dHigh, out dLow))
                            {
                                isValueOut = true;
                            }
                        }                        
                    }
                    catch (Exception e)
                    {
                        isValueOut = false;
                        logger.LogMessage("GetHighLow : Exception Error Message = " + e.Message, MessageType.Exception);
                    }
                }
            }
            return isValueOut;
        }

        public bool GetCloseValuesList(string symbol, int barCount, out List<double> closeValues, out List<string> dateTimeList)
        {
            List<double> ListOfCloseValues = new List<double>();
            List<string> dateTime = new List<string>();
            //if (m_useSqlite)
            {               
                if (Sql_Datacon == null)
                {
                    OpenConnection();
                }
                string tableName = symbol;
                try
                {
                    int itry = 0;
                    SQLiteDataReader Sql_readNow;
                    SQLiteCommand Sql_cmdSelectMaxAndMin;
                    string insertSQLQuery = "SELECT Close, DoubleDate FROM '" + tableName + "' WHERE DoubleDate IN(SELECT DoubleDate FROM '" + tableName + "' order by DoubleDate DESC LIMIT " + barCount + ")";
                    Sql_cmdSelectMaxAndMin = new SQLiteCommand(insertSQLQuery, Sql_Datacon);
                    Sql_readNow = Sql_cmdSelectMaxAndMin.ExecuteReader();
                    if (Sql_readNow != null && Sql_readNow.HasRows)
                    {
                        while (Sql_readNow.Read())
                        {
                            ListOfCloseValues.Add((double)Sql_readNow["Close"]);
                        }
                    }
                    if (ListOfCloseValues.Count() == 0)
                    {
                        logger.LogMessage("GetCloseValuesList : Not able to get close values", MessageType.Informational);
                    }
                }
                catch (Exception e)
                {
                    logger.LogMessage("GetCloseValuesList : Exception Error Message = " + e.Message, MessageType.Exception);
                }                
            }
            closeValues = ListOfCloseValues;
            dateTimeList = new List<string>();//   dateTime;
            return true;
        }

        public void WriteTickDatainCSVFile()
        {
            if (m_useCSV)
            {
                try
                {
                    lock (this)
                    {
                        var path_DB = Directory.GetCurrentDirectory();
                        string todayDate = DateTime.Now.ToString("M-d-yyyy");
                        System.IO.Directory.CreateDirectory(path_DB + "\\" + "Data\\ZerodhaData");
                        foreach (var d in FinalZerodhaData)
                        {
                            string dName = path_DB + "\\" + "Data\\ZerodhaData\\" + d.Key + ".csv";
                            //IRDS::26-Aug-2019::Sanika::Added to write header into csv file
                            if (!File.Exists(dName))
                            {
                                string header = String.Join(Environment.NewLine, "Date,Time,Open,High,Low,Close,Volume");
                                header += Environment.NewLine;
                                System.IO.File.AppendAllText(dName, header);
                            }
                            using (StreamWriter streamWriter = File.AppendText(dName))
                            {
                                string time = null;
                                int lastIndex = d.Value[0].LastIndexOf(":");
                                if (lastIndex != -1)
                                {
                                    time = d.Value[0].Substring(0, lastIndex);
                                }
                                String csv = String.Join(Environment.NewLine, todayDate + "," + time + "," + d.Value[2] + "," + d.Value[3] + "," + d.Value[4] + "," + d.Value[5] + "," + d.Value[6]);

                                streamWriter.WriteLine(csv);
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    logger.LogMessage("WriteTickDatainCSVFile : Exception Error Message = " + e.Message, MessageType.Exception);
                }
            }
        }

        public void createArray(Tick data, DateTime dstart, DateTime dend, string symbolName)
        {
            try
            {
                string currentTime = (Convert.ToDateTime(data.Timestamp.ToString())).ToString("HH:mm:ss");
                string start = dstart.ToString("HH:mm:ss");
                string end = dend.ToString("HH:mm:ss");
                if (dictionaryForLastTickTime.ContainsKey(symbolName))
                {
                    dictionaryForLastTickTime[symbolName][0] = dictionaryForLastTickTime[symbolName][1];
                    dictionaryForLastTickTime[symbolName][1] = currentTime;
                }
                else
                {
                    List<string> tmplist = new List<string>();
                    tmplist.Insert(0, currentTime);
                    tmplist.Insert(1, currentTime);
                    dictionaryForLastTickTime.Add(symbolName, new List<string>(tmplist));
                }

                //for every 1st data of tick
                if (!ListOfSymbolsForStartValues.Contains(symbolName))
                {
                    List<string> list = new List<string>();
                    list.Insert(0, start);
                    list.Insert(1, end);
                    list.Insert(2, data.LastPrice.ToString());
                    list.Insert(3, data.LastPrice.ToString());
                    list.Insert(4, data.LastPrice.ToString());
                    list.Insert(5, data.LastPrice.ToString());
                    list.Insert(6, data.Volume.ToString());
                    list.Insert(7, data.Volume.ToString()); // for intial volume

                    if (ZerodhaData.ContainsKey(symbolName))
                    {
                        ZerodhaData[symbolName] = new List<string>(list);
                    }
                    else
                    {
                        ZerodhaData.TryAdd(symbolName, new List<string>(list));
                    }

                    ListOfSymbolsForStartValues.Add(symbolName);
                }
                else
                {
                    //IRDS::27-dec-2019::Commented now we are taking first open instead of latest one
                    ////for latest open
                    //if (!ListStartOpenValues.Contains(symbolName))
                    //{
                    //    if (dictionaryForLastTickTime.ContainsKey(symbolName))
                    //    {
                    //        if (dictionaryForLastTickTime[symbolName][0] == dictionaryForLastTickTime[symbolName][1])
                    //        {
                    //            ZerodhaData[symbolName][2] = data.LastPrice.ToString();
                    //        }
                    //    }
                    //    ListStartOpenValues.Add(symbolName);
                    //}

                    if (ZerodhaData.ContainsKey(symbolName))
                    {
                        ZerodhaData[symbolName][0] = start;
                        ZerodhaData[symbolName][1] = end;
                        if (Convert.ToDecimal(ZerodhaData[symbolName][3]) < Convert.ToDecimal(data.LastPrice))
                        {
                            ZerodhaData[symbolName][3] = data.LastPrice.ToString();
                        }

                        if (Convert.ToDecimal(ZerodhaData[symbolName][4]) > Convert.ToDecimal(data.LastPrice))
                        {
                            ZerodhaData[symbolName][4] = data.LastPrice.ToString();
                        }

                        ZerodhaData[symbolName][5] = data.LastPrice.ToString();
                        decimal volume = Convert.ToDecimal(data.Volume) - Convert.ToDecimal(ZerodhaData[symbolName][7]);
                        ZerodhaData[symbolName][6] = volume.ToString();
                    }
                    else
                    {
                        List<string> list = new List<string>();
                        list.Insert(0, start);
                        list.Insert(1, end);
                        list.Insert(2, data.LastPrice.ToString());
                        list.Insert(3, data.LastPrice.ToString());
                        list.Insert(4, data.LastPrice.ToString());
                        list.Insert(5, data.LastPrice.ToString());
                        list.Insert(6, data.Volume.ToString());
                        list.Insert(7, data.Volume.ToString()); // for intial volume

                        if (ZerodhaData.ContainsKey(symbolName))
                        {
                            ZerodhaData[symbolName] = new List<string>(list);
                        }
                        else
                        {
                            ZerodhaData.TryAdd(symbolName, new List<string>(list));
                        }
                    }
                }
            }
            catch (Exception e)
            {
                logger.LogMessage("CreateArray : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }


        //public string getName(string token)
        //{
        //    string symbolName = "";
        //    var path_DB = Directory.GetCurrentDirectory();
        //    string directoryName = path_DB + "\\" + "Database";

        //    string DBFilePath = directoryName + "\\" + "kite.db";
        //    try
        //    {
        //        string connectionString = String.Format("Data Source={0};Version=3;New=False;Compress=True;", DBFilePath);
        //        if (Sql_con == null)
        //        {

        //        }
        //        using (Sql_con = new SQLiteConnection(connectionString))
        //        {
        //            Sql_con.Open();

        //            using (Tr = Sql_con.BeginTransaction())
        //            {
        //                string query = "select symbol FROM instruments where InstrumentToken = " + token;
        //                SQLiteCommand Sql_cmd = new SQLiteCommand(query, Sql_con);
        //                Sql_read = Sql_cmd.ExecuteReader();
        //                if (Sql_read != null && Sql_read.HasRows)
        //                {
        //                    while (Sql_read.Read())
        //                    {
        //                        symbolName = (string)Sql_read["symbol"];
        //                    }
        //                }
        //                Tr.Commit();
        //            }

        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine("Exception : " + e.Message);
        //    }
        //    return symbolName;
        //}
        bool m_OrderUpdate = false;
        public void OnOrderUpdate(Order OrderData)
        {
            //if (TradingSymbol.Contains(OrderData.Tradingsymbol + "." + OrderData.Exchange) || RealTimeSymbols.Contains(OrderData.Tradingsymbol + "." + OrderData.Exchange))
            {                
                OrderExt objOrderExt = new OrderExt(OrderData);
                m_GlobalOrderStore.AddOrUpdateOrder(objOrderExt);
                m_GlobalOrderStore.AddOrder(objOrderExt);
                SetOrderUpdateStatus(true);
                Trace.WriteLine("OrderUpdate " + Utils.JsonSerialize(OrderData));
                WriteUniquelogs("OnOrderUpdate", "OrderUpdate " + Utils.JsonSerialize(OrderData), MessageType.Informational);
                // Console.WriteLine("OrderUpdate " + Utils.JsonSerialize(OrderData));
                // testc(objOrderExt.order.OrderId.ToString());
            }
        }
        public void SetOrderUpdateStatus(bool isUpdated)
        {
            //WriteUniquelogs("OnOrderUpdate", "SetOrderUpdateStatus : m_OrderUpdate set to true ", MessageType.Informational);
            m_OrderUpdate = isUpdated;
        }
        public bool GetOrderUpdateStatus()
        {
            return m_OrderUpdate;
        }
        private static readonly object m_WriteLog = new object();
        public void WriteUniquelogs(string symbol, string message, MessageType msgType)
        {
            try
            {
                lock (m_WriteLog)
                {
                    SymbolLogs log = new SymbolLogs();
                    log.createFile(symbol);
                    log.LogMessage(message, MessageType.Informational);
                    log = null;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : " + e.Message);
                Trace.WriteLine("Exception : " + e.Message);
            }
        }

        //public void testc(string orderid)
        //{
        //    OrderExt orderExt;
        //    m_GlobalOrderStore.GetOpenOrderbyID("IOC", "NSE", orderid, out orderExt);
        //    List<Order> orderInfo = m_GlobalOrderStore.GetOpenOrderbySymbol("IOC", "NSE");
        //    int i = 0;
        //    i++;
        //}

        /// <summary>
        /// Retrieve historical data (candles) for an instrument.
        /// </summary>
        /// <param name="InstrumentToken">Identifier for the instrument whose historical records you want to fetch. This is obtained with the instrument list API.</param>
        /// <param name="FromDate">Date in format yyyy-MM-dd for fetching candles between two days. Date in format yyyy-MM-dd hh:mm:ss for fetching candles between two timestamps.</param>
        /// <param name="ToDate">Date in format yyyy-MM-dd for fetching candles between two days. Date in format yyyy-MM-dd hh:mm:ss for fetching candles between two timestamps.</param>
        /// <param name="Interval">The candle record interval. Possible values are: minute, day, 3minute, 5minute, 10minute, 15minute, 30minute, 60minute</param>
        /// <param name="Continuous">Pass true to get continous data of expired instruments.</param>
        /// <returns>List of Historical objects.</returns>
        public List<Historical> GetHistoricalData(
            string InstrumentToken,
            DateTime FromDate,
            DateTime ToDate,
            string Interval,
            bool Continuous = false)
        {
            var param = new Dictionary<string, dynamic>();

            param.Add("instrument_token", InstrumentToken);
            param.Add("from", FromDate.ToString("yyyy-MM-dd HH:mm:ss"));
            param.Add("to", ToDate.ToString("yyyy-MM-dd HH:mm:ss"));
            param.Add("interval", Interval);
            param.Add("continuous", Continuous ? "1" : "0");

            var historicalData = Get("market.historical", param);

            List<Historical> historicals = new List<Historical>();

            foreach (ArrayList item in historicalData["data"]["candles"])
                historicals.Add(new Historical(item));

            return historicals;
        }


        public List<Historical> GetHistoricalDataWithOtherAccount(
           string InstrumentToken,
           DateTime FromDate,
           DateTime ToDate,
           string Interval,
           bool Continuous = false)
        {
            var param = new Dictionary<string, dynamic>();

            param.Add("instrument_token", InstrumentToken);
            param.Add("from", FromDate.ToString("yyyy-MM-dd HH:mm:ss"));
            param.Add("to", ToDate.ToString("yyyy-MM-dd HH:mm:ss"));
            param.Add("interval", Interval);
            param.Add("continuous", Continuous ? "1" : "0");

            var historicalData = GetForDifferentAccount("market.historical", param);

            List<Historical> historicals = new List<Historical>();

            foreach (ArrayList item in historicalData["data"]["candles"])
                historicals.Add(new Historical(item));

            return historicals;
        }


        /// <summary>
        /// Get account balance and cash margin details for a particular segment.
        /// </summary>
        /// <param name="Segment">Trading segment (eg: equity or commodity)</param>
        /// <returns>Margins for specified segment.</returns>
        public UserMargin GetMargins(string Segment)
        {
            var userMarginData = Get("user.segment_margins", new Dictionary<string, dynamic> { { "segment", Segment } });
            return new UserMargin(userMarginData["data"]);
        }

        /// <summary>
        /// Retrieve LTP and OHLC of upto 200 instruments
        /// </summary>
        /// <param name="InstrumentId">Indentification of instrument in the form of EXCHANGE:TRADINGSYMBOL (eg: NSE:INFY) or InstrumentToken (eg: 408065)</param>
        /// <returns>Dictionary of all OHLC objects with keys as in InstrumentId</returns>
        public Dictionary<string, OHLC> GetOHLC(string[] InstrumentId)
        {
            var param = new Dictionary<string, dynamic>();
            param.Add("i", InstrumentId);
            Dictionary<string, dynamic> ohlcData = Get("market.ohlc", param)["data"];

            Dictionary<string, OHLC> ohlcs = new Dictionary<string, OHLC>();
            foreach (string item in ohlcData.Keys)
                ohlcs.Add(item, new OHLC(ohlcData[item]));

            return ohlcs;
        }

        public Dictionary<string, Quote> GetQuote(string[] InstrumentId)
        {
            var param = new Dictionary<string, dynamic>();
            param.Add("i", InstrumentId);
            Dictionary<string, dynamic> quoteData = Get("market.quote", param)["data"];

            Dictionary<string, Quote> quotes = new Dictionary<string, Quote>();
            foreach (string item in quoteData.Keys)
                quotes.Add(item, new Quote(quoteData[item]));

            return quotes;
        }

        public List<Holding> GetHoldings()
        {
            var holdingsData = Get("portfolio.holdings");

            List<Holding> holdings = new List<Holding>();

            foreach (Dictionary<string, dynamic> item in holdingsData["data"])
                holdings.Add(new Holding(item));

            return holdings;
        }

        //IRDSPM::Pratiksha::-01-07-2020::For fetching symbol names from db --start
        //IRDS::Jyoti::12-Sept-20::Added exchange for symbol settings form
        public List<string> GetDataforTradingsymbol(string exchange)
        {
            List<string> symbolDet = new List<string>();
            string token = "";
            try
            {
                if (Sql_conFetchSymbolName == null)
                {
                    var path_DB = Directory.GetCurrentDirectory();
                    string directoryName = path_DB + "\\" + "Database";

                    string DBFilePath = directoryName + "\\" + "kite.db";
                    string connectionString = String.Format("Data Source={0};Version=3;New=False;Compress=True;", DBFilePath);
                    Sql_conFetchSymbolName = new SQLiteConnection(connectionString);
                    Sql_conFetchSymbolName.Open();
                }
                SQLiteTransaction TrToken;
                SQLiteCommand Sql_cmdToken;
                SQLiteDataReader Sql_readToken;

                string condition = "";
                string tableName = "instruments";
                if (exchange == "NFO")
                {
                    condition = " where exchange = 'NFO';";
                    tableName = FetchTableName();
                }
                else if (exchange == "MCX")
                {
                    condition = " where exchange = 'MCX';";
                    tableName = FetchTableName();
                }
                else if (exchange == "OPTIONS")
                {
                    tableName = "options";
                }
                //Pratiksha::01-12-2020:: For CDS
                else if (exchange == "CDS")
                {
                    tableName = "Currency";
                }
                string query = "select symbol from '" + tableName + "'" + condition;
                Sql_cmdToken = new SQLiteCommand(query, Sql_conFetchSymbolName);
                Sql_readToken = Sql_cmdToken.ExecuteReader();
                if (Sql_readToken != null && Sql_readToken.HasRows)
                {
                    while (Sql_readToken.Read())
                    {
                        token = (Sql_readToken["symbol"]).ToString();
                        symbolDet.Add(token);
                    }
                }
            }
            catch (Exception e)
            {
                logger.LogMessage("getInstrumentToken : Exception Error Message = " + e.Message, MessageType.Exception);
                //Console.WriteLine("Exception : " + e.Message);
            }
            return symbolDet;
        }

        //IRDS::03-Jul-2020::Jyoti::added for queue changes
        //IRDS::04-August-2020::Sandip::process tick data
        public void ProcessTickDataToDatabase()
        {            
            Trace.WriteLine("ProcessTickDataToDatabase");
            long myprocesstick = 0;
            long Totalmyprocesstick = 0;
            //sanika::29-sep-2020::added stop thread condition
            while (true && m_StopThread == false)
            {
                if(m_useMysql == false && m_useCSV == false && m_useSqlite == false)
                {
                    Trace.WriteLine("ProcessTickDataToDatabase");
                    Thread.Sleep(1000);
                    continue;
                }
                Thread.Sleep(1);
                //sanika::18-Nov-2020::changed end time to mcx end time
                if (DateTime.Now.ToOADate() <= EndTimeMCX.ToOADate())
                {
                    Tick tick;
                    //sanika::29-sep-2020::added stop thread condition
                    while (m_queueTick.TryDequeue(out tick) && m_StopThread == false)
                    {                        
                        myprocesstick++;
                        Totalmyprocesstick++;
                        //if (myprocesstick % 10 == 0)
                        //{
                        //    Trace.WriteLine("##TICKStatus : 100 Ticks process  " + m_queueTick.Count.ToString() + " myprocesstick " + tickCurrentTime + " Totalmyprocesstick " + Totalmyprocesstick.ToString() );
                        //    myprocesstick = 0;//
                        //    Thread.Sleep(1);
                        //    if (Totalmyprocesstick > 100000) Totalmyprocesstick = 0;                            
                        //}
                        //continue;
                        try
                        {
                            if (tick.Timestamp != null)
                            {
                                //string currentDate = DateTime.Now.ToString("M/d/yyyy");
                                //string tickDate = (Convert.ToDateTime(tick.Timestamp.ToString())).ToString("M/d/yyyy");
                                string symbolName = SymbolListWithFutureName[tick.InstrumentToken.ToString()];
                                if (true )//(tickDate == currentDate))
                                {
                                    DateTime tickDateTime = (DateTime)tick.Timestamp;
                                    //tickCurrentTime = (Convert.ToDateTime(tick.Timestamp.ToString())).ToString("HH:mm:ss");
                                    if (myprocesstick % 100 == 0)
                                    {
                                        Trace.WriteLine("##TICKStatus : 100 Ticks process  " + m_queueTick.Count.ToString() + " myprocesstick " + tickCurrentTime + " Totalmyprocesstick " + Totalmyprocesstick.ToString()+" symbol "+ symbolName);
                                        myprocesstick = 0;//
                                        Thread.Sleep(1);
                                        if (Totalmyprocesstick > 100000) Totalmyprocesstick = 0;
                                    }
                                    createBar(tick, tickDateTime);
                                    continue;

                                    //sanika::18-Nov-2020::added condition of end time exchange wise

                                    DateTime EndTime = GetEndTime(symbolName);
                                    if (tickDateTime.ToOADate() <= EndTime.ToOADate())
                                    {
                                        
                                        DateTime startDateTime = GetStartTime(symbolName);
                                        if (tickDateTime.ToOADate() >= startDateTime.ToOADate())
                                        {
                                            //sanika::22-sep-2020::remove dictionary and added structure
                                            if (m_TickData!=null)
                                            {
                                                Trace.WriteLine("##TICKStatus : started to update structure");
                                               // m_TickData.AddOrUpdateTickData(tick, symbolName);
                                                Trace.WriteLine("##TICKStatus : stopped to update structure");
                                            }
                                           // createBar(tick, tickDateTime);
                                        } 
                                    }
                                }
                            }
                        }

                        catch (Exception e)
                        {
                            Trace.WriteLine("##TICKStatus : Exception from ProcessTickDataToDatabas " + e.Message);
                            logger.LogMessage("ProcessTickDataToDatabase : Exception Error Message " + e.Message, MessageType.Exception);
                            //Trace.WriteLine("from insertFromQueueInTable : " + e.Message + " " + tick.Timestamp.ToString());
                        }
                        Thread.Sleep(1);//sleep 1sec to recive more ticks
                    }
                }
                else
                {
                    StopThread();
                }
            }
            logger.LogMessage("ProcessTickDataToDatabase : Thread stopped m_StopThread " + m_StopThread, MessageType.Informational);
        }
        

        public void ProcessTickDataToDatabaseNew(string token)
        {
            Trace.WriteLine("ProcessTickDataToDatabase");
            long myprocesstick = 0;
            long Totalmyprocesstick = 0;
            //sanika::29-sep-2020::added stop thread condition
            while (true && m_StopThread == false)
            {
                if (m_useMysql == false && m_useCSV == false && m_useSqlite == false)
                {
                    Trace.WriteLine("ProcessTickDataToDatabase");
                    Thread.Sleep(1000);
                    continue;
                }
                Thread.Sleep(1);
                //sanika::18-Nov-2020::changed end time to mcx end time
                if (DateTime.Now.ToOADate() <= EndTimeMCX.ToOADate())
                {
                    Tick tick;                   
                    ConcurrentQueue<Tick> _queue = null;
                    if(m_QueueOfAllSymbols.ContainsKey(token))
                        _queue =  m_QueueOfAllSymbols[token];
                    while (_queue.TryDequeue(out tick) && m_StopThread == false) //sanika::29-sep-2020::added stop thread condition
                    {
                        myprocesstick++;
                        Totalmyprocesstick++;
                        try
                        {
                            if (tick.Timestamp != null)
                            {
                                string currentDate = DateTime.Now.ToString("M/d/yyyy");
                                string tickDate = (Convert.ToDateTime(tick.Timestamp.ToString())).ToString("M/d/yyyy");
                                if ((tickDate == currentDate))
                                {
                                    string symbolName = SymbolListWithFutureName[tick.InstrumentToken.ToString()];
                                    DateTime tickDateTime = (DateTime)tick.Timestamp;
                                    tickCurrentTime = (Convert.ToDateTime(tick.Timestamp.ToString())).ToString("HH:mm:ss");
                                    if (myprocesstick % 10 == 0)
                                    {
                                        Trace.WriteLine("##$$$TICKStatus :Current Ticks process  " + _queue.Count.ToString() + " myprocesstick " + tickCurrentTime + " Totalmyprocesstick " + Totalmyprocesstick.ToString() +" symbol "+ symbolName);
                                        myprocesstick = 0;//
                                        Thread.Sleep(100);
                                        if (Totalmyprocesstick > 100000) Totalmyprocesstick = 0;
                                    }

                                    //sanika::18-Nov-2020::added condition of end time exchange wise
                                   
                                    DateTime EndTime = GetEndTime(symbolName);
                                    if (tickDateTime.ToOADate() <= EndTime.ToOADate())
                                    {

                                        DateTime startDateTime = GetStartTime(symbolName);
                                        if (tickDateTime.ToOADate() >= startDateTime.ToOADate())
                                        {
                                            //sanika::22-sep-2020::remove dictionary and added structure
                                            if (m_TickData != null)
                                            {
                                                Trace.WriteLine("##TICKStatus : started to update structure");
                                               // m_TickData.AddOrUpdateTickData(tick, symbolName);
                                                Trace.WriteLine("##TICKStatus : stopped to update structure");
                                            }
                                            createBar(tick, tickDateTime);
                                        }
                                    }
                                }
                            }
                        }

                        catch (Exception e)
                        {
                            Trace.WriteLine("##TICKStatus : Exception from ProcessTickDataToDatabas " + e.Message);
                            logger.LogMessage("ProcessTickDataToDatabase : Exception Error Message " + e.Message, MessageType.Exception);
                            //Trace.WriteLine("from insertFromQueueInTable : " + e.Message + " " + tick.Timestamp.ToString());
                        }
                        Thread.Sleep(1);//sleep 1sec to recive more ticks
                    }
                }
                else
                {
                    StopThread();
                }
            }
            logger.LogMessage("ProcessTickDataToDatabase : Thread stopped m_StopThread " + m_StopThread, MessageType.Informational);
        }

        public DateTime GetEndTime(string symbolName)
        {
            DateTime dateTime = new DateTime();
            if(m_NSESymbolsInstruements.Contains(symbolName))
            {
                dateTime = EndTimeNSE;
            }
            else if(m_NFOSymbolsInstruements.Contains(symbolName))
            {
                dateTime = EndTimeNFO;
            }
            else if (m_MCXSymbolsInstruements.Contains(symbolName))
            {
                dateTime = EndTimeMCX;
            }
            //sanika::2-dec-2020::Added for cds
            else if (m_CDSSymbolsInstruements.Contains(symbolName))
            {
                dateTime = EndTimeCDS;
            }
            return dateTime;
        }
        public DateTime GetStartTime(string symbolName)
        {
            DateTime dateTime = new DateTime();
            if(m_NSESymbolsInstruements.Contains(symbolName))
            {
                dateTime = StartNSE;
            }
            else if(m_NFOSymbolsInstruements.Contains(symbolName))
            {
                dateTime = StartNFO;
            }
            else if(m_MCXSymbolsInstruements.Contains(symbolName))
            {
                dateTime = StartMCX;
            }
            //sanika::2-dec-2020::Added for cds
            else if (m_CDSSymbolsInstruements.Contains(symbolName))
            {
                dateTime = StartCDS;
            }
            return dateTime;
        }

        public void MCXProcessTickDataToDatabase(string token)
        {
            long myprocesstick = 0;
            string symbolLog = "";
            //sanika::29-sep-2020::added stop thread condition
            while (true && m_StopThread == false)
            {
                Tick data;
                //sanika::15-Dec-2020::Added to restart exe
                RestartExe();
                ConcurrentQueue<Tick> _queue = null;
                if (m_QueueOfAllSymbols.ContainsKey(token))
                    _queue = m_QueueOfAllSymbols[token];
                //sanika::29-sep-2020::added stop thread condition
                while (_queue.TryDequeue(out data) && m_StopThread == false)
                    {
                        //sanika::15-Dec-2020::Added to restart exe
                        RestartExe();
                        if (myprocesstick % 100 == 0)
                        {
                            Trace.WriteLine("##TICKStatus : LOOOPING Process Tick Data " + _queue.Count.ToString() + " myprocesstick " + myprocesstick.ToString());
                            myprocesstick = 0;//
                            Thread.Sleep(1);
                        }
                        myprocesstick++;
                        try
                        {
                            if (m_isConnected)
                            {
                                //IRDS::Jyoti::14-08-2020::Added condition for ohlc > 0
                                if (data.Timestamp != null && data.Open > 0 && data.High > 0 && data.Low > 0 && data.Close > 0)
                                {
                                    string currentDate = DateTime.Now.ToString("M/d/yyyy");
                                    string tickDate = (Convert.ToDateTime(data.Timestamp.ToString())).ToString("M/d/yyyy");
                                    DateTime tickDateTime = (DateTime)data.Timestamp;
                                    if ((tickDate == currentDate))
                                    {
                                        DateTime endDateTime = DateTime.Now;
                                        string symbolName = SymbolListWithFutureName[data.InstrumentToken.ToString()];
                                        symbolLog = symbolName;
                                        DateTime startDateTimeTick = GetStartTime(symbolName);
                                        TimeSpan difference = endDateTime - startDateTime;
                                        //string symbolName = SymbolListWithFutureName[data.InstrumentToken.ToString()];
                                        if (tickDateTime.ToOADate() >= startDateTimeTick.ToOADate())
                                        {
                                        //sanika::22-sep-2020::remove dictionary and added structure
                                            if (m_TickData != null)
                                            {
                                                m_TickData.AddOrUpdateTickData(data, symbolName);
                                            }
                                            decimal changeInValues = data.LastPrice - data.Close;
                                            decimal basePrice;
                                            if (data.LastPrice > data.Close)
                                            {
                                                basePrice = data.LastPrice;
                                            }
                                            else
                                            {
                                                basePrice = data.Close;
                                            }
                                            decimal changeInPercentage = Math.Round(((changeInValues / basePrice) * 100), 2);
                                            string tableName = SymbolListWithExchanges[symbolName];
                                            //tableName = tableName.Replace("-", "");
                                            //tableName = tableName.Replace("&", "");
                                            string newSymbolName = tableName.Split('_')[0];

                                            //IRDS::07-Aug-2020::Jyoti::Commented insert for each tick
                                            /*if (SymbolListExistingRecord[tableName] > 0)
                                            {
                                                m_Mysql.UpdateRow(tableName, data.LastTradeTime.ToString(), newSymbolName, data.Bids[0].Price, data.Offers[0].Price, data.LastPrice, changeInValues, changeInPercentage, data.Open, data.High, data.Low, data.Close, data.AveragePrice, data.Volume, data.OI, data.BuyQuantity, data.SellQuantity, SymbolListWithExpDate[symbolName]);
                                            }
                                            else
                                            {
                                                m_Mysql.InsertDataMultipleValues(tableName, "(STR_TO_DATE('" + data.LastTradeTime + "', '%d-%m-%Y %H:%i:%s'),'" + newSymbolName + "','" + data.Bids[0].Price + "','" + data.Offers[0].Price + "','" + data.LastPrice + "','" + changeInValues + "','" + changeInPercentage + "','" + data.Open + "','" + data.High + "','" + data.Low + "','" + data.Close + "','" + data.AveragePrice + "','" + data.Volume + "','" + data.OI + "','" + data.BuyQuantity + "','" + data.SellQuantity + "','" + SymbolListWithExpDate[symbolName] + "')");
                                                SymbolListExistingRecord[tableName] = 1;
                                            }*/

                                        //IRDS::07-Aug-2020::Jyoti::Added insert after 1 sec
                                            if (difference.Milliseconds > 500)//time.ToOADate() >= TickStartTime[symbolName].ToOADate())
                                            {
                                                Trace.WriteLine("##TICKStatus : &&&&&&&&&&&&&&&& Starts 1 sec Data &&&&&&&&&&&&&&&&&&&&& " + _queue.Count.ToString() + " symbolName " + symbolName + " " + data.LastTradeTime);
                                                startDateTime = DateTime.Now;
                                                try
                                                {
                                                    m_Mysql.beginTran();
                                                    //Parallel.ForEach(DictMysqlData.Keys, item =>
                                                    foreach (var item in DictMysqlData.Keys)
                                                    {
                                                        tableName = item;
                                                        string[] values = DictMysqlData[item].Split(',');
                                                        //IRDS::Jyoti::14-08-2020::Added condition for ohlc > 0
                                                        if (values[7] != "0" && values[8] != "0" && values[9] != "0" && values[10] != "0")
                                                        {
                                                            int bidChange = 0;
                                                            int askChange = 0;
                                                            int ltpChange = 0;
                                                            if (OldDictMysqlData.ContainsKey(item))
                                                            {
                                                                string[] oldValues = OldDictMysqlData[item].Split(',');
                                                                //bidChange = Convert.ToInt32(oldValues[17]);
                                                                //askChange = Convert.ToInt32(oldValues[18]);
                                                                //ltpChange = Convert.ToInt32(oldValues[19]);
                                                                if (Convert.ToDecimal(values[2]) < Convert.ToDecimal(oldValues[2]))
                                                                {
                                                                    bidChange = -1;
                                                                }
                                                                if (Convert.ToDecimal(values[2]) > Convert.ToDecimal(oldValues[2]))
                                                                {
                                                                    bidChange = 1;
                                                                }
                                                                if (Convert.ToDecimal(values[3]) < Convert.ToDecimal(oldValues[3]))
                                                                {
                                                                    askChange = -1;
                                                                }
                                                                if (Convert.ToDecimal(values[3]) > Convert.ToDecimal(oldValues[3]))
                                                                {
                                                                    askChange = 1;
                                                                }
                                                                if (Convert.ToDecimal(values[4]) < Convert.ToDecimal(oldValues[4]))
                                                                {
                                                                    ltpChange = -1;
                                                                }
                                                                if (Convert.ToDecimal(values[4]) > Convert.ToDecimal(oldValues[4]))
                                                                {
                                                                    ltpChange = 1;
                                                                }
                                                                //Trace.WriteLine("Old Record: " + OldDictMysqlData[item] + " AskChange: " + askChange + " BidChange: " + bidChange + " LtpChange: " + ltpChange);
                                                                //Trace.WriteLine("New Record: " + DictMysqlData[item]);

                                                                //DateTime oldDate = DateTime.ParseExact(Convert.ToDateTime(values[0]).ToString("dd-MM-yyyy") + " 00:00:00", "dd-MM-yyyy HH:mm:ss", null);
                                                                string oldDate = Convert.ToDateTime(oldValues[0]).ToString("dd-MM-yyyy");
                                                                string currDate = Convert.ToDateTime(values[0]).ToString("dd-MM-yyyy");
                                                                if (oldDate != currDate)
                                                                {
                                                                    logger.LogMessage("New Date found: oldDate: " + oldDate + " newDate: " + currDate, MessageType.Informational);
                                                                    DateTime olddate = DateTime.ParseExact(oldDate, "dd-MM-yyyy", null);
                                                                    DateTime currdate = DateTime.ParseExact(currDate, "dd-MM-yyyy", null);
                                                                    if (olddate < currdate)
                                                                    {
                                                                        logger.LogMessage("Yesterday record: " + OldDictMysqlData[item], MessageType.Informational);
                                                                        logger.LogMessage("Yesterday Values to be updated as: " + oldValues[7] + "," + oldValues[8] + "," + oldValues[9] + "," + oldValues[10], MessageType.Informational);
                                                                        SymbolListYestRecord[item] = oldValues[7] + "," + oldValues[8] + "," + oldValues[9] + "," + oldValues[10];
                                                                    }
                                                                }
                                                            }
                                                            if (SymbolListExistingRecord[tableName] > 0)
                                                            {
                                                                //m_Mysql.UpdateRow(tableName, data.LastTradeTime.ToString(), newSymbolName, data.Bids[0].Price, data.Offers[0].Price, data.LastPrice, changeInValues, changeInPercentage, data.Open, data.High, data.Low, data.Close, data.AveragePrice, data.Volume, data.OI, data.BuyQuantity, data.SellQuantity, SymbolListWithExpDate[symbolName]);
                                                                string[] yValues = SymbolListYestRecord[tableName].Split(',');
                                                                //m_Mysql.UpdateRow(tableName, values[0], values[1], Convert.ToDecimal(values[2]), Convert.ToDecimal(values[3]), Convert.ToDecimal(values[4]), Convert.ToDecimal(values[5]), Convert.ToDecimal(values[6]), Convert.ToDecimal(values[7]), Convert.ToDecimal(values[8]), Convert.ToDecimal(values[9]), Convert.ToDecimal(values[10]), Convert.ToDecimal(values[11]), Convert.ToUInt32(values[12]), Convert.ToUInt32(values[13]), Convert.ToUInt32(values[14]), Convert.ToUInt32(values[15]), values[16], Convert.ToInt32(values[17]), Convert.ToInt32(values[18]), Convert.ToInt32(values[19]), Convert.ToDecimal(yValues[0]), Convert.ToDecimal(yValues[1]), Convert.ToDecimal(yValues[2]), Convert.ToDecimal(yValues[3]));
                                                                m_Mysql.UpdateRow(tableName, values[0], values[1], Convert.ToDecimal(values[2]), Convert.ToDecimal(values[3]), Convert.ToDecimal(values[4]), Convert.ToDecimal(values[5]), Convert.ToDecimal(values[6]), Convert.ToDecimal(values[7]), Convert.ToDecimal(values[8]), Convert.ToDecimal(values[9]), Convert.ToDecimal(values[10]), Convert.ToDecimal(values[11]), Convert.ToUInt32(values[12]), Convert.ToUInt32(values[13]), Convert.ToUInt32(values[14]), Convert.ToUInt32(values[15]), values[16], bidChange, askChange, ltpChange, Convert.ToDecimal(yValues[0]), Convert.ToDecimal(yValues[1]), Convert.ToDecimal(yValues[2]), Convert.ToDecimal(yValues[3]));
                                                            }
                                                            else
                                                            {
                                                                //m_Mysql.InsertDataMultipleValues(tableName, "(STR_TO_DATE('" + data.LastTradeTime + "', '%d-%m-%Y %H:%i:%s'),'" + newSymbolName + "','" + data.Bids[0].Price + "','" + data.Offers[0].Price + "','" + data.LastPrice + "','" + changeInValues + "','" + changeInPercentage + "','" + data.Open + "','" + data.High + "','" + data.Low + "','" + data.Close + "','" + data.AveragePrice + "','" + data.Volume + "','" + data.OI + "','" + data.BuyQuantity + "','" + data.SellQuantity + "','" + SymbolListWithExpDate[symbolName] + "')");
                                                                //string[] yValues = SymbolListYestRecord[tableName].Split(',');
                                                                //m_Mysql.InsertDataMultipleValues(tableName, "(STR_TO_DATE('" + values[0] + "', '%d-%m-%Y %H:%i:%s'),'" + values[1] + "','" + values[2] + "','" + values[3] + "','" + values[4] + "','" + values[5] + "','" + values[6] + "','" + values[7] + "','" + values[8] + "','" + values[9] + "','" + values[10] + "','" + values[11] + "','" + values[12] + "','" + values[13] + "','" + values[14] + "','" + values[15] + "','" + values[16] + "','" + values[17] + "','" + values[18] + "','" + values[19] + "','" + yValues[0] + "','" + yValues[1] + "','" + yValues[2] + "','" + yValues[3] + "')");
                                                                //IRDS::Jyoti::14-08-2020::Inserted yesterday ohlc as current ohlc
                                                                m_Mysql.InsertDataMultipleValues(tableName, "(STR_TO_DATE('" + values[0] + "', '%d-%m-%Y %H:%i:%s'),'" + values[1] + "','" + values[2] + "','" + values[3] + "','" + values[4] + "','" + values[5] + "','" + values[6] + "','" + values[7] + "','" + values[8] + "','" + values[9] + "','" + values[10] + "','" + values[11] + "','" + values[12] + "','" + values[13] + "','" + values[14] + "','" + values[15] + "','" + values[16] + "','" + bidChange + "','" + askChange + "','" + ltpChange + "','" + values[7] + "','" + values[8] + "','" + values[9] + "','" + values[10] + "')");
                                                                SymbolListExistingRecord[tableName] = 1;
                                                            }
                                                            //DictMysqlData[item] = DictMysqlData[item] + "," + bidChange + "," + askChange + "," + ltpChange;
                                                            if (OldDictMysqlData.ContainsKey(item))
                                                            {
                                                                OldDictMysqlData[item] = DictMysqlData[item] + "," + bidChange + "," + askChange + "," + ltpChange;
                                                            }
                                                            else
                                                            {
                                                                OldDictMysqlData.Add(item, DictMysqlData[item] + "," + bidChange + "," + askChange + "," + ltpChange);
                                                            }
                                                        }
                                                    }
                                                    //OldDictMysqlData = DictMysqlData;
                                                    //DictMysqlData.Clear();
                                                    //);
                                                }
                                                catch (Exception e)
                                                {
                                                    logger.LogMessage("MCXProcessTickDataToDatabase : Exception Error Message =  " + e.Message, MessageType.Exception);
                                                }
                                                finally
                                                {
                                                    m_Mysql.CommitTran();
                                                }
                                                Trace.WriteLine("##TICKStatus : &&&&&&&&&&&&&&&& ENDENDENDENDN 1 sec Data &&&&&&&&&&&&&&&&&&&&&");
                                            }
                                            else
                                            {
                                                if (DictMysqlData.ContainsKey(tableName))
                                                {
                                                    //DictMysqlData[tableName] = data.LastTradeTime.ToString() +","+ newSymbolName + "," + data.Bids[0].Price + "," + data.Offers[0].Price + "," + data.LastPrice + "," + changeInValues + "," + changeInPercentage + "," + data.Open + "," + data.High + "," + data.Low + "," + data.Close + "," + data.AveragePrice + "," + data.Volume + "," + data.OI + "," + data.BuyQuantity + "," + data.SellQuantity + "," + SymbolListWithExpDate[symbolName] + "," + bidChange + "," + askChange + "," + ltpChange;
                                                    DictMysqlData[tableName] = data.LastTradeTime.ToString() + "," + newSymbolName + "," + data.Bids[0].Price + "," + data.Offers[0].Price + "," + data.LastPrice + "," + changeInValues + "," + changeInPercentage + "," + data.Open + "," + data.High + "," + data.Low + "," + data.Close + "," + data.AveragePrice + "," + data.Volume + "," + data.OI + "," + data.BuyQuantity + "," + data.SellQuantity + "," + SymbolListWithExpDate[symbolName];
                                                }
                                                else
                                                {
                                                    //DictMysqlData.Add(tableName, "(STR_TO_DATE('" + data.LastTradeTime + "', '%d-%m-%Y %H:%i:%s'),'" + newSymbolName + "','" + data.Bids[0].Price + "','" + data.Offers[0].Price + "','" + data.LastPrice + "','" + changeInValues + "','" + changeInPercentage + "','" + data.Open + "','" + data.High + "','" + data.Low + "','" + data.Close + "','" + data.AveragePrice + "','" + data.Volume + "','" + data.OI + "','" + data.BuyQuantity + "','" + data.SellQuantity + "','" + SymbolListWithExpDate[symbolName] + "'),");
                                                    DictMysqlData.Add(tableName, data.LastTradeTime.ToString() + "," + newSymbolName + "," + data.Bids[0].Price + "," + data.Offers[0].Price + "," + data.LastPrice + "," + changeInValues + "," + changeInPercentage + "," + data.Open + "," + data.High + "," + data.Low + "," + data.Close + "," + data.AveragePrice + "," + data.Volume + "," + data.OI + "," + data.BuyQuantity + "," + data.SellQuantity + "," + SymbolListWithExpDate[symbolName]);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        catch (Exception e)
                        {                           
                            logger.LogMessage("MCXProcessTickDataToDatabase : Exception Error Message = " + e.Message, MessageType.Exception);
                            //Trace.WriteLine("from insertFromQueueInTable : " + e.Message + " " + tick.Timestamp.ToString());
                        }
                    }
                Thread.Sleep(1);
            }
            logger.LogMessage("MCXProcessTickDataToDatabase : Thread stopped m_StopThread " + m_StopThread, MessageType.Informational);
        }

        public void MCXinitTicker()
        {
            //IRDS::04-August-2020::Sandip::lock to create thread.
            lock (this)
            {
                if (m_Mysql == null)
                {
                    m_Mysql = new MySQLConnectZerodha();
                    //IRDS::03-Jul-2020::Jyoti::added for database settings from ini file                    
                    if (m_Mysql.Connect(logger, dbServer, dbUserid, dbPassword, databaseName))
                    {
                        m_isConnected = true;
                        // IRDS::13 - Jul - 2020::Jyoti::added for SymbolDetails table
                        if (m_Mysql.IsTableExists("SymbolDetails", databaseName))
                        {
                            //m_Mysql.CleanDatabase("SymbolDetails", databaseName);
                            m_Mysql.DeleteAll("SymbolDetails");
                        }
                        else
                        {
                            string fields = "Symbol_name VARCHAR(50) NOT NULL, Table_name VARCHAR(50), Exchange VARCHAR(50),SortIndex int NOT NULL";
                            m_Mysql.CreateTable("SymbolDetails", fields, "SortIndex");
                        }
                    }
                }

            }
            try
            {              

                string symbolName = "";
                foreach (var symbol in RealTimeSymbols)
                {
                    //IRDS::04-Aug-2020::Jyoti::Change ini expiry date
                    symbolName = symbol.Split(',')[0];
                    string expDate = symbol.Split(',')[1];
                    string year = expDate.Substring(expDate.Length - 2);
                    string month = expDate.Substring(expDate.Length - 5, 3).ToUpper();
                    string newSymbol = symbolName.Split('.')[0] + year + month + "FUT";
                    if (symbolName.Split('.')[1] == Constants.EXCHANGE_NSE && (!listOfNSESymbols.Contains(symbolName.Split('.')[0])))
                    {
                        listOfNSESymbols.Add(symbolName.Split('.')[0]);
                    }
                    else if (symbolName.Split('.')[1] == Constants.EXCHANGE_NFO && (!listOfNFOSymbols.Contains(newSymbol)))
                    {
                        //listOfNFOSymbols.Add(symbolName.Split('.')[0]);
                        listOfNFOSymbols.Add(newSymbol);
                    }
                    else if (symbolName.Split('.')[1] == Constants.EXCHANGE_MCX && (!listOfMCXSymbols.Contains(newSymbol)))
                    {
                        //listOfMCXSymbols.Add(symbolName.Split('.')[0]);
                        listOfMCXSymbols.Add(newSymbol);

                    }

                    //IRDS::13-Jul-2020::Jyoti::added for SymbolDetails table
                    //if (!SymbolListWithExchanges.ContainsKey(newSymbol))
                        updateSymbolDetails(symbol);
                }

                if (TradingSymbol.Count > 0)
                {
                    foreach (var symbol in TradingSymbol)
                    {
                        symbolName = symbol;
                        string newSymbol = symbolName.Split('.')[0];
                        if (symbolName.Split('.')[1] != Constants.EXCHANGE_NFO && (!listOfNSESymbols.Contains(symbolName.Split('.')[0])))
                        {
                            listOfNSESymbols.Add(symbolName.Split('.')[0]);
                        }
                        else if (symbolName.Split('.')[1] != Constants.EXCHANGE_NSE && (!listOfNFOSymbols.Contains(symbolName.Split('.')[0])))
                        {
                            listOfNFOSymbols.Add(symbolName.Split('.')[0]);
                        }
                        //IRDS::13-Jul-2020::Jyoti::added for SymbolDetails table
                        //if (!SymbolListWithExchanges.ContainsKey(newSymbol))
                            updateSymbolDetails(symbolName);
                    }
                }

                bool tablesRenamed = checkTablesRenamed();
                //to create db file and tables                
                if (m_isConnected)
                {
                    foreach (var data in SymbolListWithExchanges)
                    {
                        string tableName = data.Value;
                        //tableName = tableName.Replace("-", "");
                        //tableName = tableName.Replace("&", "");
                        //m_Mysql.CreateZerodhaTable(tableName, databaseName);
                        //string fields = "id INT NOT NULL AUTO_INCREMENT, LastTradeTime DATETIME NOT NULL, Symbol VARCHAR(50) NOT NULL, Bid DOUBLE NOT NULL,Ask DOUBLE NOT NULL,LTP DOUBLE NOT NULL,ChangeInValues DOUBLE NOT NULL,ChangeInPercentage DOUBLE NOT NULL,Open DOUBLE NOT NULL,High DOUBLE NOT NULL,Low DOUBLE NOT NULL,Close DOUBLE NOT NULL,Average DOUBLE NOT NULL,Volume BIGINT NOT NULL,OpenInterest BIGINT NOT NULL,Buyers BIGINT NOT NULL,Sellers BIGINT NOT NULL,Expiry VARCHAR(30) NOT NULL";
                        string fields = "id INT NOT NULL AUTO_INCREMENT, LastTradeTime DATETIME NOT NULL, Symbol VARCHAR(50) NOT NULL, Bid DOUBLE NOT NULL,Ask DOUBLE NOT NULL,LTP DOUBLE NOT NULL,ChangeInValues DOUBLE NOT NULL,ChangeInPercentage DOUBLE NOT NULL,Open DOUBLE NOT NULL,High DOUBLE NOT NULL,Low DOUBLE NOT NULL,Close DOUBLE NOT NULL,Average DOUBLE NOT NULL,Volume BIGINT NOT NULL,OpenInterest BIGINT NOT NULL,Buyers BIGINT NOT NULL,Sellers BIGINT NOT NULL,Expiry VARCHAR(30) NOT NULL,BidChange INT NOT NULL,AskChange INT NOT NULL,LTPChange INT NOT NULL,yOpen DOUBLE NOT NULL,yHigh DOUBLE NOT NULL,yLow DOUBLE NOT NULL,yClose DOUBLE NOT NULL";

                        //if (m_Mysql.IsTableExists(tableName, databaseName) && !tablesRenamed)
                        if (m_Mysql.IsTableExists(tableName, databaseName))
                        {
                            //m_Mysql.CleanDatabase(tableName, databaseName);
                            //m_Mysql.CreateTable(tableName, fields, "id");
                            //m_Mysql.DeleteOldRows(tableName);
                            SymbolListExistingRecord.Add(tableName, m_Mysql.GetRowsCount(tableName));
                            SymbolListYestRecord.Add(tableName, m_Mysql.GetYestValues(tableName));
                        }
                        if (!m_Mysql.IsTableExists(tableName, databaseName))
                        {
                            m_Mysql.CreateTable(tableName, fields, "id");
                            SymbolListExistingRecord.Add(tableName, 0);
                            SymbolListYestRecord.Add(tableName, "0,0,0,0");
                        }

                    }
                    writeIntoMysqlFile();
                }
                //tablesCreated = true;

                //IRDS::04-August-2020::Sandip::moved thread creation code after mysql code.
                //if (m_addInTableThread == null)
                //{
                //    Trace.WriteLine("Started MCXProcessTickDataToDatabase ");
                //    m_addInTableThread = new Thread(() => MCXProcessTickDataToDatabase());
                //    m_addInTableThread.Start();
                //}

                if (m_createNewThread == null)
                {
                    m_createNewThread = new Thread(() => CreateNewThreadForEachSymbol());
                    m_createNewThread.Start();
                }

                //get token from symbol name
                string instrumentToken = "";
                UInt32[] instrumentList = new UInt32[listOfNSESymbols.Count() + listOfNFOSymbols.Count() + listOfMCXSymbols.Count()];
                if (listOfNSESymbols.Count > 0)
                {
                    for (int i = 0; i < listOfNSESymbols.Count(); i++)
                    {
                        instrumentToken = getInstrumentToken(listOfNSESymbols[i]);
                        if (instrumentToken != "")
                        {
                            instrumentList[i] = Convert.ToUInt32(instrumentToken);
                            if (!SymbolList.ContainsKey(instrumentToken))
                            {
                                SymbolList.Add(instrumentToken, listOfNSESymbols[i]);
                                SymbolListWithFutureName.Add(instrumentToken, listOfNSESymbols[i]);
                                m_NSESymbolsInstruements.Add(listOfNSESymbols[i]);
                            }
                        }
                        else
                        {
                            logger.LogMessage(listOfNSESymbols[i] + " this ignore because not present in db", MessageType.Informational);
                        }
                    }
                }
                if (listOfNFOSymbols.Count > 0)
                {
                    //string concatString = FetchTableName();
                    for (int i = listOfNSESymbols.Count(), j = 0; i < listOfNFOSymbols.Count() + listOfNSESymbols.Count() && j < listOfNFOSymbols.Count(); i++, j++)
                    {
                        string name = listOfNFOSymbols[j];
                        string tableName = name.Substring(name.Length - 8, 8);
                        instrumentToken = getInstrumentToken(listOfNFOSymbols[j], tableName);
                        if (instrumentToken != "")
                        {
                            instrumentList[i] = Convert.ToUInt32(instrumentToken);
                            if (!SymbolList.ContainsKey(instrumentToken))
                            {
                                SymbolList.Add(instrumentToken, name);
                                SymbolListWithFutureName.Add(instrumentToken, listOfNFOSymbols[j]);
                                m_NFOSymbolsInstruements.Add(listOfNFOSymbols[j]);
                            }
                        }
                        else
                        {
                            logger.LogMessage(listOfNFOSymbols[j] + " this ignore because not present in db", MessageType.Informational);
                        }
                    }
                }

                if (listOfMCXSymbols.Count > 0)
                {
                    //string concatString = FetchTableName();
                    for (int i = (listOfNFOSymbols.Count() + listOfNSESymbols.Count()), j = 0; i < listOfNFOSymbols.Count() + listOfNSESymbols.Count() + listOfMCXSymbols.Count() && j < listOfMCXSymbols.Count(); i++, j++)
                    {
                        string name = listOfMCXSymbols[j];
                        string tableName = name.Substring(name.Length - 8, 8);
                        instrumentToken = getInstrumentToken(listOfMCXSymbols[j], tableName);
                        if (instrumentToken != "")
                        {
                            instrumentList[i] = Convert.ToUInt32(instrumentToken);
                            if (!SymbolList.ContainsKey(instrumentToken))
                            {
                                SymbolList.Add(instrumentToken, name);
                                SymbolListWithFutureName.Add(instrumentToken, listOfMCXSymbols[j]);
                                m_MCXSymbolsInstruements.Add(listOfMCXSymbols[j]);
                            }
                        }
                        else
                        {
                            logger.LogMessage(listOfMCXSymbols[j] + " this ignore because not present in db", MessageType.Informational);
                        }
                    }
                }
                Console.WriteLine("Inside initTicker");
                ticker = new Ticker(apiKey, accessToken);

                ticker.OnTick += OnTick;
                ticker.OnReconnect += OnReconnect;
                ticker.OnNoReconnect += OnNoReconnect;
                ticker.OnError += OnError;
                ticker.OnClose += OnClose;
                ticker.OnConnect += OnConnect;
                ticker.OnOrderUpdate += OnOrderUpdate;

                ticker.EnableReconnect(Interval: 5, Retries: 50);
                ticker.Connect();
                //dynamic list
                ticker.Subscribe(Tokens: instrumentList);
                ticker.SetMode(Tokens: instrumentList, Mode: Constants.MODE_FULL);

            }
            catch (Exception e)
            {
                logger.LogMessage("MCXinitTicker : Exception Error Message ="+e.Message, MessageType.Exception);
            }

            //IRDS::03-Jul-2020::Jyoti::added for cleaning and creating tables
            try
            {
                

            }
            catch (Exception e)
            {
                logger.LogMessage("MCXinitTicker : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        public void writeIntoMysqlFile()
        {
            string currentDate = DateTime.Now.ToString("M-d-yyyy");
            var path_File = Directory.GetCurrentDirectory();
            string FilePath = path_File + @"\Configuration\" + "mysql.txt";
            string line = currentDate;
            using (StreamWriter streamWriter = new StreamWriter(FilePath, false))
            {
                streamWriter.WriteLine(line);
            }
        }

        public bool checkTablesRenamed()
        {
            var path_File = Directory.GetCurrentDirectory();
            string FilePath = path_File + @"\Configuration\" + "mysql.txt";
            if (!File.Exists(FilePath))
            {
                Console.WriteLine("credentials.txt file does not exist");
                return false;
            }
            else
            {
                string lastLine = System.IO.File.ReadLines(FilePath).Last();
                string[] splittedString = lastLine.Split(' ');
                string currentDate = DateTime.Now.ToString("M-d-yyyy");
                if (splittedString[0] == currentDate)
                {
                    return true;
                }
            }
            return false;
        }

        public void updateSymbolDetails(string symbolWithDate)
        {
            try
            {
                //IRDS::04-Aug-2020::Jyoti::Change ini expiry date
                string symbolName = symbolWithDate.Split(',')[0];
                string table_name = "";
                if (symbolName.Split('.')[1] != Constants.EXCHANGE_NFO)
                {
                    table_name = symbolName.Split('.')[0] + "_" + symbolName.Split('.')[1];
                }
                else
                {
                    table_name = symbolName.Split('.')[0] + "_FUT";
                }
                if (symbolName.Split('.')[1] == Constants.EXCHANGE_MCX)
                {
                    string name = symbolName.Split('.')[0];
                    //table_name = name.Remove(name.Length - 8, 8) + "_MCX";
                    table_name = name + "_MCX";
                }
                string fullSymbolName = "";
                string expDate = "";
                if (symbolName.Split('.')[1] != Constants.EXCHANGE_NSE)
                {
                    //string concatString = FetchTableName();
                    expDate = symbolWithDate.Split(',')[1];
                    string year = expDate.Substring(expDate.Length - 2);
                    string month = expDate.Substring(expDate.Length - 5, 3).ToUpper();
                    string concatString = year + month + "FUT";
                    fullSymbolName = symbolName.Split('.')[0] + concatString;
                }
                else
                {
                    fullSymbolName = symbolName.Split('.')[0];

                }
                //table_name = table_name.Replace("-", "");
                //table_name = table_name.Replace("&", "");
                bool validEntry = false;
                if (!SymbolListWithExchanges.ContainsValue(table_name))//sanika::20-JUl-2020::Added check
                {
                    SymbolListWithExchanges.Add(fullSymbolName, table_name);
                    validEntry = true;
                }
                else
                {
                    table_name = symbolName.Split('.')[0] + "_1_MCX";
                    if (!SymbolListWithExchanges.ContainsValue(table_name))
                    {
                        SymbolListWithExchanges.Add(fullSymbolName, table_name);
                        validEntry = true;
                    }
                }
                if (validEntry)
                {
                    if (!SymbolListWithExpDate.ContainsKey(fullSymbolName))//sanika::20-JUl-2020::Added check
                    {
                        SymbolListWithExpDate.Add(fullSymbolName, expDate);
                    }
                    string symbol = symbolName.Split('.')[0];
                    if (symbolName.Split('.')[1] == Constants.EXCHANGE_MCX)
                    {
                        string name = symbolName.Split('.')[0];
                        //symbol = name.Remove(name.Length - 8, 8);
                        symbol = name;
                    }
                    m_Mysql.InsertorUpdateSymbol("SymbolDetails", symbol, table_name, symbolName.Split('.')[1], m_SortCounter);
                    m_SortCounter++;
                }
            }
            catch(Exception e)
            {
                logger.LogMessage("updateSymbolDetails : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        //IRDSPM::Pratiksha::-01-07-2020::For fetching symbol names from db --end


        //IRDS::Sanika::27-Aug-2020::Connect account for historical data
        public bool ConnectAccount()
        {
            bool isConnect = false;
            try
            {
                WebDriver webDriver = new WebDriver(false, usernameDownloadHistoricalData, passwordDownloadHistoricalData, PinDownloadHistoricalData,logger);
                webDriver.getChromeDriver(login, apiKeyDownloadHistoricalData);

                string reqToken = webDriver.currentUrl;
                Uri tmp = new Uri(reqToken);
                NameValueCollection Parms = HttpUtility.ParseQueryString(tmp.Query);

                string token = "";
                foreach (string x in Parms.AllKeys)
                {
                    if (x.Contains("request_token"))
                    {
                        token = Parms[x];
                    }
                }
                User user = GenerateSessionForDifferentAccount(token, apiSecretDownloadHistoricalData);

                m_accessTokenDownloadHistoricalData = user.AccessToken;
                m_MyPublicTokenDownloadHistoricalData = user.PublicToken;
                writeHistoricalDataCredentialIntoFile(m_accessTokenDownloadHistoricalData, m_MyPublicTokenDownloadHistoricalData, usernameDownloadHistoricalData);
                isConnect = true;
            }
            catch(Exception e)
            {
                logger.LogMessage("ConnectAccount : Exception Error Message = " + e.Message,MessageType.Exception);
                isConnect = false;
            }
            return isConnect;
        }

        public void writeHistoricalDataCredentialIntoFile(string accessToken, string MyPublicToken,string userName)
        {
            string currentDate = DateTime.Now.ToString("M-d-yyyy");
            var path_File = Directory.GetCurrentDirectory();
            string FilePath = path_File + @"\Configuration\" + "credentialsHistoricalData.txt";
            string line = currentDate + " " + userName+ " " + accessToken + " " + MyPublicToken;
            using (StreamWriter streamWriter = new StreamWriter(FilePath, false))
            {
                streamWriter.WriteLine(line);
            }
        }

        //IRDSPM::Pratiksha::12-09-2020::For searchlist on buy form
        public List<string> fetchTablenamesForsearchboxList()
        {
            List<string> MasterSymbolList = new List<string>();
            List<string> tableNames = new List<string>();
            string concatString = "";
            string year = DateTime.Now.ToString("yy");
            string nextYear = DateTime.Now.AddYears(1).ToString("yy");
            string currentMonth = DateTime.Now.ToString("MMM");
            string tablename = "";
            try
            {
                if (Sql_conTableName == null)
                {
                    var path_DB = Directory.GetCurrentDirectory();
                    string directoryName = path_DB + "\\" + "Database";
                    string DBFilePath = directoryName + "\\" + "kite.db";
                    string connectionString = String.Format("Data Source={0};Version=3;New=False;Compress=True;", DBFilePath);
                    Sql_conTableName = new SQLiteConnection(connectionString);
                    Sql_conTableName.Open();
                }
                SQLiteTransaction TrTableName;
                SQLiteCommand Sql_cmdTableName;
                SQLiteDataReader Sql_readTableName;
                string query = "SELECT name FROM sqlite_master WHERE type = 'table' AND name NOT LIKE 'sqlite_%'";
                Sql_cmdTableName = new SQLiteCommand(query, Sql_conTableName);
                Sql_readTableName = Sql_cmdTableName.ExecuteReader();
                if (Sql_readTableName != null && Sql_readTableName.HasRows)
                {
                    while (Sql_readTableName.Read())
                    {
                        tablename = (string)Sql_readTableName["name"];
                        tableNames.Add(tablename);
                    }
                }
                Console.WriteLine(tableNames.Count());
                string currMonthinStr = DateTime.Now.ToString("MMM");
                int currMonthinInt = DateTime.ParseExact(currMonthinStr, "MMM", CultureInfo.CurrentCulture).Month;
                for (int i = 0; i < tableNames.Count(); i++)
                {
                    {
                        if (tableNames[i].Substring(0, 2).Any(char.IsDigit))
                        {
                            string currMonth = tableNames[i].Substring(2, 3);
                            int month = DateTime.ParseExact(currMonth, "MMM", CultureInfo.CurrentCulture).Month;
                            if (currMonthinInt <= month)
                            {
                                int yearint = Convert.ToInt32(tableNames[i].Substring(0, 2));
                                if (yearint >= Convert.ToInt32(year))
                                {
                                    concatString = tableNames[i];
                                    MasterSymbolList.Add(concatString);
                                }
                            }
                            else
                            {
                                int yearint = Convert.ToInt32(tableNames[i].Substring(0, 2));
                                if (yearint > Convert.ToInt32(year))
                                {
                                    concatString = tableNames[i];
                                    MasterSymbolList.Add(concatString);
                                }
                            }
                        }
                        else if ((tableNames[i].Substring(0, 3).Any(char.IsLetter)) && (tableNames[i].Substring(3, 2).Any(char.IsDigit)))
                        {
                            string currMonth = tableNames[i].Substring(0, 3);
                            int month = DateTime.ParseExact(currMonth, "MMM", CultureInfo.CurrentCulture).Month;
                            if (currMonthinInt <= month)
                            {
                                int yearint = Convert.ToInt32(tableNames[i].Substring(3, 2));
                                if (yearint >= Convert.ToInt32(year))
                                {
                                    concatString = tableNames[i];
                                    MasterSymbolList.Add(concatString);
                                }
                            }
                        }
                        else
                        {
                            MasterSymbolList.Add(tableNames[i].ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return MasterSymbolList;
        }

        public bool GetChangeInPercent(string TradingSymbol,out double highPercent, out double lowPercent)
        {
            // File.AppendAllText("time.txt", "**** "+ TradingSymbol +" GetChangeInPercent - getting change in percent for end time "+endTime+"\n");
            highPercent = 0;
            lowPercent = 0;
           // if (m_useSqlite)
            {
              //  if (m_isConnectedToSqlite)
                {
                    try
                    {
                       /// string tableName = TradingSymbol;                       
                        //lock (databaselock)
                        {
                            double dHigh = 0, dLow = 0;
                            double close = GetYesterdayClose(TradingSymbol);
                            dHigh = Convert.ToDouble(m_TickData.GetHighPrice(TradingSymbol));
                            dLow = Convert.ToDouble(m_TickData.GetLowPrice(TradingSymbol));
                            //sqliteConnectZerodha.GetMaxHighLow(tableName, startTime,endTime, out dHigh, out dLow);
                            logger.LogMessage("GetChangeInPercent : TradingSymbol = "+ TradingSymbol  + " dHigh = "+ dHigh + " dLow = "+ dLow, MessageType.Informational);
                            highPercent = ((dHigh - close) * 100) / dHigh;
                            lowPercent = ((close - dLow) * 100) / dLow;
                            logger.LogMessage("GetChangeInPercent :  highPercent = " + highPercent + " lowPercent = " + lowPercent, MessageType.Informational);
                            // "GetChangeInPercent :
                        }
                        return true;
                    }
                    catch (Exception e)
                    {                            
                        logger.LogMessage("GetChangeInPercent : Exception Error Message = " + e.Message, MessageType.Exception);
                    }
                }
            }
            return false;
        }

        //IRDS::sanika::30-oct-2020::Added to fetch yesterday close from db
        public double GetYesterdayClose(string TradingSymbol)
        {            
            double close = 0;
            if (m_useSqlite)
            {
                if (m_isConnectedToSqlite)
                {
                    try
                    {
                        string tableName = TradingSymbol;
                        close = Convert.ToDouble(m_TickData.GetYesterdayClose(TradingSymbol));
                        logger.LogMessage("GetClose : TradingSymbol = " + TradingSymbol + " GetYesterdayClose = " + close, MessageType.Informational);
                        if (close == 0)
                        {
                            lock (databaselock)
                            {
                                close = sqliteConnectZerodha.GetYesterdayClose(tableName);
                                logger.LogMessage("GetClose : TradingSymbol = " + TradingSymbol + " close = " + close, MessageType.Informational);
                                if(close != 0)
                                {
                                    m_TickData.AddYesterdayClose(close, TradingSymbol);
                                }
                                else
                                {
                                    logger.LogMessage("GetClose : Close retrive as 0 from db TradingSymbol = " + TradingSymbol + " close = " + close, MessageType.Informational);
                                    close = Convert.ToDouble(m_TickData.GetClosePrice(TradingSymbol));
                                }
                            }
                        }
                        return close;
                    }
                    catch (Exception e)
                    {
                        logger.LogMessage("GetClose : Exception Error Message = " + e.Message, MessageType.Exception);
                    }
                }
            }
            return close;
        }

        //sanika::5-nov-2020::Added for rajesh sir's startegy 
        public bool GetHighLowFromDB(string symbol, string StartTime,string EndTime, out double dHigh, out double dLow)
        {
            bool isValueOut = false;
            dHigh = 0;
            dLow = 0;
            if (m_useSqlite)
            {
                if (m_isConnectedToSqlite)
                {
                    try
                    {
                        string tableName = symbol;
                        string currentdate = DateTime.Now.ToString("dd-MM-yyyy");                       
                        double start = DateTime.ParseExact(currentdate+" "+ StartTime, "dd-MM-yyyy HH:mm:ss", null).ToOADate();
                        double end = DateTime.ParseExact(currentdate + " " + EndTime, "dd-MM-yyyy HH:mm:ss", null).ToOADate();
                        lock (databaselock)
                        {
                            //sanika::14-sep-2020::wrote seperate class for sqlite db
                            if (sqliteConnectZerodha.GetMaxHighLow(symbol, start, end, out dHigh, out dLow))
                            {
                                isValueOut = true;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        isValueOut = false;
                        logger.LogMessage("GetHighLow : Exception Error Message = " + e.Message, MessageType.Exception);
                    }
                }
            }
            return isValueOut;
        }

        //sanika::15-Dec-2020::Added to restart exe
        public void RestartExe()
        {
            try
            {
                //current time>8.30 and current time <8.45
                DateTime currentTime = DateTime.ParseExact(DateTime.Now.ToString("HH:mm:ss"), "HH:mm:ss", null);
                if (currentTime.TimeOfDay > m_ExeRestartStartTime.TimeOfDay && currentTime.TimeOfDay < m_ExeRestartEndTime.TimeOfDay)
                {                    
                    if (!CheckTodaysLogin())
                    {
                        logger.LogMessage("Condition matched for time currentTime " + currentTime + " m_ExeRestartStartTime " + m_ExeRestartStartTime + " m_ExeRestartEndTime " + m_ExeRestartEndTime, MessageType.Informational);
                        logger.LogMessage("No token present in file for todays date. Going to restart exe" , MessageType.Informational);
                        // Wait for the process to terminate
                        Process process = null;
                        string applicationName = Process.GetCurrentProcess().ProcessName;
                        if (applicationName.Contains('.'))
                        {
                            applicationName = applicationName.Split('.')[0];
                        }
                        try
                        {

                            int nProcessID = Process.GetCurrentProcess().Id;
                            process = Process.GetProcessById(nProcessID);
                            Process.Start(applicationName, "AUTORESTART");
                            process.Kill();
                        }
                        catch (ArgumentException ex)
                        {
                            // ArgumentException to indicate that the 
                            // process doesn't exist?   LAME!!
                        }
                    }

                }
            }
            catch (Exception er)
            {

            }            
        }

        public bool CheckTodaysLogin()
        {
            try
            {
                var path_File = Directory.GetCurrentDirectory();
                string FilePath = path_File + @"\Configuration\" + "credentials_" + username + ".txt";
                if (!File.Exists(FilePath))
                {
                    logger.LogMessage("CheckTodaysLogin : credentials.txt file does not exist", MessageType.Informational);
                    return false;
                }
                else
                {
                    string lastLine = System.IO.File.ReadLines(FilePath).Last();
                    string[] splittedString = lastLine.Split(' ');
                    string currentDate = DateTime.Now.ToString("M-d-yyyy");
                    if (splittedString[0] == currentDate && splittedString[1] == username)
                    {                        
                        return true;                    
                    }
                }
            }
            catch (Exception e)
            {
                logger.LogMessage("CheckTodaysLogin : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return false;
        }

        public void GetFirstTwoTick(string TradingSymbol, string FirstTickTime, string SecondTickTime, out double firstCandleHigh, out double firstCandleLow, out double secondCandleHigh, out double secondCandleLow)
        {
            firstCandleHigh = firstCandleLow = secondCandleHigh = secondCandleLow = 0;
            if (m_useSqlite)
            {
                if (m_isConnectedToSqlite)
                {
                    try
                    {
                        string tableName = TradingSymbol;
                        string currentdate = DateTime.Now.ToString("dd-MM-yyyy");
                        double firstTick = DateTime.ParseExact(currentdate + " " + FirstTickTime, "dd-MM-yyyy HH:mm:ss", null).AddMinutes(-1).ToOADate();
                        double secondTick = DateTime.ParseExact(currentdate + " " + SecondTickTime, "dd-MM-yyyy HH:mm:ss", null).AddMinutes(1).ToOADate();
                        lock (databaselock)
                        {
                            //sanika::14-sep-2020::wrote seperate class for sqlite db
                            if (sqliteConnectZerodha.GetTwoTick(tableName, firstTick, secondTick, out firstCandleHigh, out firstCandleLow,out secondCandleHigh,out secondCandleLow))
                            {
                              
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        //isValueOut = false;
                        logger.LogMessage("GetHighLow : Exception Error Message = " + e.Message, MessageType.Exception);
                    }
                }
            }
        }

        private void CreateNewThreadForEachSymbol()
        {
            try
            {
                while (true && m_StopThread == false)
                {
                    if (m_QueueOfAllSymbols.Count != m_ThreadsOfAllSymbols.Count)
                    {
                        Dictionary<string, ConcurrentQueue<Tick>> temp = new Dictionary<string, ConcurrentQueue<Tick>>(m_QueueOfAllSymbols);
                        foreach (var symbol in temp)
                        {
                            if (!m_ThreadsOfAllSymbols.ContainsKey(symbol.Key) && (!m_bTickMCXDataDownloader))
                            {
                                Thread _newThread = new Thread(() => ProcessTickDataToDatabaseNew(symbol.Key));
                                _newThread.Start();
                                Trace.WriteLine("##$$$TICKStatus : thread no " + _newThread.Name +" "+ _newThread.GetHashCode());
                                m_ThreadsOfAllSymbols.Add(symbol.Key, _newThread);
                            }
                            else if (!m_ThreadsOfAllSymbols.ContainsKey(symbol.Key))
                            {
                                Thread _newThread = new Thread(() => MCXProcessTickDataToDatabase(symbol.Key));
                                _newThread.Start();
                                Trace.WriteLine("##$$$TICKStatus : thread no " + _newThread.Name + " " + _newThread.GetHashCode());
                                m_ThreadsOfAllSymbols.Add(symbol.Key, _newThread);
                            }
                        }
                    }
                    Thread.Sleep(100);
                }
            }
            catch(Exception e)
            {

            }
        }
    }    
}

