﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IRDSAlgoOMS.Zerodha
{
    class HistoricalDataDownloading
    {
        MySQLConnectZerodha m_MySQLConnectZerodha = null;
        bool m_IsConnected = false;
        private static readonly object sync = new Object();
        Dictionary<string,int> DatabaseCreatedMysql = new Dictionary<string, int>();
        string path = Directory.GetCurrentDirectory() + "\\";
        string m_dbName = "";
        string m_FutureName = "";
        public void DownloadData(List<string>Symbols,string interval,int days,string serverName,string userName,string password,string db,Kite kite)
        {
            try
            {
                m_FutureName = kite.FetchTableName();
                //to connect db
                if (m_IsConnected == false)
                {
                    ConnectToDB(serverName, userName, password, db);
                }
                if(m_IsConnected)
                    WriteUniquelogs("DownloadData", "DownloadData : Connected to db "+ db, MessageType.Informational);

                m_dbName = db;
                foreach (var sym in Symbols)
                {
                    
                   // WriteUniquelogs("DownloadData", "DownloadData : Last date "+ lastDate +" of "+ sym +" table ", MessageType.Informational);
                    string dateTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm") + ":00";
                    DateTime todateTime = DateTime.ParseExact(dateTime, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture).AddMinutes(1);
                    DateTime startdateTime = todateTime.AddDays(-days);
                    todateTime = todateTime.AddDays(1);
                    // string StartDate = lastDate;
                    string StartDate = startdateTime.ToString("yyyy-MM-dd HH:mm:ss");
                    string EndDate = todateTime.ToString("yyyy-MM-dd HH:mm:ss");
                    string[] strAarray = sym.Split('.');
                    string TradingSymbol = strAarray[0];
                    if (strAarray[1] == Constants.EXCHANGE_NFO || strAarray[1] == Constants.EXCHANGE_CDS)
                    {
                        //IRDS::05-Sep-2020::Jyoti::Commented for saving original symbol
                        if (!TradingSymbol.Contains("-I"))
                            TradingSymbol +=  m_FutureName;
                    }
                    string token = kite.GetToken(sym);
                    
                    
                    GetHistoricalData(TradingSymbol, token, Convert.ToInt32(interval),"minute", StartDate, EndDate, kite);
                }
            }
            catch(Exception e)
            {
                WriteUniquelogs("DownloadData", "DownloadData : Exception Error Message = " + e.Message,MessageType.Exception);
            }
        }

        //to connect db
        public void ConnectToDB(string serverName, string userName, string password, string db)
        {
            try
            {
                if (m_MySQLConnectZerodha == null)
                {
                    m_MySQLConnectZerodha = new MySQLConnectZerodha();
                }
                m_IsConnected = m_MySQLConnectZerodha.Connect(null, serverName, userName, password, db);
            }
            catch(Exception e)
            {
                WriteUniquelogs("DownloadData", "ConnectToDB : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        public string GetLastDate(string tableName)
        {
            string lastDate = DateTime.Now.ToString();
            try
            {
                if(m_IsConnected)
                {
                    lastDate = m_MySQLConnectZerodha.GetLastRecordFromTable(tableName);
                }
            }
            catch(Exception e)
            {
                WriteUniquelogs("DownloadData", "GetLastDate : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return lastDate;
        }

        public string GetInterval(int m_Interval)
        {
            //minute, 3minute, 5minute, 10minute, 15minute, 30minute, 60minute
            string interval = "minute";
            if (m_Interval == 1)
                interval = "minute";
            else if (m_Interval == 3)
                interval = "3minute";
            else if (m_Interval == 5)
                interval = "5minute";
            else if (m_Interval == 10)
                interval = "10minute";
            else if (m_Interval == 15)
                interval = "15minute";
            else if (m_Interval == 30)
                interval = "30minute";
            else if (m_Interval == 60)
                interval = "60minute";
            return interval;
        }

        private bool GetHistoricalData(string strSymbolName, string token, int intBarIntervalQuantity, string strBarIntervalUnit, string strStartDate, string strEndDate, Kite kite)
        {
            Boolean bolGetHistoricalBarBackData = false;
            try
            {
                WriteUniquelogs("DownloadData", "GetHistoricalData : SymbolName: " + strSymbolName + " , BarIntervalQuantity: " + intBarIntervalQuantity.ToString() + " , BarIntervalUnit: " + strBarIntervalUnit + " , StartDate: " + strStartDate + " , EndDate: " + strEndDate, MessageType.Informational);

                bool ContinueExecution = false;
                string limit = "";
                string EndDate = Convert.ToDateTime(strEndDate).ToString();
                limit = GetLimit(intBarIntervalQuantity.ToString()).ToString();
                string interval = GetInterval(intBarIntervalQuantity);
                System.Globalization.CultureInfo tempCultureInfo = System.Globalization.CultureInfo.InvariantCulture;
                string CurrentDate = DateTime.Now.AddMinutes(1).ToString("yyyy-MM-dd", tempCultureInfo);
                DateTime current = DateTime.ParseExact(CurrentDate, "yyyy-MM-dd", null);
                while (!ContinueExecution)
                {
                    lock (sync)
                    {
                        strEndDate = Convert.ToDateTime(strStartDate).AddDays(Convert.ToInt32(limit)).ToString();
                        string NewEndDate = strEndDate;
                        string NewStartDate = strStartDate;

                        if (Convert.ToDateTime(NewStartDate) > current)
                        {
                            break;
                        }
                        else
                        {
                            if (Convert.ToDateTime(NewEndDate) > Convert.ToDateTime(EndDate))
                            {
                                strEndDate = EndDate;
                                ContinueExecution = true;
                            }
                        }
                        List<Historical> historicalList = new List<Historical>();
                        historicalList = kite.GetHistoricalData(token, Convert.ToDateTime(strStartDate), Convert.ToDateTime(strEndDate), interval, false);
                        SaveBulkDataIntoDB(strSymbolName, historicalList);
                        if (File.Exists(path + strSymbolName + ".csv"))
                        {
                            File.Delete(path + strSymbolName + ".csv");
                        }
                        if (strBarIntervalUnit == "minute")
                        {
                            strStartDate = Convert.ToDateTime(strEndDate).AddMinutes(intBarIntervalQuantity).ToString();
                        }
                        else
                        {
                            strStartDate = strEndDate;
                        }
                    }
                }
                WriteUniquelogs("DownloadData", "GetHistoricalData : Data Downloaded", MessageType.Informational);
            }
            catch (Exception ex)
            {
                WriteUniquelogs("DownloadData", "GetHistoricalData : Exception Error Message = " + ex.Message, MessageType.Informational);
            }
            return bolGetHistoricalBarBackData;
        }

        private void SaveBulkDataIntoDB(string SymbolName, List<Historical> HistoricalData)
        {
            string insertData = "";
            string newfilePath = "";
            string newLine = "";
            try
            {
                newfilePath = path + SymbolName + ".csv";
                newLine = string.Format("{0},{1},{2},{3},{4},{5},{6}", "Symbol", "LastTradeTime", "Open", "High", "Low", "Close", "Volume");
                CheckMysqlTableExistOrNot(SymbolName, newfilePath); 
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(newfilePath))
                {
                    file.WriteLine(newLine);
                    foreach (var item in HistoricalData)
                    {
                        int year = item.TimeStamp.Year;
                        int month = item.TimeStamp.Month;
                        int day = item.TimeStamp.Day;
                        int hr = item.TimeStamp.Hour;
                        int min = item.TimeStamp.Minute;
                        int sec = item.TimeStamp.Second;
                        int milli = item.TimeStamp.Millisecond;

                        //DateTime dt = new DateTime(year, month, day, hr, min, sec, milli);
                        //String.Format("{0:y yy yyy yyyy}", dt);
                        //CultureInfo en = new CultureInfo("en-US");
                        //Thread.CurrentThread.CurrentCulture = en;
                        string timestamp = item.TimeStamp.ToString("yyyy-MM-dd HH:mm:ss").TrimStart();
                        //DateTime dtimeStamp = DateTime.ParseExact(timestamp, "yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
                        //DateTime dateTime = dtimeStamp;
                        double Open = Convert.ToDouble(item.Open);
                        double High = Convert.ToDouble(item.High);
                        double Low = Convert.ToDouble(item.Low);
                        double Close = Convert.ToDouble(item.Close);
                        double Volume = Convert.ToDouble(item.Volume);
                       
                        newLine = SymbolName+"," + timestamp + "," + Open + "," + High + "," + Low + "," + Close + "," + Volume;
                        file.WriteLine(newLine);
                        //insertData = insertData + "('" + TimeStamp + "','" + Date + "','" + Time + "','" + Open + "','" + High + "','" + Low + "','" + Close + "','" + Volume + "','" + OI + "'),";
                    }
                }
                //sanika::5-Apr-2021::As per sir's suggestion future table as e.g. NIFTY-I
                if (SymbolName.Contains(m_FutureName))
                    SymbolName = SymbolName.Replace(m_FutureName, "-I");
                m_MySQLConnectZerodha.BulkDataImport(newfilePath, SymbolName);
            }
            catch (Exception ex)
            {
                WriteUniquelogs("DownloadData", "SaveBulkDataIntoDB : Exception Error Message = " + ex.Message.ToString(),MessageType.Exception);
            }
        }

        public void CheckMysqlTableExistOrNot(string TradingSymbol, string newfilePath)
        {
            try
            {
                //sanika::5-Apr-2021::As per sir's suggestion future table as e.g. NIFTY-I
                if (TradingSymbol.Contains(m_FutureName))
                    TradingSymbol = TradingSymbol.Replace(m_FutureName, "-I");
                if (DatabaseCreatedMysql.ContainsKey(TradingSymbol) == false)
                {
                    if (m_MySQLConnectZerodha.IsTableExists(TradingSymbol, m_dbName))
                    {
                        //m_Mysql.CleanDatabase(TradingSymbol);
                    }
                    else
                    {
                        m_MySQLConnectZerodha.CreateTable(TradingSymbol, "Symbol varchar(50) NOT NULL, LastTradeTime datetime NOT NULL, Open double NOT NULL, High double NOT NULL,Low double NOT NULL,Close double NOT NULL,Volume BIGINT NOT NULL", "LastTradeTime");
                        
                        WriteUniquelogs("DownloadData", "CheckMysqlTableExistOrNot : Table is created for " + TradingSymbol, MessageType.Informational);
                    }
                }
                else
                {
                    DatabaseCreatedMysql[TradingSymbol] = 1;
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs("DownloadData", "CheckMysqlTableExistOrNot : Exception Error Message " + e.Message, MessageType.Exception);
            }
        }

        public int GetLimit(string interval)
        {
            int limit = 60;
            try
            {
                if (interval == "1")
                    limit = 60;
                else if (interval == "3")
                    limit = 100;
                else if (interval == "5")
                    limit = 100;
                else if (interval == "10")
                    limit = 100;
                else if (interval == "15")
                    limit = 200;
                else if (interval == "30")
                    limit = 200;
                else if (interval == "60")
                    limit = 400;
                else if (interval == "Daily")
                    limit = 2000;
                else if (interval == "Weekly")
                    limit = 2000;
                WriteUniquelogs("DownloadData", "GetLimit : interval "+ interval + " limit "+ limit, MessageType.Informational);
            }
            catch(Exception e)
            {
                WriteUniquelogs("DownloadData", "GetLimit : Exception Error Message = " + e.Message, MessageType.Exception);
            }

            return limit;
        }

        public void WriteUniquelogs(string symbol, string message, MessageType msgType)
        {
            try
            {
                SymbolLogs log = new SymbolLogs();
                log.createFile(symbol);
                log.LogMessage(message, MessageType.Informational);
                log = null;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : " + e.Message);
                Trace.WriteLine("Exception : " + e.Message);
            }
        }
    }
}
