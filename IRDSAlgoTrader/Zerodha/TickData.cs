﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRDSAlgoOMS.Zerodha
{
    public class TickData
    {
        private Dictionary<string, TickData> symbolWiseTickInfo;
        public decimal LTP { get; set; }
        decimal Close { get; set; }
        public decimal BidPrice { get; set; }
        public decimal AskPrice { get; set; }
        decimal Volume { get; set; }
        decimal Open { get; set; }
        decimal High { get; set; }
        decimal Low { get; set; }
        decimal LastBarHigh { get; set; }
        decimal LastBarLow { get; set; }
        bool LastBarReset { get; set; }
        decimal ChangeInPercent { get; set; }
        //sanika::8-oct-2020::Added to add change in absolute
        decimal ChangeInAbsolute { get; set; }

        decimal YesterdayClose { get; set; }
        private static readonly object tickdatalock = new object();//sanika::14-dec-2020::Added for random crash
        public TickData()
        {
            symbolWiseTickInfo = new Dictionary<string, TickData>();
        }
        public void ResetLastBarHighLow(string Symbol)
        {
            //need to reset last bar high and low 
            lock (tickdatalock)
            {
                if (symbolWiseTickInfo.ContainsKey(Symbol))
                {
                    symbolWiseTickInfo[Symbol].LastBarReset = true;
                }
            }
        }
        public void AddOrUpdateTickData(Tick tick,string Symbol)
        {//sanika::14-dec-2020::Added for random crash
            lock (tickdatalock)
            {
                decimal changeInValues = tick.LastPrice - tick.Close;
                decimal basePrice;
                if (tick.LastPrice > tick.Close)
                {
                    basePrice = tick.LastPrice;
                }
                else
                {
                    basePrice = tick.Close;
                }
                decimal changeInPercentage = Math.Round(((changeInValues / basePrice) * 100), 2);

                if (symbolWiseTickInfo.ContainsKey(Symbol))
                {
                    symbolWiseTickInfo[Symbol].LTP = tick.LastPrice;
                    symbolWiseTickInfo[Symbol].Close = tick.Close;
                    symbolWiseTickInfo[Symbol].Volume = tick.Volume;
                    symbolWiseTickInfo[Symbol].Open = tick.Open;
                    symbolWiseTickInfo[Symbol].High = tick.High;
                    symbolWiseTickInfo[Symbol].Low = tick.Low;                   
                    if (symbolWiseTickInfo[Symbol].LastBarReset == true || symbolWiseTickInfo[Symbol].LastBarHigh < tick.LastPrice)
                        symbolWiseTickInfo[Symbol].LastBarHigh = tick.LastPrice;
                    if (symbolWiseTickInfo[Symbol].LastBarReset == true || symbolWiseTickInfo[Symbol].LastBarLow > tick.LastPrice)
                        symbolWiseTickInfo[Symbol].LastBarLow = tick.LastPrice;
                    symbolWiseTickInfo[Symbol].ChangeInPercent = changeInPercentage;
                    //sanika::8-oct-2020::Added to add change in absolute
                    symbolWiseTickInfo[Symbol].ChangeInAbsolute = changeInValues;
                    if (tick.Bids != null)
                    {
                        symbolWiseTickInfo[Symbol].BidPrice = tick.Bids[0].Price;
                    }
                    else
                    {
                        symbolWiseTickInfo[Symbol].BidPrice = 0;
                    }
                    if (tick.Offers != null)
                    {
                        symbolWiseTickInfo[Symbol].AskPrice = tick.Offers[0].Price;
                    }
                    else
                    {
                        symbolWiseTickInfo[Symbol].AskPrice = 0;
                    }
                    symbolWiseTickInfo[Symbol].LastBarReset = false;
                }
                else
                {
                    TickData tickData = new TickData();
                    tickData.LTP = tick.LastPrice;
                    tickData.Close = tick.Close;
                    tickData.Volume = tick.Volume;
                    tickData.Open = tick.Open;
                    tickData.High = tick.High;
                    tickData.Low = tick.Low;
                    tickData.ChangeInPercent = changeInPercentage;
                    tickData.LastBarLow = tick.LastPrice;
                    tickData.LastBarHigh = tick.LastPrice;
                    //sanika::8-oct-2020::Added to add change in absolute
                    tickData.ChangeInAbsolute = changeInValues;
                    if (tick.Bids != null)
                    {
                        tickData.BidPrice = tick.Bids[0].Price;
                    }
                    else
                    {
                        tickData.BidPrice = 0;
                    }
                    if (tick.Offers != null)
                    {
                        tickData.AskPrice = tick.Offers[0].Price;
                    }
                    else
                    {
                        tickData.AskPrice = 0;
                    }                    
                    symbolWiseTickInfo.Add(Symbol, tickData);
                }                  
            }
        }
    
        public decimal GetLastPrice(string Symbol)
        {
            decimal lastPrice = 0;
            if(symbolWiseTickInfo.ContainsKey(Symbol))
            {
                lastPrice = symbolWiseTickInfo[Symbol].LTP;
            }
            return lastPrice;
        }

        public decimal GetClosePrice(string Symbol)
        {
            decimal closePrice = 0;
            if (symbolWiseTickInfo.ContainsKey(Symbol))
            {
                closePrice = symbolWiseTickInfo[Symbol].Close;
            }
            return closePrice;
        }

        public decimal GetBidPrice(string Symbol)
        {
            decimal bidPrice = 0;
            if (symbolWiseTickInfo.ContainsKey(Symbol))
            {
                bidPrice = symbolWiseTickInfo[Symbol].BidPrice;
            }
            return bidPrice;
        }

        public decimal GetAskPrice(string Symbol)
        {
            decimal askPrice = 0;
            if (symbolWiseTickInfo.ContainsKey(Symbol))
            {
                askPrice = symbolWiseTickInfo[Symbol].AskPrice;
            }
            return askPrice;
        }

        public decimal GetVolume(string Symbol)
        {
            decimal volume = 0;
            if (symbolWiseTickInfo.ContainsKey(Symbol))
            {
                volume = symbolWiseTickInfo[Symbol].Volume;
            }
            return volume;
        }

        public decimal GetOpenPrice(string Symbol)
        {
            decimal openPrice = 0;
            if (symbolWiseTickInfo.ContainsKey(Symbol))
            {
                openPrice = symbolWiseTickInfo[Symbol].Open;
            }
            return openPrice;
        }

        public decimal GetHighPrice(string Symbol)
        {
            decimal highPrice = 0;
            if (symbolWiseTickInfo.ContainsKey(Symbol))
            {
                highPrice = symbolWiseTickInfo[Symbol].High;
            }
            return highPrice;
        }

        public decimal GetLowPrice(string Symbol)
        {
            decimal lowPrice = 0;
            if (symbolWiseTickInfo.ContainsKey(Symbol))
            {
                lowPrice = symbolWiseTickInfo[Symbol].Low;
            }
            return lowPrice;
        }

        public decimal GetChangeInPercentage(string Symbol)
        {
            decimal changeInPercentage = 0;
            if (symbolWiseTickInfo.ContainsKey(Symbol))
            {
                changeInPercentage = symbolWiseTickInfo[Symbol].ChangeInPercent;
            }
            return changeInPercentage;
        }

        //sanika::8-oct-2020::Added to get change in absolute
        public decimal GetChangeInAbsolute(string Symbol)
        {
            decimal changeInAbsolute = 0;
            if (symbolWiseTickInfo.ContainsKey(Symbol))
            {
                changeInAbsolute = symbolWiseTickInfo[Symbol].ChangeInAbsolute;
            }
            return changeInAbsolute;
        }

        public decimal GetYesterdayClose(string Symbol)
        {
            decimal yesterdayClose = 0;
            if (symbolWiseTickInfo.ContainsKey(Symbol))
            {
                yesterdayClose = symbolWiseTickInfo[Symbol].YesterdayClose;
            }
            return yesterdayClose;
        }

        public void AddYesterdayClose(double close, string symbol)
        {
            if (symbolWiseTickInfo.ContainsKey(symbol))
            {
                symbolWiseTickInfo[symbol].YesterdayClose = Convert.ToDecimal(close);
            }
            else
            {
                TickData tickData = new TickData();
                tickData.YesterdayClose = Convert.ToDecimal(close);
                symbolWiseTickInfo.Add(symbol, tickData);
            }
        }

        public decimal GetLastBarHigh(string Symbol)
        {
            decimal high = 0;
            if (symbolWiseTickInfo.ContainsKey(Symbol))
            {
                high = symbolWiseTickInfo[Symbol].LastBarHigh;
            }
            return high;
        }

        public decimal GetLastBarLow(string Symbol)
        {
            decimal low = 0;
            if (symbolWiseTickInfo.ContainsKey(Symbol))
            {
                low = symbolWiseTickInfo[Symbol].LastBarLow;
            }
            return low;
        }
    }
}
