﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRDSAlgoOMS
{
    public class TradingSymbolInfo
    {
        public string SymbolWithExchange;
        public int Quantity;
        public decimal Volume;
        public double ProfitPercent;
        public double Stoploss;
        public string TradingSymbol;
        public string Exchange;
        public decimal Difference1;
        public decimal Difference2;
        public int BarCount;

        public string getSymbol()
        {
            return TradingSymbol;
        }

        public int getQuantity()
        {
            return Quantity;
        }

        public decimal getVolume()
        {
            return Volume;
        }

        public double getProfitPercent()
        {
            return ProfitPercent;
        }

        public double getStopLoss()
        {
            return Stoploss;
        }

        public string getExchange()
        {
            return Exchange;
        }

        public decimal getDifference1()
        {
            return Difference1;
        }

        public decimal getDifference2()
        {
            return Difference2;
        }

        public int getBarCount()
        {
            return BarCount;
        }

    }
}
