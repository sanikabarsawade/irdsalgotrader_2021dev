﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRDSAlgoOMS.Zerodha
{
    class CloseOrders
    {        

        Dictionary<string, Order> ClosedOrder = null;

        public CloseOrders()
        {
            ClosedOrder = new Dictionary<string, Order>();
        }

        public void AddCloseOrder(Order order)
        {
            if(!ClosedOrder.ContainsKey(order.OrderId))
            {
                ClosedOrder.Add(order.OrderId, order);
            }
        }

        public bool IsOrderClosed(string OrderId)
        {
            if(ClosedOrder.ContainsKey(OrderId))
            {
                return true;
            }
            return false;
        }
    }
}
