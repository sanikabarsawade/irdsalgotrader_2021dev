﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IRDSAlgoOMS
{
    public class SqliteConnectZerodha
    {
        static string m_PathDB = Directory.GetCurrentDirectory();
        string m_DirectoryName = m_PathDB + "\\" + "Database";
        bool m_ConnectSuccess = false;
        SQLiteConnection m_SqlCon;
        private static readonly object databaselock = new object();

        //testing purpose
        Thread thread = null;
        public bool Connect(string DBName)
        {
            bool isConnected = true;
            try
            {
                lock (databaselock)
                {
                    if (!Directory.Exists(m_DirectoryName))
                        Directory.CreateDirectory(m_DirectoryName);

                    string DBFilePath = m_DirectoryName + "\\" + DBName;
                    if (!File.Exists(DBFilePath))
                    {
                        SQLiteConnection.CreateFile(DBFilePath);
                        Thread.Sleep(100);
                    }
                    string connectionString = String.Format("Data Source={0};Version=3;New=False;Compress=True;PRAGMA locking_mode=EXCLUSIVE;", DBFilePath);
                    m_SqlCon = new SQLiteConnection(connectionString);
                    //open sqlite connection
                    m_SqlCon.Open();
                    isConnected = true;
                }              
            }
            catch (Exception e)
            {
                isConnected = false;
                WriteUniquelogs("SqliteLogs", "Connect : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            m_ConnectSuccess = isConnected;
            return isConnected;
        }

        public void ExecuteNonQueryCommand(string strQuery)
        {
            if (m_ConnectSuccess) //check open connection
            {
                try
                {
                    lock (databaselock)
                    {
                        using (SQLiteTransaction mytransaction = m_SqlCon.BeginTransaction())
                        {
                            using (SQLiteCommand mycommand = new SQLiteCommand(m_SqlCon))
                            {
                                mycommand.CommandText = strQuery;
                                mycommand.ExecuteNonQuery();
                            }
                            mytransaction.Commit();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    WriteUniquelogs("SqliteLogs", "ExecuteNonQueryCommand : Exception Error Message = " + ex.Message, MessageType.Exception);
                }
            }
        }

        public Boolean CreateTable(string tblName, string fieldsList, string key)
        {
            if (m_ConnectSuccess) //check open connection
            {
                try
                {
                    ExecuteNonQueryCommand("CREATE TABLE " + tblName + " (" + fieldsList + ", PRIMARY KEY(" + key + "));");

                    return true;
                }
                catch (Exception ex)
                {
                    WriteUniquelogs("SqliteLogs", "CreateTable : Exception Error Message = " + ex.Message, MessageType.Exception);
                }
            }
            return false;
        }

        public bool IsTableExists(string Table_Name)
        {
            bool isTableExist = false;
            try
            {
                lock (databaselock)
                {
                    using (SQLiteTransaction mytransaction = m_SqlCon.BeginTransaction())
                    {
                        using (SQLiteCommand mycommand = new SQLiteCommand(m_SqlCon))
                        {
                            mycommand.CommandText = "SELECT * FROM sqlite_master WHERE type='table' AND name=@name";
                            mycommand.Parameters.AddWithValue("@name", Table_Name);

                            using (SQLiteDataReader Sql_readNow = mycommand.ExecuteReader())
                            {
                                if (Sql_readNow != null && Sql_readNow.HasRows)
                                {
                                    isTableExist = true;
                                }
                                else
                                {
                                    isTableExist = false;
                                }
                            }
                        }
                        mytransaction.Commit();
                    }
                }
            }
            catch (Exception e)
            {
                isTableExist = false;
                WriteUniquelogs("SqliteLogs", "TableDataExists : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return isTableExist;
        }
        //20-July-2021: sandip: added function to check duplicate entries.
        public bool IsTokenExists(string tokenname, string tableName, string symbolValue, string optionsSymbol = "")
        {
            bool IsTokenExists = false;
            try
            {
                lock (databaselock)
                {
                    //using (SQLiteTransaction mytransaction = m_SqlCon.BeginTransaction())
                    {

                        string insertSQLQuery = "SELECT * FROM '" + tableName + "' WHERE InstrumentToken == '"+tokenname+"' and symbol =='"+ symbolValue+"'";
                        if(optionsSymbol!="")
                            insertSQLQuery = "SELECT * FROM '" + tableName + "' WHERE InstrumentToken == '" + tokenname + "' and symbol =='" + symbolValue + "' and optionsSymbol =='" + optionsSymbol + "'";
                        //using (SQLiteTransaction mytransaction = m_SqlCon.BeginTransaction())
                        {
                            using (SQLiteCommand mycommand = new SQLiteCommand(m_SqlCon))
                            {
                                mycommand.CommandText = insertSQLQuery;
                                using (SQLiteDataReader Sql_readNow = mycommand.ExecuteReader())
                                {
                                    if (Sql_readNow != null && Sql_readNow.HasRows)
                                    {
                                        IsTokenExists = true;
                                    }
                                }
                            }
                            //mytransaction.Commit();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                IsTokenExists = false;
                WriteUniquelogs("SqliteLogs", "IsTokenExists : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return IsTokenExists;
        }
        public bool InsertDataMultipleValues(string TblName, List<string> DBInsertQueries)
        {
            Boolean bolInsertBasicFileData = false;
            try
            {
                if (m_ConnectSuccess)
                {
                    lock (databaselock)
                    {
                        using (SQLiteTransaction mytransaction = m_SqlCon.BeginTransaction())
                        {
                            using (SQLiteCommand mycommand = new SQLiteCommand(m_SqlCon))
                            {
                                SQLiteParameter myparam = new SQLiteParameter();
                                int n;
                                for (n = 0; n < DBInsertQueries.Count(); n++)
                                {
                                    mycommand.CommandText = DBInsertQueries[n];
                                    mycommand.ExecuteNonQuery();
                                }
                            }
                            mytransaction.Commit();
                            DBInsertQueries.Clear();
                        }
                        bolInsertBasicFileData = true;
                    }
                }
            }
            catch (Exception ex)
            {
                bolInsertBasicFileData = false;
                WriteUniquelogs("SqliteLogs", "InsertDataMultipleValues : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
            return bolInsertBasicFileData;
        }

        public void CloseConnection()
        {
            try
            {
                m_SqlCon.Close();
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SqliteLogs", "CloseConnection : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        //sanika::14-sep-2020::wrote methos to return high and low
        public bool GetHighLow(string tableName, int barCount, out double dHigh, out double dLow)
        {
            dHigh = 0;
            dLow = 0;
            try
            {
                lock (databaselock)
                {
                    //sanika::15-dec-2020::Added distinct keyword in query
                    string insertSQLQuery = "SELECT max(High) as high, min(low) as low FROM '" + tableName + "' WHERE DoubleDate IN(SELECT DISTINCT DoubleDate FROM '" + tableName + "' order by DoubleDate DESC LIMIT " + barCount + ")";
                    using (SQLiteTransaction mytransaction = m_SqlCon.BeginTransaction())
                    {
                        using (SQLiteCommand mycommand = new SQLiteCommand(m_SqlCon))
                        {
                            mycommand.CommandText = insertSQLQuery;
                            using (SQLiteDataReader Sql_readNow = mycommand.ExecuteReader())
                            {
                                if (Sql_readNow != null && Sql_readNow.HasRows)
                                {
                                    if (Sql_readNow.Read())
                                    {
                                        dHigh = (double)Sql_readNow["high"];
                                        dLow = (double)Sql_readNow["low"];
                                    }
                                    if (dHigh == 0 || dLow == 0)
                                    {
                                        WriteUniquelogs("SqliteLogs", "GetHighLow : values are zero", MessageType.Informational);
                                    }
                                }
                            }
                        }
                        mytransaction.Commit();
                    }
                    return true;
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs("SqliteLogs", "GetHighLow : Exception Error Message = " + e.Message + " " + tableName, MessageType.Exception);
            }
            return false;
        }

        public void WriteUniquelogs(string symbol, string message, MessageType msgType)
        {
            try
            {
                SymbolLogs log = new SymbolLogs();
                log.createFile(symbol);
                log.LogMessage(message, MessageType.Informational);
                log = null;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : " + e.Message);
                Trace.WriteLine("Exception : " + e.Message);
            }
        }


        public bool GetMaxHighLow(string tableName,double startTime,double endTime,out double dHigh, out double dLow)
        {
            dHigh = 0;
            dLow = 0;
            try
            {
                lock (databaselock)
                {
                    string insertSQLQuery = "SELECT max(High) as high, min(low) as low FROM '" + tableName + "' WHERE DoubleDate between "+startTime +" and "+endTime+ " order by DoubleDate";
                    //sandip removed log
                    //WriteUniquelogs("SqliteLogs", "insertSQLQuery : " + insertSQLQuery + " startTime: " + DateTime.FromOADate(startTime) + " endTime: " + DateTime.FromOADate(endTime), MessageType.Informational);
                    using (SQLiteTransaction mytransaction = m_SqlCon.BeginTransaction())
                    {
                        using (SQLiteCommand mycommand = new SQLiteCommand(m_SqlCon))
                        {
                            mycommand.CommandText = insertSQLQuery;
                            using (SQLiteDataReader Sql_readNow = mycommand.ExecuteReader())
                            {
                                if (Sql_readNow != null && Sql_readNow.HasRows)
                                {
                                    if (Sql_readNow.Read())
                                    {
                                        if(!string.IsNullOrEmpty(Sql_readNow["high"].ToString()))
                                            dHigh = (double)Sql_readNow["high"];
                                        if (!string.IsNullOrEmpty(Sql_readNow["low"].ToString()))
                                            dLow = (double)Sql_readNow["low"];
                                    }
                                    if (dHigh == 0 || dLow == 0)
                                    {
                                        WriteUniquelogs("SqliteLogs", "GetMaxHigh : values are zero", MessageType.Informational);
                                    }
                                }
                            }
                        }
                        mytransaction.Commit();
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                WriteUniquelogs("SqliteLogs", "GetMaxHigh : Exception Error Message = " + e.Message + " " + tableName, MessageType.Exception);
            }
            return false;
        }

        public bool GetHighLow(string tableName, double startTime, double endTime, out double dHigh, out double dLow)
        {
            dHigh = 0;
            dLow = 0;
            try
            {
                lock (databaselock)
                {
                    string insertSQLQuery = "SELECT High as high, low as low FROM '" + tableName + "' WHERE DoubleDate between " + startTime + " and " + endTime + " order by DoubleDate";
                    using (SQLiteTransaction mytransaction = m_SqlCon.BeginTransaction())
                    {
                        using (SQLiteCommand mycommand = new SQLiteCommand(m_SqlCon))
                        {
                            mycommand.CommandText = insertSQLQuery;
                            using (SQLiteDataReader Sql_readNow = mycommand.ExecuteReader())
                            {
                                if (Sql_readNow != null && Sql_readNow.HasRows)
                                {
                                    if (Sql_readNow.Read())
                                    {
                                        if (!string.IsNullOrEmpty(Sql_readNow["high"].ToString()))
                                            dHigh = (double)Sql_readNow["high"];
                                        if (!string.IsNullOrEmpty(Sql_readNow["low"].ToString()))
                                            dLow = (double)Sql_readNow["low"];
                                    }
                                    if (dHigh == 0 || dLow == 0)
                                    {
                                        WriteUniquelogs("SqliteLogs", "GetHighLow : values are zero", MessageType.Informational);
                                    }
                                }
                            }
                        }
                        mytransaction.Commit();
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                WriteUniquelogs("SqliteLogs", "GetHighLow : Exception Error Message = " + e.Message + " " + tableName, MessageType.Exception);
            }
            return false;
        }



        //IRDS::sanika::30-oct-2020::Added to fetch yesterday close from db
        public double GetYesterdayClose(string tableName)
        {
            double close = 0;
            string tickTime = "";
            bool timeConfition24 = true;
            bool timeConfition12 = true;
            try
            {
                lock (databaselock)
                {
                    string date = DateTime.Now.ToString("dd-MM-yyyy");
                    WriteUniquelogs("SqliteLogs", "GetYesterdayClose with  day date " + date + " " + tableName, MessageType.Informational);
                    double doubleDate = DateTime.ParseExact(date, "dd-MM-yyyy", null).ToOADate();
                    string insertSQLQuery = "SELECT Close as close,DoubleDate FROM '" + tableName + "' where DoubleDate < "+ doubleDate +" order by DoubleDate DESC LIMIT 1";
                    using (SQLiteTransaction mytransaction = m_SqlCon.BeginTransaction())
                    {
                        using (SQLiteCommand mycommand = new SQLiteCommand(m_SqlCon))
                        {
                            mycommand.CommandText = insertSQLQuery;
                            using (SQLiteDataReader Sql_readNow = mycommand.ExecuteReader())
                            {
                                if (Sql_readNow != null && Sql_readNow.HasRows)
                                {
                                    if (Sql_readNow.Read())
                                    {
                                        close = (double)Sql_readNow["close"];
                                        double tick = (double)Sql_readNow["DoubleDate"];                                         
                                        tickTime = DateTime.FromOADate(tick).ToString();  
                                        if ((!tickTime.Contains("15:29:00")))
                                        {
                                            timeConfition24 = false;
                                            WriteUniquelogs("SqliteLogs", "GetClose : time not contains 15:29:00, tickTime " + tickTime + " "+ tableName, MessageType.Informational);
                                        }
                                        else
                                        {
                                            timeConfition24 = true;
                                            //WriteUniquelogs("SqliteLogs", "GetClose : tick time matches, tickTime " + tickTime + " " + tableName, MessageType.Informational);
                                        }
                                        if (timeConfition24==false && (!tickTime.Contains("3:29:00 PM")))
                                        {
                                            timeConfition12 = false;
                                            WriteUniquelogs("SqliteLogs", "GetClose : time not contains 03:29:00, tickTime " + tickTime + " "+ tableName, MessageType.Informational);
                                        }
                                        else
                                        {
                                            timeConfition12 = true;
                                            //WriteUniquelogs("SqliteLogs", "GetClose : tick time matches, tickTime " + tickTime + " " + tableName, MessageType.Informational);
                                        }
                                        if(timeConfition24 == false && timeConfition12 == false)
                                        {
                                            close = 0;
                                            WriteUniquelogs("SqliteLogs", "GetClose : close set 0, tickTime " + tickTime +" "+ tableName, MessageType.Informational);
                                        }
                                        
                                    }
                                    if (close == 0)
                                    {
                                        WriteUniquelogs("SqliteLogs", "GetClose : values are zero "+ tableName, MessageType.Informational);
                                    }
                                }
                            }
                        }
                        mytransaction.Commit();
                    }
                }
            }
            catch(Exception e)
            {
                WriteUniquelogs("SqliteLogs", "GetClose : Exception Error Message = " + e.Message + " " + tableName, MessageType.Exception);
            }
            return close;
        }

        public bool GetTwoTick(string tableName, double firstTickTime, double secondTickTime, out double firstCandleHigh, out double firstCandleLow, out double secondCandleHigh, out double secondCandleLow)
        {
            firstCandleHigh = firstCandleLow = secondCandleHigh = secondCandleLow = 0;
            string tickTime = "";
            try
            {
                lock (databaselock)
                {
                    string insertSQLQuery = "SELECT * FROM '" + tableName + "' WHERE DoubleDate between " + firstTickTime + " and " + secondTickTime + " order by DoubleDate";
                    using (SQLiteTransaction mytransaction = m_SqlCon.BeginTransaction())
                    {
                        using (SQLiteCommand mycommand = new SQLiteCommand(m_SqlCon))
                        {
                            mycommand.CommandText = insertSQLQuery;
                            using (SQLiteDataReader Sql_readNow = mycommand.ExecuteReader())
                            {
                                if (Sql_readNow != null && Sql_readNow.HasRows)
                                {
                                    //if (Sql_readNow.Read())
                                    {
                                        Console.WriteLine(Sql_readNow.FieldCount);
                                        while (Sql_readNow.Read())
                                        {
                                            if (firstCandleHigh == 0 && firstCandleLow == 0)
                                            {
                                                firstCandleHigh = (double)Sql_readNow["High"];
                                                firstCandleLow = (double)Sql_readNow["Low"];                                                
                                            }
                                            else
                                            {
                                                secondCandleHigh = (double)Sql_readNow["High"];
                                                secondCandleLow = (double)Sql_readNow["Low"];
                                            }
                                        }

                                        if (firstCandleHigh == 0 || firstCandleLow == 0 || secondCandleHigh == 0 || secondCandleLow == 0)
                                        {
                                            WriteUniquelogs("SqliteLogs", "GetMaxHigh : values are zero", MessageType.Informational);
                                        }
                                    }
                                }
                            }
                            mytransaction.Commit();
                        }
                    }
                    return true;
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs("SqliteLogs", "GetMaxHigh : Exception Error Message = " + e.Message + " " + tableName, MessageType.Exception);
            }
            return false;
        }
    }
    
}
