﻿using IRDSAlgoOMS;
using IRDSAlgoOMS.GUI;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IRDSAlgoTraderGUI
{
    public class RegistrationProcess
    {                 
        ReadSettings m_ReadSetting = null;
        ConfigSettings m_ConfigObj = null;
        string m_RegCredentialsFilePath = Directory.GetCurrentDirectory() + @"\Configuration\" + "RegCredentials.INFO";
        Logger logger = Logger.Instance;
        //Read userId from apiSetting ini file
        string path = Directory.GetCurrentDirectory();
        dynamic m_WrapperObj = null;
        INIFile m_iNIFile = null;
        //Pratiksha::02-Dec-2020::For Displaying alert only one time
        bool isDisplyExpiryAlert = false;
		//IRDSPM::Pratiksha::08-02-2021::Dynamic title
        string title = "";
        //sanika::14-sep-2020::Get broker name from ini file
        public string GetBroker()
        {
            string path = Directory.GetCurrentDirectory();
            path += @"\Configuration\Setting.ini";
            string brokerName = "";
            try
            {
                if (m_iNIFile == null)
                {
                    m_iNIFile = new INIFile(path);
                }
                brokerName = m_iNIFile.IniReadValue("BROKER", "brokername");
            }
            catch (Exception e)
            {
                WriteUniquelogs("RegistrationProcess", "GetBroker : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            if (brokerName == "")
            {
                brokerName = "Zerodha";
            }
            return brokerName;
        }

        void WriteUniquelogs(string fileName, string message, MessageType msgType)
        {
            try
            {
                SymbolLogs log = new SymbolLogs();
                log.createFile(fileName);
                log.LogMessage(message, MessageType.Informational);
                log = null;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : " + e.Message);
            }
        }

        public void StartGUI()
        {
            try
            {
                bool IsnetworkCheck = false;
                string strIniFilePath = GetIniFilePath();               
                if (System.IO.File.Exists(strIniFilePath))
                {                    
                    IsnetworkCheck = CheckForInternetConnection();
                    if (IsnetworkCheck == false)
                    {
                        WriteUniquelogs("RegistrationProcess", "StartGUI : IsnetworkCheck is false "+ IsnetworkCheck, MessageType.Informational);
                        connection(IsnetworkCheck, strIniFilePath);
                    }
                    else
                    {
                        RegistrationWindowForm registrationWindowObj = null;
                        double expiryDate = 0.0;                        
                        //sanika::9-oct-2020::get hardwareid
                        string userid = GetHardwareID();
                        if (File.Exists(m_RegCredentialsFilePath))
                        {
                            m_ConfigObj = new ConfigSettings(logger);
                            m_ConfigObj.ReadSettingConfigFile(path);
                            m_ConfigObj.ReadRegistrationInfoFile();
                            if (m_ConfigObj.strExpiryDate.ToString().Length > 0 && m_ConfigObj.strName.Length > 0)
                            {
                                expiryDate = Convert.ToDouble(m_ConfigObj.strExpiryDate);
                                //If you want to test from INI file then plase comment above line and add days to below line
                                //expiryDate = Convert.ToDouble(DateTime.Now.AddDays(-1).ToOADate());
                                string lastDateCheckedFromDB = m_ConfigObj.strDate;
                                string ClientID = m_ConfigObj.strName;
                                if (ClientID == userid)
                                {                                    
                                    WriteUniquelogs("RegistrationProcess", "StartGUI : userid present in RegCredentials file", MessageType.Informational);
                                    double today = DateTime.Now.ToOADate();
                                    if (expiryDate > today)
                                    {
                                        WriteUniquelogs("RegistrationProcess", "StartGUI : Going to calculate expiry date", MessageType.Informational);
                                        ForExpiryCheck(expiryDate);
                                        if(lastDateCheckedFromDB != DateTime.Now.ToString("M-d-yyyy"))
                                        {
                                            ReadFromServerDB(registrationWindowObj, userid);                                            
                                            INIFile iNIFile = new INIFile(m_RegCredentialsFilePath);
                                            iNIFile.TradingIniWriteValue("X1eaJ0kA6ril", "0xiW6Ajwsa", EncryptData(DateTime.Now.ToString("M-d-yyyy"), "IrdsalgoTrader"), m_RegCredentialsFilePath);
                                            
                                        }
                                    }
                                    else
                                    {
                                        WriteUniquelogs("RegistrationProcess", "StartGUI : Checking expiry date in db", MessageType.Informational);
                                        ReadFromServerDB(registrationWindowObj, userid);
                                    }
                                }
                                else
                                {
                                    WriteUniquelogs("RegistrationProcess", "StartGUI : userid not present in RegCredentials file", MessageType.Informational);
                                    ShowRegistrationForm(registrationWindowObj);
                                }
                            }
                            else
                            {
                                WriteUniquelogs("RegistrationProcess", "StartGUI : Details not matched from file", MessageType.Informational);
                                ReadFromServerDB(registrationWindowObj, userid);
                            }
                        }
                        else
                        {
                            WriteUniquelogs("RegistrationProcess", "StartGUI : RegCredentials file not exist", MessageType.Informational);
                            ReadFromServerDB(registrationWindowObj, userid);
                        }
                       
                    }
                }               
                if (IsnetworkCheck != false)
                {
                    string selectedBroker = GetBroker();
                    StartConnection(selectedBroker);
                }

            }
            catch (Exception ex)
            {
                WriteUniquelogs("RegistrationProcess", "StartGUI : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void ForExpiryCheck(double expiryDate)
        {
            try
            {
                double tempDate = DateTime.Now.ToOADate();
                DateTime date1 = DateTime.Now;
                DateTime date2 = DateTime.FromOADate(expiryDate);
                double today = DateTime.Now.ToOADate();
                TimeSpan t = date2.Subtract(date1);

                if (t.Days == 3 || t.Days == 2 || t.Days == 1 || (t.Days == 0))
                {
                    WriteUniquelogs("RegistrationProcess", "ForExpiryCheck : going to expire!! expiryDate "+ date2.ToString() + "  Days " + t.Days + " hours "+ t.Hours, MessageType.Informational);
                    //sanika::1-oct-2020::changed condition for if today is expiry date
                    if (expiryDate < today && date1.Date != date2.Date)
                    {
                        //Checking in db
                    }
                    else
                    {
                        //IRDSPM::PRatiksha::08-10-2020::Added messages for difference of day 0,1,2,3, aadded different message because 1 mesage not giving proper things
                        //string message = "Your validity period is going to expire soon, please contact to support team.";
                        string message = "";
                        MessageBoxButtons buttons = MessageBoxButtons.OK;
                        //Pratiksha::02-Dec-2020::here checked the flag value
                        if (isDisplyExpiryAlert == false)
                        {
                        if ((t.Days > 1 && t.Hours > 12) || (t.Days <=3 && t.Days > 0) )
                        {
                            if( t.Days <2)
                            {
                                message = "Your validity period is going to expire within 2 days, please contact to support team before " + date2.ToString("dd/MM/yyyy") + ".";
                            }
                            else if(t.Days == 2 && t.Hours >12)
                            {
                                message = "Your validity period is going to expire within 3 days, please contact to support team before " + date2.ToString("dd/MM/yyyy") + ".";
                            }
                            else
                            {
                                message = "Your validity period is going to expire within " + t.Days + " days, please contact to support team before " + date2.ToString("dd/MM/yyyy") + ".";
                            }
                        }
                       else if (t.Days <= 1 && t.Hours > 12)
                        {
                            message = "Your validity period is going to expire within a day, please contact to support team before " + date2.ToString("dd/MM/yyyy") + ".";
                        }
                        else if(t.Hours < 12 && t.Hours > 0 )//sanika::1-Dec-2020::added condition for empty message box 
                        {
                            message = "Your validity period is going to expire within "+t.Hours+" hours, please contact to support team before " + date2.ToString("dd/MM/yyyy") + ".";
                        }
                        else if (date1.Date == date2.Date)
                        {
                            // message = "Your validity period is going to expire today, please contact to support team before " + date2 + ".";
                            MessageForregister();
                        }
                    
                        DialogResult result = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                            //Pratiksha::02-Dec-2020::Here set the value
                            isDisplyExpiryAlert = true;
                        }
                    }

                }
                else
                {
                    WriteUniquelogs("RegistrationProcess", "ForExpiryCheck : Not expired!! expiryDate = " + date2.ToString(), MessageType.Informational);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("RegistrationProcess", "ForExpiryCheck : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        //sanika::1-oct-2020::Remove return value - no need to return form object
        private void CheckingDataFromserverDb(RegistrationWindowForm regFormObj, string userid)
        {
            try
            {

                string broker = GetBroker(); 
                double tempTodday = DateTime.Now.ToOADate();
                int totalUserCount = m_WrapperObj.RetrieveUserdetails(userid);
                WriteUniquelogs("RegistrationProcess", "CheckingDataFromserverDb : totalUserCount " + totalUserCount, MessageType.Informational);
                if (totalUserCount == 1)
                {
                    double getExpiryDate;
                    getExpiryDate = m_WrapperObj.GetExpiryDate(userid);
                    WriteUniquelogs("RegistrationProcess", "CheckingDataFromserverDb : getExpiryDate " + getExpiryDate, MessageType.Informational);
                    //If you want to test from server please comment above line
                    // getExpiryDate = Convert.ToDouble(DateTime.Now.AddDays(-1).ToOADate());
                    ForExpiryCheck(getExpiryDate);
                    if (getExpiryDate > 0)
                    {
                        //sanika::1-oct-2020::commented no need to create obj again
                        //regFormObj = new RegistrationWindowForm();
                        //RegistrationWPFForm obj = new RegistrationWPFForm(regFormObj);
                        //sanika::1-oct-2020::wrote method in windows form and call that commented above line
                        regFormObj.writeInINIFile(userid, getExpiryDate);
                        if (tempTodday > getExpiryDate)
                        {
                            WriteUniquelogs("RegistrationProcess", "CheckingDataFromserverDb : date expired "+ getExpiryDate, MessageType.Informational);
                            MessageForregister();
                        }
                        else
                        {
                            WriteUniquelogs("RegistrationProcess", "CheckingDataFromserverDb : date not expired " + getExpiryDate, MessageType.Informational);
                        }
                    }
                    else
                    {
                        WriteUniquelogs("RegistrationProcess", "CheckingDataFromserverDb : Error comes for connection totalUserCount " + totalUserCount, MessageType.Informational);
                        string message = "Technical error has occured.";
                        MessageBoxButtons buttons = MessageBoxButtons.OK;
                        DialogResult result = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                        if (DialogResult.OK == result)
                        {
                            Environment.Exit(0);
                        }
                    }
                }
                else if (totalUserCount == 0)
                {
                    WriteUniquelogs("RegistrationProcess", "CheckingDataFromserverDb : user has not registered yet.. going to open register form", MessageType.Informational);
                    ShowRegistrationForm(regFormObj);
                }
                else
                {
                    WriteUniquelogs("RegistrationProcess", "CheckingDataFromserverDb : Error comes for connection totalUserCount "+ totalUserCount, MessageType.Informational);
                    string message = "Technical error has occured.";
                    MessageBoxButtons buttons = MessageBoxButtons.OK;
                    DialogResult result = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                    if (DialogResult.OK == result)
                    {
                        Environment.Exit(0);
                    }
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("RegistrationProcess", "CheckingDataFromserverDb : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }


        private void MessageForregister()
        {
            //IRDSPM::Pratiksha::21-12-2020::For double messsagebox
            string message = "Your expiry date has expired, would you like to purchase license?";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
            if (result == DialogResult.Yes)
            {
                string message1 = "After payment please send Payment ID on 'info@irdsalgotrader.com'";
                MessageBoxButtons buttons1 = MessageBoxButtons.OK;
                DialogResult result1 = System.Windows.Forms.MessageBox.Show(message1, title, buttons1, MessageBoxIcon.Information);
                try
                {
                    Process.Start("https://rzp.io/i/dxIZMgn");
                }
                catch (Exception ex)
                {
                    WriteUniquelogs("RegistrationProcess", "MessageForregister : Exception Error Message = " + ex.Message, MessageType.Exception);
                }
                //message = "Please contact to support team for license purchase.";
                //System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
            }
            Environment.Exit(0);
        }


        //IRDSPM::PRatiksha::21-09-2020::For internet conncetion


        //IRDSPM::PRatiksha::21-09-2020::To check weather it is connected to internet or not
        public bool CheckForInternetConnection()
        {
            try
            {
                using (var client = new WebClient())
                using (client.OpenRead("http://google.com/generate_204"))
                    return true;
            }
            catch(Exception e)
            {
                WriteUniquelogs("RegistrationProcess", "CheckForInternetConnection : Exception Error Message = " + e.Message, MessageType.Exception);
                return false;
            }
        }

        public void connection(bool IsnetworkChecked, string filePath)
        {
            try
            {
                string selectedBroker = GetBroker();
                string userid = "";
                double expiryDate = 0.0;
                string ClientID = "";
                string mobileNo = "";
                string strIniFilePath = filePath;
                if (System.IO.File.Exists(strIniFilePath))
                {
                    //sanika::1-oct-2020::added check for obj                   
                    if (m_ConfigObj == null)
                    {
                        m_ConfigObj = new ConfigSettings(logger);
                    }
                    userid = GetHardwareID();                   
                    if (File.Exists(m_RegCredentialsFilePath))
                    {
                        m_ConfigObj.ReadRegistrationInfoFile();
                        //sanika::09-oct-2020::added condition for mobile no also
                        if (m_ConfigObj.strExpiryDate.ToString().Length > 0 && m_ConfigObj.strName.Length > 0)
                        {
                            expiryDate = Convert.ToDouble(m_ConfigObj.strExpiryDate);
                            //If you want to test from server please comment above line
                            // expiryDate = Convert.ToDouble(DateTime.Now.AddDays(-20).ToOADate());
                            ClientID = m_ConfigObj.strName;

                            if (ClientID == userid)
                            {
                                double today = DateTime.Now.ToOADate();
                                if (expiryDate > today)
                                {
                                    WriteUniquelogs("RegistrationProcess", "connection : Going to check expiry", MessageType.Informational);
                                    ForExpiryCheck(expiryDate);
                                    StartConnection(selectedBroker);
                                }
                                else
                                {
                                    WriteUniquelogs("RegistrationProcess", "connection : Expiry date greater than today = "+expiryDate, MessageType.Informational);
                                    NoInternetConnectionMessagebox();
                                }
                            }
                            else
                            {
                                WriteUniquelogs("RegistrationProcess", "connection : user Id not match", MessageType.Informational);
                                NoInternetConnectionMessagebox();
                            }
                        }
                        else
                        {
                            WriteUniquelogs("RegistrationProcess", "connection : credentials details not match", MessageType.Informational);
                            NoInternetConnectionMessagebox();
                        }
                    }
                    else
                    {
                        WriteUniquelogs("RegistrationProcess", "connection : RegCredentials file not exist", MessageType.Informational);
                        NoInternetConnectionMessagebox();
                    }
                }
                else
                {
                    WriteUniquelogs("RegistrationProcess", "connection : Ini file not exist", MessageType.Informational);
                    //sanika::1-oct-2020::commented wrong message box
                    //NoInternetConnectionMessagebox();
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs("RegistrationProcess", "connection : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        private void NoInternetConnectionMessagebox()
        {
            string message = "Please check internet connection!!";
            MessageBoxButtons buttons = MessageBoxButtons.OK;
            System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
            Environment.Exit(0);
        }

        //sanika::1-oct-2020::Remove return value - no need to return form object
        private void ShowRegistrationForm(RegistrationWindowForm regFormObj)
        {
            //if (regFormObj == null)
            //{
            //    regFormObj = new RegistrationWindowForm(m_WrapperObj, title);
            //}
            //regFormObj.ShowDialog();
        }

        //sanika::1-oct-2020::Remove return value - no need to return form object
        private void ReadFromServerDB(RegistrationWindowForm regFormObj, string userid)
        {
            //if (regFormObj == null)
            //{
            //    regFormObj = new RegistrationWindowForm(m_WrapperObj, title);
            //}
             CheckingDataFromserverDb(regFormObj, userid);
        }

        //sanika::1-oct-2020::Added function to connection
        void StartConnection(string brokerName)
        {
            try
            {
                dynamic AlgoOMSBridge = new AlgoOMS();
                if (brokerName.ToLower() == "zerodha")
                { 
                    AlgoOMSBridge.SetBroker(0);
                    AlgoOMSBridge.CreateKiteConnectObject("XXXXXX", "APISetting.ini", true);
                }
                else if (brokerName.ToLower() == "samco")
                {
                    AlgoOMSBridge.SetBroker(1);
                    AlgoOMSBridge.CreateKiteConnectObject("XXXXXX", "SamcoSettings.ini", true);
                }
                else if (brokerName.ToLower() == "composite")
                {
                    AlgoOMSBridge.SetBroker(2);
                    AlgoOMSBridge.CreateKiteConnectObject("XXXXXX", "CompositeSettings.ini", true);
                }
                WriteUniquelogs("RegistrationProcess", "StartConnection : going to start " + brokerName, MessageType.Informational);
                AlgoOMSBridge.Connect("XXXXXX");
            }
            catch (Exception e)
            {
                WriteUniquelogs("RegistrationProcess", "StartConnection : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        //sanika::1-oct-2020::added seperate function
        string GetIniFilePath()
        {
            string selectedBroker = GetBroker();
            WriteUniquelogs("RegistrationProcess", "GetIniFilePath : broker name = "+ selectedBroker, MessageType.Informational);
            string filePath = "";
            if (selectedBroker.ToLower() == "zerodha")
            {
                filePath = path + @"\Configuration\" + "APISetting.ini";
                if (m_WrapperObj == null)
                {
                    m_WrapperObj = new KiteConnectWrapper();
                }
            }
            else if (selectedBroker.ToLower() == "samco")
            {
                filePath = path + @"\Configuration\" + "SamcoSettings.ini";
                if (m_WrapperObj == null)
                {
                    m_WrapperObj = new SamcoConnectWrapper();
                }
            }
            else if (selectedBroker.ToLower() == "composite")
            {                
                filePath = path + @"\Configuration\" + "CompositeSettings.ini";
                if (m_WrapperObj == null)
                {
                    m_WrapperObj = new CompositeConnectWrapper();
                }
            }
            return filePath;
        }

        //sanika::Added ot get harware id
        public string GetHardwareID()
        {
            string hardwareId = "";
            hardwareId = License.Status.HardwareID;
            return hardwareId;
        }

        public static string EncryptData(string strInText, string strKey)
        {
            byte[] bytesBuff = Encoding.Unicode.GetBytes(strInText);

            using (Aes aes__1 = Aes.Create())
            {
                Rfc2898DeriveBytes crypto = new Rfc2898DeriveBytes(strKey, new byte[] {
                0x49,
                0x76,
                0x61,
                0x6e,
                0x20,
                0x4d,
                0x65,
                0x64,
                0x76,
                0x65,
                0x64,
                0x65,
                0x76
            });
                aes__1.Key = crypto.GetBytes(32);
                aes__1.IV = crypto.GetBytes(16);

                using (MemoryStream mStream = new MemoryStream())
                {
                    using (CryptoStream cStream = new CryptoStream(mStream, aes__1.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cStream.Write(bytesBuff, 0, bytesBuff.Length);
                        cStream.Close();
                    }

                    strInText = Convert.ToBase64String(mStream.ToArray());
                }
            }

            return strInText;
        }
        //Pratiksha::04-12-2020
        public bool CheckinDbForBlock()
        {            
            string licenceID = "";
            bool statusUSer = false;
            try
            {
                GetIniFilePath();
                //if (m_WrapperObj == null)
                //{
                //    WriteUniquelogs("RegistrationProcess", "CheckinDbForBlock : m_WrapperObj = object creation", MessageType.Informational);
                //    m_WrapperObj = new KiteConnectWrapper();
                //}
                licenceID = GetHardwareID();                         
                WriteUniquelogs("RegistrationProcess", "CheckinDbForBlock : licenceID = "+ licenceID, MessageType.Informational);
                statusUSer = m_WrapperObj.FetchUserdetailsfromBlockuser(licenceID); 
                WriteUniquelogs("RegistrationProcess", "CheckinDbForBlock :  " + statusUSer, MessageType.Informational);
            }
            catch (Exception e)
            {
                WriteUniquelogs("RegistrationProcess", "CheckinDbForBlock : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return statusUSer;
        }
		//IRDSPM::Pratiksha::08-02-2021::For reading title
       public string readTitle()
        {
            m_ConfigObj = new ConfigSettings(logger);
            string iniPath = path + @"\Configuration\Setting.ini";
            m_ConfigObj.ReadSettingConfigFile(iniPath);
            title = m_ConfigObj.m_Title;
            return title;
        }
    }
}
