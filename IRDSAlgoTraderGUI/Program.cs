﻿using IRDSAlgoOMS;
using IRDSAlgoOMS.GUI;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using SendEmailSMS;

namespace IRDSAlgoTraderGUI
{
    static class Program
    {
        static public System.Threading.Mutex mtex1 = null;

        [STAThread]
        static void Main(string[] args)
        {
            bool m_AutoRestart = false;
            if (args.Count() > 0)
            {
                if (args[0] == "AUTORESTART")
                {
                    m_AutoRestart = true;
                    Thread.Sleep(2000);
                    foreach (Process proc in Process.GetProcesses())
                    {
                        if (proc.ProcessName.Equals(Process.GetCurrentProcess().ProcessName) && proc.Id != Process.GetCurrentProcess().Id)
                        {
                            proc.Kill();
                            break;
                        }
                    }
                }
            }

            bool instanceCountOne = false;
            mtex1 = new System.Threading.Mutex(true, "IRDSAlgoTrader11", out instanceCountOne);
            if (!instanceCountOne && m_AutoRestart == false)
            {
                //sanika::22-oct-2020::added message box to close running application and start again
                string message = "Already Application is running. Do you want to close forcefully?";
                string title = readTitle();
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                DialogResult dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                if (dialog == DialogResult.Yes)
                {
                    //MessageBox.Show("An application instance is already running", "IntellirichOHLSystem");
                    foreach (Process proc in Process.GetProcesses())
                    {
                        if (proc.ProcessName.Equals(Process.GetCurrentProcess().ProcessName) && proc.Id != Process.GetCurrentProcess().Id)
                        {
                            proc.Kill();
                            break;
                        }
                    }
                }
                else if (dialog == DialogResult.No)
                {
                    Application.Exit();
                    return;
                }
            }

            StartProcess();
        }

        //IRDSPM::Pratiksha::08-02-2021::For reading title
        public static string readTitle()
        {
            string path = Directory.GetCurrentDirectory();
            string title = "";
            IRDSAlgoOMS.ConfigSettings m_ConfigObj = new IRDSAlgoOMS.ConfigSettings();
            string iniPath = path + @"\Configuration\Setting.ini";
            m_ConfigObj.ReadSettingConfigFile(iniPath);
            title = m_ConfigObj.m_Title;
            return title;
        }
        public static void StartProcess()
        {
            string path = Directory.GetCurrentDirectory();
            path += @"\Configuration\Setting.ini";
            INIFile iNIFile = new INIFile(path);
            RegistrationProcess registrationProcess = new RegistrationProcess();
            //Pratiksha::04-12-2020
            //DateTime dtExpiryDate = DateTime.Today.AddDays(25);
            //double dexpirydate = dtExpiryDate.ToOADate();

            //sanika::30-sep-2020::check registration allow or not
            // bool isRegistrationAllow = GetRegistrationAllowFlag(iNIFile);
            bool isRegistrationAllow = false;
            if (isRegistrationAllow)
            {
                if (!registrationProcess.CheckinDbForBlock())
                {
                    //sanika::30-sep-2020::added seperate classs for registration process flow
                    registrationProcess.StartGUI();
                }
                else
                {
                    string title = "IRDS Algo Trader";
                    string message = "Your account has been blocked by Algo Trader, please contact to support team for more information.";
                    MessageBoxButtons buttons = MessageBoxButtons.OK;
                    System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                    Environment.Exit(0);
                }
            }
            else
            {

                //MessageBox.Show("False : registration proress");
                dynamic AlgoOMSBridge = new AlgoOMS();//
                Trace.WriteLine("Before fetching broker name");
                //sanika::14-sep-2020::Get broker name from ini file
                string brokerName = GetBroker(iNIFile);
                Trace.WriteLine("brokerName " + brokerName);
                if (brokerName.ToLower() == "zerodha")
                {
                    AlgoOMSBridge.SetBroker(0);
                    AlgoOMSBridge.CreateKiteConnectObject("XXXXXX", "APISetting.ini", true);
                }
                else if (brokerName.ToLower() == "samco")
                {
                    AlgoOMSBridge.SetBroker(1);
                    AlgoOMSBridge.CreateKiteConnectObject("XXXXXX", "SamcoSettings.ini", true);
                }
                else if (brokerName.ToLower() == "composite")
                {
                    AlgoOMSBridge.SetBroker(2);
                    AlgoOMSBridge.CreateKiteConnectObject("XXXXXX", "CompositeSettings.ini", true);
                }


                Trace.WriteLine("Going to login ");
                AlgoOMSBridge.Connect("XXXXXX");
                Trace.WriteLine("After login");
            }
        }




        //sanika::14-sep-2020::Get broker name from ini file
        static string GetBroker(INIFile iNIFile)
        {
            string brokerName = "";
            try
            {
                brokerName = iNIFile.IniReadValue("BROKER", "brokername");
            }
            catch (Exception e)
            {
                Trace.WriteLine("exception " + e.Message);
            }
            if (brokerName == "")
            {
                brokerName = "Zerodha";
            }
            return brokerName;
        }

        static bool GetRegistrationAllowFlag(INIFile iNIFile)
        {
            bool isAllow = false;
            try
            {
                isAllow = Convert.ToBoolean(iNIFile.IniReadBoolValue("REGISTRATION", "allowRegistration"));
            }
            catch (Exception e)
            {
                Trace.WriteLine("exception " + e.Message);
            }
            return isAllow;
        }
    }
}

