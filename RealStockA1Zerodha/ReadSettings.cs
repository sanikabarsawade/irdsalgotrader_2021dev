﻿using IRDSAlgoOMS;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Linq;

namespace RealStockA1Strategy
{   
    public  class ReadSettings
    {
        static INIFile inif;       
        public List<string> tradingSymbolListWithAllData = new List<string>();
        public TradingSymbolInfo tradingSymbolInfo;
        public List<TradingSymbolInfo> TradingSymbolsInfoList = new List<TradingSymbolInfo>();
        public List<string> TradingsymbolList = new List<string>(); 
        Logger logger;  

        public string m_AccountNumber = "";
        public string m_StartEntryTime = "";
        public string m_EndEntryTime = "";
       // public string m_StartExitTime = "";
        public string m_EndExitTime = "";
        //IRDS::Pratiksha::08-06-2021::Comment 2 fields
        //public int m_Interval = 1;
        //public int m_NoOfLots = 0;
        //public double m_CallStrikeValue = 0;
        //public double m_PutStrikeValue = 0;
        public double m_StopLossPercent = 0;
        public bool m_BreakEvenStop = false;
        //public double m_ReducePercentInLot = 0;
        public int m_StoplossRejectTimeInterval = 0;
        //IRDSPM::Pratiksha::14-05-2021::For aditional account details
        public string m_AdditionalAccount = "";

        //IRDSPM::PRatiksha::22-04-2021::For symbol list
        public List<string> RealtimeSymbolfromsymbolini = new List<string>();
        public List<string> SymbolListFromsymbolini = new List<string>();
        List<string> DecryptDetListComposite;
        List<string> EncDetListComposite;

        //composite

        public string root = "";
        public string username = "";
        public string appKey = "";
        public string secretKey = "";
        public string marketDataAppKey = "";
        public string marketDataSecretKey = "";
        public bool showGUIForOpenPositions = false;

        List<string> PlainDetListZerodha;
        List<string> EncDetListZerodha;
        List<string> decryptDetListZerodha;
        //Zerodha

        public string Zroot = "";
        public string Zuserid = "";
        public string ZappKey = "";
        public string ZsecretKey = "";
        public string ZPassword = "";
        public string ZPin = "";
        public string Zlogin = "";
        public ReadSettings(Logger logger)
        {
            this.logger = logger;
        }
        public ReadSettings(Logger logger,AlgoOMS AlgoOMS )
        {
            if(this.logger == null)
                this.logger = logger;            
        }

        public void writeINIFile(string section,string key,string value)
        {           
            inif.IniWriteValue(section, key, value);            
        }


        public void writeINIFileSymbolSetting(string section, string key, string value)
        {
            inif.IniWriteValueSymbolSetting(section, key, value);
        }

        public void ReadTradingSymbols(string filepath)
        {
            try
            {
                TradingsymbolList.Clear();                
                inif = new INIFile(filepath);
                tradingSymbolListWithAllData = inif.GetKeyValues("TRADINGSYMBOL");
                TradingSymbolsInfoList.Clear();
                foreach (var symbol in tradingSymbolListWithAllData)
                {
                    string[] str = symbol.Split(',');
                    if (!(str.Length < 7))
                    {
                        tradingSymbolInfo = new TradingSymbolInfo();
                        TradingsymbolList.Add((str[0]));
                        tradingSymbolInfo.TradingSymbol = str[0];
                        tradingSymbolInfo.Exchange = str[1];
                        tradingSymbolInfo.ExpiryDate = str[2];
                        //IRDSPM::29-04-2021::For fetching new values
                        tradingSymbolInfo.ExpiryPeriod = str[3];
                        //IRDSPM::Pratiksha::Added new parameter order type
                        tradingSymbolInfo.OrderType = str[4];
                        tradingSymbolInfo.lotsize = Convert.ToDouble(str[5]);
                        //IRDS::Pratiksha::08-06-2021::Add TotalNoOfLots
                        tradingSymbolInfo.TotalNoOfLots = Convert.ToInt32(str[6]);
                        tradingSymbolInfo.Roundoff = Convert.ToInt32(str[7]);
                        //IRDSPM::Pratiksha::19-05-2021::For strike price
                        tradingSymbolInfo.StrikePriceCall = Convert.ToDouble(str[8]);
                        tradingSymbolInfo.StrikePricePut = Convert.ToDouble(str[9]);
                    }
                    TradingSymbolsInfoList.Add(tradingSymbolInfo);
                }
            }
            catch (Exception e)
            {
                logger.LogMessage("ReadTradingSymbols : Exception Error Message = " + e.Message, MessageType.Informational);
            }
        }        

        public bool ReadINIFile(string filename)
        {
            bool isload = true;
            try
            {              
                inif = new INIFile(filename);
                m_AccountNumber = inif.IniReadValue("SDATA", "AccountNumber");
                m_StartEntryTime = inif.IniReadValue("SDATA", "StartEntryTime");
                m_EndEntryTime = inif.IniReadValue("SDATA", "EndEntryTime");
                //m_StartExitTime = inif.IniReadValue("SDATA", "StartExitTime");
                m_EndExitTime = inif.IniReadValue("SDATA", "EndExitTime");
                //m_Interval = Convert.ToInt32(inif.IniReadDoubleValue("SDATA", "Interval"));
                //m_NoOfLots = Convert.ToInt32(inif.IniReadDoubleValue("SDATA", "NoOfLots"));
                //m_CallStrikeValue = Convert.ToDouble(inif.IniReadDoubleValue("SDATA", "CallStrikeValue"));
                //m_PutStrikeValue = Convert.ToDouble(inif.IniReadDoubleValue("SDATA", "PutStrikeValue"));
                m_StopLossPercent = Convert.ToDouble(inif.IniReadDoubleValue("SDATA", "StoplossPercent"));
                m_BreakEvenStop = Convert.ToBoolean(inif.IniReadBoolValue("SDATA", "BreakEvenStop"));
                //m_ReducePercentInLot = Convert.ToDouble(inif.IniReadDoubleValue("SDATA", "ReducePercentInLot"));
                m_StoplossRejectTimeInterval = Convert.ToInt32(inif.IniReadDoubleValue("SDATA", "StoplossRejectTimeInterval"));
                //IRDSPM::Pratiksha::14-05-2021::For aditional account details
                m_AdditionalAccount = inif.IniReadValue("SDATA", "AdditionalAccount");
                ReadTradingSymbols(filename);
            }
            catch (Exception e)
            {
                logger.LogMessage("ReadINIFile : Exception Error Message =" + e.Message, MessageType.Informational);
            }
            return isload;
        }

        public void readCompositeConfigFile(string filename)
        {
            try
            {
                EncDetListComposite = new List<string>();
                inif = new INIFile(filename);
                root = inif.IniReadValue("credentials", "root");
                //string user_name = inif.IniReadValue("credentials", "username");
                string CName = inif.IniReadValue("credentials", "MyUserId");
                string CappKey = inif.IniReadValue("credentials", "appKey");
                string CsecretKey = inif.IniReadValue("credentials", "secretKey");
                string CmarketDataAppKey = inif.IniReadValue("credentials", "MarketDataappKey");
                string CmarketDataSecretKey = inif.IniReadValue("credentials", "MarketDatasecretKey");
                showGUIForOpenPositions = Convert.ToBoolean(inif.IniReadBoolValue("credentials", "showGUIForOpenPositions"));
                if (CName != "XXXXXX" && CappKey != "XXXXXX" && CsecretKey != "XXXXXX" && CmarketDataAppKey != "XXXXXX" && CmarketDataSecretKey != "XXXXXX")
                {
                    EncDetListComposite.Add(CName); EncDetListComposite.Add(CappKey); EncDetListComposite.Add(CsecretKey);
                    EncDetListComposite.Add(CmarketDataAppKey); EncDetListComposite.Add(CmarketDataSecretKey);
                    decryptDetComposite(EncDetListComposite);

                    username = DecryptDetListComposite[0];
                    appKey = DecryptDetListComposite[1];
                    secretKey = DecryptDetListComposite[2];
                    marketDataAppKey = DecryptDetListComposite[3];
                    marketDataSecretKey = DecryptDetListComposite[4];
                }
                else
                {
                    username = CName;
                    appKey = CappKey;
                    secretKey = CsecretKey;
                    marketDataAppKey = CmarketDataAppKey;
                    marketDataSecretKey = CmarketDataSecretKey;
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs("ReadConfigSettings", "readCompositeConfigFile : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        public void readConfigFileZerodha(string filename)
        {
            //PlainDetListZerodha = new List<string>();
            EncDetListZerodha = new List<string>();
            decryptDetListZerodha = new List<string>();
            try
            {
                logger.LogMessage("In readConfigFileZerodha", MessageType.Informational);
                inif = new INIFile(filename);
                Zroot = inif.IniReadValue("credentials", "root");
                Zlogin = inif.IniReadValue("credentials", "login");
                string api = inif.IniReadValue("credentials", "MyAPIKey");
                string secret = inif.IniReadValue("credentials", "MySecret");
                string id = inif.IniReadValue("credentials", "MyUserId");
                string pwd = inif.IniReadValue("credentials", "MyPassword");
                string pin = inif.IniReadValue("credentials", "Pin");


                if (api != "XXXXXX" && secret != "XXXXXX" && id != "XXXXXX" && pwd != "XXXXXX" && pin != "XXXXXX")
                {
                    EncDetListZerodha.Add(api); EncDetListZerodha.Add(secret); EncDetListZerodha.Add(id); EncDetListZerodha.Add(pwd); EncDetListZerodha.Add(pin);
                    decryptDetZerodha(EncDetListZerodha);
                    ZappKey = decryptDetListZerodha[0];
                    ZsecretKey = decryptDetListZerodha[1];
                    Zuserid = decryptDetListZerodha[2];
                    ZPassword = decryptDetListZerodha[3];
                    ZPin = decryptDetListZerodha[4];
                }
                else
                {
                    ZappKey = api;
                    ZsecretKey = secret;
                    Zuserid = id;
                    ZPassword = pwd;
                    ZPin = pin;
                }
            }
            catch (Exception e)
            {
                logger.LogMessage("readConfigFile - exception" + e.Message, MessageType.Informational);
            }
        }

        //Pratiksha::01-01-2021::For Composite
        public void decryptDetComposite(List<string> EncDetList)
        {
            try
            {
                DecryptDetListComposite = new List<string>();
                for (int i = 0; i < EncDetList.Count; i++)
                {
                    string plaintext = DecryptData(EncDetList[i], "IrdsalgoTrader");
                    DecryptDetListComposite.Add(plaintext);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs("ReadConfigSettings", "decryptDetComposite : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        public void decryptDetZerodha(List<string> EncDetList)
        {
            try
            {
                decryptDetListZerodha = new List<string>();
                for (int i = 0; i < EncDetList.Count; i++)
                {
                    string plaintext = DecryptData(EncDetList[i], "IrdsalgoTrader");
                    decryptDetListZerodha.Add(plaintext);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs("ReadConfigSettings", "decryptDetComposite : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }
        public void WriteUniquelogs(string symbol, string message, MessageType msgType)
        {
            try
            {
                SymbolLogs log = new SymbolLogs();
                //log = SymbolLogs.InstanceSymbolLogs;
                log.createFile(symbol);
                log.LogMessage(message, MessageType.Informational);
                log = null;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : " + e.Message);
                Trace.WriteLine("Exception : " + e.Message);
            }
        }
        public static string DecryptData(string strCryptTxt, string strKey)
        {
            strCryptTxt = strCryptTxt.Replace(" ", "+");

            byte[] bytesBuff = Convert.FromBase64String(strCryptTxt);

            using (Aes aes__1 = Aes.Create())
            {
                Rfc2898DeriveBytes crypto = new Rfc2898DeriveBytes(strKey, new byte[] {
                0x49,
                0x76,
                0x61,
                0x6e,
                0x20,
                0x4d,
                0x65,
                0x64,
                0x76,
                0x65,
                0x64,
                0x65,
                0x76
            });
                aes__1.Key = crypto.GetBytes(32);
                aes__1.IV = crypto.GetBytes(16);

                using (MemoryStream mStream = new MemoryStream())
                {
                    using (CryptoStream cStream = new CryptoStream(mStream, aes__1.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cStream.Write(bytesBuff, 0, bytesBuff.Length);
                        cStream.Close();
                    }

                    strCryptTxt = Encoding.Unicode.GetString(mStream.ToArray());
                }
            }
            return strCryptTxt;
        }

        public string readUserId(string filepath)
        {
            string userId = "";            
            inif = new INIFile(filepath);
            string encUserId = inif.IniReadValue("credentials", "MyUserId");
            if (encUserId != "XXXXXX")
            {
                userId = DecryptData(encUserId, "IrdsalgoTrader");
            }
            else
            {
                userId = encUserId;
            }           
            return userId;
        }

        public List<string> ReadCompositeAccountNumbers(string path)
        {
            List<string> accountNumbers = new List<string>();
            List<string> values = new List<string>();
            inif = new INIFile(path);
            values = inif.GetKeyValues("CompositeAccountNumbers");
            if(values.Count >0)
            {
                string line = values.ElementAt(0);
                string[] accountNo = line.Split(',');
                accountNumbers = accountNo.ToList();
            }
            return accountNumbers;
        }
        //IRDSPM::pratiksha::17-06-2021::For zerodha accounts reading
        public List<string> ReadZerodhaAccountNumbers(string path)
        {
            List<string> accountNumbers = new List<string>();
            List<string> values = new List<string>();
            inif = new INIFile(path);
            values = inif.GetKeyValues("ZerodhaAccountNumbers");
            if (values.Count > 0)
            {
                string line = values.ElementAt(0);
                string[] accountNo = line.Split(',');
                accountNumbers = accountNo.ToList();
            }
            return accountNumbers;
        }
    }
}
