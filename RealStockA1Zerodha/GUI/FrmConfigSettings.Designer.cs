﻿namespace RealStockA1Strategy
{
    partial class FrmConfigSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmConfigSettings));
            this.GBAccountList = new System.Windows.Forms.GroupBox();
            this.AccountList = new System.Windows.Forms.ComboBox();
            this.GBAccountDetails = new System.Windows.Forms.GroupBox();
            this.lblRoot = new System.Windows.Forms.Label();
            this.txtRoot = new System.Windows.Forms.TextBox();
            this.lblUserID = new System.Windows.Forms.Label();
            this.txtUserid = new System.Windows.Forms.TextBox();
            this.lblAppKey = new System.Windows.Forms.Label();
            this.txtAppKey = new System.Windows.Forms.TextBox();
            this.lblSecretKey = new System.Windows.Forms.Label();
            this.txtSecretKey = new System.Windows.Forms.TextBox();
            this.lblMarketAppKey = new System.Windows.Forms.Label();
            this.txtMarketAppKey = new System.Windows.Forms.TextBox();
            this.lblMarketSecretKey = new System.Windows.Forms.Label();
            this.txtMarketSecretKey = new System.Windows.Forms.TextBox();
            this.lblLogin = new System.Windows.Forms.Label();
            this.txtLogin = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.btnNew = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.GBChooseBroker = new System.Windows.Forms.GroupBox();
            this.CompositeTrue = new System.Windows.Forms.RadioButton();
            this.ZerodhaTrue = new System.Windows.Forms.RadioButton();
            this.GBAccountList.SuspendLayout();
            this.GBAccountDetails.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.GBChooseBroker.SuspendLayout();
            this.SuspendLayout();
            // 
            // GBAccountList
            // 
            this.GBAccountList.Controls.Add(this.AccountList);
            this.GBAccountList.Location = new System.Drawing.Point(14, 9);
            this.GBAccountList.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.GBAccountList.Name = "GBAccountList";
            this.GBAccountList.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.GBAccountList.Size = new System.Drawing.Size(479, 71);
            this.GBAccountList.TabIndex = 1;
            this.GBAccountList.TabStop = false;
            this.GBAccountList.Text = "Accounts List";
            // 
            // AccountList
            // 
            this.AccountList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.AccountList.Font = new System.Drawing.Font("Arial", 10F);
            this.AccountList.FormattingEnabled = true;
            this.AccountList.Location = new System.Drawing.Point(17, 27);
            this.AccountList.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.AccountList.Name = "AccountList";
            this.AccountList.Size = new System.Drawing.Size(448, 27);
            this.AccountList.TabIndex = 2;
            this.AccountList.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // GBAccountDetails
            // 
            this.GBAccountDetails.Controls.Add(this.lblRoot);
            this.GBAccountDetails.Controls.Add(this.txtRoot);
            this.GBAccountDetails.Controls.Add(this.lblUserID);
            this.GBAccountDetails.Controls.Add(this.txtUserid);
            this.GBAccountDetails.Controls.Add(this.lblAppKey);
            this.GBAccountDetails.Controls.Add(this.txtAppKey);
            this.GBAccountDetails.Controls.Add(this.lblSecretKey);
            this.GBAccountDetails.Controls.Add(this.txtSecretKey);
            this.GBAccountDetails.Controls.Add(this.lblMarketAppKey);
            this.GBAccountDetails.Controls.Add(this.txtMarketAppKey);
            this.GBAccountDetails.Controls.Add(this.lblMarketSecretKey);
            this.GBAccountDetails.Controls.Add(this.txtMarketSecretKey);
            this.GBAccountDetails.Controls.Add(this.lblLogin);
            this.GBAccountDetails.Controls.Add(this.txtLogin);
            this.GBAccountDetails.Location = new System.Drawing.Point(14, 151);
            this.GBAccountDetails.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.GBAccountDetails.Name = "GBAccountDetails";
            this.GBAccountDetails.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.GBAccountDetails.Size = new System.Drawing.Size(479, 305);
            this.GBAccountDetails.TabIndex = 6;
            this.GBAccountDetails.TabStop = false;
            this.GBAccountDetails.Text = "Account Details";
            // 
            // lblRoot
            // 
            this.lblRoot.AutoSize = true;
            this.lblRoot.Font = new System.Drawing.Font("Arial", 10F);
            this.lblRoot.Location = new System.Drawing.Point(13, 30);
            this.lblRoot.Name = "lblRoot";
            this.lblRoot.Size = new System.Drawing.Size(42, 19);
            this.lblRoot.TabIndex = 9;
            this.lblRoot.Text = "Root";
            // 
            // txtRoot
            // 
            this.txtRoot.Font = new System.Drawing.Font("Arial", 11F);
            this.txtRoot.Location = new System.Drawing.Point(173, 25);
            this.txtRoot.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtRoot.Multiline = true;
            this.txtRoot.Name = "txtRoot";
            this.txtRoot.Size = new System.Drawing.Size(292, 25);
            this.txtRoot.TabIndex = 6;
            // 
            // lblUserID
            // 
            this.lblUserID.AutoSize = true;
            this.lblUserID.Font = new System.Drawing.Font("Arial", 10F);
            this.lblUserID.Location = new System.Drawing.Point(13, 68);
            this.lblUserID.Name = "lblUserID";
            this.lblUserID.Size = new System.Drawing.Size(70, 19);
            this.lblUserID.TabIndex = 7;
            this.lblUserID.Text = "User ID ";
            // 
            // txtUserid
            // 
            this.txtUserid.Font = new System.Drawing.Font("Arial", 11F);
            this.txtUserid.Location = new System.Drawing.Point(173, 65);
            this.txtUserid.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtUserid.Multiline = true;
            this.txtUserid.Name = "txtUserid";
            this.txtUserid.Size = new System.Drawing.Size(292, 25);
            this.txtUserid.TabIndex = 7;
            // 
            // lblAppKey
            // 
            this.lblAppKey.AutoSize = true;
            this.lblAppKey.Font = new System.Drawing.Font("Arial", 10F);
            this.lblAppKey.Location = new System.Drawing.Point(13, 106);
            this.lblAppKey.Name = "lblAppKey";
            this.lblAppKey.Size = new System.Drawing.Size(77, 19);
            this.lblAppKey.TabIndex = 11;
            this.lblAppKey.Text = "App Key ";
            // 
            // txtAppKey
            // 
            this.txtAppKey.Font = new System.Drawing.Font("Arial", 11F);
            this.txtAppKey.Location = new System.Drawing.Point(173, 105);
            this.txtAppKey.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtAppKey.Multiline = true;
            this.txtAppKey.Name = "txtAppKey";
            this.txtAppKey.Size = new System.Drawing.Size(292, 25);
            this.txtAppKey.TabIndex = 8;
            // 
            // lblSecretKey
            // 
            this.lblSecretKey.AutoSize = true;
            this.lblSecretKey.Font = new System.Drawing.Font("Arial", 10F);
            this.lblSecretKey.Location = new System.Drawing.Point(13, 149);
            this.lblSecretKey.Name = "lblSecretKey";
            this.lblSecretKey.Size = new System.Drawing.Size(96, 19);
            this.lblSecretKey.TabIndex = 13;
            this.lblSecretKey.Text = "Secret Key ";
            // 
            // txtSecretKey
            // 
            this.txtSecretKey.Font = new System.Drawing.Font("Arial", 11F);
            this.txtSecretKey.Location = new System.Drawing.Point(173, 145);
            this.txtSecretKey.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtSecretKey.Multiline = true;
            this.txtSecretKey.Name = "txtSecretKey";
            this.txtSecretKey.Size = new System.Drawing.Size(292, 25);
            this.txtSecretKey.TabIndex = 9;
            // 
            // lblMarketAppKey
            // 
            this.lblMarketAppKey.AutoSize = true;
            this.lblMarketAppKey.Font = new System.Drawing.Font("Arial", 10F);
            this.lblMarketAppKey.Location = new System.Drawing.Point(13, 188);
            this.lblMarketAppKey.Name = "lblMarketAppKey";
            this.lblMarketAppKey.Size = new System.Drawing.Size(130, 19);
            this.lblMarketAppKey.TabIndex = 15;
            this.lblMarketAppKey.Text = "Market App Key ";
            // 
            // txtMarketAppKey
            // 
            this.txtMarketAppKey.Font = new System.Drawing.Font("Arial", 11F);
            this.txtMarketAppKey.Location = new System.Drawing.Point(173, 185);
            this.txtMarketAppKey.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtMarketAppKey.Multiline = true;
            this.txtMarketAppKey.Name = "txtMarketAppKey";
            this.txtMarketAppKey.Size = new System.Drawing.Size(292, 25);
            this.txtMarketAppKey.TabIndex = 10;
            // 
            // lblMarketSecretKey
            // 
            this.lblMarketSecretKey.AutoSize = true;
            this.lblMarketSecretKey.Font = new System.Drawing.Font("Arial", 10F);
            this.lblMarketSecretKey.Location = new System.Drawing.Point(13, 228);
            this.lblMarketSecretKey.Name = "lblMarketSecretKey";
            this.lblMarketSecretKey.Size = new System.Drawing.Size(150, 19);
            this.lblMarketSecretKey.TabIndex = 17;
            this.lblMarketSecretKey.Text = "Market Secret Key ";
            // 
            // txtMarketSecretKey
            // 
            this.txtMarketSecretKey.Font = new System.Drawing.Font("Arial", 11F);
            this.txtMarketSecretKey.Location = new System.Drawing.Point(173, 225);
            this.txtMarketSecretKey.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtMarketSecretKey.Multiline = true;
            this.txtMarketSecretKey.Name = "txtMarketSecretKey";
            this.txtMarketSecretKey.Size = new System.Drawing.Size(292, 25);
            this.txtMarketSecretKey.TabIndex = 11;
            // 
            // lblLogin
            // 
            this.lblLogin.AutoSize = true;
            this.lblLogin.Font = new System.Drawing.Font("Arial", 10F);
            this.lblLogin.Location = new System.Drawing.Point(13, 266);
            this.lblLogin.Name = "lblLogin";
            this.lblLogin.Size = new System.Drawing.Size(54, 19);
            this.lblLogin.TabIndex = 19;
            this.lblLogin.Text = "Login ";
            // 
            // txtLogin
            // 
            this.txtLogin.Font = new System.Drawing.Font("Arial", 11F);
            this.txtLogin.Location = new System.Drawing.Point(173, 265);
            this.txtLogin.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtLogin.Multiline = true;
            this.txtLogin.Name = "txtLogin";
            this.txtLogin.Size = new System.Drawing.Size(292, 25);
            this.txtLogin.TabIndex = 12;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.btnNew);
            this.groupBox6.Controls.Add(this.btnDelete);
            this.groupBox6.Controls.Add(this.btnSave);
            this.groupBox6.Controls.Add(this.btnCancel);
            this.groupBox6.Location = new System.Drawing.Point(15, 457);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox6.Size = new System.Drawing.Size(479, 76);
            this.groupBox6.TabIndex = 28;
            this.groupBox6.TabStop = false;
            // 
            // btnNew
            // 
            this.btnNew.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNew.Location = new System.Drawing.Point(15, 24);
            this.btnNew.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(90, 37);
            this.btnNew.TabIndex = 13;
            this.btnNew.Text = "New";
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Location = new System.Drawing.Point(129, 24);
            this.btnDelete.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(90, 37);
            this.btnDelete.TabIndex = 14;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold);
            this.btnSave.Location = new System.Drawing.Point(247, 24);
            this.btnSave.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(90, 37);
            this.btnSave.TabIndex = 15;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Location = new System.Drawing.Point(365, 24);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(90, 37);
            this.btnCancel.TabIndex = 16;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // GBChooseBroker
            // 
            this.GBChooseBroker.Controls.Add(this.CompositeTrue);
            this.GBChooseBroker.Controls.Add(this.ZerodhaTrue);
            this.GBChooseBroker.Location = new System.Drawing.Point(14, 82);
            this.GBChooseBroker.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.GBChooseBroker.Name = "GBChooseBroker";
            this.GBChooseBroker.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.GBChooseBroker.Size = new System.Drawing.Size(479, 67);
            this.GBChooseBroker.TabIndex = 3;
            this.GBChooseBroker.TabStop = false;
            this.GBChooseBroker.Text = "Choose Broker";
            // 
            // CompositeTrue
            // 
            this.CompositeTrue.AutoSize = true;
            this.CompositeTrue.Font = new System.Drawing.Font("Arial", 10F);
            this.CompositeTrue.Location = new System.Drawing.Point(322, 27);
            this.CompositeTrue.Name = "CompositeTrue";
            this.CompositeTrue.Size = new System.Drawing.Size(107, 23);
            this.CompositeTrue.TabIndex = 5;
            this.CompositeTrue.TabStop = true;
            this.CompositeTrue.Text = "Composite";
            this.CompositeTrue.UseVisualStyleBackColor = true;
            this.CompositeTrue.CheckedChanged += new System.EventHandler(this.CompositeTrue_CheckedChanged);
            // 
            // ZerodhaTrue
            // 
            this.ZerodhaTrue.AutoSize = true;
            this.ZerodhaTrue.Font = new System.Drawing.Font("Arial", 10F);
            this.ZerodhaTrue.Location = new System.Drawing.Point(43, 27);
            this.ZerodhaTrue.Name = "ZerodhaTrue";
            this.ZerodhaTrue.Size = new System.Drawing.Size(90, 23);
            this.ZerodhaTrue.TabIndex = 4;
            this.ZerodhaTrue.TabStop = true;
            this.ZerodhaTrue.Text = "Zerodha";
            this.ZerodhaTrue.UseVisualStyleBackColor = true;
            this.ZerodhaTrue.CheckedChanged += new System.EventHandler(this.ZerodhaTrue_CheckedChanged);
            // 
            // FrmConfigSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(515, 544);
            this.Controls.Add(this.GBAccountList);
            this.Controls.Add(this.GBChooseBroker);
            this.Controls.Add(this.GBAccountDetails);
            this.Controls.Add(this.groupBox6);
            this.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "FrmConfigSettings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Account Setting";
            this.Load += new System.EventHandler(this.FrmConfigSettings_Load);
            this.GBAccountList.ResumeLayout(false);
            this.GBAccountDetails.ResumeLayout(false);
            this.GBAccountDetails.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.GBChooseBroker.ResumeLayout(false);
            this.GBChooseBroker.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GBAccountList;
        private System.Windows.Forms.ComboBox AccountList;
        private System.Windows.Forms.GroupBox GBAccountDetails;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.TextBox txtMarketSecretKey;
        private System.Windows.Forms.Label lblMarketSecretKey;
        private System.Windows.Forms.TextBox txtMarketAppKey;
        private System.Windows.Forms.Label lblMarketAppKey;
        private System.Windows.Forms.TextBox txtSecretKey;
        private System.Windows.Forms.Label lblSecretKey;
        private System.Windows.Forms.TextBox txtAppKey;
        private System.Windows.Forms.Label lblAppKey;
        private System.Windows.Forms.TextBox txtRoot;
        private System.Windows.Forms.Label lblRoot;
        private System.Windows.Forms.TextBox txtUserid;
        private System.Windows.Forms.Label lblUserID;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.GroupBox GBChooseBroker;
        private System.Windows.Forms.RadioButton CompositeTrue;
        private System.Windows.Forms.RadioButton ZerodhaTrue;
        private System.Windows.Forms.TextBox txtLogin;
        private System.Windows.Forms.Label lblLogin;
    }
}