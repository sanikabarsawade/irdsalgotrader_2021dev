﻿
using IRDSAlgoOMS;

namespace RealStockA1Strategy
{
    partial class TradeSettingWindowsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        private void InitializeComponent(Logger logger, AlgoOMS obj, TradeSettingWindowsForm tradeObj, string iniChoose)
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AutoTradingForm));
            this.elementHost1 = new System.Windows.Forms.Integration.ElementHost();
            this.tradeSettingWPFForm1 = new RealStockA1Strategy.TradeSettingWPFForm(logger, obj, tradeObj, iniChoose);
            this.SuspendLayout();
            // 
            // elementHost1
            // 
            this.elementHost1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.elementHost1.Location = new System.Drawing.Point(0, 0);
            this.elementHost1.Name = "elementHost1";
            this.elementHost1.Size = new System.Drawing.Size(735,693);
            this.elementHost1.TabIndex = 0;
            this.elementHost1.Text = "elementHost1";
            this.elementHost1.Child = this.tradeSettingWPFForm1;
            // 
            // TradeSettingWindowsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(735, 693);
            this.Controls.Add(this.elementHost1);
            this.Name = "TradeSettingWindowsForm"+ iniChoose;
            this.Text = "Trade Setting " + iniChoose;
            this.MaximizeBox = false;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.TopMost = false;
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Integration.ElementHost elementHost1;
        private TradeSettingWPFForm tradeSettingWPFForm1;
    }
}