﻿using IRDSAlgoOMS;
using RealStockA1Strategy;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MomentumStrategy.GUI
{
    public partial class SymbolSettingDet : Form
    {
        string inisymbolList;
        RealStockA1Strategy.ReadSettings Settings;
        IRDSAlgoOMS.Logger logger = IRDSAlgoOMS.Logger.Instance;
        string title = "RealStockA1 Strategy";
        string message = "";
        MessageBoxButtons buttons = MessageBoxButtons.OK;
        DialogResult dialog;
        List<string> finalExchangeList = new List<string>();
        dynamic ConnectWrapper = null;
        string path = System.AppDomain.CurrentDomain.BaseDirectory + "Configuration";
        public SymbolSettingDet(dynamic kiteConnectWrapper,string userID)
        {
            InitializeComponent();
            try
            {
                ConnectWrapper = kiteConnectWrapper;
                ExchangeList.Items.Add("OPTIONS");
                //IRDSPM::Pratiksha::03-05-2021::To have default value in exchange setting
                //if(ExchangeList.Items.Count == 1)
                //{
                    ExchangeList.SelectedIndex = 0;
                //}
                string[] files = Directory.GetFiles(path, "StrategySetting_*");
                foreach (string fileName in files)
                {
                    string file = Path.GetFileNameWithoutExtension(fileName);
                    AccountList.Items.Add(file.Split('_')[1]);
                }
                //IRDSPM::Pratiksha::28-05-2021::for selected account need to be default selected
                if (AccountList.Items.Contains(userID))
                {
                    AccountList.SelectedItem = userID;
                }
                else
                {
                AccountList.SelectedIndex = 0;
                }
                OrderTypeList.Items.Add("MIS");
                OrderTypeList.Items.Add("NRML");
                ExpiryPeriodList.Items.Add("Weekly");
                ExpiryPeriodList.Items.Add("Monthly");
                //For Call strike multiplier
                for(int i = 1; i < 11; i++)
                {
                    strikepriceCallList.Items.Add(i+"0");
                    strikepriceputList.Items.Add(i+"0");
                }
                //IRDSPM::Pratiksha::20-05-2021::List default selectd and disabled
                strikepriceCallList.SelectedIndex = 0;
                strikepriceputList.SelectedIndex = 0;
                //IRDSPM::PRatiksha::21-05-2021::List should not disabled
                //strikepriceCallList.Enabled = false;
                //strikepriceputList.Enabled = false;
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs", "InitializeComponent : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            int j = 0;
            int counter = 0;
            int k = dataGridView.Rows.Count;
            bool flagSymbolname = false;
            var re = @"^[0-9]{2}[a-zA-Z]{3}[0-9]{2}$";
            INIFile iniObj = new INIFile(inisymbolList);
            DialogResult dialog;
            try
            {
                if (AccountList.SelectedItem.ToString() != "Choose Account")
                {
                    message = "Do you want to save the changes for " + AccountList.SelectedItem.ToString() + "?";
                    buttons = MessageBoxButtons.YesNo;
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                    if (dialog == DialogResult.Yes)
                    {

                        foreach (DataGridViewRow row in dataGridView.Rows)
                        {
                            flagSymbolname = false;
                            if (((row.Cells["SymbolName"].Value.ToString() == null) && (row.Cells["Exchange"].Value.ToString() == null) && (row.Cells["ExpiryDate"].Value.ToString() == null) && (row.Cells["ExpiryPeriod"].Value.ToString() == null) && (row.Cells["OrderType"].Value.ToString() == null) && (row.Cells["MarketLotSize"].Value.ToString() == null) && (row.Cells["TotalNumberofLotSize"].Value.ToString() == null) && (row.Cells["RoundOff"].Value.ToString() == null) && (row.Cells["CallStrikePrice"].Value.ToString() == null) && (row.Cells["PutStrikePrice"].Value.ToString() == null)))
                            {
                            }
                            else
                            {
                                if (((row.Cells["SymbolName"].Value.ToString().Length == 0) || (row.Cells["Exchange"].Value.ToString().Length == 0) || (row.Cells["ExpiryDate"].Value.ToString().Length == 0) || (row.Cells["ExpiryPeriod"].Value.ToString().Length == 0) || (row.Cells["OrderType"].Value.ToString().Length == 0) || (row.Cells["MarketLotSize"].Value.ToString().Length == 0) || (row.Cells["TotalNumberofLotSize"].Value.ToString().Length == 0) || (row.Cells["RoundOff"].Value.ToString().Length == 0) || (row.Cells["CallStrikePrice"].Value.ToString().Length == 0) || (row.Cells["PutStrikePrice"].Value.ToString().Length == 0)))
                                {
                                    message = "Not able to add this because null value is present.";
                                    buttons = MessageBoxButtons.YesNo;
                                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                                    WriteUniquelogs("SymbolSettingLogs", "btnApply_Click : Null value present at :" + row.Cells["SymbolName"].Value.ToString() + row.Cells["Exchange"].Value.ToString() + (row.Cells["ExpiryDate"].Value.ToString().Length == 0) + (row.Cells["ExpiryPeriod"].Value.ToString().Length == 0) + (row.Cells["OrderType"].Value.ToString().Length == 0) + (row.Cells["RoundOff"].Value.ToString().Length == 0) + (row.Cells["CallStrikePrice"].Value.ToString().Length == 0) + (row.Cells["PutStrikePrice"].Value.ToString().Length == 0) + ". Not able to add this value.", MessageType.Informational);
                                }
                                else
                                {
                                    if (row.Cells["SymbolName"].Value.ToString().Length > 0)
                                    {
                                        Match m = Regex.Match(row.Cells["ExpiryDate"].Value.ToString(), re);

                                        if (m.Success)
                                        {
                                            j++;
                                        }
                                        else
                                        {
                                            message = "Not able to add " + row.Cells["SymbolName"].Value.ToString() + " because expiry date is not proper.";
                                            buttons = MessageBoxButtons.OK;
                                            dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                                            WriteUniquelogs("SymbolSettingLogs", "btnApply_Click : " + message, MessageType.Informational);
                                        }
                                    }
                                    else
                                    {
                                        flagSymbolname = false;
                                        message = "Not able to add " + row.Cells["SymbolName"].Value.ToString() + " because symbol name is not proper.";
                                        buttons = MessageBoxButtons.OK;
                                        dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                                        WriteUniquelogs("SymbolSettingLogs", "btnApply_Click : "+ message, MessageType.Informational);
                                    }
                                }
                            }
                        }

                        if (j == k)
                        {
                            if (Settings == null)
                            {
                                Settings = new RealStockA1Strategy.ReadSettings(logger);
                            }
                            iniObj.clearTestingSymbol("TRADINGSYMBOL", null, null, inisymbolList);
                            foreach (DataGridViewRow row in dataGridView.Rows)
                            {
                                Settings.writeINIFileSymbolSetting("TRADINGSYMBOL", row.Cells["SymbolName"].Value.ToString().ToUpper() + "," + (row.Cells["Exchange"].Value.ToString().ToUpper() + "," + (row.Cells["ExpiryDate"].Value.ToString().ToUpper()) + "," + (row.Cells["ExpiryPeriod"].Value.ToString()) + "," + (row.Cells["OrderType"].Value.ToString()) + "," + (row.Cells["MarketLotSize"].Value.ToString()) + "," + (row.Cells["TotalNumberofLotSize"].Value.ToString()) + "," + (row.Cells["RoundOff"].Value.ToString()) + "," + (row.Cells["CallStrikePrice"].Value.ToString()) + "," + (row.Cells["PutStrikePrice"].Value.ToString())), inisymbolList);
                                counter++;
                            }

                            message = "Changes updated successfully!!!";
                            buttons = MessageBoxButtons.OK;
                            dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                            this.Close();
                        }
                        WriteUniquelogs("SymbolSettingLogs", "btnApply_Click : "+ message, MessageType.Informational);
                    }
                    else
                    {
                        this.Close();
                        WriteUniquelogs("SymbolSettingLogs", "btnApply_Click : You have clicked No, no data updated in trade setting.ini file.", MessageType.Informational);
                    }
                }
                else
                {
                    message = "You can not save this changes?";
                    buttons = MessageBoxButtons.OK;
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs", "btnApply_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void btnDeleteselectedrow_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridView.Rows.Count > 0)
                {
                    foreach (DataGridViewRow row in dataGridView.SelectedRows)
                    {
                        //IRDSPM::Pratiksha::04-06-2021::To delete perticular row
                        message = "Are you sure you want to delete the selected symbol?";
                        buttons = MessageBoxButtons.YesNo;
                        dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);

                        if (dialog == DialogResult.Yes)
                        {
                        dataGridView.Rows.RemoveAt(row.Index);
                        WriteUniquelogs("SymbolSettingLogs", "btnDeleteselectedrow_Click : Row deleted at :" + row.Index + " position.", MessageType.Informational);
                            //IRDSPM::Pratiksha::04-06-2021::For clearing details for deleted symbol
                            SymbolList.Text = null;
                            ClearFields();
                            strikepriceCallList.SelectedIndex = 0;
                            strikepriceputList.SelectedIndex = 0;
                            strikepricecallSubList.SelectedIndex = -1;
                            strikepriceputSubList.SelectedIndex = -1;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs", "btnDeleteselectedrow_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }
        public void WriteUniquelogs(string fileName, string message, MessageType msgType)
        {
            try
            {
                SymbolLogs log = new SymbolLogs();
                log.createFile(fileName);
                log.LogMessage(message, MessageType.Informational);
                log = null;
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs", "WriteUniquelogs : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                AddUpdateRowinDataGrid(0);
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs", "btnAdd_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void AddUpdateRowinDataGrid(int choice)
        {
            try
            {
                Boolean found = false;
                if (ExchangeList.SelectedItem != null && SymbolList.SelectedItem != null && ExpirydtList.Text != null && OrderTypeList.SelectedItem != null && ExpiryPeriodList.SelectedItem != null && txtroundoff.Text.ToString().Length > 0 && txtMarketLotSize.Text.ToString().Length > 0 && txtTotalNumberofLotSize.Text.ToString().Length > 0 && strikepricecallSubList.SelectedItem != null && strikepriceputSubList.SelectedItem != null)
            {
                //IRDSPM::Pratiksha::19-05-2021::For validations
                    int strikeCall = Int16.Parse(strikepricecallSubList.SelectedItem.ToString());
                    int strikeput = Int16.Parse(strikepriceputSubList.SelectedItem.ToString());

                    string Decpattern = "^[-+]?[.]?([0-9]+[.]?[0-9]+)$";
                    string Intpattern = "^[0-9]*$";
                    //For roundoff
                    Regex r1 = new Regex(Decpattern);
                    Regex r2 = new Regex(Intpattern);
                    Match rountOffm = r1.Match(txtroundoff.Text.ToString());
                    Match rountOffn = r2.Match(txtroundoff.Text.ToString());

                    if (rountOffm.Success || rountOffn.Success)
                    {
                        //for Market lot size
                        Regex p1 = new Regex(Decpattern);
                        Regex p2 = new Regex(Intpattern);
                        Match MarketLotSizep1 = p1.Match(txtMarketLotSize.Text.ToString());
                        Match MarketLotSizep2 = p2.Match(txtMarketLotSize.Text.ToString());
                        if (MarketLotSizep1.Success || MarketLotSizep2.Success)
                        {
                            //For one lot size
                            Regex p3 = new Regex(Decpattern);
                            Regex p4 = new Regex(Intpattern);
                            Match OneLotSizep1 = p1.Match(txtTotalNumberofLotSize.Text.ToString());
                            Match OneLotSizep2 = p2.Match(txtTotalNumberofLotSize.Text.ToString());
                            if (OneLotSizep1.Success || OneLotSizep2.Success)
                {
                    string exchange = "";
                    if (ExchangeList.SelectedItem.ToString() == "OPTIONS")
                    {
                        exchange = "NFO";
                    }
                    foreach (DataGridViewRow row in this.dataGridView.Rows)
                    {
                                    if ((row.Cells[0].Value.Equals(SymbolList.SelectedItem.ToString())) && (row.Cells[1].Value.Equals(exchange)) && (row.Cells[2].Value.Equals(ExpirydtList.Text.ToString())) && (row.Cells[3].Value.Equals(ExpiryPeriodList.SelectedItem.ToString())) && (row.Cells[4].Value.Equals(OrderTypeList.SelectedItem.ToString())) && (row.Cells[5].Value.Equals(txtMarketLotSize.Text.ToString())) && (row.Cells[6].Value.Equals(txtTotalNumberofLotSize.Text.ToString())) && (row.Cells[7].Value.Equals(txtroundoff.Text.ToString())) && (row.Cells[8].Value.Equals(strikepricecallSubList.SelectedItem.ToString())) && (row.Cells[9].Value.Equals(strikepriceputSubList.SelectedItem.ToString())))
                        {
                            found = true;
                            break;
                        }
                    }

                    if (!found)
                    {
                            if (choice == 0)
                            {
                                        //IRDSPM::Pratiksha::19-05-2021::For strike price
                                        dataGridView.Rows.Add(SymbolList.SelectedItem.ToString(), exchange, ExpirydtList.SelectedItem.ToString(), ExpiryPeriodList.SelectedItem.ToString(), OrderTypeList.SelectedItem.ToString(), txtMarketLotSize.Text, txtTotalNumberofLotSize.Text, txtroundoff.Text, strikepricecallSubList.SelectedItem, strikepriceputSubList.SelectedItem);
                            }
                            else
                            {
                                foreach (DataGridViewRow r in dataGridView.SelectedRows)
                                {
                                    r.Cells["SymbolName"].Value = SymbolList.Text;
                                    if (ExchangeList.Text == "OPTIONS")
                                    {
                                                r.Cells["Exchange"].Value = "NFO";
                                    }
                                    else
                                    {
                                                r.Cells["Exchange"].Value = ExchangeList.Text;
                                    }
                                    r.Cells["ExpiryDate"].Value = ExpirydtList.Text;
                                    r.Cells["ExpiryPeriod"].Value = ExpiryPeriodList.Text;
                                    r.Cells["OrderType"].Value = OrderTypeList.Text;
                                            r.Cells["MarketLotSize"].Value = txtMarketLotSize.Text;
                                        //IRDS::Pratiksha::08-06-2021:: for new field
                                        r.Cells["TotalNumberofLotSize"].Value = txtTotalNumberofLotSize.Text;
                                    r.Cells["RoundOff"].Value = txtroundoff.Text;
                                            //IRDSPM::Pratiksha::19-05-2021::For strike price
                                            r.Cells["CallStrikePrice"].Value = strikepricecallSubList.SelectedItem;
                                            r.Cells["PutStrikePrice"].Value = strikepriceputSubList.SelectedItem;
                                }
                            }
                        WriteUniquelogs("SymbolSettingLogs", "btnAdd_Click : Row added.", MessageType.Informational);
                            SymbolList.SelectedItem = null;
                            //IRDSPM::Pratiksha::30-04-2021::For clearing lists
                            ExpirydtList.Text = null;
                            ExpiryPeriodList.Text = null;
                            OrderTypeList.Text = null;
                            txtroundoff.Text = null;
                                    txtMarketLotSize.Text = null;
                                        txtTotalNumberofLotSize.Text = null;
                                    strikepricecallSubList.SelectedIndex = -1;
                                    strikepriceputSubList.SelectedIndex = -1;
                    }
                    else
                    {
                        message = "This entry is already exist.";
                        buttons = MessageBoxButtons.OK;
                        dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                        WriteUniquelogs("SymbolSettingLogs", "btnAdd_Click : "+ message, MessageType.Informational);
                    }
                }
                else
                {
                                message = "Please enter interger value for One Lot Size.";
                                buttons = MessageBoxButtons.OK;
                                dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                                WriteUniquelogs("SymbolSettingLogs", "btnAdd_Click : " + message, MessageType.Informational);
                            }
                        }
                        else
                        {
                            message = "Please enter interger value for Market Lot Size.";
                        buttons = MessageBoxButtons.OK;
                        dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                        WriteUniquelogs("SymbolSettingLogs", "btnAdd_Click : " + message, MessageType.Informational);
                    }
                }
                else
                {
                        message = "Please enter interger value for Round Off.";
                        buttons = MessageBoxButtons.OK;
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                        WriteUniquelogs("SymbolSettingLogs", "btnAdd_Click : " + message, MessageType.Informational);
                    }
            }
            else
            {
                    string parameter = "";
                    if (ExchangeList.SelectedItem == null) { parameter += "Instruments"; } 
                    if(SymbolList.SelectedItem == null) { parameter += "\nSymbol"; }
                    if (ExpirydtList.SelectedItem == null) { parameter += "\nExpiry Date"; }
                    if (OrderTypeList.SelectedItem == null) { parameter += "\nOrder Type"; }
                    if (ExpiryPeriodList.SelectedItem == null) { parameter += "\nExpiry Period"; }
                    if (txtroundoff.Text.ToString().Length == 0) { parameter += "\nRound Off"; }
                    if (txtMarketLotSize.Text.ToString().Length == 0) { parameter += "\nMarket Lot Size"; }
                    if (txtTotalNumberofLotSize.Text.ToString().Length == 0) { parameter += "\nTotal Number of Lot Size"; }
                //IRDSPM::Pratiksha::19-05-2021::For strike price
                if (strikepricecallSubList.SelectedItem == null) { parameter += "\nCall Strike Multiplier"; }
                if (strikepriceputSubList.SelectedItem == null) { parameter += "\nPut Strike Multiplier."; }
                    message = "Please add Proper values for " + parameter + ".";
                    buttons = MessageBoxButtons.OK;
                dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                    WriteUniquelogs("SymbolSettingLogs", "btnAdd_Click : " + message, MessageType.Informational);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs", "AddUpdateRowinDataGrid : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void SymbolList_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                List<string> finalOptionsList = new List<string>();
                ExpirydtList.Items.Clear();
                //ExpiryPeriodList.Items.Clear();
                if (SymbolList.SelectedItem != null)
                {
                    string symbolname = SymbolList.SelectedItem.ToString();
                    string tablename = ExchangeList.SelectedItem.ToString();
                    finalOptionsList = ConnectWrapper.fetchExpiryDtForOptionSymbol(symbolname, tablename);
                    foreach (string listItem in finalOptionsList)
                    {
                        ExpirydtList.Items.Add(listItem);
                    }
                    strikepriceCallList.SelectedIndex = 0;
                    strikepriceputList.SelectedIndex = 0;
                    strikepricecallSubList.SelectedIndex = 10;
                    strikepriceputSubList.SelectedIndex = 10;
                    ClearFields();
                }
                WriteUniquelogs("SymbolSettingLogs", "SymbolList_SelectedValueChanged : Added expiry dates into list.", MessageType.Informational);
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs", "SymbolList_SelectedValueChanged : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void ExchangeList_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                finalExchangeList = null;
                SymbolList.SelectedItem = null;
                SymbolList.Items.Clear();
                if (ExchangeList.SelectedItem != null)
                {
                    if (ExchangeList.SelectedItem.ToString() == "OPTIONS")
                    {
                        finalExchangeList = ConnectWrapper.GetOptionsSymbols();
                        foreach (string listItem in finalExchangeList)
                        {
                            Trace.WriteLine("Symbol : Combo" + listItem);
                            SymbolList.Items.Add(listItem);
                        }
                    }
                    WriteUniquelogs("SymbolSettingLogs", "ExchangeList_SelectedValueChanged : SymbolList added.", MessageType.Informational);
                }
                else
                {
                    ////ExpiryPeriodList.Items.Clear();
                    WriteUniquelogs("SymbolSettingLogs", "ExchangeList_SelectedValueChanged : ExpiryPeriodList got clear.", MessageType.Informational);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs", "ExchangeList_SelectedValueChanged : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void AccountList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (AccountList.SelectedItem.ToString() != "Choose Account")
                {
                    SymbolList.Text = null;
                    ClearFields();
                    dataGridView.Rows.Clear();
                    FetchDetailsFromINI(AccountList.SelectedItem.ToString());
                    WriteUniquelogs("SymbolSettingLogs", "AccountList_SelectedIndexChanged : Fetched details properly.", MessageType.Informational);
                }
                else
                {
                    GPAddDet.Enabled = false;
                    dataGridView.Rows.Clear();
                    WriteUniquelogs("SymbolSettingLogs", "AccountList_SelectedIndexChanged : dataGridView got clear.", MessageType.Informational);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs", "AccountList_SelectedIndexChanged : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void ClearFields()
        {
            try
            {
                ExpirydtList.Text = null;
                ExpiryPeriodList.Text = null;
                OrderTypeList.Text = null;
                txtroundoff.Text = null;
                txtMarketLotSize.Text = null;
                txtTotalNumberofLotSize.Text = null;
                GPAddDet.Enabled = true;
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs", "ClearFields : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void FetchDetailsFromINI(string Path)
        {
            try
            {
                string filePath = path + @"\StrategySetting_" + Path + ".ini";
                inisymbolList = filePath;
                int rank = 0;
                if (File.Exists(inisymbolList))
                {
                    try
                    {
                        if (Settings == null)
                        {
                        Settings = new RealStockA1Strategy.ReadSettings(logger);
                        }
                        //IRDSPM::Pratiksha::29-04-2021::Used existing method change for reading values
                        Settings.ReadTradingSymbols(inisymbolList);
                        foreach (var item in Settings.TradingSymbolsInfoList)
                        {
                            try
                            {
                                string TradingSymbol = item.TradingSymbol;
                                string Exchange = item.Exchange;
                                string ExpiryDate = item.ExpiryDate;
                                string ExpiryPeriod = item.ExpiryPeriod;
                                string OrderType = item.OrderType;
                                double lotsize = item.lotsize;
                                //IRDS::Pratiksha::08-06-2021::Add TotalNoOfLots 
                                int TotalNoOfLots = item.TotalNoOfLots;
                                int Roundoff = item.Roundoff;
                                //IRDSPM::Pratiksha::19-05-2021::For strike price
                                double StrikePricePositive = item.StrikePriceCall;
                                double StrikePriceNegative = item.StrikePricePut;
                                dataGridView.Rows.Add(new object[] { TradingSymbol, Exchange, ExpiryDate, ExpiryPeriod, OrderType, lotsize, TotalNoOfLots, Roundoff, StrikePricePositive, StrikePriceNegative });
                                rank++;
                            }
                            catch (Exception ex)
                            {
                                WriteUniquelogs("SymbolSettingLogs", "FetchDetailsFromINI : Exception Error Message = " + ex.Message, MessageType.Exception);
                            }
                        }
                        dataGridView.AllowUserToAddRows = false;
                    }
                    catch (Exception ex)
                    {
                        WriteUniquelogs("SymbolSettingLogs", "FetchDetailsFromINI : Exception Error Message = " + ex.Message, MessageType.Exception);
                    }
                }
                else
                {
                    WriteUniquelogs("SymbolSettingLogs", "FetchDetailsFromINI : INI file not present.", MessageType.Informational);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs", "FetchDetailsFromINI : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs", "btnCancel_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void dataGridView_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0)
                {
                    DataGridViewRow row = dataGridView.Rows[e.RowIndex];
                    SymbolList.Text = row.Cells[0].Value.ToString();
                    if (row.Cells[1].Value.ToString() == "NFO")
                    {
                        ExchangeList.Text = "OPTIONS";
                    }
                    else
                    {
                        ExchangeList.Text = ExchangeList.Text;
                    }
                    // ExchangeList.Text = row.Cells[1].Value.ToString();
                    ExpirydtList.Text = row.Cells[2].Value.ToString();
                    ExpiryPeriodList.Text = row.Cells[3].Value.ToString();
                    OrderTypeList.Text = row.Cells[4].Value.ToString();
                    txtMarketLotSize.Text = row.Cells[5].Value.ToString();
                    txtTotalNumberofLotSize.Text = row.Cells[6].Value.ToString();
                    txtroundoff.Text = row.Cells[7].Value.ToString();
                    //IRDSPM::Pratiksha::20-05-2021::Clear data from selected list
                    strikepricecallSubList.ResetText();
                    strikepriceputSubList.ResetText();
                    //IRDSPM::Pratikha::07-06-2021::For -1 change by default selected for -10
                    strikepricecallSubList.Text = row.Cells[8].Value.ToString();
                    strikepriceputSubList.Text = row.Cells[9].Value.ToString();
                    //strikepricecallSubList.SelectedText = row.Cells[8].Value.ToString();
                    //strikepriceputSubList.SelectedText = row.Cells[9].Value.ToString();
                    double d = (Convert.ToInt32(row.Cells[9].Value.ToString()) / 10);
                    strikepriceputList.SelectedIndex = Convert.ToInt32(Math.Abs(d));
                    d = (Convert.ToInt32(row.Cells[8].Value.ToString()) / 10);
                    strikepriceCallList.SelectedIndex = Convert.ToInt32(Math.Abs(d));

                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs", "btnCancel_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridView.SelectedRows.Count == 1)
                {
                    AddUpdateRowinDataGrid(1);
                }
                else
                {
                    message = "Please select the row first.";
                    buttons = MessageBoxButtons.OK;
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs", "btnCancel_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void strikepriceCallList_SelectedValueChanged(object sender, EventArgs e)
        {
            strikepricecallSubList.Items.Clear();
            int count = Convert.ToInt32(strikepriceCallList.SelectedItem);
            for(int i = -count; i < 0; i++)
            {
                strikepricecallSubList.Items.Add(i);
            }
            for (int i= 1; i < count+1; i++)
            {
                strikepricecallSubList.Items.Add(i);
            }
        }

        private void strikepriceputList_SelectedValueChanged(object sender, EventArgs e)
        {
            strikepriceputSubList.Items.Clear();
            int count = Convert.ToInt32(strikepriceputList.SelectedItem);
            for (int i = -count; i < 0; i++)
            {
                strikepriceputSubList.Items.Add(i);
            }
            for (int i = 1; i < count + 1; i++)
            {
                strikepriceputSubList.Items.Add(i);
            }
        }

        //IRDS::Pratiksha::07-06-2021::For fetching lotsize
        private void ExpirydtList_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (SymbolList.SelectedItem != null)
                {
                    string symbolname = SymbolList.SelectedItem.ToString();
                    string tablename = ExchangeList.SelectedItem.ToString();
                    string expirydt = ExpirydtList.SelectedItem.ToString();

                    DateTime dt = Convert.ToDateTime(expirydt);
                    DateTime date1 = new DateTime(dt.Year, dt.Month, dt.Day+1);
                    string expiryDate = date1.ToUniversalTime().ToString("yyyy'-'MM'-'dd");
                    string UTCDate = expiryDate.Split(' ')[0] + "T14:30:00";
                    string quantity = ConnectWrapper.fetchLotsizeForOptionSymbol(symbolname, tablename, UTCDate);
                    txtMarketLotSize.Text = null;
                    txtMarketLotSize.Text = quantity;
                }
                WriteUniquelogs("SymbolSettingLogs", "ExpirydtList_SelectedValueChanged : Added expiry dates into list.", MessageType.Informational);
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs", "ExpirydtList_SelectedValueChanged : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }
    }
}
