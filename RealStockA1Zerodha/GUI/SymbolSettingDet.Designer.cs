﻿
namespace MomentumStrategy.GUI
{
    partial class SymbolSettingDet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SymbolSettingDet));
            this.btnDeleteselectedrow = new System.Windows.Forms.Button();
            this.btnApply = new System.Windows.Forms.Button();
            this.GPSymbolDet = new System.Windows.Forms.GroupBox();
            this.GPAccount = new System.Windows.Forms.GroupBox();
            this.lblAccount = new System.Windows.Forms.Label();
            this.AccountList = new System.Windows.Forms.ComboBox();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.SymbolName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Exchange = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ExpiryDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ExpiryPeriod = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OrderType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MarketLotSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalNumberofLotSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RoundOff = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CallStrikePrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PutStrikePrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GPAddDet = new System.Windows.Forms.GroupBox();
            this.pnlTotalNumberofLotSize = new System.Windows.Forms.Panel();
            this.txtTotalNumberofLotSize = new System.Windows.Forms.TextBox();
            this.lblTotalNumberofLotSize = new System.Windows.Forms.Label();
            this.lblRoundOff = new System.Windows.Forms.Label();
            this.lblstrikepriceCall = new System.Windows.Forms.Label();
            this.lblInstruments = new System.Windows.Forms.Label();
            this.ExchangeList = new System.Windows.Forms.ComboBox();
            this.lblSymbol = new System.Windows.Forms.Label();
            this.SymbolList = new System.Windows.Forms.ComboBox();
            this.lblExpiryDate = new System.Windows.Forms.Label();
            this.ExpirydtList = new System.Windows.Forms.ComboBox();
            this.lblExpiryPeriod = new System.Windows.Forms.Label();
            this.ExpiryPeriodList = new System.Windows.Forms.ComboBox();
            this.lblOrdertype = new System.Windows.Forms.Label();
            this.OrderTypeList = new System.Windows.Forms.ComboBox();
            this.lblMarketLotSize = new System.Windows.Forms.Label();
            this.pnlLotSize = new System.Windows.Forms.Panel();
            this.txtMarketLotSize = new System.Windows.Forms.TextBox();
            this.pnlRoundoff = new System.Windows.Forms.Panel();
            this.txtroundoff = new System.Windows.Forms.TextBox();
            this.strikepriceCallList = new System.Windows.Forms.ComboBox();
            this.strikepricecallSubList = new System.Windows.Forms.ComboBox();
            this.lblstrikepriceput = new System.Windows.Forms.Label();
            this.strikepriceputList = new System.Windows.Forms.ComboBox();
            this.strikepriceputSubList = new System.Windows.Forms.ComboBox();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.GPSymbolDet.SuspendLayout();
            this.GPAccount.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.GPAddDet.SuspendLayout();
            this.pnlTotalNumberofLotSize.SuspendLayout();
            this.pnlLotSize.SuspendLayout();
            this.pnlRoundoff.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnDeleteselectedrow
            // 
            this.btnDeleteselectedrow.Font = new System.Drawing.Font("Arial", 10.5F);
            this.btnDeleteselectedrow.Location = new System.Drawing.Point(33, 856);
            this.btnDeleteselectedrow.Name = "btnDeleteselectedrow";
            this.btnDeleteselectedrow.Size = new System.Drawing.Size(213, 40);
            this.btnDeleteselectedrow.TabIndex = 20;
            this.btnDeleteselectedrow.Text = "Delete symbol";
            this.btnDeleteselectedrow.UseVisualStyleBackColor = true;
            this.btnDeleteselectedrow.Click += new System.EventHandler(this.btnDeleteselectedrow_Click);
            // 
            // btnApply
            // 
            this.btnApply.Font = new System.Drawing.Font("Arial", 10.5F);
            this.btnApply.Location = new System.Drawing.Point(656, 856);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(134, 40);
            this.btnApply.TabIndex = 21;
            this.btnApply.Text = "Apply";
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // GPSymbolDet
            // 
            this.GPSymbolDet.Controls.Add(this.GPAccount);
            this.GPSymbolDet.Controls.Add(this.dataGridView);
            this.GPSymbolDet.Controls.Add(this.GPAddDet);
            this.GPSymbolDet.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GPSymbolDet.Location = new System.Drawing.Point(12, 13);
            this.GPSymbolDet.Name = "GPSymbolDet";
            this.GPSymbolDet.Size = new System.Drawing.Size(952, 837);
            this.GPSymbolDet.TabIndex = 1;
            this.GPSymbolDet.TabStop = false;
            this.GPSymbolDet.Text = "Symbol Details";
            // 
            // GPAccount
            // 
            this.GPAccount.Controls.Add(this.lblAccount);
            this.GPAccount.Controls.Add(this.AccountList);
            this.GPAccount.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GPAccount.Location = new System.Drawing.Point(13, 22);
            this.GPAccount.Name = "GPAccount";
            this.GPAccount.Size = new System.Drawing.Size(926, 67);
            this.GPAccount.TabIndex = 0;
            this.GPAccount.TabStop = false;
            this.GPAccount.Text = "Account";
            // 
            // lblAccount
            // 
            this.lblAccount.AutoSize = true;
            this.lblAccount.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblAccount.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblAccount.Location = new System.Drawing.Point(22, 24);
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Size = new System.Drawing.Size(129, 19);
            this.lblAccount.TabIndex = 1;
            this.lblAccount.Text = "Choose Account";
            // 
            // AccountList
            // 
            this.AccountList.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.AccountList.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.AccountList.Font = new System.Drawing.Font("Arial", 12F);
            this.AccountList.FormattingEnabled = true;
            this.AccountList.Location = new System.Drawing.Point(394, 21);
            this.AccountList.Name = "AccountList";
            this.AccountList.Size = new System.Drawing.Size(376, 31);
            this.AccountList.TabIndex = 1;
            this.AccountList.SelectedIndexChanged += new System.EventHandler(this.AccountList_SelectedIndexChanged);
            // 
            // dataGridView
            // 
            this.dataGridView.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SymbolName,
            this.Exchange,
            this.ExpiryDate,
            this.ExpiryPeriod,
            this.OrderType,
            this.MarketLotSize,
            this.TotalNumberofLotSize,
            this.RoundOff,
            this.CallStrikePrice,
            this.PutStrikePrice});
            this.dataGridView.Location = new System.Drawing.Point(14, 508);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.RowHeadersWidth = 51;
            this.dataGridView.RowTemplate.Height = 24;
            this.dataGridView.Size = new System.Drawing.Size(926, 316);
            this.dataGridView.TabIndex = 1;
            this.dataGridView.TabStop = false;
            this.dataGridView.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView_CellMouseClick);
            // 
            // SymbolName
            // 
            this.SymbolName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.SymbolName.HeaderText = "Symbol Name";
            this.SymbolName.MinimumWidth = 6;
            this.SymbolName.Name = "SymbolName";
            this.SymbolName.Width = 113;
            // 
            // Exchange
            // 
            this.Exchange.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Exchange.FillWeight = 90F;
            this.Exchange.HeaderText = "Exchange";
            this.Exchange.MinimumWidth = 6;
            this.Exchange.Name = "Exchange";
            // 
            // ExpiryDate
            // 
            this.ExpiryDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ExpiryDate.HeaderText = "Expiry Date";
            this.ExpiryDate.MinimumWidth = 10;
            this.ExpiryDate.Name = "ExpiryDate";
            // 
            // ExpiryPeriod
            // 
            this.ExpiryPeriod.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ExpiryPeriod.FillWeight = 76.19048F;
            this.ExpiryPeriod.HeaderText = "Expiry Period";
            this.ExpiryPeriod.MinimumWidth = 6;
            this.ExpiryPeriod.Name = "ExpiryPeriod";
            // 
            // OrderType
            // 
            this.OrderType.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.OrderType.FillWeight = 76.19048F;
            this.OrderType.HeaderText = "Order Type";
            this.OrderType.MinimumWidth = 6;
            this.OrderType.Name = "OrderType";
            // 
            // MarketLotSize
            // 
            this.MarketLotSize.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.MarketLotSize.FillWeight = 76.19048F;
            this.MarketLotSize.HeaderText = "Market Lot Size";
            this.MarketLotSize.MinimumWidth = 6;
            this.MarketLotSize.Name = "MarketLotSize";
            // 
            // TotalNumberofLotSize
            // 
            this.TotalNumberofLotSize.FillWeight = 80F;
            this.TotalNumberofLotSize.HeaderText = "Total Number of Lot Size";
            this.TotalNumberofLotSize.MinimumWidth = 6;
            this.TotalNumberofLotSize.Name = "TotalNumberofLotSize";
            this.TotalNumberofLotSize.Width = 80;
            // 
            // RoundOff
            // 
            this.RoundOff.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.RoundOff.FillWeight = 76.19048F;
            this.RoundOff.HeaderText = "Round Off";
            this.RoundOff.MinimumWidth = 6;
            this.RoundOff.Name = "RoundOff";
            // 
            // CallStrikePrice
            // 
            this.CallStrikePrice.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CallStrikePrice.FillWeight = 76.19048F;
            this.CallStrikePrice.HeaderText = "Call Strike Multiplier";
            this.CallStrikePrice.MinimumWidth = 6;
            this.CallStrikePrice.Name = "CallStrikePrice";
            // 
            // PutStrikePrice
            // 
            this.PutStrikePrice.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.PutStrikePrice.FillWeight = 76.19048F;
            this.PutStrikePrice.HeaderText = "Put Strike Multiplier";
            this.PutStrikePrice.MinimumWidth = 6;
            this.PutStrikePrice.Name = "PutStrikePrice";
            // 
            // GPAddDet
            // 
            this.GPAddDet.Controls.Add(this.pnlTotalNumberofLotSize);
            this.GPAddDet.Controls.Add(this.lblTotalNumberofLotSize);
            this.GPAddDet.Controls.Add(this.lblRoundOff);
            this.GPAddDet.Controls.Add(this.lblstrikepriceCall);
            this.GPAddDet.Controls.Add(this.lblInstruments);
            this.GPAddDet.Controls.Add(this.ExchangeList);
            this.GPAddDet.Controls.Add(this.lblSymbol);
            this.GPAddDet.Controls.Add(this.SymbolList);
            this.GPAddDet.Controls.Add(this.lblExpiryDate);
            this.GPAddDet.Controls.Add(this.ExpirydtList);
            this.GPAddDet.Controls.Add(this.lblExpiryPeriod);
            this.GPAddDet.Controls.Add(this.ExpiryPeriodList);
            this.GPAddDet.Controls.Add(this.lblOrdertype);
            this.GPAddDet.Controls.Add(this.OrderTypeList);
            this.GPAddDet.Controls.Add(this.lblMarketLotSize);
            this.GPAddDet.Controls.Add(this.pnlLotSize);
            this.GPAddDet.Controls.Add(this.pnlRoundoff);
            this.GPAddDet.Controls.Add(this.strikepriceCallList);
            this.GPAddDet.Controls.Add(this.strikepricecallSubList);
            this.GPAddDet.Controls.Add(this.lblstrikepriceput);
            this.GPAddDet.Controls.Add(this.strikepriceputList);
            this.GPAddDet.Controls.Add(this.strikepriceputSubList);
            this.GPAddDet.Controls.Add(this.btnUpdate);
            this.GPAddDet.Controls.Add(this.btnAdd);
            this.GPAddDet.Font = new System.Drawing.Font("Arial", 10F);
            this.GPAddDet.Location = new System.Drawing.Point(14, 95);
            this.GPAddDet.Name = "GPAddDet";
            this.GPAddDet.Size = new System.Drawing.Size(926, 407);
            this.GPAddDet.TabIndex = 2;
            this.GPAddDet.TabStop = false;
            this.GPAddDet.Text = "Add details";
            // 
            // pnlTotalNumberofLotSize
            // 
            this.pnlTotalNumberofLotSize.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlTotalNumberofLotSize.Controls.Add(this.txtTotalNumberofLotSize);
            this.pnlTotalNumberofLotSize.Location = new System.Drawing.Point(394, 247);
            this.pnlTotalNumberofLotSize.Name = "pnlTotalNumberofLotSize";
            this.pnlTotalNumberofLotSize.Size = new System.Drawing.Size(376, 31);
            this.pnlTotalNumberofLotSize.TabIndex = 10;
            this.pnlTotalNumberofLotSize.TabStop = true;
            // 
            // txtTotalNumberofLotSize
            // 
            this.txtTotalNumberofLotSize.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTotalNumberofLotSize.Font = new System.Drawing.Font("Arial", 12F);
            this.txtTotalNumberofLotSize.Location = new System.Drawing.Point(6, 3);
            this.txtTotalNumberofLotSize.Name = "txtTotalNumberofLotSize";
            this.txtTotalNumberofLotSize.Size = new System.Drawing.Size(365, 23);
            this.txtTotalNumberofLotSize.TabIndex = 11;
            // 
            // lblTotalNumberofLotSize
            // 
            this.lblTotalNumberofLotSize.AutoSize = true;
            this.lblTotalNumberofLotSize.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblTotalNumberofLotSize.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblTotalNumberofLotSize.Location = new System.Drawing.Point(25, 257);
            this.lblTotalNumberofLotSize.Name = "lblTotalNumberofLotSize";
            this.lblTotalNumberofLotSize.Size = new System.Drawing.Size(186, 19);
            this.lblTotalNumberofLotSize.TabIndex = 19;
            this.lblTotalNumberofLotSize.Text = "Total Number of Lot Size";
            // 
            // lblRoundOff
            // 
            this.lblRoundOff.AutoSize = true;
            this.lblRoundOff.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblRoundOff.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblRoundOff.Location = new System.Drawing.Point(25, 295);
            this.lblRoundOff.Name = "lblRoundOff";
            this.lblRoundOff.Size = new System.Drawing.Size(83, 19);
            this.lblRoundOff.TabIndex = 12;
            this.lblRoundOff.Text = "Round Off";
            // 
            // lblstrikepriceCall
            // 
            this.lblstrikepriceCall.AutoSize = true;
            this.lblstrikepriceCall.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblstrikepriceCall.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblstrikepriceCall.Location = new System.Drawing.Point(25, 333);
            this.lblstrikepriceCall.Name = "lblstrikepriceCall";
            this.lblstrikepriceCall.Size = new System.Drawing.Size(152, 19);
            this.lblstrikepriceCall.TabIndex = 5;
            this.lblstrikepriceCall.Text = "Call Strike Multiplier";
            // 
            // lblInstruments
            // 
            this.lblInstruments.AutoSize = true;
            this.lblInstruments.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblInstruments.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblInstruments.Location = new System.Drawing.Point(25, 29);
            this.lblInstruments.Name = "lblInstruments";
            this.lblInstruments.Size = new System.Drawing.Size(93, 19);
            this.lblInstruments.TabIndex = 18;
            this.lblInstruments.Text = "Instruments";
            // 
            // ExchangeList
            // 
            this.ExchangeList.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ExchangeList.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.ExchangeList.Font = new System.Drawing.Font("Arial", 12F);
            this.ExchangeList.FormattingEnabled = true;
            this.ExchangeList.Location = new System.Drawing.Point(394, 19);
            this.ExchangeList.Name = "ExchangeList";
            this.ExchangeList.Size = new System.Drawing.Size(376, 31);
            this.ExchangeList.TabIndex = 3;
            this.ExchangeList.SelectedValueChanged += new System.EventHandler(this.ExchangeList_SelectedValueChanged);
            // 
            // lblSymbol
            // 
            this.lblSymbol.AutoSize = true;
            this.lblSymbol.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblSymbol.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblSymbol.Location = new System.Drawing.Point(25, 67);
            this.lblSymbol.Name = "lblSymbol";
            this.lblSymbol.Size = new System.Drawing.Size(63, 19);
            this.lblSymbol.TabIndex = 17;
            this.lblSymbol.Text = "Symbol";
            // 
            // SymbolList
            // 
            this.SymbolList.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.SymbolList.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.SymbolList.Font = new System.Drawing.Font("Arial", 12F);
            this.SymbolList.FormattingEnabled = true;
            this.SymbolList.Location = new System.Drawing.Point(394, 57);
            this.SymbolList.Name = "SymbolList";
            this.SymbolList.Size = new System.Drawing.Size(376, 31);
            this.SymbolList.TabIndex = 4;
            this.SymbolList.SelectedValueChanged += new System.EventHandler(this.SymbolList_SelectedValueChanged);
            // 
            // lblExpiryDate
            // 
            this.lblExpiryDate.AutoSize = true;
            this.lblExpiryDate.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblExpiryDate.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblExpiryDate.Location = new System.Drawing.Point(25, 105);
            this.lblExpiryDate.Name = "lblExpiryDate";
            this.lblExpiryDate.Size = new System.Drawing.Size(94, 19);
            this.lblExpiryDate.TabIndex = 16;
            this.lblExpiryDate.Text = "Expiry Date";
            // 
            // ExpirydtList
            // 
            this.ExpirydtList.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ExpirydtList.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.ExpirydtList.Font = new System.Drawing.Font("Arial", 12F);
            this.ExpirydtList.FormattingEnabled = true;
            this.ExpirydtList.Location = new System.Drawing.Point(394, 95);
            this.ExpirydtList.Name = "ExpirydtList";
            this.ExpirydtList.Size = new System.Drawing.Size(376, 31);
            this.ExpirydtList.TabIndex = 5;
            this.ExpirydtList.SelectedValueChanged += new System.EventHandler(this.ExpirydtList_SelectedValueChanged);
            // 
            // lblExpiryPeriod
            // 
            this.lblExpiryPeriod.AutoSize = true;
            this.lblExpiryPeriod.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblExpiryPeriod.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblExpiryPeriod.Location = new System.Drawing.Point(25, 143);
            this.lblExpiryPeriod.Name = "lblExpiryPeriod";
            this.lblExpiryPeriod.Size = new System.Drawing.Size(108, 19);
            this.lblExpiryPeriod.TabIndex = 13;
            this.lblExpiryPeriod.Text = "Expiry Period";
            // 
            // ExpiryPeriodList
            // 
            this.ExpiryPeriodList.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ExpiryPeriodList.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.ExpiryPeriodList.Font = new System.Drawing.Font("Arial", 12F);
            this.ExpiryPeriodList.FormattingEnabled = true;
            this.ExpiryPeriodList.Location = new System.Drawing.Point(394, 133);
            this.ExpiryPeriodList.Name = "ExpiryPeriodList";
            this.ExpiryPeriodList.Size = new System.Drawing.Size(376, 31);
            this.ExpiryPeriodList.TabIndex = 6;
            // 
            // lblOrdertype
            // 
            this.lblOrdertype.AutoSize = true;
            this.lblOrdertype.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblOrdertype.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblOrdertype.Location = new System.Drawing.Point(25, 181);
            this.lblOrdertype.Name = "lblOrdertype";
            this.lblOrdertype.Size = new System.Drawing.Size(91, 19);
            this.lblOrdertype.TabIndex = 9;
            this.lblOrdertype.Text = "Order Type";
            // 
            // OrderTypeList
            // 
            this.OrderTypeList.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.OrderTypeList.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.OrderTypeList.Font = new System.Drawing.Font("Arial", 12F);
            this.OrderTypeList.FormattingEnabled = true;
            this.OrderTypeList.Location = new System.Drawing.Point(394, 171);
            this.OrderTypeList.Name = "OrderTypeList";
            this.OrderTypeList.Size = new System.Drawing.Size(376, 31);
            this.OrderTypeList.TabIndex = 7;
            // 
            // lblMarketLotSize
            // 
            this.lblMarketLotSize.AutoSize = true;
            this.lblMarketLotSize.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblMarketLotSize.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblMarketLotSize.Location = new System.Drawing.Point(25, 219);
            this.lblMarketLotSize.Name = "lblMarketLotSize";
            this.lblMarketLotSize.Size = new System.Drawing.Size(122, 19);
            this.lblMarketLotSize.TabIndex = 8;
            this.lblMarketLotSize.Text = "Market Lot Size";
            // 
            // pnlLotSize
            // 
            this.pnlLotSize.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlLotSize.Controls.Add(this.txtMarketLotSize);
            this.pnlLotSize.Location = new System.Drawing.Point(394, 209);
            this.pnlLotSize.Name = "pnlLotSize";
            this.pnlLotSize.Size = new System.Drawing.Size(376, 31);
            this.pnlLotSize.TabIndex = 8;
            this.pnlLotSize.TabStop = true;
            // 
            // txtMarketLotSize
            // 
            this.txtMarketLotSize.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtMarketLotSize.Font = new System.Drawing.Font("Arial", 12F);
            this.txtMarketLotSize.Location = new System.Drawing.Point(6, 3);
            this.txtMarketLotSize.Name = "txtMarketLotSize";
            this.txtMarketLotSize.Size = new System.Drawing.Size(365, 23);
            this.txtMarketLotSize.TabIndex = 9;
            // 
            // pnlRoundoff
            // 
            this.pnlRoundoff.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlRoundoff.Controls.Add(this.txtroundoff);
            this.pnlRoundoff.Location = new System.Drawing.Point(394, 285);
            this.pnlRoundoff.Name = "pnlRoundoff";
            this.pnlRoundoff.Size = new System.Drawing.Size(376, 31);
            this.pnlRoundoff.TabIndex = 12;
            this.pnlRoundoff.TabStop = true;
            // 
            // txtroundoff
            // 
            this.txtroundoff.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtroundoff.Font = new System.Drawing.Font("Arial", 12F);
            this.txtroundoff.Location = new System.Drawing.Point(4, 3);
            this.txtroundoff.Name = "txtroundoff";
            this.txtroundoff.Size = new System.Drawing.Size(365, 23);
            this.txtroundoff.TabIndex = 13;
            // 
            // strikepriceCallList
            // 
            this.strikepriceCallList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.strikepriceCallList.Font = new System.Drawing.Font("Arial", 12F);
            this.strikepriceCallList.FormattingEnabled = true;
            this.strikepriceCallList.Location = new System.Drawing.Point(279, 323);
            this.strikepriceCallList.Name = "strikepriceCallList";
            this.strikepriceCallList.Size = new System.Drawing.Size(97, 31);
            this.strikepriceCallList.TabIndex = 14;
            this.strikepriceCallList.SelectedValueChanged += new System.EventHandler(this.strikepriceCallList_SelectedValueChanged);
            // 
            // strikepricecallSubList
            // 
            this.strikepricecallSubList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.strikepricecallSubList.Font = new System.Drawing.Font("Arial", 12F);
            this.strikepricecallSubList.FormattingEnabled = true;
            this.strikepricecallSubList.Location = new System.Drawing.Point(394, 323);
            this.strikepricecallSubList.Name = "strikepricecallSubList";
            this.strikepricecallSubList.Size = new System.Drawing.Size(376, 31);
            this.strikepricecallSubList.TabIndex = 15;
            // 
            // lblstrikepriceput
            // 
            this.lblstrikepriceput.AutoSize = true;
            this.lblstrikepriceput.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblstrikepriceput.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblstrikepriceput.Location = new System.Drawing.Point(25, 371);
            this.lblstrikepriceput.Name = "lblstrikepriceput";
            this.lblstrikepriceput.Size = new System.Drawing.Size(149, 19);
            this.lblstrikepriceput.TabIndex = 4;
            this.lblstrikepriceput.Text = "Put Strike Multiplier";
            // 
            // strikepriceputList
            // 
            this.strikepriceputList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.strikepriceputList.Font = new System.Drawing.Font("Arial", 12F);
            this.strikepriceputList.FormattingEnabled = true;
            this.strikepriceputList.Location = new System.Drawing.Point(279, 361);
            this.strikepriceputList.Name = "strikepriceputList";
            this.strikepriceputList.Size = new System.Drawing.Size(97, 31);
            this.strikepriceputList.TabIndex = 16;
            this.strikepriceputList.SelectedValueChanged += new System.EventHandler(this.strikepriceputList_SelectedValueChanged);
            // 
            // strikepriceputSubList
            // 
            this.strikepriceputSubList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.strikepriceputSubList.Font = new System.Drawing.Font("Arial", 12F);
            this.strikepriceputSubList.FormattingEnabled = true;
            this.strikepriceputSubList.Location = new System.Drawing.Point(394, 361);
            this.strikepriceputSubList.Name = "strikepriceputSubList";
            this.strikepriceputSubList.Size = new System.Drawing.Size(376, 31);
            this.strikepriceputSubList.TabIndex = 17;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Font = new System.Drawing.Font("Arial", 10F);
            this.btnUpdate.Location = new System.Drawing.Point(787, 323);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(127, 31);
            this.btnUpdate.TabIndex = 18;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("Arial", 10F);
            this.btnAdd.Location = new System.Drawing.Point(787, 361);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(127, 31);
            this.btnAdd.TabIndex = 19;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("Arial", 10.5F);
            this.btnCancel.Location = new System.Drawing.Point(815, 856);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(134, 40);
            this.btnCancel.TabIndex = 22;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // SymbolSettingDet
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(976, 907);
            this.Controls.Add(this.GPSymbolDet);
            this.Controls.Add(this.btnDeleteselectedrow);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.btnCancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "SymbolSettingDet";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Symbol Setting";
            this.GPSymbolDet.ResumeLayout(false);
            this.GPAccount.ResumeLayout(false);
            this.GPAccount.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.GPAddDet.ResumeLayout(false);
            this.GPAddDet.PerformLayout();
            this.pnlTotalNumberofLotSize.ResumeLayout(false);
            this.pnlTotalNumberofLotSize.PerformLayout();
            this.pnlLotSize.ResumeLayout(false);
            this.pnlLotSize.PerformLayout();
            this.pnlRoundoff.ResumeLayout(false);
            this.pnlRoundoff.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        //private System.Windows.Forms.Button btnAddRow;
        private System.Windows.Forms.ComboBox AccountList;
        private System.Windows.Forms.ComboBox ExchangeList;
        private System.Windows.Forms.ComboBox SymbolList;
        private System.Windows.Forms.ComboBox ExpirydtList;
        private System.Windows.Forms.ComboBox ExpiryPeriodList;
        private System.Windows.Forms.ComboBox OrderTypeList;
        private System.Windows.Forms.TextBox txtMarketLotSize;
        private System.Windows.Forms.TextBox txtroundoff;
        private System.Windows.Forms.ComboBox strikepriceCallList;
        private System.Windows.Forms.ComboBox strikepriceputSubList;
        private System.Windows.Forms.ComboBox strikepriceputList;
        private System.Windows.Forms.ComboBox strikepricecallSubList;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnDeleteselectedrow;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.Button btnCancel;


        private System.Windows.Forms.GroupBox GPSymbolDet;
        private System.Windows.Forms.GroupBox GPAddDet;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.GroupBox GPAccount;
        private System.Windows.Forms.Panel pnlRoundoff;
        private System.Windows.Forms.Panel pnlLotSize;
        private System.Windows.Forms.Label lblRoundOff;
        private System.Windows.Forms.Label lblOrdertype;
        private System.Windows.Forms.Label lblstrikepriceput;
        private System.Windows.Forms.Label lblstrikepriceCall;
        private System.Windows.Forms.Label lblMarketLotSize;
        private System.Windows.Forms.Label lblExpiryPeriod;
        private System.Windows.Forms.Label lblAccount;
        private System.Windows.Forms.Label lblExpiryDate;
        private System.Windows.Forms.Label lblInstruments;
        private System.Windows.Forms.Label lblSymbol;
        private System.Windows.Forms.Label lblTotalNumberofLotSize;
        private System.Windows.Forms.Panel pnlTotalNumberofLotSize;
        private System.Windows.Forms.TextBox txtTotalNumberofLotSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn SymbolName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Exchange;
        private System.Windows.Forms.DataGridViewTextBoxColumn ExpiryDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ExpiryPeriod;
        private System.Windows.Forms.DataGridViewTextBoxColumn OrderType;
        private System.Windows.Forms.DataGridViewTextBoxColumn MarketLotSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalNumberofLotSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn RoundOff;
        private System.Windows.Forms.DataGridViewTextBoxColumn CallStrikePrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn PutStrikePrice;
    }
}