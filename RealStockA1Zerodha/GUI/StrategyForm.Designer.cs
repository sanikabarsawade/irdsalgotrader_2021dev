﻿namespace RealStockA1Strategy
{
    partial class StrategyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StrategyForm));
            this.btnLogin = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnStopCloseOrder = new System.Windows.Forms.Button();
            this.btnTradeSetting = new System.Windows.Forms.Button();
            this.btnSymbosetting = new System.Windows.Forms.Button();
            this.AutomationBox = new System.Windows.Forms.GroupBox();
            this.lblStatusValue = new System.Windows.Forms.Label();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.lblStatusKey = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.groupBox2.SuspendLayout();
            this.AutomationBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnLogin
            // 
            this.btnLogin.Location = new System.Drawing.Point(27, 32);
            this.btnLogin.Margin = new System.Windows.Forms.Padding(4);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(236, 36);
            this.btnLogin.TabIndex = 1;
            this.btnLogin.TabStop = false;
            this.btnLogin.Text = "Login";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnLogin);
            this.groupBox2.Controls.Add(this.btnStopCloseOrder);
            this.groupBox2.Controls.Add(this.btnTradeSetting);
            this.groupBox2.Controls.Add(this.btnSymbosetting);
            this.groupBox2.Location = new System.Drawing.Point(24, 15);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(573, 151);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            // 
            // btnStopCloseOrder
            // 
            this.btnStopCloseOrder.Location = new System.Drawing.Point(291, 32);
            this.btnStopCloseOrder.Margin = new System.Windows.Forms.Padding(4);
            this.btnStopCloseOrder.Name = "btnStopCloseOrder";
            this.btnStopCloseOrder.Size = new System.Drawing.Size(252, 36);
            this.btnStopCloseOrder.TabIndex = 1;
            this.btnStopCloseOrder.Text = "Close All Orders And Stop Trading";
            this.btnStopCloseOrder.UseVisualStyleBackColor = true;
            this.btnStopCloseOrder.Click += new System.EventHandler(this.btnStopCloseOrder_Click_1);
            // 
            // btnTradeSetting
            // 
            this.btnTradeSetting.Location = new System.Drawing.Point(27, 89);
            this.btnTradeSetting.Margin = new System.Windows.Forms.Padding(4);
            this.btnTradeSetting.Name = "btnTradeSetting";
            this.btnTradeSetting.Size = new System.Drawing.Size(236, 36);
            this.btnTradeSetting.TabIndex = 2;
            this.btnTradeSetting.Text = "Trading Setting";
            this.btnTradeSetting.UseVisualStyleBackColor = true;
            this.btnTradeSetting.Click += new System.EventHandler(this.btnTradeSetting_Click);
            // 
            // btnSymbosetting
            // 
            this.btnSymbosetting.Location = new System.Drawing.Point(291, 89);
            this.btnSymbosetting.Margin = new System.Windows.Forms.Padding(4);
            this.btnSymbosetting.Name = "btnSymbosetting";
            this.btnSymbosetting.Size = new System.Drawing.Size(252, 36);
            this.btnSymbosetting.TabIndex = 3;
            this.btnSymbosetting.Text = "Symbol Setting";
            this.btnSymbosetting.UseVisualStyleBackColor = true;
            this.btnSymbosetting.Click += new System.EventHandler(this.btnSymbosetting_Click);
            // 
            // AutomationBox
            // 
            this.AutomationBox.Controls.Add(this.lblStatusValue);
            this.AutomationBox.Controls.Add(this.btnStart);
            this.AutomationBox.Controls.Add(this.btnStop);
            this.AutomationBox.Controls.Add(this.lblStatusKey);
            this.AutomationBox.Location = new System.Drawing.Point(24, 187);
            this.AutomationBox.Margin = new System.Windows.Forms.Padding(4);
            this.AutomationBox.Name = "AutomationBox";
            this.AutomationBox.Padding = new System.Windows.Forms.Padding(4);
            this.AutomationBox.Size = new System.Drawing.Size(573, 149);
            this.AutomationBox.TabIndex = 4;
            this.AutomationBox.TabStop = false;
            this.AutomationBox.Text = "Automation Details";
            // 
            // lblStatusValue
            // 
            this.lblStatusValue.AutoSize = true;
            this.lblStatusValue.BackColor = System.Drawing.Color.Blue;
            this.lblStatusValue.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatusValue.ForeColor = System.Drawing.Color.White;
            this.lblStatusValue.Location = new System.Drawing.Point(316, 92);
            this.lblStatusValue.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblStatusValue.Name = "lblStatusValue";
            this.lblStatusValue.Padding = new System.Windows.Forms.Padding(4);
            this.lblStatusValue.Size = new System.Drawing.Size(75, 31);
            this.lblStatusValue.TabIndex = 12;
            this.lblStatusValue.Text = "Waiting";
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(27, 23);
            this.btnStart.Margin = new System.Windows.Forms.Padding(4);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(236, 36);
            this.btnStart.TabIndex = 5;
            this.btnStart.Text = "Start Automation";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(291, 23);
            this.btnStop.Margin = new System.Windows.Forms.Padding(4);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(252, 36);
            this.btnStop.TabIndex = 6;
            this.btnStop.Text = "Stop Automation";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // lblStatusKey
            // 
            this.lblStatusKey.AutoSize = true;
            this.lblStatusKey.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Bold);
            this.lblStatusKey.Location = new System.Drawing.Point(131, 96);
            this.lblStatusKey.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblStatusKey.Name = "lblStatusKey";
            this.lblStatusKey.Size = new System.Drawing.Size(187, 22);
            this.lblStatusKey.TabIndex = 11;
            this.lblStatusKey.Text = "Automation Status:";
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(411, 354);
            this.btnClose.Margin = new System.Windows.Forms.Padding(4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(156, 36);
            this.btnClose.TabIndex = 7;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // StrategyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(620, 412);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.AutomationBox);
            this.Controls.Add(this.btnClose);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "StrategyForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RealStockA1 Strategy";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.StrategyForm_FormClosing);
            this.Load += new System.EventHandler(this.StrategyForm_Load);
            this.groupBox2.ResumeLayout(false);
            this.AutomationBox.ResumeLayout(false);
            this.AutomationBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox AutomationBox;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Label lblStatusValue;
        private System.Windows.Forms.Label lblStatusKey;
        private System.Windows.Forms.Button btnStopCloseOrder;
        private System.Windows.Forms.Button btnSymbosetting;
        private System.Windows.Forms.Button btnTradeSetting;
        private System.Windows.Forms.Button btnClose;
    }
}

