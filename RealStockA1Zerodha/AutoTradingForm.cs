﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using IRDSAlgoOMS;

namespace RealStockA1Strategy
{
    public partial class AutoTradingForm : Form
    {
        AlgoOMS AlgoOMS = null;
        public int m_StrategyCount = 1;
        Dictionary<string, StrategyForm> m_StrategyObjects = null;
        string m_StrategyName = "RealStockA1Strategy_";
        bool isZerodha = false;
        bool isComposite = true;
        List<string> m_CompositeAccountNumbers = new List<string>();
        List<string> m_ZerodhaAccountNumbers = new List<string>();
        ReadSettings m_ReadSettings = new ReadSettings(null);
        static string m_Path = Directory.GetCurrentDirectory();
        string m_FilePath = m_Path + @"\Configuration\AccountNumber.ini";
        public AutoTradingForm()
        {
            InitializeComponent();
            NetworkChange.NetworkAvailabilityChanged +=
             new NetworkAvailabilityChangedEventHandler(OnNetworkAvailabilityChanged);

            m_StrategyObjects = new Dictionary<string, StrategyForm>();
            AlgoOMS = new AlgoOMS();
            ReadAccountNumbers();
        }

        public void ReadAccountNumbers()
        {
            if (m_ReadSettings == null)
            {
                m_ReadSettings = new ReadSettings(null);
            }
            if (File.Exists(m_FilePath))
            {
                m_CompositeAccountNumbers = m_ReadSettings.ReadCompositeAccountNumbers(m_FilePath);
                m_ZerodhaAccountNumbers = m_ReadSettings.ReadZerodhaAccountNumbers(m_FilePath);
            }
            else
            {
                MessageBox.Show("AccountNumber.ini file does not exist");
            }
        }


        static void OnNetworkAvailabilityChanged(object sender, NetworkAvailabilityEventArgs e)
        {
            if (e.IsAvailable)
            {
                Console.WriteLine("Network has become available");
            }
            else
            {
                bool flag = false;
                try
                {
                    using (var client = new WebClient())
                    using (client.OpenRead("http://clients3.google.com/generate_204"))
                    {
                        flag = true;
                    }
                }
                catch
                {
                    flag = false;
                }
                if (!flag)
                {
                    MessageBox.Show("Please check internet connection!!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

            }
        }

        private void btnRealStockStrategy1_Click(object sender, EventArgs e)
        {
            ReadAccountNumbers();
            string brokerName = GetBroker();
            if (Application.OpenForms["RealStockA1 Strategy 1"] == null)
            {
                string path = Directory.GetCurrentDirectory();
                //sanika::14-Jun-2021::Added for dynamic userid
                if (brokerName == "Composite")
                {
                    string accountNumber = m_CompositeAccountNumbers.ElementAt(0);
                    WriteUniquelogs("StrategySelection", "Clicked on 1st strategy accountNumber " + accountNumber, MessageType.Informational);
                    string iniFile1 = path + @"\Configuration\" + "CompositeSettings_" + accountNumber + ".ini";
                    ReadSettings Settings = new ReadSettings(null, AlgoOMS);
                    string userid = Settings.readUserId(iniFile1);
                    //sanika::14-Jun-2021::Added for dynamic file name and added message box if ini file not found
                    if (File.Exists(iniFile1))
                    {
                        if (Application.OpenForms["RealStockA1 Strategy 1"] == null)
                        {
                            StrategyForm obj = new StrategyForm("1", AlgoOMS, brokerName, userid);
                            obj.Text = "RealStockA1 Strategy 1";
                            obj.Name = "RealStockA1 Strategy 1";
                            obj.Show();
                        }
                    }
                    else
                    {
                        WriteUniquelogs("StrategySelection", "INI file not found", MessageType.Informational);
                        MessageBox.Show("Account not set on strategy 1");
                    }
                }
                else if (brokerName == "Zerodha")
                {
                    string accountNumber = m_ZerodhaAccountNumbers.ElementAt(0);
                    WriteUniquelogs("StrategySelection", "Clicked on 1st strategy accountNumber " + accountNumber, MessageType.Informational);
                    string iniFile1 = path + @"\Configuration\" + "ZerodhaSettings_" + accountNumber + ".ini";
                    ReadSettings Settings = new ReadSettings(null, AlgoOMS);
                    string userid = Settings.readUserId(iniFile1);
                    //sanika::14-Jun-2021::Added for dynamic file name and added message box if ini file not found
                    if (File.Exists(iniFile1))
                    {
                        if (Application.OpenForms["RealStockA1 Strategy 1"] == null)
                        {
                            StrategyForm obj = new StrategyForm("1", AlgoOMS, brokerName, userid);
                            obj.Text = "RealStockA1 Strategy 1";
                            obj.Name = "RealStockA1 Strategy 1";
                            obj.Show();
                        }
                    }   
                    else
                    {
                        WriteUniquelogs("StrategySelection", "INI file not found", MessageType.Informational);
                        MessageBox.Show("Account not set on strategy 1");
                    }
                }
            }
        }

        private void btnRealStockStrategy2_Click(object sender, EventArgs e)
        {
            ReadAccountNumbers();
            bool needtoProceed = false;
            string brokerName = GetBroker();
            if (brokerName == "Composite")
            {
                if (Application.OpenForms["RealStockA1 Strategy 2"] == null)
                {
                    string path = Directory.GetCurrentDirectory();
                    //sanika::14-Jun-2021::Added for dynamic userid
                    string accountNumber = "";
                    if (m_CompositeAccountNumbers.Count > 1)
                    {
                        accountNumber = m_CompositeAccountNumbers.ElementAt(1);
                        needtoProceed = true;
                    }
                    else
                    {
                        WriteUniquelogs("StrategySelection", "Other account not found set default account", MessageType.Informational);
                        accountNumber = m_CompositeAccountNumbers.ElementAt(0);
                        string title = "RealStock Strategy";
                        MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                        DialogResult dialog = System.Windows.Forms.MessageBox.Show("No account set on this button so starting automation on " + accountNumber + "\nDo you want to proceed?", title, buttons, MessageBoxIcon.Information);
                        if (dialog == DialogResult.Yes)
                        {
                            needtoProceed = true;
                            WriteUniquelogs("StrategySelection", "Clicked on yes " + accountNumber, MessageType.Informational);
                        }
                        else
                        {
                            needtoProceed = false;
                            WriteUniquelogs("StrategySelection", "Clicked on no " + accountNumber, MessageType.Informational);
                        }
                    }
                    WriteUniquelogs("StrategySelection", "Clicked on 2nd strategy accountNumber " + accountNumber, MessageType.Informational);
                    if (needtoProceed)
                    {
                        string iniFile1 = path + @"\Configuration\" + "CompositeSettings_" + accountNumber + ".ini";
                        ReadSettings Settings = new ReadSettings(null, AlgoOMS);
                        string userid = Settings.readUserId(iniFile1);
                        if (File.Exists(iniFile1))
                        {
                            WriteUniquelogs("StrategySelection", "INI file found", MessageType.Informational);
                            if (Application.OpenForms["RealStockA1 Strategy 2"] == null)
                            {
                                StrategyForm obj = new StrategyForm("2", AlgoOMS, brokerName, userid);
                                obj.Text = "RealStockA1 Strategy 2";
                                obj.Name = "RealStockA1 Strategy 2";
                                obj.Show();
                            }
                        }
                        else
                        {
                            WriteUniquelogs("StrategySelection", "INI file not found", MessageType.Informational);
                            MessageBox.Show("Account not set on strategy 2");
                        }
                    }
                }
            }
            else if (brokerName == "Zerodha")
            {
                if (Application.OpenForms["RealStockA1 Strategy 2"] == null)
                {
                    string path = Directory.GetCurrentDirectory();
                    //sanika::14-Jun-2021::Added for dynamic userid
                    string accountNumber = "";
                    if (m_ZerodhaAccountNumbers.Count > 1)
                    {
                        accountNumber = m_ZerodhaAccountNumbers.ElementAt(1);
                        needtoProceed = true;
                    }
                    else
                    {
                        WriteUniquelogs("StrategySelection", "Other account not found set default account", MessageType.Informational);
                        accountNumber = m_ZerodhaAccountNumbers.ElementAt(0);
                        string title = "RealStock Strategy";
                        MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                        DialogResult dialog = System.Windows.Forms.MessageBox.Show("No account set on this button so starting automation on " + accountNumber + "\nDo you want to proceed?", title, buttons, MessageBoxIcon.Information);
                        if (dialog == DialogResult.Yes)
                        {
                            needtoProceed = true;
                            WriteUniquelogs("StrategySelection", "Clicked on yes " + accountNumber, MessageType.Informational);
                        }
                        else
                        {
                            needtoProceed = false;
                            WriteUniquelogs("StrategySelection", "Clicked on no " + accountNumber, MessageType.Informational);
                        }
                    }
                    WriteUniquelogs("StrategySelection", "Clicked on 2nd strategy accountNumber " + accountNumber, MessageType.Informational);
                    if (needtoProceed)
                    {
                        string iniFile1 = path + @"\Configuration\" + "ZerodhaSettings_" + accountNumber + ".ini";
                        ReadSettings Settings = new ReadSettings(null, AlgoOMS);
                        string userid = Settings.readUserId(iniFile1);
                        if (File.Exists(iniFile1))
                        {
                            WriteUniquelogs("StrategySelection", "INI file found", MessageType.Informational);
                            if (Application.OpenForms["RealStockA1 Strategy 2"] == null)
                            {
                                StrategyForm obj = new StrategyForm("2", AlgoOMS, brokerName, userid);
                                obj.Text = "RealStockA1 Strategy 2";
                                obj.Name = "RealStockA1 Strategy 2";
                                obj.Show();
                            }
                        }
                        else
                        {
                            WriteUniquelogs("StrategySelection", "INI file not found", MessageType.Informational);
                            MessageBox.Show("Account not set on strategy 2");
                        }
                    }
                }
            }
        }

        private void btnRealStockStrategy3_Click(object sender, EventArgs e)
        {
            ReadAccountNumbers();
            bool needtoProceed = false;
            string brokerName = GetBroker();
            if (brokerName == "Composite")
            {
                if (Application.OpenForms["RealStockA1 Strategy 3"] == null)
                {
                    string path = Directory.GetCurrentDirectory();
                    //sanika::14-Jun-2021::Added for dynamic userid
                    string accountNumber = "";
                    if (m_CompositeAccountNumbers.Count > 2)
                    {
                        accountNumber = m_CompositeAccountNumbers.ElementAt(2);
                        needtoProceed = true;
                    }
                    else
                    {
                        WriteUniquelogs("StrategySelection", "Other account not found set default account", MessageType.Informational);
                        accountNumber = m_CompositeAccountNumbers.ElementAt(0);
                        string title = "RealStock Strategy";
                        MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                        DialogResult dialog = System.Windows.Forms.MessageBox.Show("No account set on this button so starting automation on " + accountNumber + "\nDo you want to proceed?", title, buttons, MessageBoxIcon.Information);
                        if (dialog == DialogResult.Yes)
                        {
                            needtoProceed = true;
                            WriteUniquelogs("StrategySelection", "Clicked on yes " + accountNumber, MessageType.Informational);
                        }
                        else
                        {
                            needtoProceed = false;
                            WriteUniquelogs("StrategySelection", "Clicked on no " + accountNumber, MessageType.Informational);
                        }
                    }
                    WriteUniquelogs("StrategySelection", "Clicked on 3rd strategy accountNumber " + accountNumber, MessageType.Informational);
                    if (needtoProceed)
                    {
                        string iniFile1 = path + @"\Configuration\" + "CompositeSettings_" + accountNumber + ".ini";
                        ReadSettings Settings = new ReadSettings(null, AlgoOMS);
                        string userid = Settings.readUserId(iniFile1);
                        //sanika::14-Jun-2021::Added for dynamic file name and added message box if ini file not found
                        if (File.Exists(iniFile1))
                        {
                            WriteUniquelogs("StrategySelection", "INI file found", MessageType.Informational);
                            if (Application.OpenForms["RealStockA1 Strategy 3"] == null)
                            {
                                StrategyForm obj = new StrategyForm("3", AlgoOMS, brokerName, userid);
                                obj.Text = "RealStockA1 Strategy 3";
                                obj.Name = "RealStockA1 Strategy 3";
                                obj.Show();
                            }
                        }
                        else
                        {
                            WriteUniquelogs("StrategySelection", "INI file not found", MessageType.Informational);
                            MessageBox.Show("Account not set on strategy 3");
                        }
                    }
                }
            }
            else if (brokerName == "Zerodha")
            {
                if (Application.OpenForms["RealStockA1 Strategy 3"] == null)
                {
                    string path = Directory.GetCurrentDirectory();
                    //sanika::14-Jun-2021::Added for dynamic userid
                    string accountNumber = "";
                    if (m_ZerodhaAccountNumbers.Count > 2)
                    {
                        accountNumber = m_ZerodhaAccountNumbers.ElementAt(2);
                        needtoProceed = true;
                    }
                    else
                    {
                        WriteUniquelogs("StrategySelection", "Other account not found set default account", MessageType.Informational);
                        accountNumber = m_ZerodhaAccountNumbers.ElementAt(0);
                        string title = "RealStock Strategy";
                        MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                        DialogResult dialog = System.Windows.Forms.MessageBox.Show("No account set on this button so starting automation on " + accountNumber + "\nDo you want to proceed?", title, buttons, MessageBoxIcon.Information);
                        if (dialog == DialogResult.Yes)
                        {
                            needtoProceed = true;
                            WriteUniquelogs("StrategySelection", "Clicked on yes " + accountNumber, MessageType.Informational);
                        }
                        else
                        {
                            needtoProceed = false;
                            WriteUniquelogs("StrategySelection", "Clicked on no " + accountNumber, MessageType.Informational);
                        }
                    }
                    WriteUniquelogs("StrategySelection", "Clicked on 3rd strategy accountNumber " + accountNumber, MessageType.Informational);
                    if (needtoProceed)
                    {
                        string iniFile1 = path + @"\Configuration\" + "ZerodhaSettings_" + accountNumber + ".ini";
                        ReadSettings Settings = new ReadSettings(null, AlgoOMS);
                        string userid = Settings.readUserId(iniFile1);
                        //sanika::14-Jun-2021::Added for dynamic file name and added message box if ini file not found
                        if (File.Exists(iniFile1))
                        {
                            WriteUniquelogs("StrategySelection", "INI file found", MessageType.Informational);
                            if (Application.OpenForms["RealStockA1 Strategy 3"] == null)
                            {
                                StrategyForm obj = new StrategyForm("3", AlgoOMS, brokerName, userid);
                                obj.Text = "RealStockA1 Strategy 3";
                                obj.Name = "RealStockA1 Strategy 3";
                                obj.Show();
                            }
                        }
                        else
                        {
                            WriteUniquelogs("StrategySelection", "INI file not found", MessageType.Informational);
                            MessageBox.Show("Account not set on strategy 3");
                        }
                    }
                }
            }
        }

        private void btnRealStockStrategy4_Click(object sender, EventArgs e)
        {
            ReadAccountNumbers();
            bool needtoProceed = false;
            string brokerName = GetBroker();
            if (brokerName == "Composite")
            {
                if (Application.OpenForms["RealStockA1 Strategy 4"] == null)
                {
                    string path = Directory.GetCurrentDirectory();
                    //sanika::14-Jun-2021::Added for dynamic userid
                    string accountNumber = "";
                    if (m_CompositeAccountNumbers.Count > 3)
                    {
                        accountNumber = m_CompositeAccountNumbers.ElementAt(3);
                        needtoProceed = true;
                    }
                    else
                    {
                        WriteUniquelogs("StrategySelection", "Other account not found set default account", MessageType.Informational);
                        accountNumber = m_CompositeAccountNumbers.ElementAt(0);
                        string title = "RealStock Strategy";
                        MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                        DialogResult dialog = System.Windows.Forms.MessageBox.Show("No account set on this button so starting automation on " + accountNumber + "\nDo you want to proceed?", title, buttons, MessageBoxIcon.Information);
                        if (dialog == DialogResult.Yes)
                        {
                            needtoProceed = true;
                            WriteUniquelogs("StrategySelection", "Clicked on yes " + accountNumber, MessageType.Informational);
                        }
                        else
                        {
                            needtoProceed = false;
                            WriteUniquelogs("StrategySelection", "Clicked on no " + accountNumber, MessageType.Informational);
                        }

                    }
                    WriteUniquelogs("StrategySelection", "Clicked on 4th strategy accountNumber " + accountNumber, MessageType.Informational);
                    if (needtoProceed)
                    {
                        string iniFile1 = path + @"\Configuration\" + "CompositeSettings_" + accountNumber + ".ini";
                        ReadSettings Settings = new ReadSettings(null, AlgoOMS);
                        string userid = Settings.readUserId(iniFile1);
                        //sanika::14-Jun-2021::Added for dynamic file name and added message box if ini file not found
                        if (File.Exists(iniFile1))
                        {
                            WriteUniquelogs("StrategySelection", "INI file found", MessageType.Informational);
                            if (Application.OpenForms["RealStockA1 Strategy 4"] == null)
                            {
                                StrategyForm obj = new StrategyForm("4", AlgoOMS, brokerName, userid);
                                obj.Text = "RealStockA1 Strategy 4";
                                obj.Name = "RealStockA1 Strategy 4";
                                obj.Show();
                            }
                        }
                        else
                        {
                            WriteUniquelogs("StrategySelection", "INI file not found", MessageType.Informational);
                            MessageBox.Show("Account not set on strategy 4");
                        }
                    }
                }
            }
            else if (brokerName == "Zerodha")
            {
                string path = Directory.GetCurrentDirectory();
                //sanika::14-Jun-2021::Added for dynamic userid
                string accountNumber = "";
                if (m_ZerodhaAccountNumbers.Count > 3)
                {
                    accountNumber = m_ZerodhaAccountNumbers.ElementAt(3);
                    needtoProceed = true;
                }
                else
                {
                    WriteUniquelogs("StrategySelection", "Other account not found set default account", MessageType.Informational);
                    accountNumber = m_ZerodhaAccountNumbers.ElementAt(0);
                    string title = "RealStock Strategy";
                    MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                    DialogResult dialog = System.Windows.Forms.MessageBox.Show("No account set on this button so starting automation on " + accountNumber + "\nDo you want to proceed?", title, buttons, MessageBoxIcon.Information);
                    if (dialog == DialogResult.Yes)
                    {
                        needtoProceed = true;
                        WriteUniquelogs("StrategySelection", "Clicked on yes " + accountNumber, MessageType.Informational);
                    }
                    else
                    {
                        needtoProceed = false;
                        WriteUniquelogs("StrategySelection", "Clicked on no " + accountNumber, MessageType.Informational);
                    }

                }
                WriteUniquelogs("StrategySelection", "Clicked on 4th strategy accountNumber " + accountNumber, MessageType.Informational);
                if (needtoProceed)
                {
                    string iniFile1 = path + @"\Configuration\" + "ZerodhaSettings_" + accountNumber + ".ini";
                    ReadSettings Settings = new ReadSettings(null, AlgoOMS);
                    string userid = Settings.readUserId(iniFile1);
                    //sanika::14-Jun-2021::Added for dynamic file name and added message box if ini file not found
                    if (File.Exists(iniFile1))
                    {
                        WriteUniquelogs("StrategySelection", "INI file found", MessageType.Informational);
                        if (Application.OpenForms["RealStockA1 Strategy 4"] == null)
                        {
                            StrategyForm obj = new StrategyForm("4", AlgoOMS, brokerName, userid);
                            obj.Text = "RealStockA1 Strategy 4";
                            obj.Name = "RealStockA1 Strategy 4";
                            obj.Show();
                        }
                    }
                    else
                    {
                        WriteUniquelogs("StrategySelection", "INI file not found", MessageType.Informational);
                        MessageBox.Show("Account not set on strategy 4");
                    }
                }
            }
        }

        private void btnRealStockStrategy5_Click(object sender, EventArgs e)
        {
            ReadAccountNumbers();
            bool needtoProceed = false;
            string brokerName = GetBroker();
            if (brokerName == "Composite")
            {
                if (Application.OpenForms["RealStockA1 Strategy 5"] == null)
                {
                    string path = Directory.GetCurrentDirectory();
                    //sanika::14-Jun-2021::Added for dynamic userid
                    string accountNumber = "";
                    if (m_CompositeAccountNumbers.Count > 4)
                    {
                        accountNumber = m_CompositeAccountNumbers.ElementAt(4);
                        needtoProceed = true;
                    }
                    else
                    {
                        WriteUniquelogs("StrategySelection", "Other account not found set default account", MessageType.Informational);
                        accountNumber = m_CompositeAccountNumbers.ElementAt(0);
                        string title = "RealStock Strategy";
                        MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                        DialogResult dialog = System.Windows.Forms.MessageBox.Show("No account set on this button so starting automation on " + accountNumber + "\nDo you want to proceed?", title, buttons, MessageBoxIcon.Information);
                        if (dialog == DialogResult.Yes)
                        {
                            needtoProceed = true;
                            WriteUniquelogs("StrategySelection", "Clicked on yes " + accountNumber, MessageType.Informational);
                        }
                        else
                        {
                            needtoProceed = false;
                            WriteUniquelogs("StrategySelection", "Clicked on no " + accountNumber, MessageType.Informational);
                        }
                    }
                    WriteUniquelogs("StrategySelection", "Clicked on 5th strategy accountNumber " + accountNumber, MessageType.Informational);
                    if (needtoProceed)
                    {
                        string iniFile1 = path + @"\Configuration\" + "CompositeSettings_" + accountNumber + ".ini";
                        ReadSettings Settings = new ReadSettings(null, AlgoOMS);
                        string userid = Settings.readUserId(iniFile1);
                        //sanika::14-Jun-2021::Added for dynamic file name and added message box if ini file not found 
                        if (File.Exists(iniFile1))
                        {
                            WriteUniquelogs("StrategySelection", "INI file found", MessageType.Informational);
                            if (Application.OpenForms["RealStockA1 Strategy 5"] == null)
                            {
                                StrategyForm obj = new StrategyForm("5", AlgoOMS, brokerName, userid);
                                obj.Text = "RealStockA1 Strategy 5";
                                obj.Name = "RealStockA1 Strategy 5";
                                obj.Show();
                            }
                        }
                        else
                        {
                            WriteUniquelogs("StrategySelection", "INI file not found", MessageType.Informational);
                            MessageBox.Show("Account not set on strategy 5");
                        }
                    }
                }
            }
            else if (brokerName == "Zerodha")
            {
                if (Application.OpenForms["RealStockA1 Strategy 5"] == null)
                {
                    string path = Directory.GetCurrentDirectory();
                    //sanika::14-Jun-2021::Added for dynamic userid
                    string accountNumber = "";
                    if (m_CompositeAccountNumbers.Count > 4)
                    {
                        accountNumber = m_ZerodhaAccountNumbers.ElementAt(4);
                        needtoProceed = true;
                    }
                    else
                    {
                        WriteUniquelogs("StrategySelection", "Other account not found set default account", MessageType.Informational);
                        accountNumber = m_ZerodhaAccountNumbers.ElementAt(0);
                        string title = "RealStock Strategy";
                        MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                        DialogResult dialog = System.Windows.Forms.MessageBox.Show("No account set on this button so starting automation on " + accountNumber + "\nDo you want to proceed?", title, buttons, MessageBoxIcon.Information);
                        if (dialog == DialogResult.Yes)
                        {
                            needtoProceed = true;
                            WriteUniquelogs("StrategySelection", "Clicked on yes " + accountNumber, MessageType.Informational);
                        }
                        else
                        {
                            needtoProceed = false;
                            WriteUniquelogs("StrategySelection", "Clicked on no " + accountNumber, MessageType.Informational);
                        }
                    }
                    WriteUniquelogs("StrategySelection", "Clicked on 5th strategy accountNumber " + accountNumber, MessageType.Informational);
                    if (needtoProceed)
                    {
                        string iniFile1 = path + @"\Configuration\" + "ZerodhaSettings_" + accountNumber + ".ini";
                        ReadSettings Settings = new ReadSettings(null, AlgoOMS);
                        string userid = Settings.readUserId(iniFile1);
                        //sanika::14-Jun-2021::Added for dynamic file name and added message box if ini file not found 
                        if (File.Exists(iniFile1))
                        {
                            WriteUniquelogs("StrategySelection", "INI file found", MessageType.Informational);
                            if (Application.OpenForms["RealStockA1 Strategy 5"] == null)
                            {
                                StrategyForm obj = new StrategyForm("5", AlgoOMS, brokerName, userid);
                                obj.Text = "RealStockA1 Strategy 5";
                                obj.Name = "RealStockA1 Strategy 5";
                                obj.Show();
                            }
                        }
                        else
                        {
                            WriteUniquelogs("StrategySelection", "INI file not found", MessageType.Informational);
                            MessageBox.Show("Account not set on strategy 5");
                        }
                    }
                }
            }
        }

        private void AutoTradingForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                string message = "Do you really want to exit?";
                //IRDSPM::Pratiksha::29-07-2020::Change the title
                string title = "RealStock Strategy";
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                DialogResult dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                if (dialog == DialogResult.Yes)
                {
                    Environment.Exit(0);
                }
                else
                {
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void rbtnZerodha_CheckedChanged(object sender, EventArgs e)
        {
            isZerodha = true;
            isComposite = false;
        }

        private void rbtnComposite_CheckedChanged(object sender, EventArgs e)
        {
            isZerodha = false;
            isComposite = true;
        }

        public string GetBroker()
        {
            string brokerName = "";
            if (isZerodha)
            {
                brokerName = "Zerodha";
            }
            else if (isComposite)
            {
                brokerName = "Composite";
            }
            return brokerName;
        }
        string userID = "";


        public void loginToAccount()
        {
            btnLogin.Enabled = false;
            Cursor.Current = Cursors.WaitCursor;
            string error = "";
            int loginCount = 0;
            try
            {
                // logger.LogMessage("Clicked on login button", MessageType.Informational);
                AlgoOMS.Connect(userID);
                while (loginCount < 3)
                {
                    if (AlgoOMS.kiteWrapperConnect != null || AlgoOMS.compositeWrapperConnect != null)
                    {
                        if (AlgoOMS.GetLoginStatus(userID))
                        {

                        }
                        break;
                    }
                }
                loginCount++;

            }
            catch (Exception er)
            {
                error = er.Message;
                if (error.Contains("empty") && error.Contains("api_key") && error.Contains("access_token"))
                {
                    //logger.LogMessage("Error : " + er.Message, MessageType.Exception);
                    MessageBox.Show("Error : Please check account credentials", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    // logger.LogMessage("Error : " + er.Message, MessageType.Exception);
                    MessageBox.Show("Error : " + er.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            finally
            {
                if (error.Contains("System.Data.SQLite"))
                {
                    MessageBox.Show("Please add dll for sqlite", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    // logger.LogMessage("Please add dll for sqlite", MessageType.Informational);
                    //bool res = automation.StopThread();
                    this.Close();
                }
            }
            Cursor.Current = Cursors.Default;

        }


        private void accountSettingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Logger logger = Logger.Instance;
            FrmConfigSettings obj = new FrmConfigSettings(logger);
            obj.ShowDialog();
        }

        public void WriteUniquelogs(string symbol, string message, MessageType msgType)
        {
            try
            {
                SymbolLogs log = new SymbolLogs();
                //log = SymbolLogs.InstanceSymbolLogs;
                log.createFile(symbol);
                log.LogMessage(message, MessageType.Informational);
                log = null;
            }
            catch (Exception e)
            {
                Console.WriteLine("WriteUniquelogs : Exception : " + e.Message);
            }
        }
    }
}
