﻿
namespace CryptoStrategyExecutor
{
    partial class SymbolSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SymbolSetting));
            this.btnDeleteselectedrow = new System.Windows.Forms.Button();
            this.btnApply = new System.Windows.Forms.Button();
            this.GPSymbolDet = new System.Windows.Forms.GroupBox();
            this.GPAddDet = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.RBFuture = new System.Windows.Forms.RadioButton();
            this.RBEquity = new System.Windows.Forms.RadioButton();
            this.lblExchange = new System.Windows.Forms.Label();
            this.TradingSymbolList = new System.Windows.Forms.ComboBox();
            this.pnlperiodOfbar = new System.Windows.Forms.Panel();
            this.txtperiodOfbar = new System.Windows.Forms.TextBox();
            this.pnlprofitpercent = new System.Windows.Forms.Panel();
            this.txtprofitpercent = new System.Windows.Forms.TextBox();
            this.pnlstoploss = new System.Windows.Forms.Panel();
            this.txtstoploss = new System.Windows.Forms.TextBox();
            this.pnlNA4 = new System.Windows.Forms.Panel();
            this.txtNA4 = new System.Windows.Forms.TextBox();
            this.btnClearAll = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.lblNA4 = new System.Windows.Forms.Label();
            this.lblstoploss = new System.Windows.Forms.Label();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.TradingSymbol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PeriodOfBar = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProfitPercent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Stoploss = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.profitTrail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lbltradingsymbol = new System.Windows.Forms.Label();
            this.lblperiodOfbar = new System.Windows.Forms.Label();
            this.lblprofitpercent = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnExport = new System.Windows.Forms.Button();
            this.SaveFDExportFile = new System.Windows.Forms.SaveFileDialog();
            this.BtnImport = new System.Windows.Forms.Button();
            this.OpenFDImportFile = new System.Windows.Forms.OpenFileDialog();
            this.GPSymbolDet.SuspendLayout();
            this.GPAddDet.SuspendLayout();
            this.panel1.SuspendLayout();
            this.pnlperiodOfbar.SuspendLayout();
            this.pnlprofitpercent.SuspendLayout();
            this.pnlstoploss.SuspendLayout();
            this.pnlNA4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // btnDeleteselectedrow
            // 
            this.btnDeleteselectedrow.Font = new System.Drawing.Font("Arial", 10.5F);
            this.btnDeleteselectedrow.Location = new System.Drawing.Point(16, 627);
            this.btnDeleteselectedrow.Name = "btnDeleteselectedrow";
            this.btnDeleteselectedrow.Size = new System.Drawing.Size(213, 40);
            this.btnDeleteselectedrow.TabIndex = 12;
            this.btnDeleteselectedrow.Text = "Delete symbol";
            this.btnDeleteselectedrow.UseVisualStyleBackColor = true;
            this.btnDeleteselectedrow.Click += new System.EventHandler(this.btnDeleteselectedrow_Click);
            // 
            // btnApply
            // 
            this.btnApply.Font = new System.Drawing.Font("Arial", 10.5F);
            this.btnApply.Location = new System.Drawing.Point(598, 627);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(134, 40);
            this.btnApply.TabIndex = 13;
            this.btnApply.Text = "Apply";
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // GPSymbolDet
            // 
            this.GPSymbolDet.Controls.Add(this.GPAddDet);
            this.GPSymbolDet.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GPSymbolDet.Location = new System.Drawing.Point(12, 19);
            this.GPSymbolDet.Name = "GPSymbolDet";
            this.GPSymbolDet.Size = new System.Drawing.Size(869, 599);
            this.GPSymbolDet.TabIndex = 1;
            this.GPSymbolDet.TabStop = false;
            this.GPSymbolDet.Text = "Symbol Details";
            // 
            // GPAddDet
            // 
            this.GPAddDet.Controls.Add(this.panel1);
            this.GPAddDet.Controls.Add(this.lblExchange);
            this.GPAddDet.Controls.Add(this.TradingSymbolList);
            this.GPAddDet.Controls.Add(this.pnlperiodOfbar);
            this.GPAddDet.Controls.Add(this.pnlprofitpercent);
            this.GPAddDet.Controls.Add(this.pnlstoploss);
            this.GPAddDet.Controls.Add(this.pnlNA4);
            this.GPAddDet.Controls.Add(this.btnClearAll);
            this.GPAddDet.Controls.Add(this.btnUpdate);
            this.GPAddDet.Controls.Add(this.btnAdd);
            this.GPAddDet.Controls.Add(this.lblNA4);
            this.GPAddDet.Controls.Add(this.lblstoploss);
            this.GPAddDet.Controls.Add(this.dataGridView);
            this.GPAddDet.Controls.Add(this.lbltradingsymbol);
            this.GPAddDet.Controls.Add(this.lblperiodOfbar);
            this.GPAddDet.Controls.Add(this.lblprofitpercent);
            this.GPAddDet.Font = new System.Drawing.Font("Arial", 10F);
            this.GPAddDet.Location = new System.Drawing.Point(12, 23);
            this.GPAddDet.Name = "GPAddDet";
            this.GPAddDet.Size = new System.Drawing.Size(843, 565);
            this.GPAddDet.TabIndex = 2;
            this.GPAddDet.TabStop = false;
            this.GPAddDet.Text = "Add details";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.RBFuture);
            this.panel1.Controls.Add(this.RBEquity);
            this.panel1.Location = new System.Drawing.Point(195, 24);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(506, 35);
            this.panel1.TabIndex = 35;
            // 
            // RBFuture
            // 
            this.RBFuture.AutoSize = true;
            this.RBFuture.Enabled = false;
            this.RBFuture.Location = new System.Drawing.Point(309, 5);
            this.RBFuture.Name = "RBFuture";
            this.RBFuture.Size = new System.Drawing.Size(77, 23);
            this.RBFuture.TabIndex = 1;
            this.RBFuture.Text = "Future";
            this.RBFuture.UseVisualStyleBackColor = true;
            //this.RBFuture.CheckedChanged += new System.EventHandler(this.RBFuture_CheckedChanged);
            // 
            // RBEquity
            // 
            this.RBEquity.AutoSize = true;
            this.RBEquity.Location = new System.Drawing.Point(56, 6);
            this.RBEquity.Name = "RBEquity";
            this.RBEquity.Size = new System.Drawing.Size(76, 23);
            this.RBEquity.TabIndex = 0;
            this.RBEquity.Text = "Equity";
            this.RBEquity.UseVisualStyleBackColor = true;
            this.RBEquity.CheckedChanged += new System.EventHandler(this.RBEquity_CheckedChanged);
            // 
            // lblExchange
            // 
            this.lblExchange.Font = new System.Drawing.Font("Arial", 10F);
            this.lblExchange.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblExchange.Location = new System.Drawing.Point(25, 34);
            this.lblExchange.Name = "lblExchange";
            this.lblExchange.Size = new System.Drawing.Size(110, 19);
            this.lblExchange.TabIndex = 34;
            this.lblExchange.Text = "Exchange";
            // 
            // TradingSymbolList
            // 
            this.TradingSymbolList.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.TradingSymbolList.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.TradingSymbolList.Font = new System.Drawing.Font("Arial", 12F);
            this.TradingSymbolList.FormattingEnabled = true;
            this.TradingSymbolList.Location = new System.Drawing.Point(195, 71);
            this.TradingSymbolList.Name = "TradingSymbolList";
            this.TradingSymbolList.Size = new System.Drawing.Size(506, 31);
            this.TradingSymbolList.TabIndex = 0;
            // 
            // pnlperiodOfbar
            // 
            this.pnlperiodOfbar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlperiodOfbar.Controls.Add(this.txtperiodOfbar);
            this.pnlperiodOfbar.Location = new System.Drawing.Point(195, 112);
            this.pnlperiodOfbar.Name = "pnlperiodOfbar";
            this.pnlperiodOfbar.Size = new System.Drawing.Size(171, 30);
            this.pnlperiodOfbar.TabIndex = 21;
            // 
            // txtperiodOfbar
            // 
            this.txtperiodOfbar.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtperiodOfbar.Font = new System.Drawing.Font("Arial", 11F);
            this.txtperiodOfbar.Location = new System.Drawing.Point(5, 3);
            this.txtperiodOfbar.Name = "txtperiodOfbar";
            this.txtperiodOfbar.Size = new System.Drawing.Size(160, 22);
            this.txtperiodOfbar.TabIndex = 2;
            // 
            // pnlprofitpercent
            // 
            this.pnlprofitpercent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlprofitpercent.Controls.Add(this.txtprofitpercent);
            this.pnlprofitpercent.Location = new System.Drawing.Point(195, 150);
            this.pnlprofitpercent.Name = "pnlprofitpercent";
            this.pnlprofitpercent.Size = new System.Drawing.Size(171, 30);
            this.pnlprofitpercent.TabIndex = 22;
            // 
            // txtprofitpercent
            // 
            this.txtprofitpercent.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtprofitpercent.Font = new System.Drawing.Font("Arial", 11F);
            this.txtprofitpercent.Location = new System.Drawing.Point(5, 3);
            this.txtprofitpercent.Name = "txtprofitpercent";
            this.txtprofitpercent.Size = new System.Drawing.Size(160, 22);
            this.txtprofitpercent.TabIndex = 3;
            // 
            // pnlstoploss
            // 
            this.pnlstoploss.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlstoploss.Controls.Add(this.txtstoploss);
            this.pnlstoploss.Location = new System.Drawing.Point(531, 112);
            this.pnlstoploss.Name = "pnlstoploss";
            this.pnlstoploss.Size = new System.Drawing.Size(171, 30);
            this.pnlstoploss.TabIndex = 26;
            // 
            // txtstoploss
            // 
            this.txtstoploss.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtstoploss.Font = new System.Drawing.Font("Arial", 11F);
            this.txtstoploss.Location = new System.Drawing.Point(5, 3);
            this.txtstoploss.Name = "txtstoploss";
            this.txtstoploss.Size = new System.Drawing.Size(160, 22);
            this.txtstoploss.TabIndex = 5;
            // 
            // pnlNA4
            // 
            this.pnlNA4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlNA4.Controls.Add(this.txtNA4);
            this.pnlNA4.Location = new System.Drawing.Point(531, 152);
            this.pnlNA4.Name = "pnlNA4";
            this.pnlNA4.Size = new System.Drawing.Size(171, 30);
            this.pnlNA4.TabIndex = 31;
            // 
            // txtNA4
            // 
            this.txtNA4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNA4.Font = new System.Drawing.Font("Arial", 11F);
            this.txtNA4.Location = new System.Drawing.Point(5, 3);
            this.txtNA4.Name = "txtNA4";
            this.txtNA4.Size = new System.Drawing.Size(160, 22);
            this.txtNA4.TabIndex = 7;
            // 
            // btnClearAll
            // 
            this.btnClearAll.Font = new System.Drawing.Font("Arial", 10F);
            this.btnClearAll.Location = new System.Drawing.Point(718, 72);
            this.btnClearAll.Name = "btnClearAll";
            this.btnClearAll.Size = new System.Drawing.Size(109, 31);
            this.btnClearAll.TabIndex = 9;
            this.btnClearAll.Text = "Clear All";
            this.btnClearAll.UseVisualStyleBackColor = true;
            this.btnClearAll.Click += new System.EventHandler(this.btnClearAll_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Font = new System.Drawing.Font("Arial", 10F);
            this.btnUpdate.Location = new System.Drawing.Point(718, 112);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(109, 31);
            this.btnUpdate.TabIndex = 10;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("Arial", 10F);
            this.btnAdd.Location = new System.Drawing.Point(718, 151);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(109, 31);
            this.btnAdd.TabIndex = 11;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // lblNA4
            // 
            this.lblNA4.Font = new System.Drawing.Font("Arial", 10F);
            this.lblNA4.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblNA4.Location = new System.Drawing.Point(399, 156);
            this.lblNA4.Name = "lblNA4";
            this.lblNA4.Size = new System.Drawing.Size(129, 19);
            this.lblNA4.TabIndex = 30;
            this.lblNA4.Text = "Profit Trail";
            // 
            // lblstoploss
            // 
            this.lblstoploss.Font = new System.Drawing.Font("Arial", 10F);
            this.lblstoploss.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblstoploss.Location = new System.Drawing.Point(399, 116);
            this.lblstoploss.Name = "lblstoploss";
            this.lblstoploss.Size = new System.Drawing.Size(89, 19);
            this.lblstoploss.TabIndex = 23;
            this.lblstoploss.Text = "Stoploss";
            // 
            // dataGridView
            // 
            this.dataGridView.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TradingSymbol,
            this.PeriodOfBar,
            this.ProfitPercent,
            this.Stoploss,
            this.profitTrail});
            this.dataGridView.Location = new System.Drawing.Point(15, 205);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.ReadOnly = true;
            this.dataGridView.RowHeadersWidth = 51;
            this.dataGridView.RowTemplate.Height = 24;
            this.dataGridView.Size = new System.Drawing.Size(812, 347);
            this.dataGridView.TabIndex = 1;
            this.dataGridView.TabStop = false;
            this.dataGridView.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView_CellMouseClick);
            // 
            // TradingSymbol
            // 
            this.TradingSymbol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.TradingSymbol.HeaderText = "Trading Symbol";
            this.TradingSymbol.MinimumWidth = 6;
            this.TradingSymbol.Name = "TradingSymbol";
            this.TradingSymbol.ReadOnly = true;
            // 
            // PeriodOfBar
            // 
            this.PeriodOfBar.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.PeriodOfBar.HeaderText = "Period of Bar";
            this.PeriodOfBar.MinimumWidth = 10;
            this.PeriodOfBar.Name = "PeriodOfBar";
            this.PeriodOfBar.ReadOnly = true;
            // 
            // ProfitPercent
            // 
            this.ProfitPercent.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ProfitPercent.FillWeight = 76.19048F;
            this.ProfitPercent.HeaderText = "Profit Percent";
            this.ProfitPercent.MinimumWidth = 6;
            this.ProfitPercent.Name = "ProfitPercent";
            this.ProfitPercent.ReadOnly = true;
            // 
            // Stoploss
            // 
            this.Stoploss.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Stoploss.FillWeight = 76.19048F;
            this.Stoploss.HeaderText = "Stoploss";
            this.Stoploss.MinimumWidth = 6;
            this.Stoploss.Name = "Stoploss";
            this.Stoploss.ReadOnly = true;
            // 
            // profitTrail
            // 
            this.profitTrail.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.profitTrail.FillWeight = 76.19048F;
            this.profitTrail.HeaderText = "profit Trail";
            this.profitTrail.MinimumWidth = 6;
            this.profitTrail.Name = "profitTrail";
            this.profitTrail.ReadOnly = true;
            // 
            // lbltradingsymbol
            // 
            this.lbltradingsymbol.Font = new System.Drawing.Font("Arial", 10F);
            this.lbltradingsymbol.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lbltradingsymbol.Location = new System.Drawing.Point(25, 78);
            this.lbltradingsymbol.Name = "lbltradingsymbol";
            this.lbltradingsymbol.Size = new System.Drawing.Size(110, 19);
            this.lbltradingsymbol.TabIndex = 17;
            this.lbltradingsymbol.Text = "Symbol";
            // 
            // lblperiodOfbar
            // 
            this.lblperiodOfbar.Font = new System.Drawing.Font("Arial", 10F);
            this.lblperiodOfbar.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblperiodOfbar.Location = new System.Drawing.Point(25, 116);
            this.lblperiodOfbar.Name = "lblperiodOfbar";
            this.lblperiodOfbar.Size = new System.Drawing.Size(129, 19);
            this.lblperiodOfbar.TabIndex = 13;
            this.lblperiodOfbar.Text = "Period of Bar";
            // 
            // lblprofitpercent
            // 
            this.lblprofitpercent.Font = new System.Drawing.Font("Arial", 10F);
            this.lblprofitpercent.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblprofitpercent.Location = new System.Drawing.Point(25, 156);
            this.lblprofitpercent.Name = "lblprofitpercent";
            this.lblprofitpercent.Size = new System.Drawing.Size(129, 19);
            this.lblprofitpercent.TabIndex = 9;
            this.lblprofitpercent.Text = "Profit Percent";
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("Arial", 10.5F);
            this.btnCancel.Location = new System.Drawing.Point(748, 627);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(134, 40);
            this.btnCancel.TabIndex = 14;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnExport
            // 
            this.btnExport.Font = new System.Drawing.Font("Arial", 10.5F);
            this.btnExport.Location = new System.Drawing.Point(251, 627);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(134, 40);
            this.btnExport.TabIndex = 15;
            this.btnExport.Text = "Export";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // BtnImport
            // 
            this.BtnImport.Font = new System.Drawing.Font("Arial", 10.5F);
            this.BtnImport.Location = new System.Drawing.Point(407, 627);
            this.BtnImport.Name = "BtnImport";
            this.BtnImport.Size = new System.Drawing.Size(134, 40);
            this.BtnImport.TabIndex = 16;
            this.BtnImport.Text = "Import";
            this.BtnImport.UseVisualStyleBackColor = true;
            this.BtnImport.Click += new System.EventHandler(this.BtnImport_Click);
            // 
            // OpenFDImportFile
            // 
            this.OpenFDImportFile.FileName = "openFileDialog1";
            // 
            // SymbolSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(894, 677);
            this.Controls.Add(this.BtnImport);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.GPSymbolDet);
            this.Controls.Add(this.btnDeleteselectedrow);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.btnCancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "SymbolSetting";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Symbol Setting";
            this.GPSymbolDet.ResumeLayout(false);
            this.GPAddDet.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.pnlperiodOfbar.ResumeLayout(false);
            this.pnlperiodOfbar.PerformLayout();
            this.pnlprofitpercent.ResumeLayout(false);
            this.pnlprofitpercent.PerformLayout();
            this.pnlstoploss.ResumeLayout(false);
            this.pnlstoploss.PerformLayout();
            this.pnlNA4.ResumeLayout(false);
            this.pnlNA4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ComboBox TradingSymbolList;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnDeleteselectedrow;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.Button btnCancel;


        private System.Windows.Forms.GroupBox GPSymbolDet;
        private System.Windows.Forms.GroupBox GPAddDet;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.Label lblprofitpercent;
        private System.Windows.Forms.Label lblperiodOfbar;
        private System.Windows.Forms.Label lbltradingsymbol;
        private System.Windows.Forms.Panel pnlprofitpercent;
        private System.Windows.Forms.Panel pnlperiodOfbar;
        private System.Windows.Forms.TextBox txtprofitpercent;
        private System.Windows.Forms.TextBox txtperiodOfbar;
        private System.Windows.Forms.Panel pnlstoploss;
        private System.Windows.Forms.TextBox txtstoploss;
        private System.Windows.Forms.Label lblstoploss;
        private System.Windows.Forms.Panel pnlNA4;
        private System.Windows.Forms.TextBox txtNA4;
        private System.Windows.Forms.Label lblNA4;
        private System.Windows.Forms.DataGridViewTextBoxColumn NA1;
        private System.Windows.Forms.DataGridViewTextBoxColumn NA4;
        private System.Windows.Forms.Button btnClearAll;
        private System.Windows.Forms.Label lblExchange;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton RBFuture;
        private System.Windows.Forms.RadioButton RBEquity;
        private System.Windows.Forms.DataGridViewTextBoxColumn TradingSymbol;
        private System.Windows.Forms.DataGridViewTextBoxColumn PeriodOfBar;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProfitPercent;
        private System.Windows.Forms.DataGridViewTextBoxColumn Stoploss;
        private System.Windows.Forms.DataGridViewTextBoxColumn profitTrail;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.SaveFileDialog SaveFDExportFile;
        private System.Windows.Forms.Button BtnImport;
        private System.Windows.Forms.OpenFileDialog OpenFDImportFile;
    }
}