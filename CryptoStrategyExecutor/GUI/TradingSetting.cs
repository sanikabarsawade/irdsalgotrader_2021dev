﻿using IRDSAlgoOMS;
using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace CryptoStrategyExecutor
{
    public partial class TradingSetting : Form
    {
        [field: NonSerialized()]
        AlgoOMS AlgoOMSObj;
        //sanika::25-sep-2020::added for resume 
        [field: NonSerialized()]
        CryptoStrategyExecutor.ReadSettings readSettings;
        //sanika::25-sep-2020::added for resume 
        [field: NonSerialized()]
        Logger logger;
        string path = System.IO.Directory.GetCurrentDirectory();

        string message = "";
        string title = "RealStockA1 Strategy";
        MessageBoxButtons buttons;
        DialogResult dialog;
        INIFile iniObj = null;
        string userID = null;
        static string ConfigFilePath = System.AppDomain.CurrentDomain.BaseDirectory + "Configuration";

        //26-Jully-2021::Pratiksha::For getting values in Local variables, and for cancel click comparision
        string m_StartTime = "";
        string m_EndTime = " ";
        string m_TimeLimitToPlaceOrder = "";
        string m_TimeForExitAllOpenPosition = "";
        string m_PercentQuantity = "";
        string m_TotalMoney = "";
        string m_BarInterval = "";
        string m_MaxStrategyOrderCount = "";
        string m_IndividualCount = "";
        string m_DaysForHistroricalData = "";
        string m_IndividualMaxLossPercent = "";
        string m_OverallProfitAmt = "";
        string m_TotalOpenPositions = "";
        string m_BarCount = "";
        int m_choice = 0;
        //05-AUG-2021::Pratiksha::Added 2 new fields for start data and end data
        string m_StartDataTime = "";
        string m_EndDataTime = " ";

        public TradingSetting(Logger logger, AlgoOMS obj, int choice)
        {
            InitializeComponent();
            try
            {
                this.AlgoOMSObj = obj;
                this.logger = logger;
                readSettings = new CryptoStrategyExecutor.ReadSettings(this.logger, this.AlgoOMSObj);
                FetchDetailsFromINI(path);
                //02-AUG-2021::Pratiksha::Show fields on the basis of choice
                m_choice = choice;
                if (m_choice == 1)
                {
                    txtStartDataTime.Enabled = false;
                    txtEndDataTime.Enabled = false;
                }
                else if(m_choice == 0)
                {
                    txtBarCount.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("TradingSetting", "InitializeComponent : Exception Error Message = " + ex.Message, MessageType.Informational);
            }
        }

        private void FetchDetailsFromINI(string Path)
        {
            try
            {
                string filePath = Path + @"\Configuration\Crypto_Sandip.ini";
                if (File.Exists(filePath))
                {
                    if (readSettings == null)
                    {
                        readSettings = new CryptoStrategyExecutor.ReadSettings(logger);
                    }
                    readSettings.readCrypto_SandipConfigFile(filePath);
                    m_StartTime = readSettings.m_StartTime;
                    m_EndTime = readSettings.m_EndTime;                    
                    //05-AUG-2021::Pratiksha::Added 2 new fields for start data and end data
                    m_StartDataTime = readSettings.m_StartDataTime;
                    m_EndDataTime = readSettings.m_EndDataTime;
                    m_TimeLimitToPlaceOrder = readSettings.m_TimeLimitToPlaceOrder;
                    m_TimeForExitAllOpenPosition = readSettings.m_TimeForExitAllOpenPosition;
                    m_PercentQuantity = readSettings.m_PercentQuantity.ToString();
                    //02-AUG-2021::Pratiksha::Added new field barcount
                    m_BarCount = readSettings.m_BarCount.ToString();
                    m_TotalMoney = readSettings.m_TotalMoney.ToString();
                    m_BarInterval = readSettings.m_BarInterval.ToString();
                    m_MaxStrategyOrderCount = readSettings.m_MaxStrategyOrderCount.ToString();
                    m_IndividualCount = readSettings.m_IndividualCount.ToString();
                    m_DaysForHistroricalData = readSettings.m_DaysForHistroricalData.ToString();
                    m_IndividualMaxLossPercent = readSettings.m_IndividualMaxLossPercent.ToString();
                    m_OverallProfitAmt = readSettings.m_OverallProfitAmt.ToString();
                    m_TotalOpenPositions = readSettings.m_TotalOpenPositions.ToString();

                    txtStartTime.Value = Convert.ToDateTime(m_StartTime);
                    txtEndTime.Value = Convert.ToDateTime(m_EndTime);
                    //05-AUG-2021::Pratiksha::Added 2 new fields for start data and end data
                    txtStartDataTime.Value = Convert.ToDateTime(m_StartDataTime);
                    txtEndDataTime.Value = Convert.ToDateTime(m_EndDataTime);
                    txtTimeLimitToPlaceOrder.Value = Convert.ToDateTime(m_TimeLimitToPlaceOrder);
                    txtTimeForExitAllOpenPosition.Value = Convert.ToDateTime(m_TimeForExitAllOpenPosition);
                    txtPercentQuantity.Text = m_PercentQuantity;
                    //02-AUG-2021::Pratiksha::Added new field barcount
                    txtBarCount.Text = m_BarCount;
                    txttotalmoney.Text = m_TotalMoney;
                    txtBarInterval.Text = m_BarInterval;
                    txtMaxStrategyOrderCount.Text = m_MaxStrategyOrderCount;
                    txtindiviualCount.Text = m_IndividualCount;
                    txtdaysForHistroricalData.Text = m_DaysForHistroricalData;
                    txtIndividualMaxLossPercent.Text = m_IndividualMaxLossPercent;
                    txtOverallProfitAmt.Text = m_OverallProfitAmt;
                    txtTotalOpenPositions.Text = m_TotalOpenPositions;
                    WriteUniquelogs("TradingSetting", "FetchDetailsFromINI : Reading details from INI file.", MessageType.Informational);
                }
                else
                {
                    Clearfields();
                    WriteUniquelogs("TradingSetting", "FetchDetailsFromINI : INI file not exist.", MessageType.Informational);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("TradingSetting", "FetchDetailsFromINI : Exception Error Message = " + ex.Message, MessageType.Informational);
            }
        }
        private void btnApply_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtStartTime.Text != "" && txtEndTime.Text != "" && txtTimeLimitToPlaceOrder.Text != "" && txtTimeForExitAllOpenPosition.Text != "" && txtPercentQuantity.Text != ""
                    && txttotalmoney.Text != "" && txtBarInterval.Text != "" && txtMaxStrategyOrderCount.Text != "" && txtindiviualCount.Text != "" && txtOverallProfitAmt.Text != "" &&
                    txtdaysForHistroricalData.Text != "" && txtIndividualMaxLossPercent.Text != "" && txtTotalOpenPositions.Text != "" && txtBarCount.Text != "")
                {
                    message = "Do you want to save the changes?";
                    buttons = MessageBoxButtons.YesNo;
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                    if (dialog == DialogResult.Yes)
                    {
                        string filePath = ConfigFilePath + "\\Crypto_Sandip.ini";
                        iniObj = new INIFile(filePath);
                        //02-AUG-2021::Pratiksha::Added one more index
                        string[] key = new string[16];
                        string[] value = new string[16];
                        key[0] = "StartTime";
                        key[1] = "EndTime";
                        key[2] = "TimeLimitToPlaceOrder";
                        key[3] = "TimeForExitAllOpenPosition";
                        key[4] = "PercentQuantity";
                        key[5] = "TotalMoney";
                        key[6] = "BarInterval";
                        key[7] = "MaxStrategyOrderCount";
                        key[8] = "IndividualCount";
                        key[9] = "DaysForHistroricalData";
                        key[10] = "IndividualMaxLossPercent";
                        key[11] = "OverallProfitAmt";
                        key[12] = "TotalOpenPositions";
                        //02-AUG-2021::Pratiksha::Added new field barcount
                        key[13] = "BarCount";                      
                        //05-AUG-2021::Pratiksha::Added 2 new fields for start data and end data
                        key[14] = "StartDataTime";
                        key[15] = "EndDataTime";

                        value[0] = txtStartTime.Text;
                        value[1] = txtEndTime.Text;
                        value[2] = txtTimeLimitToPlaceOrder.Text;
                        value[3] = txtTimeForExitAllOpenPosition.Text;
                        value[4] = txtPercentQuantity.Text;
                        value[5] = txttotalmoney.Text;
                        value[6] = txtBarInterval.Text;
                        value[7] = txtMaxStrategyOrderCount.Text;
                        value[8] = txtindiviualCount.Text;
                        value[9] = txtdaysForHistroricalData.Text;
                        value[10] = txtIndividualMaxLossPercent.Text;
                        value[11] = txtOverallProfitAmt.Text;
                        value[12] = txtTotalOpenPositions.Text;
                        //02-AUG-2021::Pratiksha::Added new field barcount
                        value[13] = txtBarCount.Text;                       
                        //05-AUG-2021::Pratiksha::Added 2 new fields for start data and end data
                        value[14] = txtStartDataTime.Text;
                        value[15] = txtEndDataTime.Text;

                        for (int i = 0; i < 16; i++)
                        {
                            iniObj.clearTestingSymbol("SDATA", key[i], value[i], filePath);
                            WriteUniquelogs("TradingSetting", "btnApply_Click : " + key[i] + " = " + value[i], MessageType.Informational);
                        }
                        message = "Saved changes successfully!!!";
                        buttons = MessageBoxButtons.OK;
                        dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                        if (dialog == DialogResult.OK)
                        {
                            this.Close();
                        }

                        WriteUniquelogs("TradingSetting", "btnApply_Click :  Details saved in ini file.", MessageType.Informational);
                    }
                    else
                    {
                        this.Close();
                        WriteUniquelogs("TradingSetting", "btnApply_Click : You have clicked No, no data updated in trade setting.ini file.", MessageType.Informational);
                    }
                }
                else
                {
                    string parameter = "";
                    if (txtStartTime.Text == "") { parameter += "\nStart Time"; }
                    if (txtEndTime.Text == "") { parameter += "\nEnd Time"; }
                    if (txtStartDataTime.Text == "") { parameter += "\nStart Data Time"; }
                    if (txtEndDataTime.Text == "") { parameter += "\nEnd Data Time"; }
                    if (txtTimeLimitToPlaceOrder.Text == "") { parameter += "\nTime Limit To Place Order"; }
                    if (txtTimeForExitAllOpenPosition.Text == "") { parameter += "\nTime For Exit All Open Position"; }
                    if (txtPercentQuantity.Text == "") { parameter += "\nPercent Quantity"; }
                    //02-AUG-2021::Pratiksha::Added new field barcount
                    if (txtBarCount.Text == "") { parameter += "\nBar Count"; }
                    if (txttotalmoney.Text == "") { parameter += "\nTotal Money"; }
                    if (txtBarInterval.Text == "") { parameter += "\nBar Interval"; }
                    if (txtMaxStrategyOrderCount.Text == "") { parameter += "\nMax Strategy Order Count"; }
                    if (txtindiviualCount.Text == "") { parameter += "\nIndiviual Count"; }
                    if (txtdaysForHistroricalData.Text == "") { parameter += "\nDays For Histrorical Data"; }
                    if (txtIndividualMaxLossPercent.Text == "") { parameter += "\nIndividual Max Loss Percent"; }
                    if (txtOverallProfitAmt.Text == "") { parameter += "\nOverall Profit Amt"; }
                    if (txtTotalOpenPositions.Text == "") { parameter += "\nTotal Open Positions"; }
                    message = "Please enter proper values for following field: " + parameter;
                    buttons = MessageBoxButtons.OK;
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                    WriteUniquelogs("TradingSetting", "btnApply_Click : Please enter proper values.", MessageType.Informational);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("TradingSetting", "btnApply_Click : Exception Error Message = " + ex.Message, MessageType.Informational);
            }
        }
        public void WriteUniquelogs(string symbol, string message, MessageType msgType)
        {
            try
            {
                SymbolLogs log = new SymbolLogs();
                log.createFile(symbol);
                log.LogMessage(message, MessageType.Informational);
                log = null;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : " + e.Message);
                Trace.WriteLine("Exception : " + e.Message);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtStartTime.Text != m_StartTime || txtEndTime.Text != m_EndTime || txtTimeLimitToPlaceOrder.Text != m_TimeLimitToPlaceOrder ||
                    txtTimeForExitAllOpenPosition.Text != m_TimeForExitAllOpenPosition || txtPercentQuantity.Text != m_PercentQuantity
                    || txttotalmoney.Text != m_TotalMoney || txtBarInterval.Text != m_BarInterval || txtMaxStrategyOrderCount.Text != m_MaxStrategyOrderCount
                    || txtindiviualCount.Text != m_IndividualCount || txtOverallProfitAmt.Text != m_OverallProfitAmt ||
                    txtdaysForHistroricalData.Text != m_DaysForHistroricalData || txtIndividualMaxLossPercent.Text != m_IndividualMaxLossPercent || txtTotalOpenPositions.Text != m_TotalOpenPositions
                    || txtBarCount.Text != m_BarCount || txtStartDataTime.Text != m_StartDataTime || txtEndDataTime.Text != m_EndDataTime)
                {
                    message = "You have some unsaved changes, do you want to save those changes?";
                    buttons = MessageBoxButtons.YesNo;
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                    if (dialog == DialogResult.Yes)
                    {
                        if (txtStartTime.Text != "" && txtEndTime.Text != "" && txtTimeLimitToPlaceOrder.Text != "" && txtTimeForExitAllOpenPosition.Text != "" && txtPercentQuantity.Text != ""
    && txttotalmoney.Text != "" && txtBarInterval.Text != "" && txtMaxStrategyOrderCount.Text != "" && txtindiviualCount.Text != "" && txtOverallProfitAmt.Text != "" &&
    txtdaysForHistroricalData.Text != "" && txtIndividualMaxLossPercent.Text != "" && txtTotalOpenPositions.Text != "" && txtBarCount.Text != "" && txtStartDataTime.Text != "" && txtEndDataTime.Text != "")
                        {
                            string filePath = ConfigFilePath + "\\Crypto_Sandip.ini";
                            iniObj = new INIFile(filePath);
                            string[] key = new string[16];
                            string[] value = new string[16];
                            key[0] = "StartTime";
                            key[1] = "EndTime";
                            key[2] = "TimeLimitToPlaceOrder";
                            key[3] = "TimeForExitAllOpenPosition";
                            key[4] = "PercentQuantity";
                            key[5] = "TotalMoney";
                            key[6] = "BarInterval";
                            key[7] = "MaxStrategyOrderCount";
                            key[8] = "IndividualCount";
                            key[9] = "DaysForHistroricalData";
                            key[10] = "IndividualMaxLossPercent";
                            key[11] = "OverallProfitAmt";
                            key[12] = "TotalOpenPositions";
                            //02-AUG-2021::Pratiksha::Added new field barcount
                            key[13] = "BarCount";
                            //05-AUG-2021::Pratiksha::Added 2 new fields for start data and end data
                            key[14] = "StartDataTime";
                            key[15] = "EndDataTime";

                            value[0] = txtStartTime.Text;
                            value[1] = txtEndTime.Text;
                            value[2] = txtTimeLimitToPlaceOrder.Text;
                            value[3] = txtTimeForExitAllOpenPosition.Text;
                            value[4] = txtPercentQuantity.Text;
                            value[5] = txttotalmoney.Text;
                            value[6] = txtBarInterval.Text;
                            value[7] = txtMaxStrategyOrderCount.Text;
                            value[8] = txtindiviualCount.Text;
                            value[9] = txtdaysForHistroricalData.Text;
                            value[10] = txtIndividualMaxLossPercent.Text;
                            value[11] = txtOverallProfitAmt.Text;
                            value[12] = txtTotalOpenPositions.Text;
                            //02-AUG-2021::Pratiksha::Added new field barcount
                            value[13] = txtBarCount.Text;
                            //05-AUG-2021::Pratiksha::Added 2 new fields for start data and end data
                            value[14] = txtStartDataTime.Text;
                            value[15] = txtEndDataTime.Text;

                            for (int i = 0; i < 16; i++)
                            {
                                iniObj.clearTestingSymbol("SDATA", key[i], value[i], filePath);
                                WriteUniquelogs("TradingSetting", "btnCancel_Click : " + key[i] + " = " + value[i], MessageType.Informational);
                            }
                            message = "Saved changes successfully!!!";
                            buttons = MessageBoxButtons.OK;
                            dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                            if (dialog == DialogResult.OK)
                            {
                                this.Close();
                            }

                            WriteUniquelogs("TradingSetting", "btnCancel_Click :  Details saved in ini file.", MessageType.Informational);
                        }
                        else
                        {
                            string parameter = "";
                            if (txtStartTime.Text == "") { parameter += "\nStart Time"; }
                            if (txtEndTime.Text == "") { parameter += "\nEnd Time"; }
                            if (txtStartDataTime.Text == "") { parameter += "\nStart Data Time"; }
                            if (txtEndDataTime.Text == "") { parameter += "\nEnd Data Time"; }
                            if (txtTimeLimitToPlaceOrder.Text == "") { parameter += "\nTime Limit To Place Order"; }
                            if (txtTimeForExitAllOpenPosition.Text == "") { parameter += "\nTime For Exit All Open Position"; }
                            if (txtPercentQuantity.Text == "") { parameter += "\nPercent Quantity"; }
                            //02-AUG-2021::Pratiksha::Added new field barcount
                            if (txtBarCount.Text == "") { parameter += "\nBar Count"; }
                            if (txttotalmoney.Text == "") { parameter += "\nTotal Money"; }
                            if (txtBarInterval.Text == "") { parameter += "\nBar Interval"; }
                            if (txtMaxStrategyOrderCount.Text == "") { parameter += "\nMax Strategy Order Count"; }
                            if (txtindiviualCount.Text == "") { parameter += "\nIndiviual Count"; }
                            if (txtdaysForHistroricalData.Text == "") { parameter += "\nDays For Histrorical Data"; }
                            if (txtIndividualMaxLossPercent.Text == "") { parameter += "\nIndividual Max Loss Percent"; }
                            if (txtOverallProfitAmt.Text == "") { parameter += "\nOverall Profit Amt"; }
                            if (txtTotalOpenPositions.Text == "") { parameter += "\nTotal Open Positions"; }
                            message = "Please enter proper values for following field: " + parameter;
                            buttons = MessageBoxButtons.OK;
                            dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                            WriteUniquelogs("TradingSetting", "btnCancel_Click : Please enter proper values.", MessageType.Informational);
                        }
                    }
                    else
                    {
                        this.Close();
                        WriteUniquelogs("TradingSetting", "btnCancel_Click : You have clicked No, no data updated in trade setting.ini file.", MessageType.Informational);
                    }
                }
                else
                {
                    this.Close();
                }
                WriteUniquelogs("TradingSetting", "btnCancel_Click : Cancel click.", MessageType.Informational);
            }
            catch (Exception ex)
            {
                WriteUniquelogs("TradingSetting", "btnCancel_Click : Exception Error Message = " + ex.Message, MessageType.Informational);
            }
        }

        private void Clearfields()
        {
            try
            {
                txtStartTime.Text = null;
                txtEndTime.Text = null;
                txtStartDataTime.Text = null;
                txtEndDataTime.Text = null;
                txtTimeLimitToPlaceOrder.Text = null;
                txtTimeForExitAllOpenPosition.Text = null;
                txtPercentQuantity.Text = null;
                txttotalmoney.Text = null;
                txtBarInterval.Text = null;
                txtMaxStrategyOrderCount.Text = null;
                txtindiviualCount.Text = null;
                txtdaysForHistroricalData.Text = null;
                txtIndividualMaxLossPercent.Text = null;
                txtOverallProfitAmt.Text = null;
                txtTotalOpenPositions.Text = null;
                txtBarCount.Text = null;
                WriteUniquelogs("TradingSetting", "Clearfields : All fields cleared.", MessageType.Informational);
            }
            catch (Exception ex)
            {
                WriteUniquelogs("TradingSetting", "Clearfields : Exception Error Message = " + ex.Message, MessageType.Informational);
            }
        }

        private void btnOpenSymbolSetting_Click(object sender, EventArgs e)
        {
            try
            {
                SymbolSetting symbolObj = new SymbolSetting(this.AlgoOMSObj, readSettings);
                symbolObj.ShowDialog();
            }
            catch (Exception ex)
            {
                WriteUniquelogs("TradingSetting", "btnOpenSymbolSetting_Click : Exception Error Message = " + ex.Message, MessageType.Informational);
            }
        }
    }
}

