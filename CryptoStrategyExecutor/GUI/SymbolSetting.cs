﻿using IRDSAlgoOMS;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CryptoStrategyExecutor
{
    public partial class SymbolSetting : Form
    {
        string inisymbolList;
        CryptoStrategyExecutor.ReadSettings Settings;
        IRDSAlgoOMS.Logger logger = IRDSAlgoOMS.Logger.Instance;
        string title = "RealStockA1 Strategy";
        string message = "";
        MessageBoxButtons buttons = MessageBoxButtons.OK;
        DialogResult dialog;
        bool isexit = false;
        string path = System.AppDomain.CurrentDomain.BaseDirectory + "Configuration";

        List<string> finalNSEList = new List<string>();
        public SymbolSetting(AlgoOMS obj, CryptoStrategyExecutor.ReadSettings readSettings)
        {
            InitializeComponent();
            try
            {
                Settings = readSettings;
                GetSymbolsFromDB(obj);
                string filePath = path + @"\Crypto_Sandip.ini";
                FetchDetailsFromINI(filePath);
                RBEquity.Checked = true;
                WriteUniquelogs("SymbolSetting", "SymbolSetting : loaded symbols and fetched details properly.", MessageType.Informational);
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSetting", "SymbolSetting : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }
        private void btnApply_Click(object sender, EventArgs e)
        {
            try
            {
                if (TradingSymbolList.Text.Length == 0 && txtNA4.Text.Length == 0 && txtperiodOfbar.Text.Length == 0 && txtprofitpercent.Text.Length == 0 && txtstoploss.Text.Length == 0)
                {
                    message = "Do you want to save the changes?";
                    buttons = MessageBoxButtons.YesNo;
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                    if (dialog == DialogResult.Yes)
                    {
                        AddIntoINIFile();
                        this.Close();
                    }
                }
                else
                {
                    if (TradingSymbolList.Text.Length > 0 && txtNA4.Text.ToString().Length > 0 && txtperiodOfbar.Text.ToString().Length > 0 && txtprofitpercent.Text.ToString().Length > 0 && txtstoploss.Text.ToString().Length > 0)
                    {
                        Match PeriodOfbar1, Profitpercent1, Stoploss1, NA4;
                        Validations(out PeriodOfbar1, out Profitpercent1, out Stoploss1, out NA4);
                        if (PeriodOfbar1.Success && Profitpercent1.Success && Stoploss1.Success && NA4.Success)
                        {
                            message = "Do you want to save the changes?";
                            buttons = MessageBoxButtons.YesNo;
                            dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                            if (dialog == DialogResult.Yes)
                            {
                                AddIntoINIFile();
                            }
                        }
                        else
                        {
                            string Logmessage = "";
                            string parameter = "";
                            if (!PeriodOfbar1.Success) { parameter += "\nPeriod Of Bar"; Logmessage += "Period Of Bar,"; }
                            if (!Profitpercent1.Success) { parameter += "\nProfit Percent"; Logmessage += "Profit Percent,"; }
                            if (!Stoploss1.Success) { parameter += "\nStoploss"; Logmessage += "Stoploss,"; }
                            if (!NA4.Success) { parameter += "\nProfit Trail"; Logmessage += "Profit Trail,"; }
                            message = "Please add valid values for " + parameter + ".";
                            buttons = MessageBoxButtons.OK;
                            dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                            WriteUniquelogs("SymbolSetting", "btnApply_Click : " + Logmessage, MessageType.Informational);
                        }
                    }
                    else
                    {
                        CheckNullValuesAndShowPopup();
                    }
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSetting", "btnApply_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void AddIntoINIFile(bool IsDisplay)
        {
            bool issaved = false;
            bool isAdded = false;
            int j = 0;
            int counter = 0;
            int k = dataGridView.Rows.Count;
            bool flagSymbolname = false;
            INIFile iniObj = new INIFile(inisymbolList);
            DialogResult dialog;
            try
            {
                foreach (DataGridViewRow row in dataGridView.Rows)
                {
                    flagSymbolname = false;
                    if ((row.Cells["TradingSymbol"].Value.ToString() == null) || (row.Cells["PeriodOfBar"].Value.ToString() == null) || (row.Cells["ProfitPercent"].Value.ToString() == null) || (row.Cells["Stoploss"].Value.ToString() == null) || (row.Cells["profitTrail"].Value.ToString() == null))
                    {
                    }
                    else
                    {
                        if (TradingSymbolList.Text.Length > 0 && txtNA4.Text.ToString().Length > 0 && txtperiodOfbar.Text.ToString().Length > 0 && txtprofitpercent.Text.ToString().Length > 0 && txtstoploss.Text.ToString().Length > 0)
                        {
                            string symbol = GetExchangeOnSymbolBasis(TradingSymbolList.Text);
                            if (symbol == row.Cells["TradingSymbol"].Value.ToString())
                            {
                                if ((row.Cells[0].Value.Equals(symbol)) && (row.Cells[2].Value.Equals(txtperiodOfbar.Text.ToString())) && (row.Cells[3].Value.Equals(txtprofitpercent.Text.ToString())) && (row.Cells[5].Value.Equals(txtstoploss.Text.ToString())) && (row.Cells[7].Value.Equals(txtNA4.Text.ToString())))
                                {
                                    if (AddUpdateRowinDataGrid(0, false) == true)
                                    {
                                        k += 1;
                                        TradingSymbolList.SelectedIndex = -1;
                                        ClearFields();
                                        issaved = true;
                                        isAdded = true;
                                    }
                                    else
                                    {
                                        issaved = false;
                                    }
                                }
                                else
                                {
                                    foreach (DataGridViewRow r in dataGridView.SelectedRows)
                                    {
                                        string sym = GetExchangeOnSymbolBasis(TradingSymbolList.Text);
                                        r.Cells["TradingSymbol"].Value = sym;
                                        r.Cells["PeriodOfBar"].Value = txtperiodOfbar.Text;
                                        r.Cells["ProfitPercent"].Value = txtprofitpercent.Text;
                                        r.Cells["Stoploss"].Value = txtstoploss.Text;
                                        r.Cells["profitTrail"].Value = txtNA4.Text;
                                        TradingSymbolList.SelectedIndex = -1;
                                        ClearFields();
                                        issaved = true;
                                    }
                                }
                            }
                            else
                            {
                                if (isAdded == false)
                                {
                                    bool isFound = false;
                                    foreach (DataGridViewRow r in dataGridView.Rows)
                                    {
                                        string sym = GetExchangeOnSymbolBasis(TradingSymbolList.Text);
                                        if (sym == row.Cells["TradingSymbol"].Value.ToString())
                                        {
                                            isFound = true;
                                            break;
                                        }
                                        else
                                        {
                                            isFound = false;
                                        }
                                    }
                                    if (isFound == false)
                                    {
                                        if (AddUpdateRowinDataGrid(0, false) == true)
                                        {
                                            k += 1;
                                            TradingSymbolList.SelectedIndex = -1;
                                            ClearFields();
                                            issaved = true;
                                            isAdded = true;
                                        }
                                        else
                                        {
                                            issaved = false;
                                        }
                                    }
                                }
                            }
                        }
                        if (((row.Cells["TradingSymbol"].Value.ToString().Length == 0) || (row.Cells["PeriodOfBar"].Value.ToString().Length == 0) || (row.Cells["ProfitPercent"].Value.ToString().Length == 0) || (row.Cells["Stoploss"].Value.ToString().Length == 0) || (row.Cells["profitTrail"].Value.ToString().Length == 0)))
                        {
                            message = "Not able to add this because null value is present.";
                            buttons = MessageBoxButtons.OK;
                            dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                            WriteUniquelogs("SymbolSetting", "AddIntoINIFile : Null value present at :" + row.Cells["TradingSymbol"].Value.ToString() + (row.Cells["PeriodOfBar"].Value.ToString().Length == 0) + (row.Cells["ProfitPercent"].Value.ToString().Length == 0) + (row.Cells["Stoploss"].Value.ToString().Length == 0) + (row.Cells["profitTrail"].Value.ToString().Length == 0) + ". Not able to add this value.", MessageType.Informational);
                        }
                        else
                        {
                            j++;
                        }
                    }
                }

                if ((j == k) && (issaved = true))
                {
                    if (Settings == null)
                    {
                        Settings = new CryptoStrategyExecutor.ReadSettings(logger);
                    }
                    iniObj.clearTestingSymbol("TRADINGSYMBOL", null, null, inisymbolList);
                    foreach (DataGridViewRow row in dataGridView.Rows)
                    {
                        Settings.writeINIFileSymbolSetting("TRADINGSYMBOL", row.Cells["TradingSymbol"].Value.ToString().ToUpper() + "," + (row.Cells["PeriodOfBar"].Value.ToString().ToUpper()) + /*"," + Strikevalue + */"," + (row.Cells["ProfitPercent"].Value.ToString()) + "," + (row.Cells["Stoploss"].Value.ToString()) + "," + (row.Cells["profitTrail"].Value.ToString()), inisymbolList);
                        counter++;
                        string rowOutout = row.Cells["TradingSymbol"].Value.ToString().ToUpper() + "," + (row.Cells["PeriodOfBar"].Value.ToString().ToUpper()) +/* "," + Strikevalue +*/ "," + (row.Cells["ProfitPercent"].Value.ToString()) + "," + (row.Cells["Stoploss"].Value.ToString()) + "," + (row.Cells["profitTrail"].Value.ToString());
                        WriteUniquelogs("SymbolSetting", "AddIntoINIFile :  Details saved " + rowOutout, MessageType.Informational);
                    }
                    if (IsDisplay == false)
                    {
                        message = "Changes updated successfully!!!";
                        buttons = MessageBoxButtons.OK;
                        dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                    }
                    WriteUniquelogs("SymbolSetting", "AddIntoINIFile : " + message, MessageType.Informational);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSetting", "AddIntoINIFile : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void ClearFields()
        {
            try
            {
                txtNA4.Text = "";
                txtperiodOfbar.Text = "";
                txtprofitpercent.Text = "";
                txtstoploss.Text = "";
                TradingSymbolList.SelectedIndex = -1;
                TradingSymbolList.Text = "";
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSetting", "ClearFields : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }
        private void btnDeleteselectedrow_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridView.Rows.Count > 0)
                {
                    if (dataGridView.SelectedRows.Count == 1)
                    {
                        foreach (DataGridViewRow row in dataGridView.SelectedRows)
                        {
                            string symbol = row.Cells[0].Value.ToString();
                            message = "Are you sure you want to delete the " + symbol + " ?";
                            buttons = MessageBoxButtons.YesNo;
                            dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);

                            if (dialog == DialogResult.Yes)
                            {
                                dataGridView.Rows.RemoveAt(row.Index);
                                WriteUniquelogs("SymbolSetting", "btnDeleteselectedrow_Click : Row deleted at :" + row.Cells[0].Value + " position.", MessageType.Informational);
                                ClearFields();
                                AddIntoINIFile(false);
                                var value = Settings.TradingSymbolsInfoList.Find(r => r.TradingSymbol == symbol.Split('.')[0]);
                                if (value != null)
                                {
                                    Settings.TradingSymbolsInfoList.Remove(value);
                                }
                            }
                        }
                    }
                    else
                    {
                        message = "Please select the row first.";
                        buttons = MessageBoxButtons.OK;
                        dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                    }
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSetting", "btnDeleteselectedrow_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }
        public void WriteUniquelogs(string fileName, string message, MessageType msgType)
        {
            try
            {
                SymbolLogs log = new SymbolLogs();
                log.createFile(fileName);
                log.LogMessage(message, MessageType.Informational);
                log = null;
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSetting", "WriteUniquelogs : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                bool found = false;
                if (TradingSymbolList.Text.ToString().Length > 0 && txtNA4.Text.ToString().Length > 0 && txtperiodOfbar.Text.ToString().Length > 0 && txtprofitpercent.Text.ToString().Length > 0 && txtstoploss.Text.ToString().Length > 0)
                {
                    Match PeriodOfbar1, Profitpercent1, Stoploss1, NA4;
                    Validations(out PeriodOfbar1, out Profitpercent1, out Stoploss1, out NA4);
                    if (PeriodOfbar1.Success && Profitpercent1.Success && Stoploss1.Success && NA4.Success)
                    {
                        foreach (DataGridViewRow row in this.dataGridView.Rows)
                        {
                            string sym = GetExchangeOnSymbolBasis(TradingSymbolList.Text);
                            if (row.Cells[0].Value.Equals(sym))
                            {
                                found = true;
                                break;
                            }
                        }
                        if (found == false)
                        {
                            AddUpdateRowinDataGrid(0, true);
                        }
                        else
                        {
                            message = "This entry is already exist.";
                            buttons = MessageBoxButtons.OK;
                            dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                            WriteUniquelogs("SymbolSetting", "btnAdd_Click : " + message, MessageType.Informational);
                            ClearFields();
                            TradingSymbolList.SelectedIndex = -1;
                        }
                    }
                    else
                    {
                        string Logmessage = "";
                        string parameter = "";
                        if (!PeriodOfbar1.Success) { parameter += "\nPeriod Of Bar"; Logmessage += "Period Of Bar,"; }
                        if (!Profitpercent1.Success) { parameter += "\nProfit Percent"; Logmessage += "Profit Percent,"; }
                        if (!Stoploss1.Success) { parameter += "\nStoploss"; Logmessage += "Stoploss,"; }
                        if (!NA4.Success) { parameter += "\nProfit Trail"; Logmessage += "Profit Trail,"; }
                        message = "Please add valid values for " + parameter + ".";
                        buttons = MessageBoxButtons.OK;
                        dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                        WriteUniquelogs("SymbolSetting", "btnAdd_Click : " + Logmessage, MessageType.Informational);
                    }
                }
                else
                {
                    CheckNullValuesAndShowPopup();
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSetting", "btnAdd_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private bool AddUpdateRowinDataGrid(int choice, bool SavedIniFile)
        {
            bool IsAdded = false;
            try
            {
                bool found = false;
                string symbol = GetExchangeOnSymbolBasis(TradingSymbolList.Text);
                foreach (DataGridViewRow row in this.dataGridView.Rows)
                {
                    if ((row.Cells[0].Value.ToString().Equals(symbol)) && (row.Cells[1].Value.ToString().Equals(txtperiodOfbar.Text.ToString())) && (row.Cells[2].Value.ToString().Equals(txtprofitpercent.Text.ToString())) && (row.Cells[3].Value.ToString().Equals(txtstoploss.Text.ToString())) && (row.Cells[4].Value.ToString().Equals(txtNA4.Text.ToString())))
                    {
                        found = true;
                        break;
                    }
                }

                if (!found)
                {
                    if (choice == 0)
                    {

                        string OutputRow = TradingSymbolList.Text.ToString() + "," + txtperiodOfbar.Text + "," + txtprofitpercent.Text + "," + txtstoploss.Text + "," + txtNA4.Text;
                        WriteUniquelogs("SymbolSetting", "AddUpdateRowinDataGrid : Row added in datagrid : " + OutputRow, MessageType.Informational);
                        dataGridView.Rows.Add(symbol, txtperiodOfbar.Text, txtprofitpercent.Text, txtstoploss.Text, txtNA4.Text);
                        IsAdded = true;
                        ClearFields();
                    }
                    else
                    {
                        foreach (DataGridViewRow r in dataGridView.SelectedRows)
                        {
                            string sym = GetExchangeOnSymbolBasis(TradingSymbolList.Text);
                            r.Cells["TradingSymbol"].Value = sym;
                            r.Cells["PeriodOfBar"].Value = txtperiodOfbar.Text;
                            r.Cells["ProfitPercent"].Value = txtprofitpercent.Text;
                            r.Cells["Stoploss"].Value = txtstoploss.Text;
                            r.Cells["profitTrail"].Value = txtNA4.Text;
                        }
                    }
                    if (SavedIniFile == true)
                    {
                        AddIntoINIFile();
                        IsAdded = true;
                        WriteUniquelogs("SymbolSetting", "AddUpdateRowinDataGrid : Row added.", MessageType.Informational);
                    }
                    ClearFields();
                }
                else
                {
                    message = "This entry is already exist.";
                    buttons = MessageBoxButtons.OK;
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                    WriteUniquelogs("SymbolSetting", "AddUpdateRowinDataGrid : " + message, MessageType.Informational);
                    IsAdded = false;
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSetting", "AddUpdateRowinDataGrid : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
            return IsAdded;
        }
        private void GetSymbolsFromDB(AlgoOMS obj)
        {
            try
            {
                finalNSEList = obj.GetInstrumentsSymbols(Constants.EXCHANGE_NSE.ToString());
                WriteUniquelogs("SymbolSetting", "GetSymbolsFromDB : Added expiry dates into list.", MessageType.Informational);
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSetting", "GetSymbolsFromDB : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void FetchDetailsFromINI(string Path)
        {
            try
            {
                inisymbolList = Path;
                int rank = 0;
                if (File.Exists(inisymbolList))
                {
                    try
                    {
                        if (Settings == null)
                        {
                            Settings = new CryptoStrategyExecutor.ReadSettings(logger);
                        }
                        Settings.readCrypto_SandipConfigFile(inisymbolList);
                        foreach (var item in Settings.TradingSymbolsInfoList)
                        {
                            try
                            {
                                string TradingSymbol = item.TradingSymbol.ToString();
                                string PeriodofBar = item.BarCount.ToString();
                                string ProfitPercent = item.ProfitPercent.ToString();
                                string Stoploss = item.Stoploss.ToString();
                                string NA4 = item.ProfitTrail.ToString();
                                string sym = GetExchangeOnSymbolBasis(item.TradingSymbol.ToString());
                                dataGridView.Rows.Add(new object[] { sym, PeriodofBar, ProfitPercent, Stoploss, NA4 });
                                rank++;
                            }
                            catch (Exception ex)
                            {
                                WriteUniquelogs("SymbolSetting", "FetchDetailsFromINI : Exception Error Message = " + ex.Message, MessageType.Exception);
                            }
                        }
                        dataGridView.AllowUserToAddRows = false;
                    }
                    catch (Exception ex)
                    {
                        WriteUniquelogs("SymbolSetting", "FetchDetailsFromINI : Exception Error Message = " + ex.Message, MessageType.Exception);
                    }
                }
                else
                {
                    WriteUniquelogs("SymbolSetting", "FetchDetailsFromINI : INI file not present.", MessageType.Informational);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSetting", "FetchDetailsFromINI : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                OnCancelClickSaveChanges();
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSetting", "btnCancel_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void dataGridView_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0)
                {
                    dataGridView.Rows[e.RowIndex].Selected = true;
                    DataGridViewRow row = dataGridView.Rows[e.RowIndex];
                    string[] symbol = row.Cells[0].Value.ToString().Split('.');
                    TradingSymbolList.Text = symbol[0];
                    txtperiodOfbar.Text = row.Cells[1].Value.ToString();
                    txtprofitpercent.Text = row.Cells[2].Value.ToString();
                    txtstoploss.Text = row.Cells[3].Value.ToString();
                    txtNA4.Text = row.Cells[4].Value.ToString();
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSetting", "dataGridView_CellMouseClick : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridView.SelectedRows.Count == 1)
                {
                    if (TradingSymbolList.Text.Length > 0 && txtNA4.Text.ToString().Length > 0 && txtperiodOfbar.Text.ToString().Length > 0 && txtprofitpercent.Text.ToString().Length > 0 && txtstoploss.Text.ToString().Length > 0)
                    {
                        Match PeriodOfbar1, Profitpercent1, Stoploss1, NA4;
                        Validations(out PeriodOfbar1, out Profitpercent1, out Stoploss1, out NA4);
                        if (PeriodOfbar1.Success && Profitpercent1.Success && Stoploss1.Success && NA4.Success)
                        {
                            AddIntoINIFile();
                        }
                        else
                        {
                            string Logmessage = "";
                            string parameter = "";
                            if (!PeriodOfbar1.Success) { parameter += "\nPeriod Of Bar"; Logmessage += "Period Of Bar,"; }
                            if (!Profitpercent1.Success) { parameter += "\nProfit Percent"; Logmessage += "Profit Percent,"; }
                            if (!Stoploss1.Success) { parameter += "\nStoploss"; Logmessage += "Stoploss,"; }
                            if (!NA4.Success) { parameter += "\nProfit Trail"; Logmessage += "Profit Trail,"; }
                            message = "Please add valid values for " + parameter + ".";
                            buttons = MessageBoxButtons.OK;
                            dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                            WriteUniquelogs("SymbolSetting", "btnUpdate_Click : " + Logmessage, MessageType.Informational);
                        }
                    }
                    else
                    {
                        CheckNullValuesAndShowPopup();
                    }
                }
                else
                {
                    message = "Please select the row first.";
                    buttons = MessageBoxButtons.OK;
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSetting", "btnUpdate_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }
        private void CheckNullValuesAndShowPopup()
        {
            try
            {
                string Logmessage = "Please add Proper values for ";
                string parameter = "";

                if (TradingSymbolList.Text == null) { parameter += "\nSymbol"; }
                if (txtperiodOfbar.Text.ToString().Length == 0) { parameter += "\nPeriod Of Bar"; }
                if (txtprofitpercent.Text.ToString().Length == 0) { parameter += "\nProfit Percent"; }
                if (txtstoploss.Text.ToString().Length == 0) { parameter += "\nStoploss"; }
                if (txtNA4.Text.ToString().Length == 0) { parameter += "\nProfit Trail"; }
                message = "Please add proper values for " + parameter + ".";
                buttons = MessageBoxButtons.OK;
                dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                WriteUniquelogs("SymbolSetting", "CheckNullValuesAndShowPopup : " + Logmessage, MessageType.Informational);
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSetting", "CheckNullValuesAndShowPopup : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }
        private void Validations(out Match PeriodOfbar1, out Match Profitpercent1, out Match Stoploss1, out Match NA4)
        {
            string Decpattern = "^[0-9]+(.|,)?[0-9]*?$";
            Regex r2 = new Regex(Decpattern);
            PeriodOfbar1 = r2.Match(txtperiodOfbar.Text.ToString());
            Profitpercent1 = r2.Match(txtprofitpercent.Text.ToString());
            Stoploss1 = r2.Match(txtstoploss.Text.ToString());
            NA4 = r2.Match(txtNA4.Text.ToString());
        }

        private bool OnCancelClickSaveChanges()
        {
            bool formCloseStatus = false;
            bool changeFound = false;
            try
            {
                if (TradingSymbolList.Text.Length > 0 || txtNA4.Text.ToString().Length > 0 || txtperiodOfbar.Text.ToString().Length > 0 || txtprofitpercent.Text.ToString().Length > 0 || txtstoploss.Text.ToString().Length > 0)
                {
                    foreach (DataGridViewRow row in dataGridView.Rows)
                    {
                        string symbol = "";
                        if (TradingSymbolList.Text.Length > 0)
                        {
                            if (finalNSEList.Contains(TradingSymbolList.Text))
                            {
                                symbol = TradingSymbolList.Text.ToString() + "." + Constants.EXCHANGE_NSE;
                            }

                            if (row.Cells[0].Value.ToString().Equals(symbol))
                            {
                                if ((row.Cells[2].Value.ToString().Equals(txtperiodOfbar.Text)) && (row.Cells[3].Value.ToString().Equals(txtprofitpercent.Text)) && (row.Cells[5].Value.ToString().Equals(txtstoploss.Text)) && (row.Cells[7].Value.ToString().Equals(txtNA4.Text)))
                                {
                                    changeFound = false;
                                    isexit = true;
                                    break;
                                }
                                else
                                {
                                    changeFound = true;
                                }
                            }
                            else
                            {
                                changeFound = true;
                            }
                        }
                    }
                    if (changeFound == true)
                    {
                        message = "You have some unsaved changes, do you want to save those changes?";
                        buttons = MessageBoxButtons.YesNo;
                        dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                        if (DialogResult.Yes == dialog)
                        {
                            if (TradingSymbolList.Text.Length > 0 && txtNA4.Text.ToString().Length > 0 && txtperiodOfbar.Text.ToString().Length > 0 && txtprofitpercent.Text.ToString().Length > 0 && txtstoploss.Text.ToString().Length > 0)
                            {
                                Match PeriodOfbar1, Profitpercent1, Stoploss1, NA4;
                                Validations(out PeriodOfbar1, out Profitpercent1, out Stoploss1, out NA4);
                                if (PeriodOfbar1.Success && Profitpercent1.Success && Stoploss1.Success && NA4.Success)
                                {
                                    AddIntoINIFile();
                                    isexit = true;
                                    this.Close();
                                }
                                else
                                {
                                    string Logmessage = "";
                                    string parameter = "";
                                    if (!PeriodOfbar1.Success) { parameter += "\nPeriod Of Bar"; Logmessage += "Period Of Bar,"; }
                                    if (!Profitpercent1.Success) { parameter += "\nProfit Percent"; Logmessage += "Profit Percent,"; }
                                    if (!Stoploss1.Success) { parameter += "\nStoploss"; Logmessage += "Stoploss,"; }
                                    if (!NA4.Success) { parameter += "\nProfit Trail"; Logmessage += "Profit Trail,"; }
                                    message = "Please add valid values for " + parameter + ".";
                                    buttons = MessageBoxButtons.OK;
                                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                                    WriteUniquelogs("SymbolSetting", "OnCancelClickSaveChanges : " + Logmessage, MessageType.Informational);
                                }
                            }
                            else
                            {
                                CheckNullValuesAndShowPopup();
                                formCloseStatus = false;
                            }
                        }
                        else
                        {
                            isexit = true;
                            this.Close();
                            WriteUniquelogs("SymbolSetting", "OnCancelClickSaveChanges : You clicked on No.", MessageType.Informational);
                        }
                    }
                    else
                    {
                        isexit = true;
                        this.Close();
                        WriteUniquelogs("SymbolSetting", "OnCancelClickSaveChanges : No change found so close the form.", MessageType.Informational);
                    }
                }
                else
                {
                    this.Close();
                    WriteUniquelogs("SymbolSetting", "OnCancelClickSaveChanges : No change found so close the form.", MessageType.Informational);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSetting", "OnCancelClickSaveChanges : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
            return formCloseStatus;
        }

        private void AddIntoINIFile()
        {
            bool issaved = false;
            bool isAdded = false;
            int j = 0;
            int counter = 0;
            int k = dataGridView.Rows.Count;
            bool flagSymbolname = false;
            var re = @"^[0-9]{2}[a-zA-Z]{3}[0-9]{2}$";
            INIFile iniObj = new INIFile(inisymbolList);
            DialogResult dialog;
            try
            {
                foreach (DataGridViewRow row in dataGridView.Rows)
                {
                    flagSymbolname = false;
                    if (((row.Cells["TradingSymbol"].Value.ToString().Length == 0) || (row.Cells["PeriodOfBar"].Value.ToString().Length == 0) || (row.Cells["ProfitPercent"].Value.ToString().Length == 0) || (row.Cells["Stoploss"].Value.ToString().Length == 0) || (row.Cells["profitTrail"].Value.ToString().Length == 0)))
                    {
                    }
                    else
                    {
                        if (TradingSymbolList.Text.Length > 0 || txtNA4.Text.ToString().Length > 0 || txtperiodOfbar.Text.ToString().Length > 0 || txtprofitpercent.Text.ToString().Length > 0 || txtstoploss.Text.ToString().Length > 0)
                        {
                            string Finalsymbol = GetExchangeOnSymbolBasis(TradingSymbolList.Text);
                            if (Finalsymbol == row.Cells["TradingSymbol"].Value.ToString())
                            {
                                if ((row.Cells[0].Value.Equals(Finalsymbol)) && (row.Cells[2].Value.Equals(txtperiodOfbar.Text.ToString())) && (row.Cells[3].Value.Equals(txtprofitpercent.Text.ToString())) && (row.Cells[5].Value.Equals(txtstoploss.Text.ToString())) && (row.Cells[7].Value.Equals(txtNA4.Text.ToString())))
                                {
                                    if (AddUpdateRowinDataGrid(0, false) == true)
                                    {
                                        k += 1;
                                        ClearFields();
                                        issaved = true;
                                        isAdded = true;
                                    }
                                    else
                                    {
                                        issaved = false;
                                    }
                                }
                                else
                                {
                                    foreach (DataGridViewRow r in dataGridView.SelectedRows)
                                    {
                                        string symbol = GetExchangeOnSymbolBasis(TradingSymbolList.Text);
                                        r.Cells["TradingSymbol"].Value = symbol;
                                        r.Cells["PeriodOfBar"].Value = txtperiodOfbar.Text;
                                        r.Cells["ProfitPercent"].Value = txtprofitpercent.Text;
                                        r.Cells["Stoploss"].Value = txtstoploss.Text;
                                        r.Cells["profitTrail"].Value = txtNA4.Text;
                                        TradingSymbolList.SelectedIndex = -1;
                                        ClearFields();
                                        issaved = true;
                                    }
                                }
                            }
                            else
                            {
                                if (isAdded == false)
                                {
                                    bool isFound = false;
                                    foreach (DataGridViewRow r in dataGridView.Rows)
                                    {
                                        string symbol = GetExchangeOnSymbolBasis(TradingSymbolList.Text);
                                        if (symbol == r.Cells["TradingSymbol"].Value.ToString())
                                        {
                                            isFound = true;
                                            break;
                                        }
                                        else
                                        {
                                            isFound = false;
                                        }
                                    }
                                    if (isFound == false)
                                    {
                                        if (AddUpdateRowinDataGrid(0, false) == true)
                                        {
                                            k += 1;
                                            ClearFields();
                                            issaved = true;
                                            isAdded = true;
                                        }
                                        else
                                        {
                                            issaved = false;
                                        }
                                    }
                                }
                            }
                        }
                        if (((row.Cells["TradingSymbol"].Value.ToString().Length == 0) || (row.Cells["PeriodOfBar"].Value.ToString().Length == 0) || (row.Cells["ProfitPercent"].Value.ToString().Length == 0) || (row.Cells["Stoploss"].Value.ToString().Length == 0) || (row.Cells["profitTrail"].Value.ToString().Length == 0)))
                        {
                            message = "Not able to add this because null value is present.";
                            buttons = MessageBoxButtons.OK;
                            dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                            WriteUniquelogs("SymbolSetting", "AddIntoINIFile : Null value present at :" + row.Cells["SymbolName"].Value.ToString() + row.Cells["Exchange"].Value.ToString() + (row.Cells["ExpiryDate"].Value.ToString().Length == 0) + (row.Cells["ExpiryPeriod"].Value.ToString().Length == 0) + (row.Cells["OrderType"].Value.ToString().Length == 0) + (row.Cells["MarketLotSize"].Value.ToString().Length == 0) + (row.Cells["TotalNumberofLotSize"].Value.ToString().Length == 0) + (row.Cells["RoundOff"].Value.ToString().Length == 0) + (row.Cells["MarginPerLot"].Value.ToString().Length == 0) + (row.Cells["Stoploss"].Value.ToString().Length == 0) + ". Not able to add this value.", MessageType.Informational);
                        }
                        else
                        {
                            j++;
                        }
                    }
                }

                if ((j == k) && (issaved = true))
                {
                    if (Settings == null)
                    {
                        Settings = new CryptoStrategyExecutor.ReadSettings(logger);
                    }
                    iniObj.clearTestingSymbol("TRADINGSYMBOL", null, null, inisymbolList);
                    foreach (DataGridViewRow row in dataGridView.Rows)
                    {
                        Settings.writeINIFileSymbolSetting("TRADINGSYMBOL", row.Cells["TradingSymbol"].Value.ToString().ToUpper() + "," + (row.Cells["PeriodOfBar"].Value.ToString().ToUpper()) + /*"," + Strikevalue + */"," + (row.Cells["ProfitPercent"].Value.ToString()) + "," + (row.Cells["Stoploss"].Value.ToString()) + "," + (row.Cells["profitTrail"].Value.ToString()), inisymbolList);
                        counter++;
                        string rowOutout = row.Cells["TradingSymbol"].Value.ToString().ToUpper() + "," + (row.Cells["PeriodOfBar"].Value.ToString().ToUpper()) +/* "," + Strikevalue +*/ "," + (row.Cells["ProfitPercent"].Value.ToString()) + "," + (row.Cells["Stoploss"].Value.ToString()) + "," + (row.Cells["profitTrail"].Value.ToString());
                        WriteUniquelogs("SymbolSetting", "AddIntoINIFile :  Details saved " + rowOutout, MessageType.Informational);
                    }

                    message = "Changes updated successfully!!!";
                    buttons = MessageBoxButtons.OK;
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);

                    WriteUniquelogs("SymbolSetting", "AddIntoINIFile : " + message, MessageType.Informational);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSetting", "AddIntoINIFile : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private string GetExchangeOnSymbolBasis(string symbol)
        {
            string Finalsymbol = "";
            if (symbol.Length > 0)
            {
                if (finalNSEList.Contains(symbol))
                {
                    Finalsymbol = symbol.ToString() + "." + Constants.EXCHANGE_NSE;
                }
            }
            return Finalsymbol;
        }

        private void btnClearAll_Click(object sender, EventArgs e)
        {
            try
            {
                ClearFields();
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSetting", "btnClearAll_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void RBEquity_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                TradingSymbolList.Items.Clear();
                foreach (string listItem in finalNSEList)
                {
                    TradingSymbolList.Items.Add(listItem);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSetting", "RBEquity_CheckedChanged : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void RBFuture_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                //TradingSymbolList.Items.Clear();
                //foreach (string listItem in finalNFOList)
                //{
                //    TradingSymbolList.Items.Add(listItem);
                //}
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSetting", "RBFuture_CheckedChanged : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                string pathname = "";
                SaveFDExportFile.InitialDirectory = @"C:\";
                SaveFDExportFile.Title = "Save File";
                SaveFDExportFile.DefaultExt = "txt";
                SaveFDExportFile.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                SaveFDExportFile.FilterIndex = 2;
                if (SaveFDExportFile.ShowDialog() == DialogResult.OK)
                {
                    pathname = SaveFDExportFile.FileName;
                    INIFile iniObj = new INIFile(pathname);
                    if (Settings == null)
                    {
                        Settings = new CryptoStrategyExecutor.ReadSettings(logger);
                    }
                    foreach (DataGridViewRow row in dataGridView.Rows)
                    {
                        Settings.writeINIFileSymbolSetting("TRADINGSYMBOL", row.Cells["TradingSymbol"].Value.ToString().ToUpper() + "," + (row.Cells["PeriodOfBar"].Value.ToString().ToUpper()) + "," + (row.Cells["ProfitPercent"].Value.ToString()) + "," + (row.Cells["Stoploss"].Value.ToString()) + "," + (row.Cells["profitTrail"].Value.ToString()), pathname);
                        string rowOutout = row.Cells["TradingSymbol"].Value.ToString().ToUpper() + "," + (row.Cells["PeriodOfBar"].Value.ToString().ToUpper()) + "," + (row.Cells["ProfitPercent"].Value.ToString()) + "," + (row.Cells["Stoploss"].Value.ToString()) + "," + (row.Cells["profitTrail"].Value.ToString());
                        WriteUniquelogs("SymbolSetting", "btnExport_Click :  Details saved " + rowOutout, MessageType.Informational);
                    }
                    message = "File exported successfully!!!";
                    buttons = MessageBoxButtons.OK;
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                    WriteUniquelogs("SymbolSetting", "btnExport_Click : " + message, MessageType.Informational);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSetting", "btnExport_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void BtnImport_Click(object sender, EventArgs e)
        {
            try
            {
                INIFile iniObj = new INIFile(inisymbolList);
                string pathname = "";
                OpenFDImportFile.InitialDirectory = @"C:\";
                OpenFDImportFile.Title = "Save File";
                OpenFDImportFile.DefaultExt = "txt";
                OpenFDImportFile.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                if (OpenFDImportFile.ShowDialog() == DialogResult.OK)
                {
                    pathname = OpenFDImportFile.FileName;
                    dataGridView.Rows.Clear();
                    if (Settings == null)
                    {
                        Settings = new CryptoStrategyExecutor.ReadSettings(logger);
                    }
                    Settings.ReadTradingSymbols(pathname);
                    foreach (var item in Settings.TradingSymbolsInfoList)
                    {
                        try
                        {
                            string TradingSymbol = item.TradingSymbol.ToString();
                            string PeriodofBar = item.BarCount.ToString();
                            string ProfitPercent = item.ProfitPercent.ToString();
                            string Stoploss = item.Stoploss.ToString();
                            string NA4 = item.ProfitTrail.ToString();
                            string sym = GetExchangeOnSymbolBasis(item.TradingSymbol.ToString());
                            dataGridView.Rows.Add(new object[] { sym, PeriodofBar, ProfitPercent, Stoploss, NA4 });
                        }
                        catch (Exception ex)
                        {
                            WriteUniquelogs("SymbolSetting", "BtnImport_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
                        }
                    }
                    iniObj.clearTestingSymbol("TRADINGSYMBOL", null, null, inisymbolList);
                    AddIntoINIFile(true);
                    dataGridView.AllowUserToAddRows = false;
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSetting", "BtnImport_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }
    }
}
