﻿
namespace CryptoStrategyExecutor
{
    partial class TradingSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TradingSetting));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblBarCount = new System.Windows.Forms.Label();
            this.pnlBarCount = new System.Windows.Forms.Panel();
            this.txtBarCount = new System.Windows.Forms.TextBox();
            this.txtStartTime = new System.Windows.Forms.DateTimePicker();
            this.txtEndTime = new System.Windows.Forms.DateTimePicker();
            this.txtTimeLimitToPlaceOrder = new System.Windows.Forms.DateTimePicker();
            this.txtTimeForExitAllOpenPosition = new System.Windows.Forms.DateTimePicker();
            this.pnlPercentQuantity = new System.Windows.Forms.Panel();
            this.txtPercentQuantity = new System.Windows.Forms.TextBox();
            this.pnltotalmoney = new System.Windows.Forms.Panel();
            this.txttotalmoney = new System.Windows.Forms.TextBox();
            this.pnlBarInterval = new System.Windows.Forms.Panel();
            this.txtBarInterval = new System.Windows.Forms.TextBox();
            this.pnlMaxStrategyOrderCount = new System.Windows.Forms.Panel();
            this.txtMaxStrategyOrderCount = new System.Windows.Forms.TextBox();
            this.pnlindiviualCount = new System.Windows.Forms.Panel();
            this.txtindiviualCount = new System.Windows.Forms.TextBox();
            this.pnldaysForHistroricalData = new System.Windows.Forms.Panel();
            this.txtdaysForHistroricalData = new System.Windows.Forms.TextBox();
            this.pnlIndividualMaxLossPercent = new System.Windows.Forms.Panel();
            this.txtIndividualMaxLossPercent = new System.Windows.Forms.TextBox();
            this.lblIntervl = new System.Windows.Forms.Label();
            this.pnlOverallProfitAmt = new System.Windows.Forms.Panel();
            this.txtOverallProfitAmt = new System.Windows.Forms.TextBox();
            this.lblTotalOpenPositions = new System.Windows.Forms.Label();
            this.pnlTotalOpenPositions = new System.Windows.Forms.Panel();
            this.txtTotalOpenPositions = new System.Windows.Forms.TextBox();
            this.lblOverallProfitAmt = new System.Windows.Forms.Label();
            this.lbldaysForHistroricalData = new System.Windows.Forms.Label();
            this.lblindiviualCount = new System.Windows.Forms.Label();
            this.lblIndividualMaxLossPercent = new System.Windows.Forms.Label();
            this.lblMaxStrategyOrderCount = new System.Windows.Forms.Label();
            this.lblTimeForExitAllOpenPosition = new System.Windows.Forms.Label();
            this.lbltotalmoney = new System.Windows.Forms.Label();
            this.lblPercentQuantity = new System.Windows.Forms.Label();
            this.lblTimeLimitToPlaceOrder = new System.Windows.Forms.Label();
            this.lblEndTime = new System.Windows.Forms.Label();
            this.lblStartTime = new System.Windows.Forms.Label();
            this.lblNoOfLots = new System.Windows.Forms.Label();
            this.btnApply = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOpenSymbolSetting = new System.Windows.Forms.Button();
            this.txtStartDataTime = new System.Windows.Forms.DateTimePicker();
            this.txtEndDataTime = new System.Windows.Forms.DateTimePicker();
            this.lblEndDataTime = new System.Windows.Forms.Label();
            this.lblStartDataTime = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.pnlBarCount.SuspendLayout();
            this.pnlPercentQuantity.SuspendLayout();
            this.pnltotalmoney.SuspendLayout();
            this.pnlBarInterval.SuspendLayout();
            this.pnlMaxStrategyOrderCount.SuspendLayout();
            this.pnlindiviualCount.SuspendLayout();
            this.pnldaysForHistroricalData.SuspendLayout();
            this.pnlIndividualMaxLossPercent.SuspendLayout();
            this.pnlOverallProfitAmt.SuspendLayout();
            this.pnlTotalOpenPositions.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtStartDataTime);
            this.groupBox1.Controls.Add(this.txtEndDataTime);
            this.groupBox1.Controls.Add(this.lblEndDataTime);
            this.groupBox1.Controls.Add(this.lblStartDataTime);
            this.groupBox1.Controls.Add(this.lblBarCount);
            this.groupBox1.Controls.Add(this.pnlBarCount);
            this.groupBox1.Controls.Add(this.txtStartTime);
            this.groupBox1.Controls.Add(this.txtEndTime);
            this.groupBox1.Controls.Add(this.txtTimeLimitToPlaceOrder);
            this.groupBox1.Controls.Add(this.txtTimeForExitAllOpenPosition);
            this.groupBox1.Controls.Add(this.pnlPercentQuantity);
            this.groupBox1.Controls.Add(this.pnltotalmoney);
            this.groupBox1.Controls.Add(this.pnlBarInterval);
            this.groupBox1.Controls.Add(this.pnlMaxStrategyOrderCount);
            this.groupBox1.Controls.Add(this.pnlindiviualCount);
            this.groupBox1.Controls.Add(this.pnldaysForHistroricalData);
            this.groupBox1.Controls.Add(this.pnlIndividualMaxLossPercent);
            this.groupBox1.Controls.Add(this.lblIntervl);
            this.groupBox1.Controls.Add(this.pnlOverallProfitAmt);
            this.groupBox1.Controls.Add(this.lblTotalOpenPositions);
            this.groupBox1.Controls.Add(this.pnlTotalOpenPositions);
            this.groupBox1.Controls.Add(this.lblOverallProfitAmt);
            this.groupBox1.Controls.Add(this.lbldaysForHistroricalData);
            this.groupBox1.Controls.Add(this.lblindiviualCount);
            this.groupBox1.Controls.Add(this.lblIndividualMaxLossPercent);
            this.groupBox1.Controls.Add(this.lblMaxStrategyOrderCount);
            this.groupBox1.Controls.Add(this.lblTimeForExitAllOpenPosition);
            this.groupBox1.Controls.Add(this.lbltotalmoney);
            this.groupBox1.Controls.Add(this.lblPercentQuantity);
            this.groupBox1.Controls.Add(this.lblTimeLimitToPlaceOrder);
            this.groupBox1.Controls.Add(this.lblEndTime);
            this.groupBox1.Controls.Add(this.lblStartTime);
            this.groupBox1.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(632, 677);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Trade Details";
            // 
            // lblBarCount
            // 
            this.lblBarCount.AutoSize = true;
            this.lblBarCount.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblBarCount.Location = new System.Drawing.Point(15, 308);
            this.lblBarCount.Name = "lblBarCount";
            this.lblBarCount.Size = new System.Drawing.Size(83, 19);
            this.lblBarCount.TabIndex = 85;
            this.lblBarCount.Text = "Bar Count";
            // 
            // pnlBarCount
            // 
            this.pnlBarCount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlBarCount.Controls.Add(this.txtBarCount);
            this.pnlBarCount.Location = new System.Drawing.Point(338, 300);
            this.pnlBarCount.Name = "pnlBarCount";
            this.pnlBarCount.Size = new System.Drawing.Size(277, 32);
            this.pnlBarCount.TabIndex = 7;
            this.pnlBarCount.TabStop = true;
            // 
            // txtBarCount
            // 
            this.txtBarCount.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtBarCount.Font = new System.Drawing.Font("Arial", 11F);
            this.txtBarCount.Location = new System.Drawing.Point(7, 4);
            this.txtBarCount.Name = "txtBarCount";
            this.txtBarCount.Size = new System.Drawing.Size(261, 22);
            this.txtBarCount.TabIndex = 8;
            // 
            // txtStartTime
            // 
            this.txtStartTime.CustomFormat = "HH:mm";
            this.txtStartTime.Font = new System.Drawing.Font("Arial", 11F);
            this.txtStartTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txtStartTime.Location = new System.Drawing.Point(338, 25);
            this.txtStartTime.Name = "txtStartTime";
            this.txtStartTime.ShowUpDown = true;
            this.txtStartTime.Size = new System.Drawing.Size(277, 29);
            this.txtStartTime.TabIndex = 1;
            // 
            // txtEndTime
            // 
            this.txtEndTime.CustomFormat = "HH:mm";
            this.txtEndTime.Font = new System.Drawing.Font("Arial", 11F);
            this.txtEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txtEndTime.Location = new System.Drawing.Point(338, 65);
            this.txtEndTime.Name = "txtEndTime";
            this.txtEndTime.ShowUpDown = true;
            this.txtEndTime.Size = new System.Drawing.Size(277, 29);
            this.txtEndTime.TabIndex = 2;
            // 
            // txtTimeLimitToPlaceOrder
            // 
            this.txtTimeLimitToPlaceOrder.CustomFormat = "HH:mm";
            this.txtTimeLimitToPlaceOrder.Font = new System.Drawing.Font("Arial", 11F);
            this.txtTimeLimitToPlaceOrder.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txtTimeLimitToPlaceOrder.Location = new System.Drawing.Point(338, 181);
            this.txtTimeLimitToPlaceOrder.Name = "txtTimeLimitToPlaceOrder";
            this.txtTimeLimitToPlaceOrder.ShowUpDown = true;
            this.txtTimeLimitToPlaceOrder.Size = new System.Drawing.Size(277, 29);
            this.txtTimeLimitToPlaceOrder.TabIndex = 3;
            // 
            // txtTimeForExitAllOpenPosition
            // 
            this.txtTimeForExitAllOpenPosition.CustomFormat = "HH:mm";
            this.txtTimeForExitAllOpenPosition.Font = new System.Drawing.Font("Arial", 11F);
            this.txtTimeForExitAllOpenPosition.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txtTimeForExitAllOpenPosition.Location = new System.Drawing.Point(338, 223);
            this.txtTimeForExitAllOpenPosition.Name = "txtTimeForExitAllOpenPosition";
            this.txtTimeForExitAllOpenPosition.ShowUpDown = true;
            this.txtTimeForExitAllOpenPosition.Size = new System.Drawing.Size(277, 29);
            this.txtTimeForExitAllOpenPosition.TabIndex = 4;
            // 
            // pnlPercentQuantity
            // 
            this.pnlPercentQuantity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPercentQuantity.Controls.Add(this.txtPercentQuantity);
            this.pnlPercentQuantity.Location = new System.Drawing.Point(338, 259);
            this.pnlPercentQuantity.Name = "pnlPercentQuantity";
            this.pnlPercentQuantity.Size = new System.Drawing.Size(277, 32);
            this.pnlPercentQuantity.TabIndex = 5;
            this.pnlPercentQuantity.TabStop = true;
            // 
            // txtPercentQuantity
            // 
            this.txtPercentQuantity.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPercentQuantity.Font = new System.Drawing.Font("Arial", 11F);
            this.txtPercentQuantity.Location = new System.Drawing.Point(7, 4);
            this.txtPercentQuantity.Name = "txtPercentQuantity";
            this.txtPercentQuantity.Size = new System.Drawing.Size(261, 22);
            this.txtPercentQuantity.TabIndex = 6;
            // 
            // pnltotalmoney
            // 
            this.pnltotalmoney.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnltotalmoney.Controls.Add(this.txttotalmoney);
            this.pnltotalmoney.Location = new System.Drawing.Point(338, 344);
            this.pnltotalmoney.Name = "pnltotalmoney";
            this.pnltotalmoney.Size = new System.Drawing.Size(277, 32);
            this.pnltotalmoney.TabIndex = 9;
            this.pnltotalmoney.TabStop = true;
            // 
            // txttotalmoney
            // 
            this.txttotalmoney.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txttotalmoney.Font = new System.Drawing.Font("Arial", 11F);
            this.txttotalmoney.Location = new System.Drawing.Point(7, 4);
            this.txttotalmoney.Name = "txttotalmoney";
            this.txttotalmoney.Size = new System.Drawing.Size(261, 22);
            this.txttotalmoney.TabIndex = 10;
            // 
            // pnlBarInterval
            // 
            this.pnlBarInterval.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlBarInterval.Controls.Add(this.txtBarInterval);
            this.pnlBarInterval.Location = new System.Drawing.Point(338, 385);
            this.pnlBarInterval.Name = "pnlBarInterval";
            this.pnlBarInterval.Size = new System.Drawing.Size(277, 32);
            this.pnlBarInterval.TabIndex = 11;
            this.pnlBarInterval.TabStop = true;
            // 
            // txtBarInterval
            // 
            this.txtBarInterval.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtBarInterval.Font = new System.Drawing.Font("Arial", 11F);
            this.txtBarInterval.Location = new System.Drawing.Point(7, 4);
            this.txtBarInterval.Name = "txtBarInterval";
            this.txtBarInterval.Size = new System.Drawing.Size(261, 22);
            this.txtBarInterval.TabIndex = 12;
            // 
            // pnlMaxStrategyOrderCount
            // 
            this.pnlMaxStrategyOrderCount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlMaxStrategyOrderCount.Controls.Add(this.txtMaxStrategyOrderCount);
            this.pnlMaxStrategyOrderCount.Location = new System.Drawing.Point(338, 424);
            this.pnlMaxStrategyOrderCount.Name = "pnlMaxStrategyOrderCount";
            this.pnlMaxStrategyOrderCount.Size = new System.Drawing.Size(277, 32);
            this.pnlMaxStrategyOrderCount.TabIndex = 13;
            this.pnlMaxStrategyOrderCount.TabStop = true;
            // 
            // txtMaxStrategyOrderCount
            // 
            this.txtMaxStrategyOrderCount.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtMaxStrategyOrderCount.Font = new System.Drawing.Font("Arial", 11F);
            this.txtMaxStrategyOrderCount.Location = new System.Drawing.Point(7, 4);
            this.txtMaxStrategyOrderCount.Name = "txtMaxStrategyOrderCount";
            this.txtMaxStrategyOrderCount.Size = new System.Drawing.Size(261, 22);
            this.txtMaxStrategyOrderCount.TabIndex = 14;
            // 
            // pnlindiviualCount
            // 
            this.pnlindiviualCount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlindiviualCount.Controls.Add(this.txtindiviualCount);
            this.pnlindiviualCount.Location = new System.Drawing.Point(338, 462);
            this.pnlindiviualCount.Name = "pnlindiviualCount";
            this.pnlindiviualCount.Size = new System.Drawing.Size(277, 32);
            this.pnlindiviualCount.TabIndex = 15;
            this.pnlindiviualCount.TabStop = true;
            // 
            // txtindiviualCount
            // 
            this.txtindiviualCount.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtindiviualCount.Font = new System.Drawing.Font("Arial", 11F);
            this.txtindiviualCount.Location = new System.Drawing.Point(7, 4);
            this.txtindiviualCount.Name = "txtindiviualCount";
            this.txtindiviualCount.Size = new System.Drawing.Size(261, 22);
            this.txtindiviualCount.TabIndex = 16;
            // 
            // pnldaysForHistroricalData
            // 
            this.pnldaysForHistroricalData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnldaysForHistroricalData.Controls.Add(this.txtdaysForHistroricalData);
            this.pnldaysForHistroricalData.Location = new System.Drawing.Point(338, 502);
            this.pnldaysForHistroricalData.Name = "pnldaysForHistroricalData";
            this.pnldaysForHistroricalData.Size = new System.Drawing.Size(277, 32);
            this.pnldaysForHistroricalData.TabIndex = 17;
            this.pnldaysForHistroricalData.TabStop = true;
            // 
            // txtdaysForHistroricalData
            // 
            this.txtdaysForHistroricalData.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtdaysForHistroricalData.Font = new System.Drawing.Font("Arial", 11F);
            this.txtdaysForHistroricalData.Location = new System.Drawing.Point(7, 4);
            this.txtdaysForHistroricalData.Name = "txtdaysForHistroricalData";
            this.txtdaysForHistroricalData.Size = new System.Drawing.Size(261, 22);
            this.txtdaysForHistroricalData.TabIndex = 18;
            // 
            // pnlIndividualMaxLossPercent
            // 
            this.pnlIndividualMaxLossPercent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlIndividualMaxLossPercent.Controls.Add(this.txtIndividualMaxLossPercent);
            this.pnlIndividualMaxLossPercent.Location = new System.Drawing.Point(339, 545);
            this.pnlIndividualMaxLossPercent.Name = "pnlIndividualMaxLossPercent";
            this.pnlIndividualMaxLossPercent.Size = new System.Drawing.Size(277, 32);
            this.pnlIndividualMaxLossPercent.TabIndex = 19;
            this.pnlIndividualMaxLossPercent.TabStop = true;
            // 
            // txtIndividualMaxLossPercent
            // 
            this.txtIndividualMaxLossPercent.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtIndividualMaxLossPercent.Font = new System.Drawing.Font("Arial", 11F);
            this.txtIndividualMaxLossPercent.Location = new System.Drawing.Point(7, 4);
            this.txtIndividualMaxLossPercent.Name = "txtIndividualMaxLossPercent";
            this.txtIndividualMaxLossPercent.Size = new System.Drawing.Size(265, 22);
            this.txtIndividualMaxLossPercent.TabIndex = 20;
            // 
            // lblIntervl
            // 
            this.lblIntervl.AutoSize = true;
            this.lblIntervl.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblIntervl.Location = new System.Drawing.Point(15, 391);
            this.lblIntervl.Name = "lblIntervl";
            this.lblIntervl.Size = new System.Drawing.Size(92, 19);
            this.lblIntervl.TabIndex = 83;
            this.lblIntervl.Text = "Bar Interval";
            // 
            // pnlOverallProfitAmt
            // 
            this.pnlOverallProfitAmt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlOverallProfitAmt.Controls.Add(this.txtOverallProfitAmt);
            this.pnlOverallProfitAmt.Location = new System.Drawing.Point(339, 588);
            this.pnlOverallProfitAmt.Name = "pnlOverallProfitAmt";
            this.pnlOverallProfitAmt.Size = new System.Drawing.Size(277, 32);
            this.pnlOverallProfitAmt.TabIndex = 21;
            this.pnlOverallProfitAmt.TabStop = true;
            // 
            // txtOverallProfitAmt
            // 
            this.txtOverallProfitAmt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtOverallProfitAmt.Font = new System.Drawing.Font("Arial", 11F);
            this.txtOverallProfitAmt.Location = new System.Drawing.Point(7, 4);
            this.txtOverallProfitAmt.Name = "txtOverallProfitAmt";
            this.txtOverallProfitAmt.Size = new System.Drawing.Size(265, 22);
            this.txtOverallProfitAmt.TabIndex = 22;
            // 
            // lblTotalOpenPositions
            // 
            this.lblTotalOpenPositions.AutoSize = true;
            this.lblTotalOpenPositions.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblTotalOpenPositions.Location = new System.Drawing.Point(15, 636);
            this.lblTotalOpenPositions.Name = "lblTotalOpenPositions";
            this.lblTotalOpenPositions.Size = new System.Drawing.Size(156, 19);
            this.lblTotalOpenPositions.TabIndex = 81;
            this.lblTotalOpenPositions.Text = "Total Open Positions";
            // 
            // pnlTotalOpenPositions
            // 
            this.pnlTotalOpenPositions.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlTotalOpenPositions.Controls.Add(this.txtTotalOpenPositions);
            this.pnlTotalOpenPositions.Location = new System.Drawing.Point(339, 630);
            this.pnlTotalOpenPositions.Name = "pnlTotalOpenPositions";
            this.pnlTotalOpenPositions.Size = new System.Drawing.Size(277, 32);
            this.pnlTotalOpenPositions.TabIndex = 22;
            this.pnlTotalOpenPositions.TabStop = true;
            // 
            // txtTotalOpenPositions
            // 
            this.txtTotalOpenPositions.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTotalOpenPositions.Font = new System.Drawing.Font("Arial", 11F);
            this.txtTotalOpenPositions.Location = new System.Drawing.Point(7, 4);
            this.txtTotalOpenPositions.Name = "txtTotalOpenPositions";
            this.txtTotalOpenPositions.Size = new System.Drawing.Size(265, 22);
            this.txtTotalOpenPositions.TabIndex = 23;
            // 
            // lblOverallProfitAmt
            // 
            this.lblOverallProfitAmt.AutoSize = true;
            this.lblOverallProfitAmt.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblOverallProfitAmt.Location = new System.Drawing.Point(15, 594);
            this.lblOverallProfitAmt.Name = "lblOverallProfitAmt";
            this.lblOverallProfitAmt.Size = new System.Drawing.Size(134, 19);
            this.lblOverallProfitAmt.TabIndex = 76;
            this.lblOverallProfitAmt.Text = "Overall Profit Amt";
            // 
            // lbldaysForHistroricalData
            // 
            this.lbldaysForHistroricalData.AutoSize = true;
            this.lbldaysForHistroricalData.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lbldaysForHistroricalData.Location = new System.Drawing.Point(15, 509);
            this.lbldaysForHistroricalData.Name = "lbldaysForHistroricalData";
            this.lbldaysForHistroricalData.Size = new System.Drawing.Size(194, 19);
            this.lbldaysForHistroricalData.TabIndex = 61;
            this.lbldaysForHistroricalData.Text = "Days For Histrorical Data";
            // 
            // lblindiviualCount
            // 
            this.lblindiviualCount.AutoSize = true;
            this.lblindiviualCount.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblindiviualCount.Location = new System.Drawing.Point(15, 470);
            this.lblindiviualCount.Name = "lblindiviualCount";
            this.lblindiviualCount.Size = new System.Drawing.Size(125, 19);
            this.lblindiviualCount.TabIndex = 59;
            this.lblindiviualCount.Text = "Individual Count";
            // 
            // lblIndividualMaxLossPercent
            // 
            this.lblIndividualMaxLossPercent.AutoSize = true;
            this.lblIndividualMaxLossPercent.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblIndividualMaxLossPercent.Location = new System.Drawing.Point(15, 551);
            this.lblIndividualMaxLossPercent.Name = "lblIndividualMaxLossPercent";
            this.lblIndividualMaxLossPercent.Size = new System.Drawing.Size(212, 19);
            this.lblIndividualMaxLossPercent.TabIndex = 68;
            this.lblIndividualMaxLossPercent.Text = "Individual Max Loss Percent";
            // 
            // lblMaxStrategyOrderCount
            // 
            this.lblMaxStrategyOrderCount.AutoSize = true;
            this.lblMaxStrategyOrderCount.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblMaxStrategyOrderCount.Location = new System.Drawing.Point(15, 431);
            this.lblMaxStrategyOrderCount.Name = "lblMaxStrategyOrderCount";
            this.lblMaxStrategyOrderCount.Size = new System.Drawing.Size(199, 19);
            this.lblMaxStrategyOrderCount.TabIndex = 57;
            this.lblMaxStrategyOrderCount.Text = "Max Strategy Order Count";
            // 
            // lblTimeForExitAllOpenPosition
            // 
            this.lblTimeForExitAllOpenPosition.AutoSize = true;
            this.lblTimeForExitAllOpenPosition.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblTimeForExitAllOpenPosition.Location = new System.Drawing.Point(15, 228);
            this.lblTimeForExitAllOpenPosition.Name = "lblTimeForExitAllOpenPosition";
            this.lblTimeForExitAllOpenPosition.Size = new System.Drawing.Size(232, 19);
            this.lblTimeForExitAllOpenPosition.TabIndex = 66;
            this.lblTimeForExitAllOpenPosition.Text = "Time For Exit All Open Position";
            // 
            // lbltotalmoney
            // 
            this.lbltotalmoney.AutoSize = true;
            this.lbltotalmoney.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lbltotalmoney.Location = new System.Drawing.Point(15, 351);
            this.lbltotalmoney.Name = "lbltotalmoney";
            this.lbltotalmoney.Size = new System.Drawing.Size(95, 19);
            this.lbltotalmoney.TabIndex = 60;
            this.lbltotalmoney.Text = "Total Money";
            // 
            // lblPercentQuantity
            // 
            this.lblPercentQuantity.AutoSize = true;
            this.lblPercentQuantity.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblPercentQuantity.Location = new System.Drawing.Point(15, 265);
            this.lblPercentQuantity.Name = "lblPercentQuantity";
            this.lblPercentQuantity.Size = new System.Drawing.Size(131, 19);
            this.lblPercentQuantity.TabIndex = 54;
            this.lblPercentQuantity.Text = "Percent Quantity";
            // 
            // lblTimeLimitToPlaceOrder
            // 
            this.lblTimeLimitToPlaceOrder.AutoSize = true;
            this.lblTimeLimitToPlaceOrder.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblTimeLimitToPlaceOrder.Location = new System.Drawing.Point(15, 188);
            this.lblTimeLimitToPlaceOrder.Name = "lblTimeLimitToPlaceOrder";
            this.lblTimeLimitToPlaceOrder.Size = new System.Drawing.Size(196, 19);
            this.lblTimeLimitToPlaceOrder.TabIndex = 59;
            this.lblTimeLimitToPlaceOrder.Text = "Time Limit To Place Order";
            // 
            // lblEndTime
            // 
            this.lblEndTime.AutoSize = true;
            this.lblEndTime.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblEndTime.Location = new System.Drawing.Point(15, 70);
            this.lblEndTime.Name = "lblEndTime";
            this.lblEndTime.Size = new System.Drawing.Size(77, 19);
            this.lblEndTime.TabIndex = 61;
            this.lblEndTime.Text = "End Time";
            // 
            // lblStartTime
            // 
            this.lblStartTime.AutoSize = true;
            this.lblStartTime.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblStartTime.Location = new System.Drawing.Point(15, 34);
            this.lblStartTime.Name = "lblStartTime";
            this.lblStartTime.Size = new System.Drawing.Size(82, 19);
            this.lblStartTime.TabIndex = 62;
            this.lblStartTime.Text = "Start Time";
            // 
            // lblNoOfLots
            // 
            this.lblNoOfLots.AutoSize = true;
            this.lblNoOfLots.Location = new System.Drawing.Point(20, 251);
            this.lblNoOfLots.Name = "lblNoOfLots";
            this.lblNoOfLots.Size = new System.Drawing.Size(186, 19);
            this.lblNoOfLots.TabIndex = 57;
            this.lblNoOfLots.Text = "Total Number of Lot Size";
            // 
            // btnApply
            // 
            this.btnApply.Font = new System.Drawing.Font("Arial", 11F);
            this.btnApply.Location = new System.Drawing.Point(333, 700);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(134, 43);
            this.btnApply.TabIndex = 25;
            this.btnApply.Text = "Apply";
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("Arial", 11F);
            this.btnCancel.Location = new System.Drawing.Point(494, 700);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(134, 43);
            this.btnCancel.TabIndex = 26;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOpenSymbolSetting
            // 
            this.btnOpenSymbolSetting.Font = new System.Drawing.Font("Arial", 11F);
            this.btnOpenSymbolSetting.Location = new System.Drawing.Point(14, 700);
            this.btnOpenSymbolSetting.Name = "btnOpenSymbolSetting";
            this.btnOpenSymbolSetting.Size = new System.Drawing.Size(197, 43);
            this.btnOpenSymbolSetting.TabIndex = 24;
            this.btnOpenSymbolSetting.Text = "Symbol Setting";
            this.btnOpenSymbolSetting.UseVisualStyleBackColor = true;
            this.btnOpenSymbolSetting.Click += new System.EventHandler(this.btnOpenSymbolSetting_Click);
            // 
            // txtStartDataTime
            // 
            this.txtStartDataTime.CustomFormat = "HH:mm";
            this.txtStartDataTime.Font = new System.Drawing.Font("Arial", 11F);
            this.txtStartDataTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txtStartDataTime.Location = new System.Drawing.Point(338, 102);
            this.txtStartDataTime.Name = "txtStartDataTime";
            this.txtStartDataTime.ShowUpDown = true;
            this.txtStartDataTime.Size = new System.Drawing.Size(277, 29);
            this.txtStartDataTime.TabIndex = 86;
            // 
            // txtEndDataTime
            // 
            this.txtEndDataTime.CustomFormat = "HH:mm";
            this.txtEndDataTime.Font = new System.Drawing.Font("Arial", 11F);
            this.txtEndDataTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txtEndDataTime.Location = new System.Drawing.Point(338, 142);
            this.txtEndDataTime.Name = "txtEndDataTime";
            this.txtEndDataTime.ShowUpDown = true;
            this.txtEndDataTime.Size = new System.Drawing.Size(277, 29);
            this.txtEndDataTime.TabIndex = 87;
            // 
            // lblEndDataTime
            // 
            this.lblEndDataTime.AutoSize = true;
            this.lblEndDataTime.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblEndDataTime.Location = new System.Drawing.Point(15, 147);
            this.lblEndDataTime.Name = "lblEndDataTime";
            this.lblEndDataTime.Size = new System.Drawing.Size(116, 19);
            this.lblEndDataTime.TabIndex = 88;
            this.lblEndDataTime.Text = "End Data Time";
            // 
            // lblStartDataTime
            // 
            this.lblStartDataTime.AutoSize = true;
            this.lblStartDataTime.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblStartDataTime.Location = new System.Drawing.Point(15, 111);
            this.lblStartDataTime.Name = "lblStartDataTime";
            this.lblStartDataTime.Size = new System.Drawing.Size(121, 19);
            this.lblStartDataTime.TabIndex = 89;
            this.lblStartDataTime.Text = "Start Data Time";
            // 
            // TradingSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(655, 750);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnOpenSymbolSetting);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.btnCancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "TradingSetting";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Trading Setting";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.pnlBarCount.ResumeLayout(false);
            this.pnlBarCount.PerformLayout();
            this.pnlPercentQuantity.ResumeLayout(false);
            this.pnlPercentQuantity.PerformLayout();
            this.pnltotalmoney.ResumeLayout(false);
            this.pnltotalmoney.PerformLayout();
            this.pnlBarInterval.ResumeLayout(false);
            this.pnlBarInterval.PerformLayout();
            this.pnlMaxStrategyOrderCount.ResumeLayout(false);
            this.pnlMaxStrategyOrderCount.PerformLayout();
            this.pnlindiviualCount.ResumeLayout(false);
            this.pnlindiviualCount.PerformLayout();
            this.pnldaysForHistroricalData.ResumeLayout(false);
            this.pnldaysForHistroricalData.PerformLayout();
            this.pnlIndividualMaxLossPercent.ResumeLayout(false);
            this.pnlIndividualMaxLossPercent.PerformLayout();
            this.pnlOverallProfitAmt.ResumeLayout(false);
            this.pnlOverallProfitAmt.PerformLayout();
            this.pnlTotalOpenPositions.ResumeLayout(false);
            this.pnlTotalOpenPositions.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblNoOfLots;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Panel pnlPercentQuantity;
        private System.Windows.Forms.TextBox txtPercentQuantity;
        private System.Windows.Forms.DateTimePicker txtStartTime;
        private System.Windows.Forms.DateTimePicker txtEndTime;
        private System.Windows.Forms.DateTimePicker txtTimeLimitToPlaceOrder;
        private System.Windows.Forms.Button btnOpenSymbolSetting;
        protected System.Windows.Forms.Label lblTimeLimitToPlaceOrder;
        protected System.Windows.Forms.Label lblEndTime;
        protected System.Windows.Forms.Label lblStartTime;
        protected System.Windows.Forms.Label lblPercentQuantity;
        private System.Windows.Forms.DateTimePicker txtTimeForExitAllOpenPosition;
        protected System.Windows.Forms.Label lblTimeForExitAllOpenPosition;
        private System.Windows.Forms.Panel pnlindiviualCount;
        private System.Windows.Forms.TextBox txtindiviualCount;
        protected System.Windows.Forms.Label lblindiviualCount;
        private System.Windows.Forms.Panel pnltotalmoney;
        private System.Windows.Forms.TextBox txttotalmoney;
        private System.Windows.Forms.Panel pnlMaxStrategyOrderCount;
        private System.Windows.Forms.TextBox txtMaxStrategyOrderCount;
        protected System.Windows.Forms.Label lblMaxStrategyOrderCount;
        protected System.Windows.Forms.Label lbltotalmoney;
        private System.Windows.Forms.Panel pnldaysForHistroricalData;
        private System.Windows.Forms.TextBox txtdaysForHistroricalData;
        protected System.Windows.Forms.Label lbldaysForHistroricalData;
        private System.Windows.Forms.Panel pnlTotalOpenPositions;
        private System.Windows.Forms.TextBox txtTotalOpenPositions;
        private System.Windows.Forms.Panel pnlOverallProfitAmt;
        private System.Windows.Forms.TextBox txtOverallProfitAmt;
        protected System.Windows.Forms.Label lblTotalOpenPositions;
        protected System.Windows.Forms.Label lblOverallProfitAmt;
        private System.Windows.Forms.Panel pnlIndividualMaxLossPercent;
        private System.Windows.Forms.TextBox txtIndividualMaxLossPercent;
        protected System.Windows.Forms.Label lblIndividualMaxLossPercent;
        private System.Windows.Forms.Panel pnlBarInterval;
        private System.Windows.Forms.TextBox txtBarInterval;
        protected System.Windows.Forms.Label lblIntervl;
        private System.Windows.Forms.Panel pnlBarCount;
        private System.Windows.Forms.TextBox txtBarCount;
        protected System.Windows.Forms.Label lblBarCount;
        private System.Windows.Forms.DateTimePicker txtStartDataTime;
        private System.Windows.Forms.DateTimePicker txtEndDataTime;
        protected System.Windows.Forms.Label lblEndDataTime;
        protected System.Windows.Forms.Label lblStartDataTime;
    }
}