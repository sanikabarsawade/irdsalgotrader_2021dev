﻿
using System;
using System.IO;
using System.Reflection;

namespace CryptoStrategyExecutor
{
    partial class FrmAutoTradingWindows
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent(string title)
        {
            this.SuspendLayout();
            // 
            // FrmAutoTradingWindows
            // 
            Assembly myAssembly = Assembly.GetExecutingAssembly();
            Stream IconStream = null;
            try
            {
                if (title != "")
                {
                    IconStream = myAssembly.GetManifestResourceStream("CryptoStrategyExecutor.Resources." + title + ".ico");
                    if (title.ToLower() == "irds algo trader")
                    {
                        this.Text = title + " 1.0.1";
                    }
                    else
                    {
                        this.Text = title;
                    }
                }
                else
                {
                    title = "IRDS Algo Trader";
                    IconStream = myAssembly.GetManifestResourceStream("CryptoStrategyExecutor.Resources." + title + ".ico");
                }
                this.Icon = new System.Drawing.Icon(IconStream);
            }
            catch (Exception ex)
            {
            }
            this.elementHost1 = new System.Windows.Forms.Integration.ElementHost();
            this.frmAutoTrading1 = new CryptoStrategyExecutor.FrmAutoTrading(this, title);
            this.SuspendLayout();
            // 
            // elementHost1
            // 

            this.elementHost1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.elementHost1.Location = new System.Drawing.Point(0, 0);
            this.elementHost1.Name = "elementHost1";
            this.elementHost1.Size = new System.Drawing.Size(685, 470);
            this.elementHost1.TabIndex = 0;
            this.elementHost1.Text = "elementHost1";
            this.elementHost1.Child = this.frmAutoTrading1;
            // 
            // FrmAutoTradingWindows
            // 

            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(685, 470);
            this.Controls.Add(this.elementHost1);
            this.Name = "FrmAutoTradingWindows";
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = title;
            this.MaximizeBox = false;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmAutoTradingWindows_FormClosing);
            this.ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Integration.ElementHost elementHost1;
        private FrmAutoTrading frmAutoTrading1;
    }
}