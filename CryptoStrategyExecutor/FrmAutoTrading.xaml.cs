﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using IRDSAlgoOMS;

namespace CryptoStrategyExecutor
{
    public partial class FrmAutoTrading : System.Windows.Controls.UserControl
    {
        private IRDSAlgoOMS.AlgoOMS AlgoOMS = new IRDSAlgoOMS.AlgoOMS();
        AutomationWithFile automationWithFile;
        bool login = false;
        bool automationStart = false;
        Logger logger = Logger.Instance;
        string userID = " ";
        ReadSettings Settings;
        public bool isChanbreakstrategySelect = false;
        public bool isOpenHighStrategySelect = false;
        public bool isChanbreakStopped = false;
        public bool isOpenHighStopped = false;
        string m_BrokerName = "Zerodha";
        string message = "";
        string title = "";
        MessageBoxButtons BtnOK = MessageBoxButtons.OK;
        FrmAutoTradingWindows m_objFrmAutoTradingWindows;
        BrushConverter bc = new BrushConverter();
        public FrmAutoTrading(FrmAutoTradingWindows objFrmAutoTradingWindows, string m_title)
        {
            InitializeComponent();
            RBOpenBreakoutStrategy.IsChecked = true;
            m_objFrmAutoTradingWindows = objFrmAutoTradingWindows;
            string path = Directory.GetCurrentDirectory();
            path += @"\Configuration\Setting.ini";
            INIFile iNIFile = new INIFile(path);

            if (Settings == null)
            {
                Settings = new ReadSettings(logger);
            }
            title = m_title;

            m_BrokerName = GetBroker(iNIFile);
            if (m_BrokerName == "Zerodha")
            {
                path = Directory.GetCurrentDirectory();
                string iniFile = path + @"\Configuration\" + "APISetting.ini";
                if (File.Exists(iniFile))
                {
                    Settings = new ReadSettings(logger, AlgoOMS);
                    userID = Settings.readUserId(iniFile);
                }
                else
                {
                    System.Windows.Forms.MessageBox.Show("INI file does not exit!!", title, BtnOK, MessageBoxIcon.Warning);
                }
                AlgoOMS.SetBroker(0);
                AlgoOMS.CreateKiteConnectObject(userID, "APISetting.ini", true);
            }
            else if (m_BrokerName == "Samco")
            {
                path = Directory.GetCurrentDirectory();
                string iniFile = path + @"\Configuration\" + "SamcoSettings.ini";
                if (File.Exists(iniFile))
                {
                    Settings = new ReadSettings(logger, AlgoOMS);
                    userID = Settings.readUserId(iniFile);
                }
                else
                {
                    System.Windows.Forms.MessageBox.Show("INI file does not exit!!", "Warning", BtnOK, MessageBoxIcon.Warning);
                }
                AlgoOMS.SetBroker(1);
                AlgoOMS.CreateKiteConnectObject(userID, "SamcoSettings.ini", true);
            }
            else if (m_BrokerName == "Composite")
            {
                path = Directory.GetCurrentDirectory();
                string iniFile = path + @"\Configuration\" + "CompositeSettings.ini";
                if (File.Exists(iniFile))
                {
                    Settings = new ReadSettings(logger, AlgoOMS);
                    userID = Settings.readUserId(iniFile);
                }
                else
                {
                    System.Windows.Forms.MessageBox.Show("INI file does not exit!!", "Warning", BtnOK, MessageBoxIcon.Warning);
                }
                AlgoOMS.SetBroker(2);
                AlgoOMS.CreateKiteConnectObject(userID, "CompositeSettings.ini", true);
            }
            AutomationNotstarted();
        }
        private void OnNetworkAvailabilityChanged(object sender, NetworkAvailabilityEventArgs e)
        {
            if (e.IsAvailable)
            {
                Console.WriteLine("Network has become available");
            }
            else
            {
                bool flag = false;
                try
                {
                    using (var client = new WebClient())
                    using (client.OpenRead("http://clients3.google.com/generate_204"))
                    {
                        flag = true;
                    }
                }
                catch
                {
                    flag = false;
                }
                if (!flag)
                {
                    System.Windows.Forms.MessageBox.Show("Please check internet connection!!", title, BtnOK, MessageBoxIcon.Warning);
                }

            }
        }
        private void BtnLogin_Click(object sender, RoutedEventArgs e)
        {
            loginToAccount();
            if (!login)
            {
                System.Windows.Forms.MessageBox.Show("Not able to login.Please try with forcefully login!!", title, BtnOK, MessageBoxIcon.Warning);
                logger.LogMessage("Not able to login.Please try with forcefully login", MessageType.Informational);
            }
            else if (login)
            {
                System.Windows.Forms.MessageBox.Show("Logged in successfully!!", title, BtnOK, MessageBoxIcon.Information);
                logger.LogMessage("Logged on successfully", MessageType.Informational);
            }
        }

        private void BtnClose_Click(object sender, RoutedEventArgs e)
        {
            AlgoOMS.AddNotificationInQueue(userID, "Client : " + userID + "\nExe closed.\n" + DateTime.Now);
            m_objFrmAutoTradingWindows.Close();
        }

        private void BtnStartAutomation_Click(object sender, RoutedEventArgs e)
        {
            StartAutomation();
        }

        private void BtnStopAutomation_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                logger.LogMessage("Clicked on stop button", MessageType.Informational);
                if (login)
                {
                    if (automationStart)
                    {
                        bool res = false;
                        if (isChanbreakstrategySelect)
                        {
                            res = automationWithFile.StopThread();
                        }
                        if (res)
                        {
                            System.Windows.Forms.MessageBox.Show("Automation stopped", title, BtnOK, MessageBoxIcon.Exclamation);
                            logger.LogMessage("Automation stopped", MessageType.Informational);
                            automationStart = false;
                            Automationstopped();
                            BtnStartAutomation.IsEnabled = true;
                            RBChannelBreakoutstrategy.IsEnabled = true;
                            RBOpenBreakoutStrategy.IsEnabled = true;
                        }
                        else
                        {
                            System.Windows.Forms.MessageBox.Show("Unable to stop automation.", title, BtnOK, MessageBoxIcon.Warning);
                            logger.LogMessage("Unable to stop automation", MessageType.Informational);
                        }

                    }
                    else
                    {
                        System.Windows.Forms.MessageBox.Show("To perform stop. first Start the automation.", title, BtnOK, MessageBoxIcon.Warning);
                        logger.LogMessage("To perform stop. first Start the automation", MessageType.Informational);
                    }
                }
                else
                {
                    System.Windows.Forms.MessageBox.Show("Stop operation not able to perform.", title, BtnOK, MessageBoxIcon.Warning);
                    logger.LogMessage("Stop operation not able to perform", MessageType.Informational);
                }
            }
            catch (Exception er)
            {
                System.Windows.Forms.MessageBox.Show("Error : " + er.Message, title, BtnOK, MessageBoxIcon.Error);
                logger.LogMessage("Error : " + er.Message, MessageType.Informational);
            }
        }

        private void BtnStopAndCloseOrders_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                logger.LogMessage("Clicked on stop automation and close all order button", MessageType.Informational);
                if (login)
                {
                    if (automationStart)
                    {
                        bool res = false;
                        if (isChanbreakstrategySelect)
                        {
                            AlgoOMS.AddNotificationInQueue(userID, "Client : " + userID + "\nAutomation stopped and closed open orders. Clicked on Stop Automation and close all orders button\n" + DateTime.Now);
                            res = automationWithFile.StopThread();
                            automationWithFile.CloseAllOrder();
                        }
                        if (res)
                        {
                            System.Windows.Forms.MessageBox.Show("Automation stopped.", title, BtnOK, MessageBoxIcon.Warning);
                            logger.LogMessage("Automation stopped", MessageType.Informational);
                            automationStart = false;
                            Automationstopped();
                            BtnStartAutomation.IsEnabled = true;
                            RBChannelBreakoutstrategy.IsEnabled = true;
                            RBOpenBreakoutStrategy.IsEnabled = true;
                        }
                        else
                        {
                            System.Windows.Forms.MessageBox.Show("Unable to stop automation.", title, BtnOK, MessageBoxIcon.Warning);
                            logger.LogMessage("Unable to stop automation", MessageType.Informational);
                        }

                    }
                    else
                    {
                        System.Windows.Forms.MessageBox.Show("To perform stop. first Start the automation.", title, BtnOK, MessageBoxIcon.Information);
                        logger.LogMessage("To perform stop. first Start the automation", MessageType.Informational);
                    }
                }
                else
                {
                    System.Windows.Forms.MessageBox.Show("Stop operation not able to perform.", title, BtnOK, MessageBoxIcon.Warning);
                    logger.LogMessage("Stop operation not able to perform", MessageType.Informational);
                }
            }
            catch (Exception er)
            {
                System.Windows.Forms.MessageBox.Show("Error : " + er.Message, title, BtnOK, MessageBoxIcon.Error);
                logger.LogMessage("Error : " + er.Message, MessageType.Informational);
            }
        }

        private void BtnTradingSetting_Click(object sender, RoutedEventArgs e)
        {
            int choice = 0;
            Logger logger = Logger.Instance;
            //02-AUG-2021::Pratiksha::Show fields on the basis of choice
            if (RBOpenBreakoutStrategy.IsChecked == true)
            {
                choice = 0;
            }
            else if (RBChannelBreakoutstrategy.IsChecked == true)
            {
                choice = 1;
            }
            TradingSetting obj = new TradingSetting(logger, AlgoOMS, choice);
            obj.ShowDialog();
        }

        private void BtnSymbolSetting_Click(object sender, RoutedEventArgs e)
        {
            CryptoStrategyExecutor.SymbolSetting obj = new CryptoStrategyExecutor.SymbolSetting(AlgoOMS, Settings);
            obj.ShowDialog();
        }

        private void MainGrid_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                NetworkChange.NetworkAvailabilityChanged +=
                 new NetworkAvailabilityChangedEventHandler(OnNetworkAvailabilityChanged);
                if (m_BrokerName == "Zerodha")
                {
                    var path = Directory.GetCurrentDirectory();
                    string iniFile = path + @"\Configuration\" + "APISetting.ini";
                    if (File.Exists(iniFile))
                    {
                        Settings = new ReadSettings(logger, AlgoOMS);
                        userID = Settings.readUserId(iniFile);
                    }
                    else
                    {
                        System.Windows.Forms.MessageBox.Show("INI file does not exit!!", title, BtnOK, MessageBoxIcon.Warning);
                    }
                }
                else if (m_BrokerName == "Samco")
                {
                    var path = Directory.GetCurrentDirectory();
                    string iniFile = path + @"\Configuration\" + "SamcoSettings.ini";
                    if (File.Exists(iniFile))
                    {
                        Settings = new ReadSettings(logger, AlgoOMS);
                        userID = Settings.readUserId(iniFile);
                    }
                    else
                    {
                        System.Windows.Forms.MessageBox.Show("INI file does not exit!!", title, BtnOK, MessageBoxIcon.Warning);
                    }
                }
                else if (m_BrokerName == "Composite")
                {
                    var path = Directory.GetCurrentDirectory();
                    string iniFile = path + @"\Configuration\" + "CompositeSettings.ini";
                    if (File.Exists(iniFile))
                    {
                        Settings = new ReadSettings(logger, AlgoOMS);
                        userID = Settings.readUserId(iniFile);
                    }
                    else
                    {
                        System.Windows.Forms.MessageBox.Show("INI file does not exit!!", title, BtnOK, MessageBoxIcon.Warning);
                    }
                }

                loginToAccount();
            }
            catch (Exception er)
            {
                Trace.WriteLine("exception from form load : " + er.Message);
            }
        }
        public void loginToAccount()
        {
            if (RBChannelBreakoutstrategy.IsChecked == true)
            {
                isChanbreakstrategySelect = true;
            }
            if (RBOpenBreakoutStrategy.IsChecked == true)
            {
                isOpenHighStrategySelect = true;
            }

            if (isChanbreakstrategySelect == false && isOpenHighStrategySelect == false)
            {
                System.Windows.Forms.MessageBox.Show("Please Select strategy first.", title, BtnOK, MessageBoxIcon.Warning);
            }
            else
            {
                BtnLogin.IsEnabled = false;
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                string error = "";
                int loginCount = 0;
                try
                {
                    logger.LogMessage("Clicked on login button", MessageType.Informational);
                    AlgoOMS.Connect(userID);
                    while (loginCount < 3)
                    {
                        if (AlgoOMS.kiteWrapperConnect != null || AlgoOMS.samcoWrapperConnect != null || AlgoOMS.compositeWrapperConnect !=null)
                        {
                            if (AlgoOMS.GetLoginStatus(userID))
                            {
                                login = true;                                
                                automationWithFile = new AutomationWithFile(AlgoOMS, logger, userID);

                                automationWithFile.loadObject(this);

                                automationWithFile.readTradingSymbol(Settings);
                               
                                break;
                            }
                            else
                            {
                                login = false;
                                logger.LogMessage("Not able to login trying " + loginCount, MessageType.Informational);
                            }
                        }
                        loginCount++;
                    }

                }
                catch (Exception er)
                {
                    error = er.Message;
                    if (error.Contains("empty") && error.Contains("api_key") && error.Contains("access_token"))
                    {
                        logger.LogMessage("Error : " + er.Message, MessageType.Exception);
                        message = "Please check account credentials.";
                        System.Windows.Forms.MessageBox.Show(message, title, BtnOK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        logger.LogMessage("Error : " + er.Message, MessageType.Exception);
                        message = "INI file does not exit!!";
                        System.Windows.Forms.MessageBox.Show(message, title, BtnOK, MessageBoxIcon.Error);
                    }
                }
                finally
                {
                    if (error.Contains("System.Data.SQLite"))
                    {
                        message = "Please add dll for sqlite.";
                        System.Windows.Forms.MessageBox.Show(message, title, BtnOK, MessageBoxIcon.Error);
                        logger.LogMessage("Please add dll for sqlite", MessageType.Informational);
                        m_objFrmAutoTradingWindows.Close();
                    }
                }
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }
        }
        public void DownloadData()
        {
            logger.LogMessage("Started Download", MessageType.Informational);
            var path = Directory.GetCurrentDirectory();
            string iniFile = path + @"\Configuration\";
            if (isChanbreakstrategySelect)
            {
                Settings.readTradingSymbols(iniFile + "Crypto_Sandip.ini");
                Settings.readDayLimit(iniFile + "Crypto_Sandip.ini");
                AlgoOMS.DownloadData(userID, Settings.TradingsymbolList, Settings.m_DaysForHistroricalData);
            }
        }
        public OpenOrderInfo GetObj()
        {
            if (automationWithFile != null)
                return automationWithFile.m_OrderInfo;
            return null;
        }
        public void SaveBin()
        {
            var path = Directory.GetCurrentDirectory();
            string FileName = path + @"\\Chanbreak.bin";//sanika::12-Feb-2021::changed file name
            Stream SaveFileStream = File.Create(FileName);
            BinaryFormatter serializer = new BinaryFormatter();
            OpenOrderInfo openOrderInfo = GetObj();
            if (openOrderInfo != null)
                serializer.Serialize(SaveFileStream, openOrderInfo);
            SaveFileStream.Close();
        }
        string GetBroker(INIFile iNIFile)
        {
            string brokerName = "";
            try
            {
                brokerName = iNIFile.IniReadValue("BROKER", "brokername");
            }
            catch (Exception e)
            {
                Trace.WriteLine("exception " + e.Message);
            }
            if (brokerName == "")
            {
                brokerName = "Zerodha";
            }
            return brokerName;
        }

        public void StartAutomation()
        {
            BtnStartAutomation.IsEnabled = false;
            try
            {
                logger.LogMessage("Clicked on start button", MessageType.Informational);
                if (login)
                {
                    automationStart = true;
                    Automationstarted();
                    
                        var path = Directory.GetCurrentDirectory();
                        string iniFile = path + @"\Configuration\";
                        bool autoDownload = true;
                        if (autoDownload)
                        {
                            logger.LogMessage("autoDownload flag for historical data is true", MessageType.Informational);
                            DownloadData();
                        }
                        else
                        {
                            logger.LogMessage("autoDownload flag for historical data is false", MessageType.Informational);
                        }
                        //sanika::30-July-2021::created automation object
                        if (automationWithFile == null)
                        {
                            automationWithFile = new AutomationWithFile(AlgoOMS, logger, userID);

                            automationWithFile.loadObject(this);

                            automationWithFile.readTradingSymbol(Settings);
                            automationWithFile.loadObject(this);
                        }
                        bool res = automationWithFile.startThread(userID);
                        if (!res)
                        {
                            BtnStartAutomation.IsEnabled = true;
                            System.Windows.Forms.MessageBox.Show("Some values are missing in TRADINGSYMBOL section.. Please check ini file.", title, BtnOK, MessageBoxIcon.Exclamation);
                        }
                }
                else
                {
                    if (isOpenHighStrategySelect == false && isChanbreakstrategySelect == false)
                    {
                        System.Windows.Forms.MessageBox.Show("Please Select strategy first.", title, BtnOK, MessageBoxIcon.Warning);
                        BtnStartAutomation.IsEnabled = true;
                        RBChannelBreakoutstrategy.IsEnabled = true;
                        RBOpenBreakoutStrategy.IsEnabled = true;
                    }
                    else
                    {
                        AlgoOMS.CreateKiteConnectObject(userID, "APISetting.ini", true);
                        login = true;
                        BtnStartAutomation.IsEnabled = false;
                        logger.LogMessage("Login successfully", MessageType.Informational);
                        automationStart = true;
                        BtnLogin.IsEnabled = false;
                        if (isChanbreakstrategySelect)
                        {
                            automationWithFile = new AutomationWithFile(AlgoOMS, logger, userID);
                            automationWithFile.loadObject(this);
                            automationWithFile.readTradingSymbol(Settings);
                            bool res = automationWithFile.startThread(userID);
                            if (!res)
                            {
                                BtnStartAutomation.IsEnabled = true;
                                RBChannelBreakoutstrategy.IsEnabled = true;
                                RBOpenBreakoutStrategy.IsEnabled = true;
                                System.Windows.Forms.MessageBox.Show("Some values are missing in TRADINGSYMBOL section.. Please check ini file.", title, BtnOK, MessageBoxIcon.Exclamation);
                            }
                        }
                    }
                }
            }
            catch (Exception er)
            {
                System.Windows.Forms.MessageBox.Show("Error : " + er.Message, title, BtnOK, MessageBoxIcon.Error);
                logger.LogMessage("Error : " + er.Message, MessageType.Exception);
            }
        }
        public void enableButton()
        {
            this.Dispatcher.Invoke(() =>
            {
                BtnStartAutomation.IsEnabled = true;
            });
        }
        public void formClose(FormClosingEventArgs e)
        {
            try
            {
                string message = "Do you really want to exit from application?";
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                DialogResult dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                if (dialog == DialogResult.Yes)
                {
                    logger.LogMessage("Message box display and clicked on yes!!", MessageType.Informational);
                    //sanika::25-sep-2020::added for resume testing
                    SaveBin();
                    logger.LogMessage("clicked on close form", MessageType.Informational);
                    if (login)
                    {
                        bool res = false;
                        if (AlgoOMS.stopThread())
                        {
                            logger.LogMessage("All threads are closed", MessageType.Informational);
                        }
                        if (isChanbreakstrategySelect)
                        {
                            //automationWithFile.SaveBin();
                            res = automationWithFile.StopThread();
                            if (res)
                            {
                                logger.LogMessage("Chanbreak threads are closed", MessageType.Informational);

                            }
                            else
                                logger.LogMessage("Threads are not able to close", MessageType.Informational);
                        }
                        Environment.Exit(0);
                    }
                }
                else
                {
                    e.Cancel = true;
                    logger.LogMessage("Message box display and clicked on No!!", MessageType.Informational);
                }

            }
            catch (Exception er)
            {
                System.Windows.Forms.MessageBox.Show("Error : " + er.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                logger.LogMessage("Error : " + er.Message, MessageType.Exception);
            }
        }
        private void AutomationNotstarted()
        {
            LblStatusValue.Content = "Waiting";
            LblStatusValue.Background = new SolidColorBrush(Colors.Blue);
            LblStatusValue.Foreground = new SolidColorBrush(Colors.White);
        }
        private void Automationstarted()
        {
            LblStatusValue.Content = "Started";
            LblStatusValue.Background = new SolidColorBrush(Colors.LimeGreen);
            LblStatusValue.Foreground = new SolidColorBrush(Colors.White);
        }
        private void Automationstopped()
        {
            LblStatusValue.Content = "Stopped";
            LblStatusValue.Background = new SolidColorBrush(Colors.Red);
            LblStatusValue.Foreground = new SolidColorBrush(Colors.White);
        }

        //sanika::18-Aug-2021::added check for strategy selection
        private void RBOpenBreakoutStrategy_Checked(object sender, RoutedEventArgs e)
        {
            isOpenHighStrategySelect = true;
            isChanbreakstrategySelect = false; 
        }

        private void RBChannelBreakoutstrategy_Checked(object sender, RoutedEventArgs e)
        {
            isOpenHighStrategySelect = false;
            isChanbreakstrategySelect = true;
        }
    }
}
