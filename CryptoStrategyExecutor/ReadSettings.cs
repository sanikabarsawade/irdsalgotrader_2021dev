﻿using IRDSAlgoOMS;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace CryptoStrategyExecutor
{   
    public  class ReadSettings
    {
        static INIFile inif;
        public  string username = "";
        public double CommanLotSize = 0;
        public  decimal overallProfit = 0;
        public  decimal overallLoss = 0;
        //26-Jully-2021::Pratiksha::For Having global name starts with m_
        public string m_StartTime = "";
        public string m_EndTime = " ";
        public string m_TimeLimitToPlaceOrder = "";
        public string m_TimeForExitAllOpenPosition = "";
        public double m_PercentQuantity = 0;
        public double m_TotalMoney = 0;
        public double m_BarInterval = 0;
        public double m_MaxStrategyOrderCount = 0;
        public double m_IndividualCount = 0;
        public double m_IndividualMaxLossPercent = 0;
        public double m_OverallProfitAmt = 0;
        public double m_TotalOpenPositions = 0;
        //02-AUG-2021::Pratiksha::Added new field BarCount
        public double m_BarCount = 0;
        public string interval = "";
        public List<string> tradingSymbolListWithAllData = new List<string>();
        public TradingSymbolInfo tradingSymbolInfo;
        public List<TradingSymbolInfo> TradingSymbolsInfoList = new List<TradingSymbolInfo>();

        public List<string> TradingsymbolList = new List<string>();
        public List<string> TradingsymbolListWithFutureName = new List<string>();
        Logger logger;
        AlgoOMS AlgoOMS;
        //public bool isReadDataFromDB = true;
        public double Percent = 0;
        public double ProfitTrail = 0;
        public bool m_AutoDowbloadHistoricalData = false;
        public double m_MaxLossValueForIndividualSymbol = 0;
        public double m_OverallProfitPercent = 0;
        public int m_DaysForHistroricalData = 0;
        List<string> PlainDetList;
        public string m_Title = "";
        //05-AUG-2021::Pratiksha::Added 2 new fields for start data and end data
        public string m_StartDataTime = "";
        public string m_EndDataTime = " ";
        public ReadSettings()
        { 
        }
        public ReadSettings(Logger logger)
        {
            this.logger = logger;
        }
        public ReadSettings(Logger logger,AlgoOMS AlgoOMS )
        {
            if(this.logger == null)
                this.logger = logger;
            this.AlgoOMS = AlgoOMS;
        }

        public string readUserId(string filepath)
        {
            string userId = "";
            try
            {
                //logger.LogMessage("In readUserId", MessageType.Informational);
                inif = new INIFile(filepath);
                userId = inif.IniReadValue("credentials", "MyUserId");
                List<string> list = new List<string>();
                list.Add(userId);
                decryptDet(list);
                if (PlainDetList.Count > 0)
                    username = userId = PlainDetList[0];
                else
                    username = userId;
            }
            catch(Exception e)
            {
                logger.LogMessage("readUserId : Exception Error Message = "+e.Message, MessageType.Exception);
            }
            return userId;
        }

        public void readTradingSymbols(string filepath)
        {
            try
            {
                TradingsymbolList.Clear();
                //logger.LogMessage("In readTradingSymbols", MessageType.Informational);
                inif = new INIFile(filepath);
                List<string> AllData = inif.GetKeyValues("TRADINGSYMBOL");
                foreach (var symbol in AllData)
                {
                    string[] str = symbol.Split(',');
                    TradingsymbolList.Add((str[0]));
                }
            }
            catch (Exception e)
            {
                logger.LogMessage("readTradingSymbols : Exception Error Message = "+e.Message, MessageType.Informational);
            }
        }

        public void readDayLimit(string filepath)
        {
            try
            {
                inif = new INIFile(filepath);
                m_DaysForHistroricalData = Convert.ToInt32(inif.IniReadValue("SDATA", "DaysForHistroricalData"));
            }
            catch (Exception e)
            {
                logger.LogMessage("readDayLimit : Exception Error Message = " + e.Message, MessageType.Informational);
            }
        }

        //sanika::10-dec-2020::Added check to auto download historical data
        public bool ReadAutoDownloadFlag(string filepath)
        {
            try
            {
                inif = new INIFile(filepath);
                m_AutoDowbloadHistoricalData = Convert.ToBoolean(inif.IniReadValue("SDATA", "AutoDownloadHistoricalData"));
            }
            catch (Exception e)
            {
                logger.LogMessage("readDayLimit : Exception Error Message = " + e.Message, MessageType.Informational);
            }
            return m_AutoDowbloadHistoricalData;
        }

        public bool readCrypto_SandipConfigFile(string filename)
        {
            bool isload = true;
            try
            {
                inif = new INIFile(filename);
                m_StartTime = inif.IniReadValue("SDATA", "StartTime");
                m_EndTime = inif.IniReadValue("SDATA", "EndTime");
                //05-AUG-2021::Pratiksha::Added 2 new fields for start data and end data
                m_StartDataTime = inif.IniReadValue("SDATA", "StartDataTime");
                m_EndDataTime = inif.IniReadValue("SDATA", "EndDataTime");
                m_TimeLimitToPlaceOrder = inif.IniReadValue("SDATA", "TimeLimitToPlaceOrder");
                m_TimeForExitAllOpenPosition = inif.IniReadValue("SDATA", "TimeForExitAllOpenPosition");
                m_PercentQuantity = Convert.ToDouble(inif.IniReadValue("SDATA", "PercentQuantity"));
                //02-AUG-2021::Pratiksha::Added new field BarCount
                m_BarCount = Convert.ToDouble(inif.IniReadValue("SDATA", "BarCount"));
                m_TotalMoney = Convert.ToDouble(inif.IniReadValue("SDATA", "TotalMoney"));
                m_BarInterval = Convert.ToDouble(inif.IniReadValue("SDATA", "BarInterval"));
                m_MaxStrategyOrderCount = Convert.ToDouble(inif.IniReadValue("SDATA", "MaxStrategyOrderCount"));
                m_IndividualCount = Convert.ToDouble(inif.IniReadValue("SDATA", "IndividualCount"));
                m_DaysForHistroricalData = Convert.ToInt32(inif.IniReadValue("SDATA", "DaysForHistroricalData"));
                m_IndividualMaxLossPercent = Convert.ToDouble(inif.IniReadValue("SDATA", "IndividualMaxLossPercent"));
                m_OverallProfitAmt = Convert.ToDouble(inif.IniReadValue("SDATA", "OverallProfitAmt"));
                m_TotalOpenPositions = Convert.ToInt32(inif.IniReadValue("SDATA", "TotalOpenPositions"));
                //29-Jully-2021::Pratiksha::Facing problem while importing, so added method which read trading symbols
                ReadTradingSymbols(filename);
            }
            catch (Exception e)
            {
                logger.LogMessage("readConfigFile : Exception Error Message =" + e.Message, MessageType.Informational);
            }
            return isload;
        }

        public void ReadTradingSymbols(string filename)
        {
            TradingSymbolsInfoList.Clear();
            inif = new INIFile(filename);
            tradingSymbolListWithAllData = inif.GetKeyValues("TRADINGSYMBOL");
            TradingsymbolList.Clear();
            TradingsymbolListWithFutureName.Clear();
            foreach (var symbol in tradingSymbolListWithAllData)
            {
                string[] values = symbol.Split(',');
                if (!(values.Length < 4))
                {
                    tradingSymbolInfo = new TradingSymbolInfo();
                    TradingsymbolList.Add(values[0]);
                    tradingSymbolInfo.SymbolWithExchange = values[0];
                    tradingSymbolInfo.BarCount = Convert.ToDouble(values[1]);
                    tradingSymbolInfo.ProfitPercent = Convert.ToDouble(values[2]);
                    tradingSymbolInfo.Stoploss = Convert.ToDouble(values[3]);
                    tradingSymbolInfo.ProfitTrail = Convert.ToDouble(values[4]);
                    string exchnage = values[0].Split('.')[1];
                    tradingSymbolInfo.Exchange = exchnage;
                    string TradingSymbol = values[0].Split('.')[0];
                    tradingSymbolInfo.TradingSymbol = TradingSymbol;
                    if (exchnage == "NFO")
                    {
                        TradingSymbol = this.AlgoOMS.GetFutureSymbolName(username, TradingSymbol);
                    }
                    TradingsymbolListWithFutureName.Add(TradingSymbol);
                }
                else
                {
                    //isload = false;
                    logger.LogMessage("Some values are missing in TRADINGSYMBOL section.. Please check ini file", MessageType.Informational);
                    break;
                }
                var value = TradingSymbolsInfoList.Find(r => r.TradingSymbol == tradingSymbolInfo.TradingSymbol);
                if (value == null)
                {
                    TradingSymbolsInfoList.Add(tradingSymbolInfo);
                }
            }

            //return isload;
        }

        public void decryptDet(List<string> EncDetList)
        {
            try
            {
                if(PlainDetList == null)
                    PlainDetList = new List<string>();

                PlainDetList.Clear();
                for (int i = 0; i < EncDetList.Count; i++)
                {
                    string plaintext = DecryptData(EncDetList[i], "IrdsalgoTrader");
                    PlainDetList.Add(plaintext);
                }
            }
            catch (Exception e)
            {
                logger.LogMessage("decryptDet : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        public static string DecryptData(string strCryptTxt, string strKey)
        {
            strCryptTxt = strCryptTxt.Replace(" ", "+");

            byte[] bytesBuff = Convert.FromBase64String(strCryptTxt);

            using (Aes aes__1 = Aes.Create())
            {
                Rfc2898DeriveBytes crypto = new Rfc2898DeriveBytes(strKey, new byte[] {
                0x49,
                0x76,
                0x61,
                0x6e,
                0x20,
                0x4d,
                0x65,
                0x64,
                0x76,
                0x65,
                0x64,
                0x65,
                0x76
            });
                aes__1.Key = crypto.GetBytes(32);
                aes__1.IV = crypto.GetBytes(16);

                using (MemoryStream mStream = new MemoryStream())
                {
                    using (CryptoStream cStream = new CryptoStream(mStream, aes__1.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cStream.Write(bytesBuff, 0, bytesBuff.Length);
                        cStream.Close();
                    }

                    strCryptTxt = Encoding.Unicode.GetString(mStream.ToArray());
                }
            }
            return strCryptTxt;
        }
        public void writeINIFileSymbolSetting(string section, string key, string value)
        {
            inif.IniWriteValueSymbolSetting(section, key, value);
        }

        public string ReadTitle(string filename)
        {
            
            try
            {
                inif = new INIFile(filename);
                m_Title = inif.IniReadValue("Title", "title");

            }
            catch (Exception ex)
            {
                logger.LogMessage("ReadTitle : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
            return m_Title;
        }
    }
}
