﻿namespace CryptoStrategyExecutor
{
    partial class form_autoTrading
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(form_autoTrading));
            this.btnLogin = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.CloseBtn = new System.Windows.Forms.Button();
            this.btnStopCloseOrder = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.RBChannelBreakoutstrategy = new System.Windows.Forms.CheckBox();
            this.RBOpenBreakoutStrategy = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnSymbolSetting = new System.Windows.Forms.Button();
            this.BtnTradingSetting = new System.Windows.Forms.Button();
            this.groupBoxAutomationDetails = new System.Windows.Forms.GroupBox();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBoxAutomationDetails.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnLogin
            // 
            this.btnLogin.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogin.Location = new System.Drawing.Point(15, 26);
            this.btnLogin.Margin = new System.Windows.Forms.Padding(4);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(185, 47);
            this.btnLogin.TabIndex = 0;
            this.btnLogin.Text = "Login";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // btnStart
            // 
            this.btnStart.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStart.Location = new System.Drawing.Point(15, 25);
            this.btnStart.Margin = new System.Windows.Forms.Padding(4);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(185, 47);
            this.btnStart.TabIndex = 2;
            this.btnStart.Text = "Start Automation";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnStop
            // 
            this.btnStop.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStop.Location = new System.Drawing.Point(222, 25);
            this.btnStop.Margin = new System.Windows.Forms.Padding(4);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(185, 47);
            this.btnStop.TabIndex = 3;
            this.btnStop.Text = "Stop Automation";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.CloseBtn);
            this.groupBox2.Controls.Add(this.btnLogin);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(16, 127);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(645, 89);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Login Details";
            // 
            // CloseBtn
            // 
            this.CloseBtn.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CloseBtn.Location = new System.Drawing.Point(222, 26);
            this.CloseBtn.Margin = new System.Windows.Forms.Padding(4);
            this.CloseBtn.Name = "CloseBtn";
            this.CloseBtn.Size = new System.Drawing.Size(185, 47);
            this.CloseBtn.TabIndex = 7;
            this.CloseBtn.Text = "Close";
            this.CloseBtn.UseVisualStyleBackColor = true;
            this.CloseBtn.Click += new System.EventHandler(this.CloseBtn_Click);
            // 
            // btnStopCloseOrder
            // 
            this.btnStopCloseOrder.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStopCloseOrder.Location = new System.Drawing.Point(439, 25);
            this.btnStopCloseOrder.Margin = new System.Windows.Forms.Padding(4);
            this.btnStopCloseOrder.Name = "btnStopCloseOrder";
            this.btnStopCloseOrder.Size = new System.Drawing.Size(185, 47);
            this.btnStopCloseOrder.TabIndex = 4;
            this.btnStopCloseOrder.Text = "Stop & Close Orders";
            this.btnStopCloseOrder.UseVisualStyleBackColor = true;
            this.btnStopCloseOrder.Click += new System.EventHandler(this.btnStopCloseOrder_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.RBChannelBreakoutstrategy);
            this.groupBox3.Controls.Add(this.RBOpenBreakoutStrategy);
            this.groupBox3.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(16, 19);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox3.Size = new System.Drawing.Size(645, 94);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Strategy Selection";
            // 
            // RBChannelBreakoutstrategy
            // 
            this.RBChannelBreakoutstrategy.AutoSize = true;
            this.RBChannelBreakoutstrategy.Checked = true;
            this.RBChannelBreakoutstrategy.CheckState = System.Windows.Forms.CheckState.Checked;
            this.RBChannelBreakoutstrategy.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RBChannelBreakoutstrategy.Location = new System.Drawing.Point(295, 40);
            this.RBChannelBreakoutstrategy.Margin = new System.Windows.Forms.Padding(4);
            this.RBChannelBreakoutstrategy.Name = "RBChannelBreakoutstrategy";
            this.RBChannelBreakoutstrategy.Size = new System.Drawing.Size(283, 23);
            this.RBChannelBreakoutstrategy.TabIndex = 3;
            this.RBChannelBreakoutstrategy.Text = "Channel/Candle Breakout Strategy";
            this.RBChannelBreakoutstrategy.UseVisualStyleBackColor = true;
            this.RBChannelBreakoutstrategy.CheckedChanged += new System.EventHandler(this.ChanBreakStrategy_CheckedChanged_1);
            // 
            // RBOpenBreakoutStrategy
            // 
            this.RBOpenBreakoutStrategy.AutoSize = true;
            this.RBOpenBreakoutStrategy.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RBOpenBreakoutStrategy.Location = new System.Drawing.Point(35, 40);
            this.RBOpenBreakoutStrategy.Margin = new System.Windows.Forms.Padding(4);
            this.RBOpenBreakoutStrategy.Name = "RBOpenBreakoutStrategy";
            this.RBOpenBreakoutStrategy.Size = new System.Drawing.Size(206, 23);
            this.RBOpenBreakoutStrategy.TabIndex = 2;
            this.RBOpenBreakoutStrategy.Text = "Open Breakout Strategy";
            this.RBOpenBreakoutStrategy.UseVisualStyleBackColor = true;
            this.RBOpenBreakoutStrategy.CheckedChanged += new System.EventHandler(this.OpenHighStrategy_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnSymbolSetting);
            this.groupBox1.Controls.Add(this.BtnTradingSetting);
            this.groupBox1.Font = new System.Drawing.Font("Arial", 10.2F);
            this.groupBox1.Location = new System.Drawing.Point(16, 345);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(645, 94);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Settings";
            // 
            // btnSymbolSetting
            // 
            this.btnSymbolSetting.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSymbolSetting.Location = new System.Drawing.Point(215, 29);
            this.btnSymbolSetting.Margin = new System.Windows.Forms.Padding(4);
            this.btnSymbolSetting.Name = "btnSymbolSetting";
            this.btnSymbolSetting.Size = new System.Drawing.Size(185, 47);
            this.btnSymbolSetting.TabIndex = 4;
            this.btnSymbolSetting.Text = "Symbol Setting";
            this.btnSymbolSetting.UseVisualStyleBackColor = true;
            this.btnSymbolSetting.Click += new System.EventHandler(this.btnSymbolSetting_Click);
            // 
            // BtnTradingSetting
            // 
            this.BtnTradingSetting.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnTradingSetting.Location = new System.Drawing.Point(8, 29);
            this.BtnTradingSetting.Margin = new System.Windows.Forms.Padding(4);
            this.BtnTradingSetting.Name = "BtnTradingSetting";
            this.BtnTradingSetting.Size = new System.Drawing.Size(185, 47);
            this.BtnTradingSetting.TabIndex = 3;
            this.BtnTradingSetting.Text = "Trading Setting";
            this.BtnTradingSetting.UseVisualStyleBackColor = true;
            this.BtnTradingSetting.Click += new System.EventHandler(this.BtnTradingSetting_Click);
            // 
            // groupBoxAutomationDetails
            // 
            this.groupBoxAutomationDetails.Controls.Add(this.btnStart);
            this.groupBoxAutomationDetails.Controls.Add(this.btnStopCloseOrder);
            this.groupBoxAutomationDetails.Controls.Add(this.btnStop);
            this.groupBoxAutomationDetails.Font = new System.Drawing.Font("Arial", 10.2F);
            this.groupBoxAutomationDetails.Location = new System.Drawing.Point(16, 235);
            this.groupBoxAutomationDetails.Name = "groupBoxAutomationDetails";
            this.groupBoxAutomationDetails.Size = new System.Drawing.Size(645, 89);
            this.groupBoxAutomationDetails.TabIndex = 10;
            this.groupBoxAutomationDetails.TabStop = false;
            this.groupBoxAutomationDetails.Text = "Automation Details";
            // 
            // form_autoTrading
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(686, 460);
            this.Controls.Add(this.groupBoxAutomationDetails);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "form_autoTrading";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "IntellirichOHLSystem";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.form_autoTrading_FormClosing);
            this.Load += new System.EventHandler(this.form_autoTrading_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBoxAutomationDetails.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox RBChannelBreakoutstrategy;
        private System.Windows.Forms.CheckBox RBOpenBreakoutStrategy;
        private System.Windows.Forms.Button btnStopCloseOrder;
        private System.Windows.Forms.Button CloseBtn;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnSymbolSetting;
        private System.Windows.Forms.Button BtnTradingSetting;
        private System.Windows.Forms.GroupBox groupBoxAutomationDetails;
    }
}

