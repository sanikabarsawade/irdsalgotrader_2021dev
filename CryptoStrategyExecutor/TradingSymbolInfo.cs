﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoStrategyExecutor
{
    public class TradingSymbolInfo
    {
        public string SymbolWithExchange;
        public double ProfitPercent;
        public double Stoploss;
        public string TradingSymbol;
        public string Exchange;
        public double BarCount;
        public double ProfitTrail;
        public double StopLoss;
  //sanika::4-Aug-2021::values remains same for after last symbol stored in storage
        public double High;
        public double Low;
        public double Middle;

        public string getSymbol()
        {
            return TradingSymbol;
        }


        public double getProfitPercent()
        {
            return ProfitPercent;
        }

        public double getStopLoss()
        {
            return Stoploss;
        }

        public string getExchange()
        {
            return Exchange;
        }

        public double getBarCount()
        {
            return BarCount;
        }

        //sanika::17-May-2021::added inputs symbolwise
        public double getProfitTrail()
        {
            return ProfitTrail;
        }
    }
}
