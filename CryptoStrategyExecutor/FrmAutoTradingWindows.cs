﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CryptoStrategyExecutor
{
    public partial class FrmAutoTradingWindows : Form
    {
        public FrmAutoTradingWindows(string title)
        {
            InitializeComponent(title);
        }

        private void FrmAutoTradingWindows_FormClosing(object sender, FormClosingEventArgs e)
        {
            frmAutoTrading1.formClose(e);
        }
    }
}
