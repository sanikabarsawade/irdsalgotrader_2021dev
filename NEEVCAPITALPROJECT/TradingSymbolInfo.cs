﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NEEVCAPITALPROJECT
{
    public class TradingSymbolInfo
    {
        //public string SymbolWithExchange;
        public int Quantity;  
        public string TradingSymbol;
        public string Exchange;
        public string ExpiryDate;
        //IRDSPM::PRatiksha::28-04::2021::For accepting expiry period
        public string ExpiryPeriod;
        public string Strike;
        public int Roundoff;
        //IRDS::Pratiksha::08-06-2021::Add TotalNoOfLots 
        public int TotalNoOfLots;
         //IRDSPM::Pratiksha::05-05-2021::For order type
        public string OrderType;
        public double lotsize;
        //IRDSPM::Pratiksha::19-05-2021::For strike price
        public double StrikePricePut;
        public double StrikePriceCall;
        public double marginPerlot;
        public string getSymbol()
        {
            return TradingSymbol;
        }

        public int getQuantity()
        {
            return Quantity;
        }

        public string getExpiryDate()
        {
            return ExpiryDate;
        }
       
        public string getExchange()
        {
            return Exchange;
        }

        public string getExpiryPeriod()
        {
            return ExpiryPeriod;
        }
        public int getRoundoff()
        {
            return Roundoff;
        }

        public int getTotalNoOfLots()
        {
            return TotalNoOfLots;
        }
        public string getOrderType()
        {
            return OrderType;
        }
        public double getLotSize()
        {
            return lotsize;
        }
        public double getStrikePricePut()
        {
            return StrikePricePut;
        }
        public double getStrikePriceCall()
        {
            return StrikePriceCall;
        }
        public double getMarginPerLot()
        {
            return marginPerlot;
        }
    }
}
