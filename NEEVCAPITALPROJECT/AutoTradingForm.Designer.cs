﻿namespace NEEVCAPITALPROJECT
{
    partial class AutoTradingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AutoTradingForm));
            this.btnLogin = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.btnHistorical = new System.Windows.Forms.Button();
            this.btnStopCloseOrder = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.ChanBreakStrategy = new System.Windows.Forms.CheckBox();
            this.btnRealStockStrategy1 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbtnZerodha = new System.Windows.Forms.RadioButton();
            this.rbtnComposite = new System.Windows.Forms.RadioButton();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.settingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.accountSettingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnLogin
            // 
            this.btnLogin.Location = new System.Drawing.Point(0, 0);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(75, 23);
            this.btnLogin.TabIndex = 2;
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(0, 0);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 0;
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(0, 0);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(75, 23);
            this.btnStop.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.btnHistorical);
            this.groupBox2.Controls.Add(this.btnLogin);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(430, 87);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(0, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            // 
            // btnHistorical
            // 
            this.btnHistorical.Location = new System.Drawing.Point(0, 0);
            this.btnHistorical.Name = "btnHistorical";
            this.btnHistorical.Size = new System.Drawing.Size(75, 23);
            this.btnHistorical.TabIndex = 1;
            // 
            // btnStopCloseOrder
            // 
            this.btnStopCloseOrder.Location = new System.Drawing.Point(0, 0);
            this.btnStopCloseOrder.Name = "btnStopCloseOrder";
            this.btnStopCloseOrder.Size = new System.Drawing.Size(75, 23);
            this.btnStopCloseOrder.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.ChanBreakStrategy);
            this.groupBox3.Location = new System.Drawing.Point(18, 162);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(430, 93);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Strategy Selection";
            // 
            // ChanBreakStrategy
            // 
            this.ChanBreakStrategy.Location = new System.Drawing.Point(0, 0);
            this.ChanBreakStrategy.Name = "ChanBreakStrategy";
            this.ChanBreakStrategy.Size = new System.Drawing.Size(104, 24);
            this.ChanBreakStrategy.TabIndex = 0;
            // 
            // btnRealStockStrategy1
            // 
            this.btnRealStockStrategy1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRealStockStrategy1.Location = new System.Drawing.Point(32, 155);
            this.btnRealStockStrategy1.Name = "btnRealStockStrategy1";
            this.btnRealStockStrategy1.Size = new System.Drawing.Size(233, 36);
            this.btnRealStockStrategy1.TabIndex = 3;
            this.btnRealStockStrategy1.Text = "NeevCapital Strategy 1";
            this.btnRealStockStrategy1.UseVisualStyleBackColor = true;
            this.btnRealStockStrategy1.Click += new System.EventHandler(this.btnRealStockStrategy1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbtnZerodha);
            this.groupBox1.Controls.Add(this.rbtnComposite);
            this.groupBox1.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(32, 69);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(446, 66);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Broker Setting";
            // 
            // rbtnZerodha
            // 
            this.rbtnZerodha.AutoSize = true;
            this.rbtnZerodha.Enabled = false;
            this.rbtnZerodha.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnZerodha.Location = new System.Drawing.Point(49, 27);
            this.rbtnZerodha.Name = "rbtnZerodha";
            this.rbtnZerodha.Size = new System.Drawing.Size(90, 23);
            this.rbtnZerodha.TabIndex = 1;
            this.rbtnZerodha.TabStop = true;
            this.rbtnZerodha.Text = "Zerodha";
            this.rbtnZerodha.UseVisualStyleBackColor = true;
            this.rbtnZerodha.CheckedChanged += new System.EventHandler(this.rbtnZerodha_CheckedChanged);
            // 
            // rbtnComposite
            // 
            this.rbtnComposite.AutoSize = true;
            this.rbtnComposite.Checked = true;
            this.rbtnComposite.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnComposite.Location = new System.Drawing.Point(305, 27);
            this.rbtnComposite.Name = "rbtnComposite";
            this.rbtnComposite.Size = new System.Drawing.Size(107, 23);
            this.rbtnComposite.TabIndex = 2;
            this.rbtnComposite.TabStop = true;
            this.rbtnComposite.Text = "Composite";
            this.rbtnComposite.UseVisualStyleBackColor = true;
            this.rbtnComposite.CheckedChanged += new System.EventHandler(this.rbtnComposite_CheckedChanged);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(624, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.TabStop = true;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // settingToolStripMenuItem
            // 
            this.settingToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.accountSettingToolStripMenuItem});
            this.settingToolStripMenuItem.Name = "settingToolStripMenuItem";
            this.settingToolStripMenuItem.ShortcutKeyDisplayString = "";
            this.settingToolStripMenuItem.Size = new System.Drawing.Size(70, 24);
            this.settingToolStripMenuItem.Text = "Setting";
            // 
            // accountSettingToolStripMenuItem
            // 
            this.accountSettingToolStripMenuItem.Name = "accountSettingToolStripMenuItem";
            this.accountSettingToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+F1";
            this.accountSettingToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F1)));
            this.accountSettingToolStripMenuItem.Size = new System.Drawing.Size(254, 26);
            this.accountSettingToolStripMenuItem.Text = "Account Setting";
            this.accountSettingToolStripMenuItem.Click += new System.EventHandler(this.accountSettingToolStripMenuItem_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(298, 155);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(233, 36);
            this.button2.TabIndex = 4;
            this.button2.Text = "NeevCapital Strategy 2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // AutoTradingForm
            // 
            this.ClientSize = new System.Drawing.Size(624, 229);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnRealStockStrategy1);
            this.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "AutoTradingForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Algo Trading";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AutoTradingForm_FormClosing);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox ChanBreakStrategy;
        private System.Windows.Forms.Button btnStopCloseOrder;
        private System.Windows.Forms.Button btnHistorical;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnRealStockStrategy1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbtnComposite;
        private System.Windows.Forms.RadioButton rbtnZerodha;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem settingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem accountSettingToolStripMenuItem;
        private System.Windows.Forms.Button button2;
    }
}

