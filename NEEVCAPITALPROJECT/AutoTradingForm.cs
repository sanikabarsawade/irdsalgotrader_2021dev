﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using IRDSAlgoOMS;

namespace NEEVCAPITALPROJECT
{
    public partial class AutoTradingForm : Form
    {
        AlgoOMS AlgoOMS = null;
        public int m_StrategyCount = 1;
        Dictionary<string, StrategyForm> m_StrategyObjects = null;
        string m_StrategyName = "RealStockA1Strategy_";
        bool isZerodha = false;
        bool isComposite = true;

        List<string> m_CompositeAccountNumbers = new List<string>();
        List<string> m_ZerodhaAccountNumbers = new List<string>();
        public AutoTradingForm()
        {
            InitializeComponent();           
            NetworkChange.NetworkAvailabilityChanged +=
             new NetworkAvailabilityChangedEventHandler(OnNetworkAvailabilityChanged);

            m_StrategyObjects = new Dictionary<string, StrategyForm>();
            AlgoOMS = new AlgoOMS();            
            ReadSettings readSettings = new ReadSettings(null);
            string path = Directory.GetCurrentDirectory();
            string filePath = path + @"\Configuration\AccountNumber.ini";
            if (File.Exists(filePath))
            {
                m_CompositeAccountNumbers = readSettings.ReadCompositeAccountNumbers(filePath);
                m_ZerodhaAccountNumbers  = readSettings.ReadZerodhaAccountNumbers(filePath);
            }
            else
            {
                MessageBox.Show("AccountNumber.ini file does not exist");
            }
            //IRDSPM::Pratiksha::07-07-2021::For reading broker name
            readSettings.ReadBrokerName(path + @"\Configuration\Setting.ini");
            string brokerName = readSettings.m_BrokerName;
            if (brokerName.ToLower() == "zerodha")
            {
                rbtnZerodha.Checked = true;
            }
            else if (brokerName.ToLower() == "composite")
            {
                rbtnComposite.Checked = true;
            }
        }

        static void OnNetworkAvailabilityChanged(object sender, NetworkAvailabilityEventArgs e)
        {
            if (e.IsAvailable)
            {
                Console.WriteLine("Network has become available");
            }
            else
            {
                bool flag = false;
                try
                {
                    using (var client = new WebClient())
                    using (client.OpenRead("http://clients3.google.com/generate_204"))
                    {
                        flag = true;
                    }
                }
                catch
                {
                    flag = false;
                }
                if(!flag)
                {
                    MessageBox.Show("Please check internet connection!!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                
            }
        }

        private void btnRealStockStrategy1_Click(object sender, EventArgs e)
        {           
            string brokerName = GetBroker();
            if (Application.OpenForms["RealStockA1 Strategy 1"] == null)
            {
                string path = Directory.GetCurrentDirectory();
                if (brokerName == "Composite")
                {
                    string accountNumber = m_CompositeAccountNumbers.ElementAt(0);
                WriteUniquelogs("StrategySelection", "Clicked on 1st strategy accountNumber " + accountNumber, MessageType.Informational);
                string iniFile1 = path + @"\Configuration\" + "CompositeSettings_"+ accountNumber+".ini";
                ReadSettings Settings = new ReadSettings(null, AlgoOMS);
                string userid = Settings.readUserId(iniFile1);
                if (File.Exists(iniFile1))
                {
                    if (Application.OpenForms["RealStockA1 Strategy 1"] == null)
                    {
                StrategyForm obj = new StrategyForm("1", AlgoOMS, brokerName, userid);
                obj.Text = "RealStockA1 Strategy 1";
                obj.Name = "RealStockA1 Strategy 1";
                obj.Show();
            }
        }
                else
                {
                    WriteUniquelogs("StrategySelection", "INI file not found", MessageType.Informational);
                    MessageBox.Show("Account not set on strategy 1");
                }
            }
                else if (brokerName == "Zerodha")
        {
                    string accountNumber = m_ZerodhaAccountNumbers.ElementAt(0);
                    WriteUniquelogs("StrategySelection", "Clicked on 1st strategy accountNumber " + accountNumber, MessageType.Informational);
                    string iniFile1 = path + @"\Configuration\" + "ZerodhaSettings_" + accountNumber + ".ini";
                    ReadSettings Settings = new ReadSettings(null, AlgoOMS);
                    string userid = Settings.readUserId(iniFile1);
                    if (File.Exists(iniFile1))
            {
                        if (Application.OpenForms["RealStockA1 Strategy 1"] == null)
                {
                            StrategyForm obj = new StrategyForm("1", AlgoOMS, brokerName, userid);
                            obj.Text = "RealStockA1 Strategy 1";
                            obj.Name = "RealStockA1 Strategy 1";
                            obj.Show();
                        }
                }
                else
                {
                        WriteUniquelogs("StrategySelection", "INI file not found", MessageType.Informational);
                        MessageBox.Show("Account not set on strategy 1");
                    }
                    }
                    }
                }

        private void btnRealStockStrategy2_Click(object sender, EventArgs e)
                {
            string brokerName = GetBroker();
                    if (Application.OpenForms["RealStockA1 Strategy 2"] == null)
                    {
                string path = Directory.GetCurrentDirectory();
                string iniFile1 = path + @"\Configuration\" + "CompositeSettings_KAP70.ini";
                ReadSettings Settings = new ReadSettings(null, AlgoOMS);
                string userid = Settings.readUserId(iniFile1);
                StrategyForm obj = new StrategyForm("2", AlgoOMS, brokerName, userid);
                obj.Text = "RealStockA1 Strategy 2";
                obj.Name = "RealStockA1 Strategy 2";
                obj.Show();
            }
        }

        private void btnRealStockStrategy3_Click(object sender, EventArgs e)
        {
            string brokerName = GetBroker();
            if (Application.OpenForms["RealStockA1 Strategy 3"] == null)
            {
                string path = Directory.GetCurrentDirectory();
                string iniFile1 = path + @"\Configuration\" + "CompositeSettings.ini";
                ReadSettings Settings = new ReadSettings(null, AlgoOMS);
                string userid = Settings.readUserId(iniFile1);
                StrategyForm obj = new StrategyForm("3", AlgoOMS, brokerName, userid);
                obj.Text = "RealStockA1 Strategy 3";
                obj.Name = "RealStockA1 Strategy 3";
                obj.Show();
            }
        }

        private void btnRealStockStrategy4_Click(object sender, EventArgs e)
        {
            string path = Directory.GetCurrentDirectory();
            string iniFile1 = path + @"\Configuration\" + "CompositeSettings.ini";
            ReadSettings Settings = new ReadSettings(null, AlgoOMS);
            string userid = Settings.readUserId(iniFile1);
            string brokerName = GetBroker();
            if (Application.OpenForms["RealStockA1 Strategy 4"] == null)
            {
                StrategyForm obj = new StrategyForm("4", AlgoOMS, brokerName, userid);
                obj.Text = "RealStockA1 Strategy 4";
                obj.Name = "RealStockA1 Strategy 4";
                obj.Show();
            }
        }

        private void btnRealStockStrategy5_Click(object sender, EventArgs e)
        {
            string brokerName = GetBroker();
            if (Application.OpenForms["RealStockA1 Strategy 5"] == null)
            {
                string path = Directory.GetCurrentDirectory();
                string iniFile1 = path + @"\Configuration\" + "CompositeSettings.ini";
                ReadSettings Settings = new ReadSettings(null, AlgoOMS);
                string userid = Settings.readUserId(iniFile1);
                StrategyForm obj = new StrategyForm("5", AlgoOMS, brokerName, userid);
                obj.Text = "RealStockA1 Strategy 5";
                obj.Name = "RealStockA1 Strategy 5";
                obj.Show();
            }
        }

        private void AutoTradingForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                string message = "Do you really want to exit?";
                string title = "RealStock Strategy";
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                DialogResult dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                if (dialog == DialogResult.Yes)
                {
                    Environment.Exit(0);
                }
                else
                {
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void rbtnZerodha_CheckedChanged(object sender, EventArgs e)
        {
            isZerodha = true;
            isComposite = false;
        }

        private void rbtnComposite_CheckedChanged(object sender, EventArgs e)
        {
            isZerodha = false;
            isComposite = true;
        }

        public string GetBroker()
        {
            string brokerName = "";
            if(isZerodha)
            {
                brokerName = "Zerodha";
            }
            else if(isComposite)
            {
                brokerName = "Composite";
            }
            return brokerName;
        }
        string userID = "";
        private void button2_Click(object sender, EventArgs e)
        {
            objCreation();
          
        }

        public void loginToAccount()
        {
            btnLogin.Enabled = false;
            Cursor.Current = Cursors.WaitCursor;
            string error = "";
            int loginCount = 0;
            try
            {
                AlgoOMS.Connect(userID);
                while (loginCount < 3)
                {
                    if (AlgoOMS.kiteWrapperConnect != null || AlgoOMS.compositeWrapperConnect != null)
                    {
                        if (AlgoOMS.GetLoginStatus(userID))
                        {

                        }
                        break;
                    }                    
                }
                loginCount++;

            }
            catch (Exception er)
            {
                error = er.Message;
                if (error.Contains("empty") && error.Contains("api_key") && error.Contains("access_token"))
                {
                    MessageBox.Show("Error : Please check account credentials", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    MessageBox.Show("Error : " + er.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            finally
            {
                if (error.Contains("System.Data.SQLite"))
                {
                    MessageBox.Show("Please add dll for sqlite", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                }
            }
            Cursor.Current = Cursors.Default;

        }

        public void objCreation()
        {
            ReadSettings Settings;
            Logger logger = Logger.Instance;

            string userid = "KPA70";
            StrategyForm obj = new StrategyForm("1", AlgoOMS, "Zerodha", userid);
            obj.Text = "RealStockA1 Strategy 1";
            obj.Name = "RealStockA1 Strategy 1";

            //logger.LogMessage("In constructor", MessageType.Informational);
            string path = Directory.GetCurrentDirectory();
            // string iniFile = path + @"\Configuration\" + "SamcoSettings.ini";
            string iniFile = path + @"\Configuration\" + "APISetting.ini";
            if (File.Exists(iniFile))
            {
                Settings = new ReadSettings(logger, AlgoOMS);
                //userID = Settings.readUserId(iniFile);
            }
            else
            {
                MessageBox.Show("INI file does not exit!!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            AlgoOMS.SetBroker(0);
            AlgoOMS.CreateKiteConnectObject(userID, "APISetting.ini");
            loginToAccount();
        }

        private void accountSettingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Logger logger = Logger.Instance;
            FrmConfigSettings obj = new FrmConfigSettings(logger);
            obj.ShowDialog();
        }

        public void WriteUniquelogs(string symbol, string message, MessageType msgType)
        {
            try
            {
                SymbolLogs log = new SymbolLogs();
                //log = SymbolLogs.InstanceSymbolLogs;
                log.createFile(symbol);
                log.LogMessage(message, MessageType.Informational);
                log = null;
            }
            catch (Exception e)
            {
                Console.WriteLine("WriteUniquelogs : Exception : " + e.Message);
            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            string brokerName = GetBroker();
            if (Application.OpenForms["RealStockA1 Strategy 1"] == null)
            {
                string path = Directory.GetCurrentDirectory();
                if (brokerName == "Composite")
                {
                    string accountNumber = "";
                    if (m_CompositeAccountNumbers.Count > 1)
                    {
                        accountNumber = m_CompositeAccountNumbers.ElementAt(1);
                    }
                    else
                    {
                        accountNumber = m_CompositeAccountNumbers.ElementAt(0);
                    }
                    WriteUniquelogs("StrategySelection", "Clicked on 1st strategy accountNumber " + accountNumber, MessageType.Informational);
                    string iniFile1 = path + @"\Configuration\" + "CompositeSettings_" + accountNumber + ".ini";
                    ReadSettings Settings = new ReadSettings(null, AlgoOMS);
                    string userid = Settings.readUserId(iniFile1);
                    if (File.Exists(iniFile1))
                    {
                        if (Application.OpenForms["RealStockA1 Strategy 1"] == null)
                        {
                            StrategyForm obj = new StrategyForm("1", AlgoOMS, brokerName, userid);
                            obj.Text = "RealStockA1 Strategy 1";
                            obj.Name = "RealStockA1 Strategy 1";
                            obj.Show();
                        }
                    }
                    else
                    {
                        WriteUniquelogs("StrategySelection", "INI file not found", MessageType.Informational);
                        MessageBox.Show("Account not set on strategy 1");
                    }
                }
                else if (brokerName == "Zerodha")
                {
                    string accountNumber = "";
                    if (m_ZerodhaAccountNumbers.Count > 1)
                    {
                        accountNumber = m_ZerodhaAccountNumbers.ElementAt(1);
                    }
                    else
                    {
                        accountNumber = m_ZerodhaAccountNumbers.ElementAt(0);
                    }
                    WriteUniquelogs("StrategySelection", "Clicked on 1st strategy accountNumber " + accountNumber, MessageType.Informational);
                    string iniFile1 = path + @"\Configuration\" + "ZerodhaSettings_" + accountNumber + ".ini";
                    ReadSettings Settings = new ReadSettings(null, AlgoOMS);
                    string userid = Settings.readUserId(iniFile1);
                    if (File.Exists(iniFile1))
                    {
                        if (Application.OpenForms["RealStockA1 Strategy 1"] == null)
                        {
                            StrategyForm obj = new StrategyForm("1", AlgoOMS, brokerName, userid);
                            obj.Text = "RealStockA1 Strategy 1";
                            obj.Name = "RealStockA1 Strategy 1";
                            obj.Show();
                        }
                    }
                    else
                    {
                        WriteUniquelogs("StrategySelection", "INI file not found", MessageType.Informational);
                        MessageBox.Show("Account not set on strategy 1");
                    }
                }
            }
        }
    }
}
