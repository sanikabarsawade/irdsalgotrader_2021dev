﻿
namespace NEEVCAPITALPROJECT
{
    partial class TradingSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TradingSetting));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblStopLoss = new System.Windows.Forms.Label();
            this.txtStartEntryTime = new System.Windows.Forms.DateTimePicker();
            this.txtEndEntryTime = new System.Windows.Forms.DateTimePicker();
            this.txtEndExitTime = new System.Windows.Forms.DateTimePicker();
            this.pnlIncreaseDescreasePercent = new System.Windows.Forms.Panel();
            this.txtIncreaseDescreasePercent = new System.Windows.Forms.TextBox();
            this.pnlSlicingLotSize = new System.Windows.Forms.Panel();
            this.txtSlicingLotSize = new System.Windows.Forms.TextBox();
            this.pnlOrderWaitTime = new System.Windows.Forms.Panel();
            this.txtOrderWaitTime = new System.Windows.Forms.TextBox();
            this.lblOrderWaitTime = new System.Windows.Forms.Label();
            this.lblSlicingLotSize = new System.Windows.Forms.Label();
            this.lblIncreaseDescreasePercent = new System.Windows.Forms.Label();
            this.lblEndExitTime = new System.Windows.Forms.Label();
            this.lblEndEntryTime = new System.Windows.Forms.Label();
            this.lblStartEntryTime = new System.Windows.Forms.Label();
            this.pnlAccountNumber = new System.Windows.Forms.Panel();
            this.txtAccountNumber = new System.Windows.Forms.TextBox();
            this.lblNoOfLots = new System.Windows.Forms.Label();
            this.btnApply = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOpenSymbolSetting = new System.Windows.Forms.Button();
            this.GPAccount = new System.Windows.Forms.GroupBox();
            this.lblAccount = new System.Windows.Forms.Label();
            this.pnlStoploss = new System.Windows.Forms.Panel();
            this.txtstoploss = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.pnlIncreaseDescreasePercent.SuspendLayout();
            this.pnlSlicingLotSize.SuspendLayout();
            this.pnlOrderWaitTime.SuspendLayout();
            this.pnlAccountNumber.SuspendLayout();
            this.GPAccount.SuspendLayout();
            this.pnlStoploss.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pnlStoploss);
            this.groupBox1.Controls.Add(this.lblStopLoss);
            this.groupBox1.Controls.Add(this.txtStartEntryTime);
            this.groupBox1.Controls.Add(this.txtEndEntryTime);
            this.groupBox1.Controls.Add(this.txtEndExitTime);
            this.groupBox1.Controls.Add(this.pnlIncreaseDescreasePercent);
            this.groupBox1.Controls.Add(this.pnlSlicingLotSize);
            this.groupBox1.Controls.Add(this.pnlOrderWaitTime);
            this.groupBox1.Controls.Add(this.lblOrderWaitTime);
            this.groupBox1.Controls.Add(this.lblSlicingLotSize);
            this.groupBox1.Controls.Add(this.lblIncreaseDescreasePercent);
            this.groupBox1.Controls.Add(this.lblEndExitTime);
            this.groupBox1.Controls.Add(this.lblEndEntryTime);
            this.groupBox1.Controls.Add(this.lblStartEntryTime);
            this.groupBox1.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 95);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(764, 408);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Trade Details";
            // 
            // lblStopLoss
            // 
            this.lblStopLoss.AutoSize = true;
            this.lblStopLoss.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblStopLoss.Location = new System.Drawing.Point(24, 351);
            this.lblStopLoss.Name = "lblStopLoss";
            this.lblStopLoss.Size = new System.Drawing.Size(81, 19);
            this.lblStopLoss.TabIndex = 64;
            this.lblStopLoss.Text = "Stop Loss";
            // 
            // txtStartEntryTime
            // 
            this.txtStartEntryTime.CustomFormat = "HH:mm";
            this.txtStartEntryTime.Font = new System.Drawing.Font("Arial", 12F);
            this.txtStartEntryTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txtStartEntryTime.Location = new System.Drawing.Point(330, 29);
            this.txtStartEntryTime.Name = "txtStartEntryTime";
            this.txtStartEntryTime.ShowUpDown = true;
            this.txtStartEntryTime.Size = new System.Drawing.Size(413, 30);
            this.txtStartEntryTime.TabIndex = 6;
            // 
            // txtEndEntryTime
            // 
            this.txtEndEntryTime.CustomFormat = "HH:mm";
            this.txtEndEntryTime.Font = new System.Drawing.Font("Arial", 12F);
            this.txtEndEntryTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txtEndEntryTime.Location = new System.Drawing.Point(330, 78);
            this.txtEndEntryTime.Name = "txtEndEntryTime";
            this.txtEndEntryTime.ShowUpDown = true;
            this.txtEndEntryTime.Size = new System.Drawing.Size(413, 30);
            this.txtEndEntryTime.TabIndex = 7;
            // 
            // txtEndExitTime
            // 
            this.txtEndExitTime.CustomFormat = "HH:mm";
            this.txtEndExitTime.Font = new System.Drawing.Font("Arial", 12F);
            this.txtEndExitTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txtEndExitTime.Location = new System.Drawing.Point(330, 127);
            this.txtEndExitTime.Name = "txtEndExitTime";
            this.txtEndExitTime.ShowUpDown = true;
            this.txtEndExitTime.Size = new System.Drawing.Size(413, 30);
            this.txtEndExitTime.TabIndex = 8;
            // 
            // pnlIncreaseDescreasePercent
            // 
            this.pnlIncreaseDescreasePercent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlIncreaseDescreasePercent.Controls.Add(this.txtIncreaseDescreasePercent);
            this.pnlIncreaseDescreasePercent.Location = new System.Drawing.Point(330, 177);
            this.pnlIncreaseDescreasePercent.Name = "pnlIncreaseDescreasePercent";
            this.pnlIncreaseDescreasePercent.Size = new System.Drawing.Size(413, 35);
            this.pnlIncreaseDescreasePercent.TabIndex = 9;
            this.pnlIncreaseDescreasePercent.TabStop = true;
            // 
            // txtIncreaseDescreasePercent
            // 
            this.txtIncreaseDescreasePercent.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtIncreaseDescreasePercent.Font = new System.Drawing.Font("Arial", 13F);
            this.txtIncreaseDescreasePercent.Location = new System.Drawing.Point(7, 4);
            this.txtIncreaseDescreasePercent.Name = "txtIncreaseDescreasePercent";
            this.txtIncreaseDescreasePercent.Size = new System.Drawing.Size(401, 25);
            this.txtIncreaseDescreasePercent.TabIndex = 10;
            // 
            // pnlSlicingLotSize
            // 
            this.pnlSlicingLotSize.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlSlicingLotSize.Controls.Add(this.txtSlicingLotSize);
            this.pnlSlicingLotSize.Location = new System.Drawing.Point(330, 233);
            this.pnlSlicingLotSize.Name = "pnlSlicingLotSize";
            this.pnlSlicingLotSize.Size = new System.Drawing.Size(413, 35);
            this.pnlSlicingLotSize.TabIndex = 11;
            this.pnlSlicingLotSize.TabStop = true;
            // 
            // txtSlicingLotSize
            // 
            this.txtSlicingLotSize.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSlicingLotSize.Font = new System.Drawing.Font("Arial", 13F);
            this.txtSlicingLotSize.Location = new System.Drawing.Point(7, 4);
            this.txtSlicingLotSize.Name = "txtSlicingLotSize";
            this.txtSlicingLotSize.Size = new System.Drawing.Size(401, 25);
            this.txtSlicingLotSize.TabIndex = 16;
            // 
            // pnlOrderWaitTime
            // 
            this.pnlOrderWaitTime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlOrderWaitTime.Controls.Add(this.txtOrderWaitTime);
            this.pnlOrderWaitTime.Location = new System.Drawing.Point(330, 289);
            this.pnlOrderWaitTime.Name = "pnlOrderWaitTime";
            this.pnlOrderWaitTime.Size = new System.Drawing.Size(413, 35);
            this.pnlOrderWaitTime.TabIndex = 14;
            this.pnlOrderWaitTime.TabStop = true;
            // 
            // txtOrderWaitTime
            // 
            this.txtOrderWaitTime.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtOrderWaitTime.Font = new System.Drawing.Font("Arial", 13F);
            this.txtOrderWaitTime.Location = new System.Drawing.Point(7, 4);
            this.txtOrderWaitTime.Name = "txtOrderWaitTime";
            this.txtOrderWaitTime.Size = new System.Drawing.Size(401, 25);
            this.txtOrderWaitTime.TabIndex = 15;
            // 
            // lblOrderWaitTime
            // 
            this.lblOrderWaitTime.AutoSize = true;
            this.lblOrderWaitTime.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblOrderWaitTime.Location = new System.Drawing.Point(20, 294);
            this.lblOrderWaitTime.Name = "lblOrderWaitTime";
            this.lblOrderWaitTime.Size = new System.Drawing.Size(225, 19);
            this.lblOrderWaitTime.TabIndex = 51;
            this.lblOrderWaitTime.Text = "Order Wait Time (In seconds)";
            // 
            // lblSlicingLotSize
            // 
            this.lblSlicingLotSize.AutoSize = true;
            this.lblSlicingLotSize.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblSlicingLotSize.Location = new System.Drawing.Point(20, 240);
            this.lblSlicingLotSize.Name = "lblSlicingLotSize";
            this.lblSlicingLotSize.Size = new System.Drawing.Size(122, 19);
            this.lblSlicingLotSize.TabIndex = 53;
            this.lblSlicingLotSize.Text = "Slicing Lot Size";
            // 
            // lblIncreaseDescreasePercent
            // 
            this.lblIncreaseDescreasePercent.AutoSize = true;
            this.lblIncreaseDescreasePercent.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblIncreaseDescreasePercent.Location = new System.Drawing.Point(20, 184);
            this.lblIncreaseDescreasePercent.Name = "lblIncreaseDescreasePercent";
            this.lblIncreaseDescreasePercent.Size = new System.Drawing.Size(211, 19);
            this.lblIncreaseDescreasePercent.TabIndex = 54;
            this.lblIncreaseDescreasePercent.Text = "Increase/Decrease Percent";
            // 
            // lblEndExitTime
            // 
            this.lblEndExitTime.AutoSize = true;
            this.lblEndExitTime.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblEndExitTime.Location = new System.Drawing.Point(20, 136);
            this.lblEndExitTime.Name = "lblEndExitTime";
            this.lblEndExitTime.Size = new System.Drawing.Size(108, 19);
            this.lblEndExitTime.TabIndex = 59;
            this.lblEndExitTime.Text = "End Exit Time";
            // 
            // lblEndEntryTime
            // 
            this.lblEndEntryTime.AutoSize = true;
            this.lblEndEntryTime.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblEndEntryTime.Location = new System.Drawing.Point(20, 87);
            this.lblEndEntryTime.Name = "lblEndEntryTime";
            this.lblEndEntryTime.Size = new System.Drawing.Size(121, 19);
            this.lblEndEntryTime.TabIndex = 61;
            this.lblEndEntryTime.Text = "End Entry Time";
            // 
            // lblStartEntryTime
            // 
            this.lblStartEntryTime.AutoSize = true;
            this.lblStartEntryTime.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblStartEntryTime.Location = new System.Drawing.Point(20, 33);
            this.lblStartEntryTime.Name = "lblStartEntryTime";
            this.lblStartEntryTime.Size = new System.Drawing.Size(126, 19);
            this.lblStartEntryTime.TabIndex = 62;
            this.lblStartEntryTime.Text = "Start Entry Time";
            // 
            // pnlAccountNumber
            // 
            this.pnlAccountNumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlAccountNumber.Controls.Add(this.txtAccountNumber);
            this.pnlAccountNumber.Location = new System.Drawing.Point(330, 21);
            this.pnlAccountNumber.Name = "pnlAccountNumber";
            this.pnlAccountNumber.Size = new System.Drawing.Size(413, 35);
            this.pnlAccountNumber.TabIndex = 4;
            this.pnlAccountNumber.TabStop = true;
            // 
            // txtAccountNumber
            // 
            this.txtAccountNumber.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtAccountNumber.Font = new System.Drawing.Font("Arial", 13F);
            this.txtAccountNumber.Location = new System.Drawing.Point(7, 4);
            this.txtAccountNumber.Name = "txtAccountNumber";
            this.txtAccountNumber.Size = new System.Drawing.Size(401, 25);
            this.txtAccountNumber.TabIndex = 5;
            // 
            // lblNoOfLots
            // 
            this.lblNoOfLots.AutoSize = true;
            this.lblNoOfLots.Location = new System.Drawing.Point(20, 251);
            this.lblNoOfLots.Name = "lblNoOfLots";
            this.lblNoOfLots.Size = new System.Drawing.Size(186, 19);
            this.lblNoOfLots.TabIndex = 57;
            this.lblNoOfLots.Text = "Total Number of Lot Size";
            // 
            // btnApply
            // 
            this.btnApply.Font = new System.Drawing.Font("Arial", 11F);
            this.btnApply.Location = new System.Drawing.Point(477, 528);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(134, 43);
            this.btnApply.TabIndex = 20;
            this.btnApply.Text = "Apply";
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("Arial", 11F);
            this.btnCancel.Location = new System.Drawing.Point(642, 528);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(134, 43);
            this.btnCancel.TabIndex = 21;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOpenSymbolSetting
            // 
            this.btnOpenSymbolSetting.Font = new System.Drawing.Font("Arial", 11F);
            this.btnOpenSymbolSetting.Location = new System.Drawing.Point(12, 528);
            this.btnOpenSymbolSetting.Name = "btnOpenSymbolSetting";
            this.btnOpenSymbolSetting.Size = new System.Drawing.Size(197, 43);
            this.btnOpenSymbolSetting.TabIndex = 19;
            this.btnOpenSymbolSetting.Text = "Symbol Setting";
            this.btnOpenSymbolSetting.UseVisualStyleBackColor = true;
            this.btnOpenSymbolSetting.Click += new System.EventHandler(this.btnOpenSymbolSetting_Click);
            // 
            // GPAccount
            // 
            this.GPAccount.Controls.Add(this.lblAccount);
            this.GPAccount.Controls.Add(this.pnlAccountNumber);
            this.GPAccount.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GPAccount.Location = new System.Drawing.Point(12, 12);
            this.GPAccount.Name = "GPAccount";
            this.GPAccount.Size = new System.Drawing.Size(760, 67);
            this.GPAccount.TabIndex = 22;
            this.GPAccount.TabStop = false;
            this.GPAccount.Text = "Account";
            // 
            // lblAccount
            // 
            this.lblAccount.AutoSize = true;
            this.lblAccount.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblAccount.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblAccount.Location = new System.Drawing.Point(22, 27);
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Size = new System.Drawing.Size(91, 19);
            this.lblAccount.TabIndex = 1;
            this.lblAccount.Text = "Account ID";
            // 
            // pnlStoploss
            // 
            this.pnlStoploss.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlStoploss.Controls.Add(this.txtstoploss);
            this.pnlStoploss.Location = new System.Drawing.Point(330, 341);
            this.pnlStoploss.Name = "pnlStoploss";
            this.pnlStoploss.Size = new System.Drawing.Size(413, 35);
            this.pnlStoploss.TabIndex = 65;
            this.pnlStoploss.TabStop = true;
            // 
            // txtstoploss
            // 
            this.txtstoploss.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtstoploss.Font = new System.Drawing.Font("Arial", 13F);
            this.txtstoploss.Location = new System.Drawing.Point(7, 4);
            this.txtstoploss.Name = "txtstoploss";
            this.txtstoploss.Size = new System.Drawing.Size(401, 25);
            this.txtstoploss.TabIndex = 15;
            // 
            // TradingSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(790, 592);
            this.Controls.Add(this.GPAccount);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnOpenSymbolSetting);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.btnCancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "TradingSetting";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Trading Setting";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.pnlIncreaseDescreasePercent.ResumeLayout(false);
            this.pnlIncreaseDescreasePercent.PerformLayout();
            this.pnlSlicingLotSize.ResumeLayout(false);
            this.pnlSlicingLotSize.PerformLayout();
            this.pnlOrderWaitTime.ResumeLayout(false);
            this.pnlOrderWaitTime.PerformLayout();
            this.pnlAccountNumber.ResumeLayout(false);
            this.pnlAccountNumber.PerformLayout();
            this.GPAccount.ResumeLayout(false);
            this.GPAccount.PerformLayout();
            this.pnlStoploss.ResumeLayout(false);
            this.pnlStoploss.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblEndExitTime;
        //private System.Windows.Forms.Label lblInterval;
        private System.Windows.Forms.Label lblEndEntryTime;
        private System.Windows.Forms.Label lblStartEntryTime;
        private System.Windows.Forms.Label lblOrderWaitTime;
        //private System.Windows.Forms.Label lblReducePercentInLot;
        private System.Windows.Forms.Label lblSlicingLotSize;
        private System.Windows.Forms.Label lblIncreaseDescreasePercent;
        //private System.Windows.Forms.Label lblPutStrikeValue;
        //private System.Windows.Forms.Label lblCallStrikeValue;
        private System.Windows.Forms.Label lblNoOfLots;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Panel pnlSlicingLotSize;
        //private System.Windows.Forms.Panel panel5;
        //private System.Windows.Forms.TextBox txtEndEntryTime;
        //private System.Windows.Forms.Panel panel6;
        //private System.Windows.Forms.TextBox txtStartExitTime;
        //private System.Windows.Forms.Panel pnlNoOfLots;
        //private System.Windows.Forms.TextBox txtNoOfLots;
        //private System.Windows.Forms.Panel pnlInterval;
        //private System.Windows.Forms.TextBox txtInterval;
        //private System.Windows.Forms.Panel panel7;
        //private System.Windows.Forms.TextBox txtEndExitTime;
        //private System.Windows.Forms.Panel pnlCallStrikeValue;
       // private System.Windows.Forms.TextBox txtCallStrikeValue;
        private System.Windows.Forms.Panel pnlIncreaseDescreasePercent;
        private System.Windows.Forms.TextBox txtIncreaseDescreasePercent;
        //private System.Windows.Forms.Panel pnlPutStrikeValue;
       // private System.Windows.Forms.TextBox txtPutStrikeValue;
        private System.Windows.Forms.Panel pnlOrderWaitTime;
        private System.Windows.Forms.TextBox txtOrderWaitTime;
        //private System.Windows.Forms.Panel pnlReducePercentInLot;
       // private System.Windows.Forms.TextBox txtReducePercentInLot;
        private System.Windows.Forms.Panel pnlAccountNumber;
        private System.Windows.Forms.TextBox txtAccountNumber;
        private System.Windows.Forms.DateTimePicker txtStartEntryTime;
        private System.Windows.Forms.DateTimePicker txtEndEntryTime;
        private System.Windows.Forms.DateTimePicker txtEndExitTime;
        private System.Windows.Forms.Button btnOpenSymbolSetting;
        private System.Windows.Forms.TextBox txtSlicingLotSize;
        private System.Windows.Forms.GroupBox GPAccount;
        private System.Windows.Forms.Label lblAccount;
        private System.Windows.Forms.Label lblStopLoss;
        private System.Windows.Forms.Panel pnlStoploss;
        private System.Windows.Forms.TextBox txtstoploss;
    }
}