﻿using System;
using System;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
namespace NEEVCAPITALPROJECT
{
    partial class SymbolSettingDet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SymbolSettingDet));
            this.btnDeleteselectedrow = new System.Windows.Forms.Button();
            this.btnApply = new System.Windows.Forms.Button();
            this.GPSymbolDet = new System.Windows.Forms.GroupBox();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.SymbolName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Exchange = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ExpiryDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Strike = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ExpiryPeriod = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OrderType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MarketLotSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalNumberofLotSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RoundOff = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GPAddDet = new System.Windows.Forms.GroupBox();
            this.btnClearAll = new System.Windows.Forms.Button();
            this.lblStrike = new System.Windows.Forms.Label();
            this.StrikedtList = new System.Windows.Forms.ComboBox();
            this.pnlTotalNumberofLotSize = new System.Windows.Forms.Panel();
            this.txtTotalNumberofLotSize = new System.Windows.Forms.TextBox();
            this.lblTotalNumberofLotSize = new System.Windows.Forms.Label();
            this.lblRoundOff = new System.Windows.Forms.Label();
            this.lblInstruments = new System.Windows.Forms.Label();
            this.ExchangeList = new System.Windows.Forms.ComboBox();
            this.lblSymbol = new System.Windows.Forms.Label();
            this.SymbolList = new System.Windows.Forms.ComboBox();
            this.lblExpiryDate = new System.Windows.Forms.Label();
            this.ExpirydtList = new System.Windows.Forms.ComboBox();
            this.lblExpiryPeriod = new System.Windows.Forms.Label();
            this.ExpiryPeriodList = new System.Windows.Forms.ComboBox();
            this.lblOrdertype = new System.Windows.Forms.Label();
            this.OrderTypeList = new System.Windows.Forms.ComboBox();
            this.lblMarketLotSize = new System.Windows.Forms.Label();
            this.pnlLotSize = new System.Windows.Forms.Panel();
            this.txtMarketLotSize = new System.Windows.Forms.TextBox();
            this.pnlRoundoff = new System.Windows.Forms.Panel();
            this.txtroundoff = new System.Windows.Forms.TextBox();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.txtMarginPerLot = new System.Windows.Forms.TextBox();
            this.lblMargin = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.MarginPerLot = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GPSymbolDet.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.GPAddDet.SuspendLayout();
            this.pnlTotalNumberofLotSize.SuspendLayout();
            this.pnlLotSize.SuspendLayout();
            this.pnlRoundoff.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnDeleteselectedrow
            // 
            this.btnDeleteselectedrow.Font = new System.Drawing.Font("Arial", 10.5F);
            this.btnDeleteselectedrow.Location = new System.Drawing.Point(58, 606);
            this.btnDeleteselectedrow.Name = "btnDeleteselectedrow";
            this.btnDeleteselectedrow.Size = new System.Drawing.Size(213, 40);
            this.btnDeleteselectedrow.TabIndex = 20;
            this.btnDeleteselectedrow.Text = "Delete symbol";
            this.btnDeleteselectedrow.UseVisualStyleBackColor = true;
            this.btnDeleteselectedrow.Click += new System.EventHandler(this.btnDeleteselectedrow_Click);
            // 
            // btnApply
            // 
            this.btnApply.Font = new System.Drawing.Font("Arial", 10.5F);
            this.btnApply.Location = new System.Drawing.Point(872, 601);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(134, 40);
            this.btnApply.TabIndex = 21;
            this.btnApply.Text = "Apply";
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // GPSymbolDet
            // 
            this.GPSymbolDet.Controls.Add(this.dataGridView);
            this.GPSymbolDet.Controls.Add(this.GPAddDet);
            this.GPSymbolDet.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GPSymbolDet.Location = new System.Drawing.Point(12, 13);
            this.GPSymbolDet.Name = "GPSymbolDet";
            this.GPSymbolDet.Size = new System.Drawing.Size(1171, 582);
            this.GPSymbolDet.TabIndex = 1;
            this.GPSymbolDet.TabStop = false;
            this.GPSymbolDet.Text = "Symbol Details";
            // 
            // dataGridView
            // 
            this.dataGridView.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SymbolName,
            this.Exchange,
            this.ExpiryDate,
            this.Strike,
            this.ExpiryPeriod,
            this.OrderType,
            this.MarketLotSize,
            this.TotalNumberofLotSize,
            this.RoundOff,
            this.MarginPerLot});
            this.dataGridView.Location = new System.Drawing.Point(14, 270);
            this.dataGridView.MultiSelect = false;
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.ReadOnly = true;
            this.dataGridView.RowHeadersWidth = 51;
            this.dataGridView.RowTemplate.Height = 24;
            this.dataGridView.Size = new System.Drawing.Size(1151, 302);
            this.dataGridView.TabIndex = 1;
            this.dataGridView.TabStop = false;
            this.dataGridView.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView_CellMouseClick);
            // 
            // SymbolName
            // 
            this.SymbolName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.SymbolName.HeaderText = "Symbol Name";
            this.SymbolName.MinimumWidth = 6;
            this.SymbolName.Name = "SymbolName";
            this.SymbolName.ReadOnly = true;
            this.SymbolName.Width = 113;
            // 
            // Exchange
            // 
            this.Exchange.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Exchange.FillWeight = 90F;
            this.Exchange.HeaderText = "Exchange";
            this.Exchange.MinimumWidth = 6;
            this.Exchange.Name = "Exchange";
            this.Exchange.ReadOnly = true;
            // 
            // ExpiryDate
            // 
            this.ExpiryDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ExpiryDate.HeaderText = "Expiry Date";
            this.ExpiryDate.MinimumWidth = 10;
            this.ExpiryDate.Name = "ExpiryDate";
            this.ExpiryDate.ReadOnly = true;
            // 
            // Strike
            // 
            this.Strike.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Strike.FillWeight = 76.19048F;
            this.Strike.HeaderText = "Strike";
            this.Strike.MinimumWidth = 6;
            this.Strike.Name = "Strike";
            this.Strike.ReadOnly = true;
            // 
            // ExpiryPeriod
            // 
            this.ExpiryPeriod.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ExpiryPeriod.FillWeight = 76.19048F;
            this.ExpiryPeriod.HeaderText = "Expiry Period";
            this.ExpiryPeriod.MinimumWidth = 6;
            this.ExpiryPeriod.Name = "ExpiryPeriod";
            this.ExpiryPeriod.ReadOnly = true;
            // 
            // OrderType
            // 
            this.OrderType.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.OrderType.FillWeight = 76.19048F;
            this.OrderType.HeaderText = "Order Type";
            this.OrderType.MinimumWidth = 6;
            this.OrderType.Name = "OrderType";
            this.OrderType.ReadOnly = true;
            // 
            // MarketLotSize
            // 
            this.MarketLotSize.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.MarketLotSize.FillWeight = 76.19048F;
            this.MarketLotSize.HeaderText = "Market Lot Size";
            this.MarketLotSize.MinimumWidth = 6;
            this.MarketLotSize.Name = "MarketLotSize";
            this.MarketLotSize.ReadOnly = true;
            // 
            // TotalNumberofLotSize
            // 
            this.TotalNumberofLotSize.FillWeight = 80F;
            this.TotalNumberofLotSize.HeaderText = "Total Number of Lot Size";
            this.TotalNumberofLotSize.MinimumWidth = 6;
            this.TotalNumberofLotSize.Name = "TotalNumberofLotSize";
            this.TotalNumberofLotSize.ReadOnly = true;
            this.TotalNumberofLotSize.Width = 80;
            // 
            // RoundOff
            // 
            this.RoundOff.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.RoundOff.FillWeight = 76.19048F;
            this.RoundOff.HeaderText = "Round Off";
            this.RoundOff.MinimumWidth = 6;
            this.RoundOff.Name = "RoundOff";
            this.RoundOff.ReadOnly = true;
            // 
            // GPAddDet
            // 
            this.GPAddDet.Controls.Add(this.panel1);
            this.GPAddDet.Controls.Add(this.lblMargin);
            this.GPAddDet.Controls.Add(this.btnClearAll);
            this.GPAddDet.Controls.Add(this.lblStrike);
            this.GPAddDet.Controls.Add(this.StrikedtList);
            this.GPAddDet.Controls.Add(this.pnlTotalNumberofLotSize);
            this.GPAddDet.Controls.Add(this.lblTotalNumberofLotSize);
            this.GPAddDet.Controls.Add(this.lblRoundOff);
            this.GPAddDet.Controls.Add(this.lblInstruments);
            this.GPAddDet.Controls.Add(this.ExchangeList);
            this.GPAddDet.Controls.Add(this.lblSymbol);
            this.GPAddDet.Controls.Add(this.SymbolList);
            this.GPAddDet.Controls.Add(this.lblExpiryDate);
            this.GPAddDet.Controls.Add(this.ExpirydtList);
            this.GPAddDet.Controls.Add(this.lblExpiryPeriod);
            this.GPAddDet.Controls.Add(this.ExpiryPeriodList);
            this.GPAddDet.Controls.Add(this.lblOrdertype);
            this.GPAddDet.Controls.Add(this.OrderTypeList);
            this.GPAddDet.Controls.Add(this.lblMarketLotSize);
            this.GPAddDet.Controls.Add(this.pnlLotSize);
            this.GPAddDet.Controls.Add(this.pnlRoundoff);
            this.GPAddDet.Controls.Add(this.btnUpdate);
            this.GPAddDet.Controls.Add(this.btnAdd);
            this.GPAddDet.Font = new System.Drawing.Font("Arial", 7.8F);
            this.GPAddDet.Location = new System.Drawing.Point(14, 18);
            this.GPAddDet.Name = "GPAddDet";
            this.GPAddDet.Size = new System.Drawing.Size(1151, 246);
            this.GPAddDet.TabIndex = 2;
            this.GPAddDet.TabStop = false;
            this.GPAddDet.Text = "Add details";
           // this.GPAddDet.MouseCaptureChanged += new System.EventHandler(this.GPAddDet_MouseCaptureChanged);
            // 
            // btnClearAll
            // 
            this.btnClearAll.Font = new System.Drawing.Font("Arial", 10F);
            this.btnClearAll.Location = new System.Drawing.Point(1009, 109);
            this.btnClearAll.Name = "btnClearAll";
            this.btnClearAll.Size = new System.Drawing.Size(127, 31);
            this.btnClearAll.TabIndex = 22;
            this.btnClearAll.Text = "Clear All";
            this.btnClearAll.UseVisualStyleBackColor = true;
            this.btnClearAll.Click += new System.EventHandler(this.btnClearAll_Click);
            // 
            // lblStrike
            // 
            this.lblStrike.AutoSize = true;
            this.lblStrike.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblStrike.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblStrike.Location = new System.Drawing.Point(25, 165);
            this.lblStrike.Name = "lblStrike";
            this.lblStrike.Size = new System.Drawing.Size(51, 19);
            this.lblStrike.TabIndex = 21;
            this.lblStrike.Text = "Strike";
            // 
            // StrikedtList
            // 
            this.StrikedtList.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.StrikedtList.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.StrikedtList.Font = new System.Drawing.Font("Arial", 12F);
            this.StrikedtList.FormattingEnabled = true;
            this.StrikedtList.Location = new System.Drawing.Point(187, 155);
            this.StrikedtList.Name = "StrikedtList";
            this.StrikedtList.Size = new System.Drawing.Size(231, 31);
            this.StrikedtList.TabIndex = 20;
            // 
            // pnlTotalNumberofLotSize
            // 
            this.pnlTotalNumberofLotSize.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlTotalNumberofLotSize.Controls.Add(this.txtTotalNumberofLotSize);
            this.pnlTotalNumberofLotSize.Location = new System.Drawing.Point(730, 109);
            this.pnlTotalNumberofLotSize.Name = "pnlTotalNumberofLotSize";
            this.pnlTotalNumberofLotSize.Size = new System.Drawing.Size(234, 31);
            this.pnlTotalNumberofLotSize.TabIndex = 10;
            this.pnlTotalNumberofLotSize.TabStop = true;
            // 
            // txtTotalNumberofLotSize
            // 
            this.txtTotalNumberofLotSize.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTotalNumberofLotSize.Font = new System.Drawing.Font("Arial", 12F);
            this.txtTotalNumberofLotSize.Location = new System.Drawing.Point(6, 3);
            this.txtTotalNumberofLotSize.Name = "txtTotalNumberofLotSize";
            this.txtTotalNumberofLotSize.Size = new System.Drawing.Size(225, 23);
            this.txtTotalNumberofLotSize.TabIndex = 11;
            // 
            // lblTotalNumberofLotSize
            // 
            this.lblTotalNumberofLotSize.AutoSize = true;
            this.lblTotalNumberofLotSize.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblTotalNumberofLotSize.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblTotalNumberofLotSize.Location = new System.Drawing.Point(477, 117);
            this.lblTotalNumberofLotSize.Name = "lblTotalNumberofLotSize";
            this.lblTotalNumberofLotSize.Size = new System.Drawing.Size(186, 19);
            this.lblTotalNumberofLotSize.TabIndex = 19;
            this.lblTotalNumberofLotSize.Text = "Total Number of Lot Size";
            // 
            // lblRoundOff
            // 
            this.lblRoundOff.AutoSize = true;
            this.lblRoundOff.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblRoundOff.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblRoundOff.Location = new System.Drawing.Point(477, 159);
            this.lblRoundOff.Name = "lblRoundOff";
            this.lblRoundOff.Size = new System.Drawing.Size(83, 19);
            this.lblRoundOff.TabIndex = 12;
            this.lblRoundOff.Text = "Round Off";
            // 
            // lblInstruments
            // 
            this.lblInstruments.AutoSize = true;
            this.lblInstruments.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblInstruments.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblInstruments.Location = new System.Drawing.Point(25, 29);
            this.lblInstruments.Name = "lblInstruments";
            this.lblInstruments.Size = new System.Drawing.Size(93, 19);
            this.lblInstruments.TabIndex = 18;
            this.lblInstruments.Text = "Instruments";
            // 
            // ExchangeList
            // 
            this.ExchangeList.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ExchangeList.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.ExchangeList.Font = new System.Drawing.Font("Arial", 12F);
            this.ExchangeList.FormattingEnabled = true;
            this.ExchangeList.Location = new System.Drawing.Point(187, 17);
            this.ExchangeList.Name = "ExchangeList";
            this.ExchangeList.Size = new System.Drawing.Size(231, 31);
            this.ExchangeList.TabIndex = 3;
            this.ExchangeList.SelectedValueChanged += new System.EventHandler(this.ExchangeList_SelectedValueChanged);
            // 
            // lblSymbol
            // 
            this.lblSymbol.AutoSize = true;
            this.lblSymbol.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblSymbol.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblSymbol.Location = new System.Drawing.Point(25, 73);
            this.lblSymbol.Name = "lblSymbol";
            this.lblSymbol.Size = new System.Drawing.Size(63, 19);
            this.lblSymbol.TabIndex = 17;
            this.lblSymbol.Text = "Symbol";
            // 
            // SymbolList
            // 
            this.SymbolList.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.SymbolList.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.SymbolList.Font = new System.Drawing.Font("Arial", 12F);
            this.SymbolList.FormattingEnabled = true;
            this.SymbolList.Location = new System.Drawing.Point(187, 64);
            this.SymbolList.Name = "SymbolList";
            this.SymbolList.Size = new System.Drawing.Size(231, 31);
            this.SymbolList.TabIndex = 4;
            this.SymbolList.SelectedValueChanged += new System.EventHandler(this.SymbolList_SelectedValueChanged);
            // 
            // lblExpiryDate
            // 
            this.lblExpiryDate.AutoSize = true;
            this.lblExpiryDate.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblExpiryDate.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblExpiryDate.Location = new System.Drawing.Point(25, 118);
            this.lblExpiryDate.Name = "lblExpiryDate";
            this.lblExpiryDate.Size = new System.Drawing.Size(94, 19);
            this.lblExpiryDate.TabIndex = 16;
            this.lblExpiryDate.Text = "Expiry Date";
            // 
            // ExpirydtList
            // 
            this.ExpirydtList.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ExpirydtList.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.ExpirydtList.Font = new System.Drawing.Font("Arial", 12F);
            this.ExpirydtList.FormattingEnabled = true;
            this.ExpirydtList.Location = new System.Drawing.Point(187, 109);
            this.ExpirydtList.Name = "ExpirydtList";
            this.ExpirydtList.Size = new System.Drawing.Size(231, 31);
            this.ExpirydtList.TabIndex = 5;
            this.ExpirydtList.SelectedValueChanged += new System.EventHandler(this.ExpirydtList_SelectedValueChanged);
            // 
            // lblExpiryPeriod
            // 
            this.lblExpiryPeriod.AutoSize = true;
            this.lblExpiryPeriod.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblExpiryPeriod.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblExpiryPeriod.Location = new System.Drawing.Point(25, 208);
            this.lblExpiryPeriod.Name = "lblExpiryPeriod";
            this.lblExpiryPeriod.Size = new System.Drawing.Size(108, 19);
            this.lblExpiryPeriod.TabIndex = 13;
            this.lblExpiryPeriod.Text = "Expiry Period";
            // 
            // ExpiryPeriodList
            // 
            this.ExpiryPeriodList.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ExpiryPeriodList.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.ExpiryPeriodList.Font = new System.Drawing.Font("Arial", 12F);
            this.ExpiryPeriodList.FormattingEnabled = true;
            this.ExpiryPeriodList.Location = new System.Drawing.Point(187, 199);
            this.ExpiryPeriodList.Name = "ExpiryPeriodList";
            this.ExpiryPeriodList.Size = new System.Drawing.Size(231, 31);
            this.ExpiryPeriodList.TabIndex = 6;
            // 
            // lblOrdertype
            // 
            this.lblOrdertype.AutoSize = true;
            this.lblOrdertype.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblOrdertype.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblOrdertype.Location = new System.Drawing.Point(477, 27);
            this.lblOrdertype.Name = "lblOrdertype";
            this.lblOrdertype.Size = new System.Drawing.Size(91, 19);
            this.lblOrdertype.TabIndex = 9;
            this.lblOrdertype.Text = "Order Type";
            // 
            // OrderTypeList
            // 
            this.OrderTypeList.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.OrderTypeList.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.OrderTypeList.Font = new System.Drawing.Font("Arial", 12F);
            this.OrderTypeList.FormattingEnabled = true;
            this.OrderTypeList.Location = new System.Drawing.Point(730, 17);
            this.OrderTypeList.Name = "OrderTypeList";
            this.OrderTypeList.Size = new System.Drawing.Size(234, 31);
            this.OrderTypeList.TabIndex = 7;
            // 
            // lblMarketLotSize
            // 
            this.lblMarketLotSize.AutoSize = true;
            this.lblMarketLotSize.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblMarketLotSize.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblMarketLotSize.Location = new System.Drawing.Point(477, 72);
            this.lblMarketLotSize.Name = "lblMarketLotSize";
            this.lblMarketLotSize.Size = new System.Drawing.Size(122, 19);
            this.lblMarketLotSize.TabIndex = 8;
            this.lblMarketLotSize.Text = "Market Lot Size";
            // 
            // pnlLotSize
            // 
            this.pnlLotSize.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlLotSize.Controls.Add(this.txtMarketLotSize);
            this.pnlLotSize.Location = new System.Drawing.Point(730, 64);
            this.pnlLotSize.Name = "pnlLotSize";
            this.pnlLotSize.Size = new System.Drawing.Size(234, 31);
            this.pnlLotSize.TabIndex = 8;
            this.pnlLotSize.TabStop = true;
            // 
            // txtMarketLotSize
            // 
            this.txtMarketLotSize.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtMarketLotSize.Enabled = false;
            this.txtMarketLotSize.Font = new System.Drawing.Font("Arial", 12F);
            this.txtMarketLotSize.Location = new System.Drawing.Point(6, 3);
            this.txtMarketLotSize.Name = "txtMarketLotSize";
            this.txtMarketLotSize.Size = new System.Drawing.Size(225, 23);
            this.txtMarketLotSize.TabIndex = 9;
            // 
            // pnlRoundoff
            // 
            this.pnlRoundoff.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlRoundoff.Controls.Add(this.txtroundoff);
            this.pnlRoundoff.Location = new System.Drawing.Point(730, 155);
            this.pnlRoundoff.Name = "pnlRoundoff";
            this.pnlRoundoff.Size = new System.Drawing.Size(234, 31);
            this.pnlRoundoff.TabIndex = 12;
            this.pnlRoundoff.TabStop = true;
            // 
            // txtroundoff
            // 
            this.txtroundoff.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtroundoff.Enabled = false;
            this.txtroundoff.Font = new System.Drawing.Font("Arial", 12F);
            this.txtroundoff.Location = new System.Drawing.Point(4, 3);
            this.txtroundoff.Name = "txtroundoff";
            this.txtroundoff.Size = new System.Drawing.Size(225, 23);
            this.txtroundoff.TabIndex = 13;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Font = new System.Drawing.Font("Arial", 10F);
            this.btnUpdate.Location = new System.Drawing.Point(1009, 155);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(127, 31);
            this.btnUpdate.TabIndex = 18;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("Arial", 10F);
            this.btnAdd.Location = new System.Drawing.Point(1009, 199);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(127, 31);
            this.btnAdd.TabIndex = 19;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("Arial", 10.5F);
            this.btnCancel.Location = new System.Drawing.Point(1031, 601);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(134, 40);
            this.btnCancel.TabIndex = 22;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // txtMarginPerLot
            // 
            this.txtMarginPerLot.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtMarginPerLot.Font = new System.Drawing.Font("Arial", 12F);
            this.txtMarginPerLot.Location = new System.Drawing.Point(4, 3);
            this.txtMarginPerLot.Name = "txtMarginPerLot";
            this.txtMarginPerLot.Size = new System.Drawing.Size(225, 23);
            this.txtMarginPerLot.TabIndex = 23;
            // 
            // lblMargin
            // 
            this.lblMargin.AutoSize = true;
            this.lblMargin.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblMargin.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblMargin.Location = new System.Drawing.Point(477, 208);
            this.lblMargin.Name = "lblMargin";
            this.lblMargin.Size = new System.Drawing.Size(117, 19);
            this.lblMargin.TabIndex = 24;
            this.lblMargin.Text = "Margin Per Lot";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.txtMarginPerLot);
            this.panel1.Location = new System.Drawing.Point(730, 199);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(234, 31);
            this.panel1.TabIndex = 25;
            this.panel1.TabStop = true;
            // 
            // MarginPerLot
            // 
            this.MarginPerLot.HeaderText = "Margin Per Lot";
            this.MarginPerLot.MinimumWidth = 6;
            this.MarginPerLot.Name = "MarginPerLot";
            this.MarginPerLot.ReadOnly = true;
            this.MarginPerLot.Width = 125;
            // 
            // SymbolSettingDet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1195, 656);
            this.Controls.Add(this.GPSymbolDet);
            this.Controls.Add(this.btnDeleteselectedrow);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.btnCancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "SymbolSettingDet";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Symbol Setting";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SymbolSettingDet_FormClosing);
            this.GPSymbolDet.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.GPAddDet.ResumeLayout(false);
            this.GPAddDet.PerformLayout();
            this.pnlTotalNumberofLotSize.ResumeLayout(false);
            this.pnlTotalNumberofLotSize.PerformLayout();
            this.pnlLotSize.ResumeLayout(false);
            this.pnlLotSize.PerformLayout();
            this.pnlRoundoff.ResumeLayout(false);
            this.pnlRoundoff.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ComboBox ExchangeList;
        private System.Windows.Forms.ComboBox SymbolList;
        private System.Windows.Forms.ComboBox ExpirydtList;
        private System.Windows.Forms.ComboBox ExpiryPeriodList;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnDeleteselectedrow;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.Button btnCancel;


        private System.Windows.Forms.GroupBox GPSymbolDet;
        private System.Windows.Forms.GroupBox GPAddDet;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.Label lblExpiryPeriod;
        private System.Windows.Forms.Label lblExpiryDate;
        private System.Windows.Forms.Label lblInstruments;
        private System.Windows.Forms.Label lblSymbol;
        private System.Windows.Forms.Label lblStrike;
        private System.Windows.Forms.ComboBox StrikedtList;
        private System.Windows.Forms.DataGridViewTextBoxColumn SymbolName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Exchange;
        private System.Windows.Forms.DataGridViewTextBoxColumn ExpiryDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Strike;
        private System.Windows.Forms.DataGridViewTextBoxColumn ExpiryPeriod;
        private System.Windows.Forms.DataGridViewTextBoxColumn OrderType;
        private System.Windows.Forms.DataGridViewTextBoxColumn MarketLotSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalNumberofLotSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn RoundOff;
        private System.Windows.Forms.Panel pnlTotalNumberofLotSize;
        private System.Windows.Forms.TextBox txtTotalNumberofLotSize;
        private System.Windows.Forms.Label lblTotalNumberofLotSize;
        private System.Windows.Forms.Label lblRoundOff;
        private System.Windows.Forms.Label lblOrdertype;
        private System.Windows.Forms.ComboBox OrderTypeList;
        private System.Windows.Forms.Label lblMarketLotSize;
        private System.Windows.Forms.Panel pnlLotSize;
        private System.Windows.Forms.TextBox txtMarketLotSize;
        private System.Windows.Forms.Panel pnlRoundoff;
        private System.Windows.Forms.TextBox txtroundoff;
        private System.Windows.Forms.Button btnClearAll;
        private TextBox txtMarginPerLot;
        private Label lblMargin;
        private Panel panel1;
        private DataGridViewTextBoxColumn MarginPerLot;
    }
}