﻿using IRDSAlgoOMS;
using NEEVCAPITALPROJECT;
using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace NEEVCAPITALPROJECT
{
    public partial class TradingSetting : Form
    {
        [field: NonSerialized()]
        AlgoOMS AlgoOMSObj;
        //sanika::25-sep-2020::added for resume 
        [field: NonSerialized()]
        NEEVCAPITALPROJECT.ReadSettings readSettings;
        //sanika::25-sep-2020::added for resume 
        [field: NonSerialized()]
        Logger logger;
        string path = System.IO.Directory.GetCurrentDirectory();

        string message = "";
        string title = "RealStockA1 Strategy";
        MessageBoxButtons buttons;
        string m_iniChoose = "";
        DialogResult dialog;
        INIFile iniObj = null;
        string userID = null;
        static string ConfigFilePath = System.AppDomain.CurrentDomain.BaseDirectory + "Configuration";
        public TradingSetting(Logger logger, AlgoOMS obj, string userid)
        {
            InitializeComponent();
            try
            {
                this.AlgoOMSObj = obj;
                this.logger = logger;
                userID = userid;
                txtAccountNumber.Enabled = false;
                readSettings = new NEEVCAPITALPROJECT.ReadSettings(this.logger, this.AlgoOMSObj);
                FetchDetailsFromINI(userid);
            }
            catch (Exception ex)
            {
                WriteUniquelogs("TradingSettingLogs", "InitializeComponent : Exception Error Message = " + ex.Message, MessageType.Informational);
            }
        }
        private void FetchDetailsFromINI(string userid)
        {
            try
            {

                string filePath = path + @"\Configuration\StrategySetting.ini";
                if (File.Exists(filePath))
                {
                    if (readSettings == null)
                    {
                        readSettings = new NEEVCAPITALPROJECT.ReadSettings(logger);
                    }
                    readSettings.ReadNeevCapitalINIFile(filePath);
                    txtAccountNumber.Text = readSettings.m_AccountNumber.ToString();
                    txtStartEntryTime.Value = Convert.ToDateTime(readSettings.m_StartEntryTime);
                    txtEndEntryTime.Value = Convert.ToDateTime(readSettings.m_EndEntryTime);
                    txtEndExitTime.Value = Convert.ToDateTime(readSettings.m_EndExitTime);
                    txtIncreaseDescreasePercent.Text = readSettings.m_IncreaseDescreasePercent.ToString();
                    txtSlicingLotSize.Text = readSettings.m_SlicingLotSize.ToString();
                    txtOrderWaitTime.Text = readSettings.m_OrderWaitTime.ToString();
                    txtstoploss.Text = readSettings.m_Stoploss.ToString();
                    WriteUniquelogs("TradingSettingLogs", "FetchDetailsFromINI : Reading details from INI file.", MessageType.Informational);
                }
                else
                {
                    Clearfields();
                    WriteUniquelogs("TradingSettingLogs", "FetchDetailsFromINI : INI file not exist.", MessageType.Informational);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("TradingSettingLogs", "FetchDetailsFromINI : Exception Error Message = " + ex.Message, MessageType.Informational);
            }
        }
        private void btnApply_Click(object sender, EventArgs e)
        {
            try
            { 
                if (txtAccountNumber.Text != "" && txtStartEntryTime.Text != "" && txtEndEntryTime.Text != "" && txtEndExitTime.Text != "" && txtIncreaseDescreasePercent.Text != "" && txtOrderWaitTime.Text != "" && txtstoploss.Text != "" && txtSlicingLotSize.Text !="")
            {
                message = "Do you want to save the changes for " + txtAccountNumber.Text.ToString() + "?";
                buttons = MessageBoxButtons.YesNo;
                dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                if (dialog == DialogResult.Yes)
                {
                        string filePath = ConfigFilePath + "\\StrategySetting.ini";
                        iniObj = new INIFile(filePath);
                        string[] key = new string[8];
                        string[] value = new string[8];
                        key[0] = "StartEntryTime";
                        key[1] = "EndEntryTime";
                        key[2] = "EndExitTime";
                        key[3] = "IncreaseDescreasePercent";
                        key[4] = "SlicingLotSize";
                        key[5] = "OrderWaitTime";
                        key[6] = "Stoploss";

                        value[0] = txtStartEntryTime.Text;
                        value[1] = txtEndEntryTime.Text;
                        value[2] = txtEndExitTime.Text;
                        value[3] = txtIncreaseDescreasePercent.Text;
                        value[4] = txtSlicingLotSize.Text;
                        value[5] = txtOrderWaitTime.Text;
                        value[6] = txtstoploss.Text;
                        for (int i = 0; i < 7; i++)
                        {
                            iniObj.clearTestingSymbol("SDATA", key[i], value[i], filePath);
                        }
                    message = "Saved changes successfully!!!";
                    buttons = MessageBoxButtons.OK;
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                    if (dialog == DialogResult.OK)
                    {
                        this.Close();
                    }

                    WriteUniquelogs("TradingSettingLogs", "btnApply_Click :  Details saved in ini file. Details are as follows: Start Entry Time= " + txtStartEntryTime.Text+ "  End Entry Time= " + txtEndEntryTime.Text 
                        + "  End Exit Time= "+ txtEndExitTime.Text + "  Increase/Descrease Percent= " + txtIncreaseDescreasePercent.Text+ "  Slicing Lot Size= " + txtSlicingLotSize.Text+ "  Order Wait Time= " + txtOrderWaitTime.Text + "  Stop loss= " + txtstoploss.Text+ ".", MessageType.Informational);
                }
                else
                {
                    //this.Close();
                    WriteUniquelogs("TradingSettingLogs", "btnApply_Click : You have clicked No, no data updated in trade setting.ini file.", MessageType.Informational);
                }
            }
            else
            {
                string parameter = "";
                if (txtAccountNumber.Text == "") { parameter += "\n Account Number"; }
                if (txtStartEntryTime.Text == "") { parameter += "\nStart Entry Time"; }
                if (txtEndEntryTime.Text == "") { parameter += "\nEnd Entry Time"; }
                if (txtEndExitTime.Text == "") { parameter += "\nEnd Exit Time"; }
                if (txtIncreaseDescreasePercent.Text == "") { parameter += "\nIncrease/Decrease Percent"; }
                if (txtSlicingLotSize.Text == "") { parameter += "\nSlicing Lot Size"; }
                if(txtOrderWaitTime.Text == "") { parameter += "\nOrder Wait Time"; }
                if (txtstoploss.Text == "") { parameter += "\nStoploss"; }
                message = "Please enter proper values for following field: " + parameter;
                buttons = MessageBoxButtons.OK;
                dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                WriteUniquelogs("TradingSettingLogs", "btnApply_Click : Please enter proper values.", MessageType.Informational);
            }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("TradingSettingLogs", "btnApply_Click : Exception Error Message = " + ex.Message, MessageType.Informational);
            }
        }
        public void WriteUniquelogs(string symbol, string message, MessageType msgType)
        {
            try
            {
                SymbolLogs log = new SymbolLogs();
                log.createFile(symbol);
                log.LogMessage(message, MessageType.Informational);
                log = null;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : " + e.Message);
                Trace.WriteLine("Exception : " + e.Message);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
                WriteUniquelogs("TradingSettingLogs", "btnCancel_Click : Cancel click.", MessageType.Informational);
            }
            catch (Exception ex)
            {
                WriteUniquelogs("TradingSettingLogs", "btnCancel_Click : Exception Error Message = " + ex.Message, MessageType.Informational);
            }
        }

        private void Clearfields()
        {
            try
            {
                txtAccountNumber.Text = null;
                txtStartEntryTime.Text = null;
                txtEndEntryTime.Text = null;
                txtEndExitTime.Text = null;
                txtIncreaseDescreasePercent.Text = null;
                txtOrderWaitTime.Text = null;
                txtstoploss.Text = null;
                WriteUniquelogs("TradingSettingLogs", "Clearfields : All fields cleared.", MessageType.Informational);
            }
            catch (Exception ex)
            {
                WriteUniquelogs("TradingSettingLogs", "Clearfields : Exception Error Message = " + ex.Message, MessageType.Informational);
            }
        }

        private void btnOpenSymbolSetting_Click(object sender, EventArgs e)
        {
            try
            {
                SymbolSettingDet symbolObj = new SymbolSettingDet(this.AlgoOMSObj, userID);
                symbolObj.ShowDialog();
            }
            catch (Exception ex)
            {
                WriteUniquelogs("TradingSettingLogs", "btnOpenSymbolSetting_Click : Exception Error Message = " + ex.Message, MessageType.Informational);
            }
        }
    }
}
