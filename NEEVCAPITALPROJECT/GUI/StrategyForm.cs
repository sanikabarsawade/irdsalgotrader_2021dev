﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using IRDSAlgoOMS;
using IRDSAlgoOMS.GUI;

namespace NEEVCAPITALPROJECT
{
    public partial class StrategyForm : Form
    {
        private AlgoOMS AlgoOMS = null;
        MainStrategyClass m_RealStockA1StrategyClass;
        bool login = false;
        bool automationStart = false;
        Logger logger = Logger.Instance;
        string userID = " ";
        ReadSettings Settings;       
        string m_BrokerName = "Zerodha";       
        string m_TradingSettingFileName = "StrategySetting.ini";
        string title = "RealStock Strategy";
        //IRDSPM::PRatiksha::30-04-2021::For messagebox Changes
        string message = "";
        MessageBoxButtons buttons;
        DialogResult dialog;

        public delegate void UpdateControlsDelegate();
        public StrategyForm(string Ininumber,AlgoOMS algoOMS,string brokerName, string iniUsername)
        {
            InitializeComponent();
            if (AlgoOMS == null)
                AlgoOMS = algoOMS;


            m_BrokerName = brokerName;        
            
           // m_TradingSettingFileName += iniUsername + ".ini";
            if (m_BrokerName == "Zerodha")
            {                
                string path = Directory.GetCurrentDirectory();               
                string iniFile = path + @"\Configuration\" + "ZerodhaSettings_" + iniUsername + ".ini";
                if (File.Exists(iniFile))
                {                   
                    Settings = new ReadSettings(logger, AlgoOMS);
                    userID = Settings.readUserId(iniFile);
                }
                else
                {
                    message = "INI file does not exit!!";
                    buttons = MessageBoxButtons.OK;
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                }
                AlgoOMS.SetBroker(0);
                AlgoOMS.CreateKiteConnectObject(userID, "ZerodhaSettings_" + iniUsername + ".ini");
                
                //GetSymbols
            }
            else if (m_BrokerName == "Samco")
            {                
               string path = Directory.GetCurrentDirectory();
                string iniFile = path + @"\Configuration\" + "SamcoSettings.ini";
                if (File.Exists(iniFile))
                {
                    Settings = new ReadSettings(logger, AlgoOMS);
                   // userID = Settings.readUserId(iniFile);
                }
                else
                {
                    message = "INI file does not exit!!";
                    buttons = MessageBoxButtons.OK;
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                }
                AlgoOMS.SetBroker(1);
                AlgoOMS.CreateKiteConnectObject(userID, "SamcoSettings.ini");                
            }
            else if(m_BrokerName == "Composite")
            {
                string path = Directory.GetCurrentDirectory();
                string iniFile = path + @"\Configuration\" + "CompositeSettings_" + iniUsername + ".ini";
                if (File.Exists(iniFile))
                {
                    Settings = new ReadSettings(logger, AlgoOMS);
                    userID = Settings.readUserId(iniFile);
                }
                else
                {
                    message = "INI file does not exit!!";
                    buttons = MessageBoxButtons.OK;
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                }
                AlgoOMS.SetBroker(2);
                AlgoOMS.CreateKiteConnectObject(userID, "CompositeSettings_" + iniUsername + ".ini");               
            }
            //IRDSPM::Pratiksha::29-12-2020::For color of Automation status
            AutomationNotstarted();
        }

        static void OnNetworkAvailabilityChanged(object sender, NetworkAvailabilityEventArgs e)
        {
            if (e.IsAvailable)
            {
                Console.WriteLine("Network has become available");
            }
            else
            {
                bool flag = false;
                try
                {
                    using (var client = new WebClient())
                    using (client.OpenRead("http://clients3.google.com/generate_204"))
                    {
                        flag = true;
                    }
                }
                catch
                {
                    flag = false;
                }
                if (!flag)
                {
                    string title = "RealStock Strategy";
                    string message = "Please check internet connection!!";
                    MessageBoxButtons buttons = MessageBoxButtons.OK;
                    System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                }

            }
        }

        private void StrategyForm_Load(object sender, EventArgs e)
        {
            try
            {

                //automationWithFile.loadObject(this);
                NetworkChange.NetworkAvailabilityChanged +=
                 new NetworkAvailabilityChangedEventHandler(OnNetworkAvailabilityChanged);
                if (m_BrokerName == "Zerodha")
                {
                    var path = Directory.GetCurrentDirectory();
                    string iniFile = path + @"\Configuration\" + "ZerodhaSettings_AY1598.ini";
                    //string iniFile = path + @"\Configuration\" + "SamcoSettings.ini";
                    if (File.Exists(iniFile))
                    {
                        Settings = new ReadSettings(logger, AlgoOMS);
                        //userID = Settings.readUserId(iniFile);
                    }
                    else
                    {
                        message = "INI file does not exit!!";
                        buttons = MessageBoxButtons.OK;
                        dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                    }
                }
                else if (m_BrokerName == "Samco")
                {
                    var path = Directory.GetCurrentDirectory();
                    string iniFile = path + @"\Configuration\" + "SamcoSettings.ini";                   
                    if (File.Exists(iniFile))
                    {
                        Settings = new ReadSettings(logger, AlgoOMS);
                        //userID = Settings.readUserId(iniFile);
                    }
                    else
                    {
                        message = "INI file does not exit!!";
                        buttons = MessageBoxButtons.OK;
                        dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                    }
                }
                else if (m_BrokerName == "Composite")
                {
                    //string path = Directory.GetCurrentDirectory();
                    //string iniFile = path + @"\Configuration\" + "CompositeSettings_" + userID + ".ini";
                    //if (File.Exists(iniFile))
                    //{
                    //    Settings = new ReadSettings(logger, AlgoOMS);
                    //    //userID = Settings.readUserId(iniFile);
                    //}
                    //else
                    //{
                    //    message = "INI file does not exit!!";
                    //    buttons = MessageBoxButtons.OK;
                    //    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                    //}
                    //AlgoOMS.SetBroker(2);
                    //AlgoOMS.CreateKiteConnectObject(userID, "CompositeSettings_" + userID + " .ini");
                }

                loginToAccount();
            }
            catch (Exception er)
            {
                Trace.WriteLine("exception from form load : " + er.Message);
            }
        }

        public void loginToAccount()
        {
            btnLogin.Enabled = false;              
            Cursor.Current = Cursors.WaitCursor;
            string error = "";
            int loginCount = 0;
            try
            {
                logger.LogMessage("Clicked on login button", MessageType.Informational);
                AlgoOMS.Connect(userID);
                while (loginCount < 3)
                {
                    if (AlgoOMS.kiteWrapperConnect != null || AlgoOMS.compositeWrapperConnect != null)
                    {
                        if (AlgoOMS.GetLoginStatus(userID))
                        {
                            login = true;
                            m_RealStockA1StrategyClass = new MainStrategyClass(AlgoOMS, logger, userID, m_TradingSettingFileName);
                            m_RealStockA1StrategyClass.loadObject(this);
                            m_RealStockA1StrategyClass.readTradingSymbol(Settings);                         

                        }
                            break;
                        }
                        else
                        {
                            login = false;
                            logger.LogMessage("Not able to login trying " + loginCount, MessageType.Informational);                            
                        }
                    }
                    loginCount++;                    

            }
            catch (Exception er)
            {
                error = er.Message;
                if (error.Contains("empty") && error.Contains("api_key") && error.Contains("access_token"))
                {
                    logger.LogMessage("Error : " + er.Message, MessageType.Exception);
                    message = "Please check account credentials.";
                    buttons = MessageBoxButtons.OK;
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Error);
                }
                else
                {
                    logger.LogMessage("Error : " + er.Message, MessageType.Exception);
                    message = "Error : " + er.Message;
                    buttons = MessageBoxButtons.OK;
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Error);
                }
            }
            finally
            {
                if (error.Contains("System.Data.SQLite"))
                {
                    message = "Please add dll for sqlite.";
                    buttons = MessageBoxButtons.OK;
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Error);
                    logger.LogMessage("Please add dll for sqlite", MessageType.Informational);                   
                    this.Close();
                }
            }
            Cursor.Current = Cursors.Default;
            
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            loginToAccount();
            if (!login)
            {
                message = "Not able to login. Please try with forcefully login!!";
                buttons = MessageBoxButtons.OK;
                dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                logger.LogMessage("Not able to login.Please try with forcefully login", MessageType.Informational);
            }
            else if (login)
            {
                //IRDSPM::Pratiksha::29-07-2020::Change the title
                message = "Logged in successfully!!";
                buttons = MessageBoxButtons.OK;
                dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                logger.LogMessage("Logged on successfully", MessageType.Informational);
            }
        }
        
  
        private void btnStart_Click(object sender, EventArgs e)
        {
            btnStart.Enabled = false;           
            try
            {
                logger.LogMessage("Clicked on start button", MessageType.Informational);
                if (login)
                {                   
                    automationStart = true;
                    //IRDSPM::Pratiksha::29-12-2020::For color of Automation status
                    Automationstarted();
                    m_RealStockA1StrategyClass.loadObject(this);
                    bool res = m_RealStockA1StrategyClass.startThread(userID);
                    if (res)
                    {
                        btnStart.Enabled = false;
                    }
                    else
                    {
                        btnStart.Enabled = true;
                        message = "Some values are missing in settings section. Please check ini file.";
                        buttons = MessageBoxButtons.OK;
                        dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Exclamation);
                    }

                }                               
            }
            catch (Exception er)
            {
                message = "Error : " + er.Message;
                buttons = MessageBoxButtons.OK;
                dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Error);
                logger.LogMessage("Error : " + er.Message, MessageType.Exception);
            }

        }

        private void btnStop_Click(object sender, EventArgs e)
        {           
            try
            {
                logger.LogMessage("Clicked on stop button", MessageType.Informational);
                message = "It Will only stop the automation.Orders will not close \n Do you want to stop automation?";
                buttons = MessageBoxButtons.YesNo;
                dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Exclamation);
                if (dialog== DialogResult.Yes)
                {
                if (login)
                {
                    if (automationStart)
                    {
                        bool res = false;                       
                        {
                            res = m_RealStockA1StrategyClass.StopThread();
                        }
                        if (res)
                        {
                            message = "Automation stopped.";
                            buttons = MessageBoxButtons.OK;
                            dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Exclamation);
                            logger.LogMessage("Automation stopped", MessageType.Informational);
                            automationStart = false;
                            //IRDSPM::Pratiksha::29-12-2020::For color of Automation status
                            Automationstopped();
                            btnStart.Enabled = true;                           
                        }
                        else
                        {
                            message = "Unable to stop automation.";
                            buttons = MessageBoxButtons.OK;
                            dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                            logger.LogMessage("Unable to stop automation", MessageType.Informational);
                        }

                    }
                    else
                    {
                        message = "To perform stop. first Start the automation.";
                        buttons = MessageBoxButtons.OK;
                        dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                        logger.LogMessage("To perform stop. first Start the automation", MessageType.Informational);
                    }
                }
                else
                {
                    message = "Stop operation not able to perform.";
                    buttons = MessageBoxButtons.OK;
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                    logger.LogMessage("Stop operation not able to perform", MessageType.Informational);
                }
            }
            }
            catch (Exception er)
            {
                message = "Error : " + er.Message;
                buttons = MessageBoxButtons.OK;
                dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Error);
                logger.LogMessage("Error : " + er.Message, MessageType.Informational);
            }
        }



        private void btnRefresh_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            logger.LogMessage("clicked on refresh button  ", MessageType.Informational);
            if (login)
            {
                try
                {
                    m_RealStockA1StrategyClass.readTradingSymbol(Settings);                      

                }
                catch (Exception ew)
                {
                    logger.LogMessage("Exception from clicking refresh button : " + ew.Message, MessageType.Exception);
                }
            }
            else
            {
                logger.LogMessage("Please login first to check open positions", MessageType.Informational);
                message = "Please login first to check open positions.";
                buttons = MessageBoxButtons.OK;
                dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Error);
            }
            Cursor.Current = Cursors.Default;
        }

        public void enableButton()
        {
            InvokeUpdateControls();
        }

        public void InvokeUpdateControls()
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new UpdateControlsDelegate(UpdateControls));
            }
            else
            {
                UpdateControls();
            }
        }

        private void UpdateControls()
        {            
            btnStart.Enabled = true;            
        }



        private void btnStopCloseOrder_Click(object sender, EventArgs e)
        {
            try
            {
                logger.LogMessage("Clicked on stop automation and close all order button", MessageType.Informational);
                if (login)
                {
                    if (automationStart)
                    {
                        bool res = false;
                        
                        res = m_RealStockA1StrategyClass.StopThread();
                        m_RealStockA1StrategyClass.CloseAllOrder();                       

                        if (res)
                        {
                            message = "Automation stopped.";
                            buttons = MessageBoxButtons.OK;
                            dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Exclamation);
                            logger.LogMessage("Automation stopped", MessageType.Informational);
                            automationStart = false;
                            //IRDSPM::Pratiksha::29-12-2020::For color of Automation status
                            Automationstopped();
                            btnStart.Enabled = true;                          
                        }
                        else
                        {
                            message = "Unable to stop automation.";
                            buttons = MessageBoxButtons.OK;
                            dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                            logger.LogMessage("Unable to stop automation", MessageType.Informational);
                        }

                    }
                    else
                    {
                        message = "To perform stop. first Start the automation.";
                        buttons = MessageBoxButtons.OK;
                        dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                        logger.LogMessage("To perform stop. first Start the automation", MessageType.Informational);
                    }
                }
                else
                {
                    message = "Stop operation not able to perform.";
                    buttons = MessageBoxButtons.OK;
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                    logger.LogMessage("Stop operation not able to perform", MessageType.Informational);
                }
            }
            catch (Exception er)
            {
                message = "Error : " + er.Message;
                buttons = MessageBoxButtons.OK;
                dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Error);
                logger.LogMessage("Error : " + er.Message, MessageType.Informational);
            }
        }
        private void btnHistorical_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                if (login)
                {
                    Cursor.Current = Cursors.Default;                    
                    logger.LogMessage("Stopped Download", MessageType.Informational);
                    message = "Successfully Downloaded.";
                    buttons = MessageBoxButtons.OK;
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Exclamation);
                }
                else
                {
                    logger.LogMessage("Please login first to download data", MessageType.Informational);
                    message = "Please login first to download data.";
                    buttons = MessageBoxButtons.OK;
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                logger.LogMessage("error in  btnHistorical_Click " + ex.Message, MessageType.Informational);
            }
        }


        //sanika::25-sep-2020::added for resume testing
        public void SaveBin()
        {
            var path = Directory.GetCurrentDirectory();
            string FileName = path + @"/RealStockA1_"+userID+".bin";
           if(m_RealStockA1StrategyClass != null)
            {
                if(m_RealStockA1StrategyClass.m_BasicOperation !=null)
                {
                    m_RealStockA1StrategyClass.m_BasicOperation.SaveBinFile(FileName);
                }
            }
        }

        private void StrategyForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                message = "Do you really want to close strategy?";
                buttons = MessageBoxButtons.YesNo;
                dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);

                if (dialog == DialogResult.Yes)
                {
                    logger.LogMessage("Message box display and clicked on yes!!", MessageType.Informational);
                    //sanika::25-sep-2020::added for resume testing
                    SaveBin();
                    logger.LogMessage("clicked on close form", MessageType.Informational);
                    if (login)
                    {
                        bool res = false;
                        if (AlgoOMS.stopThread())
                        {
                            logger.LogMessage("All threads are closed", MessageType.Informational);
                        }
                        if (m_RealStockA1StrategyClass != null)
                        {
                            //automationWithFile.SaveBin();
                            res = m_RealStockA1StrategyClass.StopThread();
                            if (res)
                            {
                                logger.LogMessage("Chanbreak threads are closed", MessageType.Informational);

                            }
                            else
                                logger.LogMessage("Threads are not able to close", MessageType.Informational);
                        }

                        // Environment.Exit(0);
                        //this.Close();
                        e.Cancel = false;
                    }
                }
                else
                {
                    e.Cancel = true;
                    logger.LogMessage("Message box display and clicked on No!!", MessageType.Informational);
                }

            }
            catch (Exception er)
            {
                message = "Error : " + er.Message;
                buttons = MessageBoxButtons.OK;
                dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Error);
                logger.LogMessage("Error : " + er.Message, MessageType.Exception);
            }
        }

        string GetBroker(INIFile iNIFile)
        {
            string brokerName = "";
            try
            {
                brokerName = iNIFile.IniReadValue("BROKER", "brokername");
            }
            catch (Exception e)
            {
                Trace.WriteLine("exception " + e.Message);
            }
            if (brokerName == "")
            {
                brokerName = "Zerodha";
            }
            return brokerName;
        }

        //IRDSPM::Pratiksha::29-12-2020::For color changes of automation
        private void AutomationNotstarted()
        {
            lblStatusValue.Text = "Waiting";
            lblStatusValue.BackColor = Color.Blue;
            lblStatusValue.ForeColor = Color.White;
        }
        private void Automationstarted()
        {
            lblStatusValue.Text = "Started";
            lblStatusValue.BackColor = Color.LimeGreen;
            lblStatusValue.ForeColor = Color.White;
        }
        private void Automationstopped()
        {
            lblStatusValue.Text = "Stopped";
            lblStatusValue.BackColor = Color.Red;
            lblStatusValue.ForeColor = Color.White;
        }

        private void btnStopCloseOrder_Click_1(object sender, EventArgs e)
        {
            try
            {
                logger.LogMessage("Clicked on stop automation and close all order button", MessageType.Informational);
                message = "It will close the orders and Stop the automation.\n Do you want to close orders and stop the automation?";
                buttons = MessageBoxButtons.YesNo;
                dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Exclamation);
                if (dialog == DialogResult.Yes)
                {
                if (login)
                {
                    if (automationStart)
                    {
                        bool res = false;

                        res = m_RealStockA1StrategyClass.StopThread();
                        m_RealStockA1StrategyClass.CloseAllOrder();

                        if (res)
                        {
                            message = "Automation stopped.";
                            buttons = MessageBoxButtons.OK;
                            dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Exclamation);
                            logger.LogMessage("Automation stopped", MessageType.Informational);
                            automationStart = false;
                            //IRDSPM::Pratiksha::29-12-2020::For color of Automation status
                            Automationstopped();
                            btnStart.Enabled = true;
                        }
                        else
                        {
                            message = "Unable to stop automation.";
                            buttons = MessageBoxButtons.OK;
                            dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                            logger.LogMessage("Unable to stop automation", MessageType.Informational);
                        }

                    }
                    else
                    {
                        message = "To perform stop. first Start the automation.";
                        buttons = MessageBoxButtons.OK;
                        dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                        logger.LogMessage("To perform stop. first Start the automation", MessageType.Informational);
                    }
                }
                else
                {
                    message = "Stop operation not able to perform.";
                    buttons = MessageBoxButtons.OK;
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                    logger.LogMessage("Stop operation not able to perform", MessageType.Informational);
                    }
                }
            }
            catch (Exception er)
            {
                message = "Error : " + er.Message;
                buttons = MessageBoxButtons.OK;
                dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Error);
                logger.LogMessage("Error : " + er.Message, MessageType.Informational);
            }
        }
        //IRDSPM::Pratiksha::29-12-2020::To open trading setting form
        TradingSetting obj;
        private void btnTradeSetting_Click_1(object sender, EventArgs e)
        {
            //IRDSPM::Pratiksha::28-05-2021::added user id as parameter
            obj = new TradingSetting(logger, AlgoOMS, userID);
                obj.ShowDialog();
            }
        //IRDSPM::Pratiksha::29-12-2020::To open symbol setting form
        private void btnSymbosetting_Click_1(object sender, EventArgs e)
        {
            //IRDSPM::Pratiksha::28-05-2021::added user id as parameter
            SymbolSettingDet symbolSettingWindows = new SymbolSettingDet(AlgoOMS, userID);
            symbolSettingWindows.ShowDialog();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
