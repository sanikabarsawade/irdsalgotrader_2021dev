﻿using IRDSAlgoOMS;
using NEEVCAPITALPROJECT;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace NEEVCAPITALPROJECT
{
    public partial class SymbolSettingDet : Form
    {
        string inisymbolList;
        NEEVCAPITALPROJECT.ReadSettings Settings;
        IRDSAlgoOMS.Logger logger = IRDSAlgoOMS.Logger.Instance;
        string title = "RealStockA1 Strategy";
        string message = "";
        MessageBoxButtons buttons = MessageBoxButtons.OK;
        DialogResult dialog;
        List<string> finalExchangeList = new List<string>();
        dynamic ConnectWrapper = null;
        string path = System.AppDomain.CurrentDomain.BaseDirectory + "Configuration";
        List<string> lastthursday = new List<string>();
        int count = 5;
        bool isexit = false;
        public SymbolSettingDet(dynamic kiteConnectWrapper,string userID)
        {
            InitializeComponent();
            try
            {               
                ConnectWrapper = kiteConnectWrapper;
                ExchangeList.Items.Add("OPTIONS");
                if(ExchangeList.Items.Count == 1)
                {
                    ExchangeList.SelectedIndex = 0;
                }
                OrderTypeList.Items.Add("MIS");
                OrderTypeList.Items.Add("NRML");
                //OrderTypeList.SelectedIndex = 0;
                ExpiryPeriodList.Items.Add("Weekly");
                ExpiryPeriodList.Items.Add("Monthly");


                for (int i = 1; i <= count; i++)
                {
                    StrikedtList.Items.Add("ITM" + i);
                }
                StrikedtList.Items.Add("ATM");
                for (int i = 1; i <= count; i++)
                {
                    StrikedtList.Items.Add("OTM" +i);
                }
                FetchDetailsFromINI();

                for (var month = 1; month <= 12; month++)
                {
                    var date = new DateTime(DateTime.Now.Year, month, 1).AddMonths(1).AddDays(-1);
                    while (date.DayOfWeek != DayOfWeek.Thursday)
                {
                        date = date.AddDays(-1);
                }
                string date1 = date.ToString("dd-MMM-yy");
                lastthursday.Add((date1.Replace("-","")).ToString().ToUpper());
                }

            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs", "InitializeComponent : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            try
            {
                if ( SymbolList.SelectedItem == null && ExpirydtList.SelectedItem == null && OrderTypeList.SelectedItem == null && ExpiryPeriodList.SelectedItem == null || txtroundoff.Text.ToString().Length > 0 || txtMarginPerLot.Text.ToString().Length > 0 && txtMarketLotSize.Text.ToString().Length > 0 && txtTotalNumberofLotSize.Text.ToString().Length > 0 && StrikedtList.SelectedItem != null)
                {
                    message = "Do you want to save the changes?";
                    buttons = MessageBoxButtons.YesNo;
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                    if (dialog == DialogResult.Yes)
                    {
                        AddIntoINIFile();
                    }
                }
                else
                {
                    if (ExchangeList.SelectedItem != null && SymbolList.SelectedItem != null && ExpirydtList.Text != null && OrderTypeList.SelectedItem != null && ExpiryPeriodList.SelectedItem != null && txtroundoff.Text.ToString().Length > 0 && txtMarginPerLot.Text.ToString().Length > 0 && txtMarketLotSize.Text.ToString().Length > 0 && txtTotalNumberofLotSize.Text.ToString().Length > 0 && StrikedtList.SelectedItem != null)
                    {
                        Match rountOff1, MarketLotSize1, OneLotSize1, MarginPerLot1;
                        Validations(out rountOff1, out MarketLotSize1, out OneLotSize1, out MarginPerLot1);
                        if (rountOff1.Success && MarketLotSize1.Success && OneLotSize1.Success && MarginPerLot1.Success)
                        {
                            message = "Do you want to save the changes?";
                            buttons = MessageBoxButtons.YesNo;
                            dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                            if (dialog == DialogResult.Yes)
                            {
                                AddIntoINIFile();
                            }
                        }
                        else
                        {
                            string Logmessage = "";
                            string parameter = "";
                            if (!MarketLotSize1.Success) { parameter += "\nMarket Lot Size"; Logmessage += "Market Lot Size,"; }
                            if (!OneLotSize1.Success) { parameter += "\nTotal Number of Lot Size"; Logmessage += "Total Number of Lot Size,"; }
                            if (!rountOff1.Success) { parameter += "\nRound Off"; Logmessage += "Round Off"; }
                            if (!MarginPerLot1.Success) { parameter += "\nMargin Per Lot"; Logmessage += "Margin Per Lot"; }
                            message = "Please add valid values for " + parameter + ".";
                            buttons = MessageBoxButtons.OK;
                            dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                            WriteUniquelogs("SymbolSettingLogs", "btnAdd_Click : " + Logmessage, MessageType.Informational);
                        }
                    }
                    else
                    {
                        CheckNullValuesAndShowPopup();
                    }
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs", "btnApply_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void CheckNullValuesAndShowPopup()
        {
            string Logmessage = "Please add Proper values for ";
            string parameter = "";
            if (ExchangeList.SelectedItem == null) { parameter += "Instruments"; Logmessage += "Instruments,"; }
            if (SymbolList.SelectedItem == null) { parameter += "\nSymbol"; Logmessage += "Symbol,"; }
            if (ExpirydtList.SelectedItem == null) { parameter += "\nExpiry Date"; Logmessage += "Expiry Date,"; }
            if (StrikedtList.SelectedItem == null) { parameter += "\nStrike"; Logmessage += "Strike,"; }
            if (OrderTypeList.SelectedItem == null) { parameter += "\nOrder Type"; Logmessage += "Order Type,"; }
            if (ExpiryPeriodList.SelectedItem == null) { parameter += "\nExpiry Period"; Logmessage += "Expiry Period,"; }
            if (txtMarketLotSize.Text.ToString().Length == 0) { parameter += "\nMarket Lot Size"; Logmessage += "Market Lot Size,"; }
            if (txtTotalNumberofLotSize.Text.ToString().Length == 0) { parameter += "\nTotal Number of Lot Size"; Logmessage += "Total Number of Lot Size,"; }
            if (txtroundoff.Text.ToString().Length == 0) { parameter += "\nRound Off"; Logmessage += "Round Off,"; }
            if (txtMarginPerLot.Text.ToString().Length == 0) { parameter += "\nMargin Per Lot"; Logmessage += "Margin Per Lot"; }
            message = "Please add Proper values for " + parameter + ".";
            buttons = MessageBoxButtons.OK;
            dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
            WriteUniquelogs("SymbolSettingLogs", "btnAdd_Click : " + Logmessage, MessageType.Informational);
        }

        private void AddIntoINIFile()
        {
            bool issaved = false;
            bool isAdded = false;
            int j = 0;
            int counter = 0;
            int k = dataGridView.Rows.Count;
            bool flagSymbolname = false;
            var re = @"^[0-9]{2}[a-zA-Z]{3}[0-9]{2}$";
            INIFile iniObj = new INIFile(inisymbolList);
            DialogResult dialog;
            try
            {
                foreach (DataGridViewRow row in dataGridView.Rows)
                {
                    flagSymbolname = false;
                    if (((row.Cells["SymbolName"].Value.ToString() == null) && (row.Cells["Exchange"].Value.ToString() == null) && (row.Cells["ExpiryDate"].Value.ToString() == null) && (row.Cells["Strike"].Value.ToString() == null) && (row.Cells["ExpiryPeriod"].Value.ToString() == null) && (row.Cells["OrderType"].Value.ToString() == null) && (row.Cells["MarketLotSize"].Value.ToString() == null) && (row.Cells["TotalNumberofLotSize"].Value.ToString() == null) && (row.Cells["RoundOff"].Value.ToString() == null) && (row.Cells["MarginPerLot"].Value.ToString() == null)))
                    {
                    }
                    else
                    {
                        if (SymbolList.SelectedItem != null && ExpirydtList.SelectedItem != null && ExchangeList.SelectedItem != null && StrikedtList.SelectedItem != null && ExpiryPeriodList.SelectedItem != null && OrderTypeList.SelectedItem != null && txtMarketLotSize.Text != null && txtTotalNumberofLotSize.Text != null && txtroundoff.Text != null && txtMarginPerLot.Text != null)
                        {
                            if (SymbolList.SelectedItem.ToString() == row.Cells["SymbolName"].Value.ToString())
                    {

                                if ((row.Cells[0].Value.Equals(SymbolList.SelectedItem.ToString())) && (row.Cells[1].Value.Equals("NFO")) && (row.Cells[2].Value.Equals(ExpirydtList.Text.ToString())) && (row.Cells[3].Value.Equals(StrikedtList.SelectedItem.ToString())) && (row.Cells[4].Value.Equals(ExpiryPeriodList.SelectedItem.ToString())) && (row.Cells[5].Value.Equals(OrderTypeList.SelectedItem.ToString())) && (row.Cells[6].Value.Equals(txtMarketLotSize.Text.ToString())) && (row.Cells[7].Value.Equals(txtTotalNumberofLotSize.Text.ToString())) && (row.Cells[8].Value.Equals(txtroundoff.Text.ToString())) && (row.Cells[9].Value.Equals(txtMarginPerLot.Text.ToString())))
                                {
                                    if (AddUpdateRowinDataGrid(0, false) == true)
                                    {
                                        k += 1;
                                        SymbolList.SelectedIndex = -1;
                                        ClearFields();
                                        issaved = true;
                                        isAdded = true;
                                    }
                                    else
                                    {
                                        issaved = false;
                                    }
                                }
                                else
                                {
                                    foreach (DataGridViewRow r in dataGridView.SelectedRows)
                                    {
                                        r.Cells["SymbolName"].Value = SymbolList.Text;
                                        if (ExchangeList.Text == "OPTIONS")
                                        {
                                            r.Cells["Exchange"].Value = "NFO";
                                        }
                                        else
                                        {
                                            r.Cells["Exchange"].Value = ExchangeList.Text;
                                        }
                                        r.Cells["ExpiryDate"].Value = ExpirydtList.Text;
                                        r.Cells["Strike"].Value = StrikedtList.Text;
                                        r.Cells["ExpiryPeriod"].Value = ExpiryPeriodList.Text;
                                        r.Cells["OrderType"].Value = OrderTypeList.Text;
                                        r.Cells["MarketLotSize"].Value = txtMarketLotSize.Text;
                                        r.Cells["TotalNumberofLotSize"].Value = txtTotalNumberofLotSize.Text;
                                        r.Cells["RoundOff"].Value = txtroundoff.Text;
                                        r.Cells["MarginPerLot"].Value = txtMarginPerLot.Text;
                                        SymbolList.SelectedIndex = -1;
                                        ClearFields();
                                        issaved = true;
                                    }
                                }
                            }
                            else
                            {
                                if (isAdded == false)
                                {
                                    bool isFound = false;
                                    foreach (DataGridViewRow r in dataGridView.Rows)
                                    {
                                        if (SymbolList.SelectedItem.ToString() == r.Cells["SymbolName"].Value.ToString())
                                        {
                                            isFound = true;
                                            break;
                                        }
                                        else
                                        {
                                            isFound = false;
                                        }
                                    }
                                    if (isFound == false)
                        {
                                        if (AddUpdateRowinDataGrid(0, false) == true)
                            {
                                            k += 1;
                                            SymbolList.SelectedIndex = -1;
                                            ClearFields();
                                            issaved = true;
                                            isAdded = true;
                            }
                            else
                            {
                                            issaved = false;
                                        }
                                    }
                                }
                            }
                        }
                        if (((row.Cells["SymbolName"].Value.ToString().Length == 0) || (row.Cells["Exchange"].Value.ToString().Length == 0) || (row.Cells["ExpiryDate"].Value.ToString().Length == 0) || (row.Cells["Strike"].Value.ToString().Length == 0) || (row.Cells["ExpiryPeriod"].Value.ToString().Length == 0) || (row.Cells["OrderType"].Value.ToString().Length == 0) || (row.Cells["MarketLotSize"].Value.ToString().Length == 0) || (row.Cells["TotalNumberofLotSize"].Value.ToString().Length == 0) || (row.Cells["RoundOff"].Value.ToString().Length == 0) || (row.Cells["MarginPerLot"].Value.ToString().Length == 0)))
                                {
                                    message = "Not able to add this because null value is present.";
                            buttons = MessageBoxButtons.OK;
                                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                            WriteUniquelogs("SymbolSettingLogs", "btnApply_Click : Null value present at :" + row.Cells["SymbolName"].Value.ToString() + row.Cells["Exchange"].Value.ToString() + (row.Cells["ExpiryDate"].Value.ToString().Length == 0) + (row.Cells["Strike"].Value.ToString().Length == 0) + (row.Cells["ExpiryPeriod"].Value.ToString().Length == 0) + (row.Cells["OrderType"].Value.ToString().Length == 0) + (row.Cells["MarketLotSize"].Value.ToString().Length == 0) + (row.Cells["TotalNumberofLotSize"].Value.ToString().Length == 0) + (row.Cells["RoundOff"].Value.ToString().Length == 0) + (row.Cells["MarginPerLot"].Value.ToString().Length == 0) + ". Not able to add this value.", MessageType.Informational);
                                }
                                else
                                {
                                    if (row.Cells["SymbolName"].Value.ToString().Length > 0)
                                    {
                                        Match m = Regex.Match(row.Cells["ExpiryDate"].Value.ToString(), re);

                                        if (m.Success)
                                        {
                                            j++;
                                        }
                                        else
                                        {
                                            message = "Not able to add " + row.Cells["SymbolName"].Value.ToString() + " because expiry date is not proper.";
                                            buttons = MessageBoxButtons.OK;
                                            dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                                            WriteUniquelogs("SymbolSettingLogs", "btnApply_Click : " + message, MessageType.Informational);
                                        }
                                    }
                                    else
                                    {
                                        flagSymbolname = false;
                                        message = "Not able to add " + row.Cells["SymbolName"].Value.ToString() + " because symbol name is not proper.";
                                        buttons = MessageBoxButtons.OK;
                                        dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                                        WriteUniquelogs("SymbolSettingLogs", "btnApply_Click : "+ message, MessageType.Informational);
                                    }
                                }
                            }
                        }

                if ((j == k) && (issaved = true))
                        {
                            if (Settings == null)
                            {
                                Settings = new NEEVCAPITALPROJECT.ReadSettings(logger);
                            }
                            iniObj.clearTestingSymbol("TRADINGSYMBOL", null, null, inisymbolList);
                            foreach (DataGridViewRow row in dataGridView.Rows)
                            {
                            string Strikevalue = "";
                            string strike = row.Cells["Strike"].Value.ToString();
                            if (strike.Contains("ATM"))
                            {
                                Strikevalue = "0";
                            }
                            else if (strike.Contains("ITM"))
                            {
                                Strikevalue = "-"+strike.Remove(0, 3);
                            }
                            else if (strike.Contains("OTM"))
                            {
                            Strikevalue = strike.Remove(0, 3);
                            }
                        Settings.writeINIFileSymbolSetting("TRADINGSYMBOL", row.Cells["SymbolName"].Value.ToString().ToUpper() + "," + (row.Cells["Exchange"].Value.ToString().ToUpper()) + "," + (row.Cells["ExpiryDate"].Value.ToString().ToUpper()) + "," + Strikevalue + "," + (row.Cells["ExpiryPeriod"].Value.ToString()) + "," + (row.Cells["OrderType"].Value.ToString()) + "," + (row.Cells["MarketLotSize"].Value.ToString()) + "," + (row.Cells["TotalNumberofLotSize"].Value.ToString()) + "," + (row.Cells["RoundOff"].Value.ToString()) + "," + (row.Cells["MarginPerLot"].Value.ToString()), inisymbolList);
                                counter++;
                        string rowOutout = row.Cells["SymbolName"].Value.ToString().ToUpper() + "," + (row.Cells["Exchange"].Value.ToString().ToUpper()) + "," + (row.Cells["ExpiryDate"].Value.ToString().ToUpper()) + "," + Strikevalue + "," + (row.Cells["ExpiryPeriod"].Value.ToString()) + "," + (row.Cells["OrderType"].Value.ToString()) + "," + (row.Cells["MarketLotSize"].Value.ToString()) + "," + (row.Cells["TotalNumberofLotSize"].Value.ToString()) + "," + (row.Cells["RoundOff"].Value.ToString()) + "," + (row.Cells["MarginPerLot"].Value.ToString());
                        WriteUniquelogs("SymbolSettingLogs", "btnApply_Click :  Details saved " + rowOutout, MessageType.Informational);
                            }

                            message = "Changes updated successfully!!!";
                            buttons = MessageBoxButtons.OK;
                            dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                    //this.Close();

                        WriteUniquelogs("SymbolSettingLogs", "btnApply_Click : "+ message, MessageType.Informational);
                    }
                }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs", "btnApply_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void btnDeleteselectedrow_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridView.Rows.Count > 0)
                {
                    if (dataGridView.SelectedRows.Count == 1)
                    {
                    foreach (DataGridViewRow row in dataGridView.SelectedRows)
                    {
                        //IRDSPM::Pratiksha::04-06-2021::To delete perticular row
                            message = "Are you sure you want to delete the " + row.Cells[0].Value + " ?";
                        buttons = MessageBoxButtons.YesNo;
                        dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);

                        if (dialog == DialogResult.Yes)
                        {
                        dataGridView.Rows.RemoveAt(row.Index);
                                WriteUniquelogs("SymbolSettingLogs", "btnDeleteselectedrow_Click : Row deleted " + row.Cells[0].Value, MessageType.Informational);
                            //IRDSPM::Pratiksha::04-06-2021::For clearing details for deleted symbol
                            SymbolList.Text = null;
                            ClearFields();
                                AddIntoINIFile();
                            }
                        }
                    }
                    else
                    {
                        message = "Please select the row first.";
                        buttons = MessageBoxButtons.OK;
                        dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                    }
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs", "btnDeleteselectedrow_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }
        public void WriteUniquelogs(string fileName, string message, MessageType msgType)
        {
            try
            {
                SymbolLogs log = new SymbolLogs();
                log.createFile(fileName);
                log.LogMessage(message, MessageType.Informational);
                log = null;
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs", "WriteUniquelogs : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                bool found = false;
                if (ExchangeList.SelectedItem != null && SymbolList.SelectedItem != null && ExpirydtList.Text != null && OrderTypeList.SelectedItem != null && ExpiryPeriodList.SelectedItem != null && txtroundoff.Text.ToString().Length > 0 && txtMarginPerLot.Text.ToString().Length > 0 && txtMarketLotSize.Text.ToString().Length > 0 && txtTotalNumberofLotSize.Text.ToString().Length > 0 && StrikedtList.SelectedItem != null)
                {
                    //Match rountOffm, rountOffn, MarketLotSizep1, MarketLotSizep2, OneLotSizep1, OneLotSizep2;
                    //Validations(out rountOffm, out rountOffn, out MarketLotSizep1, out MarketLotSizep2, out OneLotSizep1, out OneLotSizep2);

                    //if ((rountOffm.Success || rountOffn.Success) && (MarketLotSizep1.Success || MarketLotSizep2.Success) && (OneLotSizep1.Success || OneLotSizep2.Success))
                    Match rountOff1, MarketLotSize1, OneLotSize1, MarginPerLot1;
                    Validations(out rountOff1, out MarketLotSize1, out OneLotSize1, out MarginPerLot1);
                    if (rountOff1.Success && MarketLotSize1.Success && OneLotSize1.Success && MarginPerLot1.Success)
                    {
                        foreach (DataGridViewRow row in this.dataGridView.Rows)
                        {
                            if (row.Cells[0].Value.Equals(SymbolList.SelectedItem.ToString()))
                            {
                                found = true;
                                break;
                            }
                        }
                        if (found == false)
                        {
                            AddUpdateRowinDataGrid(0, true);
                        }
                        else
                        {
                            message = "This entry is already exist.";
                            buttons = MessageBoxButtons.OK;
                            dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                            WriteUniquelogs("SymbolSettingLogs", "btnAdd_Click : " + message, MessageType.Informational);
                            ClearFields();
                            SymbolList.SelectedIndex = -1;
                        }
                    }
                    else
                    {
                        string Logmessage = "";
                        string parameter = "";
                        if (!MarketLotSize1.Success) { parameter += "\nMarket Lot Size"; Logmessage += "Market Lot Size,"; }
                        if (!OneLotSize1.Success) { parameter += "\nTotal Number of Lot Size"; Logmessage += "Total Number of Lot Size,"; }
                        if (!rountOff1.Success) { parameter += "\nRound Off"; Logmessage += "Round Off"; }
                        if (!MarginPerLot1.Success) { parameter += "\nMargin Per Lot"; Logmessage += "Margin Per Lot"; }
                        message = "Please add valid values for " + parameter + ".";
                        buttons = MessageBoxButtons.OK;
                        dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                        WriteUniquelogs("SymbolSettingLogs", "btnAdd_Click : " + Logmessage, MessageType.Informational);
                    }
                }
                else
                {
                    CheckNullValuesAndShowPopup();
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs", "btnAdd_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        //private void Validations(out Match rountOffm, out Match rountOffn, out Match MarketLotSizep1, out Match MarketLotSizep2, out Match OneLotSizep1, out Match OneLotSizep2)
        private void Validations(out Match rountOff1,out Match MarketLotSize1, out Match OneLotSize1, out Match MarginPerLot1)
            {
            //string Decpattern = "^[-+]?[.]?([0-9]+[.]?[0-9]+)$";
            //string Intpattern = "^[0-9]*$";
            ////For roundoff
            //Regex r1 = new Regex(Decpattern);
            //Regex r2 = new Regex(Intpattern);
            //rountOffm = r1.Match(txtroundoff.Text.ToString());
            //rountOffn = r2.Match(txtroundoff.Text.ToString());
            ////for Market lot size
            //MarketLotSizep1 = r1.Match(txtMarketLotSize.Text.ToString());
            //MarketLotSizep2 = r2.Match(txtMarketLotSize.Text.ToString());
            ////For Total Number of Lot Size
            //OneLotSizep1 = r1.Match(txtTotalNumberofLotSize.Text.ToString());
            //OneLotSizep2 = r2.Match(txtTotalNumberofLotSize.Text.ToString());

                    string Decpattern = "^[-+]?[.]?([0-9]+[.]?[0-9]+)$";
                    string Intpattern = "^[0-9]*$";
                    //For roundoff
            Regex r1 = new Regex(Intpattern);
            Regex r2 = new Regex(Decpattern);
            rountOff1 = r1.Match(txtroundoff.Text.ToString());
            MarginPerLot1 = r2.Match(txtMarginPerLot.Text.ToString());
            //for Market lot size
            MarketLotSize1 = r1.Match(txtMarketLotSize.Text.ToString());
            //For Total Number of Lot Size
            OneLotSize1 = r1.Match(txtTotalNumberofLotSize.Text.ToString());
        }

        private bool AddUpdateRowinDataGrid(int choice, bool SavedIniFile)
                    {
            bool IsAdded = false;
            try
                {
                bool found = false;
                    string exchange = "";
                    if (ExchangeList.SelectedItem.ToString() == "OPTIONS")
                    {
                        exchange = "NFO";
                    }
                    foreach (DataGridViewRow row in this.dataGridView.Rows)
                    {
                    if ((row.Cells[0].Value.Equals(SymbolList.SelectedItem.ToString())) && (row.Cells[1].Value.Equals(exchange)) && (row.Cells[2].Value.Equals(ExpirydtList.Text.ToString())) && (row.Cells[3].Value.Equals(StrikedtList.SelectedItem.ToString())) && (row.Cells[4].Value.Equals(ExpiryPeriodList.SelectedItem.ToString())) && (row.Cells[5].Value.Equals(OrderTypeList.SelectedItem.ToString())) && (row.Cells[6].Value.Equals(txtMarketLotSize.Text.ToString())) && (row.Cells[7].Value.Equals(txtTotalNumberofLotSize.Text.ToString())) && (row.Cells[8].Value.Equals(txtroundoff.Text.ToString())) && (row.Cells[9].Value.Equals(txtMarginPerLot.Text.ToString())))
                        {
                            found = true;
                            break;
                        }
                    }

                    if (!found)
                    {
                            if (choice == 0)
                            {
                        dataGridView.Rows.Add(SymbolList.SelectedItem.ToString(), exchange, ExpirydtList.SelectedItem.ToString(), StrikedtList.SelectedItem.ToString(), ExpiryPeriodList.SelectedItem.ToString(), OrderTypeList.SelectedItem.ToString(), txtMarketLotSize.Text, txtTotalNumberofLotSize.Text, txtroundoff.Text, txtMarginPerLot.Text);
                        IsAdded = true;
                        ClearFields();
                        SymbolList.SelectedIndex = -1;
                            }
                            else
                            {
                                foreach (DataGridViewRow r in dataGridView.SelectedRows)
                                {
                                    r.Cells["SymbolName"].Value = SymbolList.Text;
                                    if (ExchangeList.Text == "OPTIONS")
                                    {
                                                r.Cells["Exchange"].Value = "NFO";
                                    }
                                    else
                                    {
                                                r.Cells["Exchange"].Value = ExchangeList.Text;
                                    }
                                    r.Cells["ExpiryDate"].Value = ExpirydtList.Text;
                                            r.Cells["Strike"].Value = StrikedtList.Text;
                                    r.Cells["ExpiryPeriod"].Value = ExpiryPeriodList.Text;
                                    r.Cells["OrderType"].Value = OrderTypeList.Text;
                                            r.Cells["MarketLotSize"].Value = txtMarketLotSize.Text;
                                        r.Cells["TotalNumberofLotSize"].Value = txtTotalNumberofLotSize.Text;
                                    r.Cells["RoundOff"].Value = txtroundoff.Text;
                            r.Cells["MarginPerLot"].Value = txtMarginPerLot.Text;
                                }
                            }
                    if (SavedIniFile == true)
                    {
                        AddIntoINIFile();
                        IsAdded = true;
                        WriteUniquelogs("SymbolSettingLogs", "btnAdd_Click : Row added.", MessageType.Informational);
                    }
                            SymbolList.SelectedItem = null;
                            ExpirydtList.Text = null;
                            ExpiryPeriodList.Text = null;
                            OrderTypeList.Text = null;
                            txtroundoff.Text = null;
                                    txtMarketLotSize.Text = null;
                                        txtTotalNumberofLotSize.Text = null;
                                    StrikedtList.Text = null;
                    txtMarginPerLot.Text = null;
                    }
                    else
                    {
                        message = "This entry is already exist.";
                        buttons = MessageBoxButtons.OK;
                        dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                        WriteUniquelogs("SymbolSettingLogs", "btnAdd_Click : "+ message, MessageType.Informational);
                    IsAdded = false;
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs", "AddUpdateRowinDataGrid : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
            return IsAdded;
        }

        private void SymbolList_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                List<string> finalOptionsList = new List<string>();
                ExpirydtList.Items.Clear();
                if (SymbolList.SelectedItem != null)
                {
                    string symbolname = SymbolList.SelectedItem.ToString();
                    string tablename = ExchangeList.SelectedItem.ToString();
                    finalOptionsList = ConnectWrapper.fetchExpiryDtForOptionSymbol(symbolname, tablename);
                    foreach (string listItem in finalOptionsList)
                    {
                        ExpirydtList.Items.Add(listItem);
                    }

                    ClearFields();
                    //IRDSPM::Pratiksha::28-06-2021::round off should selected as 50 and 100
                    if (SymbolList.SelectedItem.ToString().ToUpper() == "NIFTY")
                    {
                        txtroundoff.Text = "50";
                    }
                    else if (SymbolList.SelectedItem.ToString().ToUpper() == "BANKNIFTY")
                    {
                        txtroundoff.Text = "100";
                    }
                    OrderTypeList.SelectedItem = "MIS";
                }
                WriteUniquelogs("SymbolSettingLogs", "SymbolList_SelectedValueChanged : Added expiry dates into list.", MessageType.Informational);
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs", "SymbolList_SelectedValueChanged : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void ExchangeList_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                finalExchangeList = null;
                SymbolList.SelectedItem = null;
                SymbolList.Items.Clear();
                if (ExchangeList.SelectedItem != null)
                {
                    if (ExchangeList.SelectedItem.ToString() == "OPTIONS")
                    {
                        finalExchangeList = ConnectWrapper.GetOptionsSymbols();
                        foreach (string listItem in finalExchangeList)
                        {
                            Trace.WriteLine("Symbol : Combo" + listItem);
                            SymbolList.Items.Add(listItem);
                        }
                    }
                    WriteUniquelogs("SymbolSettingLogs", "ExchangeList_SelectedValueChanged : SymbolList added.", MessageType.Informational);
                }
                else
                {
                    WriteUniquelogs("SymbolSettingLogs", "ExchangeList_SelectedValueChanged : ExpiryPeriodList got clear.", MessageType.Informational);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs", "ExchangeList_SelectedValueChanged : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void ClearFields()
        {
            try
            {
                ExpirydtList.Text = null;
                ExpiryPeriodList.Text = null;
                OrderTypeList.Text = null;
                txtroundoff.Text = null;
                txtMarketLotSize.Text = null;
                txtTotalNumberofLotSize.Text = null;
                StrikedtList.Text = null;
                txtMarginPerLot.Text = null;
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs", "ClearFields : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void FetchDetailsFromINI()
        {
            try
            {
                string filePath = path + @"\StrategySetting.ini";
                inisymbolList = filePath;
                int rank = 0;
                if (File.Exists(inisymbolList))
                {
                    try
                    {
                        if (Settings == null)
                        {
                        Settings = new NEEVCAPITALPROJECT.ReadSettings(logger);
                        }
                        //IRDSPM::Pratiksha::29-04-2021::Used existing method change for reading values
                        Settings.ReadTradingSymbolsNeevCapital(inisymbolList);
                        foreach (var item in Settings.TradingSymbolsInfoList)
                        {
                            try
                            {
                                string TradingSymbol = item.TradingSymbol;
                                string Exchange = item.Exchange;
                                string ExpiryDate = item.ExpiryDate;
                                string strike = "";
                                if (item.Strike.Contains("0"))
                                {
                                    strike = "ATM";
                                }
                                else if (item.Strike.Contains("-"))
                                {
                                    strike = "ITM" + item.Strike.ElementAt(1);
                                }
                                else
                                {
                                    strike = "OTM" + item.Strike;
                                }
                                string ExpiryPeriod = item.ExpiryPeriod;
                                string OrderType = item.OrderType;
                                double lotsize = item.lotsize;
                                int TotalNoOfLots = item.TotalNoOfLots;
                                int Roundoff = item.Roundoff;
                                double marginPerlimit = item.marginPerlot;
                                dataGridView.Rows.Add(new object[] { TradingSymbol, Exchange, ExpiryDate, strike, ExpiryPeriod, OrderType, lotsize, TotalNoOfLots, Roundoff, marginPerlimit });
                                rank++;
                            }
                            catch (Exception ex)
                            {
                                WriteUniquelogs("SymbolSettingLogs", "FetchDetailsFromINI : Exception Error Message = " + ex.Message, MessageType.Exception);
                            }
                        }
                        dataGridView.AllowUserToAddRows = false;
                    }
                    catch (Exception ex)
                    {
                        WriteUniquelogs("SymbolSettingLogs", "FetchDetailsFromINI : Exception Error Message = " + ex.Message, MessageType.Exception);
                    }
                }
                else
                {
                    WriteUniquelogs("SymbolSettingLogs", "FetchDetailsFromINI : INI file not present.", MessageType.Informational);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs", "FetchDetailsFromINI : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                FormClosing1();
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs", "btnCancel_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private bool FormClosing1()
        {
            bool formCloseStatus = false;
            bool changeFound = false;
            try
            {
                //if (ExchangeList.SelectedItem != null && SymbolList.SelectedItem != null && ExpirydtList.Text != null && OrderTypeList.SelectedItem != null && ExpiryPeriodList.SelectedItem != null && txtroundoff.Text.ToString().Length > 0 && txtMarketLotSize.Text.ToString().Length > 0 && txtTotalNumberofLotSize.Text.ToString().Length > 0 && StrikedtList.SelectedItem != null)
                if (ExchangeList.SelectedItem != null || SymbolList.SelectedItem != null || ExpirydtList.Text != null || OrderTypeList.SelectedItem != null || ExpiryPeriodList.SelectedItem != null || txtroundoff.Text.ToString().Length > 0 || txtMarginPerLot.Text.ToString().Length > 0 || txtMarketLotSize.Text.ToString().Length > 0 || txtTotalNumberofLotSize.Text.ToString().Length > 0 || StrikedtList.SelectedItem != null)
                {
                    foreach (DataGridViewRow row in dataGridView.Rows)
                    {
                        if (SymbolList.SelectedItem != null)
                        {
                            if (row.Cells[0].Value.Equals(SymbolList.SelectedItem.ToString()) && (row.Cells[1].Value.Equals("NFO")))
                            {
                                if ((row.Cells[2].Value.ToString().Equals(ExpirydtList.Text.ToString())) && (row.Cells[3].Value.ToString().Equals(StrikedtList.SelectedItem.ToString())) && (row.Cells[4].Value.ToString().Equals(ExpiryPeriodList.SelectedItem.ToString())) && (row.Cells[5].Value.ToString().Equals(OrderTypeList.SelectedItem.ToString())) && (row.Cells[6].Value.ToString().Equals(txtMarketLotSize.Text.ToString())) && (row.Cells[7].Value.ToString().Equals(txtTotalNumberofLotSize.Text.ToString())) && (row.Cells[8].Value.ToString().Equals(txtroundoff.Text.ToString())) && (row.Cells[9].Value.ToString().Equals(txtMarginPerLot.Text.ToString())))
                                {
                                    changeFound = false;
                                    isexit = true;
                                    break;
                                }
                                else
                                {
                                    changeFound = true;
                                }
                            }
                            else
                            {
                                changeFound = true;
                            }
                        }
                    }
                    if (changeFound == true)
                    {
                        message = "You have some unsaved changes, do you want to save those changes?";
                        buttons = MessageBoxButtons.YesNo;
                        dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                        if (DialogResult.Yes == dialog)
                        {
                            if (ExchangeList.SelectedItem != null && SymbolList.SelectedItem != null && ExpirydtList.Text != null && OrderTypeList.SelectedItem != null && ExpiryPeriodList.SelectedItem != null && txtroundoff.Text.ToString().Length > 0 && txtMarginPerLot.Text.ToString().Length > 0 && txtMarketLotSize.Text.ToString().Length > 0 && txtTotalNumberofLotSize.Text.ToString().Length > 0 && StrikedtList.SelectedItem != null)
                            {
                                Match rountOff1, MarketLotSize1, OneLotSize1, MarginPerLot1;
                                Validations(out rountOff1, out MarketLotSize1, out OneLotSize1, out MarginPerLot1);
                                if (rountOff1.Success && MarketLotSize1.Success && OneLotSize1.Success && MarginPerLot1.Success)
                                {
                                    AddIntoINIFile();
                                    isexit = true;
                this.Close();
            }
                                else
                                {
                                    string Logmessage = "";
                                    string parameter = "";
                                    if (!MarketLotSize1.Success) { parameter += "\nMarket Lot Size"; Logmessage += "Market Lot Size,"; }
                                    if (!OneLotSize1.Success) { parameter += "\nTotal Number of Lot Size"; Logmessage += "Total Number of Lot Size,"; }
                                    if (!rountOff1.Success) { parameter += "\nRound Off"; Logmessage += "Round Off"; }
                                    if (!MarginPerLot1.Success) { parameter += "\nMargin Per Lot"; Logmessage += "Margin Per Lot"; }
                                    message = "Please add valid values for " + parameter + ".";
                                    buttons = MessageBoxButtons.OK;
                                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                                    WriteUniquelogs("SymbolSettingLogs", "btnAdd_Click : " + Logmessage, MessageType.Informational);
                                }
                            }
                            else
                            {
                                CheckNullValuesAndShowPopup();
                                formCloseStatus = false;
                            }
                        }
                        else
                        {
                            isexit = true;
                            this.Close();
                            WriteUniquelogs("SymbolSettingLogs", "btnCancel_Click : You clicked on No.", MessageType.Informational);
                        }
                    }
                    else
                    {
                        isexit = true;
                        this.Close();
                        WriteUniquelogs("SymbolSettingLogs", "btnCancel_Click : No change found so close the form.", MessageType.Informational);
                    }
                }
                else
                {
                this.Close();
                    WriteUniquelogs("SymbolSettingLogs", "btnCancel_Click : No change found so close the form.", MessageType.Informational);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs", "btnCancel_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
            return formCloseStatus;
        }

        private void dataGridView_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                this.dataGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                if (e.RowIndex >= 0)
                {
                    DataGridViewRow row = dataGridView.Rows[e.RowIndex];
                    SymbolList.Text = row.Cells[0].Value.ToString();
                    if (row.Cells[1].Value.ToString() == "NFO")
                    {
                        ExchangeList.Text = "OPTIONS";
                    }
                    else
                    {
                        ExchangeList.Text = ExchangeList.Text;
                    }
                    // ExchangeList.Text = row.Cells[1].Value.ToString();
                    ExpirydtList.Text = row.Cells[2].Value.ToString();
                    StrikedtList.Text = row.Cells[3].Value.ToString();
                    ExpiryPeriodList.Text = row.Cells[4].Value.ToString();
                    OrderTypeList.Text = row.Cells[5].Value.ToString();
                    txtMarketLotSize.Text = row.Cells[6].Value.ToString();
                    txtTotalNumberofLotSize.Text = row.Cells[7].Value.ToString();
                    txtroundoff.Text = row.Cells[8].Value.ToString();
                    txtMarginPerLot.Text = row.Cells[9].Value.ToString();
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs", "btnCancel_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridView.SelectedRows.Count == 1)
                {
                    if (ExchangeList.SelectedItem != null && SymbolList.SelectedItem != null && ExpirydtList.Text != null && OrderTypeList.SelectedItem != null && ExpiryPeriodList.SelectedItem != null && txtroundoff.Text.ToString().Length > 0 && txtMarginPerLot.Text.ToString().Length > 0 && txtMarketLotSize.Text.ToString().Length > 0 && txtTotalNumberofLotSize.Text.ToString().Length > 0 && StrikedtList.SelectedItem != null)
                    {
                        Match rountOff1, MarketLotSize1, OneLotSize1, MarginPerLot1;
                        Validations(out rountOff1, out MarketLotSize1, out OneLotSize1, out MarginPerLot1);
                        if (rountOff1.Success && MarketLotSize1.Success && OneLotSize1.Success && MarginPerLot1.Success)
                        {
                            //AddUpdateRowinDataGrid(1, true);
                            AddIntoINIFile();
                        }
                        else
                        {
                            string Logmessage = "";
                            string parameter = "";
                            if (!MarketLotSize1.Success) { parameter += "\nMarket Lot Size"; Logmessage += "Market Lot Size,"; }
                            if (!OneLotSize1.Success) { parameter += "\nTotal Number of Lot Size"; Logmessage += "Total Number of Lot Size,"; }
                            if (!rountOff1.Success) { parameter += "\nRound Off"; Logmessage += "Round Off"; }
                            if (!MarginPerLot1.Success) { parameter += "\nMargin Per Lot"; Logmessage += "Margin Per Lot"; }
                            message = "Please add valid values for " + parameter + ".";
                            buttons = MessageBoxButtons.OK;
                            dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                            WriteUniquelogs("SymbolSettingLogs", "btnAdd_Click : " + Logmessage, MessageType.Informational);
                        }
                    }
                    else
                    {
                        CheckNullValuesAndShowPopup();
                    }
                }
                else
                {
                    message = "Please select the row first.";
                    buttons = MessageBoxButtons.OK;
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs", "btnCancel_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void ExpirydtList_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (SymbolList.SelectedItem != null)
                {
                    string symbolname = SymbolList.SelectedItem.ToString();
                    string tablename = ExchangeList.SelectedItem.ToString();
                    string expirydt = ExpirydtList.SelectedItem.ToString();

                    DateTime dt = Convert.ToDateTime(expirydt);
                    DateTime date1 = new DateTime(dt.Year, dt.Month, dt.Day+1);
                    string expiryDate = date1.ToUniversalTime().ToString("yyyy'-'MM'-'dd");
                    string UTCDate = expiryDate.Split(' ')[0] + "T14:30:00";
                    string quantity = ConnectWrapper.fetchLotsizeForOptionSymbol(symbolname, tablename, UTCDate);
                    txtMarketLotSize.Text = null;
                    txtMarketLotSize.Text = quantity;
                    ExpiryPeriodList.ResetText();
                    if (lastthursday.Contains(ExpirydtList.SelectedItem))
                    {
                        ExpiryPeriodList.SelectedIndex = 1;
                    }
                    else
                    {
                        ExpiryPeriodList.SelectedIndex = 0;
                    }
                }
                WriteUniquelogs("SymbolSettingLogs", "ExpirydtList_SelectedValueChanged : Added expiry dates into list.", MessageType.Informational);
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs", "ExpirydtList_SelectedValueChanged : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void btnClearAll_Click(object sender, EventArgs e)
        {
            SymbolList.SelectedIndex = -1;
            ClearFields();
        }

        private void SymbolSettingDet_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (isexit != true)
            {
                message = "Are you sure you want to close the form?";
                buttons = MessageBoxButtons.YesNo;
                dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                if (DialogResult.Yes == dialog)
                {
                    e.Cancel = false;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void GPAddDet_MouseCaptureChanged(object sender, EventArgs e)
        {
            this.dataGridView.SelectionMode = DataGridViewSelectionMode.CellSelect;
        }
    }
}
