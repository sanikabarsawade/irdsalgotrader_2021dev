﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using IRDSAlgoOMS;

namespace RealStockA1Strategy
{
    public partial class AutoTradingForm : Form
    {
        AlgoOMS AlgoOMS = null;
        public int m_StrategyCount = 1;
        Dictionary<string, StrategyForm> m_StrategyObjects = null;
        string m_StrategyName = "RealStockA1Strategy_";
        bool isComposite = false;
        bool isZerodha = true;
        List<string> m_CompositeAccountNumbers = new List<string>();
        List<string> m_ZerodhaAccountNumbers = new List<string>();
        RealStockA1StrategyClass m_RealStockA1StrategyClass;
        bool login = false;
        bool automationStart = false;
        Logger logger = Logger.Instance;
        string userID = " ";
        ReadSettings Settings;
        string m_TradingSettingFileName = "StrategySetting_";
        string title = "RealStock Strategy";
        //IRDSPM::PRatiksha::30-04-2021::For messagebox Changes
        string message = "";
        MessageBoxButtons buttons;
        DialogResult dialog;
        public AutoTradingForm()
        {
            InitializeComponent();
            NetworkChange.NetworkAvailabilityChanged +=
             new NetworkAvailabilityChangedEventHandler(OnNetworkAvailabilityChanged);

            m_StrategyObjects = new Dictionary<string, StrategyForm>();
            AlgoOMS = new AlgoOMS();
            ReadSettings readSettings = new ReadSettings(null);
            string path = Directory.GetCurrentDirectory();
            string filePath = path + @"\Configuration\AccountNumber.ini";
            if (File.Exists(filePath))
            {
                m_CompositeAccountNumbers = readSettings.ReadCompositeAccountNumbers(filePath);
                m_ZerodhaAccountNumbers = readSettings.ReadZerodhaAccountNumbers(filePath);
            }
            else
            {
                MessageBox.Show("AccountNumber.ini file does not exist");
            }
            //IRDSPM::Pratiksha::07-07-2021::For reading broker name
            readSettings.ReadBrokerName(path + @"\Configuration\Setting.ini");
            string brokerName = readSettings.m_BrokerName;
            if (brokerName.ToLower() == "composite")
            {
                rbtnComposite.Checked = true;
            //IRDS::Jyoti::22-Jul-21::Added for displaying account id in tab name
            if (m_CompositeAccountNumbers.Count > 0)
            {
                for (int i = 0; i < m_CompositeAccountNumbers.Count; i++)
                {
                    TabRealStockStrategy.TabPages[i].Text = "RealStockA1 Strategy " + (i + 1) + " " + m_CompositeAccountNumbers[i];
                }
            }
            }

            else if (brokerName.ToLower() == "zerodha")
            {
                for (int i = 0; i < m_ZerodhaAccountNumbers.Count; i++)
                {
                    TabRealStockStrategy.TabPages[i].Text = "RealStockA1 Strategy " + (i + 1) + " " + m_ZerodhaAccountNumbers[i];
                }
            }

            openForm();
        }

        static void OnNetworkAvailabilityChanged(object sender, NetworkAvailabilityEventArgs e)
        {
            if (e.IsAvailable)
            {
                Console.WriteLine("Network has become available");
            }
            else
            {
                bool flag = false;
                try
                {
                    using (var client = new WebClient())
                    using (client.OpenRead("http://clients3.google.com/generate_204"))
                    {
                        flag = true;
                    }
                }
                catch
                {
                    flag = false;
                }
                if (!flag)
                {
                    MessageBox.Show("Please check internet connection!!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

            }
        }

        private void btnRealStockStrategy1_Click(object sender, EventArgs e)
        {
            string brokerName = GetBroker();
            if (Application.OpenForms["RealStockA1 Strategy 1"] == null)
            {
                string path = Directory.GetCurrentDirectory();
                //sanika::14-Jun-2021::Added for dynamic userid
                if (brokerName == "Composite")
                {
                    string accountNumber = m_CompositeAccountNumbers.ElementAt(0);
                    WriteUniquelogs("StrategySelection", "Clicked on 1st strategy accountNumber " + accountNumber, MessageType.Informational);
                    string iniFile1 = path + @"\Configuration\" + "CompositeSettings_" + accountNumber + ".ini";
                    ReadSettings Settings = new ReadSettings(null, AlgoOMS);
                    string userid = Settings.readUserId(iniFile1);
                    //sanika::14-Jun-2021::Added for dynamic file name and added message box if ini file not found
                    if (File.Exists(iniFile1))
                    {
                        if (Application.OpenForms["RealStockA1 Strategy 1"] == null)
                        {
                            StrategyForm obj = new StrategyForm("1", AlgoOMS, brokerName, userid);
                            obj.Text = "RealStockA1 Strategy 1";
                            obj.Name = "RealStockA1 Strategy 1";
                            obj.Show();
                        }
                    }
                    else
                    {
                        WriteUniquelogs("StrategySelection", "INI file not found", MessageType.Informational);
                        MessageBox.Show("Account not set on strategy 1");
                    }
                }
            }
        }

        private void btnRealStockStrategy2_Click(object sender, EventArgs e)
        {
            bool needtoProceed = false;
            string brokerName = GetBroker();
            if (brokerName == "Composite")
            {
                if (Application.OpenForms["RealStockA1 Strategy 2"] == null)
                {
                    string path = Directory.GetCurrentDirectory();
                    //sanika::14-Jun-2021::Added for dynamic userid
                    string accountNumber = "";
                    if (m_CompositeAccountNumbers.Count > 1)
                    {
                        accountNumber = m_CompositeAccountNumbers.ElementAt(1);
                        needtoProceed = true;
                    }
                    else
                    {
                        WriteUniquelogs("StrategySelection", "Other account not found set default account", MessageType.Informational);
                        accountNumber = m_CompositeAccountNumbers.ElementAt(0);
                        string title = "RealStock Strategy";
                        MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                        DialogResult dialog = System.Windows.Forms.MessageBox.Show("No account set on this button so starting automation on " + accountNumber + "\nDo you want to proceed?", title, buttons, MessageBoxIcon.Information);
                        if (dialog == DialogResult.Yes)
                        {
                            needtoProceed = true;
                            WriteUniquelogs("StrategySelection", "Clicked on yes " + accountNumber, MessageType.Informational);
                        }
                        else
                        {
                            needtoProceed = false;
                            WriteUniquelogs("StrategySelection", "Clicked on no " + accountNumber, MessageType.Informational);
                        }
                    }
                    WriteUniquelogs("StrategySelection", "Clicked on 2nd strategy accountNumber " + accountNumber, MessageType.Informational);
                    string iniFile1 = path + @"\Configuration\" + "CompositeSettings_" + accountNumber + ".ini";
                    ReadSettings Settings = new ReadSettings(null, AlgoOMS);
                    string userid = Settings.readUserId(iniFile1);
                    if (File.Exists(iniFile1))
                    {
                        WriteUniquelogs("StrategySelection", "INI file found", MessageType.Informational);
                        if (Application.OpenForms["RealStockA1 Strategy 2"] == null)
                        {
                            StrategyForm obj = new StrategyForm("2", AlgoOMS, brokerName, userid);
                            obj.Text = "RealStockA1 Strategy 2";
                            obj.Name = "RealStockA1 Strategy 2";
                            obj.Show();
                        }
                    }
                    else
                    {
                        WriteUniquelogs("StrategySelection", "INI file not found", MessageType.Informational);
                        MessageBox.Show("Account not set on strategy 2");
                    }
                }
            }
        }

        private void btnRealStockStrategy3_Click(object sender, EventArgs e)
        {
            bool needtoProceed = false;
            string brokerName = GetBroker();
            if (brokerName == "Composite")
            {
                if (Application.OpenForms["RealStockA1 Strategy 3"] == null)
                {
                    string path = Directory.GetCurrentDirectory();
                    //sanika::14-Jun-2021::Added for dynamic userid
                    string accountNumber = "";
                    if (m_CompositeAccountNumbers.Count > 2)
                    {
                        accountNumber = m_CompositeAccountNumbers.ElementAt(2);
                        needtoProceed = true;
                    }
                    else
                    {
                        WriteUniquelogs("StrategySelection", "Other account not found set default account", MessageType.Informational);
                        accountNumber = m_CompositeAccountNumbers.ElementAt(0);
                        string title = "RealStock Strategy";
                        MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                        DialogResult dialog = System.Windows.Forms.MessageBox.Show("No account set on this button so starting automation on " + accountNumber + "\nDo you want to proceed?", title, buttons, MessageBoxIcon.Information);
                        if (dialog == DialogResult.Yes)
                        {
                            needtoProceed = true;
                            WriteUniquelogs("StrategySelection", "Clicked on yes " + accountNumber, MessageType.Informational);
                        }
                        else
                        {
                            needtoProceed = false;
                            WriteUniquelogs("StrategySelection", "Clicked on no " + accountNumber, MessageType.Informational);
                        }
                    }
                    WriteUniquelogs("StrategySelection", "Clicked on 3rd strategy accountNumber " + accountNumber, MessageType.Informational);
                    if (needtoProceed)
                    {
                        string iniFile1 = path + @"\Configuration\" + "CompositeSettings_" + accountNumber + ".ini";
                        ReadSettings Settings = new ReadSettings(null, AlgoOMS);
                        string userid = Settings.readUserId(iniFile1);
                        //sanika::14-Jun-2021::Added for dynamic file name and added message box if ini file not found
                        if (File.Exists(iniFile1))
                        {
                            WriteUniquelogs("StrategySelection", "INI file found", MessageType.Informational);
                            if (Application.OpenForms["RealStockA1 Strategy 3"] == null)
                            {
                                StrategyForm obj = new StrategyForm("3", AlgoOMS, brokerName, userid);
                                obj.Text = "RealStockA1 Strategy 3";
                                obj.Name = "RealStockA1 Strategy 3";
                                obj.Show();
                            }
                        }
                        else
                        {
                            WriteUniquelogs("StrategySelection", "INI file not found", MessageType.Informational);
                            MessageBox.Show("Account not set on strategy 3");
                        }
                    }
                }
            }
        }

        private void btnRealStockStrategy4_Click(object sender, EventArgs e)
        {
            bool needtoProceed = false;
            string path = Directory.GetCurrentDirectory();
            //sanika::14-Jun-2021::Added for dynamic userid
            string accountNumber = "";
            if (m_CompositeAccountNumbers.Count > 3)
            {
                accountNumber = m_CompositeAccountNumbers.ElementAt(3);
                needtoProceed = true;
            }
            else
            {
                WriteUniquelogs("StrategySelection", "Other account not found set default account", MessageType.Informational);
                accountNumber = m_CompositeAccountNumbers.ElementAt(0);
                string title = "RealStock Strategy";
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                DialogResult dialog = System.Windows.Forms.MessageBox.Show("No account set on this button so starting automation on " + accountNumber + "\nDo you want to proceed?", title, buttons, MessageBoxIcon.Information);
                if (dialog == DialogResult.Yes)
                {
                    needtoProceed = true;
                    WriteUniquelogs("StrategySelection", "Clicked on yes " + accountNumber, MessageType.Informational);
                }
                else
                {
                    needtoProceed = false;
                    WriteUniquelogs("StrategySelection", "Clicked on no " + accountNumber, MessageType.Informational);
                }

            }
            WriteUniquelogs("StrategySelection", "Clicked on 4th strategy accountNumber " + accountNumber, MessageType.Informational);
            if (needtoProceed)
            {
                string iniFile1 = path + @"\Configuration\" + "CompositeSettings_" + accountNumber + ".ini";
                ReadSettings Settings = new ReadSettings(null, AlgoOMS);
                string userid = Settings.readUserId(iniFile1);
                string brokerName = GetBroker();
                //sanika::14-Jun-2021::Added for dynamic file name and added message box if ini file not found
                if (File.Exists(iniFile1))
                {
                    WriteUniquelogs("StrategySelection", "INI file found", MessageType.Informational);
                    if (Application.OpenForms["RealStockA1 Strategy 4"] == null)
                    {
                        StrategyForm obj = new StrategyForm("4", AlgoOMS, brokerName, userid);
                        obj.Text = "RealStockA1 Strategy 4";
                        obj.Name = "RealStockA1 Strategy 4";
                        obj.Show();
                    }
                }
                else
                {
                    WriteUniquelogs("StrategySelection", "INI file not found", MessageType.Informational);
                    MessageBox.Show("Account not set on strategy 4");
                }
            }
        }

        private void btnRealStockStrategy5_Click(object sender, EventArgs e)
        {
            bool needtoProceed = false;
            if (Application.OpenForms["RealStockA1 Strategy 5"] == null)
            {
                string path = Directory.GetCurrentDirectory();
                //sanika::14-Jun-2021::Added for dynamic userid
                string accountNumber = "";
                if (m_CompositeAccountNumbers.Count > 4)
                {
                    accountNumber = m_CompositeAccountNumbers.ElementAt(4);
                    needtoProceed = true;
                }
                else
                {
                    WriteUniquelogs("StrategySelection", "Other account not found set default account", MessageType.Informational);
                    accountNumber = m_CompositeAccountNumbers.ElementAt(0);
                    string title = "RealStock Strategy";
                    MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                    DialogResult dialog = System.Windows.Forms.MessageBox.Show("No account set on this button so starting automation on " + accountNumber + "\nDo you want to proceed?", title, buttons, MessageBoxIcon.Information);
                    if (dialog == DialogResult.Yes)
                    {
                        needtoProceed = true;
                        WriteUniquelogs("StrategySelection", "Clicked on yes " + accountNumber, MessageType.Informational);
                        needtoProceed = true;
                    }
                    else
                    {
                        needtoProceed = false;
                        WriteUniquelogs("StrategySelection", "Clicked on no " + accountNumber, MessageType.Informational);
                    }
                }
                WriteUniquelogs("StrategySelection", "Clicked on 5th strategy accountNumber " + accountNumber, MessageType.Informational);
                if (needtoProceed)
                {
                    string iniFile1 = path + @"\Configuration\" + "CompositeSettings_" + accountNumber + ".ini";
                    ReadSettings Settings = new ReadSettings(null, AlgoOMS);
                    string userid = Settings.readUserId(iniFile1);
                    //sanika::14-Jun-2021::Added for dynamic file name and added message box if ini file not found 
                    if (File.Exists(iniFile1))
                    {
                        WriteUniquelogs("StrategySelection", "INI file found", MessageType.Informational);
                        string brokerName = GetBroker();
                        if (Application.OpenForms["RealStockA1 Strategy 5"] == null)
                        {
                            StrategyForm obj = new StrategyForm("5", AlgoOMS, brokerName, userid);
                            obj.Text = "RealStockA1 Strategy 5";
                            obj.Name = "RealStockA1 Strategy 5";
                            obj.Show();
                        }
                    }
                    else
                    {
                        WriteUniquelogs("StrategySelection", "INI file not found", MessageType.Informational);
                        MessageBox.Show("Account not set on strategy 5");
                    }
                }
            }
        }

        private void AutoTradingForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                string message = "Do you really want to exit?";
                //IRDSPM::Pratiksha::29-07-2020::Change the title
                string title = "RealStock Strategy";
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                DialogResult dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                if (dialog == DialogResult.Yes)
                {
                    Environment.Exit(0);
                }
                else
                {
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void rbtnZerodha_CheckedChanged(object sender, EventArgs e)
        {
            isComposite = false;
        }

        private void rbtnComposite_CheckedChanged(object sender, EventArgs e)
        {
            isComposite = true;
        }

        public string GetBroker()
        {
            if (Settings == null)
            {
                Settings = new ReadSettings(logger, AlgoOMS);
            }
            string path = Directory.GetCurrentDirectory();
            Settings.ReadBrokerName(path + @"\Configuration\Setting.ini");
            string brokerName = Settings.m_BrokerName;
            if (brokerName == "Composite")
            {
            if (isComposite)
            {
                brokerName = "Composite";
            }
            }
            else if (brokerName == "Zerodha")
            {
                //if (isZerodha)
                {
                    brokerName = "Zerodha";
                }
            }
            return brokerName;
        }
        //private void button2_Click(object sender, EventArgs e)
        //{
        //    objCreation();

        //}

        public void loginToAccount()
        {
            btnLogin.Enabled = false;
            Cursor.Current = Cursors.WaitCursor;
            string error = "";
            int loginCount = 0;
            try
            {
                AlgoOMS.Connect(userID);
                while (loginCount < 3)
                {
                    if (AlgoOMS.kiteWrapperConnect != null || AlgoOMS.compositeWrapperConnect != null)
                    {
                        if (AlgoOMS.GetLoginStatus(userID))
                        {

                        }
                        break;
                    }
                }
                loginCount++;

            }
            catch (Exception er)
            {
                error = er.Message;
                if (error.Contains("empty") && error.Contains("api_key") && error.Contains("access_token"))
                {
                    MessageBox.Show("Error : Please check account credentials", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    MessageBox.Show("Error : " + er.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            finally
            {
                if (error.Contains("System.Data.SQLite"))
                {
                    MessageBox.Show("Please add dll for sqlite", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                }
            }
            Cursor.Current = Cursors.Default;

        }

        public void objCreation()
        {
            ReadSettings Settings;
            Logger logger = Logger.Instance;

            string userid = "KPA70";
            StrategyForm obj = new StrategyForm("1", AlgoOMS, "Zerodha", userid);
            obj.Text = "RealStockA1 Strategy 1";
            obj.Name = "RealStockA1 Strategy 1";

            string path = Directory.GetCurrentDirectory();
            string iniFile = path + @"\Configuration\" + "APISetting.ini";
            if (File.Exists(iniFile))
            {
                Settings = new ReadSettings(logger, AlgoOMS);
                //userID = Settings.readUserId(iniFile);
            }
            else
            {
                MessageBox.Show("INI file does not exit!!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            AlgoOMS.SetBroker(0);
            AlgoOMS.CreateKiteConnectObject(userID, "APISetting.ini");
            loginToAccount();
        }

        private void accountSettingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Logger logger = Logger.Instance;
            FrmConfigSettings obj = new FrmConfigSettings(logger);
            obj.ShowDialog();
        }

        public void WriteUniquelogs(string symbol, string message, MessageType msgType)
        {
            try
            {
                SymbolLogs log = new SymbolLogs();
                log.createFile(symbol);
                log.LogMessage(message, MessageType.Informational);
                log = null;
            }
            catch (Exception e)
            {
                Console.WriteLine("WriteUniquelogs : Exception : " + e.Message);
            }
        }


        private void openForm()
        {
            string userid = "";
            ReadSettings Settings = null;
            string iniFile1 = "";
            string accountNumber = "";
            string brokerName = GetBroker();
            if (Application.OpenForms["RealStockA1 Strategy 1"] == null)
            {
                string path = Directory.GetCurrentDirectory();
                //sanika::14-Jun-2021::Added for dynamic userid
                if (brokerName == "Composite")
                {
                    accountNumber = m_CompositeAccountNumbers.ElementAt(0);
                    WriteUniquelogs("StrategySelection", "Clicked on 1st strategy accountNumber " + accountNumber, MessageType.Informational);
                    iniFile1 = path + @"\Configuration\" + "CompositeSettings_" + accountNumber + ".ini";
                    Settings = new ReadSettings(null, AlgoOMS);
                    userid = Settings.readUserId(iniFile1);
                }
                else if (brokerName == "Zerodha")
                {
                    accountNumber = m_ZerodhaAccountNumbers.ElementAt(0);
                    WriteUniquelogs("StrategySelection", "Clicked on 1st strategy accountNumber " + accountNumber, MessageType.Informational);
                    iniFile1 = path + @"\Configuration\" + "ZerodhaSettings_" + accountNumber + ".ini";
                    Settings = new ReadSettings(null, AlgoOMS);
                    userid = Settings.readUserId(iniFile1);
                }
                    if (AlgoOMS.GetLoginStatus(userid) == false)
                    {
                        //sanika::14-Jun-2021::Added for dynamic file name and added message box if ini file not found
                        if (File.Exists(iniFile1))
                        {
                            if (Application.OpenForms["RealStockA1 Strategy 1"] == null)
                            {
                                StrategyForm obj = new StrategyForm("1", AlgoOMS, brokerName, userid);
                                obj.TopLevel = false;
                                obj.Parent = Tab1;
                                obj.Dock = DockStyle.Fill;
                                obj.Text = "RealStockA1 Strategy 1";
                                obj.Name = "RealStockA1 Strategy 1";
                                obj.Show();
                            }
                        }
                        else
                        {
                            WriteUniquelogs("StrategySelection", "INI file not found", MessageType.Informational);
                            MessageBox.Show("Account not set on strategy 1");
                        }
                    }
                }
            }
        //}

        private void TabRealStockStrategy_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (TabRealStockStrategy.SelectedTab.Name == "Tab1")
            {
                openForm();
            }
            else if (TabRealStockStrategy.SelectedTab.Name == "Tab2")
            {
                string brokerName = GetBroker();
                if (brokerName == "Composite")
                {
                    if (Application.OpenForms["RealStockA1 Strategy 2"] == null)
                    {
                        string path = Directory.GetCurrentDirectory();
                        //sanika::14-Jun-2021::Added for dynamic userid
                        string accountNumber = "";
                        if (m_CompositeAccountNumbers.Count > 1)
                        {
                            accountNumber = m_CompositeAccountNumbers.ElementAt(1);
                        }
                        WriteUniquelogs("StrategySelection", "Clicked on 2nd strategy accountNumber " + accountNumber, MessageType.Informational);
                        string iniFile1 = path + @"\Configuration\" + "CompositeSettings_" + accountNumber + ".ini";
                        ReadSettings Settings = new ReadSettings(null, AlgoOMS);
                        string userid = Settings.readUserId(iniFile1);

                        if (File.Exists(iniFile1))
                        {
                            WriteUniquelogs("StrategySelection", "INI file found", MessageType.Informational);
                            if (Application.OpenForms["RealStockA1 Strategy 2"] == null)
                            {
                                StrategyForm obj = new StrategyForm("2", AlgoOMS, brokerName, userid);
                                obj.TopLevel = false;
                                obj.Parent = Tab2;
                                obj.Dock = DockStyle.Fill;
                                obj.Text = "RealStockA1 Strategy 2";
                                obj.Name = "RealStockA1 Strategy 2";
                                obj.Show();
                            }
                        }
                        else
                        {
                            WriteUniquelogs("StrategySelection", "INI file not found", MessageType.Informational);
                            MessageBox.Show("Account not set on strategy 2");
                        }
                    }
                }
            }
            else if (TabRealStockStrategy.SelectedTab.Name == "Tab3")
            {
                string brokerName = GetBroker();
                if (brokerName == "Composite")
                {
                    if (Application.OpenForms["RealStockA1 Strategy 3"] == null)
                    {
                        string path = Directory.GetCurrentDirectory();
                        //sanika::14-Jun-2021::Added for dynamic userid
                        string accountNumber = "";
                        if (m_CompositeAccountNumbers.Count > 2)
                        {
                            accountNumber = m_CompositeAccountNumbers.ElementAt(2);
                        }
                        WriteUniquelogs("StrategySelection", "Clicked on 3rd strategy accountNumber " + accountNumber, MessageType.Informational);

                            string iniFile1 = path + @"\Configuration\" + "CompositeSettings_" + accountNumber + ".ini";
                            ReadSettings Settings = new ReadSettings(null, AlgoOMS);
                            string userid = Settings.readUserId(iniFile1);
                            //sanika::14-Jun-2021::Added for dynamic file name and added message box if ini file not found
                            if (File.Exists(iniFile1))
                            {
                                WriteUniquelogs("StrategySelection", "INI file found", MessageType.Informational);
                                if (Application.OpenForms["RealStockA1 Strategy 3"] == null)
                                {
                                    StrategyForm obj = new StrategyForm("3", AlgoOMS, brokerName, userid);
                                    obj.TopLevel = false;
                                    obj.Parent = Tab3;
                                    obj.Dock = DockStyle.Fill;
                                    obj.Text = "RealStockA1 Strategy 3";
                                    obj.Name = "RealStockA1 Strategy 3";
                                    obj.Show();
                                }
                            }
                            else
                            {
                                WriteUniquelogs("StrategySelection", "INI file not found", MessageType.Informational);
                                MessageBox.Show("Account not set on strategy 3");
                            }
                        }
                    }
                }
            else if (TabRealStockStrategy.SelectedTab.Name == "Tab4")
            {
                string path = Directory.GetCurrentDirectory();
                //sanika::14-Jun-2021::Added for dynamic userid
                string accountNumber = "";
                if (m_CompositeAccountNumbers.Count > 3)
                {
                    accountNumber = m_CompositeAccountNumbers.ElementAt(3);
                    }
                WriteUniquelogs("StrategySelection", "Clicked on 4th strategy accountNumber " + accountNumber, MessageType.Informational);

                    string iniFile1 = path + @"\Configuration\" + "CompositeSettings_" + accountNumber + ".ini";
                    ReadSettings Settings = new ReadSettings(null, AlgoOMS);
                    string userid = Settings.readUserId(iniFile1);
                    string brokerName = GetBroker();
                    //sanika::14-Jun-2021::Added for dynamic file name and added message box if ini file not found
                    if (File.Exists(iniFile1))
                    {
                        WriteUniquelogs("StrategySelection", "INI file found", MessageType.Informational);
                        if (Application.OpenForms["RealStockA1 Strategy 4"] == null)
                        {
                            StrategyForm obj = new StrategyForm("4", AlgoOMS, brokerName, userid);
                            obj.TopLevel = false;
                            obj.Parent = Tab4;
                            obj.Dock = DockStyle.Fill;
                            obj.Text = "RealStockA1 Strategy 4";
                            obj.Name = "RealStockA1 Strategy 4";
                            obj.Show();
                        }
                    }
                    else
                    {
                        WriteUniquelogs("StrategySelection", "INI file not found", MessageType.Informational);
                        MessageBox.Show("Account not set on strategy 4");
                    }
                }
            else if (TabRealStockStrategy.SelectedTab.Name == "Tab5")
            {
                if (Application.OpenForms["RealStockA1 Strategy 5"] == null)
                {
                    string path = Directory.GetCurrentDirectory();
                    //sanika::14-Jun-2021::Added for dynamic userid
                    string accountNumber = "";
                    if (m_CompositeAccountNumbers.Count > 4)
                    {
                        accountNumber = m_CompositeAccountNumbers.ElementAt(4);
                    }
                    WriteUniquelogs("StrategySelection", "Clicked on 5th strategy accountNumber " + accountNumber, MessageType.Informational);
                        string iniFile1 = path + @"\Configuration\" + "CompositeSettings_" + accountNumber + ".ini";
                        ReadSettings Settings = new ReadSettings(null, AlgoOMS);
                        string userid = Settings.readUserId(iniFile1);

                        //sanika::14-Jun-2021::Added for dynamic file name and added message box if ini file not found 
                        if (File.Exists(iniFile1))
                        {
                            WriteUniquelogs("StrategySelection", "INI file found", MessageType.Informational);
                            string brokerName = GetBroker();
                            if (Application.OpenForms["RealStockA1 Strategy 5"] == null)
                            {
                                StrategyForm obj = new StrategyForm("5", AlgoOMS, brokerName, userid);
                                obj.TopLevel = false;
                                obj.Parent = Tab5;
                                obj.Dock = DockStyle.Fill;
                                obj.Text = "RealStockA1 Strategy 5";
                                obj.Name = "RealStockA1 Strategy 5";
                                obj.Show();
                            }
                        }
                        else
                        {
                            WriteUniquelogs("StrategySelection", "INI file not found", MessageType.Informational);
                            MessageBox.Show("Account not set on strategy 5");
                    }
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                string message = "Do you really want to exit?";
                //IRDSPM::Pratiksha::29-07-2020::Change the title
                string title = "RealStock Strategy";
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                DialogResult dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                if (dialog == DialogResult.Yes)
                {
                    Environment.Exit(0);
                }
            }
            catch (Exception ex)
            {

            }
        }

        //26-July-2021::Pratiksha::Change the backcolor of tab Header
        private void tabControl1_DrawItem(object sender, DrawItemEventArgs e)
        {
            Graphics g = e.Graphics;
            TabPage tp = TabRealStockStrategy.TabPages[e.Index];

            StringFormat sf = new StringFormat();
            sf.Alignment = StringAlignment.Center;
            sf.LineAlignment = StringAlignment.Center;
            // This is the rectangle to draw "over" the tabpage title
            RectangleF headerRect = new RectangleF(e.Bounds.X, e.Bounds.Y + 2, e.Bounds.Width, e.Bounds.Height - 2);

            // This is the default colour to use for the non-selected tabs
            SolidBrush sb = new SolidBrush(Color.DarkGray);

            // This changes the colour if we're trying to draw the selected tabpage
            if (TabRealStockStrategy.SelectedIndex == e.Index)
                sb.Color = Color.Transparent;

            // Colour the header of the current tabpage based on what we did above
            g.FillRectangle(sb, e.Bounds);

            //Remember to redraw the text - I'm always using black for title text
            g.DrawString(tp.Text, TabRealStockStrategy.Font, new SolidBrush(Color.Black), headerRect, sf);
            TabRealStockStrategy.Font = new Font("Arial", 11, FontStyle.Bold);
        }
    }
}
