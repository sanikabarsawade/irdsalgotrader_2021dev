﻿namespace RealStockA1Strategy
{
    partial class SymbolSettingWindows
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent(dynamic obj, string userID, SymbolSettingWindows symbolSettingWindows, string iniChoose)
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AutoTradingForm));
            this.elementHost1 = new System.Windows.Forms.Integration.ElementHost();
            this.symbolSettings1 = new RealStockA1Strategy.SymbolSettings(obj, userID, symbolSettingWindows, iniChoose);
            this.SuspendLayout();
            // 
            // elementHost1
            // 
            this.elementHost1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.elementHost1.Location = new System.Drawing.Point(0, 0);
            this.elementHost1.Name = "elementHost1";
            this.elementHost1.Size = new System.Drawing.Size(510, 517);
            this.elementHost1.TabIndex = 0;
            this.elementHost1.Text = "elementHost1";
            this.elementHost1.Child = this.symbolSettings1;
            // 
            // buySellSymbolWindowsForm
            // 
            this.ClientSize = new System.Drawing.Size(510, 517);
            this.Controls.Add(this.elementHost1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Controls.Add(this.elementHost1);
            this.Name = "SymbolSettingWindows" + iniChoose;
            this.Text = "Symbol Settings " + iniChoose;
            this.ResumeLayout(false);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.MaximizeBox = false;
        }

        #endregion

        private System.Windows.Forms.Integration.ElementHost elementHost1;
        private SymbolSettings symbolSettings1;
    }
}