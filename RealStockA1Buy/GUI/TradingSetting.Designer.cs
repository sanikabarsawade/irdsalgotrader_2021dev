﻿
namespace RealStockA1Strategy
{
    partial class TradingSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TradingSetting));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblAccountNumber = new System.Windows.Forms.Label();
            this.pnlAccountNumber = new System.Windows.Forms.Panel();
            this.txtAccountNumber = new System.Windows.Forms.TextBox();
            this.txtStartEntryTime = new System.Windows.Forms.DateTimePicker();
            this.txtEndEntryTime = new System.Windows.Forms.DateTimePicker();
            this.txtEndExitTime = new System.Windows.Forms.DateTimePicker();
            this.lblAdditionalAccount = new System.Windows.Forms.Label();
            this.pnlBreakEvenStop = new System.Windows.Forms.Panel();
            this.TrueBreakEvenStop = new System.Windows.Forms.RadioButton();
            this.FalseBreakEvenStop = new System.Windows.Forms.RadioButton();
            this.pnlStoplossRejectTimeInterval = new System.Windows.Forms.Panel();
            this.txtStoplossRejectTimeInterval = new System.Windows.Forms.TextBox();
            this.lblStoplossRejectTimeInterval = new System.Windows.Forms.Label();
            this.lblBreakEvenStop = new System.Windows.Forms.Label();
            this.lblEndExitTime = new System.Windows.Forms.Label();
            this.lblEndEntryTime = new System.Windows.Forms.Label();
            this.lblStartEntryTime = new System.Windows.Forms.Label();
            this.AdditionalList = new System.Windows.Forms.ComboBox();
            this.pnlAdditionalAccount = new System.Windows.Forms.Panel();
            this.txtAdditionalAccount = new System.Windows.Forms.TextBox();
            this.lblNoOfLots = new System.Windows.Forms.Label();
            this.btnApply = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.AccountDetList = new System.Windows.Forms.ComboBox();
            this.lblChooseAccount = new System.Windows.Forms.Label();
            this.btnOpenSymbolSetting = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.pnlAccountNumber.SuspendLayout();
            this.pnlBreakEvenStop.SuspendLayout();
            this.pnlStoplossRejectTimeInterval.SuspendLayout();
            this.pnlAdditionalAccount.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblAccountNumber);
            this.groupBox1.Controls.Add(this.pnlAccountNumber);
            this.groupBox1.Controls.Add(this.txtStartEntryTime);
            this.groupBox1.Controls.Add(this.txtEndEntryTime);
            this.groupBox1.Controls.Add(this.txtEndExitTime);
            this.groupBox1.Controls.Add(this.lblAdditionalAccount);
            this.groupBox1.Controls.Add(this.pnlBreakEvenStop);
            this.groupBox1.Controls.Add(this.pnlStoplossRejectTimeInterval);
            this.groupBox1.Controls.Add(this.lblStoplossRejectTimeInterval);
            this.groupBox1.Controls.Add(this.lblBreakEvenStop);
            this.groupBox1.Controls.Add(this.lblEndExitTime);
            this.groupBox1.Controls.Add(this.lblEndEntryTime);
            this.groupBox1.Controls.Add(this.lblStartEntryTime);
            this.groupBox1.Controls.Add(this.AdditionalList);
            this.groupBox1.Controls.Add(this.pnlAdditionalAccount);
            this.groupBox1.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 94);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(754, 385);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Trade Details";
            // 
            // lblAccountNumber
            // 
            this.lblAccountNumber.AutoSize = true;
            this.lblAccountNumber.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblAccountNumber.Location = new System.Drawing.Point(20, 37);
            this.lblAccountNumber.Name = "lblAccountNumber";
            this.lblAccountNumber.Size = new System.Drawing.Size(131, 19);
            this.lblAccountNumber.TabIndex = 3;
            this.lblAccountNumber.Text = "Account Number";
            // 
            // pnlAccountNumber
            // 
            this.pnlAccountNumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlAccountNumber.Controls.Add(this.txtAccountNumber);
            this.pnlAccountNumber.Location = new System.Drawing.Point(353, 30);
            this.pnlAccountNumber.Name = "pnlAccountNumber";
            this.pnlAccountNumber.Size = new System.Drawing.Size(383, 32);
            this.pnlAccountNumber.TabIndex = 4;
            this.pnlAccountNumber.TabStop = true;
            // 
            // txtAccountNumber
            // 
            this.txtAccountNumber.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtAccountNumber.Font = new System.Drawing.Font("Arial", 13F);
            this.txtAccountNumber.Location = new System.Drawing.Point(7, 2);
            this.txtAccountNumber.Name = "txtAccountNumber";
            this.txtAccountNumber.Size = new System.Drawing.Size(371, 25);
            this.txtAccountNumber.TabIndex = 5;
            // 
            // txtStartEntryTime
            // 
            this.txtStartEntryTime.CustomFormat = "HH:mm";
            this.txtStartEntryTime.Font = new System.Drawing.Font("Arial", 12F);
            this.txtStartEntryTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txtStartEntryTime.Location = new System.Drawing.Point(353, 79);
            this.txtStartEntryTime.Name = "txtStartEntryTime";
            this.txtStartEntryTime.ShowUpDown = true;
            this.txtStartEntryTime.Size = new System.Drawing.Size(383, 30);
            this.txtStartEntryTime.TabIndex = 6;
            // 
            // txtEndEntryTime
            // 
            this.txtEndEntryTime.CustomFormat = "HH:mm";
            this.txtEndEntryTime.Font = new System.Drawing.Font("Arial", 12F);
            this.txtEndEntryTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txtEndEntryTime.Location = new System.Drawing.Point(353, 127);
            this.txtEndEntryTime.Name = "txtEndEntryTime";
            this.txtEndEntryTime.ShowUpDown = true;
            this.txtEndEntryTime.Size = new System.Drawing.Size(383, 30);
            this.txtEndEntryTime.TabIndex = 7;
            // 
            // txtEndExitTime
            // 
            this.txtEndExitTime.CustomFormat = "HH:mm";
            this.txtEndExitTime.Font = new System.Drawing.Font("Arial", 12F);
            this.txtEndExitTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txtEndExitTime.Location = new System.Drawing.Point(353, 175);
            this.txtEndExitTime.Name = "txtEndExitTime";
            this.txtEndExitTime.ShowUpDown = true;
            this.txtEndExitTime.Size = new System.Drawing.Size(383, 30);
            this.txtEndExitTime.TabIndex = 8;
            // 
            // lblAdditionalAccount
            // 
            this.lblAdditionalAccount.AutoSize = true;
            this.lblAdditionalAccount.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblAdditionalAccount.Location = new System.Drawing.Point(20, 338);
            this.lblAdditionalAccount.Name = "lblAdditionalAccount";
            this.lblAdditionalAccount.Size = new System.Drawing.Size(144, 19);
            this.lblAdditionalAccount.TabIndex = 64;
            this.lblAdditionalAccount.Text = "Additional Account";
            // 
            // pnlBreakEvenStop
            // 
            this.pnlBreakEvenStop.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlBreakEvenStop.Controls.Add(this.TrueBreakEvenStop);
            this.pnlBreakEvenStop.Controls.Add(this.FalseBreakEvenStop);
            this.pnlBreakEvenStop.Location = new System.Drawing.Point(353, 219);
            this.pnlBreakEvenStop.Name = "pnlBreakEvenStop";
            this.pnlBreakEvenStop.Size = new System.Drawing.Size(383, 37);
            this.pnlBreakEvenStop.TabIndex = 11;
            this.pnlBreakEvenStop.TabStop = true;
            // 
            // TrueBreakEvenStop
            // 
            this.TrueBreakEvenStop.AutoSize = true;
            this.TrueBreakEvenStop.Location = new System.Drawing.Point(52, 6);
            this.TrueBreakEvenStop.Name = "TrueBreakEvenStop";
            this.TrueBreakEvenStop.Size = new System.Drawing.Size(62, 23);
            this.TrueBreakEvenStop.TabIndex = 12;
            this.TrueBreakEvenStop.TabStop = true;
            this.TrueBreakEvenStop.Text = "True";
            this.TrueBreakEvenStop.UseVisualStyleBackColor = true;
            // 
            // FalseBreakEvenStop
            // 
            this.FalseBreakEvenStop.AutoSize = true;
            this.FalseBreakEvenStop.Location = new System.Drawing.Point(229, 6);
            this.FalseBreakEvenStop.Name = "FalseBreakEvenStop";
            this.FalseBreakEvenStop.Size = new System.Drawing.Size(69, 23);
            this.FalseBreakEvenStop.TabIndex = 13;
            this.FalseBreakEvenStop.TabStop = true;
            this.FalseBreakEvenStop.Text = "False";
            this.FalseBreakEvenStop.UseVisualStyleBackColor = true;
            // 
            // pnlStoplossRejectTimeInterval
            // 
            this.pnlStoplossRejectTimeInterval.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlStoplossRejectTimeInterval.Controls.Add(this.txtStoplossRejectTimeInterval);
            this.pnlStoplossRejectTimeInterval.Location = new System.Drawing.Point(353, 279);
            this.pnlStoplossRejectTimeInterval.Name = "pnlStoplossRejectTimeInterval";
            this.pnlStoplossRejectTimeInterval.Size = new System.Drawing.Size(383, 32);
            this.pnlStoplossRejectTimeInterval.TabIndex = 14;
            this.pnlStoplossRejectTimeInterval.TabStop = true;
            // 
            // txtStoplossRejectTimeInterval
            // 
            this.txtStoplossRejectTimeInterval.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtStoplossRejectTimeInterval.Font = new System.Drawing.Font("Arial", 13F);
            this.txtStoplossRejectTimeInterval.Location = new System.Drawing.Point(7, 2);
            this.txtStoplossRejectTimeInterval.Name = "txtStoplossRejectTimeInterval";
            this.txtStoplossRejectTimeInterval.Size = new System.Drawing.Size(371, 25);
            this.txtStoplossRejectTimeInterval.TabIndex = 15;
            // 
            // lblStoplossRejectTimeInterval
            // 
            this.lblStoplossRejectTimeInterval.AutoSize = true;
            this.lblStoplossRejectTimeInterval.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblStoplossRejectTimeInterval.Location = new System.Drawing.Point(20, 284);
            this.lblStoplossRejectTimeInterval.Name = "lblStoplossRejectTimeInterval";
            this.lblStoplossRejectTimeInterval.Size = new System.Drawing.Size(216, 19);
            this.lblStoplossRejectTimeInterval.TabIndex = 51;
            this.lblStoplossRejectTimeInterval.Text = "Stoploss Reject Time Interval";
            // 
            // lblBreakEvenStop
            // 
            this.lblBreakEvenStop.AutoSize = true;
            this.lblBreakEvenStop.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblBreakEvenStop.Location = new System.Drawing.Point(20, 230);
            this.lblBreakEvenStop.Name = "lblBreakEvenStop";
            this.lblBreakEvenStop.Size = new System.Drawing.Size(131, 19);
            this.lblBreakEvenStop.TabIndex = 53;
            this.lblBreakEvenStop.Text = "Break Even Stop";
            // 
            // lblEndExitTime
            // 
            this.lblEndExitTime.AutoSize = true;
            this.lblEndExitTime.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblEndExitTime.Location = new System.Drawing.Point(20, 184);
            this.lblEndExitTime.Name = "lblEndExitTime";
            this.lblEndExitTime.Size = new System.Drawing.Size(108, 19);
            this.lblEndExitTime.TabIndex = 59;
            this.lblEndExitTime.Text = "End Exit Time";
            // 
            // lblEndEntryTime
            // 
            this.lblEndEntryTime.AutoSize = true;
            this.lblEndEntryTime.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblEndEntryTime.Location = new System.Drawing.Point(20, 136);
            this.lblEndEntryTime.Name = "lblEndEntryTime";
            this.lblEndEntryTime.Size = new System.Drawing.Size(121, 19);
            this.lblEndEntryTime.TabIndex = 61;
            this.lblEndEntryTime.Text = "End Entry Time";
            // 
            // lblStartEntryTime
            // 
            this.lblStartEntryTime.AutoSize = true;
            this.lblStartEntryTime.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblStartEntryTime.Location = new System.Drawing.Point(20, 83);
            this.lblStartEntryTime.Name = "lblStartEntryTime";
            this.lblStartEntryTime.Size = new System.Drawing.Size(126, 19);
            this.lblStartEntryTime.TabIndex = 62;
            this.lblStartEntryTime.Text = "Start Entry Time";
            // 
            // AdditionalList
            // 
            this.AdditionalList.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.AdditionalList.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.AdditionalList.Enabled = false;
            this.AdditionalList.Font = new System.Drawing.Font("Arial", 13F);
            this.AdditionalList.FormattingEnabled = true;
            this.AdditionalList.Location = new System.Drawing.Point(235, 331);
            this.AdditionalList.Name = "AdditionalList";
            this.AdditionalList.Size = new System.Drawing.Size(112, 33);
            this.AdditionalList.TabIndex = 16;
            this.AdditionalList.SelectedValueChanged += new System.EventHandler(this.AdditionalList_SelectedValueChanged);
            // 
            // pnlAdditionalAccount
            // 
            this.pnlAdditionalAccount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlAdditionalAccount.Controls.Add(this.txtAdditionalAccount);
            this.pnlAdditionalAccount.Enabled = false;
            this.pnlAdditionalAccount.Location = new System.Drawing.Point(353, 332);
            this.pnlAdditionalAccount.Name = "pnlAdditionalAccount";
            this.pnlAdditionalAccount.Size = new System.Drawing.Size(383, 32);
            this.pnlAdditionalAccount.TabIndex = 17;
            this.pnlAdditionalAccount.TabStop = true;
            // 
            // txtAdditionalAccount
            // 
            this.txtAdditionalAccount.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtAdditionalAccount.Font = new System.Drawing.Font("Arial", 13F);
            this.txtAdditionalAccount.Location = new System.Drawing.Point(7, 2);
            this.txtAdditionalAccount.Name = "txtAdditionalAccount";
            this.txtAdditionalAccount.Size = new System.Drawing.Size(371, 25);
            this.txtAdditionalAccount.TabIndex = 18;
            // 
            // lblNoOfLots
            // 
            this.lblNoOfLots.AutoSize = true;
            this.lblNoOfLots.Location = new System.Drawing.Point(20, 251);
            this.lblNoOfLots.Name = "lblNoOfLots";
            this.lblNoOfLots.Size = new System.Drawing.Size(186, 19);
            this.lblNoOfLots.TabIndex = 57;
            this.lblNoOfLots.Text = "Total Number of Lot Size";
            // 
            // btnApply
            // 
            this.btnApply.Font = new System.Drawing.Font("Arial", 11F);
            this.btnApply.Location = new System.Drawing.Point(467, 491);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(134, 43);
            this.btnApply.TabIndex = 20;
            this.btnApply.Text = "Apply";
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("Arial", 11F);
            this.btnCancel.Location = new System.Drawing.Point(628, 491);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(134, 43);
            this.btnCancel.TabIndex = 21;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.AccountDetList);
            this.groupBox2.Controls.Add(this.lblChooseAccount);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 8F);
            this.groupBox2.Location = new System.Drawing.Point(13, 14);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(753, 74);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Account Details";
            // 
            // AccountDetList
            // 
            this.AccountDetList.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.AccountDetList.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.AccountDetList.Font = new System.Drawing.Font("Arial", 13F);
            this.AccountDetList.FormattingEnabled = true;
            this.AccountDetList.Location = new System.Drawing.Point(352, 24);
            this.AccountDetList.Name = "AccountDetList";
            this.AccountDetList.Size = new System.Drawing.Size(383, 33);
            this.AccountDetList.TabIndex = 1;
            this.AccountDetList.SelectedIndexChanged += new System.EventHandler(this.AccountDetList_SelectedIndexChanged);
            // 
            // lblChooseAccount
            // 
            this.lblChooseAccount.AutoSize = true;
            this.lblChooseAccount.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblChooseAccount.Location = new System.Drawing.Point(23, 31);
            this.lblChooseAccount.Name = "lblChooseAccount";
            this.lblChooseAccount.Size = new System.Drawing.Size(129, 19);
            this.lblChooseAccount.TabIndex = 0;
            this.lblChooseAccount.Text = "Choose Account";
            // 
            // btnOpenSymbolSetting
            // 
            this.btnOpenSymbolSetting.Font = new System.Drawing.Font("Arial", 11F);
            this.btnOpenSymbolSetting.Location = new System.Drawing.Point(22, 492);
            this.btnOpenSymbolSetting.Name = "btnOpenSymbolSetting";
            this.btnOpenSymbolSetting.Size = new System.Drawing.Size(197, 43);
            this.btnOpenSymbolSetting.TabIndex = 19;
            this.btnOpenSymbolSetting.Text = "Symbol Setting";
            this.btnOpenSymbolSetting.UseVisualStyleBackColor = true;
            this.btnOpenSymbolSetting.Click += new System.EventHandler(this.btnOpenSymbolSetting_Click);
            // 
            // TradingSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(778, 550);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnOpenSymbolSetting);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.btnCancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "TradingSetting";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Trading Setting";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.pnlAccountNumber.ResumeLayout(false);
            this.pnlAccountNumber.PerformLayout();
            this.pnlBreakEvenStop.ResumeLayout(false);
            this.pnlBreakEvenStop.PerformLayout();
            this.pnlStoplossRejectTimeInterval.ResumeLayout(false);
            this.pnlStoplossRejectTimeInterval.PerformLayout();
            this.pnlAdditionalAccount.ResumeLayout(false);
            this.pnlAdditionalAccount.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        //private System.Windows.Forms.Label lblPutStrikeValue;
        //private System.Windows.Forms.Label lblCallStrikeValue;
        private System.Windows.Forms.Label lblNoOfLots;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Panel pnlBreakEvenStop;
        private System.Windows.Forms.RadioButton FalseBreakEvenStop;
        private System.Windows.Forms.RadioButton TrueBreakEvenStop;
        //private System.Windows.Forms.Panel panel5;
        //private System.Windows.Forms.TextBox txtEndEntryTime;
        //private System.Windows.Forms.Panel panel6;
        //private System.Windows.Forms.TextBox txtStartExitTime;
        //private System.Windows.Forms.Panel pnlNoOfLots;
        //private System.Windows.Forms.TextBox txtNoOfLots;
        //private System.Windows.Forms.Panel pnlInterval;
        //private System.Windows.Forms.TextBox txtInterval;
        //private System.Windows.Forms.Panel panel7;
        //private System.Windows.Forms.TextBox txtEndExitTime;
        //private System.Windows.Forms.Panel pnlCallStrikeValue;
       // private System.Windows.Forms.TextBox txtCallStrikeValue;
        //private System.Windows.Forms.Panel pnlStoplossPercent;
       // private System.Windows.Forms.TextBox txtStoplossPercent;
        //private System.Windows.Forms.Panel pnlPutStrikeValue;
       // private System.Windows.Forms.TextBox txtPutStrikeValue;
        private System.Windows.Forms.Panel pnlStoplossRejectTimeInterval;
        private System.Windows.Forms.TextBox txtStoplossRejectTimeInterval;
        //private System.Windows.Forms.Panel pnlReducePercentInLot;
       // private System.Windows.Forms.TextBox txtReducePercentInLot;
        private System.Windows.Forms.Panel pnlAccountNumber;
        private System.Windows.Forms.TextBox txtAccountNumber;
        private System.Windows.Forms.DateTimePicker txtStartEntryTime;
        private System.Windows.Forms.DateTimePicker txtEndEntryTime;
        private System.Windows.Forms.DateTimePicker txtEndExitTime;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox AccountDetList;
        private System.Windows.Forms.Button btnOpenSymbolSetting;
        private System.Windows.Forms.Panel pnlAdditionalAccount;
        private System.Windows.Forms.TextBox txtAdditionalAccount;
        private System.Windows.Forms.ComboBox AdditionalList;
        protected System.Windows.Forms.Label lblAccountNumber;
        protected System.Windows.Forms.Label lblEndExitTime;
        protected System.Windows.Forms.Label lblEndEntryTime;
        protected System.Windows.Forms.Label lblStartEntryTime;
        protected System.Windows.Forms.Label lblStoplossRejectTimeInterval;
        protected System.Windows.Forms.Label lblBreakEvenStop;
        //protected System.Windows.Forms.Label lblStoplossPercent;
        protected System.Windows.Forms.Label lblChooseAccount;
        protected System.Windows.Forms.Label lblAdditionalAccount;
    }
}