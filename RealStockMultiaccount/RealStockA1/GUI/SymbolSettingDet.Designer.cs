﻿
namespace RealStockA1Strategy
{
    partial class SymbolSettingDet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDeleteselectedrow = new System.Windows.Forms.Button();
            this.btnApply = new System.Windows.Forms.Button();
            this.GPSymbolDet = new System.Windows.Forms.GroupBox();
            this.GPAccount = new System.Windows.Forms.GroupBox();
            this.lblAccount = new System.Windows.Forms.Label();
            this.AccountList = new System.Windows.Forms.ComboBox();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.SymbolName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Exchange = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ExpiryDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ExpiryPeriod = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OrderType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MarketLotSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalNumberofLotSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RoundOff = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CallStrikePrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PutStrikePrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Stoploss = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GPAddDet = new System.Windows.Forms.GroupBox();
            this.lblInstruments = new System.Windows.Forms.Label();
            this.ExchangeList = new System.Windows.Forms.ComboBox();
            this.lblSymbol = new System.Windows.Forms.Label();
            this.SymbolList = new System.Windows.Forms.ComboBox();
            this.lblExpiryDate = new System.Windows.Forms.Label();
            this.ExpirydtList = new System.Windows.Forms.ComboBox();
            this.lblExpiryPeriod = new System.Windows.Forms.Label();
            this.ExpiryPeriodList = new System.Windows.Forms.ComboBox();
            this.lblOrdertype = new System.Windows.Forms.Label();
            this.OrderTypeList = new System.Windows.Forms.ComboBox();
            this.lblMarketLotSize = new System.Windows.Forms.Label();
            this.pnlLotSize = new System.Windows.Forms.Panel();
            this.txtMarketLotSize = new System.Windows.Forms.TextBox();
            this.lblTotalNumberofLotSize = new System.Windows.Forms.Label();
            this.pnlTotalNumberofLotSize = new System.Windows.Forms.Panel();
            this.txtTotalNumberofLotSize = new System.Windows.Forms.TextBox();
            this.lblRoundOff = new System.Windows.Forms.Label();
            this.pnlRoundoff = new System.Windows.Forms.Panel();
            this.txtroundoff = new System.Windows.Forms.TextBox();
            this.lblstrikepriceCall = new System.Windows.Forms.Label();
            this.strikepricecallSubList = new System.Windows.Forms.ComboBox();
            this.lblstrikepriceput = new System.Windows.Forms.Label();
            this.strikepriceputSubList = new System.Windows.Forms.ComboBox();
            this.lblStoploss = new System.Windows.Forms.Label();
            this.pnlStoploss = new System.Windows.Forms.Panel();
            this.txtStoploss = new System.Windows.Forms.TextBox();
            this.pnlMarginLimit = new System.Windows.Forms.Panel();
            this.txtMarginLimit = new System.Windows.Forms.TextBox();
            this.btnClearAll = new System.Windows.Forms.Button();
            this.lblMargin = new System.Windows.Forms.Label();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.strikepriceputList = new System.Windows.Forms.ComboBox();
            this.strikepriceCallList = new System.Windows.Forms.ComboBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.BtnImport = new System.Windows.Forms.Button();
            this.btnExport = new System.Windows.Forms.Button();
            this.SaveFDExportFile = new System.Windows.Forms.SaveFileDialog();
            this.OpenFDImportFile = new System.Windows.Forms.OpenFileDialog();
            this.GPSymbolDet.SuspendLayout();
            this.GPAccount.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.GPAddDet.SuspendLayout();
            this.pnlLotSize.SuspendLayout();
            this.pnlTotalNumberofLotSize.SuspendLayout();
            this.pnlRoundoff.SuspendLayout();
            this.pnlStoploss.SuspendLayout();
            this.pnlMarginLimit.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnDeleteselectedrow
            // 
            this.btnDeleteselectedrow.Font = new System.Drawing.Font("Arial", 10.5F);
            this.btnDeleteselectedrow.Location = new System.Drawing.Point(51, 682);
            this.btnDeleteselectedrow.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnDeleteselectedrow.Name = "btnDeleteselectedrow";
            this.btnDeleteselectedrow.Size = new System.Drawing.Size(213, 39);
            this.btnDeleteselectedrow.TabIndex = 23;
            this.btnDeleteselectedrow.Text = "Delete symbol";
            this.btnDeleteselectedrow.UseVisualStyleBackColor = true;
            this.btnDeleteselectedrow.Click += new System.EventHandler(this.btnDeleteselectedrow_Click);
            // 
            // btnApply
            // 
            this.btnApply.Font = new System.Drawing.Font("Arial", 10.5F);
            this.btnApply.Location = new System.Drawing.Point(823, 682);
            this.btnApply.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(134, 40);
            this.btnApply.TabIndex = 22;
            this.btnApply.Text = "Apply";
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Visible = false;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // GPSymbolDet
            // 
            this.GPSymbolDet.Controls.Add(this.GPAccount);
            this.GPSymbolDet.Controls.Add(this.dataGridView);
            this.GPSymbolDet.Controls.Add(this.GPAddDet);
            this.GPSymbolDet.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GPSymbolDet.Location = new System.Drawing.Point(12, 14);
            this.GPSymbolDet.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.GPSymbolDet.Name = "GPSymbolDet";
            this.GPSymbolDet.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.GPSymbolDet.Size = new System.Drawing.Size(1117, 663);
            this.GPSymbolDet.TabIndex = 1;
            this.GPSymbolDet.TabStop = false;
            this.GPSymbolDet.Text = "Symbol Details";
            // 
            // GPAccount
            // 
            this.GPAccount.Controls.Add(this.lblAccount);
            this.GPAccount.Controls.Add(this.AccountList);
            this.GPAccount.Enabled = false;
            this.GPAccount.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GPAccount.Location = new System.Drawing.Point(13, 22);
            this.GPAccount.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.GPAccount.Name = "GPAccount";
            this.GPAccount.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.GPAccount.Size = new System.Drawing.Size(1099, 66);
            this.GPAccount.TabIndex = 0;
            this.GPAccount.TabStop = false;
            this.GPAccount.Text = "Account";
            // 
            // lblAccount
            // 
            this.lblAccount.AutoSize = true;
            this.lblAccount.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblAccount.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblAccount.Location = new System.Drawing.Point(21, 25);
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Size = new System.Drawing.Size(129, 19);
            this.lblAccount.TabIndex = 1;
            this.lblAccount.Text = "Choose Account";
            // 
            // AccountList
            // 
            this.AccountList.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.AccountList.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.AccountList.Font = new System.Drawing.Font("Arial", 12F);
            this.AccountList.FormattingEnabled = true;
            this.AccountList.Location = new System.Drawing.Point(213, 21);
            this.AccountList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.AccountList.Name = "AccountList";
            this.AccountList.Size = new System.Drawing.Size(239, 31);
            this.AccountList.TabIndex = 2;
            this.AccountList.SelectedIndexChanged += new System.EventHandler(this.AccountList_SelectedIndexChanged);
            // 
            // dataGridView
            // 
            this.dataGridView.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SymbolName,
            this.Exchange,
            this.ExpiryDate,
            this.ExpiryPeriod,
            this.OrderType,
            this.MarketLotSize,
            this.TotalNumberofLotSize,
            this.RoundOff,
            this.CallStrikePrice,
            this.PutStrikePrice,
            this.Stoploss});
            this.dataGridView.Location = new System.Drawing.Point(13, 362);
            this.dataGridView.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.ReadOnly = true;
            this.dataGridView.RowHeadersWidth = 51;
            this.dataGridView.RowTemplate.Height = 24;
            this.dataGridView.Size = new System.Drawing.Size(1099, 281);
            this.dataGridView.TabIndex = 1;
            this.dataGridView.TabStop = false;
            this.dataGridView.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView_CellMouseClick);
            // 
            // SymbolName
            // 
            this.SymbolName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.SymbolName.HeaderText = "Symbol Name";
            this.SymbolName.MinimumWidth = 6;
            this.SymbolName.Name = "SymbolName";
            this.SymbolName.ReadOnly = true;
            this.SymbolName.Width = 113;
            // 
            // Exchange
            // 
            this.Exchange.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Exchange.FillWeight = 90F;
            this.Exchange.HeaderText = "Exchange";
            this.Exchange.MinimumWidth = 6;
            this.Exchange.Name = "Exchange";
            this.Exchange.ReadOnly = true;
            // 
            // ExpiryDate
            // 
            this.ExpiryDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ExpiryDate.HeaderText = "Expiry Date";
            this.ExpiryDate.MinimumWidth = 10;
            this.ExpiryDate.Name = "ExpiryDate";
            this.ExpiryDate.ReadOnly = true;
            // 
            // ExpiryPeriod
            // 
            this.ExpiryPeriod.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ExpiryPeriod.FillWeight = 76.19048F;
            this.ExpiryPeriod.HeaderText = "Expiry Period";
            this.ExpiryPeriod.MinimumWidth = 6;
            this.ExpiryPeriod.Name = "ExpiryPeriod";
            this.ExpiryPeriod.ReadOnly = true;
            // 
            // OrderType
            // 
            this.OrderType.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.OrderType.FillWeight = 76.19048F;
            this.OrderType.HeaderText = "Order Type";
            this.OrderType.MinimumWidth = 6;
            this.OrderType.Name = "OrderType";
            this.OrderType.ReadOnly = true;
            // 
            // MarketLotSize
            // 
            this.MarketLotSize.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.MarketLotSize.FillWeight = 76.19048F;
            this.MarketLotSize.HeaderText = "Market Lot Size";
            this.MarketLotSize.MinimumWidth = 6;
            this.MarketLotSize.Name = "MarketLotSize";
            this.MarketLotSize.ReadOnly = true;
            // 
            // TotalNumberofLotSize
            // 
            this.TotalNumberofLotSize.FillWeight = 80F;
            this.TotalNumberofLotSize.HeaderText = "Total Number of Lot Size";
            this.TotalNumberofLotSize.MinimumWidth = 6;
            this.TotalNumberofLotSize.Name = "TotalNumberofLotSize";
            this.TotalNumberofLotSize.ReadOnly = true;
            this.TotalNumberofLotSize.Width = 80;
            // 
            // RoundOff
            // 
            this.RoundOff.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.RoundOff.FillWeight = 76.19048F;
            this.RoundOff.HeaderText = "Round Off";
            this.RoundOff.MinimumWidth = 6;
            this.RoundOff.Name = "RoundOff";
            this.RoundOff.ReadOnly = true;
            // 
            // CallStrikePrice
            // 
            this.CallStrikePrice.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CallStrikePrice.FillWeight = 76.19048F;
            this.CallStrikePrice.HeaderText = "Call Strike Multiplier";
            this.CallStrikePrice.MinimumWidth = 6;
            this.CallStrikePrice.Name = "CallStrikePrice";
            this.CallStrikePrice.ReadOnly = true;
            // 
            // PutStrikePrice
            // 
            this.PutStrikePrice.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.PutStrikePrice.FillWeight = 76.19048F;
            this.PutStrikePrice.HeaderText = "Put Strike Multiplier";
            this.PutStrikePrice.MinimumWidth = 6;
            this.PutStrikePrice.Name = "PutStrikePrice";
            this.PutStrikePrice.ReadOnly = true;
            // 
            // Stoploss
            // 
            this.Stoploss.HeaderText = "Stoploss";
            this.Stoploss.MinimumWidth = 6;
            this.Stoploss.Name = "Stoploss";
            this.Stoploss.ReadOnly = true;
            this.Stoploss.Width = 70;
            // 
            // GPAddDet
            // 
            this.GPAddDet.Controls.Add(this.lblInstruments);
            this.GPAddDet.Controls.Add(this.ExchangeList);
            this.GPAddDet.Controls.Add(this.lblSymbol);
            this.GPAddDet.Controls.Add(this.SymbolList);
            this.GPAddDet.Controls.Add(this.lblExpiryDate);
            this.GPAddDet.Controls.Add(this.ExpirydtList);
            this.GPAddDet.Controls.Add(this.lblExpiryPeriod);
            this.GPAddDet.Controls.Add(this.ExpiryPeriodList);
            this.GPAddDet.Controls.Add(this.lblOrdertype);
            this.GPAddDet.Controls.Add(this.OrderTypeList);
            this.GPAddDet.Controls.Add(this.lblMarketLotSize);
            this.GPAddDet.Controls.Add(this.pnlLotSize);
            this.GPAddDet.Controls.Add(this.lblTotalNumberofLotSize);
            this.GPAddDet.Controls.Add(this.pnlTotalNumberofLotSize);
            this.GPAddDet.Controls.Add(this.lblRoundOff);
            this.GPAddDet.Controls.Add(this.pnlRoundoff);
            this.GPAddDet.Controls.Add(this.lblstrikepriceCall);
            this.GPAddDet.Controls.Add(this.strikepricecallSubList);
            this.GPAddDet.Controls.Add(this.lblstrikepriceput);
            this.GPAddDet.Controls.Add(this.strikepriceputSubList);
            this.GPAddDet.Controls.Add(this.lblStoploss);
            this.GPAddDet.Controls.Add(this.pnlStoploss);
            this.GPAddDet.Controls.Add(this.pnlMarginLimit);
            this.GPAddDet.Controls.Add(this.btnClearAll);
            this.GPAddDet.Controls.Add(this.lblMargin);
            this.GPAddDet.Controls.Add(this.btnUpdate);
            this.GPAddDet.Controls.Add(this.btnAdd);
            this.GPAddDet.Controls.Add(this.strikepriceputList);
            this.GPAddDet.Controls.Add(this.strikepriceCallList);
            this.GPAddDet.Font = new System.Drawing.Font("Arial", 10F);
            this.GPAddDet.Location = new System.Drawing.Point(13, 94);
            this.GPAddDet.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.GPAddDet.Name = "GPAddDet";
            this.GPAddDet.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.GPAddDet.Size = new System.Drawing.Size(1099, 262);
            this.GPAddDet.TabIndex = 2;
            this.GPAddDet.TabStop = false;
            this.GPAddDet.Text = "Add details";
            // 
            // lblInstruments
            // 
            this.lblInstruments.AutoSize = true;
            this.lblInstruments.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblInstruments.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblInstruments.Location = new System.Drawing.Point(25, 30);
            this.lblInstruments.Name = "lblInstruments";
            this.lblInstruments.Size = new System.Drawing.Size(93, 19);
            this.lblInstruments.TabIndex = 18;
            this.lblInstruments.Text = "Instruments";
            // 
            // ExchangeList
            // 
            this.ExchangeList.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ExchangeList.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.ExchangeList.Font = new System.Drawing.Font("Arial", 11F);
            this.ExchangeList.FormatString = "```````";
            this.ExchangeList.FormattingEnabled = true;
            this.ExchangeList.Location = new System.Drawing.Point(212, 20);
            this.ExchangeList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ExchangeList.Name = "ExchangeList";
            this.ExchangeList.Size = new System.Drawing.Size(239, 29);
            this.ExchangeList.TabIndex = 3;
            this.ExchangeList.SelectedValueChanged += new System.EventHandler(this.ExchangeList_SelectedValueChanged);
            // 
            // lblSymbol
            // 
            this.lblSymbol.AutoSize = true;
            this.lblSymbol.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblSymbol.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblSymbol.Location = new System.Drawing.Point(25, 66);
            this.lblSymbol.Name = "lblSymbol";
            this.lblSymbol.Size = new System.Drawing.Size(63, 19);
            this.lblSymbol.TabIndex = 17;
            this.lblSymbol.Text = "Symbol";
            // 
            // SymbolList
            // 
            this.SymbolList.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.SymbolList.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.SymbolList.Font = new System.Drawing.Font("Arial", 11F);
            this.SymbolList.FormattingEnabled = true;
            this.SymbolList.Location = new System.Drawing.Point(212, 58);
            this.SymbolList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.SymbolList.Name = "SymbolList";
            this.SymbolList.Size = new System.Drawing.Size(239, 29);
            this.SymbolList.TabIndex = 4;
            this.SymbolList.SelectedValueChanged += new System.EventHandler(this.SymbolList_SelectedValueChanged);
            // 
            // lblExpiryDate
            // 
            this.lblExpiryDate.AutoSize = true;
            this.lblExpiryDate.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblExpiryDate.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblExpiryDate.Location = new System.Drawing.Point(25, 105);
            this.lblExpiryDate.Name = "lblExpiryDate";
            this.lblExpiryDate.Size = new System.Drawing.Size(94, 19);
            this.lblExpiryDate.TabIndex = 16;
            this.lblExpiryDate.Text = "Expiry Date";
            // 
            // ExpirydtList
            // 
            this.ExpirydtList.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ExpirydtList.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.ExpirydtList.Font = new System.Drawing.Font("Arial", 11F);
            this.ExpirydtList.FormattingEnabled = true;
            this.ExpirydtList.Location = new System.Drawing.Point(212, 96);
            this.ExpirydtList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ExpirydtList.Name = "ExpirydtList";
            this.ExpirydtList.Size = new System.Drawing.Size(239, 29);
            this.ExpirydtList.TabIndex = 5;
            this.ExpirydtList.SelectedValueChanged += new System.EventHandler(this.ExpirydtList_SelectedValueChanged);
            // 
            // lblExpiryPeriod
            // 
            this.lblExpiryPeriod.AutoSize = true;
            this.lblExpiryPeriod.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblExpiryPeriod.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblExpiryPeriod.Location = new System.Drawing.Point(25, 143);
            this.lblExpiryPeriod.Name = "lblExpiryPeriod";
            this.lblExpiryPeriod.Size = new System.Drawing.Size(108, 19);
            this.lblExpiryPeriod.TabIndex = 13;
            this.lblExpiryPeriod.Text = "Expiry Period";
            // 
            // ExpiryPeriodList
            // 
            this.ExpiryPeriodList.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ExpiryPeriodList.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.ExpiryPeriodList.Font = new System.Drawing.Font("Arial", 11F);
            this.ExpiryPeriodList.FormattingEnabled = true;
            this.ExpiryPeriodList.Location = new System.Drawing.Point(212, 134);
            this.ExpiryPeriodList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ExpiryPeriodList.Name = "ExpiryPeriodList";
            this.ExpiryPeriodList.Size = new System.Drawing.Size(239, 29);
            this.ExpiryPeriodList.TabIndex = 6;
            // 
            // lblOrdertype
            // 
            this.lblOrdertype.AutoSize = true;
            this.lblOrdertype.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblOrdertype.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblOrdertype.Location = new System.Drawing.Point(25, 181);
            this.lblOrdertype.Name = "lblOrdertype";
            this.lblOrdertype.Size = new System.Drawing.Size(91, 19);
            this.lblOrdertype.TabIndex = 9;
            this.lblOrdertype.Text = "Order Type";
            // 
            // OrderTypeList
            // 
            this.OrderTypeList.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.OrderTypeList.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.OrderTypeList.Font = new System.Drawing.Font("Arial", 11F);
            this.OrderTypeList.FormattingEnabled = true;
            this.OrderTypeList.Location = new System.Drawing.Point(212, 172);
            this.OrderTypeList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.OrderTypeList.Name = "OrderTypeList";
            this.OrderTypeList.Size = new System.Drawing.Size(239, 29);
            this.OrderTypeList.TabIndex = 7;
            // 
            // lblMarketLotSize
            // 
            this.lblMarketLotSize.AutoSize = true;
            this.lblMarketLotSize.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblMarketLotSize.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblMarketLotSize.Location = new System.Drawing.Point(25, 219);
            this.lblMarketLotSize.Name = "lblMarketLotSize";
            this.lblMarketLotSize.Size = new System.Drawing.Size(122, 19);
            this.lblMarketLotSize.TabIndex = 8;
            this.lblMarketLotSize.Text = "Market Lot Size";
            // 
            // pnlLotSize
            // 
            this.pnlLotSize.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlLotSize.Controls.Add(this.txtMarketLotSize);
            this.pnlLotSize.Location = new System.Drawing.Point(212, 210);
            this.pnlLotSize.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pnlLotSize.Name = "pnlLotSize";
            this.pnlLotSize.Size = new System.Drawing.Size(238, 32);
            this.pnlLotSize.TabIndex = 8;
            this.pnlLotSize.TabStop = true;
            // 
            // txtMarketLotSize
            // 
            this.txtMarketLotSize.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtMarketLotSize.Font = new System.Drawing.Font("Arial", 11F);
            this.txtMarketLotSize.Location = new System.Drawing.Point(5, 4);
            this.txtMarketLotSize.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtMarketLotSize.Name = "txtMarketLotSize";
            this.txtMarketLotSize.Size = new System.Drawing.Size(229, 22);
            this.txtMarketLotSize.TabIndex = 9;
            // 
            // lblTotalNumberofLotSize
            // 
            this.lblTotalNumberofLotSize.AutoSize = true;
            this.lblTotalNumberofLotSize.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblTotalNumberofLotSize.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblTotalNumberofLotSize.Location = new System.Drawing.Point(459, 30);
            this.lblTotalNumberofLotSize.Name = "lblTotalNumberofLotSize";
            this.lblTotalNumberofLotSize.Size = new System.Drawing.Size(186, 19);
            this.lblTotalNumberofLotSize.TabIndex = 19;
            this.lblTotalNumberofLotSize.Text = "Total Number of Lot Size";
            // 
            // pnlTotalNumberofLotSize
            // 
            this.pnlTotalNumberofLotSize.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlTotalNumberofLotSize.Controls.Add(this.txtTotalNumberofLotSize);
            this.pnlTotalNumberofLotSize.Location = new System.Drawing.Point(719, 21);
            this.pnlTotalNumberofLotSize.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pnlTotalNumberofLotSize.Name = "pnlTotalNumberofLotSize";
            this.pnlTotalNumberofLotSize.Size = new System.Drawing.Size(238, 32);
            this.pnlTotalNumberofLotSize.TabIndex = 10;
            this.pnlTotalNumberofLotSize.TabStop = true;
            // 
            // txtTotalNumberofLotSize
            // 
            this.txtTotalNumberofLotSize.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTotalNumberofLotSize.Font = new System.Drawing.Font("Arial", 12F);
            this.txtTotalNumberofLotSize.Location = new System.Drawing.Point(5, 2);
            this.txtTotalNumberofLotSize.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtTotalNumberofLotSize.Name = "txtTotalNumberofLotSize";
            this.txtTotalNumberofLotSize.Size = new System.Drawing.Size(229, 23);
            this.txtTotalNumberofLotSize.TabIndex = 11;
            // 
            // lblRoundOff
            // 
            this.lblRoundOff.AutoSize = true;
            this.lblRoundOff.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblRoundOff.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblRoundOff.Location = new System.Drawing.Point(459, 66);
            this.lblRoundOff.Name = "lblRoundOff";
            this.lblRoundOff.Size = new System.Drawing.Size(83, 19);
            this.lblRoundOff.TabIndex = 12;
            this.lblRoundOff.Text = "Round Off";
            // 
            // pnlRoundoff
            // 
            this.pnlRoundoff.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlRoundoff.Controls.Add(this.txtroundoff);
            this.pnlRoundoff.Location = new System.Drawing.Point(719, 59);
            this.pnlRoundoff.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pnlRoundoff.Name = "pnlRoundoff";
            this.pnlRoundoff.Size = new System.Drawing.Size(238, 32);
            this.pnlRoundoff.TabIndex = 12;
            this.pnlRoundoff.TabStop = true;
            // 
            // txtroundoff
            // 
            this.txtroundoff.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtroundoff.Font = new System.Drawing.Font("Arial", 12F);
            this.txtroundoff.Location = new System.Drawing.Point(4, 2);
            this.txtroundoff.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtroundoff.Name = "txtroundoff";
            this.txtroundoff.Size = new System.Drawing.Size(229, 23);
            this.txtroundoff.TabIndex = 13;
            // 
            // lblstrikepriceCall
            // 
            this.lblstrikepriceCall.AutoSize = true;
            this.lblstrikepriceCall.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblstrikepriceCall.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblstrikepriceCall.Location = new System.Drawing.Point(459, 105);
            this.lblstrikepriceCall.Name = "lblstrikepriceCall";
            this.lblstrikepriceCall.Size = new System.Drawing.Size(152, 19);
            this.lblstrikepriceCall.TabIndex = 5;
            this.lblstrikepriceCall.Text = "Call Strike Multiplier";
            // 
            // strikepricecallSubList
            // 
            this.strikepricecallSubList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.strikepricecallSubList.Font = new System.Drawing.Font("Arial", 12F);
            this.strikepricecallSubList.FormattingEnabled = true;
            this.strikepricecallSubList.Location = new System.Drawing.Point(719, 98);
            this.strikepricecallSubList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.strikepricecallSubList.Name = "strikepricecallSubList";
            this.strikepricecallSubList.Size = new System.Drawing.Size(239, 31);
            this.strikepricecallSubList.TabIndex = 15;
            // 
            // lblstrikepriceput
            // 
            this.lblstrikepriceput.AutoSize = true;
            this.lblstrikepriceput.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblstrikepriceput.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblstrikepriceput.Location = new System.Drawing.Point(457, 144);
            this.lblstrikepriceput.Name = "lblstrikepriceput";
            this.lblstrikepriceput.Size = new System.Drawing.Size(149, 19);
            this.lblstrikepriceput.TabIndex = 4;
            this.lblstrikepriceput.Text = "Put Strike Multiplier";
            // 
            // strikepriceputSubList
            // 
            this.strikepriceputSubList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.strikepriceputSubList.Font = new System.Drawing.Font("Arial", 12F);
            this.strikepriceputSubList.FormattingEnabled = true;
            this.strikepriceputSubList.Location = new System.Drawing.Point(719, 138);
            this.strikepriceputSubList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.strikepriceputSubList.Name = "strikepriceputSubList";
            this.strikepriceputSubList.Size = new System.Drawing.Size(237, 31);
            this.strikepriceputSubList.TabIndex = 17;
            // 
            // lblStoploss
            // 
            this.lblStoploss.AutoSize = true;
            this.lblStoploss.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblStoploss.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblStoploss.Location = new System.Drawing.Point(461, 180);
            this.lblStoploss.Name = "lblStoploss";
            this.lblStoploss.Size = new System.Drawing.Size(70, 19);
            this.lblStoploss.TabIndex = 25;
            this.lblStoploss.Text = "Stoploss";
            // 
            // pnlStoploss
            // 
            this.pnlStoploss.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlStoploss.Controls.Add(this.txtStoploss);
            this.pnlStoploss.Location = new System.Drawing.Point(719, 175);
            this.pnlStoploss.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pnlStoploss.Name = "pnlStoploss";
            this.pnlStoploss.Size = new System.Drawing.Size(238, 32);
            this.pnlStoploss.TabIndex = 18;
            this.pnlStoploss.TabStop = true;
            // 
            // txtStoploss
            // 
            this.txtStoploss.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtStoploss.Font = new System.Drawing.Font("Arial", 12F);
            this.txtStoploss.Location = new System.Drawing.Point(4, 2);
            this.txtStoploss.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtStoploss.Name = "txtStoploss";
            this.txtStoploss.Size = new System.Drawing.Size(229, 23);
            this.txtStoploss.TabIndex = 19;
            // 
            // pnlMarginLimit
            // 
            this.pnlMarginLimit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlMarginLimit.Controls.Add(this.txtMarginLimit);
            this.pnlMarginLimit.Location = new System.Drawing.Point(719, 174);
            this.pnlMarginLimit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pnlMarginLimit.Name = "pnlMarginLimit";
            this.pnlMarginLimit.Size = new System.Drawing.Size(238, 32);
            this.pnlMarginLimit.TabIndex = 13;
            this.pnlMarginLimit.TabStop = true;
            this.pnlMarginLimit.Visible = false;
            // 
            // txtMarginLimit
            // 
            this.txtMarginLimit.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtMarginLimit.Font = new System.Drawing.Font("Arial", 12F);
            this.txtMarginLimit.Location = new System.Drawing.Point(5, 2);
            this.txtMarginLimit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtMarginLimit.Name = "txtMarginLimit";
            this.txtMarginLimit.Size = new System.Drawing.Size(229, 23);
            this.txtMarginLimit.TabIndex = 11;
            // 
            // btnClearAll
            // 
            this.btnClearAll.Font = new System.Drawing.Font("Arial", 10F);
            this.btnClearAll.Location = new System.Drawing.Point(967, 96);
            this.btnClearAll.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnClearAll.Name = "btnClearAll";
            this.btnClearAll.Size = new System.Drawing.Size(127, 31);
            this.btnClearAll.TabIndex = 20;
            this.btnClearAll.Text = "Clear All";
            this.btnClearAll.UseVisualStyleBackColor = true;
            this.btnClearAll.Click += new System.EventHandler(this.btnClearAll_Click);
            // 
            // lblMargin
            // 
            this.lblMargin.AutoSize = true;
            this.lblMargin.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblMargin.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblMargin.Location = new System.Drawing.Point(460, 180);
            this.lblMargin.Name = "lblMargin";
            this.lblMargin.Size = new System.Drawing.Size(117, 19);
            this.lblMargin.TabIndex = 20;
            this.lblMargin.Text = "Margin Per Lot";
            this.lblMargin.Visible = false;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Font = new System.Drawing.Font("Arial", 10F);
            this.btnUpdate.Location = new System.Drawing.Point(967, 136);
            this.btnUpdate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(127, 31);
            this.btnUpdate.TabIndex = 21;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("Arial", 10F);
            this.btnAdd.Location = new System.Drawing.Point(967, 176);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(127, 31);
            this.btnAdd.TabIndex = 22;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // strikepriceputList
            // 
            this.strikepriceputList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.strikepriceputList.Enabled = false;
            this.strikepriceputList.Font = new System.Drawing.Font("Arial", 12F);
            this.strikepriceputList.FormattingEnabled = true;
            this.strikepriceputList.Location = new System.Drawing.Point(719, 137);
            this.strikepriceputList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.strikepriceputList.Name = "strikepriceputList";
            this.strikepriceputList.Size = new System.Drawing.Size(97, 31);
            this.strikepriceputList.TabIndex = 16;
            this.strikepriceputList.Visible = false;
            this.strikepriceputList.SelectedValueChanged += new System.EventHandler(this.strikepriceputList_SelectedValueChanged);
            // 
            // strikepriceCallList
            // 
            this.strikepriceCallList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.strikepriceCallList.Enabled = false;
            this.strikepriceCallList.Font = new System.Drawing.Font("Arial", 12F);
            this.strikepriceCallList.FormattingEnabled = true;
            this.strikepriceCallList.Location = new System.Drawing.Point(719, 98);
            this.strikepriceCallList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.strikepriceCallList.Name = "strikepriceCallList";
            this.strikepriceCallList.Size = new System.Drawing.Size(99, 31);
            this.strikepriceCallList.TabIndex = 14;
            this.strikepriceCallList.Visible = false;
            this.strikepriceCallList.SelectedValueChanged += new System.EventHandler(this.strikepriceCallList_SelectedValueChanged);
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("Arial", 10.5F);
            this.btnCancel.Location = new System.Drawing.Point(981, 682);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(134, 40);
            this.btnCancel.TabIndex = 26;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // BtnImport
            // 
            this.BtnImport.Font = new System.Drawing.Font("Arial", 10.5F);
            this.BtnImport.Location = new System.Drawing.Point(446, 682);
            this.BtnImport.Name = "BtnImport";
            this.BtnImport.Size = new System.Drawing.Size(134, 40);
            this.BtnImport.TabIndex = 25;
            this.BtnImport.Text = "Import";
            this.BtnImport.UseVisualStyleBackColor = true;
            this.BtnImport.Click += new System.EventHandler(this.BtnImport_Click);
            // 
            // btnExport
            // 
            this.btnExport.Font = new System.Drawing.Font("Arial", 10.5F);
            this.btnExport.Location = new System.Drawing.Point(290, 682);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(134, 40);
            this.btnExport.TabIndex = 24;
            this.btnExport.Text = "Export";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // SaveFDExportFile
            // 
            this.SaveFDExportFile.FileName = "SymbolSettingDetails";
            this.SaveFDExportFile.Title = "Export File";
            // 
            // OpenFDImportFile
            // 
            this.OpenFDImportFile.Title = "Import File";
            // 
            // SymbolSettingDet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1141, 729);
            this.Controls.Add(this.GPSymbolDet);
            this.Controls.Add(this.btnDeleteselectedrow);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.BtnImport);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.btnCancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.Name = "SymbolSettingDet";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Symbol Setting";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SymbolSettingDet_FormClosing);
            this.GPSymbolDet.ResumeLayout(false);
            this.GPAccount.ResumeLayout(false);
            this.GPAccount.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.GPAddDet.ResumeLayout(false);
            this.GPAddDet.PerformLayout();
            this.pnlLotSize.ResumeLayout(false);
            this.pnlLotSize.PerformLayout();
            this.pnlTotalNumberofLotSize.ResumeLayout(false);
            this.pnlTotalNumberofLotSize.PerformLayout();
            this.pnlRoundoff.ResumeLayout(false);
            this.pnlRoundoff.PerformLayout();
            this.pnlStoploss.ResumeLayout(false);
            this.pnlStoploss.PerformLayout();
            this.pnlMarginLimit.ResumeLayout(false);
            this.pnlMarginLimit.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        //private System.Windows.Forms.Button btnAddRow;
        private System.Windows.Forms.ComboBox AccountList;
        private System.Windows.Forms.ComboBox ExchangeList;
        private System.Windows.Forms.ComboBox SymbolList;
        private System.Windows.Forms.ComboBox ExpirydtList;
        private System.Windows.Forms.ComboBox ExpiryPeriodList;
        private System.Windows.Forms.ComboBox OrderTypeList;
        private System.Windows.Forms.TextBox txtMarketLotSize;
        private System.Windows.Forms.TextBox txtroundoff;
        private System.Windows.Forms.ComboBox strikepriceCallList;
        private System.Windows.Forms.ComboBox strikepriceputSubList;
        private System.Windows.Forms.ComboBox strikepriceputList;
        private System.Windows.Forms.ComboBox strikepricecallSubList;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnDeleteselectedrow;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.Button btnCancel;


        private System.Windows.Forms.GroupBox GPSymbolDet;
        private System.Windows.Forms.GroupBox GPAddDet;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.GroupBox GPAccount;
        private System.Windows.Forms.Panel pnlRoundoff;
        private System.Windows.Forms.Panel pnlLotSize;
        private System.Windows.Forms.Label lblRoundOff;
        private System.Windows.Forms.Label lblOrdertype;
        private System.Windows.Forms.Label lblstrikepriceput;
        private System.Windows.Forms.Label lblstrikepriceCall;
        private System.Windows.Forms.Label lblMarketLotSize;
        private System.Windows.Forms.Label lblExpiryPeriod;
        private System.Windows.Forms.Label lblAccount;
        private System.Windows.Forms.Label lblExpiryDate;
        private System.Windows.Forms.Label lblInstruments;
        private System.Windows.Forms.Label lblSymbol;
        private System.Windows.Forms.Label lblTotalNumberofLotSize;
        private System.Windows.Forms.Panel pnlTotalNumberofLotSize;
        private System.Windows.Forms.TextBox txtTotalNumberofLotSize;
        private System.Windows.Forms.Label lblMargin;
        private System.Windows.Forms.Button btnClearAll;
        private System.Windows.Forms.Label lblStoploss;
        private System.Windows.Forms.Panel pnlMarginLimit;
        private System.Windows.Forms.TextBox txtMarginLimit;
        private System.Windows.Forms.Panel pnlStoploss;
        private System.Windows.Forms.TextBox txtStoploss;
        private System.Windows.Forms.Button BtnImport;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.SaveFileDialog SaveFDExportFile;
        private System.Windows.Forms.OpenFileDialog OpenFDImportFile;
        private System.Windows.Forms.DataGridViewTextBoxColumn SymbolName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Exchange;
        private System.Windows.Forms.DataGridViewTextBoxColumn ExpiryDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ExpiryPeriod;
        private System.Windows.Forms.DataGridViewTextBoxColumn OrderType;
        private System.Windows.Forms.DataGridViewTextBoxColumn MarketLotSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalNumberofLotSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn RoundOff;
        private System.Windows.Forms.DataGridViewTextBoxColumn CallStrikePrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn PutStrikePrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn Stoploss;
    }
}