﻿using IRDSAlgoOMS;
using RealStockA1Strategy;
using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace RealStockA1Strategy
{
    public partial class TradingSetting : Form
    {
        [field: NonSerialized()]
        AlgoOMS AlgoOMSObj;
        //sanika::25-sep-2020::added for resume 
        [field: NonSerialized()]
        RealStockA1Strategy.ReadSettings readSettings;
        //sanika::25-sep-2020::added for resume 
        [field: NonSerialized()]
        Logger logger;
        string path = System.IO.Directory.GetCurrentDirectory();

        string message = "";
        string title = "RealStockA1 Strategy";
        MessageBoxButtons buttons;
        DialogResult dialog;
        INIFile iniObj = null;
        string userID = null;
        static string ConfigFilePath = System.AppDomain.CurrentDomain.BaseDirectory + "Configuration";
        string accountNumber = "";
        string StartEntryTime = "";
        //string EndEntryTime = "";
        string EndExitTime = "";
        //string StopLossPercent = "";
        string BreakEvenStop = "";
        string StoplossRejectTimeInterval = "";
        //IRDS::Pratiksha::18-08-2021::For 2 new fields Slicing per lot and cancel order time interval
        string SlicingLotSize = "";
        string OrderTimeWaitInterval = "";
        string AdditionalAccount = "";
        string m_FrmTitle = "";
        public TradingSetting(Logger logger, AlgoOMS obj, string userid, string FrmTitle)
        {

            //Pratiksha::12-08-2021::For having proper name on meesagebox
            Assembly myAssembly = Assembly.GetExecutingAssembly();
            Stream IconStream = null;
            IconStream = myAssembly.GetManifestResourceStream("RealStockA1.Resources." + FrmTitle + ".ico");
            this.Icon = new System.Drawing.Icon(IconStream);
            InitializeComponent();
            try
            {
                m_FrmTitle = FrmTitle;
                this.AlgoOMSObj = obj;
                this.logger = logger;
                userID = userid;
                string[] files = Directory.GetFiles(ConfigFilePath, "StrategySetting_*");
                foreach (string fileName in files)
                {
                    string file = Path.GetFileNameWithoutExtension(fileName);
                    AccountDetList.Items.Add(file.Split('_')[1]);
                    AdditionalList.Items.Add(file.Split('_')[1]);
                }

                //IRDSPM::Pratiksha::28-05-2021::for selected account need to be default selected
                if (AccountDetList.Items.Contains(userid))
                {
                    AccountDetList.SelectedItem = userid;
                    AdditionalList.SelectedItem = userid;
                }
                else
                {
                    message = userid + ".ini file does not exist!!";
                    buttons = MessageBoxButtons.OK;
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                }
                AccountDetList.Enabled = false;
                txtAccountNumber.Enabled = false;
                if (readSettings == null)
                {
                    readSettings = new RealStockA1Strategy.ReadSettings(logger);
                }
                readSettings = new RealStockA1Strategy.ReadSettings(this.logger, this.AlgoOMSObj);
            }
            catch (Exception ex)
            {
                WriteUniquelogs("TradingSettingLogs", "InitializeComponent : Exception Error Message = " + ex.Message, MessageType.Informational);
            }
        }
        private void FetchDetailsFromINI(string Path)
        {
            try
            {
                txtSlicingLotSize.Text = "1";
                string filePath = path + @"\Configuration\StrategySetting_" + Path + ".ini";
                if (File.Exists(filePath))
                {
                    if (readSettings == null)
                    {
                        readSettings = new RealStockA1Strategy.ReadSettings(logger);
                    }
                    readSettings.ReadINIFile(filePath);
                    accountNumber = readSettings.m_AccountNumber.ToString();
                    StartEntryTime = readSettings.m_StartEntryTime.ToString();
                    // EndEntryTime = readSettings.m_EndEntryTime.ToString();
                    EndExitTime = readSettings.m_EndExitTime.ToString();
                    // StopLossPercent = readSettings.m_StopLossPercent.ToString();
                    BreakEvenStop = readSettings.m_BreakEvenStop.ToString();
                    StoplossRejectTimeInterval = readSettings.m_StoplossRejectTimeInterval.ToString();
                    //IRDS::Pratiksha::18-08-2021::For 2 new fields Slicing per lot and cancel order time interval
                    SlicingLotSize = readSettings.m_SlicingLotSize.ToString();
                    OrderTimeWaitInterval = readSettings.m_OrderTimeWaitInterval.ToString();
                    AdditionalAccount = readSettings.m_AdditionalAccount.ToString();

                    txtAccountNumber.Text = accountNumber;
                    txtStartEntryTime.Value = Convert.ToDateTime(StartEntryTime);
                    //txtEndEntryTime.Value = Convert.ToDateTime(EndEntryTime);
                    txtEndExitTime.Value = Convert.ToDateTime(EndExitTime);
                    //txtStoplossPercent.Text = StopLossPercent;

                    if (BreakEvenStop.ToLower() == "true")
                    {
                        TrueBreakEvenStop.Checked = true;
                    }
                    else if (BreakEvenStop.ToString().ToLower() == "false")
                    {
                        FalseBreakEvenStop.Checked = true;
                    }
                    txtStoplossRejectTimeInterval.Text = StoplossRejectTimeInterval;
                    //IRDS::Pratiksha::18-08-2021::For 2 new fields Slicing per lot and cancel order time interval
                    txtSlicingLotSize.Text = SlicingLotSize;
                    txtOrderTimeWaitInterval.Text = OrderTimeWaitInterval;
                    txtAdditionalAccount.Text = AdditionalAccount;

                    //IRDSPM::Pratiksha::14-05-2021::For aditional account details
                    WriteUniquelogs("TradingSettingLogs", "FetchDetailsFromINI : Reading details from INI file.", MessageType.Informational);
                }
                else
                {
                    Clearfields();
                    WriteUniquelogs("TradingSettingLogs", "FetchDetailsFromINI : INI file not exist.", MessageType.Informational);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("TradingSettingLogs", "FetchDetailsFromINI : Exception Error Message = " + ex.Message, MessageType.Informational);
            }
        }
        //IRDS::Pratiksha::18-08-2021::For 2 new fields Slicing per lot and cancel order time interval
        private void Validations(out Match StoplossRejectTimeInterval1, out Match SlicingNumber1, out Match CancelOrderTimeInterval1)
        {
            string Decpattern = "^[0-9]+(.|,)?[0-9]*?$";
            Regex r1 = new Regex(Decpattern);
            StoplossRejectTimeInterval1 = r1.Match(txtStoplossRejectTimeInterval.Text.ToString());
            SlicingNumber1 = r1.Match(txtSlicingLotSize.Text.ToString());
            CancelOrderTimeInterval1 = r1.Match(txtOrderTimeWaitInterval.Text.ToString());
        }
        private void btnApply_Click(object sender, EventArgs e)
        {
            try
            {
                //IRDS::Pratiksha::18-08-2021::For 2 new fields Slicing per lot and cancel order time interval
                if (txtAccountNumber.Text != "" && txtStartEntryTime.Text != "" && txtEndExitTime.Text != "" && txtStoplossRejectTimeInterval.Text != "" && txtSlicingLotSize.Text != "" && txtOrderTimeWaitInterval.Text != "")//&& txtStoplossPercent.Text != ""&& txtEndEntryTime.Text != ""
                {
                    //IRDS::Pratiksha::18-08-2021::For 2 new fields Slicing per lot and cancel order time interval
                    Match StoplossRejectTimeInterval1, SlicingNumber1, CancelOrderTimeInterval1;
                    Validations(out StoplossRejectTimeInterval1, out SlicingNumber1, out CancelOrderTimeInterval1);
                    if (StoplossRejectTimeInterval1.Success && SlicingNumber1.Success && CancelOrderTimeInterval1.Success)
                    {
                        message = "Do you want to save the changes for " + txtAdditionalAccount.Text.ToString() + "?";
                        buttons = MessageBoxButtons.YesNo;
                        dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                        if (dialog == DialogResult.Yes)
                        {
                            SavedInIniFile(); // (totalAccount);

                            WriteUniquelogs("TradingSettingLogs", "btnApply_Click :  Details saved in ini file.", MessageType.Informational);
                        }
                        else
                        {
                            this.Close();
                            WriteUniquelogs("TradingSettingLogs", "btnApply_Click : You have clicked No, no data updated in trade setting.ini file.", MessageType.Informational);
                        }
                    }
                    else
                    {
                        string Logmessage = "";
                        string parameter = "";
                        if (!StoplossRejectTimeInterval1.Success) { parameter += "\nStoploss Reject Time Interval"; Logmessage += "Stoploss Reject Time Interval,"; }
                        //IRDS::Pratiksha::18-08-2021::For 2 new fields Slicing per lot and cancel order time interval
                        if (!SlicingNumber1.Success) { parameter += "\nSlicing Lot Size"; Logmessage += "Slicing Lot Size,"; }
                        if (!CancelOrderTimeInterval1.Success) { parameter += "\nOrder Time Wait Interval"; Logmessage += "Order Time Wait Interval"; }
                        message = "Please add valid values for " + parameter + ".";
                        buttons = MessageBoxButtons.OK;
                        dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                        WriteUniquelogs("SymbolSettingLogs", "btnApply_Click : " + Logmessage, MessageType.Informational);
                    }
                }
                else
                {
                    string parameter = "";
                    if (txtAccountNumber.Text == "") { parameter += "\n Account Number"; }
                    if (txtStartEntryTime.Text == "") { parameter += "\nStart Entry Time"; }
                    //if (txtEndEntryTime.Text == "") { parameter += "\nEnd Entry Time"; }
                    if (txtEndExitTime.Text == "") { parameter += "\nEnd Exit Time"; }
                    //if (txtStoplossPercent.Text == "") { parameter += "\nStoploss Percent"; }
                    if (txtStoplossRejectTimeInterval.Text == "") { parameter += "\nStoploss Reject Time Interval"; }
                    //IRDS::Pratiksha::18-08-2021::For 2 new fields Slicing per lot and cancel order time interval
                    if (txtSlicingLotSize.Text == "") { parameter += "\nSlicing Number"; }
                    if (txtOrderTimeWaitInterval.Text == "") { parameter += "\nOrder Time Wait Interval"; }
                    message = "Please enter proper values for following field: " + parameter;
                    buttons = MessageBoxButtons.OK;
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                    WriteUniquelogs("TradingSettingLogs", "btnApply_Click : Please enter proper values.", MessageType.Informational);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("TradingSettingLogs", "btnApply_Click : Exception Error Message = " + ex.Message, MessageType.Informational);
            }
        }

        private void SavedInIniFile(bool isDisplay = false)//(string[] totalAccount)
        {
            //for (int ii = 0; ii < totalAccount.Length; ii++)
            //{
            string filePath = ConfigFilePath + "\\StrategySetting_" + txtAccountNumber.Text + ".ini";
            iniObj = new INIFile(filePath);
            string[] key = new string[9];
            string[] value = new string[9];
            key[0] = "AccountNumber";
            key[1] = "StartEntryTime";
            //key[2] = "EndEntryTime";
            key[2] = "EndExitTime";
            key[3] = "BreakEvenStop";
            key[4] = "StoplossRejectTimeInterval";
            key[5] = "AdditionalAccount";
            //IRDS::Pratiksha::18-08-2021::For 2 new fields Slicing per lot and cancel order time interval
            key[6] = "SlicingLotSize";
            key[7] = "OrderTimeWaitInterval";

            value[0] = txtAccountNumber.Text;
            value[1] = txtStartEntryTime.Text;
            //value[2] = txtEndEntryTime.Text;
            value[2] = txtEndExitTime.Text;
            if (TrueBreakEvenStop.Checked == true)
            {
                value[3] = "true";
            }
            else if (FalseBreakEvenStop.Checked == true)
            {
                value[3] = "false";
            }
            value[4] = txtStoplossRejectTimeInterval.Text;
            value[5] = txtAdditionalAccount.Text;
            //IRDS::Pratiksha::18-08-2021::For 2 new fields Slicing per lot and cancel order time interval
            value[6] = txtSlicingLotSize.Text;
            value[7] = txtOrderTimeWaitInterval.Text;
            for (int i = 0; i < 8; i++)
            {
                iniObj.clearTestingSymbol("SDATA", key[i], value[i], filePath);
                WriteUniquelogs("TradingSettingLogs", "btnApply_Click :  Details saved " + key[i] + " = " + value[i], MessageType.Informational);
            }
            //}
            if (isDisplay == false)
            {
                message = "Saved changes successfully!!!";
                buttons = MessageBoxButtons.OK;
                dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                if (dialog == DialogResult.OK)
                {
                    this.Close();
                }
            }
        }

        public void WriteUniquelogs(string symbol, string message, MessageType msgType)
        {
            try
            {
                SymbolLogs log = new SymbolLogs();
                log.createFile(symbol);
                log.LogMessage(message, MessageType.Informational);
                log = null;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : " + e.Message);
                Trace.WriteLine("Exception : " + e.Message);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                bool currentBreakEvenStop = false;
                if (TrueBreakEvenStop.Checked == true)
                {
                    currentBreakEvenStop = true;
                }
                else if (FalseBreakEvenStop.Checked == true)
                {
                    currentBreakEvenStop = false;
                }
                if ((StartEntryTime != txtStartEntryTime.Text) || //(EndEntryTime != txtEndEntryTime.Text) ||
                    (EndExitTime != txtEndExitTime.Text) || //(StopLossPercent != txtStoplossPercent.Text) ||
                 (BreakEvenStop != currentBreakEvenStop.ToString()) ||
                 //IRDS::Pratiksha::18-08-2021::For 2 new fields Slicing per lot and cancel order time interval
                 (SlicingLotSize != txtSlicingLotSize.Text) || (OrderTimeWaitInterval != txtOrderTimeWaitInterval.Text) ||
                (StoplossRejectTimeInterval != txtStoplossRejectTimeInterval.Text))// || (readSettings.m_Stoploss.ToString() != txtstoploss.Text))
                {
                    //IRDS::Pratiksha::18-08-2021::For 2 new fields Slicing per lot and cancel order time interval
                    Match StoplossRejectTimeInterval1, SlicingNumber1, CancelOrderTimeInterval1;
                    Validations(out StoplossRejectTimeInterval1, out SlicingNumber1, out CancelOrderTimeInterval1);
                    if (StoplossRejectTimeInterval1.Success && SlicingNumber1.Success && CancelOrderTimeInterval1.Success)
                    {
                        message = "You have some unsaved changes, do you want to save those changes?";
                        buttons = MessageBoxButtons.YesNo;
                        dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                        if (DialogResult.Yes == dialog)
                        {
                            SavedInIniFile();
                        }
                        else
                        {
                            this.Close();
                        }
                    }
                    //Pratiksha::17-08-2021::For regex changes
                    else
                    {
                        string Logmessage = "";
                        string parameter = "";
                        if (!StoplossRejectTimeInterval1.Success) { parameter += "\nStoploss Reject Time Interval"; Logmessage += "Stoploss Reject Time Interval,"; }
                        //IRDS::Pratiksha::18-08-2021::For 2 new fields Slicing per lot and cancel order time interval
                        if (!SlicingNumber1.Success) { parameter += "\nSlicing Lot Size"; Logmessage += "Slicing Lot Size,"; }
                        if (!CancelOrderTimeInterval1.Success) { parameter += "\nOrder Time Wait Interval"; Logmessage += "Order Time Wait Interval"; }
                        message = "Please add valid values for " + parameter + ".";
                        buttons = MessageBoxButtons.OK;
                        dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                        WriteUniquelogs("SymbolSettingLogs", "btnApply_Click : " + Logmessage, MessageType.Informational);
                    }
                }
                else
                {
                    this.Close();
                }
                WriteUniquelogs("TradingSettingLogs", "btnCancel_Click : Cancel click.", MessageType.Informational);
            }
            catch (Exception ex)
            {
                WriteUniquelogs("TradingSettingLogs", "btnCancel_Click : Exception Error Message = " + ex.Message, MessageType.Informational);
            }
        }

        private void AccountDetList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                FetchDetailsFromINI(AccountDetList.SelectedItem.ToString());
                if (AccountDetList.SelectedItem.ToString() != "Choose Account")
                {
                    groupBox1.Enabled = true;
                }
                else
                {
                    groupBox1.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("TradingSettingLogs", "AccountDetList_SelectedIndexChanged : Exception Error Message = " + ex.Message, MessageType.Informational);
            }
        }
        private void Clearfields()
        {
            try
            {
                txtAccountNumber.Text = null;
                txtStartEntryTime.Text = null;
                //txtEndEntryTime.Text = null;
                txtEndExitTime.Text = null;
                //txtStoplossPercent.Text = null;
                txtAdditionalAccount.Text = null;
                txtStoplossRejectTimeInterval.Text = null;
                //IRDS::Pratiksha::18-08-2021::For 2 new fields Slicing per lot and cancel order time interval
                txtSlicingLotSize.Text = null;
                txtOrderTimeWaitInterval.Text = null;
                WriteUniquelogs("TradingSettingLogs", "Clearfields : All fields cleared.", MessageType.Informational);
            }
            catch (Exception ex)
            {
                WriteUniquelogs("TradingSettingLogs", "Clearfields : Exception Error Message = " + ex.Message, MessageType.Informational);
            }
        }

        private void btnOpenSymbolSetting_Click(object sender, EventArgs e)
        {
            try
            {
                SymbolSettingDet symbolObj = new SymbolSettingDet(this.AlgoOMSObj, txtAccountNumber.Text, null, m_FrmTitle);
                symbolObj.ShowDialog();
            }
            catch (Exception ex)
            {
                WriteUniquelogs("TradingSettingLogs", "btnOpenSymbolSetting_Click : Exception Error Message = " + ex.Message, MessageType.Informational);
            }
        }


        private void AdditionalList_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                string[] totalAccount = null;
                string selectedvalue = AdditionalList.SelectedItem.ToString();
                if (txtAdditionalAccount.Text.Length > 0)
                {
                    totalAccount = txtAdditionalAccount.Text.Split(',');
                }
                if (txtAdditionalAccount.Text.Length > 0)
                {
                    int pos = Array.IndexOf(totalAccount, selectedvalue);
                    if (pos > -1)
                    {
                    }
                    else
                    {
                        txtAdditionalAccount.Text = txtAdditionalAccount.Text + "," + selectedvalue;
                    }
                }
                else
                {
                    txtAdditionalAccount.Text = AdditionalList.SelectedItem.ToString();
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("TradingSettingLogs", "AdditionalList_SelectedValueChanged : Exception Error Message = " + ex.Message, MessageType.Informational);
            }
        }

        private void BtnImport_Click(object sender, EventArgs e)
        {
            try
            {
                string pathname = "";
                string strPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                string filePath = ConfigFilePath + "\\StrategySetting_" + txtAccountNumber.Text + ".ini";
                OpenFDImportFile.InitialDirectory = @strPath; // "C:\";
                OpenFDImportFile.DefaultExt = "txt";
                OpenFDImportFile.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                if (OpenFDImportFile.ShowDialog() == DialogResult.OK)
                {
                    INIFile iniObj = new INIFile(pathname);
                    pathname = OpenFDImportFile.FileName;
                    if (readSettings == null)
                    {
                        readSettings = new RealStockA1Strategy.ReadSettings(logger);
                    }
                    readSettings.ReadImportedFile(pathname);

                    try
                    {
                        StartEntryTime = readSettings.m_StartEntryTime.ToString();
                        //EndEntryTime = readSettings.m_EndEntryTime.ToString();
                        EndExitTime = readSettings.m_EndExitTime.ToString();
                        BreakEvenStop = readSettings.m_BreakEvenStop.ToString();
                        StoplossRejectTimeInterval = readSettings.m_StoplossRejectTimeInterval.ToString();
                        //IRDS::Pratiksha::18-08-2021::For 2 new fields Slicing per lot and cancel order time interval
                        SlicingLotSize = readSettings.m_SlicingLotSize.ToString();
                        OrderTimeWaitInterval = readSettings.m_OrderTimeWaitInterval.ToString();
                        txtAccountNumber.Text = accountNumber;
                        txtStartEntryTime.Value = Convert.ToDateTime(StartEntryTime);
                        //txtEndEntryTime.Value = Convert.ToDateTime(EndEntryTime);
                        txtEndExitTime.Value = Convert.ToDateTime(EndExitTime);

                        if (BreakEvenStop.ToLower() == "true")
                        {
                            TrueBreakEvenStop.Checked = true;
                        }
                        else if (BreakEvenStop.ToString().ToLower() == "false")
                        {
                            FalseBreakEvenStop.Checked = true;
                        }
                        txtStoplossRejectTimeInterval.Text = StoplossRejectTimeInterval;
                        //IRDS::Pratiksha::18-08-2021::For 2 new fields Slicing per lot and cancel order time interval
                        txtSlicingLotSize.Text = SlicingLotSize;
                        txtOrderTimeWaitInterval.Text = OrderTimeWaitInterval;
                        txtAdditionalAccount.Text = AdditionalAccount;
                        WriteUniquelogs("TradingSettingLogs", "FetchDetailsFromINI : Reading details from INI file.", MessageType.Informational);
                    }
                    catch (Exception ex)
                    {
                        WriteUniquelogs("TradingSettingLogs", "BtnImport_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
                    }

                    string[] key = new string[9];
                    string[] value = new string[9];
                    key[0] = "AccountNumber";
                    key[1] = "StartEntryTime";
                    //key[2] = "EndEntryTime";
                    key[2] = "EndExitTime";
                    key[3] = "BreakEvenStop";
                    key[4] = "StoplossRejectTimeInterval";
                    key[5] = "AdditionalAccount";
                    //IRDS::Pratiksha::18-08-2021::For 2 new fields Slicing per lot and cancel order time interval
                    key[6] = "SlicingLotSize";
                    key[7] = "OrderTimeWaitInterval";
                    value[0] = txtAccountNumber.Text;
                    value[1] = txtStartEntryTime.Text;
                    //value[2] = txtEndEntryTime.Text;
                    value[2] = txtEndExitTime.Text;
                    if (TrueBreakEvenStop.Checked == true)
                    {
                        value[3] = "true";
                    }
                    else if (FalseBreakEvenStop.Checked == true)
                    {
                        value[3] = "false";
                    }
                    value[4] = txtStoplossRejectTimeInterval.Text;
                    value[5] = txtAdditionalAccount.Text;
                    value[6] = txtSlicingLotSize.Text;
                    value[7] = txtOrderTimeWaitInterval.Text;
                    for (int i = 0; i < 8; i++)
                    {
                        iniObj.clearTestingSymbol("SDATA", key[i], value[i], filePath);
                        WriteUniquelogs("TradingSettingLogs", "BtnImport_Click :  Details saved " + key[i] + " = " + value[i], MessageType.Informational);
                    }
                    message = "File imported successfully!!!";
                    buttons = MessageBoxButtons.OK;
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                    WriteUniquelogs("TradingSettingLogs", "BtnImport_Click : " + message, MessageType.Informational);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("TradingSettingLogs", "BtnImport_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                SavedInIniFile(true);
                FetchDetailsFromINI(AccountDetList.SelectedItem.ToString());
                //IRDS::Pratiksha::18-08-2021::For 2 new fields Slicing per lot and cancel order time interval
                Match StoplossRejectTimeInterval1, SlicingNumber1, CancelOrderTimeInterval1;
                Validations(out StoplossRejectTimeInterval1, out SlicingNumber1, out CancelOrderTimeInterval1);
                if (StoplossRejectTimeInterval1.Success && SlicingNumber1.Success && CancelOrderTimeInterval1.Success)
                {
                    string pathname = "";
                    string strPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                    SaveFDExportFile.InitialDirectory = @strPath;
                    SaveFDExportFile.DefaultExt = "txt";
                    SaveFDExportFile.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                    SaveFDExportFile.FilterIndex = 2;
                    if (SaveFDExportFile.ShowDialog() == DialogResult.OK)
                    {

                        pathname = SaveFDExportFile.FileName;
                        INIFile iniObj = new INIFile(pathname);
                        if (readSettings == null)
                        {
                            readSettings = new RealStockA1Strategy.ReadSettings(logger);
                        }
                        string[] key = new string[7];
                        string[] value = new string[7];
                        key[0] = "StartEntryTime";
                        //key[1] = "EndEntryTime";
                        key[1] = "EndExitTime";
                        key[2] = "BreakEvenStop";
                        key[3] = "StoplossRejectTimeInterval";
                        key[4] = "SlicingLotSize";
                        key[5] = "OrderTimeWaitInterval";
                        value[0] = txtStartEntryTime.Text;
                        //value[1] = txtEndEntryTime.Text;
                        value[1] = txtEndExitTime.Text;
                        if (TrueBreakEvenStop.Checked == true)
                        {
                            value[2] = "true";
                        }
                        else if (FalseBreakEvenStop.Checked == true)
                        {
                            value[2] = "false";
                        }
                        value[3] = txtStoplossRejectTimeInterval.Text;
                        value[4] = txtSlicingLotSize.Text;
                        value[5] = txtOrderTimeWaitInterval.Text;
                        for (int i = 0; i < 6; i++)
                        {
                            iniObj.clearTestingSymbol("SDATA", key[i], value[i], pathname);
                            WriteUniquelogs("TradingSettingLogs", "btnExport_Click :  Details saved " + key[i] + " = " + value[i], MessageType.Informational);
                        }
                        message = "File exported successfully!!!";
                        buttons = MessageBoxButtons.OK;
                        dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                        WriteUniquelogs("TradingSettingLogs", "btnExport_Click : " + message, MessageType.Informational);
                    }
                }//Pratiksha::17-08-2021::For regex changes
                else
                {
                    string Logmessage = "";
                    string parameter = "";
                    if (!StoplossRejectTimeInterval1.Success) { parameter += "\nStoploss Reject Time Interval,"; Logmessage += "Stoploss Reject Time Interval,"; }
                    //IRDS::Pratiksha::18-08-2021::For 2 new fields Slicing per lot and cancel order time interval
                    if (!SlicingNumber1.Success) { parameter += "\nSlicing Lot Size"; Logmessage += "Slicing Lot Size,"; }
                    if (!CancelOrderTimeInterval1.Success) { parameter += "\nOrder Time Wait Interval"; Logmessage += "Order Time Wait Interval"; }
                    message = "Please add valid values for " + parameter + ".";
                    buttons = MessageBoxButtons.OK;
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                    WriteUniquelogs("SymbolSettingLogs", "btnApply_Click : " + Logmessage, MessageType.Informational);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("TradingSettingLogs", "btnExport_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }
    }
}
