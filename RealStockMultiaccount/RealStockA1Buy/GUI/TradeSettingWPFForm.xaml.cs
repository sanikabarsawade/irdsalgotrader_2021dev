﻿using IRDSAlgoOMS;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RealStockA1Strategy
{
    public partial class TradeSettingWPFForm : System.Windows.Controls.UserControl
    {
        [field: NonSerialized()]
        AlgoOMS AlgoOMSObj;
        //sanika::25-sep-2020::added for resume 
        [field: NonSerialized()]
        ReadSettings readSettings;
        //sanika::25-sep-2020::added for resume 
        [field: NonSerialized()]
        Logger logger;
        string path = System.IO.Directory.GetCurrentDirectory();

        //GeneralSetting
        string m_StartTimeOfFirstCandle = "";
        string m_PlaceOrderStartTime = "";
        string m_PlaceOrderEndTime = "";
        string m_TimeForExitAllPositions = "";
        int m_BarInterval = 0;
        double m_PipInPaisa = 0;
        int m_IndividualTradeCount = 0;
        double m_TotalCapital = 0;
        double m_RiskPercent = 0;
        int m_ATRPeriod = 0;
        
        //Trade1 setting
        double m_Trade_1_PercentMultiplierForTarget_1 = 0;
        double m_Trade_1_PercentMultiplierForTarget_2 = 0;
        double m_Trade_1_PercentMultiplierForTarget_3 = 0;
        double m_Trade_1_ATRMultiplierForTarget_1 = 0;
        double m_Trade_1_ATRMultiplierForTarget_2 = 0;
        double m_Trade_1_ATRMultiplierForTarget_3 = 0;
        double m_Trade_1_MovingATRStoplossForTarget_1 = 0;
        double m_Trade_1_MovingATRStoplossForTarget_2 = 0;
        double m_Trade_1_ATRStoplossMultiplier = 0;
        double m_Trade_1_MovingPercentStoplossForTarget_1 = 0;
        double m_Trade_1_MovingPercentStoplossForTarget_2 = 0;
        double m_Trade_1_PercentStoplossMultiplier = 0;

        //Trade2 Setting
        double m_Trade_2_PercentMultiplierForTarget_4 = 0;
        double m_Trade_2_PercentMultiplierForTarget_5 = 0;
        double m_Trade_2_PercentMultiplierForTarget_6 = 0;
        double m_Trade_2_ATRMultiplierForTarget_4 = 0;
        double m_Trade_2_ATRMultiplierForTarget_5 = 0;
        double m_Trade_2_ATRMultiplierForTarget_6 = 0;
        double m_Trade_2_MovingATRStoplossForTarget_1 = 0;
        double m_Trade_2_MovingATRStoplossForTarget_2 = 0;
        double m_Trade_2_ATRStoplossMultiplier = 0;
        double m_Trade_2_MovingPercentStoplossForTarget_1 = 0;
        double m_Trade_2_MovingPercentStoplossForTarget_2 = 0;
        double m_Trade_2_PercentStoplossMultiplier = 0;
        double m_ATRMultiplierForQuantity = 0;


        //Commonsetting
        double m_PercentOfHighAndLowCondition = 0;
        int m_Target_1_ExitQuantity = 0;
        int m_Target_2_ExitQuantity = 0;
        bool m_FinetunesFlag = false;
        bool m_ATRFlag = false;
        double m_PricePercentForNewOrder = 0;
        double m_MaxLoss = 0;
        double m_MaxProfit = 0;
        TradeSettingWindowsForm m_TradeObj;
        string message = "";
        string title = "IRDS Algo Trader";
        MessageBoxButtons buttons;
        string m_iniChoose = "";
        DialogResult dialog;
        public TradeSettingWPFForm(Logger logger, AlgoOMS obj, TradeSettingWindowsForm tradeObj, string iniChoose)
        {
            InitializeComponent();
            this.AlgoOMSObj = obj;
            this.logger = logger;
            m_TradeObj = tradeObj;
            readSettings = new ReadSettings(this.logger, this.AlgoOMSObj);
            m_iniChoose = iniChoose;
            readSettings.ReadINIFile(path + @"\Configuration\StrategySetting_KAP70.ini");
            FetchDetailsFromINI();
        }
        private void FetchDetailsFromINI()
        {
            try
            {
                //general setting
              
                //IRDSPM::Pratiksha::12-01-2021::commented
               // m_ATRMultiplierForQuantity = readSettings.m_ATRMultiplierForQuantity;
                textBox1.Text = m_StartTimeOfFirstCandle;
                textBox2.Text = m_PlaceOrderStartTime;
                textBox3.Text = m_PlaceOrderEndTime;
                textBox4.Text = m_TimeForExitAllPositions;
                textBox5.Text = m_BarInterval.ToString();
                textBox6.Text = m_PipInPaisa.ToString();
                textBox7.Text = m_TotalCapital.ToString();
                textBox8.Text = m_RiskPercent.ToString();
                textBox9.Text = m_ATRPeriod.ToString();
                //IRDSPM::Pratiksha::12-01-2021::commented
                //textBox10.Text = m_ATRMultiplierForQuantity.ToString();

                //Trade 1 setting
               
                //sanika::6-Jan-2021::Changed variable
               

                textBox21.Text = m_Trade_1_PercentMultiplierForTarget_1.ToString();
                textBox22.Text = m_Trade_1_PercentMultiplierForTarget_2.ToString();
                textBox23.Text = m_Trade_1_PercentMultiplierForTarget_3.ToString();
                textBox24.Text = m_Trade_1_ATRMultiplierForTarget_1.ToString();
                textBox25.Text = m_Trade_1_ATRMultiplierForTarget_2.ToString();
                textBox26.Text = m_Trade_1_ATRMultiplierForTarget_3.ToString();
                textBox27.Text = m_Trade_1_PercentStoplossMultiplier.ToString();
                textBox28.Text = m_Trade_1_MovingPercentStoplossForTarget_1.ToString();
                textBox29.Text = m_Trade_1_MovingPercentStoplossForTarget_2.ToString();
                //textBox30.Text = m_Trade_1_MovingATRStoplossForTarget_1.ToString();
               // textBox301.Text = m_Trade_1_MovingATRStoplossForTarget_2.ToString();
                textBox51.Text = m_Trade_1_ATRStoplossMultiplier.ToString();

                //trade2 setting
               
                //sanika::6-Jan-2021::Changed variable
               


                textBox31.Text = m_Trade_2_PercentMultiplierForTarget_4.ToString();
                textBox32.Text = m_Trade_2_PercentMultiplierForTarget_5.ToString();
                textBox33.Text = m_Trade_2_PercentMultiplierForTarget_6.ToString();
                textBox34.Text = m_Trade_2_ATRMultiplierForTarget_4.ToString();
                textBox35.Text = m_Trade_2_ATRMultiplierForTarget_5.ToString();
                textBox36.Text = m_Trade_2_ATRMultiplierForTarget_6.ToString();
                textBox52.Text = m_Trade_2_ATRStoplossMultiplier.ToString();
                textBox38.Text = m_Trade_2_MovingPercentStoplossForTarget_1.ToString();
                textBox39.Text = m_Trade_2_MovingPercentStoplossForTarget_2.ToString();
               // textBox40.Text = m_Trade_2_MovingATRStoplossForTarget_1.ToString();
               // textBox401.Text = m_Trade_2_MovingATRStoplossForTarget_2.ToString();
                textBox37.Text = m_Trade_2_PercentStoplossMultiplier.ToString();
                //Common setting
               
                textBox41.Text = m_PercentOfHighAndLowCondition.ToString();
                textBox42.Text = m_Target_1_ExitQuantity.ToString();
                textBox43.Text = m_Target_2_ExitQuantity.ToString();
                textBox100.Text = m_MaxLoss.ToString();
                textBox101.Text = m_MaxProfit.ToString();
                if (m_FinetunesFlag == true)
                { finerunesTrue.IsChecked = true; }
                else { finerunesFalse.IsChecked = true; }
                textBox45.Text = m_PricePercentForNewOrder.ToString();

                if (m_ATRFlag == true)
                { ATRTrue.IsChecked = true; }
                else { ATRFalse.IsChecked = true; }
                textBox47.Text = m_IndividualTradeCount.ToString();
                WriteUniquelogs("TradeSettingLogs" + " ", "Reading details from INI file.", MessageType.Informational);
            }
            catch (Exception e)
            {
                logger.LogMessage("FetchDetailsFromINI - Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }
       private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            m_TradeObj.Close();
            WriteUniquelogs("TradeSettingLogs" + " ", "btnCancel_Click : Cancel click.", MessageType.Informational);
        }
        private void btnApply_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            btnApply.Foreground = Brushes.Black;
        }

        private void btnCancel_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            btnCancel.Foreground = Brushes.Black;
        }
        private void btnApply_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            btnApply.Foreground = Brushes.White;
        }

        private void btnCancel_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            btnCancel.Foreground = Brushes.White;
        }
        private void btnApply_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //IRDSPM::Pratiksha::06-Jan-2021::For Time changes
                string[] inputTime = { textBox1.Text, textBox2.Text, textBox3.Text, textBox4.Text };
                bool[] result = new bool[4];
                for (int i = 0; i < inputTime.Length; i++)
                {
                    Match m = Regex.Match(inputTime[i], "^([0-1]?\\d|2[0-3])(?::([0-5]?\\d))?$");
                    result[i] = m.Success;
                }

                if (result[0] == true && result[1] == true && result[2] == true && result[3] == true)
                {
                    //IRDSPM::PRatiksha::07-01-2021::For checking empty validations
                    if (textBox5.Text != "" && textBox6.Text != "" && textBox7.Text != "" && textBox8.Text != "" && textBox8.Text != ""
                    && textBox21.Text != "" && textBox22.Text != "" && textBox23.Text != "" && textBox24.Text != "" && textBox25.Text != ""
                    && textBox26.Text != "" && textBox27.Text != "" && textBox28.Text != "" && textBox29.Text != "" //&& //textBox30.Text != "" && textBox301.Text != ""
                    && textBox31.Text != "" && textBox32.Text != "" && textBox33.Text != "" && textBox34.Text != "" && textBox35.Text != ""
                    && textBox36.Text != "" && textBox37.Text != "" && textBox38.Text != "" && textBox39.Text != "" //&& //textBox40.Text != "" && textBox401.Text != ""
                    && textBox41.Text != "" && textBox42.Text != "" && textBox43.Text != "" && textBox45.Text != "" && textBox47.Text != "" && textBox51.Text != "" 
                    && textBox52.Text != "")// && textBox10.Text != "")
                    {
            message = "Do you want to save the changes?";
            buttons = MessageBoxButtons.YesNo;
            dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
            if (dialog == DialogResult.Yes)
            {
                            string[] key = new string[43];
                            string[] value = new string[43];
                //general setting
                key[0] = "StartTimeOfFirstCandle"; 
                key[1] = "PlaceOrderStartTime"; 
                key[2] = "PlaceOrderEndTime";
                key[3] = "TimeForExitAllPositions"; 
                key[4] = "BarInterval";
                key[5] = "PipInPaisa"; 
                key[6] = "TotalCapital"; 
                key[7] = "RiskPercent";
                key[8] = "ATRPeriod"; 

                value[0]= textBox1.Text; value[1]= textBox2.Text; value[2]= textBox3.Text; 
                value[3]= textBox4.Text; value[4]= textBox5.Text;
                value[5]= textBox6.Text; value[6]= textBox7.Text; value[7]= textBox8.Text;
                value[8]= textBox9.Text;  

                //trade 1
                key[9] = "Trade_1_PercentMultiplierForTarget_1"; 
                key[10] = "Trade_1_PercentMultiplierForTarget_2";
                key[11] = "Trade_1_PercentMultiplierForTarget_3"; 
                key[12] = "Trade_1_ATRMultiplierForTarget_1";
                key[13] = "Trade_1_ATRMultiplierForTarget_2"; 
                key[14] = "Trade_1_ATRMultiplierForTarget_3"; 
                key[15] = "Trade_1_ATRStoplossMultiplier";
                            key[16] = "Trade_1_MovingPercentStoplossForTarget_1";
                            key[17] = "Trade_1_MovingPercentStoplossForTarget_2";
                            key[18] = "Trade_1_MovingATRStoplossForTarget_1";
                            key[19] = "Trade_1_MovingATRStoplossForTarget_2";

                value[9] = textBox21.Text; value[10] = textBox22.Text;
                value[11] = textBox23.Text; value[12] = textBox24.Text;
                value[13] = textBox25.Text; value[14] = textBox26.Text; 
                value[15] = textBox51.Text; value[16] = textBox28.Text;
                value[17] = textBox29.Text; //value[18] = textBox30.Text;
                //value[19] = textBox301.Text;

                //trade 2
                key[20] = "Trade_2_PercentMultiplierForTarget_4";
                key[21] = "Trade_2_PercentMultiplierForTarget_5";
                key[22] = "Trade_2_PercentMultiplierForTarget_6";
                key[23] = "Trade_2_ATRMultiplierForTarget_4";
                key[24] = "Trade_2_ATRMultiplierForTarget_5";
                key[25] = "Trade_2_ATRMultiplierForTarget_6";
                key[26] = "Trade_2_ATRStoplossMultiplier";
                key[27] = "Trade_2_MovingPercentStoplossForTarget_1";
                key[28] = "Trade_2_MovingPercentStoplossForTarget_2";
                key[29] = "Trade_2_MovingATRStoplossForTarget_1";
                key[30] = "Trade_2_MovingATRStoplossForTarget_2";

                value[20] = textBox31.Text; value[21] = textBox32.Text;
                value[22] = textBox33.Text; value[23] = textBox34.Text;
                value[24] = textBox35.Text; value[25] = textBox36.Text;
                value[26] = textBox52.Text; value[27] = textBox38.Text;
                value[28] = textBox39.Text; //value[29] = textBox40.Text;
               // value[30] = textBox401.Text;

                //common setting
                key[31] = "PercentOfHighAndLowCondition";  
                key[32] = "Target_1_ExitQuantity";
                key[33] = "Target_2_ExitQuantity"; 
                key[34] = "FinetunesFlag";
                key[35] = "PricePercentForNewOrder"; 
                key[36] = "ATRFlag"; 
                key[37] = "IndividualTradeCount";
                           
                key[38] = "Trade_1_PercentStoplossMultiplier";
                key[39] = "Trade_2_PercentStoplossMultiplier";
                value[31]= textBox41.Text; value[32]= textBox42.Text; value[33]= textBox43.Text;
                if (finerunesTrue.IsChecked == true)
                { value[34] = "true"; }
                else { value[34] = "false"; }

                value[35]= textBox45.Text;

                if (ATRTrue.IsChecked == true)
                { value[36] = "true"; }
                else { value[36] = "false"; }

                value[37]= textBox47.Text;                            
                value[38] = textBox27.Text;
                value[39] = textBox37.Text;
                key[40] = "MaxLoss";
                value[40] = textBox100.Text;
                key[40] = "MaxProfit";
                value[40] = textBox101.Text;
                for (int i = 0; i < 41; i++)
                {
                    readSettings.writeINIFile("SDATA", key[i], value[i]);
                }
               // readSettings.writeINIFileBarInterval("TRADINGSETTING", "Interval", value[4]);
                message = "Saved changes successfully!!!";
                buttons = MessageBoxButtons.OK;
                dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                if (dialog == DialogResult.OK)
                {
                    m_TradeObj.Close();
                }
               
                WriteUniquelogs("TradeSettingLogs" + " ", "btnApply_Click :  Details saved in ini file.", MessageType.Informational);
            }
            else
            {
                m_TradeObj.Close();
                WriteUniquelogs("TradeSettingLogs" + " ", "btnApply_Click : You have clicked No, no data updated in trade setting.ini file.", MessageType.Informational);
                        }
                    }//IRDSPM::PRatiksha::07-01-2021::For checking empty validations
                    else
                    {
                        string parameter = "";
                        if (textBox5.Text == "") { parameter += "\nBar Interval"; }
                        if (textBox6.Text == "") { parameter += "\nPip In Paisa"; }
                        if (textBox7.Text == "") { parameter += "\nTotal Capital"; }
                        if (textBox8.Text == "") { parameter += "\nRisk Percent"; }
                        if (textBox9.Text == "") { parameter += "\nATR Period"; }
                        //if (textBox10.Text == "") { parameter += "\nATR Multiplier For Quantity"; }
                        if (textBox21.Text == "") { parameter += "\nPercent Multiplier For Target-1"; }
                        if (textBox22.Text == "") { parameter += "\nPercent Multiplier For Target-2"; }
                        if (textBox23.Text == "") { parameter += "\nPercent Multiplier For Target-3"; }
                        if (textBox24.Text == "") { parameter += "\nATR Multiplier For Target-1"; }
                        if (textBox25.Text == "") { parameter += "\nATR Multiplier For Target-2"; }
                        if (textBox26.Text == "") { parameter += "\nATR Multiplier For Target-3"; }
                        if (textBox27.Text == "") { parameter += "\nTrade 1 Stoploss Percent Multiplier"; }
                        if (textBox28.Text == "") { parameter += "\nTrade 1 Moving Percent For StopLoss T-1"; }
                        if (textBox29.Text == "") { parameter += "\nTrade 1 Moving Percent For StopLoss T-2"; }
                       // if (textBox30.Text == "") { parameter += "\nTrade 1 Moving ATR For StopLoss Target-1"; }
                       // if (textBox301.Text == "") { parameter += "\nTrade 1 Moving ATR For StopLoss Target-2"; }
                        if (textBox31.Text == "") { parameter += "\nPercent Multiplier For Target-4"; }
                        if (textBox32.Text == "") { parameter += "\nPercent Multiplier For Target-5"; }
                        if (textBox33.Text == "") { parameter += "\nPercent Multiplier For Target-6"; }
                        if (textBox34.Text == "") { parameter += "\nATR Multiplier For Target-4"; }
                        if (textBox35.Text == "") { parameter += "\nATR Multiplier For Target-5"; }
                        if (textBox36.Text == "") { parameter += "\nATR Multiplier For Target-6"; }
                        if (textBox37.Text == "") { parameter += "\nTrade 2 Stoploss Percent Multiplier"; }
                        if (textBox38.Text == "") { parameter += "\nTrade 2 Moving Percent For StopLoss T-1"; }
                        if (textBox39.Text == "") { parameter += "\nTrade 2 Moving Percent For StopLoss T-2"; }
                       // if (textBox40.Text == "") { parameter += "\nTrade 2 Moving ATR For StopLoss Target-1"; }
                       // if (textBox401.Text == "") { parameter += "\nTrade 2 Moving ATR For StopLoss Target-2"; }
                        if (textBox41.Text == "") { parameter += "\nPercent Of High And Low Condition"; }
                        if (textBox42.Text == "") { parameter += "\nTarget-1 Exit Quantity"; }
                        if (textBox43.Text == "") { parameter += "\nTarget-2 Exit Quantity"; }
                        if (textBox45.Text == "") { parameter += "\nPrice Percent For New Order"; }
                        if (textBox47.Text == "") { parameter += "\nIndividual Trade Count"; }
                        if (textBox51.Text == "") { parameter += "\nTrade 1 Stoploss ATR Multiplier"; }
                        if (textBox52.Text == "") { parameter += "\nTrade 2 Stoploss ATR Multiplier"; }
                        if (textBox100.Text == "") { parameter += "\nMax Loss"; }
                        if (textBox101.Text == "") { parameter += "\nMax Profit"; }
                        message = "Please enter proper values for following field: " + parameter;
                        buttons = MessageBoxButtons.OK;
                        dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    //IRDSPM::Pratiksha::07-Jan-2021::For specifying field name
                    string parameter = "";
                    if (result[0] == false)
                    {
                        parameter += "\nStart Time Of First Candle";
                    }
                    if (result[1] == false)
                    {
                        parameter += "\nPlace Order Start Time";
                    }
                    if (result[2] == false)
                    {
                        parameter += "\nPlace Order End Time";
                    }
                    if (result[3] == false)
                    {
                        parameter += "\nTime For Exit All Positions.";
                    }
                    message = "Please enter proper time in HH:MM format for following field: " + parameter;
                    buttons = MessageBoxButtons.OK;
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                }
            }
            catch(Exception ex)
            {
                WriteUniquelogs("TradeSettingLogs" + " ", "btnApply_Click : Exception: "+ex.Message, MessageType.Informational);
            }
        }
        public void WriteUniquelogs(string symbol, string message, MessageType msgType)
        {
            try
            {
                SymbolLogs log = new SymbolLogs();
                log.createFile(symbol);
                log.LogMessage(message, MessageType.Informational);
                log = null;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : " + e.Message);
                Trace.WriteLine("Exception : " + e.Message);
            }
        }
    }
}
