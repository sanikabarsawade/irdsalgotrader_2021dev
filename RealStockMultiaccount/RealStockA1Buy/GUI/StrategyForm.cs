﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using IRDSAlgoOMS;
using IRDSAlgoOMS.GUI;

namespace RealStockA1Strategy
{
    public partial class StrategyForm : Form
    {
        private AlgoOMS AlgoOMS = null;
        RealStockA1StrategyClass m_RealStockA1StrategyClass;
        bool login = false;
        bool automationStart = false;
        Logger logger = Logger.Instance;
        string userID = " ";
        ReadSettings Settings;
        string m_BrokerName = "Zerodha";
        string m_TradingSettingFileName = "StrategySetting_";
        string m_MsgTitle = "RealStock Strategy";
        //IRDSPM::PRatiksha::30-04-2021::For messagebox Changes
        string message = "";
        MessageBoxButtons btnOK = MessageBoxButtons.OK;
        MessageBoxButtons btnYesNo = MessageBoxButtons.YesNo;
        DialogResult dialog;
        string m_FrmTitle = "";
        public delegate void UpdateControlsDelegate();
        public StrategyForm(AlgoOMS algoOMS, string brokerName, string iniUsername, MainWindow obj, string FrmTitle, string msgTitle)
        {
            try
            {
                InitializeComponent();
                m_BrokerName = brokerName;
                m_FrmTitle = FrmTitle;
                m_MsgTitle = msgTitle;
                if (AlgoOMS == null)
                    AlgoOMS = algoOMS;
                m_BrokerName = brokerName;

                m_TradingSettingFileName += iniUsername + ".ini";
                if (m_BrokerName == "Zerodha")
                {
                    string path = Directory.GetCurrentDirectory();
                    string iniFile = path + @"\Configuration\" + "ZerodhaSettings_" + iniUsername + ".ini";
                    if (File.Exists(iniFile))
                    {
                        Settings = new ReadSettings(logger, AlgoOMS);
                        userID = Settings.readUserId(iniFile);
                        gpMain.Text = "For " + userID;
                    }
                    else
                    {
                        message = iniFile + " INI file does not exist!!";
                        dialog = System.Windows.Forms.MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Warning);
                    }
                    AlgoOMS.SetBroker(0);
                    AlgoOMS.CreateKiteConnectObject(userID, "ZerodhaSettings_" + iniUsername + ".ini", obj);
                }
                else if (m_BrokerName == "Samco")
                {
                    string path = Directory.GetCurrentDirectory();
                    string iniFile = path + @"\Configuration\" + "SamcoSettings.ini";
                    if (File.Exists(iniFile))
                    {
                        Settings = new ReadSettings(logger, AlgoOMS);
                        // userID = Settings.readUserId(iniFile);
                    }
                    else
                    {
                        message = iniFile + " INI file does not exist!!";
                        dialog = System.Windows.Forms.MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Warning);
                    }
                    AlgoOMS.SetBroker(1);
                    AlgoOMS.CreateKiteConnectObject(userID, "SamcoSettings.ini", obj);
                }
                else if (m_BrokerName == "Composite")
                {
                    string path = Directory.GetCurrentDirectory();
                    string iniFile = path + @"\Configuration\" + "CompositeSettings_" + iniUsername + ".ini";
                    if (File.Exists(iniFile))
                    {
                        Settings = new ReadSettings(logger, AlgoOMS);
                        userID = Settings.readUserId(iniFile);
                        gpMain.Text = "For " + userID;
                    }
                    else
                    {
                        message = iniFile + " INI file does not exist!!";
                        dialog = System.Windows.Forms.MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Warning);
                    }
                    AlgoOMS.SetBroker(2);
                    AlgoOMS.CreateKiteConnectObject(userID, "CompositeSettings_" + iniUsername + ".ini", obj);
                }
                //IRDSPM::Pratiksha::29-12-2020::For color of Automation status
                AutomationNotstarted();
            }
            catch (Exception er)
            {
                logger.LogMessage("Error : " + er.Message + " " + userID, MessageType.Exception);
            }
        }

        private void OnNetworkAvailabilityChanged(object sender, NetworkAvailabilityEventArgs e)
        {
            try
            {
                if (e.IsAvailable)
                {
                    Console.WriteLine("Network has become available");
                }
                else
                {
                    bool flag = false;
                    try
                    {
                        using (var client = new WebClient())
                        using (client.OpenRead("http://clients3.google.com/generate_204"))
                        {
                            flag = true;
                        }
                    }
                    catch
                    {
                        flag = false;
                    }
                    if (!flag)
                    {
                        string message = "Please check internet connection!!";
                        System.Windows.Forms.MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Warning);
                    }

                }
            }
            catch (Exception er)
            {
                logger.LogMessage("Error : " + er.Message + " " + userID, MessageType.Exception);
            }
        }

        private void StrategyForm_Load(object sender, EventArgs e)
        {
            try
            {
                NetworkChange.NetworkAvailabilityChanged +=
                 new NetworkAvailabilityChangedEventHandler(OnNetworkAvailabilityChanged);
                if (m_BrokerName == "Zerodha")
                {
                    var path = Directory.GetCurrentDirectory();
                    //sanika::12-Aug-2021::removed hardcode userid
                    string iniFile = path + @"\Configuration\" + "ZerodhaSettings_"+userID+".ini";
                    if (File.Exists(iniFile))
                    {
                        Settings = new ReadSettings(logger, AlgoOMS);
                        //userID = Settings.readUserId(iniFile);
                    }
                    else
                    {
                        message = iniFile + " INI file does not exist!!";
                        dialog = System.Windows.Forms.MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Warning);
                    }
                }
                DoLoginProcess();
            }
            catch (Exception er)
            {
                logger.LogMessage("Error : " + er.Message + " " + userID, MessageType.Exception);
                Trace.WriteLine("exception from form load : " + er.Message);
            }
        }
        public void DoLoginProcess()
        {
            loginToAccount();
        }
        public void loginToAccount()
        {
            string error = "";
            int loginCount = 0;
            try
            {
                Cursor.Current = Cursors.WaitCursor;


                logger.LogMessage("Clicked on login button " + userID, MessageType.Informational);
                AlgoOMS.Connect(userID);
                while (loginCount < 3)
                {
                    if (AlgoOMS.kiteWrapperConnect != null || AlgoOMS.compositeWrapperConnect != null)
                    {
                        if (AlgoOMS.GetLoginStatus(userID))
                        {
                           login = true;
                            m_RealStockA1StrategyClass = new RealStockA1StrategyClass(AlgoOMS, logger, userID, m_TradingSettingFileName, m_BrokerName);
                            m_RealStockA1StrategyClass.loadObject(this);
                            m_RealStockA1StrategyClass.readTradingSymbol(Settings);
                            
                        }
                        break;
                    }
                    else
                    {
                        login = false;
                        logger.LogMessage("Not able to login trying " + loginCount + " " + userID, MessageType.Informational);
                    }
                }
                loginCount++;

            }
            catch (Exception er)
            {
                logger.LogMessage("Cross thread exception " + userID, MessageType.Exception);
                error = er.Message;
                if (error.Contains("empty") && error.Contains("api_key") && error.Contains("access_token"))
                {
                    logger.LogMessage("Error : " + er.Message, MessageType.Exception);
                    message = "Please check account credentials.";
                    System.Windows.Forms.MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Error);
                }
                else
                {
                    logger.LogMessage("Error : " + er.Message + " " + userID, MessageType.Exception);
                    message = "Error : " + er.Message;
                    System.Windows.Forms.MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Error);
                }
            }
            finally
            {
                if (error.Contains("System.Data.SQLite"))
                {
                    message = "Please add dll for sqlite.";
                    System.Windows.Forms.MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Error);
                    logger.LogMessage("Please add dll for sqlite " + userID, MessageType.Informational);
                    this.Close();
                }
            }
            Cursor.Current = Cursors.Default;
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            Settings.m_IsNewSymbolAdded = false;
            btnStart.Enabled = false;
            try
            {
                logger.LogMessage("Clicked on start button " + userID, MessageType.Informational);
                if (login)
                {
                    automationStart = true;
                    //IRDSPM::Pratiksha::29-12-2020::For color of Automation status
                    Automationstarted();
                    m_RealStockA1StrategyClass.loadObject(this);
                    //IRDS::Jyoti::16-07-2021::Added for restore changes
                    m_RealStockA1StrategyClass.readTradingSymbol(Settings);
                    m_RealStockA1StrategyClass.m_isCloseAllOrder = false;
                    bool res = m_RealStockA1StrategyClass.startThread(userID, Settings);
                    if (res)
                    {
                        btnStart.Enabled = false;
                    }
                    else
                    {
                        btnStart.Enabled = true;
                        message = "Some values are missing in settings section. Please check ini file.";
                        System.Windows.Forms.MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Exclamation);
                    }
                }
            }
            catch (Exception er)
            {
                message = "Error : " + er.Message;
                System.Windows.Forms.MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Error);
                logger.LogMessage("Error : " + er.Message + " " + userID, MessageType.Exception);
            }

        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            try
            {
                if (automationStart)
                {
                logger.LogMessage("Clicked on stop button " + userID, MessageType.Informational);
                    message = "It will only stop the automation, Orders will not close. \n Do you want to stop automation?";
                dialog = System.Windows.Forms.MessageBox.Show(message, m_MsgTitle, btnYesNo, MessageBoxIcon.Exclamation);
                if (dialog == DialogResult.Yes)
                {
                    if (login)
                    {

                            bool res = false;
                            {
                                res = m_RealStockA1StrategyClass.StopThread();
                            }
                            if (res)
                            {
                                message = "Automation stopped.";
                                dialog = System.Windows.Forms.MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Exclamation);
                                logger.LogMessage("Automation stopped " + userID, MessageType.Informational);
                                automationStart = false;
                                //IRDSPM::Pratiksha::29-12-2020::For color of Automation status
                                Automationstopped();
                                btnStart.Enabled = true;
                            }
                            else
                            {
                                message = "Unable to stop automation.";
                                dialog = System.Windows.Forms.MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Warning);
                                logger.LogMessage("Unable to stop automation " + userID, MessageType.Informational);
                            }

                        }
                        else
                        {
                            message = "Stop operation not able to perform.";
                            dialog = System.Windows.Forms.MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Warning);
                            logger.LogMessage("Stop operation not able to perform " + userID, MessageType.Informational);
                        }
                        }
                    }
                    else
                    {
                    message = "To perform stop automation, first need to start the automation.";
                    dialog = System.Windows.Forms.MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Information);
                    logger.LogMessage("To perform stop. first Start the automation " + userID, MessageType.Informational);
                }
            }
            catch (Exception er)
            {
                message = "Error : " + er.Message;
                dialog = System.Windows.Forms.MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Error);
                logger.LogMessage("Error : " + er.Message + " " + userID, MessageType.Informational);
            }
        }



        private void btnRefresh_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            logger.LogMessage("clicked on refresh button  " + userID, MessageType.Informational);
            if (login)
            {
                try
                {
                    m_RealStockA1StrategyClass.readTradingSymbol(Settings);

                }
                catch (Exception ew)
                {
                    logger.LogMessage("Exception from clicking refresh button : " + ew.Message + " " + userID, MessageType.Exception);
                }
            }
            else
            {
                logger.LogMessage("Please login first to check open positions " + userID, MessageType.Informational);
                message = "Please login first to check open positions.";
                dialog = System.Windows.Forms.MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Error);
            }
            Cursor.Current = Cursors.Default;
        }

        public void enableButton()
        {
            InvokeUpdateControls();
        }

        public void InvokeUpdateControls()
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new UpdateControlsDelegate(UpdateControls));
            }
            else
            {
                UpdateControls();
            }
        }

        private void UpdateControls()
        {
            btnStart.Enabled = true;
        }



        private void btnStopCloseOrder_Click(object sender, EventArgs e)
        {
            try
            {
                logger.LogMessage("Clicked on stop automation and close all order button " + userID, MessageType.Informational);
                if (login)
                {
                    if (automationStart)
                    {
                        message = "Are you sure you want to close all orders?";
                        dialog = System.Windows.Forms.MessageBox.Show(message, m_MsgTitle, btnYesNo, MessageBoxIcon.Exclamation);
                        if (dialog == DialogResult.Yes)
                        {
                        bool res = false;

                        res = m_RealStockA1StrategyClass.StopThread();
                        m_RealStockA1StrategyClass.CloseAllOrder();

                        if (res)
                        {
                            message = "Automation stopped.";
                            dialog = System.Windows.Forms.MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Exclamation);
                            logger.LogMessage("Automation stopped " + userID, MessageType.Informational);
                            automationStart = false;
                            //IRDSPM::Pratiksha::29-12-2020::For color of Automation status
                            Automationstopped();
                            btnStart.Enabled = true;
                        }
                        else
                        {
                            message = "Unable to stop automation.";
                            dialog = System.Windows.Forms.MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Warning);
                            logger.LogMessage("Unable to stop automation " + userID, MessageType.Informational);
                        }
                        }
                    }
                    else
                    {
                        message = "To perform stop. first Start the automation.";
                        dialog = System.Windows.Forms.MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Information);
                        logger.LogMessage("To perform stop. first Start the automation " + userID, MessageType.Informational);
                    }
                }
                else
                {
                    message = "Stop operation not able to perform.";
                    dialog = System.Windows.Forms.MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Warning);
                    logger.LogMessage("Stop operation not able to perform " + userID, MessageType.Informational);
                }
            }
            catch (Exception er)
            {
                message = "Error : " + er.Message;
                dialog = System.Windows.Forms.MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Error);
                logger.LogMessage("Error : " + er.Message + " " + userID, MessageType.Informational);
            }
        }
        private void btnHistorical_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                if (login)
                {
                    Cursor.Current = Cursors.Default;
                    logger.LogMessage("Stopped Download " + userID, MessageType.Informational);
                    message = "Successfully Downloaded.";
                    dialog = System.Windows.Forms.MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    logger.LogMessage("Please login first to download data " + userID, MessageType.Informational);
                    message = "Please login first to download data.";
                    dialog = System.Windows.Forms.MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                logger.LogMessage("error in  btnHistorical_Click " + ex.Message + " " + userID, MessageType.Informational);
            }
        }


        //sanika::25-sep-2020::added for resume testing
        public void SaveBin()
        {
            var path = Directory.GetCurrentDirectory();
            string FileName = path + @"/RealStockA1_" + userID + ".bin";
            if (m_RealStockA1StrategyClass != null)
            {
                if (m_RealStockA1StrategyClass.m_BasicOperation != null)
                {
                    m_RealStockA1StrategyClass.m_BasicOperation.SaveBinFile(FileName);
                }
            }
        }

        private void StrategyForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                message = "Do you really want to close strategy?";
                dialog = System.Windows.Forms.MessageBox.Show(message, m_MsgTitle, btnYesNo, MessageBoxIcon.Warning);

                if (dialog == DialogResult.Yes)
                {
                    logger.LogMessage("Message box display and clicked on yes!! " + userID, MessageType.Informational);
                    //sanika::25-sep-2020::added for resume testing
                    SaveBin();
                    logger.LogMessage("clicked on close form " + userID, MessageType.Informational);
                    if (login)
                    {
                        bool res = false;
                        if (AlgoOMS.stopThread())
                        {
                            logger.LogMessage("All threads are closed " + userID, MessageType.Informational);
                        }
                        if (m_RealStockA1StrategyClass != null)
                        {
                            //automationWithFile.SaveBin();
                            res = m_RealStockA1StrategyClass.StopThread();
                            if (res)
                            {
                                logger.LogMessage("Chanbreak threads are closed " + userID, MessageType.Informational);

                            }
                            else
                                logger.LogMessage("Threads are not able to close " + userID, MessageType.Informational);
                        }
                        e.Cancel = false;
                    }
                }
                else
                {
                    e.Cancel = true;
                    logger.LogMessage("Message box display and clicked on No!! " + userID, MessageType.Informational);
                }

            }
            catch (Exception er)
            {
                message = "Error : " + er.Message;
                dialog = System.Windows.Forms.MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Error);
                logger.LogMessage("Error : " + er.Message + " " + userID, MessageType.Exception);
            }
        }

        //IRDSPM::Pratiksha::29-12-2020::For color changes of automation
        private void AutomationNotstarted()
        {
            lblStatusValue.Text = "Waiting";
            lblStatusValue.BackColor = Color.Blue;
            lblStatusValue.ForeColor = Color.White;
        }
        private void Automationstarted()
        {
            lblStatusValue.Text = "Started";
            lblStatusValue.BackColor = Color.LimeGreen;
            lblStatusValue.ForeColor = Color.White;
        }
        private void Automationstopped()
        {
            lblStatusValue.Text = "Stopped";
            lblStatusValue.BackColor = Color.Red;
            lblStatusValue.ForeColor = Color.White;
        }

        private void btnStopCloseOrder_Click_1(object sender, EventArgs e)
        {
            try
            {
                logger.LogMessage("Clicked on stop automation and close all order button " + userID, MessageType.Informational);
                message = "It will close the orders and Stop the automation.\n Do you want to close orders and stop the automation?";
                dialog = System.Windows.Forms.MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Exclamation);
                if (dialog == DialogResult.Yes)
                {
                    if (login)
                    {
                        if (automationStart)
                        {
                            bool res = false;

                            res = m_RealStockA1StrategyClass.StopThread();
                            m_RealStockA1StrategyClass.CloseAllOrder();

                            if (res)
                            {
                                message = "Automation stopped.";
                                dialog = System.Windows.Forms.MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Exclamation);
                                logger.LogMessage("Automation stopped " + userID, MessageType.Informational);
                                automationStart = false;
                                //IRDSPM::Pratiksha::29-12-2020::For color of Automation status
                                Automationstopped();
                                btnStart.Enabled = true;
                            }
                            else
                            {
                                message = "Unable to stop automation.";
                                dialog = System.Windows.Forms.MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Warning);
                                logger.LogMessage("Unable to stop automation " + userID, MessageType.Informational);
                            }

                        }
                        else
                        {
                            message = "To perform stop. first Start the automation.";
                            dialog = System.Windows.Forms.MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Information);
                            logger.LogMessage("To perform stop. first Start the automation " + userID, MessageType.Informational);
                        }
                    }
                    else
                    {
                        message = "Stop operation not able to perform.";
                        dialog = System.Windows.Forms.MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Warning);
                        logger.LogMessage("Stop operation not able to perform " + userID, MessageType.Informational);
                    }
                }
            }
            catch (Exception er)
            {
                message = "Error : " + er.Message;
                dialog = System.Windows.Forms.MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Error);
                logger.LogMessage("Error : " + er.Message + " " + userID, MessageType.Informational);
            }
        }
        //IRDSPM::Pratiksha::29-12-2020::To open trading setting form
        TradingSetting obj;
        private void btnTradeSetting_Click(object sender, EventArgs e)
        {
            //IRDSPM::Pratiksha::28-05-2021::added user id as parameter
            obj = new TradingSetting(logger, AlgoOMS, userID, m_FrmTitle);
            obj.ShowDialog();
        }
        //IRDSPM::Pratiksha::29-12-2020::To open symbol setting form
        private void btnSymbosetting_Click(object sender, EventArgs e)
        {
            SymbolSettingDet symbolSettingWindows = new SymbolSettingDet(AlgoOMS, userID, Settings, m_FrmTitle);
            symbolSettingWindows.ShowDialog();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            m_RealStockA1StrategyClass.StopThread();
            //sanika::16-July-2021::call stop thread function to stop threads from dll
            m_RealStockA1StrategyClass.StopMainThreads();
            this.Close();
        }

        //26-July-2021::Pratiksha::For changing backcolor of button on the basis of enable and disable 
        
        private void btnStart_EnabledChanged(object sender, EventArgs e)
        {
            if (btnStart.Enabled == false)
            {
                btnStart.BackColor = Color.DarkGray;
            }
            else
            {
                btnStart.BackColor = Color.Gainsboro;
                //03-AUG-2021::Pratiksha::Added to change the label to waiting
                AutomationNotstarted();
            }
        }
    }
}
