﻿namespace RealStockA1Strategy
{
    partial class StrategyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StrategyForm));
            this.gpMain = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnSymbosetting = new System.Windows.Forms.Button();
            this.btnTradeSetting = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblStatusValue = new System.Windows.Forms.Label();
            this.lblStatusKey = new System.Windows.Forms.Label();
            this.btnLogin = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnStopCloseOrder = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.gpMain.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gpMain
            // 
            this.gpMain.Controls.Add(this.tableLayoutPanel1);
            this.gpMain.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpMain.Location = new System.Drawing.Point(13, 2);
            this.gpMain.Margin = new System.Windows.Forms.Padding(4);
            this.gpMain.Name = "gpMain";
            this.gpMain.Padding = new System.Windows.Forms.Padding(4);
            this.gpMain.Size = new System.Drawing.Size(1546, 108);
            this.gpMain.TabIndex = 0;
            this.gpMain.TabStop = false;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70.93628F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 29.06372F));
            this.tableLayoutPanel1.Controls.Add(this.groupBox3, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.groupBox1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(4, 24);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1538, 80);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnSymbosetting);
            this.groupBox3.Controls.Add(this.btnTradeSetting);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(1093, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(442, 74);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Trading Details";
            // 
            // btnSymbosetting
            // 
            this.btnSymbosetting.BackColor = System.Drawing.Color.Gainsboro;
            this.btnSymbosetting.Location = new System.Drawing.Point(241, 21);
            this.btnSymbosetting.Margin = new System.Windows.Forms.Padding(4);
            this.btnSymbosetting.Name = "btnSymbosetting";
            this.btnSymbosetting.Size = new System.Drawing.Size(171, 39);
            this.btnSymbosetting.TabIndex = 16;
            this.btnSymbosetting.Text = "Symbol Setting";
            this.btnSymbosetting.UseVisualStyleBackColor = false;
            this.btnSymbosetting.Click += new System.EventHandler(this.btnSymbosetting_Click);
            // 
            // btnTradeSetting
            // 
            this.btnTradeSetting.BackColor = System.Drawing.Color.Gainsboro;
            this.btnTradeSetting.Location = new System.Drawing.Point(39, 21);
            this.btnTradeSetting.Margin = new System.Windows.Forms.Padding(4);
            this.btnTradeSetting.Name = "btnTradeSetting";
            this.btnTradeSetting.Size = new System.Drawing.Size(171, 39);
            this.btnTradeSetting.TabIndex = 15;
            this.btnTradeSetting.Text = "Trading Setting";
            this.btnTradeSetting.UseVisualStyleBackColor = false;
            this.btnTradeSetting.Click += new System.EventHandler(this.btnTradeSetting_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblStatusValue);
            this.groupBox1.Controls.Add(this.lblStatusKey);
            this.groupBox1.Controls.Add(this.btnLogin);
            this.groupBox1.Controls.Add(this.btnStart);
            this.groupBox1.Controls.Add(this.btnStopCloseOrder);
            this.groupBox1.Controls.Add(this.btnStop);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1084, 74);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Automation Details";
            // 
            // lblStatusValue
            // 
            this.lblStatusValue.AutoSize = true;
            this.lblStatusValue.BackColor = System.Drawing.Color.Blue;
            this.lblStatusValue.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.lblStatusValue.ForeColor = System.Drawing.Color.White;
            this.lblStatusValue.Location = new System.Drawing.Point(166, 18);
            this.lblStatusValue.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblStatusValue.Name = "lblStatusValue";
            this.lblStatusValue.Padding = new System.Windows.Forms.Padding(24, 8, 24, 8);
            this.lblStatusValue.Size = new System.Drawing.Size(128, 40);
            this.lblStatusValue.TabIndex = 20;
            this.lblStatusValue.Text = "Waiting";
            // 
            // lblStatusKey
            // 
            this.lblStatusKey.AutoSize = true;
            this.lblStatusKey.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold);
            this.lblStatusKey.Location = new System.Drawing.Point(7, 21);
            this.lblStatusKey.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblStatusKey.Name = "lblStatusKey";
            this.lblStatusKey.Size = new System.Drawing.Size(151, 30);
            this.lblStatusKey.TabIndex = 19;
            this.lblStatusKey.Text = "Run Status:";
            // 
            // btnLogin
            // 
            this.btnLogin.BackColor = System.Drawing.Color.Gainsboro;
            this.btnLogin.Location = new System.Drawing.Point(12, 66);
            this.btnLogin.Margin = new System.Windows.Forms.Padding(4);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(155, 39);
            this.btnLogin.TabIndex = 13;
            this.btnLogin.TabStop = false;
            this.btnLogin.Text = "Login";
            this.btnLogin.UseVisualStyleBackColor = false;
            this.btnLogin.Visible = false;
            // 
            // btnStart
            // 
            this.btnStart.BackColor = System.Drawing.Color.Gainsboro;
            this.btnStart.Location = new System.Drawing.Point(336, 19);
            this.btnStart.Margin = new System.Windows.Forms.Padding(4);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(171, 39);
            this.btnStart.TabIndex = 17;
            this.btnStart.Text = "Start Automation";
            this.btnStart.UseVisualStyleBackColor = false;
            this.btnStart.EnabledChanged += new System.EventHandler(this.btnStart_EnabledChanged);
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnStopCloseOrder
            // 
            this.btnStopCloseOrder.BackColor = System.Drawing.Color.Gainsboro;
            this.btnStopCloseOrder.Location = new System.Drawing.Point(770, 19);
            this.btnStopCloseOrder.Margin = new System.Windows.Forms.Padding(4);
            this.btnStopCloseOrder.Name = "btnStopCloseOrder";
            this.btnStopCloseOrder.Size = new System.Drawing.Size(283, 39);
            this.btnStopCloseOrder.TabIndex = 14;
            this.btnStopCloseOrder.Text = "Close All Orders And Stop Trading";
            this.btnStopCloseOrder.UseVisualStyleBackColor = false;
            this.btnStopCloseOrder.Click += new System.EventHandler(this.btnStopCloseOrder_Click);
            // 
            // btnStop
            // 
            this.btnStop.BackColor = System.Drawing.Color.Gainsboro;
            this.btnStop.Location = new System.Drawing.Point(554, 19);
            this.btnStop.Margin = new System.Windows.Forms.Padding(4);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(171, 39);
            this.btnStop.TabIndex = 18;
            this.btnStop.Text = "Stop Automation";
            this.btnStop.UseVisualStyleBackColor = false;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // StrategyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1700, 140);
            this.Controls.Add(this.gpMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "StrategyForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RealStockA1 Strategy";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.StrategyForm_FormClosing);
            this.Load += new System.EventHandler(this.StrategyForm_Load);
            this.gpMain.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox gpMain;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblStatusValue;
        private System.Windows.Forms.Label lblStatusKey;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnStopCloseOrder;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnTradeSetting;
        private System.Windows.Forms.Button btnSymbosetting;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    }
}

