﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealStockA1Strategy
{
    public class TradingSymbolInfo
    {
        //public string SymbolWithExchange;
        public double Quantity;  
        public string TradingSymbol;
        public string Exchange;
        public string ExpiryDate;
        //IRDSPM::PRatiksha::28-04::2021::For accepting expiry period
        public string ExpiryPeriod;
        //Pratiksha::22-07-2021::Change the int to double
        public double Roundoff;
        //IRDS::Pratiksha::08-06-2021::Add TotalNoOfLots 
        public double TotalNoOfLots;
         //IRDSPM::Pratiksha::05-05-2021::For order type
        public string OrderType;
        public double lotsize;

        //IRDSPM::Pratiksha::19-05-2021::For strike price
        public double StrikePricePut;
        public double StrikePriceCall;
        public double MarginLimit;
        public double Stoploss;
        public string TransactionType;
        public string getSymbol()
        {
            return TradingSymbol;
        }

        public double getQuantity()
        {
            return Quantity;
        }

        public string getExpiryDate()
        {
            return ExpiryDate;
        }
       
        public string getExchange()
        {
            return Exchange;
        }

        public string getExpiryPeriod()
        {
            return ExpiryPeriod;
        }
        public double getRoundoff()
        {
            return Roundoff;
        }

        public double getTotalNoOfLots()
        {
            return TotalNoOfLots;
        }
        public string getOrderType()
        {
            return OrderType;
        }
        public double getLotSize()
        {
            return lotsize;
        }
        public double getStrikePricePut()
        {
            return StrikePricePut;
        }
        public double getStrikePriceCall()
        {
            return StrikePriceCall;
        }
        public double getMarginLimit()
        {
            return MarginLimit;
        }
        public double getStoploss()
        {
            return Stoploss;
        }
        public string getTransactionType()
        {
            return TransactionType;
        }
    }
}
