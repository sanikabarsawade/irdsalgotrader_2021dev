﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IRDSAlgoOMS;

namespace IRDSStategyExecutorMultiAccount
{
    //sanika::25-sep-2020::added for resume 
    [Serializable()]
    public class OpenOrderInfo
    {        
        private Dictionary<string, OpenOrderInfo> symbolWiseOrderInfo;
       
        //sanika::25-sep-2020::added for resume 
        [field: NonSerialized()]
        AutomationWithFile automationWithFile;
        //sanika::25-sep-2020::added for resume 
        [field: NonSerialized()]
        EMAAutomation eMAAutomation;
        bool IsProfitOrderPlaced { get; set; }
        int BuyOrSell = 0;      
        int m_OpenOrderCounter { get; set; }
        int m_StopOrderCounter { get; set; }
        int m_MarketOrderCounter { get; set; }
        int m_ModifyBuyOrderCounter { get; set; }
        int m_ModifySellOrderCounter { get; set; }
        int m_ProfitTrailBuyOrderCounter { get; set; }
        int m_ProfitTrailSellOrderCounter { get; set; }
        int Quantity { get; set; }
        int m_EntryOrderQuantity { get; set; }
        double OpenPriceForProfitTrail { get; set; }
        double OpenPrice { get; set; }
        int IgnoreValue { get; set; }
        string BuyStopOrderId = null;
        string SellStopOrderId = null;
        string BuyMarketOrderId = null;
        string SellMarketOrderId = null;
        string PreviousBuyStopOrderId = null;
        string PreviousSellStopOrderId = null;
        string SellProfitOrderId = null;
        string BuyProfitOrderId = null;
        bool isautomationWithFile = false;
        bool iseMAAutomation = false;
        string StopLossOrderId = null;
        double dMaxEMA = 0;
        double dMinEMA = 0;
        int m_TotalOpenPositions = 0;
        OrderState m_OrderState;
        public enum OrderState
        {
            New,
            Pending,
            OpenComplete,
            StopLoss,
            StoplossComplete,
            TimeExitComplete,
            Complete,
            Rejected
        }
        public OpenOrderInfo(AutomationWithFile automationWithFile)
        {

            symbolWiseOrderInfo = new Dictionary<string, OpenOrderInfo>();
            this.automationWithFile = automationWithFile;
            isautomationWithFile = true;
            //Create OnPropertyChanged method to raise event
            
        }

        public OpenOrderInfo(EMAAutomation eMAAutomation)
        {
            symbolWiseOrderInfo = new Dictionary<string, OpenOrderInfo>();
            this.eMAAutomation = eMAAutomation;
            iseMAAutomation = true;
        }
        public OpenOrderInfo()
        {

        }

        public double GetdMaxEMA(string TradingSymbol)
        {
            double value = 0;
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    value = symbolWiseOrderInfo[TradingSymbol].dMaxEMA;
                }
                else
                {
                    value = 0;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
                value = 0;
            }
            return value;
        }

        public double GetdMinEMA(string TradingSymbol)
        {
            double value = 0;
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    value = symbolWiseOrderInfo[TradingSymbol].dMinEMA;
                }
                else
                {
                    value = 0;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
                value = 0;
            }
            return value;
        }
        public bool AddOrUpdatedMinEMA(string TradingSymbol, double Value)
        {
            bool isAddOrUpdate = false;
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    symbolWiseOrderInfo[TradingSymbol].dMinEMA = Value;
                    isAddOrUpdate = true;
                }
                else
                {
                    OpenOrderInfo orderInfo = new OpenOrderInfo();
                    orderInfo.dMinEMA = Value;
                    symbolWiseOrderInfo.Add(TradingSymbol, orderInfo);
                    isAddOrUpdate = true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
                isAddOrUpdate = false;
            }
            return isAddOrUpdate;
        }

        public bool AddOrUpdatedMaxEMA(string TradingSymbol, double Value)
        {
            bool isAddOrUpdate = false;
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    symbolWiseOrderInfo[TradingSymbol].dMaxEMA = Value;
                    isAddOrUpdate = true;
                }
                else
                {
                    OpenOrderInfo orderInfo = new OpenOrderInfo();
                    orderInfo.dMaxEMA = Value;
                    symbolWiseOrderInfo.Add(TradingSymbol, orderInfo);
                    isAddOrUpdate = true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
                isAddOrUpdate = false;
            }
            return isAddOrUpdate;
        }

        public bool AddOrUpdateIsProfitOrderPlaced(string TradingSymbol, bool Value)
        {
            bool isAddOrUpdate = false;
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    symbolWiseOrderInfo[TradingSymbol].IsProfitOrderPlaced = Value;
                    isAddOrUpdate = true;
                }
                else
                {
                    OpenOrderInfo orderInfo = new OpenOrderInfo();
                    orderInfo.IsProfitOrderPlaced = Value;
                    symbolWiseOrderInfo.Add(TradingSymbol, orderInfo);
                    isAddOrUpdate = true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
                isAddOrUpdate = false;
            }
            return isAddOrUpdate;
        } 

      

        public bool OpenOrderCounter(string TradingSymbol)
        {
            bool isIncreamentCounter = false;
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    symbolWiseOrderInfo[TradingSymbol].m_OpenOrderCounter++;
                    isIncreamentCounter = true;
                }
                else
                {
                    OpenOrderInfo orderInfo = new OpenOrderInfo();
                    orderInfo.m_OpenOrderCounter = 1;
                    symbolWiseOrderInfo.Add(TradingSymbol, orderInfo);
                    isIncreamentCounter = true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
                isIncreamentCounter = false;
            }
            return isIncreamentCounter;
        }

        public bool StopOrderCounter(string TradingSymbol)
        {
            bool isIncreamentCounter = false;
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    symbolWiseOrderInfo[TradingSymbol].m_StopOrderCounter++;
                    isIncreamentCounter = true;
                }
                else
                {
                    OpenOrderInfo orderInfo = new OpenOrderInfo();
                    orderInfo.m_StopOrderCounter = 1;
                    symbolWiseOrderInfo.Add(TradingSymbol, orderInfo);
                    isIncreamentCounter = true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
                isIncreamentCounter = false;
            }
            return isIncreamentCounter;
        }

        public bool SubtractStopOrderCounter(string TradingSymbol)
        {
            bool isSubtractCounter = false;
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    symbolWiseOrderInfo[TradingSymbol].m_StopOrderCounter--;
                    isSubtractCounter = true;
                }
                else
                {
                    OpenOrderInfo orderInfo = new OpenOrderInfo();
                    orderInfo.m_StopOrderCounter = 0;
                    symbolWiseOrderInfo.Add(TradingSymbol, orderInfo);
                    isSubtractCounter = true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
                isSubtractCounter = false;
            }
            return isSubtractCounter;
        }

        public bool ModifyBuyOrderCounter(string TradingSymbol)
        {
            bool isIncreamentCounter = false;
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    symbolWiseOrderInfo[TradingSymbol].m_ModifyBuyOrderCounter++;
                    isIncreamentCounter = true;
                }
                else
                {
                    OpenOrderInfo orderInfo = new OpenOrderInfo();
                    orderInfo.m_ModifyBuyOrderCounter = 1;
                    symbolWiseOrderInfo.Add(TradingSymbol, orderInfo);
                    isIncreamentCounter = true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
                isIncreamentCounter = false;
            }
            return isIncreamentCounter;
        }

        public bool ModifySellOrderCounter(string TradingSymbol)
        {
            bool isIncreamentCounter = false;
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    symbolWiseOrderInfo[TradingSymbol].m_ModifySellOrderCounter++;
                    isIncreamentCounter = true;
                }
                else
                {
                    OpenOrderInfo orderInfo = new OpenOrderInfo();
                    orderInfo.m_ModifySellOrderCounter = 1;
                    symbolWiseOrderInfo.Add(TradingSymbol, orderInfo);
                    isIncreamentCounter = true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
                isIncreamentCounter = false;
            }
            return isIncreamentCounter;
        }

        public bool ProfitTrailSellOrderCounter(string TradingSymbol)
        {
            bool isIncreamentCounter = false;
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    symbolWiseOrderInfo[TradingSymbol].m_ProfitTrailSellOrderCounter++;
                    isIncreamentCounter = true;
                }
                else
                {
                    OpenOrderInfo orderInfo = new OpenOrderInfo();
                    orderInfo.m_ProfitTrailSellOrderCounter = 1;
                    symbolWiseOrderInfo.Add(TradingSymbol, orderInfo);
                    isIncreamentCounter = true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
                isIncreamentCounter = false;
            }
            return isIncreamentCounter;
        }

        public bool ProfitTrailBuyOrderCounter(string TradingSymbol)
        {
            bool isIncreamentCounter = false;
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    symbolWiseOrderInfo[TradingSymbol].m_ProfitTrailBuyOrderCounter++;
                    isIncreamentCounter = true;
                }
                else
                {
                    OpenOrderInfo orderInfo = new OpenOrderInfo();
                    orderInfo.m_ProfitTrailBuyOrderCounter = 1;
                    symbolWiseOrderInfo.Add(TradingSymbol, orderInfo);
                    isIncreamentCounter = true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
                isIncreamentCounter = false;
            }
            return isIncreamentCounter;
        }

        public bool MarketOrderCounter(string TradingSymbol)
        {
            bool isIncreamentCounter = false;
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    symbolWiseOrderInfo[TradingSymbol].m_MarketOrderCounter++;
                    isIncreamentCounter = true;
                }
                else
                {
                    OpenOrderInfo orderInfo = new OpenOrderInfo();
                    orderInfo.m_MarketOrderCounter = 1;
                    symbolWiseOrderInfo.Add(TradingSymbol, orderInfo);
                    isIncreamentCounter = true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
                isIncreamentCounter = false;
            }
            return isIncreamentCounter;
        }

        public void ResetQuantity(string TradingSymbol)
        {           
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    symbolWiseOrderInfo[TradingSymbol].m_EntryOrderQuantity = 0;                    
                }               
               
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);               
            }            
        }

        public bool AddQuantity(string TradingSymbol,int Quantity)
        {
            bool isAdd = false;
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    symbolWiseOrderInfo[TradingSymbol].m_EntryOrderQuantity = Quantity;
                    isAdd = true;
                }
                else
                {
                    OpenOrderInfo orderInfo = new OpenOrderInfo();
                    orderInfo.m_EntryOrderQuantity = Quantity;
                    symbolWiseOrderInfo.Add(TradingSymbol, orderInfo);
                    isAdd = true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
                isAdd = false;
            }
            return isAdd;
        }

        public bool AddOpenPriceForProfitTrail(string TradingSymbol, double OpenPriceForProfitTrail)
        {
            bool isAdd = false;
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    symbolWiseOrderInfo[TradingSymbol].OpenPriceForProfitTrail = OpenPriceForProfitTrail;
                    isAdd = true;
                }
                else
                {
                    OpenOrderInfo orderInfo = new OpenOrderInfo();
                    orderInfo.OpenPriceForProfitTrail = OpenPriceForProfitTrail;
                    symbolWiseOrderInfo.Add(TradingSymbol, orderInfo);
                    isAdd = true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
                isAdd = false;
            }
            return isAdd;
        }

        public bool AddOpenPrice(string TradingSymbol, double OpenPrice)
        {
            bool isAdd = false;
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    symbolWiseOrderInfo[TradingSymbol].OpenPrice = OpenPrice;
                    isAdd = true;
                }
                else
                {
                    OpenOrderInfo orderInfo = new OpenOrderInfo();
                    orderInfo.OpenPrice = OpenPrice;
                    symbolWiseOrderInfo.Add(TradingSymbol, orderInfo);
                    isAdd = true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
                isAdd = false;
            }
            return isAdd;
        }

        public bool AddIgnoreValue(string TradingSymbol, int Value)
        {
            bool isAdd = false;
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    symbolWiseOrderInfo[TradingSymbol].IgnoreValue = Value;
                    isAdd = true;
                }
                else
                {
                    OpenOrderInfo orderInfo = new OpenOrderInfo();
                    orderInfo.IgnoreValue = Value;
                    symbolWiseOrderInfo.Add(TradingSymbol, orderInfo);
                    isAdd = true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
                isAdd = false;
            }
            return isAdd;
        }

        public int GetIgnoreValue(string TradingSymbol)
        {
            int value = 0;
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    value = symbolWiseOrderInfo[TradingSymbol].IgnoreValue;                    
                }
                else
                {
                    value = 0;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
                value = 0;
            }
            return value; 
        }

        public int GetQuantityFromStorage(string TradingSymbol)
        {
            int value = 0;
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    value = symbolWiseOrderInfo[TradingSymbol].m_EntryOrderQuantity;                   
                }
                
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
                value = 0;
            }
            return value;
        }

        public int GetQuantity(string TradingSymbol,string Exchange)
        {
            int value = 0;
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    value = symbolWiseOrderInfo[TradingSymbol].Quantity;
                    if(value == 0)
                    {
                        if(iseMAAutomation)
                        {
                            symbolWiseOrderInfo[TradingSymbol].Quantity = eMAAutomation.GetQuantity(TradingSymbol, Exchange);
                        }
                        if(isautomationWithFile)
                        {
                            symbolWiseOrderInfo[TradingSymbol].Quantity = automationWithFile.GetQuantity(TradingSymbol, Exchange);
                        }
                    }
                }  
                else
                {
                    if (isautomationWithFile)
                    {
                        value = automationWithFile.GetQuantity(TradingSymbol, Exchange);
                    }
                    if (iseMAAutomation)
                    {
                        value = eMAAutomation.GetQuantity(TradingSymbol, Exchange);
                    }
                    OpenOrderInfo orderInfo = new OpenOrderInfo();
                    orderInfo.Quantity = value;
                    symbolWiseOrderInfo.Add(TradingSymbol, orderInfo);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
                value = 0;
            }
            return value;
        }

        public int GetStopOrderCounter(string TradingSymbol)
        {
            int counter = 0;
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    counter = symbolWiseOrderInfo[TradingSymbol].m_StopOrderCounter;
                }
                else
                {
                    counter = 0;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
                counter = 0;
            }
            return counter;
        }

        public int GetModifyBuyOrderCounter(string TradingSymbol)
        {
            int counter = 0;
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    counter = symbolWiseOrderInfo[TradingSymbol].m_ModifyBuyOrderCounter;
                }
                else
                {
                    counter = 0;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
                counter = 0;
            }
            return counter;
        }

        public int GetModifySellOrderCounter(string TradingSymbol)
        {
            int counter = 0;
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    counter = symbolWiseOrderInfo[TradingSymbol].m_ModifySellOrderCounter;
                }
                else
                {
                    counter = 0;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
                counter = 0;
            }
            return counter;
        }

        public int GetProfitTrailBuyOrderCounter(string TradingSymbol)
        {
            int counter = 0;
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    counter = symbolWiseOrderInfo[TradingSymbol].m_ProfitTrailBuyOrderCounter;
                }
                else
                {
                    counter = 0;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
                counter = 0;
            }
            return counter;
        }

        public int GetProfitTrailSellOrderCounter(string TradingSymbol)
        {
            int counter = 0;
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    counter = symbolWiseOrderInfo[TradingSymbol].m_ProfitTrailSellOrderCounter;
                }
                else
                {
                    counter = 0;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
                counter = 0;
            }
            return counter;
        }

        public int GetMarketOrderCounter(string TradingSymbol)
        {
            int counter = 0;
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    counter = symbolWiseOrderInfo[TradingSymbol].m_MarketOrderCounter;
                }
                else
                {
                    counter = 0;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
                counter = 0;
            }
            return counter;
        }

        public int GetOpenOrderCounter(string TradingSymbol)
        {
            int counter = 0;
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    counter = symbolWiseOrderInfo[TradingSymbol].m_OpenOrderCounter;
                }
                else
                {
                    counter = 0;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
                counter = 0;
            }
            return counter;
        }

        public double GetOpenPriceForProfitTrail(string TradingSymbol)
        {
            double openPrice = 0;
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    openPrice = symbolWiseOrderInfo[TradingSymbol].OpenPriceForProfitTrail;
                }
                else
                {
                    openPrice = 0;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
                openPrice = 0;
            }
            return openPrice;
        }

        public bool GetIsProfitOrderPlaced(string TradingSymbol)
        {
            bool isPlaced = false;
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    isPlaced = symbolWiseOrderInfo[TradingSymbol].IsProfitOrderPlaced;
                }
                else
                {
                    isPlaced = false;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
                isPlaced = false;
            }
            return isPlaced;
        }

        public bool GetIsOrderPlaced(string TradingSymbol)
        {
            bool isPlaced = true;
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    if (symbolWiseOrderInfo[TradingSymbol].BuyOrSell == 0)
                    {
                        isPlaced = false;
                    }
                }
                else
                {
                    isPlaced = true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
                isPlaced = false;
            }
            return isPlaced;
        }

        public int GetBuyOrSell(string TradingSymbol)
        {
            int value = 0;
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    value = symbolWiseOrderInfo[TradingSymbol].BuyOrSell;
                }
                else
                {
                    value = 0;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
                value = 0;
            }
            return value;
        }

        public bool AddOrUpdateBuyOrSell(string TradingSymbol,string TransactionType)
        {
            bool value = false;
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    if (TransactionType == Constants.TRANSACTION_TYPE_BUY) 
                    {
                        symbolWiseOrderInfo[TradingSymbol].BuyOrSell = Math.Abs(symbolWiseOrderInfo[TradingSymbol].BuyOrSell) + 1;
                    }
                    else
                    {
                        symbolWiseOrderInfo[TradingSymbol].BuyOrSell = (Math.Abs(symbolWiseOrderInfo[TradingSymbol].BuyOrSell) + 1) * -1;
                    }
                    value = true;
                }
                else
                {
                    OpenOrderInfo orderInfo = new OpenOrderInfo();
                    if (TransactionType == Constants.TRANSACTION_TYPE_BUY)
                    {
                        orderInfo.BuyOrSell = 1;
                        symbolWiseOrderInfo.Add(TradingSymbol, orderInfo);
                    }
                    else
                    {
                        orderInfo.BuyOrSell = -1;
                        symbolWiseOrderInfo.Add(TradingSymbol, orderInfo);
                    }
                    value = true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
                value = false;
            }
            return value;
        }

        public string GetOpenOrderId(string TradingSymbol, string TransactionType)
        {
            string orderId = "";
            foreach (var orderinfo in symbolWiseOrderInfo)
            {
                OpenOrderInfo order = orderinfo.Value;
                if (orderinfo.Key == TradingSymbol && Constants.TRANSACTION_TYPE_BUY == TransactionType)
                {
                    if (order.BuyStopOrderId != null)
                    {
                        orderId = order.BuyStopOrderId;
                        break;
                    }
                }
                else if(orderinfo.Key == TradingSymbol && Constants.TRANSACTION_TYPE_SELL == TransactionType)
                {
                    if (order.SellStopOrderId != null)
                    {
                        orderId = order.SellStopOrderId;
                        break;
                    }
                }
            }
            return orderId;
        }

        public string GetStopLossOrderId(string TradingSymbol)
        {
            string orderId = "";
            if(symbolWiseOrderInfo.ContainsKey(TradingSymbol))
            {   
                if (symbolWiseOrderInfo[TradingSymbol].StopLossOrderId != null)
                {
                    orderId = symbolWiseOrderInfo[TradingSymbol].StopLossOrderId;
                }                               
            }
            return orderId;
        }

        public string GetMarketOrderId(string TradingSymbol, string TransactionType)
        {
            string orderId = "";
            foreach (var orderinfo in symbolWiseOrderInfo)
            {
                OpenOrderInfo order = orderinfo.Value;
                if (orderinfo.Key == TradingSymbol && Constants.TRANSACTION_TYPE_BUY == TransactionType)
                {
                    if (order.BuyMarketOrderId != null)
                    {
                        orderId = order.BuyMarketOrderId;
                        break;
                    }
                }
                else if (orderinfo.Key == TradingSymbol && Constants.TRANSACTION_TYPE_SELL == TransactionType)
                {
                    if (order.SellMarketOrderId != null)
                    {
                        orderId = order.SellMarketOrderId;
                        break;
                    }
                }
            }
            return orderId;
        }

        public void AddOrUpdateOrderInfo(string TradingSymbol, string TransactionType, string OrderId)
        {
            if  (TransactionType == Constants.TRANSACTION_TYPE_BUY) 
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    symbolWiseOrderInfo[TradingSymbol].BuyStopOrderId = OrderId;
                }
                else
                {
                    OpenOrderInfo openOrderInfo = new OpenOrderInfo();
                    openOrderInfo.BuyStopOrderId = OrderId;
                    symbolWiseOrderInfo.Add(TradingSymbol, openOrderInfo);
                }                
            }
            else if(TransactionType == Constants.TRANSACTION_TYPE_SELL)
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    symbolWiseOrderInfo[TradingSymbol].SellStopOrderId = OrderId;
                }
                else
                {
                    OpenOrderInfo openOrderInfo = new OpenOrderInfo();
                    openOrderInfo.SellStopOrderId = OrderId;
                    symbolWiseOrderInfo.Add(TradingSymbol, openOrderInfo);
                }
            }
            return;
        }

        public void AddOrUpdateStopOrderId(string TradingSymbol,string OrderId)
        {           
            if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
            {
                symbolWiseOrderInfo[TradingSymbol].StopLossOrderId = OrderId;
            }
            else
            {
                OpenOrderInfo openOrderInfo = new OpenOrderInfo();
                openOrderInfo.StopLossOrderId = OrderId;
                symbolWiseOrderInfo.Add(TradingSymbol, openOrderInfo);
            }            
            return;
        }


        public void AddOrUpdateMarketOrderInfo(string TradingSymbol, string TransactionType, string OrderId)
        {
            if (TransactionType == Constants.TRANSACTION_TYPE_BUY)
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    symbolWiseOrderInfo[TradingSymbol].BuyMarketOrderId = OrderId;
                }
                else
                {
                    OpenOrderInfo openOrderInfo = new OpenOrderInfo();
                    openOrderInfo.BuyMarketOrderId = OrderId;
                    symbolWiseOrderInfo.Add(TradingSymbol, openOrderInfo);
                }
            }
            else if (TransactionType == Constants.TRANSACTION_TYPE_SELL)
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    symbolWiseOrderInfo[TradingSymbol].SellMarketOrderId = OrderId;
                }
                else
                {
                    OpenOrderInfo openOrderInfo = new OpenOrderInfo();
                    openOrderInfo.SellMarketOrderId = OrderId;
                    symbolWiseOrderInfo.Add(TradingSymbol, openOrderInfo);
                }
            }
            return;
        }

        public void AddOrUpdateProfitOrder(string TradingSymbol, string TransactionType, string OrderId)
        {
            if (TransactionType == Constants.TRANSACTION_TYPE_BUY)
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    symbolWiseOrderInfo[TradingSymbol].BuyProfitOrderId = OrderId;
                }
                else
                {
                    OpenOrderInfo openOrderInfo = new OpenOrderInfo();
                    openOrderInfo.BuyProfitOrderId = OrderId;
                    symbolWiseOrderInfo.Add(TradingSymbol, openOrderInfo);
                }
            }
            else if (TransactionType == Constants.TRANSACTION_TYPE_SELL)
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    symbolWiseOrderInfo[TradingSymbol].SellProfitOrderId = OrderId;
                }
                else
                {
                    OpenOrderInfo openOrderInfo = new OpenOrderInfo();
                    openOrderInfo.SellProfitOrderId = OrderId;
                    symbolWiseOrderInfo.Add(TradingSymbol, openOrderInfo);
                }
            }
            return;
        }

        public long GetProfitOrderId(string TradingSymbol, string TransactionType)
        {
            long orderId = 0;
            foreach (var orderinfo in symbolWiseOrderInfo)
            {
                OpenOrderInfo order = orderinfo.Value;
                if (orderinfo.Key == TradingSymbol && Constants.TRANSACTION_TYPE_BUY == TransactionType)
                {
                    if (order.BuyProfitOrderId != null)
                    {
                        orderId = Convert.ToInt64(order.BuyProfitOrderId);
                        break;
                    }
                }
                else if (orderinfo.Key == TradingSymbol && Constants.TRANSACTION_TYPE_SELL == TransactionType)
                {
                    if (order.SellProfitOrderId != null)
                    {
                        orderId = Convert.ToInt64(order.SellProfitOrderId);
                        break;
                    }
                }
            }
            return orderId;
        }

        public void ResetOrderId(string TradingSymbol)
        {
            if(symbolWiseOrderInfo.ContainsKey(TradingSymbol))
            {
                symbolWiseOrderInfo[TradingSymbol].PreviousBuyStopOrderId=symbolWiseOrderInfo[TradingSymbol].BuyStopOrderId;
                symbolWiseOrderInfo[TradingSymbol].PreviousSellStopOrderId = symbolWiseOrderInfo[TradingSymbol].SellStopOrderId;
                symbolWiseOrderInfo[TradingSymbol].SellProfitOrderId = null;
                symbolWiseOrderInfo[TradingSymbol].SellStopOrderId = null;
                symbolWiseOrderInfo[TradingSymbol].BuyStopOrderId = null;
                symbolWiseOrderInfo[TradingSymbol].SellMarketOrderId = null;
                symbolWiseOrderInfo[TradingSymbol].BuyMarketOrderId = null;
                symbolWiseOrderInfo[TradingSymbol].BuyProfitOrderId = null;
                symbolWiseOrderInfo[TradingSymbol].BuyOrSell = 0;
                if(m_TotalOpenPositions > 0)
                   m_TotalOpenPositions -= 1;
            }
        }

        public void ResetModifyBUYCounter(string TradingSymbol)
        {
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    symbolWiseOrderInfo[TradingSymbol].m_ModifyBuyOrderCounter = 0;
                }
            }
#pragma warning disable CS0168 // The variable 'e' is declared but never used
            catch(Exception e)
#pragma warning restore CS0168 // The variable 'e' is declared but never used
            {

            }
        }

        public void ResetModifySELLCounter(string TradingSymbol)
        {
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    symbolWiseOrderInfo[TradingSymbol].m_ModifySellOrderCounter = 0;
                }
            }
#pragma warning disable CS0168 // The variable 'e' is declared but never used
            catch (Exception e)
#pragma warning restore CS0168 // The variable 'e' is declared but never used
            {

            }
        }

        public void AddInformationIntoLog(AutomationWithFile automationWithFile)
        {
            try
            {
                foreach(var data in symbolWiseOrderInfo)
                {
                    automationWithFile.WriteUniquelogs("RestoreStructure", "AddInformationIntoLog : ########## "+data.Key+"##########", MessageType.Informational);
                    automationWithFile.WriteUniquelogs("RestoreStructure", "BuyMarketOrderId = " + data.Value.BuyMarketOrderId + " BuyOrSell = " + data.Value.BuyOrSell + " BuyStopOrderId = " + data.Value.BuyStopOrderId, MessageType.Informational);
                    automationWithFile.WriteUniquelogs("RestoreStructure", "m_MarketOrderCounter = " + data.Value.m_MarketOrderCounter + " m_ModifyBuyOrderCounter = " + data.Value.m_ModifyBuyOrderCounter + " m_ModifySellOrderCounter = " + data.Value.m_ModifySellOrderCounter, MessageType.Informational);
                    automationWithFile.WriteUniquelogs("RestoreStructure", "m_OpenOrderCounter = " + data.Value.m_OpenOrderCounter + " m_ProfitTrailBuyOrderCounter = " + data.Value.m_ProfitTrailBuyOrderCounter + " m_ProfitTrailSellOrderCounter = " + data.Value.m_ProfitTrailSellOrderCounter, MessageType.Informational);
                    automationWithFile.WriteUniquelogs("RestoreStructure", "m_StopOrderCounter = " + data.Value.m_StopOrderCounter + " OpenPrice = " + data.Value.OpenPrice + " OpenPriceForProfitTrail = " + data.Value.OpenPriceForProfitTrail + " PreviousBuyStopOrderId = " + data.Value.PreviousBuyStopOrderId + " PreviousSellStopOrderId = " + data.Value.PreviousSellStopOrderId, MessageType.Informational);
                    automationWithFile.WriteUniquelogs("RestoreStructure", "Quantity = " + data.Value.Quantity+ " SellMarketOrderId = "+data.Value.SellMarketOrderId+ " SellProfitOrderId = "+data.Value.SellProfitOrderId+ " SellStopOrderId = "+data.Value.SellStopOrderId , MessageType.Informational);
                    automationWithFile.WriteUniquelogs("RestoreStructure", "AddInformationIntoLog : ####################", MessageType.Informational);
                }
            }
            catch(Exception e)
            {
                automationWithFile.WriteUniquelogs("RestoreStructure", "AddInformationIntoLog : Exception Error Message = "+e.Message, MessageType.Informational);
            }
        }


        public void IncreaseOpenPosition()
        {           
            try
            {
                m_TotalOpenPositions++;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);                
            }
        }

        public int GetOpenpositions()
        {
            try
            {
                return m_TotalOpenPositions;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
            }
            return m_TotalOpenPositions;
        }

        public void AddOrderState(string TradingSymbol,OrderState orderState)
        {
            if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
            {
                symbolWiseOrderInfo[TradingSymbol].m_OrderState = orderState;
            }
            else
            {
                OpenOrderInfo openOrderInfo = new OpenOrderInfo();
                openOrderInfo.m_OrderState = orderState;
                symbolWiseOrderInfo.Add(TradingSymbol, openOrderInfo);
            }
        }

        public string GetOrderState(string TradingSymbol)
        {
            string orderState ="";
            if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
            {
                orderState = symbolWiseOrderInfo[TradingSymbol].m_OrderState.ToString();
            }
            return orderState;
        }


    }
}
