﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using IRDSAlgoOMS;

namespace IRDSStategyExecutorMultiAccount
{   
    public class EMAAutomation
    {
        static string path = Directory.GetCurrentDirectory();
        string iniFile = path + @"\Configuration\" + "EMA_Sandip.ini";
        string FilePath = path + "\\Data\\ZerodhaData\\";
        Dictionary<string, string[]> FileData = new Dictionary<string, string[]>();
        AlgoOMS AlgoOMS;
        ReadSettings readSettings;
        Logger logger;
        string userID;
        int OrderCount = 1;
        int MaxOrderCount = 0;
        int IndividualOrderCount = 0;
        string StartTime = "";
        int Interval = 0;
        string FirstTickEnd = "";
        string StrategyStartTime = "";
        string End = "";
        string TimeForExitAllOpenPosition = "";
        string TimeLimitToPlaceOrder = "";
        double OverallProfit = 0;
        double OverallLoss = 0;
        List<TradingSymbolInfo> ListOfTradingSymbolsInfo;
        Thread automation = null;
#pragma warning disable CS0414 // The field 'EMAAutomation.individualOrderCount' is assigned but its value is never used
        int individualOrderCount = 0;
#pragma warning restore CS0414 // The field 'EMAAutomation.individualOrderCount' is assigned but its value is never used
        form_autoTrading form_Auto;
        List<string> ListForGridView = new List<string>();
        double Percent = 0;
        double TotalMoney = 0;
        double ProfitTrail = 0;
        OpenOrderInfo m_OrderInfo;
#pragma warning disable CS0414 // The field 'EMAAutomation.deepLog' is assigned but its value is never used
        bool deepLog = false;
#pragma warning restore CS0414 // The field 'EMAAutomation.deepLog' is assigned but its value is never used
#pragma warning disable CS0414 // The field 'EMAAutomation.m_Maxperiod' is assigned but its value is never used
        int m_Maxperiod = 7;
#pragma warning restore CS0414 // The field 'EMAAutomation.m_Maxperiod' is assigned but its value is never used
#pragma warning disable CS0414 // The field 'EMAAutomation.m_Minperiod' is assigned but its value is never used
        int m_Minperiod = 3;
#pragma warning restore CS0414 // The field 'EMAAutomation.m_Minperiod' is assigned but its value is never used
#pragma warning disable CS0414 // The field 'EMAAutomation.m_lastMaxEMA' is assigned but its value is never used
        double m_lastMaxEMA = 0;
#pragma warning restore CS0414 // The field 'EMAAutomation.m_lastMaxEMA' is assigned but its value is never used
#pragma warning disable CS0414 // The field 'EMAAutomation.m_lastMinEMA' is assigned but its value is never used
        double m_lastMinEMA = 0;
#pragma warning restore CS0414 // The field 'EMAAutomation.m_lastMinEMA' is assigned but its value is never used
        public EMAAutomation()
        {

        }
        public EMAAutomation(AlgoOMS AlgoOMS, Logger logger, string userID)
        {
            this.AlgoOMS = AlgoOMS;
            this.userID = userID;
            this.logger = logger;
            m_OrderInfo = new OpenOrderInfo(this);
        }
        public void readCSVFile(string path)
        {
            try
            {
                FileData.Clear();
                int idx = path.LastIndexOf('\\');
                string symbol = (path.Substring(idx + 1)).Split('.')[0];
                string[] csvRows = System.IO.File.ReadAllLines(path);
                FileData.Add(symbol, csvRows);
            }
            catch (Exception e)
            {
                logger.LogMessage("Exception from readCSVFile " + e.Message, MessageType.Informational);
            }
        }

        public void getHighLowValues(string TradingSymbol, int barCount, out double high, out double low, out double middle)
        {
            try
            {
                string currentTime = DateTime.Now.ToString("HH:mm:ss");
                int interval = 0;
                double dHigh = 0;
                double dLow = 0;
                string date = DateTime.Now.ToString("M-d-yyyy");
                string Time = "";
                if (FileData.ContainsKey(TradingSymbol))
                {
                    string[] csvRows = FileData[TradingSymbol];
                    for (int i = csvRows.Length - 1; i >= 0; i--)
                    {
                        string row = csvRows[i];
                        if (row.Contains("Date"))
                        {
                            continue;
                        }

                        if (interval == barCount)
                        {
                            break;
                        }
                        string[] columnValues = row.Split(',');
                        string d = columnValues[0];
                        string time = columnValues[1];
                        DateTime datetime = DateTime.Parse(time, System.Globalization.CultureInfo.CurrentCulture);
                        Time = datetime.ToString("HH:mm:ss");
                        if ((TimeSpan.Parse(currentTime) < TimeSpan.Parse(Time)) && (date == d))
                        {
                            continue;
                        }
                        if (dHigh == 0 && dLow == 0)
                        {
                            dHigh = Convert.ToDouble(columnValues[3]);
                            dLow = Convert.ToDouble(columnValues[4]);
                        }

                        if (dHigh < Convert.ToDouble(columnValues[3]))
                        {
                            dHigh = Convert.ToDouble(columnValues[3]);
                        }

                        if (dLow > Convert.ToDouble(columnValues[4]))
                        {
                            dLow = Convert.ToDouble(columnValues[4]);
                        }
                        interval++;
                    }
                }
                high = dHigh;
                low = dLow;
                middle = (dHigh + dLow) / 2;
            }
            catch (Exception e)
            {
                high = 0;
                low = 0;
                middle = 0;
                WriteUniquelogs(TradingSymbol + " " + userID, "getHighLowValues : Exception Error Message =  " + e.Message, MessageType.Exception);
            }

        }

        public void getHighLowValuesFromDB(string TradingSymbol, int barCount, out double high, out double low, out double middle)
        {
            try
            {
                double dHigh = 0;
                double dLow = 0;
                AlgoOMS.GetHighLow(userID, TradingSymbol, barCount, out dHigh, out dLow);
                //WriteUniquelogs(TradingSymbol + " " + userID, "getHighLowValuesFromDB : before round up dHigh " + dHigh + " dLow " + dLow, MessageType.Informational);
                high = AlgoOMS.RoundOff(userID, TradingSymbol, Convert.ToDecimal(dHigh), true);
                low = AlgoOMS.RoundOff(userID, TradingSymbol, Convert.ToDecimal(dLow), false);
                if (dHigh != 0 && dLow != 0)
                {
                    middle = (dHigh + dLow) / 2;
                }
                else
                {
                    WriteUniquelogs(TradingSymbol + " " + userID, "getHighLowValuesFromDB : table not exist " + TradingSymbol, MessageType.Informational);
                    middle = 0;
                }

            }
            catch (Exception e)
            {
                high = 0;
                low = 0;
                middle = 0;
                WriteUniquelogs(TradingSymbol + " " + userID, "getHighLowValuesFRomdb : Exception Error Message =  " + e.Message, MessageType.Exception);
            }
        }

        public void GetCloseValuesFromDB(string TradingSymbol, int barCount,out List<double> listOfCloseValues, out List<string> listOfDateTime)
        {
            List<double> closeValues = new List<double>();
            List<string> dateTime = new List<string>();
            try
            {                
                AlgoOMS.GetCloseValuesList(userID, TradingSymbol, barCount, out closeValues,out dateTime);              
                if(closeValues.Count() == 0)
                {
                    WriteUniquelogs(TradingSymbol + " " + userID, "GetCloseValuesFromDB : Not able to get close values ", MessageType.Informational);
                }
            }
            catch (Exception e)
            {                
                WriteUniquelogs(TradingSymbol + " " + userID, "GetCloseValuesFromDB : Exception Error Message =  " + e.Message, MessageType.Exception);
            }
            listOfCloseValues = closeValues;
            listOfDateTime = dateTime;
        }
        void RunSystemLoop(string TickCurrentTime)
        {
            double dHigh = 0;
            double dLow = 0;
            double dMiddle = 0;
           
           
            foreach (var symbolInfo in ListOfTradingSymbolsInfo) //loop for the trading symbol from ini file
            {
                int Quantity = 0;
                string TradingSymbol = symbolInfo.getSymbol();
                int BarCount = Convert.ToInt32(symbolInfo.getBarCount());
                int MaxPeriodEMA = symbolInfo.getMaxPeriodEMA();
                int MinPeriodEMA = symbolInfo.getMinPeriodEMA();
                double ProfitPercent = symbolInfo.getProfitPercent();
                double StopLoss = symbolInfo.getStopLoss();
                string Exchange = symbolInfo.getExchange();

                string symbol = TradingSymbol;
                if (Exchange == Constants.EXCHANGE_NFO)
                {
                    TradingSymbol = AlgoOMS.GetFutureSymbolName(userID, TradingSymbol);
                }

                //condition for check ignore value for tradingsymbol
                if (m_OrderInfo.GetIgnoreValue(TradingSymbol) == 1)
                {
                    WriteUniquelogs(TradingSymbol + " " + userID, "Automation : ignore for this symbol", MessageType.Informational);
                    continue;
                }

                //get quantity
                Quantity = m_OrderInfo.GetQuantity(TradingSymbol, Exchange);

                

                

                //get open price according to order id
                double openPrice = 0;
#pragma warning disable CS0219 // The variable 'direction' is assigned but its value is never used
                string direction = "";
#pragma warning restore CS0219 // The variable 'direction' is assigned but its value is never used
                string lOrderId = "";
                
                lOrderId = m_OrderInfo.GetMarketOrderId(TradingSymbol, Constants.TRANSACTION_TYPE_BUY);
                if (lOrderId != "")
                {
                    openPrice = AlgoOMS.GetOpenPostionPricebyOrderID(userID, TradingSymbol, Exchange, lOrderId);
                }

                if (openPrice == 0)
                {
                    lOrderId = m_OrderInfo.GetMarketOrderId(TradingSymbol, Constants.TRANSACTION_TYPE_SELL);
                    if (lOrderId != "")
                    {
                        openPrice = AlgoOMS.GetOpenPostionPricebyOrderID(userID, TradingSymbol, Exchange, lOrderId);
                    }
                }

                //get lastprice
                double lastPrice = AlgoOMS.GetLastPrice(userID, TradingSymbol);

                //time condition according to interval i.e. 1min
                if ((TimeSpan.Parse(TickCurrentTime) <= TimeSpan.Parse(TimeLimitToPlaceOrder)))
                {
                    if ((TimeSpan.Parse(TickCurrentTime) >= TimeSpan.Parse(FirstTickEnd)) && (TimeSpan.Parse(TickCurrentTime) < TimeSpan.Parse(End)))
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "LastPrice = "+lastPrice, MessageType.Informational);
                        double dEMAMax = 0;
                        double dEMAMin = 0;
                        if(GetEMAValues(TradingSymbol, BarCount, MaxPeriodEMA, MinPeriodEMA, out dEMAMax, out dEMAMin)) 
                        {
                            if (dEMAMax < dEMAMin && m_OrderInfo.GetBuyOrSell(TradingSymbol) <= 0)
                            {
                                if (openPrice != 0)
                                {
                                    WriteUniquelogs(TradingSymbol + " " + userID, "***** closing previous sell order", MessageType.Informational);
                                    CloseOrder(TradingSymbol, Exchange, lastPrice, dMiddle, openPrice);
                                    Thread.Sleep(1000);
                                }
                                //place buy market order
                                WriteUniquelogs(TradingSymbol + " " + userID, "***** placing buy market order ", MessageType.Informational);
                                placeEntryMarketOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_BUY, Quantity, Convert.ToDecimal(dHigh), Convert.ToDecimal(StopLoss), Convert.ToDecimal(ProfitPercent));

                            }
                            else
                            {
                                //WriteUniquelogs(TradingSymbol + " " + userID, "## Same direction found -ignored ", MessageType.Informational);
                            }
                            if (dEMAMax > dEMAMin && m_OrderInfo.GetBuyOrSell(TradingSymbol) >= 0)
                            {
                                if (openPrice != 0)
                                {
                                    WriteUniquelogs(TradingSymbol + " " + userID, "***** closing previous buy order", MessageType.Informational);
                                    CloseOrder(TradingSymbol, Exchange, lastPrice, dMiddle, openPrice);
                                    Thread.Sleep(1000);
                                }
                                //place sell market order
                                WriteUniquelogs(TradingSymbol + " " + userID, "***** placing sell market order", MessageType.Informational);
                                placeEntryMarketOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_SELL, Quantity, Convert.ToDecimal(dLow), Convert.ToDecimal(StopLoss), Convert.ToDecimal(ProfitPercent));
                            }
                            else
                            {
                               // WriteUniquelogs(TradingSymbol + " " + userID, "## Same direction found -ignored ", MessageType.Informational);
                            }
                        }

                        //30 min - 1week
                        //5min -1 day
                        //10 min -
                        //1 min - 1 day
                        //double ema1 = (40.47 * m_period) / (BarCount) + dSMA * (1.0 - (2.0 / 27.0));
                        //double ema2 = (40.69 * m_period) / (BarCount) + ema1 * (1.0 - (2.0 / 27.0));
                        //double dEMA = EMA(closeValues.ToArray(), m_period);


                        //multiplier = 2 / (length + 1)
                        //ema_ema = (target['ema'] * multiplier) + (previous['ema_ema'] * (1 - multiplier))

                    }
                }

                Thread.Sleep(200);
            }

        }

        private bool GetEMAValues(string TradingSymbol,int BarCount,int MaxPeriodEMA,int MinPeriodEMA,out double dEMAMax, out double dEMAMin)
        {
            bool result = false;
            dEMAMax = 0;
            dEMAMin = 0;
            try
            {
                double dLastEMAMax = m_OrderInfo.GetdMaxEMA(TradingSymbol);
                double dLastEMAMin = m_OrderInfo.GetdMinEMA(TradingSymbol);
                List<double> closeValues = new List<double>();
                List<string> dateTime = new List<string>();
                if (dLastEMAMax == 0 && dLastEMAMin == 0)
                {
                    //condition for way to read data i.e. database or file
                    GetCloseValuesFromDB(TradingSymbol, BarCount, out closeValues,out dateTime);
                    if (closeValues.Count() == 0)
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "GetEMAValues : Not able to fetch latest close list also add in ignore list ", MessageType.Informational);
                        m_OrderInfo.AddIgnoreValue(TradingSymbol, 1);
                    }
                    dEMAMax = EMA(closeValues.ToArray(),dateTime.ToArray(), MaxPeriodEMA);
                    dEMAMin = EMA(closeValues.ToArray(), dateTime.ToArray(), MinPeriodEMA);

                    m_OrderInfo.AddOrUpdatedMaxEMA(TradingSymbol, dEMAMax);
                    m_OrderInfo.AddOrUpdatedMinEMA(TradingSymbol, dEMAMin);
                    result = true;
                    WriteUniquelogs(TradingSymbol + " " + userID, "GetEMAValues : MinEmaValue = " + dEMAMin + " MaxEmaValue = " + dEMAMax, MessageType.Informational);
                }
                else
                {
                    GetCloseValuesFromDB(TradingSymbol, 1, out closeValues,out dateTime);
                    if (closeValues.Count() == 0)
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "GetEMAValues : Not able to fetch latest close list also add in ignore list ", MessageType.Informational);
                        m_OrderInfo.AddIgnoreValue(TradingSymbol, 1);
                    }
                    dEMAMax = EMAOnSingleValue(closeValues.ToArray(), dateTime.ToArray(), dLastEMAMax, MaxPeriodEMA);
                    dEMAMin = EMAOnSingleValue(closeValues.ToArray(), dateTime.ToArray(), dLastEMAMin, MinPeriodEMA);

                    m_OrderInfo.AddOrUpdatedMaxEMA(TradingSymbol, dEMAMax);
                    m_OrderInfo.AddOrUpdatedMinEMA(TradingSymbol, dEMAMin);
                    result = true;
                    WriteUniquelogs(TradingSymbol + " " + userID, "GetEMAValues : MinEmaValue = " + dEMAMin + " MaxEmaValue = " + dEMAMax, MessageType.Informational);                    
                }                
            }
            catch(Exception e)
            {
                result = false;
                WriteUniquelogs(TradingSymbol + " " + userID, "GetEMAValues : Exception Error Message = "+e.Message, MessageType.Informational);
            }
            return result;
        }

        private double EMA(double[] close,string[]dates, int barcount)
        {
            List<double> listEMA = new List<double>();
            double lastEMA = 0;
            double currentEma = 0;
            try
            {
                double sma = SMA(close, barcount);
                double multiplier = 2.0 / (barcount + 1.0);
                for (int i = barcount; i < close.Length - 1; i++)
                {
                    currentEma = 0;
                    if (i == barcount)
                    {
                        sma = SMA(close, barcount);
                    }
                    else if (i == barcount + 1)
                    {
                        currentEma = (close[i - 1] * multiplier) + sma * (1.0 - multiplier);
                        File.AppendAllText("EMALogs.txt", "sma = " + sma + "\n");
                        File.AppendAllText("EMALogs.txt", "close = "+ close[i - 1]+" EMA = "+ currentEma+"\n");
                        listEMA.Add(currentEma);
                        lastEMA = currentEma;
                    }
                    else
                    {
                        currentEma = (close[i - 1] * multiplier) + lastEMA * (1.0 - multiplier);
                        File.AppendAllText("EMALogs.txt", "close = " + close[i - 1] +  " EMA = " + currentEma+"\n");
                        listEMA.Add(currentEma);
                        lastEMA = currentEma;
                    }
                }                
            }
#pragma warning disable CS0168 // The variable 'e' is declared but never used
            catch(Exception e)
#pragma warning restore CS0168 // The variable 'e' is declared but never used
            {

            }
            return lastEMA;
        }

        private double EMAOnSingleValue(double[] close, string[] dates, double lastEMA,int barcount)
        {
            double currentEma = 0;
            double closeValue = 0;
#pragma warning disable CS0219 // The variable 'date' is assigned but its value is never used
            string date = "";
#pragma warning restore CS0219 // The variable 'date' is assigned but its value is never used
            try
            {
                if(close.Length == 1)
                {
                    closeValue = close[0];
                    
                    double multiplier = 2.0 / (barcount + 1.0);
                    currentEma = (closeValue * multiplier) + lastEMA * (1.0 - multiplier);
                    File.AppendAllText("EMALogs.txt", "close = " + closeValue +  " EMA = " + currentEma+"\n");
                }               
            }
#pragma warning disable CS0168 // The variable 'e' is declared but never used
            catch (Exception e)
#pragma warning restore CS0168 // The variable 'e' is declared but never used
            {

            }
            return currentEma;
        }


        private double SMA(double[] close, int period)
        {
            double sma = 0;
            double sum = 0;
            for (int i = period - 1; i >= 0; i--)
            {
                if (i < close.Length - 1)
                {
                    sum = sum + close[i];
                }
            }
            if (period != 0)
            {
                sma = (sum) / period;
            }
            return sma;
        }

        public bool CheckForPendingStopOrder(string TradingSymbol, string Exchange)
        {
            if (m_OrderInfo.GetBuyOrSell(TradingSymbol) > 0 && AlgoOMS.GetStopPricebyOrderID(userID, TradingSymbol, Exchange, m_OrderInfo.GetOpenOrderId(TradingSymbol, Constants.TRANSACTION_TYPE_BUY)) != 0)
            {
                if (AlgoOMS.GetOpenPostionPricebyOrderID(userID, TradingSymbol, Exchange, m_OrderInfo.GetOpenOrderId(TradingSymbol, Constants.TRANSACTION_TYPE_SELL)) == 0)
                {
                    return false;
                }
            }
            else if (m_OrderInfo.GetBuyOrSell(TradingSymbol) < 0 && AlgoOMS.GetStopPricebyOrderID(userID, TradingSymbol, Exchange, m_OrderInfo.GetOpenOrderId(TradingSymbol, Constants.TRANSACTION_TYPE_SELL)) != 0)
            {
                if (AlgoOMS.GetOpenPostionPricebyOrderID(userID, TradingSymbol, Exchange, m_OrderInfo.GetOpenOrderId(TradingSymbol, Constants.TRANSACTION_TYPE_BUY)) == 0)
                {
                    return false;
                }
            }
            return true;
        }

        public bool CheckForOpenPosition(string TradingSymbol, string Exchange)
        {
            //checking position open or not                     
            if (!AlgoOMS.IsPositionOpen(userID, TradingSymbol, Exchange))
            {
                Thread.Sleep(2000);
                //wait for updating order from server to local array
                if (!AlgoOMS.IsPositionOpen(userID, TradingSymbol, Exchange))
                {
                    if (AlgoOMS.IsPendingStopOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_BUY))
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "CheckForOpenPosition : buy pending order", MessageType.Informational);
                    }
                    else if (AlgoOMS.IsPendingStopOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_SELL))
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "CheckForOpenPosition : sell pending order", MessageType.Informational);
                    }
                    WriteUniquelogs(TradingSymbol + " " + userID, "CheckForOpenPosition : Reset order id", MessageType.Informational);
                    m_OrderInfo.ResetOrderId(TradingSymbol);
                    return false;
                }
            }
            return true;
        }


        public void ExecuteSyncLogic()
        {
            bool isStopThread = false;
            loadINIValues();
            CheckTradingSymbolPresentOrNot();

            while (true)
            {
                Thread.Sleep(1000);
                try
                {
                    string TickCurrentTime = AlgoOMS.getTickCurrentTime();
                    //condition for TickCurrentTime 
                    if (TickCurrentTime == "")
                    {
                        continue;
                    }
                    //condition for calculateProfit 
                    if (calculateProfit())
                    {
                        isStopThread = true;
                        StopThread();
                        break;
                    }

                    //condition for exit all orders after 3:10
                    if (TimeSpan.Parse(TickCurrentTime) >= TimeSpan.Parse(TimeForExitAllOpenPosition))
                    {
                        logger.LogMessage("ExecuteSyncLogic : condition true for close all position", MessageType.Informational);
                        CloseAllOrder();
                    }
                    else
                    {
                        //condtion for total maxcount
                        if (OrderCount <= MaxOrderCount)
                        {
                            RunSystemLoop(TickCurrentTime);
                        }
                        else
                        {
                            logger.LogMessage("ExecuteSyncLogic :Maxcount reached ordercount " + OrderCount, MessageType.Informational);
                        }
                        changeTime(TickCurrentTime);

                    }
                }
                catch (Exception e)
                {
                    logger.LogMessage("ExecuteSyncLogic :Exception Error Message =  " + e.Message, MessageType.Exception);
                }
                finally
                {
                    if (isStopThread) //condition for stop automation if calculate profit condition is true
                    {
                        form_Auto.isChanbreakStopped = true;
                        form_Auto.enableButton();
                    }
                }
            }
        }

        //modifying order after 1min  (means both orders are pending)
        public void ModifyOrderAfterOneMin(string TradingSymbol, string Exchange, double dHigh, double dLow, double openPrice, double lastPrice)
        {
            if (AlgoOMS.IsPendingStopOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_BUY) == true)
            {
                double stopPrice = AlgoOMS.GetStopPrice(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_BUY);
                if (stopPrice > dHigh)
                {
                    //if (m_OrderInfo.GetIsStopOrderPlacedValue(TradingSymbol))
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "##### ModifyOrderAfterOneMin :openPrice   " + openPrice + " and lastPrice " + lastPrice, MessageType.Informational);
                        WriteUniquelogs(TradingSymbol + " " + userID, "stopPrice   " + stopPrice + " and dHigh " + dHigh, MessageType.Informational);
                        if (AlgoOMS.ModifyStopOrder(userID, TradingSymbol, Constants.TRANSACTION_TYPE_BUY, Convert.ToDecimal(dHigh)))
                        {
                            WriteUniquelogs(TradingSymbol + " " + userID, "##### ModifyOrderAfterOneMin :Modified buy order\n", MessageType.Informational);
                        }
                        ////loadOpenPositions();
                    }

                }
                else
                {
                    WriteUniquelogs(TradingSymbol + " " + userID, "##### ModifyOrderAfterOneMin :stop price and high are same \n", MessageType.Informational);
                }
            }
            else
            {
                //WriteUniquelogs(TradingSymbol + " " + userID, "##### ModifyOrderAfterOneMin :pending order not found ", MessageType.Informational);
            }

            //modify open  sell order
            if (AlgoOMS.IsPendingStopOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_SELL) == true)
            {
                double stopPrice = AlgoOMS.GetStopPrice(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_SELL);
                if (stopPrice < dLow)
                {
                    //if (m_OrderInfo.GetIsStopOrderPlacedValue(TradingSymbol))
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "##### ModifyOrderAfterOneMin :openPrice   " + openPrice + " and lastPrice " + lastPrice, MessageType.Informational);
                        WriteUniquelogs(TradingSymbol + " " + userID, "ModifyOrderAfterOneMin :stopPrice   " + stopPrice + " and dLow " + dLow, MessageType.Informational);
                        if (AlgoOMS.ModifyStopOrder(userID, TradingSymbol, Constants.TRANSACTION_TYPE_SELL, Convert.ToDecimal(dLow)))
                        {
                            WriteUniquelogs(TradingSymbol + " " + userID, "##### ModifyOrderAfterOneMin :Modified sell order\n", MessageType.Informational);
                        }
                        ////loadOpenPositions();
                    }
                }
                else
                {
                    WriteUniquelogs(TradingSymbol + " " + userID, "##### ModifyOrderAfterOneMin :stop price and low are same\n ", MessageType.Informational);
                }
            }
            else
            {
                //WriteUniquelogs(TradingSymbol + " " + userID, "##### ModifyOrderAfterOneMin :pending order not found ", MessageType.Informational);
            }

        }

        //place stop order 
        public void placeEntryStopOrder(string userID, string TradingSymbol, string Exchange, string TransactionType, int Quantity, decimal Price, decimal StopLoss, decimal ProfitPercent)
        {
            double lastPrice = AlgoOMS.GetLastPrice(userID, TradingSymbol);
            string orderId = "NA";
            int counter = m_OrderInfo.GetStopOrderCounter(TradingSymbol);
            if (counter < IndividualOrderCount)
            {
                if (TransactionType == Constants.TRANSACTION_TYPE_SELL)
                {
                    //place stop order
                    WriteUniquelogs(TradingSymbol + " " + userID, "placeEntryStopOrder : placed sell Stoploss order", MessageType.Informational);
                    orderId = AlgoOMS.PlaceStopOrder(userID, Exchange, TradingSymbol, TransactionType, Quantity, 0, Price);
                    if (orderId == "ErrorMessageForStopOrder")
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "placeEntryStopOrder : Exception error occure for trigger price. Placing Market order", MessageType.Informational);
                        orderId = AlgoOMS.PlaceMarketOrder(userID, Exchange, TradingSymbol, TransactionType, Quantity);
                    }
                }
                else if (TransactionType == Constants.TRANSACTION_TYPE_BUY)
                {
                    //place stop order
                    WriteUniquelogs(TradingSymbol + " " + userID, "placeEntryStopOrder : placed buy Stoploss order", MessageType.Informational);
                    orderId = AlgoOMS.PlaceStopOrder(userID, Exchange, TradingSymbol, TransactionType, Quantity, 0, Price);
                    if (orderId == "ErrorMessageForStopOrder")
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "placeEntryStopOrder : Exception error occure for trigger price. Placing Market order", MessageType.Informational);
                        orderId = AlgoOMS.PlaceMarketOrder(userID, Exchange, TradingSymbol, TransactionType, Quantity);
                    }
                }

                if (orderId != "NA")
                {
                    OrderCount++;
                    m_OrderInfo.StopOrderCounter(TradingSymbol);//increament individual counter
                    m_OrderInfo.AddOrUpdateOrderInfo(TradingSymbol, TransactionType, orderId);//added order id into storage
                    WriteUniquelogs(TradingSymbol + " " + userID, "placeEntryStopOrder : placed successfully with orderId = " + orderId + "\n", MessageType.Informational);
                }
                else
                {
                    WriteUniquelogs(TradingSymbol + " " + userID, "placeEntryStopOrder : Not placed\n", MessageType.Informational);
                }
            }
            else
            {
                WriteUniquelogs(TradingSymbol + " " + userID, "placeEntryStopOrder : individualCount condition false counter = " + counter + "\n", MessageType.Informational);
            }
        }

        //place market order 
        public void placeEntryMarketOrder(string userID, string TradingSymbol, string Exchange, string TransactionType, int Quantity, decimal Price, decimal StopLoss, decimal ProfitPercent)
        {
            double lastPrice = AlgoOMS.GetLastPrice(userID, TradingSymbol);
            string orderId = "NA";
            int counter = m_OrderInfo.GetMarketOrderCounter(TradingSymbol);
            if (counter < IndividualOrderCount)
            {
                if (TransactionType == Constants.TRANSACTION_TYPE_SELL)
                {
                    //place market order
                    orderId = AlgoOMS.PlaceMarketOrder(userID, Exchange, TradingSymbol, TransactionType, Quantity);
                   
                }
                else if (TransactionType == Constants.TRANSACTION_TYPE_BUY)
                {
                    //place market order 
                    orderId = AlgoOMS.PlaceMarketOrder(userID, Exchange, TradingSymbol, TransactionType, Quantity);
                    
                }

                if (orderId != "NA")
                {
                    OrderCount++;
                    m_OrderInfo.MarketOrderCounter(TradingSymbol);//increament individual counter
                    m_OrderInfo.AddOrUpdateMarketOrderInfo(TradingSymbol, TransactionType, orderId);//added order id into storage
                    m_OrderInfo.AddOrUpdateBuyOrSell(TradingSymbol, TransactionType);
                    WriteUniquelogs(TradingSymbol + " " + userID, "placeEntryMarketOrder : placed successfully with orderId = " + orderId + "\n", MessageType.Informational);
                }
                else
                {
                    WriteUniquelogs(TradingSymbol + " " + userID, "placeEntryMarketOrder : Not placed\n", MessageType.Informational);
                }
            }
            else
            {
                WriteUniquelogs(TradingSymbol + " " + userID, "placeEntryMarketOrder : individualCount condition false counter = " + counter + "\n", MessageType.Informational);
            }
        }


        //change stoploss(trigger price)
        public void ChangeStopLossPrice(string TradingSymbol, string Exchange, double lastPrice, double openPrice, double StopLoss)
        {
            if (m_OrderInfo.GetIsOrderPlaced(TradingSymbol))
            {
                //checking for open buy order
                if (lastPrice > openPrice && AlgoOMS.IsPendingStopOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_SELL) == true)//check for pending order
                {
                    double lastopenPrice = m_OrderInfo.GetOpenPriceForProfitTrail(TradingSymbol);
                    if (lastopenPrice != 0)
                    {
                        openPrice = lastopenPrice;
                    }
                    if (openPrice != 0)//check for open price
                    {
                        double percentCalculatedForOpenPrice = (openPrice * ProfitTrail) / 100;
                        double ProfitTrailValue = percentCalculatedForOpenPrice + openPrice;
                        if (lastPrice >= (ProfitTrailValue))
                        {
                            WriteUniquelogs(TradingSymbol + " " + userID, " %%%%% ChangeStopLossPrice : ProfitTrailValue = " + ProfitTrailValue, MessageType.Informational);
                            decimal calculatedPrice = 0;
                            double percentStopLossValue = StopLoss * lastPrice / 100;
                            double calStopLossPrice = lastPrice - percentStopLossValue;
                            calculatedPrice = Convert.ToDecimal(AlgoOMS.RoundOff(userID, TradingSymbol, Convert.ToDecimal(calStopLossPrice), true));
                            WriteUniquelogs(TradingSymbol + " " + userID, "lastPrice " + lastPrice + " openprice " + openPrice, MessageType.Informational);
                            WriteUniquelogs(TradingSymbol + " " + userID, "New stoploss value " + percentStopLossValue + " calStopLossPrice " + calStopLossPrice, MessageType.Informational);
                            WriteUniquelogs(TradingSymbol + " " + userID, "Trigger Price = " + calculatedPrice, MessageType.Informational);
                            WriteUniquelogs(TradingSymbol + " " + userID, "%%%%% ChangeStopLossPrice : Change stop price - profittrail condition true for sell order \n", MessageType.Informational);
                            AlgoOMS.ModifyStopOrder(userID, TradingSymbol, Constants.TRANSACTION_TYPE_SELL, Convert.ToDecimal(calculatedPrice));
                            m_OrderInfo.AddOpenPriceForProfitTrail(TradingSymbol, lastPrice);
                        }
                    }
                    else
                    {
                        //WriteUniquelogs(TradingSymbol + " " + userID, "%%%%% ChangeStopLossPrice :open price is 0\n", MessageType.Informational);
                    }
                }
                //checking for open buy order
                if (lastPrice < openPrice && AlgoOMS.IsPendingStopOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_BUY) == true)//check for pending order
                {
                    double lastopenPrice = m_OrderInfo.GetOpenPriceForProfitTrail(TradingSymbol);
                    if (lastopenPrice != 0)
                    {
                        openPrice = lastopenPrice;
                    }
                    if (openPrice != 0) //check for open price
                    {
                        double percentCalculatedForOpenPrice = (openPrice * ProfitTrail) / 100;
                        double ProfitTrailValue = openPrice - percentCalculatedForOpenPrice;
                        if (lastPrice <= (ProfitTrailValue))
                        {
                            WriteUniquelogs(TradingSymbol + " " + userID, " %%%%% ChangeStopLossPrice : ProfitTrailValue = " + ProfitTrailValue, MessageType.Informational);
                            decimal calculatedPrice = 0;
                            double percentStopLossValue = StopLoss * lastPrice / 100;
                            double calStopLossPrice = lastPrice + percentStopLossValue;
                            calculatedPrice = Convert.ToDecimal(AlgoOMS.RoundOff(userID, TradingSymbol, Convert.ToDecimal(calStopLossPrice), false));
                            WriteUniquelogs(TradingSymbol + " " + userID, "lastPrice " + lastPrice + " openprice " + openPrice, MessageType.Informational);
                            WriteUniquelogs(TradingSymbol + " " + userID, "New stoploss value " + percentStopLossValue + " calStopLossPrice " + calStopLossPrice, MessageType.Informational);
                            WriteUniquelogs(TradingSymbol + " " + userID, "Trigger Price = " + calculatedPrice, MessageType.Informational);
                            WriteUniquelogs(TradingSymbol + " " + userID, "%%%%% ChangeStopLossPrice : Change stop price - profittrail condition true for buy order\n", MessageType.Informational);
                            AlgoOMS.ModifyStopOrder(userID, TradingSymbol, Constants.TRANSACTION_TYPE_BUY, Convert.ToDecimal(calculatedPrice));
                            m_OrderInfo.AddOpenPriceForProfitTrail(TradingSymbol, lastPrice);
                        }
                    }
                    else
                    {
                        //WriteUniquelogs(TradingSymbol + " " + userID, "%%%%% ChangeStopLossPrice :open price is 0", MessageType.Informational);
                    }
                }

            }
        }

        //close order
        public void CloseOrder(string TradingSymbol, string Exchange, double lastPrice, double dMiddle, double openPrice)
        {            
            WriteUniquelogs(TradingSymbol + " " + userID, "$$$$$ Automation :condition true for close ", MessageType.Informational);
            AlgoOMS.CloseOrder(userID, TradingSymbol, Exchange);
            WriteUniquelogs(TradingSymbol + " " + userID, "\n", MessageType.Informational);                
        }

        //modify order to place stoploss order to executed order
        public void ModifyOrderForPlacingStopLossOrder(string TradingSymbol, string Exchange, double openPrice, double lastPrice, double StopLoss)
        {
            //modify order after executing order
            if ((AlgoOMS.IsPendingStopOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_BUY) == true) && (AlgoOMS.IsPendingStopOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_SELL)) == false)
            {
                if (openPrice != 0 && (!m_OrderInfo.GetIsOrderPlaced(TradingSymbol)))//check for openprice and need to modify or not
                {
                    WriteUniquelogs(TradingSymbol + " " + userID, "@@@@@ ModifyOrderForPlacingStopLossOrder : openPrice " + openPrice, MessageType.Informational);
                    decimal calculatedPrice = 0;
                    double PercentValue = StopLoss * openPrice / 100;
                    double CalPrice = openPrice + PercentValue;
                    calculatedPrice = Convert.ToDecimal(AlgoOMS.RoundOff(userID, TradingSymbol, Convert.ToDecimal(CalPrice), true));
                    WriteUniquelogs(TradingSymbol + " " + userID, "PercentValue " + PercentValue + " CalPrice = " + CalPrice + " Trigger Price = " + calculatedPrice, MessageType.Informational);
                    if (AlgoOMS.ModifyStopOrder(userID, TradingSymbol, Constants.TRANSACTION_TYPE_BUY, Convert.ToDecimal(calculatedPrice)))
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "@@@@@ ModifyOrderForPlacingStopLossOrder : Modified buy order\n", MessageType.Informational);
                    }
                    else
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "@@@@@ ModifyOrderForPlacingStopLossOrder : Not able to modify buy order\n", MessageType.Informational);
                    }
                    m_OrderInfo.AddOrUpdateBuyOrSell(TradingSymbol, Constants.TRANSACTION_TYPE_SELL);
                    Thread.Sleep(2000);
                }
            }

            //modify order after executing order - SELL
            if ((AlgoOMS.IsPendingStopOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_BUY) == false) && (AlgoOMS.IsPendingStopOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_SELL)) == true)
            {
                if (openPrice != 0 && (!m_OrderInfo.GetIsOrderPlaced(TradingSymbol)))//check for openprice and need to modify or not
                {
                    WriteUniquelogs(TradingSymbol + " " + userID, "@@@@@ ModifyOrderForPlacingStopLossOrder :openPrice " + openPrice, MessageType.Informational);
                    decimal calculatedPrice = 0;
                    double PercentValue = StopLoss * openPrice / 100;
                    double CalPrice = openPrice - PercentValue;
                    calculatedPrice = Convert.ToDecimal(AlgoOMS.RoundOff(userID, TradingSymbol, Convert.ToDecimal(CalPrice), false));
                    WriteUniquelogs(TradingSymbol + " " + userID, "PercentValue " + PercentValue + " CalPrice = " + CalPrice + " Trigger Price = " + calculatedPrice, MessageType.Informational);

                    if (AlgoOMS.ModifyStopOrder(userID, TradingSymbol, Constants.TRANSACTION_TYPE_SELL, Convert.ToDecimal(calculatedPrice)))
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "@@@@@ ModifyOrderForPlacingStopLossOrder : Modified sell order\n", MessageType.Informational);
                    }
                    else
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "@@@@@ ModifyOrderForPlacingStopLossOrder : Not able to modify sell order\n", MessageType.Informational);
                    }
                    m_OrderInfo.AddOrUpdateBuyOrSell(TradingSymbol, Constants.TRANSACTION_TYPE_BUY);
                    Thread.Sleep(2000);
                }
            }
        }


        //place stoploss order opposite to executed order if not present
        public void PlaceStopOrderIfNotPresent(string TradingSymbol, string Exchange, double openPrice, double lastPrice, double StopLoss, int Quantity)
        {
            //condition for stop order counter
            int counter = m_OrderInfo.GetStopOrderCounter(TradingSymbol);
            if (counter < IndividualOrderCount)
            {
                string orderId = "NA";
                string transactionType = AlgoOMS.GetTransactionType(userID, TradingSymbol, Exchange);
                if (transactionType == Constants.TRANSACTION_TYPE_BUY)//condition for transaction type
                {
                    if (CheckForPendingStopOrder(TradingSymbol, Exchange))
                    {
                        if (AlgoOMS.IsPendingStopOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_SELL) == false)//condition to check pending order
                        {
                            WriteUniquelogs(TradingSymbol + " " + userID, ">>>>> PlaceStopOrderIfNotPresent :openPrice " + openPrice, MessageType.Informational);
                            decimal calculatedPrice = 0;
                            double PercentValue = StopLoss * openPrice / 100;
                            double CalPrice = openPrice - PercentValue;
                            calculatedPrice = Convert.ToDecimal(AlgoOMS.RoundOff(userID, TradingSymbol, Convert.ToDecimal(CalPrice), false));
                            WriteUniquelogs(TradingSymbol + " " + userID, "PercentValue " + PercentValue + " CalPrice = " + CalPrice + " Trigger Price = " + calculatedPrice, MessageType.Informational);
                            WriteUniquelogs(TradingSymbol + " " + userID, ">>>>> PlaceStopOrderIfNotPresent : Placed sell Stop Order because opposite stop order not present\n", MessageType.Informational);
                            orderId = AlgoOMS.PlaceStopOrder(userID, Exchange, TradingSymbol, Constants.TRANSACTION_TYPE_SELL, Quantity, 0, Convert.ToDecimal(calculatedPrice));
                        }
                    }
                    else
                    {
                        //WriteUniquelogs(TradingSymbol + " " + userID, "***** PlaceStopOrderIfNotPresent :openPrice is not greater than zero", MessageType.Informational);
                    }
                }
                else if (transactionType == Constants.TRANSACTION_TYPE_SELL)//condition for transaction type
                {
                    if (CheckForPendingStopOrder(TradingSymbol, Exchange))
                    {
                        if (AlgoOMS.IsPendingStopOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_BUY) == false)//condition to check pending order
                        {
                            WriteUniquelogs(TradingSymbol + " " + userID, ">>>>> PlaceStopOrderIfNotPresent :openPrice " + openPrice, MessageType.Informational);
                            decimal calculatedPrice = 0;
                            double PercentValue = StopLoss * openPrice / 100;
                            double CalPrice = openPrice + PercentValue;
                            calculatedPrice = Convert.ToDecimal(AlgoOMS.RoundOff(userID, TradingSymbol, Convert.ToDecimal(CalPrice), false));
                            WriteUniquelogs(TradingSymbol + " " + userID, "PercentValue" + PercentValue + " CalPrice " + CalPrice + " Trigger Price = " + calculatedPrice, MessageType.Informational);
                            WriteUniquelogs(TradingSymbol + " " + userID, ">>>>> PlaceStopOrderIfNotPresent : Placed buy Stop Order because opposite stop order not present\n", MessageType.Informational);
                            orderId = AlgoOMS.PlaceStopOrder(userID, Exchange, TradingSymbol, Constants.TRANSACTION_TYPE_BUY, Quantity, 0, Convert.ToDecimal(calculatedPrice));
                        }
                    }
                    else
                    {
                        //WriteUniquelogs(TradingSymbol + " " + userID, "***** PlaceStopOrderIfNotPresent :openPrice is not greater than zero", MessageType.Informational);
                    }
                }
                if (orderId != "NA")
                {
                    OrderCount++;
                    m_OrderInfo.StopOrderCounter(TradingSymbol);
                    WriteUniquelogs(TradingSymbol + " " + userID, ">>>>> PlaceStopOrderIfNotPresent : placed successfully with orderId = " + orderId + "\n", MessageType.Informational);
                }
                else
                {
                    //WriteUniquelogs(TradingSymbol + " " + userID, "***** placeStopOrder : Not placed", MessageType.Informational);
                }

            }
            else
            {
                //WriteUniquelogs(TradingSymbol + " " + userID, ">>>>> PlaceStopOrderIfNotPresent : individualCount condition false counter = " + counter, MessageType.Informational);
            }
        }

        //load values from ini file
        public void loadINIValues()
        {
            try
            {
                string currentTime = DateTime.Now.ToString("HH:mm") + ":00";
                MaxOrderCount = Convert.ToInt32(readSettings.maxCount);
                IndividualOrderCount = Convert.ToInt32(readSettings.individualCount);
                StartTime = readSettings.startTime;
                OverallLoss = Convert.ToDouble(readSettings.overallLoss);
                OverallProfit = Convert.ToDouble(readSettings.overallProfit);
                Percent = readSettings.Percent;
                TotalMoney = readSettings.CommanLotSize;
                ProfitTrail = readSettings.ProfitTrail;
                Interval = Convert.ToInt32(readSettings.interval);
                if (TimeSpan.Parse(StartTime) < TimeSpan.Parse(currentTime))
                {
                    StartTime = currentTime;
                }
                DateTime dt1 = DateTime.ParseExact(StartTime, "HH:mm:ss", null);
                StartTime = dt1.AddSeconds(60 * (Interval * -1)).ToString("HH:mm:ss");
                DateTime dt = DateTime.ParseExact(StartTime, "HH:mm:ss", null);
                FirstTickEnd = dt.AddSeconds(60 * Interval).ToString("HH:mm:ss");
                StrategyStartTime = FirstTickEnd;
                End = dt.AddSeconds(60 * (Interval + 1)).ToString("HH:mm:ss");
                TimeForExitAllOpenPosition = readSettings.TimeForExitAllOpenPosition;
                TimeLimitToPlaceOrder = readSettings.TimeLimitToPlaceOrder;
                ListOfTradingSymbolsInfo = readSettings.TradingSymbolsInfoList;
            }
            catch (Exception e)
            {
                logger.LogMessage("loadINIValues - Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        //function to start thread
        public bool startThread(string userID)
        {
            readSettings = new ReadSettings(this.logger,AlgoOMS);
            bool res = readSettings.readEMA_SandipConfigFile(iniFile);
            if (!res)
            {
                return res;
            }
            loadINIValues();
            if (automation == null)
            {
                logger.LogMessage("Strating StartAutomation thread", MessageType.Informational);
                automation = new Thread(() => ExecuteSyncLogic());
                automation.Start();
            }
            return true;
        }

        //add symbols to inticker 
        public void CheckTradingSymbolPresentOrNot()
        {
            try
            {
                AlgoOMS.CheckTradingSymbolPresentOrNot(userID, readSettings.TradingsymbolList);
            }
            catch (Exception e)
            {
                logger.LogMessage("CheckTradingSymbolPresentOrNot : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }


        //function to calculateProfit 
        public bool calculateProfit()
        {
            try
            {
                PositionResponse positionResponseForOverall = AlgoOMS.GetOpenPosition(userID);
                if (positionResponseForOverall.Net.Count != 0)
                {
                    double overallProfit = OverallProfit;
                    double overallLoss = OverallLoss;
                    double profitLoss = 0;
                    foreach (var symbol in ListOfTradingSymbolsInfo)
                    {
                        double lastPrice = 0;
                        string tradingSymbol = symbol.getSymbol();
                        string exchange = symbol.getExchange();
                        if (exchange == "NFO")
                        {
                            tradingSymbol = AlgoOMS.GetFutureSymbolName(userID, tradingSymbol);
                        }
                        double OpenPrice = AlgoOMS.GetOpenPostionPrice(userID, tradingSymbol);
                        if (OpenPrice != 0)
                        {
                            lastPrice = AlgoOMS.GetLastPrice(userID, tradingSymbol);
                            int quantity = m_OrderInfo.GetQuantity(tradingSymbol, exchange);//symbol.getQuantity();
                            logger.LogMessage(tradingSymbol + "calculateProfit : OpenPrice " + OpenPrice + " lastPrice " + lastPrice + " quantity " + quantity, MessageType.Informational);
                            int value = m_OrderInfo.GetBuyOrSell(tradingSymbol);
                            double profitOrLoss = 0; ;
                            if (value > 0)
                            {
                                profitOrLoss = (lastPrice - OpenPrice) * quantity;
                            }
                            else if (value < 0)
                            {
                                profitOrLoss = (OpenPrice - lastPrice) * quantity;
                            }
                            logger.LogMessage(tradingSymbol + "calculateProfit : profitOrLoss " + profitOrLoss, MessageType.Informational);
                            profitLoss += profitOrLoss;
                        }
                        else
                        {
                            //logger.LogMessage("calculateProfit : OpenPrice is zero for " + tradingSymbol, MessageType.Informational);
                        }
                    }

                    if (profitLoss > overallProfit || profitLoss < overallLoss)
                    {
                        //logger.LogMessage("calculateProfit : Condition true for closeallpostion", MessageType.Informational);
                        logger.LogMessage("calculateProfit : profitLoss " + profitLoss + " overallProfit " + overallProfit + " overallLoss " + overallLoss + "-- Condition true", MessageType.Informational);
                        foreach (var symbol in ListOfTradingSymbolsInfo)
                        {
                            string tradingSymbol = symbol.getSymbol();
                            string exchange = symbol.getExchange();
                            if (exchange == "NFO")
                            {
                                tradingSymbol = AlgoOMS.GetFutureSymbolName(userID, tradingSymbol);
                            }
                            AlgoOMS.CloseOrder(userID, tradingSymbol, exchange);
                        }
                        logger.LogMessage("calculateProfit : closed all position successfully", MessageType.Informational);
                        return true;
                    }
                    else
                    {
                        //logger.LogMessage("calculateProfit : Condition false for closeallpostion", MessageType.Informational);
                        //logger.LogMessage("calculateProfit :  profitLoss " + profitLoss + " overallProfit " + overallProfit + " overallLoss " + overallLoss + "-- Condition false", MessageType.Informational);
                        return false;
                    }
                }
            }
            catch (Exception e)
            {
                logger.LogMessage("calculateProfit : Exception Error Message =  " + e.Message, MessageType.Exception);
            }
            return false;
        }

        //function to stop thread
        public bool StopThread()
        {
            // if (AlgoOMS.stopThread())
            {
                if (automation != null)
                {
                    if (automation.IsAlive)
                    {
                        logger.LogMessage("Stopped Thread", MessageType.Informational);
                        automation.Abort();
                        Thread.Sleep(1000);
                        if (!automation.IsAlive)
                            return true;
                    }
                }
            }
            return true;
        }

        //function to copy object of form_Auto
        public void loadObject(form_autoTrading form_Auto)
        {
            this.form_Auto = form_Auto;
        }

        //function for write logs
        public void WriteUniquelogs(string symbol, string message, MessageType msgType)
        {
            try
            {
                SymbolLogs log = new SymbolLogs();
                //log = SymbolLogs.InstanceSymbolLogs;
                log.createFile(symbol);
                log.LogMessage(message, MessageType.Informational);
                log = null;
            }
            catch (Exception e)
            {
                Console.WriteLine("WriteUniquelogs : Exception : " + e.Message);
            }
        }

        ////function to add symbols in gridview(gui)
        //public void addIntoDataGridView(string TradingSymbol, string TransactionType, string Quantity, string Price)
        //{
        //    try
        //    {
        //        mydt.Rows.Add(new object[] { TradingSymbol, TransactionType, Quantity, Price });
        //        ListForGridView.Add(TradingSymbol);
        //    }
        //    catch (Exception e)
        //    {
        //        WriteUniquelogs(TradingSymbol + " " + userID, "addIntoDataGridView : Exception Error Message = " + e.Message, MessageType.Exception);
        //    }
        //}

        ////function to update values from gridview(gui)
        //public void updateIntoDataGridView(string TradingSymbol, string TransactionType, string Quantity, string Price)
        //{
        //    try
        //    {
        //        foreach (DataRow dr in mydt.Rows)
        //        {
        //            if ((dr["Symbol"]).ToString() == TradingSymbol)
        //            {
        //                WriteUniquelogs(TradingSymbol + " " + userID, "updateIntoDataGridView : Found symbol " + TradingSymbol, MessageType.Informational);
        //                dr["Transaction Type"] = TransactionType;
        //                dr["Quantity"] = Quantity;
        //                dr["Price"] = Price;
        //                break;
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        WriteUniquelogs(TradingSymbol + " " + userID, "updateIntoDataGridView : Exception Error Message = " + e.Message, MessageType.Exception);
        //    }
        //}

        //add or update symbols with there values on gui
        //public void loadOpenPositions(string user = "", AlgoOMS AlgoOMS = null, Logger logger = null)
        //{
        //    if (user != "")
        //        userID = user;
        //    if (AlgoOMS != null)
        //        this.AlgoOMS = AlgoOMS;
        //    if (logger != null)
        //        this.logger = logger;
        //    try
        //    {
        //        PositionResponse positionResponseForOverall = this.AlgoOMS.GetOpenPosition(userID);
        //        if (positionResponseForOverall.Net.Count != 0)
        //        {
        //            foreach (var Position in positionResponseForOverall.Net)
        //            {
        //                if (readSettings.TradingsymbolListWithFutureName.Contains(Position.TradingSymbol))
        //                {

        //                    string transactionType = "";
        //                    string price = "";
        //                    if (Position.Quantity > 0)
        //                    {
        //                        transactionType = Constants.TRANSACTION_TYPE_BUY;
        //                        price = Position.BuyPrice.ToString();
        //                    }
        //                    else if (Position.Quantity < 0)
        //                    {
        //                        transactionType = Constants.TRANSACTION_TYPE_SELL;
        //                        price = Position.SellPrice.ToString();
        //                    }
        //                    //  var filtered = mydt.AsEnumerable().Where(r => r.Field<String>("Symbol").Contains(Position.TradingSymbol));
        //                    if (!ListForGridView.Contains(Position.TradingSymbol))
        //                    {
        //                        addIntoDataGridView(Position.TradingSymbol, transactionType, Position.Quantity.ToString(), price);
        //                    }
        //                    else
        //                    {
        //                        updateIntoDataGridView(Position.TradingSymbol, transactionType, Position.Quantity.ToString(), price);
        //                    }

        //                }
        //            }
        //        }
        //    }
        //    catch (Exception er)
        //    {
        //        this.logger.LogMessage("LoadOpenPostion : Exception Error Message =  " + er.Message, MessageType.Exception);
        //    }
        //}

        //read trading symbols from ini
        public void readTradingSymbol(ReadSettings readSettings)
        {
            if (readSettings != null)
                this.readSettings = readSettings;
            if (readSettings.TradingsymbolList.Count == 0)
                readSettings.readEMA_SandipConfigFile(iniFile);
        }

        //function change time after interval
        public void changeTime(string tickCurrentTime)
        {
            //logger.LogMessage("tickCurrentTime " + tickCurrentTime, MessageType.Informational);
            if ((TimeSpan.Parse(tickCurrentTime) >= TimeSpan.Parse(FirstTickEnd)))
            {
                //logger.LogMessage("Before change Time - FirstTickEnd " + FirstTickEnd, MessageType.Informational);
                //logger.LogMessage("Before change Time - End " + End, MessageType.Informational);
                DateTime dt1 = DateTime.ParseExact(FirstTickEnd, "HH:mm:ss", null);
                FirstTickEnd = dt1.AddSeconds(60 * Convert.ToInt32(Interval)).ToString("HH:mm:ss");
                End = dt1.AddSeconds(60 * ((Convert.ToInt32(Interval)) + 1)).ToString("HH:mm:ss");
                //logger.LogMessage("After change Time - FirstTickEnd " + FirstTickEnd, MessageType.Informational);
                //logger.LogMessage("After change Time - End " + End, MessageType.Informational);
            }
        }

        //function calculate quantity according to lotsize
        public int GetQuantity(string TradingSymbol, string Exchange)
        {
            int quantity = 0;
            double lastPrice = AlgoOMS.GetLastPrice(userID, TradingSymbol);
            try
            {
                if (Percent == 0)
                {
                    // WriteUniquelogs(TradingSymbol + " " + userID, "GetQuantity : Percent is 0", MessageType.Informational);
                    quantity = 1;
                }
                else
                {
                    int lotSize = 0;// AlgoOMS.GetLotSize(userID, TradingSymbol, Exchange);
                    if (lotSize != 0)
                    {
                        quantity = (lotSize * Convert.ToInt32(Percent)) / 100;
                    }
                    if (lotSize == 0 || quantity == 0)
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "GetQuantity : lotSize is 0", MessageType.Informational);
                        WriteUniquelogs(TradingSymbol + " " + userID, "GetQuantity : TotalMoney " + TotalMoney, MessageType.Informational);
                        decimal value = ((Convert.ToDecimal(TotalMoney) * Convert.ToDecimal(Percent)) / 100);
                        quantity = Convert.ToInt32(value / Convert.ToDecimal(lastPrice));
                    }
                    if (quantity == 0)
                    {
                        quantity = 1;
                    }
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + userID, "GetQuantity : Exception Error Message =  " + e.Message, MessageType.Exception);
            }
            //WriteUniquelogs(TradingSymbol + " " + userID, "GetQuantity : quantity " + quantity, MessageType.Informational);
            return quantity;
        }

        //place profit order
        public void PlaceProfitOrder(string TradingSymbol, string Exchange, double OpenPrice, double LastPrice, double ProfitPercent, int Quantity)
        {
            //condition to check pending order and order already placed or not (sell)
            if (AlgoOMS.IsPendingStopOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_SELL) == true && (!m_OrderInfo.GetIsProfitOrderPlaced(TradingSymbol)))
            {
                WriteUniquelogs(TradingSymbol + " " + userID, "@#@#@#PlaceProfitOrder :price " + OpenPrice, MessageType.Informational);
                decimal calculatedPrice = 0;
                double PercentValue = ProfitPercent * OpenPrice / 100;
                double CalPrice = OpenPrice + PercentValue;
                calculatedPrice = Convert.ToDecimal(AlgoOMS.RoundOff(userID, TradingSymbol, Convert.ToDecimal(CalPrice), false));
                WriteUniquelogs(TradingSymbol + " " + userID, " PercentValue " + PercentValue + " CalPrice " + CalPrice + " Trigger Price = " + calculatedPrice, MessageType.Informational);
                WriteUniquelogs(TradingSymbol + " " + userID, "@#@#@# PlaceProfitOrder : place sell profit order\n", MessageType.Informational);
                string orderId = AlgoOMS.PlaceLimitOrder(userID, Exchange, TradingSymbol, Constants.TRANSACTION_TYPE_SELL, Quantity, calculatedPrice);
                if (orderId != "NA")
                {
                    m_OrderInfo.AddOrUpdateIsProfitOrderPlaced(TradingSymbol, true);
                    m_OrderInfo.AddOrUpdateProfitOrder(TradingSymbol, Constants.TRANSACTION_TYPE_SELL, orderId);
                }
            }
            //condition to check pending order and order already placed or not(buy)
            else if (AlgoOMS.IsPendingStopOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_BUY) == true && (!m_OrderInfo.GetIsProfitOrderPlaced(TradingSymbol)))
            {
                WriteUniquelogs(TradingSymbol + " " + userID, "@#@#@# PlaceProfitOrder :price " + OpenPrice, MessageType.Informational);
                decimal calculatedPrice = 0;
                double PercentValue = ProfitPercent * OpenPrice / 100;
                double CalPrice = OpenPrice - PercentValue;
                calculatedPrice = Convert.ToDecimal(AlgoOMS.RoundOff(userID, TradingSymbol, Convert.ToDecimal(CalPrice), false));
                WriteUniquelogs(TradingSymbol + " " + userID, "PercentValue " + PercentValue + " CalPrice " + CalPrice + "Trigger Price = " + calculatedPrice, MessageType.Informational);
                WriteUniquelogs(TradingSymbol + " " + userID, "@#@#@# PlaceProfitOrder : place buy profit order\n", MessageType.Informational);
                string orderId = AlgoOMS.PlaceLimitOrder(userID, Exchange, TradingSymbol, Constants.TRANSACTION_TYPE_BUY, Quantity, calculatedPrice);
                if (orderId != "NA")
                {
                    m_OrderInfo.AddOrUpdateIsProfitOrderPlaced(TradingSymbol, true);
                    m_OrderInfo.AddOrUpdateProfitOrder(TradingSymbol, Constants.TRANSACTION_TYPE_BUY, orderId);
                }
            }
        }

        //close all order after clikcing stop automation and close order
        public void CloseAllOrder()
        {
            try
            {
                foreach (var symbolInfo in ListOfTradingSymbolsInfo) //loop for symbols from ini file
                {
                    string TradingSymbol = symbolInfo.getSymbol();
                    string Exchange = symbolInfo.getExchange();
                    AlgoOMS.CloseOrder(userID, TradingSymbol, Exchange);
                }
                logger.LogMessage("CloseAllOrder : closed all order", MessageType.Informational);
            }
            catch (Exception e)
            {
                logger.LogMessage("CloseAllOrder : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

    }
}
