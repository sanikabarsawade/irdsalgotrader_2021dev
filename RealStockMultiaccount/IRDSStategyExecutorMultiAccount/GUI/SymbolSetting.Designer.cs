﻿
namespace IRDSStategyExecutorMultiAccount.GUI
{
    partial class SymbolSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDeleteselectedrow = new System.Windows.Forms.Button();
            this.btnApply = new System.Windows.Forms.Button();
            this.GPSymbolDet = new System.Windows.Forms.GroupBox();
            this.GPAddDet = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtStopLossChange = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtupdatedStoplossChange = new System.Windows.Forms.TextBox();
            this.lblStopLossChange = new System.Windows.Forms.Label();
            this.lblupdatedStoplossChange = new System.Windows.Forms.Label();
            this.pnllNiftyPercent = new System.Windows.Forms.Panel();
            this.txtlNiftyPercent = new System.Windows.Forms.TextBox();
            this.pnlBankNiftyPercent = new System.Windows.Forms.Panel();
            this.txtBankNiftyPercent = new System.Windows.Forms.TextBox();
            this.lblNiftyPercent = new System.Windows.Forms.Label();
            this.lblBankNiftyPercent = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.RBFuture = new System.Windows.Forms.RadioButton();
            this.RBEquity = new System.Windows.Forms.RadioButton();
            this.lblExchange = new System.Windows.Forms.Label();
            this.TradingSymbolList = new System.Windows.Forms.ComboBox();
            this.pnlNA1 = new System.Windows.Forms.Panel();
            this.txtNA1 = new System.Windows.Forms.TextBox();
            this.pnlperiodOfbar = new System.Windows.Forms.Panel();
            this.txtperiodOfbar = new System.Windows.Forms.TextBox();
            this.pnlprofitpercent = new System.Windows.Forms.Panel();
            this.txtprofitpercent = new System.Windows.Forms.TextBox();
            this.pnlNA2 = new System.Windows.Forms.Panel();
            this.txtNA2 = new System.Windows.Forms.TextBox();
            this.pnlstoploss = new System.Windows.Forms.Panel();
            this.txtstoploss = new System.Windows.Forms.TextBox();
            this.pnlNA3 = new System.Windows.Forms.Panel();
            this.txtNA3 = new System.Windows.Forms.TextBox();
            this.pnlNA4 = new System.Windows.Forms.Panel();
            this.txtNA4 = new System.Windows.Forms.TextBox();
            this.pnlNA5 = new System.Windows.Forms.Panel();
            this.txtNA5 = new System.Windows.Forms.TextBox();
            this.btnClearAll = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.lblNA4 = new System.Windows.Forms.Label();
            this.lblNA5 = new System.Windows.Forms.Label();
            this.lblNA2 = new System.Windows.Forms.Label();
            this.lblNA3 = new System.Windows.Forms.Label();
            this.lblstoploss = new System.Windows.Forms.Label();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.TradingSymbol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PeriodOfBar = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProfitPercent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.highLowPercent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Stoploss = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.updatedStoploss = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.profitTrail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Updatedprofittrail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NiftyPercent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BankNiftyPercent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StopLossChange = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UpdatedStopLossChange = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lbltradingsymbol = new System.Windows.Forms.Label();
            this.lblNA1 = new System.Windows.Forms.Label();
            this.lblperiodOfbar = new System.Windows.Forms.Label();
            this.lblprofitpercent = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.BtnImport = new System.Windows.Forms.Button();
            this.btnExport = new System.Windows.Forms.Button();
            this.SaveFDExportFile = new System.Windows.Forms.SaveFileDialog();
            this.OpenFDImportFile = new System.Windows.Forms.OpenFileDialog();
            this.GPSymbolDet.SuspendLayout();
            this.GPAddDet.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.pnllNiftyPercent.SuspendLayout();
            this.pnlBankNiftyPercent.SuspendLayout();
            this.panel1.SuspendLayout();
            this.pnlNA1.SuspendLayout();
            this.pnlperiodOfbar.SuspendLayout();
            this.pnlprofitpercent.SuspendLayout();
            this.pnlNA2.SuspendLayout();
            this.pnlstoploss.SuspendLayout();
            this.pnlNA3.SuspendLayout();
            this.pnlNA4.SuspendLayout();
            this.pnlNA5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // btnDeleteselectedrow
            // 
            this.btnDeleteselectedrow.Font = new System.Drawing.Font("Arial", 10.5F);
            this.btnDeleteselectedrow.Location = new System.Drawing.Point(16, 688);
            this.btnDeleteselectedrow.Name = "btnDeleteselectedrow";
            this.btnDeleteselectedrow.Size = new System.Drawing.Size(213, 40);
            this.btnDeleteselectedrow.TabIndex = 12;
            this.btnDeleteselectedrow.Text = "Delete symbol";
            this.btnDeleteselectedrow.UseVisualStyleBackColor = true;
            this.btnDeleteselectedrow.Click += new System.EventHandler(this.btnDeleteselectedrow_Click);
            // 
            // btnApply
            // 
            this.btnApply.Font = new System.Drawing.Font("Arial", 10.5F);
            this.btnApply.Location = new System.Drawing.Point(646, 688);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(134, 40);
            this.btnApply.TabIndex = 13;
            this.btnApply.Text = "Apply";
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // GPSymbolDet
            // 
            this.GPSymbolDet.Controls.Add(this.GPAddDet);
            this.GPSymbolDet.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GPSymbolDet.Location = new System.Drawing.Point(12, 19);
            this.GPSymbolDet.Name = "GPSymbolDet";
            this.GPSymbolDet.Size = new System.Drawing.Size(969, 652);
            this.GPSymbolDet.TabIndex = 1;
            this.GPSymbolDet.TabStop = false;
            this.GPSymbolDet.Text = "Symbol Details";
            // 
            // GPAddDet
            // 
            this.GPAddDet.Controls.Add(this.panel2);
            this.GPAddDet.Controls.Add(this.panel3);
            this.GPAddDet.Controls.Add(this.lblStopLossChange);
            this.GPAddDet.Controls.Add(this.lblupdatedStoplossChange);
            this.GPAddDet.Controls.Add(this.pnllNiftyPercent);
            this.GPAddDet.Controls.Add(this.pnlBankNiftyPercent);
            this.GPAddDet.Controls.Add(this.lblNiftyPercent);
            this.GPAddDet.Controls.Add(this.lblBankNiftyPercent);
            this.GPAddDet.Controls.Add(this.panel1);
            this.GPAddDet.Controls.Add(this.lblExchange);
            this.GPAddDet.Controls.Add(this.TradingSymbolList);
            this.GPAddDet.Controls.Add(this.pnlNA1);
            this.GPAddDet.Controls.Add(this.pnlperiodOfbar);
            this.GPAddDet.Controls.Add(this.pnlprofitpercent);
            this.GPAddDet.Controls.Add(this.pnlNA2);
            this.GPAddDet.Controls.Add(this.pnlstoploss);
            this.GPAddDet.Controls.Add(this.pnlNA3);
            this.GPAddDet.Controls.Add(this.pnlNA4);
            this.GPAddDet.Controls.Add(this.pnlNA5);
            this.GPAddDet.Controls.Add(this.btnClearAll);
            this.GPAddDet.Controls.Add(this.btnUpdate);
            this.GPAddDet.Controls.Add(this.btnAdd);
            this.GPAddDet.Controls.Add(this.lblNA4);
            this.GPAddDet.Controls.Add(this.lblNA5);
            this.GPAddDet.Controls.Add(this.lblNA2);
            this.GPAddDet.Controls.Add(this.lblNA3);
            this.GPAddDet.Controls.Add(this.lblstoploss);
            this.GPAddDet.Controls.Add(this.dataGridView);
            this.GPAddDet.Controls.Add(this.lbltradingsymbol);
            this.GPAddDet.Controls.Add(this.lblNA1);
            this.GPAddDet.Controls.Add(this.lblperiodOfbar);
            this.GPAddDet.Controls.Add(this.lblprofitpercent);
            this.GPAddDet.Font = new System.Drawing.Font("Arial", 10F);
            this.GPAddDet.Location = new System.Drawing.Point(12, 23);
            this.GPAddDet.Name = "GPAddDet";
            this.GPAddDet.Size = new System.Drawing.Size(951, 623);
            this.GPAddDet.TabIndex = 2;
            this.GPAddDet.TabStop = false;
            this.GPAddDet.Text = "Add details";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.txtStopLossChange);
            this.panel2.Location = new System.Drawing.Point(609, 266);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(171, 30);
            this.panel2.TabIndex = 42;
            // 
            // txtStopLossChange
            // 
            this.txtStopLossChange.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtStopLossChange.Font = new System.Drawing.Font("Arial", 11F);
            this.txtStopLossChange.Location = new System.Drawing.Point(5, 3);
            this.txtStopLossChange.Name = "txtStopLossChange";
            this.txtStopLossChange.Size = new System.Drawing.Size(160, 22);
            this.txtStopLossChange.TabIndex = 7;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.txtupdatedStoplossChange);
            this.panel3.Location = new System.Drawing.Point(609, 304);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(171, 30);
            this.panel3.TabIndex = 43;
            // 
            // txtupdatedStoplossChange
            // 
            this.txtupdatedStoplossChange.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtupdatedStoplossChange.Font = new System.Drawing.Font("Arial", 11F);
            this.txtupdatedStoplossChange.Location = new System.Drawing.Point(5, 3);
            this.txtupdatedStoplossChange.Name = "txtupdatedStoplossChange";
            this.txtupdatedStoplossChange.Size = new System.Drawing.Size(160, 22);
            this.txtupdatedStoplossChange.TabIndex = 8;
            // 
            // lblStopLossChange
            // 
            this.lblStopLossChange.Font = new System.Drawing.Font("Arial", 10F);
            this.lblStopLossChange.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblStopLossChange.Location = new System.Drawing.Point(399, 268);
            this.lblStopLossChange.Name = "lblStopLossChange";
            this.lblStopLossChange.Size = new System.Drawing.Size(174, 22);
            this.lblStopLossChange.TabIndex = 41;
            this.lblStopLossChange.Text = "StopLoss Change";
            // 
            // lblupdatedStoplossChange
            // 
            this.lblupdatedStoplossChange.Font = new System.Drawing.Font("Arial", 10F);
            this.lblupdatedStoplossChange.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblupdatedStoplossChange.Location = new System.Drawing.Point(399, 308);
            this.lblupdatedStoplossChange.Name = "lblupdatedStoplossChange";
            this.lblupdatedStoplossChange.Size = new System.Drawing.Size(200, 19);
            this.lblupdatedStoplossChange.TabIndex = 40;
            this.lblupdatedStoplossChange.Text = "Updated Stoploss Change";
            // 
            // pnllNiftyPercent
            // 
            this.pnllNiftyPercent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnllNiftyPercent.Controls.Add(this.txtlNiftyPercent);
            this.pnllNiftyPercent.Location = new System.Drawing.Point(609, 189);
            this.pnllNiftyPercent.Name = "pnllNiftyPercent";
            this.pnllNiftyPercent.Size = new System.Drawing.Size(171, 30);
            this.pnllNiftyPercent.TabIndex = 38;
            // 
            // txtlNiftyPercent
            // 
            this.txtlNiftyPercent.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtlNiftyPercent.Font = new System.Drawing.Font("Arial", 11F);
            this.txtlNiftyPercent.Location = new System.Drawing.Point(5, 3);
            this.txtlNiftyPercent.Name = "txtlNiftyPercent";
            this.txtlNiftyPercent.Size = new System.Drawing.Size(160, 22);
            this.txtlNiftyPercent.TabIndex = 7;
            // 
            // pnlBankNiftyPercent
            // 
            this.pnlBankNiftyPercent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlBankNiftyPercent.Controls.Add(this.txtBankNiftyPercent);
            this.pnlBankNiftyPercent.Location = new System.Drawing.Point(609, 227);
            this.pnlBankNiftyPercent.Name = "pnlBankNiftyPercent";
            this.pnlBankNiftyPercent.Size = new System.Drawing.Size(171, 30);
            this.pnlBankNiftyPercent.TabIndex = 39;
            // 
            // txtBankNiftyPercent
            // 
            this.txtBankNiftyPercent.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtBankNiftyPercent.Font = new System.Drawing.Font("Arial", 11F);
            this.txtBankNiftyPercent.Location = new System.Drawing.Point(5, 3);
            this.txtBankNiftyPercent.Name = "txtBankNiftyPercent";
            this.txtBankNiftyPercent.Size = new System.Drawing.Size(160, 22);
            this.txtBankNiftyPercent.TabIndex = 8;
            // 
            // lblNiftyPercent
            // 
            this.lblNiftyPercent.Font = new System.Drawing.Font("Arial", 10F);
            this.lblNiftyPercent.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblNiftyPercent.Location = new System.Drawing.Point(399, 193);
            this.lblNiftyPercent.Name = "lblNiftyPercent";
            this.lblNiftyPercent.Size = new System.Drawing.Size(129, 19);
            this.lblNiftyPercent.TabIndex = 37;
            this.lblNiftyPercent.Text = "Nifty Percent";
            // 
            // lblBankNiftyPercent
            // 
            this.lblBankNiftyPercent.Font = new System.Drawing.Font("Arial", 10F);
            this.lblBankNiftyPercent.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblBankNiftyPercent.Location = new System.Drawing.Point(399, 231);
            this.lblBankNiftyPercent.Name = "lblBankNiftyPercent";
            this.lblBankNiftyPercent.Size = new System.Drawing.Size(160, 19);
            this.lblBankNiftyPercent.TabIndex = 36;
            this.lblBankNiftyPercent.Text = "BankNifty Percent";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.RBFuture);
            this.panel1.Controls.Add(this.RBEquity);
            this.panel1.Location = new System.Drawing.Point(195, 24);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(506, 35);
            this.panel1.TabIndex = 35;
            // 
            // RBFuture
            // 
            this.RBFuture.AutoSize = true;
            this.RBFuture.Location = new System.Drawing.Point(309, 5);
            this.RBFuture.Name = "RBFuture";
            this.RBFuture.Size = new System.Drawing.Size(77, 23);
            this.RBFuture.TabIndex = 1;
            this.RBFuture.Text = "Future";
            this.RBFuture.UseVisualStyleBackColor = true;
            this.RBFuture.CheckedChanged += new System.EventHandler(this.RBFuture_CheckedChanged);
            // 
            // RBEquity
            // 
            this.RBEquity.AutoSize = true;
            this.RBEquity.Location = new System.Drawing.Point(56, 6);
            this.RBEquity.Name = "RBEquity";
            this.RBEquity.Size = new System.Drawing.Size(76, 23);
            this.RBEquity.TabIndex = 0;
            this.RBEquity.Text = "Equity";
            this.RBEquity.UseVisualStyleBackColor = true;
            this.RBEquity.CheckedChanged += new System.EventHandler(this.RBEquity_CheckedChanged);
            // 
            // lblExchange
            // 
            this.lblExchange.Font = new System.Drawing.Font("Arial", 10F);
            this.lblExchange.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblExchange.Location = new System.Drawing.Point(25, 34);
            this.lblExchange.Name = "lblExchange";
            this.lblExchange.Size = new System.Drawing.Size(110, 19);
            this.lblExchange.TabIndex = 34;
            this.lblExchange.Text = "Exchange";
            // 
            // TradingSymbolList
            // 
            this.TradingSymbolList.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.TradingSymbolList.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.TradingSymbolList.Font = new System.Drawing.Font("Arial", 12F);
            this.TradingSymbolList.FormattingEnabled = true;
            this.TradingSymbolList.Location = new System.Drawing.Point(195, 71);
            this.TradingSymbolList.Name = "TradingSymbolList";
            this.TradingSymbolList.Size = new System.Drawing.Size(506, 31);
            this.TradingSymbolList.TabIndex = 0;
            // 
            // pnlNA1
            // 
            this.pnlNA1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlNA1.Controls.Add(this.txtNA1);
            this.pnlNA1.Location = new System.Drawing.Point(195, 110);
            this.pnlNA1.Name = "pnlNA1";
            this.pnlNA1.Size = new System.Drawing.Size(171, 30);
            this.pnlNA1.TabIndex = 20;
            // 
            // txtNA1
            // 
            this.txtNA1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNA1.Font = new System.Drawing.Font("Arial", 11F);
            this.txtNA1.Location = new System.Drawing.Point(5, 3);
            this.txtNA1.Name = "txtNA1";
            this.txtNA1.Size = new System.Drawing.Size(160, 22);
            this.txtNA1.TabIndex = 1;
            // 
            // pnlperiodOfbar
            // 
            this.pnlperiodOfbar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlperiodOfbar.Controls.Add(this.txtperiodOfbar);
            this.pnlperiodOfbar.Location = new System.Drawing.Point(195, 148);
            this.pnlperiodOfbar.Name = "pnlperiodOfbar";
            this.pnlperiodOfbar.Size = new System.Drawing.Size(171, 30);
            this.pnlperiodOfbar.TabIndex = 21;
            // 
            // txtperiodOfbar
            // 
            this.txtperiodOfbar.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtperiodOfbar.Font = new System.Drawing.Font("Arial", 11F);
            this.txtperiodOfbar.Location = new System.Drawing.Point(5, 3);
            this.txtperiodOfbar.Name = "txtperiodOfbar";
            this.txtperiodOfbar.Size = new System.Drawing.Size(160, 22);
            this.txtperiodOfbar.TabIndex = 2;
            // 
            // pnlprofitpercent
            // 
            this.pnlprofitpercent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlprofitpercent.Controls.Add(this.txtprofitpercent);
            this.pnlprofitpercent.Location = new System.Drawing.Point(195, 186);
            this.pnlprofitpercent.Name = "pnlprofitpercent";
            this.pnlprofitpercent.Size = new System.Drawing.Size(171, 30);
            this.pnlprofitpercent.TabIndex = 22;
            // 
            // txtprofitpercent
            // 
            this.txtprofitpercent.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtprofitpercent.Font = new System.Drawing.Font("Arial", 11F);
            this.txtprofitpercent.Location = new System.Drawing.Point(5, 3);
            this.txtprofitpercent.Name = "txtprofitpercent";
            this.txtprofitpercent.Size = new System.Drawing.Size(160, 22);
            this.txtprofitpercent.TabIndex = 3;
            // 
            // pnlNA2
            // 
            this.pnlNA2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlNA2.Controls.Add(this.txtNA2);
            this.pnlNA2.Location = new System.Drawing.Point(195, 223);
            this.pnlNA2.Name = "pnlNA2";
            this.pnlNA2.Size = new System.Drawing.Size(171, 30);
            this.pnlNA2.TabIndex = 25;
            // 
            // txtNA2
            // 
            this.txtNA2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNA2.Font = new System.Drawing.Font("Arial", 11F);
            this.txtNA2.Location = new System.Drawing.Point(5, 3);
            this.txtNA2.Name = "txtNA2";
            this.txtNA2.Size = new System.Drawing.Size(160, 22);
            this.txtNA2.TabIndex = 4;
            // 
            // pnlstoploss
            // 
            this.pnlstoploss.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlstoploss.Controls.Add(this.txtstoploss);
            this.pnlstoploss.Location = new System.Drawing.Point(195, 262);
            this.pnlstoploss.Name = "pnlstoploss";
            this.pnlstoploss.Size = new System.Drawing.Size(171, 30);
            this.pnlstoploss.TabIndex = 26;
            // 
            // txtstoploss
            // 
            this.txtstoploss.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtstoploss.Font = new System.Drawing.Font("Arial", 11F);
            this.txtstoploss.Location = new System.Drawing.Point(5, 3);
            this.txtstoploss.Name = "txtstoploss";
            this.txtstoploss.Size = new System.Drawing.Size(160, 22);
            this.txtstoploss.TabIndex = 5;
            // 
            // pnlNA3
            // 
            this.pnlNA3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlNA3.Controls.Add(this.txtNA3);
            this.pnlNA3.Location = new System.Drawing.Point(194, 304);
            this.pnlNA3.Name = "pnlNA3";
            this.pnlNA3.Size = new System.Drawing.Size(171, 30);
            this.pnlNA3.TabIndex = 28;
            // 
            // txtNA3
            // 
            this.txtNA3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNA3.Font = new System.Drawing.Font("Arial", 11F);
            this.txtNA3.Location = new System.Drawing.Point(5, 2);
            this.txtNA3.Name = "txtNA3";
            this.txtNA3.Size = new System.Drawing.Size(160, 22);
            this.txtNA3.TabIndex = 6;
            // 
            // pnlNA4
            // 
            this.pnlNA4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlNA4.Controls.Add(this.txtNA4);
            this.pnlNA4.Location = new System.Drawing.Point(609, 111);
            this.pnlNA4.Name = "pnlNA4";
            this.pnlNA4.Size = new System.Drawing.Size(171, 30);
            this.pnlNA4.TabIndex = 31;
            // 
            // txtNA4
            // 
            this.txtNA4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNA4.Font = new System.Drawing.Font("Arial", 11F);
            this.txtNA4.Location = new System.Drawing.Point(5, 3);
            this.txtNA4.Name = "txtNA4";
            this.txtNA4.Size = new System.Drawing.Size(160, 22);
            this.txtNA4.TabIndex = 7;
            // 
            // pnlNA5
            // 
            this.pnlNA5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlNA5.Controls.Add(this.txtNA5);
            this.pnlNA5.Location = new System.Drawing.Point(609, 149);
            this.pnlNA5.Name = "pnlNA5";
            this.pnlNA5.Size = new System.Drawing.Size(171, 30);
            this.pnlNA5.TabIndex = 32;
            // 
            // txtNA5
            // 
            this.txtNA5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNA5.Font = new System.Drawing.Font("Arial", 11F);
            this.txtNA5.Location = new System.Drawing.Point(5, 3);
            this.txtNA5.Name = "txtNA5";
            this.txtNA5.Size = new System.Drawing.Size(160, 22);
            this.txtNA5.TabIndex = 8;
            // 
            // btnClearAll
            // 
            this.btnClearAll.Font = new System.Drawing.Font("Arial", 10F);
            this.btnClearAll.Location = new System.Drawing.Point(813, 224);
            this.btnClearAll.Name = "btnClearAll";
            this.btnClearAll.Size = new System.Drawing.Size(109, 31);
            this.btnClearAll.TabIndex = 9;
            this.btnClearAll.Text = "Clear All";
            this.btnClearAll.UseVisualStyleBackColor = true;
            this.btnClearAll.Click += new System.EventHandler(this.btnClearAll_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Font = new System.Drawing.Font("Arial", 10F);
            this.btnUpdate.Location = new System.Drawing.Point(813, 262);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(109, 31);
            this.btnUpdate.TabIndex = 10;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("Arial", 10F);
            this.btnAdd.Location = new System.Drawing.Point(813, 300);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(109, 31);
            this.btnAdd.TabIndex = 11;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // lblNA4
            // 
            this.lblNA4.Font = new System.Drawing.Font("Arial", 10F);
            this.lblNA4.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblNA4.Location = new System.Drawing.Point(399, 115);
            this.lblNA4.Name = "lblNA4";
            this.lblNA4.Size = new System.Drawing.Size(129, 19);
            this.lblNA4.TabIndex = 30;
            this.lblNA4.Text = "Profit Trail";
            // 
            // lblNA5
            // 
            this.lblNA5.Font = new System.Drawing.Font("Arial", 10F);
            this.lblNA5.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblNA5.Location = new System.Drawing.Point(399, 153);
            this.lblNA5.Name = "lblNA5";
            this.lblNA5.Size = new System.Drawing.Size(160, 19);
            this.lblNA5.TabIndex = 29;
            this.lblNA5.Text = "Updated Profit Trail";
            // 
            // lblNA2
            // 
            this.lblNA2.Font = new System.Drawing.Font("Arial", 10F);
            this.lblNA2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblNA2.Location = new System.Drawing.Point(25, 228);
            this.lblNA2.Name = "lblNA2";
            this.lblNA2.Size = new System.Drawing.Size(144, 19);
            this.lblNA2.TabIndex = 24;
            this.lblNA2.Text = "High Low Percent";
            // 
            // lblNA3
            // 
            this.lblNA3.Font = new System.Drawing.Font("Arial", 10F);
            this.lblNA3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblNA3.Location = new System.Drawing.Point(25, 308);
            this.lblNA3.Name = "lblNA3";
            this.lblNA3.Size = new System.Drawing.Size(160, 19);
            this.lblNA3.TabIndex = 27;
            this.lblNA3.Text = "Updated Stoploss";
            // 
            // lblstoploss
            // 
            this.lblstoploss.Font = new System.Drawing.Font("Arial", 10F);
            this.lblstoploss.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblstoploss.Location = new System.Drawing.Point(25, 266);
            this.lblstoploss.Name = "lblstoploss";
            this.lblstoploss.Size = new System.Drawing.Size(89, 19);
            this.lblstoploss.TabIndex = 23;
            this.lblstoploss.Text = "Stoploss";
            // 
            // dataGridView
            // 
            this.dataGridView.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TradingSymbol,
            this.Quantity,
            this.PeriodOfBar,
            this.ProfitPercent,
            this.highLowPercent,
            this.Stoploss,
            this.updatedStoploss,
            this.profitTrail,
            this.Updatedprofittrail,
            this.NiftyPercent,
            this.BankNiftyPercent,
            this.StopLossChange,
            this.UpdatedStopLossChange});
            this.dataGridView.Location = new System.Drawing.Point(19, 358);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.ReadOnly = true;
            this.dataGridView.RowHeadersWidth = 51;
            this.dataGridView.RowTemplate.Height = 24;
            this.dataGridView.Size = new System.Drawing.Size(926, 251);
            this.dataGridView.TabIndex = 1;
            this.dataGridView.TabStop = false;
            this.dataGridView.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView_CellMouseClick);
            // 
            // TradingSymbol
            // 
            this.TradingSymbol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.TradingSymbol.HeaderText = "Trading Symbol";
            this.TradingSymbol.MinimumWidth = 6;
            this.TradingSymbol.Name = "TradingSymbol";
            this.TradingSymbol.ReadOnly = true;
            // 
            // Quantity
            // 
            this.Quantity.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Quantity.FillWeight = 90F;
            this.Quantity.HeaderText = "Quantity";
            this.Quantity.MinimumWidth = 6;
            this.Quantity.Name = "Quantity";
            this.Quantity.ReadOnly = true;
            // 
            // PeriodOfBar
            // 
            this.PeriodOfBar.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.PeriodOfBar.HeaderText = "Period of Bar";
            this.PeriodOfBar.MinimumWidth = 10;
            this.PeriodOfBar.Name = "PeriodOfBar";
            this.PeriodOfBar.ReadOnly = true;
            // 
            // ProfitPercent
            // 
            this.ProfitPercent.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ProfitPercent.FillWeight = 76.19048F;
            this.ProfitPercent.HeaderText = "Profit Percent";
            this.ProfitPercent.MinimumWidth = 6;
            this.ProfitPercent.Name = "ProfitPercent";
            this.ProfitPercent.ReadOnly = true;
            // 
            // highLowPercent
            // 
            this.highLowPercent.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.highLowPercent.FillWeight = 76.19048F;
            this.highLowPercent.HeaderText = "High Low Percent";
            this.highLowPercent.MinimumWidth = 6;
            this.highLowPercent.Name = "highLowPercent";
            this.highLowPercent.ReadOnly = true;
            // 
            // Stoploss
            // 
            this.Stoploss.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Stoploss.FillWeight = 76.19048F;
            this.Stoploss.HeaderText = "Stoploss";
            this.Stoploss.MinimumWidth = 6;
            this.Stoploss.Name = "Stoploss";
            this.Stoploss.ReadOnly = true;
            // 
            // updatedStoploss
            // 
            this.updatedStoploss.FillWeight = 80F;
            this.updatedStoploss.HeaderText = "Updated Stoploss";
            this.updatedStoploss.MinimumWidth = 6;
            this.updatedStoploss.Name = "updatedStoploss";
            this.updatedStoploss.ReadOnly = true;
            this.updatedStoploss.Width = 80;
            // 
            // profitTrail
            // 
            this.profitTrail.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.profitTrail.FillWeight = 76.19048F;
            this.profitTrail.HeaderText = "profit Trail";
            this.profitTrail.MinimumWidth = 6;
            this.profitTrail.Name = "profitTrail";
            this.profitTrail.ReadOnly = true;
            // 
            // Updatedprofittrail
            // 
            this.Updatedprofittrail.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Updatedprofittrail.FillWeight = 76.19048F;
            this.Updatedprofittrail.HeaderText = "Updated Profit Trail";
            this.Updatedprofittrail.MinimumWidth = 6;
            this.Updatedprofittrail.Name = "Updatedprofittrail";
            this.Updatedprofittrail.ReadOnly = true;
            // 
            // NiftyPercent
            // 
            this.NiftyPercent.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.NiftyPercent.HeaderText = "Nifty Percent";
            this.NiftyPercent.MinimumWidth = 6;
            this.NiftyPercent.Name = "NiftyPercent";
            this.NiftyPercent.ReadOnly = true;
            // 
            // BankNiftyPercent
            // 
            this.BankNiftyPercent.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.BankNiftyPercent.HeaderText = "BankNifty Percent";
            this.BankNiftyPercent.MinimumWidth = 6;
            this.BankNiftyPercent.Name = "BankNiftyPercent";
            this.BankNiftyPercent.ReadOnly = true;
            // 
            // StopLossChange
            // 
            this.StopLossChange.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.StopLossChange.HeaderText = "StopLoss Change";
            this.StopLossChange.MinimumWidth = 6;
            this.StopLossChange.Name = "StopLossChange";
            this.StopLossChange.ReadOnly = true;
            // 
            // UpdatedStopLossChange
            // 
            this.UpdatedStopLossChange.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.UpdatedStopLossChange.HeaderText = "Updated StopLoss Change";
            this.UpdatedStopLossChange.MinimumWidth = 6;
            this.UpdatedStopLossChange.Name = "UpdatedStopLossChange";
            this.UpdatedStopLossChange.ReadOnly = true;
            // 
            // lbltradingsymbol
            // 
            this.lbltradingsymbol.Font = new System.Drawing.Font("Arial", 10F);
            this.lbltradingsymbol.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lbltradingsymbol.Location = new System.Drawing.Point(25, 78);
            this.lbltradingsymbol.Name = "lbltradingsymbol";
            this.lbltradingsymbol.Size = new System.Drawing.Size(110, 19);
            this.lbltradingsymbol.TabIndex = 17;
            this.lbltradingsymbol.Text = "Symbol";
            // 
            // lblNA1
            // 
            this.lblNA1.Font = new System.Drawing.Font("Arial", 10F);
            this.lblNA1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblNA1.Location = new System.Drawing.Point(25, 114);
            this.lblNA1.Name = "lblNA1";
            this.lblNA1.Size = new System.Drawing.Size(129, 19);
            this.lblNA1.TabIndex = 16;
            this.lblNA1.Text = "Quantity";
            // 
            // lblperiodOfbar
            // 
            this.lblperiodOfbar.Font = new System.Drawing.Font("Arial", 10F);
            this.lblperiodOfbar.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblperiodOfbar.Location = new System.Drawing.Point(25, 152);
            this.lblperiodOfbar.Name = "lblperiodOfbar";
            this.lblperiodOfbar.Size = new System.Drawing.Size(129, 19);
            this.lblperiodOfbar.TabIndex = 13;
            this.lblperiodOfbar.Text = "Period of Bar";
            // 
            // lblprofitpercent
            // 
            this.lblprofitpercent.Font = new System.Drawing.Font("Arial", 10F);
            this.lblprofitpercent.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblprofitpercent.Location = new System.Drawing.Point(25, 192);
            this.lblprofitpercent.Name = "lblprofitpercent";
            this.lblprofitpercent.Size = new System.Drawing.Size(129, 19);
            this.lblprofitpercent.TabIndex = 9;
            this.lblprofitpercent.Text = "Profit Percent";
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("Arial", 10.5F);
            this.btnCancel.Location = new System.Drawing.Point(838, 688);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(134, 40);
            this.btnCancel.TabIndex = 14;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // BtnImport
            // 
            this.BtnImport.Font = new System.Drawing.Font("Arial", 10.5F);
            this.BtnImport.Location = new System.Drawing.Point(461, 688);
            this.BtnImport.Name = "BtnImport";
            this.BtnImport.Size = new System.Drawing.Size(134, 40);
            this.BtnImport.TabIndex = 27;
            this.BtnImport.Text = "Import";
            this.BtnImport.UseVisualStyleBackColor = true;
            this.BtnImport.Click += new System.EventHandler(this.BtnImport_Click);
            // 
            // btnExport
            // 
            this.btnExport.Font = new System.Drawing.Font("Arial", 10.5F);
            this.btnExport.Location = new System.Drawing.Point(278, 688);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(134, 40);
            this.btnExport.TabIndex = 26;
            this.btnExport.Text = "Export";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);           
            // 
            // SaveFDExportFile
            // 
            this.SaveFDExportFile.FileName = "SymbolSettingDetails";
            this.SaveFDExportFile.Title = "Export File";
            // 
            // OpenFDImportFile
            // 
            this.OpenFDImportFile.Title = "Import File";
            // 
            // SymbolSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(993, 757);
            this.Controls.Add(this.BtnImport);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.GPSymbolDet);
            this.Controls.Add(this.btnDeleteselectedrow);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.btnCancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "SymbolSetting";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Symbol Setting";
            this.GPSymbolDet.ResumeLayout(false);
            this.GPAddDet.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.pnllNiftyPercent.ResumeLayout(false);
            this.pnllNiftyPercent.PerformLayout();
            this.pnlBankNiftyPercent.ResumeLayout(false);
            this.pnlBankNiftyPercent.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.pnlNA1.ResumeLayout(false);
            this.pnlNA1.PerformLayout();
            this.pnlperiodOfbar.ResumeLayout(false);
            this.pnlperiodOfbar.PerformLayout();
            this.pnlprofitpercent.ResumeLayout(false);
            this.pnlprofitpercent.PerformLayout();
            this.pnlNA2.ResumeLayout(false);
            this.pnlNA2.PerformLayout();
            this.pnlstoploss.ResumeLayout(false);
            this.pnlstoploss.PerformLayout();
            this.pnlNA3.ResumeLayout(false);
            this.pnlNA3.PerformLayout();
            this.pnlNA4.ResumeLayout(false);
            this.pnlNA4.PerformLayout();
            this.pnlNA5.ResumeLayout(false);
            this.pnlNA5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ComboBox TradingSymbolList;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnDeleteselectedrow;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.Button btnCancel;


        private System.Windows.Forms.GroupBox GPSymbolDet;
        private System.Windows.Forms.GroupBox GPAddDet;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.Label lblprofitpercent;
        private System.Windows.Forms.Label lblperiodOfbar;
        private System.Windows.Forms.Label lblNA1;
        private System.Windows.Forms.Label lbltradingsymbol;
        private System.Windows.Forms.Panel pnlNA1;
        private System.Windows.Forms.Panel pnlprofitpercent;
        private System.Windows.Forms.Panel pnlperiodOfbar;
        private System.Windows.Forms.TextBox txtprofitpercent;
        private System.Windows.Forms.TextBox txtperiodOfbar;
        private System.Windows.Forms.TextBox txtNA1;
        private System.Windows.Forms.Panel pnlstoploss;
        private System.Windows.Forms.TextBox txtstoploss;
        private System.Windows.Forms.Panel pnlNA2;
        private System.Windows.Forms.TextBox txtNA2;
        private System.Windows.Forms.Label lblNA2;
        private System.Windows.Forms.Label lblstoploss;
        private System.Windows.Forms.Panel pnlNA5;
        private System.Windows.Forms.TextBox txtNA5;
        private System.Windows.Forms.Panel pnlNA3;
        private System.Windows.Forms.TextBox txtNA3;
        private System.Windows.Forms.Panel pnlNA4;
        private System.Windows.Forms.TextBox txtNA4;
        private System.Windows.Forms.Label lblNA4;
        private System.Windows.Forms.Label lblNA5;
        private System.Windows.Forms.Label lblNA3;
        private System.Windows.Forms.DataGridViewTextBoxColumn NA1;
        private System.Windows.Forms.DataGridViewTextBoxColumn NA2;
        private System.Windows.Forms.DataGridViewTextBoxColumn NA3;
        private System.Windows.Forms.DataGridViewTextBoxColumn NA4;
        private System.Windows.Forms.DataGridViewTextBoxColumn NA5;
        private System.Windows.Forms.Button btnClearAll;
        private System.Windows.Forms.Label lblExchange;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton RBFuture;
        private System.Windows.Forms.RadioButton RBEquity;
        private System.Windows.Forms.Panel pnllNiftyPercent;
        private System.Windows.Forms.TextBox txtlNiftyPercent;
        private System.Windows.Forms.Panel pnlBankNiftyPercent;
        private System.Windows.Forms.TextBox txtBankNiftyPercent;
        private System.Windows.Forms.Label lblNiftyPercent;
        private System.Windows.Forms.Label lblBankNiftyPercent;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txtStopLossChange;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox txtupdatedStoplossChange;
        private System.Windows.Forms.Label lblStopLossChange;
        private System.Windows.Forms.Label lblupdatedStoplossChange;
        private System.Windows.Forms.DataGridViewTextBoxColumn TradingSymbol;
        private System.Windows.Forms.DataGridViewTextBoxColumn Quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn PeriodOfBar;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProfitPercent;
        private System.Windows.Forms.DataGridViewTextBoxColumn highLowPercent;
        private System.Windows.Forms.DataGridViewTextBoxColumn Stoploss;
        private System.Windows.Forms.DataGridViewTextBoxColumn updatedStoploss;
        private System.Windows.Forms.DataGridViewTextBoxColumn profitTrail;
        private System.Windows.Forms.DataGridViewTextBoxColumn Updatedprofittrail;
        private System.Windows.Forms.DataGridViewTextBoxColumn NiftyPercent;
        private System.Windows.Forms.DataGridViewTextBoxColumn BankNiftyPercent;
        private System.Windows.Forms.DataGridViewTextBoxColumn StopLossChange;
        private System.Windows.Forms.DataGridViewTextBoxColumn UpdatedStopLossChange;
        private System.Windows.Forms.Button BtnImport;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.SaveFileDialog SaveFDExportFile;
        private System.Windows.Forms.OpenFileDialog OpenFDImportFile;
    }
}