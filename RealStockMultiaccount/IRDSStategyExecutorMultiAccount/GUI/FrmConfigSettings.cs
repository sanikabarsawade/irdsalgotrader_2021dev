﻿using IRDSAlgoOMS;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IRDSStategyExecutorMultiAccount
{
    public partial class FrmConfigSettings : Form
    {
        //string iniFile = ConfigFilePath + "\\CompositeSettings.ini";
        string iniFile = "";
        bool bAutomationasserver = false;
        static string ConfigFilePath = System.AppDomain.CurrentDomain.BaseDirectory + "Configuration";

        INIFile objINI = null;
        ReadSettings objConfigSettings = null;
        string title = "IRDS Strategy Executor";
        string message = "";
        MessageBoxButtons btnOk = MessageBoxButtons.OK;
        MessageBoxButtons btnYesNo = MessageBoxButtons.YesNo;
        DialogResult dialog;
        bool status = false;
        string oldSelected = null;
        Logger m_logger = null;
        List<string> m_CompositeAccountNumbers = new List<string>();
        List<string> m_ZerodhaAccountNumbers = new List<string>();
        string m_FrmTitle = "";
        public FrmConfigSettings(Logger logger, string FrmTitle)
        {
            m_FrmTitle = FrmTitle;
            Assembly myAssembly = Assembly.GetExecutingAssembly();
            Stream IconStream = null;
            IconStream = myAssembly.GetManifestResourceStream("IRDSStategyExecutorMultiAccount.Resources." + m_FrmTitle + ".ico");
            this.Icon = new System.Drawing.Icon(IconStream);
            InitializeComponent();
            m_logger = logger;
            ZerodhaTrue.Checked = true;
            //if(objINI  == null)
            //{
            //    objINI = new INIFile(iniFile);
            //}
        }

        private void FrmConfigSettings_Load(object sender, EventArgs e)
        {
            try
            {
                if (bAutomationasserver)
                {
                    //if (File.Exists(iniFile))
                    //{
                    //    if (objConfigSettings == null)
                    //    {
                    //        objConfigSettings = new ReadSettings(m_logger);
                    //    }
                    //    objConfigSettings.readCompositeConfigFile(iniFile);
                    //    txtRoot.Text = objConfigSettings.root;
                    //    txtUserid.Text = objConfigSettings.username;
                    //    txtAppKey.Text = objConfigSettings.appKey;
                    //    txtSecretKey.Text = objConfigSettings.secretKey;
                    //    txtMarketAppKey.Text = objConfigSettings.marketDataAppKey;
                    //    txtMarketSecretKey.Text = objConfigSettings.marketDataSecretKey;
                    //    AccountList.Items.Add(txtUserid.Text.Trim());
                    //}
                    //else
                    //{
                    //    AccountList.Items.Add("Add New Account");
                    //}
                    AccountList.SelectedIndex = 0;
                    AccountList.Enabled = false;
                }
                else
                {
                    LoadComboboxForAccount();
                    AccountList.SelectedIndex = 0;
                    iniFile = ConfigFilePath + "\\CompositeSettings_" + txtUserid.Text.Trim() + ".ini";
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("ConfigSettingsGUI", "FrmConfigSettings_Load : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void LoadComboboxForAccount()
        {
            AccountList.Items.Clear();
            AccountList.Enabled = true;
            AccountList.Items.Add("Add New Account");
            string[] files = Directory.GetFiles(ConfigFilePath, "CompositeSettings_*");
            foreach (string fileName in files)
            {
                string file = Path.GetFileNameWithoutExtension(fileName);
                if (!AccountList.Items.Contains(file))
                {
                    AccountList.Items.Add(file.Split('_')[1]);
                    m_CompositeAccountNumbers.Add(file.Split('_')[1]);
                }
            }

            string[] files1 = Directory.GetFiles(ConfigFilePath, "ZerodhaSettings_*");
            foreach (string fileName in files1)
            {
                string file = Path.GetFileNameWithoutExtension(fileName);
                if (!AccountList.Items.Contains(file))
                {
                    AccountList.Items.Add(file.Split('_')[1]);
                    m_ZerodhaAccountNumbers.Add(file.Split('_')[1]);
                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string userid = txtUserid.Text.Trim();
                if (ZerodhaTrue.Checked == true)
                {
                    bool newAccount = true;
                    if ((userid != null && userid != "") && (txtRoot.Text != null && txtRoot.Text != "") &&
                    (txtAppKey.Text != null && txtAppKey.Text != "") && (txtSecretKey.Text != null && txtSecretKey.Text != "")
                    && (txtMarketAppKey.Text != null && txtMarketAppKey.Text != "") && (txtMarketSecretKey.Text != null && txtMarketSecretKey.Text != "")
                    && (txtLogin.Text != null && txtLogin.Text != ""))
                    {
                        message = "Do you want to save the changes for " + userid + "?";
                        dialog = System.Windows.Forms.MessageBox.Show(message, title, btnYesNo, MessageBoxIcon.Warning);
                        if (dialog == DialogResult.Yes)
                        {
                            if (bAutomationasserver)
                            {
                                iniFile = ConfigFilePath + "\\ZerodhaSettings.ini";
                            }
                            else
                            {
                                iniFile = ConfigFilePath + "\\ZerodhaSettings_" + userid + ".ini";
                            }
                            objINI = new INIFile(iniFile);
                            //If username changes then it should delete the previous files
                            if (AccountList.SelectedItem.ToString() != "Add New Account")
                            {
                                newAccount = false;
                                if (File.Exists(ConfigFilePath + "\\ZerodhaSettings_" + oldSelected + ".ini"))
                                {
                                    File.Delete(ConfigFilePath + "\\ZerodhaSettings_" + oldSelected + ".ini");
                                }
                                //if (File.Exists(ConfigFilePath + "\\StrategySetting_" + oldSelected + ".ini"))
                                //{
                                //    File.Delete(ConfigFilePath + "\\StrategySetting_" + oldSelected + ".ini");
                                //}
                            }
                            //status = ZerodhaDetailsSave(objINI, iniFile);
                            //sanika::30-Apr-2021::For demo purpose
                            string iniFile1 = ConfigFilePath + "\\ZerodhaSettings_" + userid + ".ini";
                            INIFile iNI = new INIFile(iniFile1);
                            if (File.Exists(iniFile1))
                            {
                                File.Delete(iniFile1);
                            }
                            //string iniFile2 = ConfigFilePath + "\\StrategySetting_" + userid + ".ini";
                            //if (File.Exists(iniFile2))
                            //{
                            //    File.Delete(iniFile2);
                            //}
                            status = ZerodhaDetailsSave(iNI, iniFile1);
                            //IRDSPM::PRatiksha::26-04-2021::For creating new strategy setting form
                            if (status == true && newAccount == true)
                            {
                                CreateNewStrategyFile();
                            }
                            SaveDetailsToAccountSettings("ZerodhaAccountNumbers");
                            message = "Details saved successfully!";
                            dialog = System.Windows.Forms.MessageBox.Show(message, title, btnOk, MessageBoxIcon.Information);

                            LoadComboboxForAccount();
                            if (!AccountList.Items.Contains(userid))
                            {
                                AccountList.Items.Add(userid);
                            }
                            AccountList.SelectedItem = userid;
                            ClearAllFields();
                        }
                        else
                        {
                            //Data not saved
                        }
                    }
                    else
                    {
                        string parameter = "";
                        if (userid == "") { parameter += "\nUser name"; }
                        if (txtRoot.Text == "") { parameter += "\nRoot"; }
                        if (txtAppKey.Text == "") { parameter += "\nApp Key"; }
                        if (txtSecretKey.Text == "") { parameter += "\nSecret Key"; }
                        if (txtMarketAppKey.Text == "") { parameter += "\nMarket App Key"; }
                        if (txtMarketSecretKey.Text == "") { parameter += "\nMarket Secret Key"; }
                        if (txtLogin.Text == "") { parameter += "\nLogin"; }
                        message = "Please enter proper values for following field: " + parameter;
                        dialog = System.Windows.Forms.MessageBox.Show(message, title, btnOk, MessageBoxIcon.Information);
                        WriteUniquelogs("TradingSettingLogs", "btnApply_Click : Please enter proper values.", MessageType.Informational);
                    }
                }
                else if (CompositeTrue.Checked == true)
                {
                    bool newAccount = true;
                    if ((userid != null && userid != "") && (txtRoot.Text != null && txtRoot.Text != "") &&
                    (txtAppKey.Text != null && txtAppKey.Text != "") && (txtSecretKey.Text != null && txtSecretKey.Text != "")
                    && (txtMarketAppKey.Text != null && txtMarketAppKey.Text != "") && (txtMarketSecretKey.Text != null && txtMarketSecretKey.Text != ""))
                    {
                        message = "Do you want to save the changes for " + userid + "?";
                        dialog = System.Windows.Forms.MessageBox.Show(message, title, btnYesNo, MessageBoxIcon.Warning);
                        if (dialog == DialogResult.Yes)
                        {
                            if (bAutomationasserver)
                            {
                                iniFile = ConfigFilePath + "\\CompositeSettings.ini";
                            }
                            else
                            {
                                iniFile = ConfigFilePath + "\\CompositeSettings_" + userid + ".ini";
                            }
                            objINI = new INIFile(iniFile);
                            //If username changes then it should delete the previous files
                            if (AccountList.SelectedItem.ToString() != "Add New Account")
                            {
                                newAccount = false;
                                if (File.Exists(ConfigFilePath + "\\CompositeSettings_" + oldSelected + ".ini"))
                                {
                                    File.Delete(ConfigFilePath + "\\CompositeSettings_" + oldSelected + ".ini");
                                }
                                //if (File.Exists(ConfigFilePath + "\\StrategySetting_" + oldSelected + ".ini"))
                                //{
                                //    File.Delete(ConfigFilePath + "\\StrategySetting_" + oldSelected + ".ini");
                                //}
                            }
                            //status = CompositeDetailsSave(objINI, iniFile);
                            //sanika::30-Apr-2021::For demo purpose
                            string iniFile1 = ConfigFilePath + "\\CompositeSettings_" + userid + ".ini";
                            INIFile iNI = new INIFile(iniFile1);
                            if (File.Exists(iniFile1))
                            {
                                File.Delete(iniFile1);
                            }

                            //string iniFile2 = ConfigFilePath + "\\StrategySetting_" + userid + ".ini";
                            //if (File.Exists(iniFile2))
                            //{
                            //    File.Delete(iniFile2);
                            //}
                            status = CompositeDetailsSave(iNI, iniFile1);
                            //IRDSPM::PRatiksha::26-04-2021::For creating new strategy setting form
                            if (status == true && newAccount == true)
                            {
                                CreateNewStrategyFile();
                            }
                            SaveDetailsToAccountSettings("CompositeAccountNumbers");
                            message = "Details saved successfully!";
                            dialog = System.Windows.Forms.MessageBox.Show(message, title, btnOk, MessageBoxIcon.Information);

                            LoadComboboxForAccount();
                            if (!AccountList.Items.Contains(userid))
                            {
                                AccountList.Items.Add(userid);
                            }
                            AccountList.SelectedItem = userid;
                            ClearAllFields();
                        }
                        else
                        {
                            //Data not saved
                        }
                    }
                    else
                    {
                        string parameter = "";
                        if (userid == "") { parameter += "\nUser name"; }
                        if (txtRoot.Text == "") { parameter += "\nRoot"; }
                        if (txtAppKey.Text == "") { parameter += "\nApp Key"; }
                        if (txtSecretKey.Text == "") { parameter += "\nSecret Key"; }
                        if (txtMarketAppKey.Text == "") { parameter += "\nMarket App Key"; }
                        if (txtMarketSecretKey.Text == "") { parameter += "\nMarket Secret Key"; }
                        message = "Please enter proper values for following field: " + parameter;
                        dialog = System.Windows.Forms.MessageBox.Show(message, title, btnOk, MessageBoxIcon.Information);
                        WriteUniquelogs("TradingSettingLogs", "btnApply_Click : Please enter proper values.", MessageType.Informational);
                    }
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("ConfigSettingsGUI", "btnSave_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        public void SaveDetailsToAccountSettings(string sectionName)
        {
            List<string> values = new List<string>();
            string filePath = ConfigFilePath + "\\AccountNumber.ini";
            INIFile iNIFile = new INIFile(filePath);
            values = iNIFile.GetKeyValues(sectionName);
            iNIFile.clearTestingSymbol(sectionName, null, null, filePath);
            if (values.Count > 0)
            {
                string s = "";
                string line = values.ElementAt(0);
                string[] accountNo = line.Split(',');
                foreach (var a in accountNo)
                {
                    s += a + ",";
                }

                string accountNumbers = s + txtUserid.Text;

                iNIFile.IniWriteValueSymbolSetting(sectionName, accountNumbers, filePath);
            }
            else
            {
                iNIFile.IniWriteValueSymbolSetting(sectionName, txtUserid.Text, filePath);
            }
        }
        /// <summary>
        /// Write Logs
        /// </summary>
        /// <param name="symbol"></param>
        /// <param name="message"></param>
        /// <param name="msgType"></param>
        public void WriteUniquelogs(string symbol, string message, MessageType msgType)
        {
            try
            {
                SymbolLogs log = new SymbolLogs();
                //log = SymbolLogs.InstanceSymbolLogs;
                log.createFile(symbol);
                log.LogMessage(message, MessageType.Informational);
                log = null;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : " + e.Message);
                Trace.WriteLine("Exception : " + e.Message);
            }
        }

        public void ClearAllFields()
        {
            try
            {
                txtRoot.Clear();
                txtAppKey.Clear();
                txtUserid.Clear();
                txtSecretKey.Clear();
                txtMarketAppKey.Clear();
                txtMarketSecretKey.Clear();
                if (AccountList.Items.Count == 0)
                {
                    AccountList.Items.Add("Add New Account");
                }
                txtLogin.Clear();
                AccountList.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                WriteUniquelogs("ConfigSettingsGUI", "ClearAllFields : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (AccountList.SelectedIndex == 0)
                {
                    message = "Please select account to delete!";
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, btnOk, MessageBoxIcon.Warning);
                }
                else
                {
                    message = "Do you want to save the changes?";
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, btnYesNo, MessageBoxIcon.Warning);

                    if (dialog == DialogResult.Yes)
                    {
                        if (File.Exists(iniFile))
                        {
                            File.Delete(iniFile);
                            string StrategyIniFile = ConfigFilePath + "\\ChanBreakoutStrategy_" + txtUserid.Text + ".ini";
                            if (File.Exists(StrategyIniFile))
                            {
                                File.Delete(StrategyIniFile);
                            }
                            AccountList.Items.Remove(txtUserid.Text);
                            ClearAllFields();
                            message = "Details deleted successfully!";
                            dialog = System.Windows.Forms.MessageBox.Show(message, title, btnOk, MessageBoxIcon.Information);
                        }
                        else
                        {
                            message = "Details already deleted!";
                            dialog = System.Windows.Forms.MessageBox.Show(message, title, btnOk, MessageBoxIcon.Warning);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("ConfigSettingsGUI", "btnDelete_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!bAutomationasserver)
                {
                    if (AccountList.SelectedItem.ToString() != "Add New Account")
                    {
                        GBChooseBroker.Enabled = false;
                        ClearFieldExceptAccList();
                        if (objConfigSettings == null)
                        {
                            objConfigSettings = new ReadSettings(m_logger);
                        }
                        if (txtUserid.Text == "" && txtRoot.Text == "" && txtAppKey.Text == "" && txtSecretKey.Text == "" && txtMarketAppKey.Text == "")
                        {

                        }
                        else if (objConfigSettings.username == txtUserid.Text && objConfigSettings.root == txtRoot.Text && objConfigSettings.appKey == txtAppKey.Text
                            && objConfigSettings.secretKey == txtSecretKey.Text && objConfigSettings.marketDataAppKey == txtMarketAppKey.Text
                            && objConfigSettings.marketDataSecretKey == txtMarketSecretKey.Text)
                        {

                        }
                        else
                        {
                            if (status == false)
                            {

                                string userid = txtUserid.Text.Trim();
                                if ((userid != null && userid != "") && (txtRoot.Text != null && txtRoot.Text != "") &&
                                (txtAppKey.Text != null && txtAppKey.Text != "") && (txtSecretKey.Text != null && txtSecretKey.Text != "")
                                && (txtMarketAppKey.Text != null && txtMarketAppKey.Text != "") && (txtMarketSecretKey.Text != null && txtMarketSecretKey.Text != ""))
                                {

                                    message = "Do you want to save details for account " + userid + "?";
                                    dialog = System.Windows.Forms.MessageBox.Show(message, title, btnYesNo, MessageBoxIcon.Warning);
                                    if (dialog == DialogResult.Yes)
                                    {
                                        iniFile = ConfigFilePath + "\\CompositeSettings_" + userid + ".ini";
                                        //WriteValuesToIniFile(iniFile);
                                        CompositeDetailsSave(objINI, iniFile);
                                    }
                                }
                                else
                                {
                                    string parameter = "";
                                    if (userid == "") { parameter += "\nUser name"; }
                                    if (txtRoot.Text == "") { parameter += "\nRoot"; }
                                    if (txtAppKey.Text == "") { parameter += "\nApp Key"; }
                                    if (txtSecretKey.Text == "") { parameter += "\nSecret Key"; }
                                    if (txtMarketAppKey.Text == "") { parameter += "\nMarket App Key"; }
                                    if (txtMarketSecretKey.Text == "") { parameter += "\nMarket Secret Key"; }
                                    message = "Please enter proper values for following fields: " + parameter;
                                    dialog = System.Windows.Forms.MessageBox.Show(message, title, btnOk, MessageBoxIcon.Information);
                                    WriteUniquelogs("TradingSettingLogs", "btnApply_Click : Please enter proper values.", MessageType.Informational);
                                }
                            }
                            else
                            {
                                //No need to make changes
                            }
                        }
                        if (m_CompositeAccountNumbers.Contains(AccountList.SelectedItem.ToString()))
                        {
                            CompositeTrue.Checked = true;
                            iniFile = ConfigFilePath + "\\CompositeSettings_" + AccountList.SelectedItem.ToString() + ".ini";
                            if (File.Exists(iniFile))
                            {
                                if (objConfigSettings == null)
                                {
                                    objConfigSettings = new ReadSettings(m_logger);
                                }
                                objConfigSettings.readCompositeConfigFile(iniFile);
                                txtRoot.Text = objConfigSettings.root;
                                txtUserid.Text = objConfigSettings.username;
                                txtAppKey.Text = objConfigSettings.appKey;
                                txtSecretKey.Text = objConfigSettings.secretKey;
                                txtMarketAppKey.Text = objConfigSettings.marketDataAppKey;
                                txtMarketSecretKey.Text = objConfigSettings.marketDataSecretKey;
                                oldSelected = txtUserid.Text;
                                txtLogin.Text = "";
                            }
                            lblRoot.Text = "Root";
                            lblUserID.Text = "User ID";
                            lblAppKey.Text = "APP Key";
                            lblSecretKey.Text = "Secret Key";
                            lblMarketAppKey.Text = "Market App Key";
                            lblMarketSecretKey.Text = "Market Data secretKey";
                            lblLogin.Text = "Login";
                            txtLogin.Enabled = false;
                        }
                        else if (m_ZerodhaAccountNumbers.Contains(AccountList.SelectedItem.ToString()))
                        {
                            ZerodhaTrue.Checked = true;
                            iniFile = ConfigFilePath + "\\ZerodhaSettings_" + AccountList.SelectedItem.ToString() + ".ini";
                            if (File.Exists(iniFile))
                            {
                                if (objConfigSettings == null)
                                {
                                    objConfigSettings = new ReadSettings(m_logger);
                                }
                                objConfigSettings.readConfigFileZerodha(iniFile);
                                txtRoot.Text = objConfigSettings.Zroot;
                                txtUserid.Text = objConfigSettings.Zuserid;
                                txtAppKey.Text = objConfigSettings.ZappKey;
                                txtSecretKey.Text = objConfigSettings.ZsecretKey;
                                txtMarketAppKey.Text = objConfigSettings.ZPassword;
                                txtMarketSecretKey.Text = objConfigSettings.ZPin;
                                txtLogin.Text = objConfigSettings.Zlogin;
                                oldSelected = txtUserid.Text;
                            }
                            lblRoot.Text = "Root";
                            lblUserID.Text = "User ID";
                            lblAppKey.Text = "API Key";
                            lblSecretKey.Text = "Secret Key";
                            lblMarketAppKey.Text = "Password";
                            lblMarketSecretKey.Text = "Pin";
                            lblLogin.Text = "Login";
                            txtLogin.Enabled = true;
                        }
                    }
                    else
                    {
                        GBChooseBroker.Enabled = true;
                        ClearAllFields();
                    }
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("ConfigSettingsGUI", "comboBox1_SelectedIndexChanged : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void ClearFieldExceptAccList()
        {
            txtRoot.Clear();
            txtAppKey.Clear();
            txtUserid.Clear();
            txtSecretKey.Clear();
            txtMarketAppKey.Clear();
            txtMarketSecretKey.Clear();
            txtLogin.Clear();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                WriteUniquelogs("ConfigSettingsGUI", "btnCancel_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            try
            {
                ClearAllFields();
            }
            catch (Exception ex)
            {
                WriteUniquelogs("ConfigSettingsGUI", "btnNew_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }
        //IRDSPM::PRatiksha::26-04-2021::For creating new strategy setting form
        private void CreateNewStrategyFile()
        {
            try
            {
                iniFile = ConfigFilePath + "\\ChanBreakoutStrategy_" + txtUserid.Text.Trim() + ".ini";
                objINI = new INIFile(iniFile);

                //objINI.IniWriteValue("SDATA", "AccountNumber", txtUserid.Text.Trim());
                objINI.IniWriteValue("SDATA", "startTime", "09:20");
                objINI.IniWriteValue("SDATA", "endTime", "15:00");
                objINI.IniWriteValue("SDATA", "TimeLimitToPlaceOrder", "14:00");
                objINI.IniWriteValue("SDATA", "TimeForExitAllOpenPosition", "15:10");
                objINI.IniWriteValue("SDATA", "PercentQuantity", "15");
                objINI.IniWriteValue("SDATA", "totalmoney", "60000");
                objINI.IniWriteValue("SDATA", "BarInterval", "1");
                objINI.IniWriteValue("SDATA", "maxStrategyOrderCount", "64");
                objINI.IniWriteValue("SDATA", "individualCount", "4");
                objINI.IniWriteValue("SDATA", "daysForHistroricalData", "7");
                objINI.IniWriteValue("SDATA", "PriceDiffPercent", "10");
                objINI.IniWriteValue("SDATA", "PlacedOrderPriceDiffPercent", "5");
                objINI.IniWriteValue("SDATA", "RiskPercentForStock", "1");
                objINI.IniWriteValue("SDATA", "RiskPercentForFuture", "5");
                objINI.IniWriteValue("SDATA", "readDataFromDB", "true");
                objINI.IniWriteValue("SDATA", "AutoDownloadHistoricalData", "true");
                objINI.IniWriteValue("SDATA", "IsNeedToParallel", "true");
                objINI.IniWriteValue("SDATA", "MaxLossValue", "2");
                objINI.IniWriteValue("SDATA", "MaxLossPercent", "0.25");
                objINI.IniWriteValue("SDATA", "OverallLoss", "2000");
                objINI.IniWriteValue("SDATA", "OverallProfitAmt", "2000");
                objINI.IniWriteValue("SDATA", "OverallProfitPercent", "1");
                objINI.IniWriteValue("SDATA", "TotalOpenPositions", "20");
                objINI.IniWriteValue("SDATA", "StopLossChangeTime", "14:30");
               // objINI.IniWriteValue("SDATA", "AdditionalAccount", txtUserid.Text.Trim());
                //IRDSPM::Pratiksha::Added new parameter order type
                objINI.IniWriteValueSymbolSetting("TRADINGSYMBOL", "ADANIPORTS.NSE,1,1,1,1,1,1,1,1,1,1,1,1", iniFile);
                objINI.IniWriteValueSymbolSetting("TRADINGSYMBOL", "ASIANPAINT.NSE,2,2,2,2,2,2,22,22,22,22,22,22", iniFile);
            }
            catch (Exception ex)
            {
                WriteUniquelogs("ConfigSettingsGUI", "CreateNewStrategyFile : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        List<string> PlainDetListZerodha;
        List<string> EncDetListZerodha;
        private bool ZerodhaDetailsSave(INIFile objINI, string iniFile)
        {
            bool Status = false;
            try
            {
                PlainDetListZerodha = new List<string>();
                string[] key = new string[7];
                string[] value = new string[7];
                key[0] = "root";
                key[1] = "MyUserId"; //Sanika::15-Feb-2021::changed name because Same for all broker
                key[2] = "MyAPIKey";
                key[3] = "MySecret";
                key[4] = "MyPassword";
                key[5] = "Pin";
                key[6] = "login";
                value[0] = txtRoot.Text;
                value[1] = txtUserid.Text.Trim();
                value[2] = txtAppKey.Text;
                value[3] = txtSecretKey.Text;
                value[4] = txtMarketAppKey.Text;
                value[5] = txtMarketSecretKey.Text;
                value[6] = txtLogin.Text;
                PlainDetListZerodha.Add(value[1]);
                PlainDetListZerodha.Add(value[2]);
                PlainDetListZerodha.Add(value[3]);
                PlainDetListZerodha.Add(value[4]);
                PlainDetListZerodha.Add(value[5]);

                if (PlainDetListZerodha[1] != "XXXXXX" || PlainDetListZerodha[2] != "XXXXXX" || PlainDetListZerodha[3] != "XXXXXX" || PlainDetListZerodha[4] != "XXXXXX" || PlainDetListZerodha[5] != "XXXXXX")
                {
                    encryptDetailsZerodha(PlainDetListZerodha);

                    for (int i = 0; i < 7; i++)
                    {
                        if (i == 1 || i == 2 || i == 3 || i == 4 || i == 5)
                        {
                            objINI.IniWriteValue("credentials", key[i], EncDetListZerodha[i - 1]);
                        }
                        else
                        {
                            objINI.IniWriteValue("credentials", key[i], value[i]);
                        }
                    }
                    WriteUniquelogs("settingLogs" + " ", "ZerodhaDetailsSave: Data updated from broker setting to apisetting.ini file.", MessageType.Informational);
                    Status = true;
                }
                else
                {
                    message = "Please add proper values.";
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, btnOk, MessageBoxIcon.Information);
                    Status = false;
                }
            }
            catch (Exception ex)
            {
                Status = false;
                WriteUniquelogs("settingLogs" + " ", "CompositeDetailsSave : " + ex.Message, MessageType.Informational);
            }
            return Status;
        }


        List<string> PlainDetListComposite;
        List<string> EncDetList;
        private bool CompositeDetailsSave(INIFile objINI, string iniFile)
        {
            bool Status = false;
            try
            {
                PlainDetListComposite = new List<string>();
                string[] key = new string[6];
                string[] value = new string[6];
                key[0] = "root";
                key[1] = "MyUserId"; //Sanika::15-Feb-2021::changed name because Same for all broker
                key[2] = "appKey";
                key[3] = "secretKey";
                key[4] = "MarketDataappKey";
                key[5] = "MarketDatasecretKey";

                value[0] = txtRoot.Text.Trim();
                value[1] = txtUserid.Text.Trim();
                value[2] = txtAppKey.Text.Trim();
                value[3] = txtSecretKey.Text.Trim();
                value[4] = txtMarketAppKey.Text.Trim();
                value[5] = txtMarketSecretKey.Text.Trim();

                PlainDetListComposite.Add(value[1]);
                PlainDetListComposite.Add(value[2]);
                PlainDetListComposite.Add(value[3]);
                PlainDetListComposite.Add(value[4]);
                PlainDetListComposite.Add(value[5]);

                if (PlainDetListComposite[1] != "XXXXXX" || PlainDetListComposite[2] != "XXXXXX" || PlainDetListComposite[3] != "XXXXXX" || PlainDetListComposite[4] != "XXXXXX" || PlainDetListComposite[5] != "XXXXXX")
                {
                    encryptDetails(PlainDetListComposite);

                    for (int i = 0; i < 6; i++)
                    {
                        if (i == 1 || i == 2 || i == 3 || i == 4 || i == 5)
                        {
                            objINI.IniWriteValue("credentials", key[i], EncDetList[i - 1]);
                        }
                        else
                        {
                            objINI.IniWriteValue("credentials", key[i], value[i]);
                        }
                    }
                    WriteUniquelogs("settingLogs" + " ", "CompositeDetailsSave: Data updated from broker setting to apisetting.ini file.", MessageType.Informational);
                    Status = true;
                }
                else
                {
                    message = "Please add proper values.";
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, btnOk, MessageBoxIcon.Information);
                    Status = false;
                }
            }
            catch (Exception ex)
            {
                Status = false;
                WriteUniquelogs("settingLogs" + " ", "CompositeDetailsSave : " + ex.Message, MessageType.Informational);
            }
            return Status;
        }
        public void encryptDetails(List<string> plainDet)
        {
            try
            {
                EncDetList = new List<string>();
                for (int i = 0; i < plainDet.Count; i++)
                {
                    string enc = EncryptData(plainDet[i], "IrdsalgoTrader");
                    EncDetList.Add(enc);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "encryptDetails : Exception Error Message =  " + ex.Message, MessageType.Exception);
            }
        }

        public void encryptDetailsZerodha(List<string> plainDet)
        {
            try
            {
                EncDetListZerodha = new List<string>();
                for (int i = 0; i < plainDet.Count; i++)
                {
                    string enc = EncryptData(plainDet[i], "IrdsalgoTrader");
                    EncDetListZerodha.Add(enc);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "encryptDetails : Exception Error Message =  " + ex.Message, MessageType.Exception);
            }
        }
        public static string EncryptData(string strInText, string strKey)
        {
            byte[] bytesBuff = Encoding.Unicode.GetBytes(strInText);

            using (Aes aes__1 = Aes.Create())
            {
                System.Security.Cryptography.Rfc2898DeriveBytes crypto = new Rfc2898DeriveBytes(strKey, new byte[] {
                0x49,
                0x76,
                0x61,
                0x6e,
                0x20,
                0x4d,
                0x65,
                0x64,
                0x76,
                0x65,
                0x64,
                0x65,
                0x76
            });
                aes__1.Key = crypto.GetBytes(32);
                aes__1.IV = crypto.GetBytes(16);

                using (MemoryStream mStream = new MemoryStream())
                {
                    using (CryptoStream cStream = new CryptoStream(mStream, aes__1.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cStream.Write(bytesBuff, 0, bytesBuff.Length);
                        cStream.Close();
                    }

                    strInText = Convert.ToBase64String(mStream.ToArray());
                }
            }

            return strInText;
        }

        private void ZerodhaTrue_CheckedChanged(object sender, EventArgs e)
        {
            if (ZerodhaTrue.Checked == true)
            {
                ClearFieldExceptAccList();
                lblRoot.Text = "Root";
                lblUserID.Text = "User ID";
                lblAppKey.Text = "API Key";
                lblSecretKey.Text = "Secret Key";
                lblMarketAppKey.Text = "Password";
                lblMarketSecretKey.Text = "Pin";
                lblLogin.Text = "Login";
                txtLogin.Enabled = true;
            }
        }

        private void CompositeTrue_CheckedChanged(object sender, EventArgs e)
        {
            if (CompositeTrue.Checked == true)
            {
                ClearFieldExceptAccList();
                lblRoot.Text = "Root";
                lblUserID.Text = "User ID";
                lblAppKey.Text = "APP Key";
                lblSecretKey.Text = "Secret Key";
                lblMarketAppKey.Text = "Market App Key";
                lblMarketSecretKey.Text = "Market Data secretKey";
                lblLogin.Text = "Login";
                txtLogin.Enabled = false;
            }
        }
    }
}
