﻿using IRDSStategyExecutorMultiAccount;
using IRDSAlgoOMS;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;

namespace IRDSStategyExecutorMultiAccount.GUI
{
    public partial class SymbolSetting : Form
    {
        string inisymbolList;
        IRDSStategyExecutorMultiAccount.ReadSettings Settings;
        IRDSAlgoOMS.Logger logger = IRDSAlgoOMS.Logger.Instance;
        string title = "RealStockA1 Strategy";
        string message = "";
        MessageBoxButtons buttons = MessageBoxButtons.OK;
        DialogResult dialog;
        dynamic ConnectWrapper = null;
        bool isexit = false;
        string path = System.AppDomain.CurrentDomain.BaseDirectory + "Configuration";
        string m_userID = "";
        List<string> finalNSEList = new List<string>();
        List<string> finalNFOList = new List<string>();
        string m_futureNameForTable = "";
        public SymbolSetting(AlgoOMS obj, IRDSStategyExecutorMultiAccount.ReadSettings readSettings, string m_FrmTitle, string userID)
        {
            m_userID = userID;
            Assembly myAssembly = Assembly.GetExecutingAssembly();
            Stream IconStream = null;
            IconStream = myAssembly.GetManifestResourceStream("IRDSStategyExecutorMultiAccount.Resources." + m_FrmTitle + ".ico");
            this.Icon = new System.Drawing.Icon(IconStream);
            InitializeComponent();
            try
            {
                Settings = readSettings;
                GetSymbolsFromDB(obj);
                FetchDetailsFromINI(path);
                RBEquity.Checked = true;
                WriteUniquelogs("SymbolSetting", "SymbolSetting : loaded symbols and fetched details properly.", MessageType.Informational);
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSetting", "SymbolSetting : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }
        private void btnApply_Click(object sender, EventArgs e)
        {
            try
            {
                if (TradingSymbolList.Text.Length == 0 && txtNA1.Text.Length == 0 && txtNA2.Text.Length == 0 && txtNA3.Text.Length == 0 && txtNA4.Text.Length == 0 && txtNA5.Text.Length == 0 && txtperiodOfbar.Text.Length == 0 && txtprofitpercent.Text.Length == 0 && txtstoploss.Text.Length == 0)
                {
                    message = "Do you want to save the changes?";
                    buttons = MessageBoxButtons.YesNo;
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                    if (dialog == DialogResult.Yes)
                    {
                        AddIntoINIFile();
                        this.Close();
                    }
                }
                else
                {
                    //Pratiksha::19-08-2021::2 New field StopLossChange and UpdatedStopLossChange
                    if (TradingSymbolList.Text.Length > 0 && txtNA1.Text.ToString().Length > 0 && txtNA2.Text.ToString().Length > 0 && txtNA3.Text.ToString().Length > 0 && txtNA4.Text.ToString().Length > 0 && txtNA5.Text.ToString().Length > 0 && txtperiodOfbar.Text.ToString().Length > 0 && txtprofitpercent.Text.ToString().Length > 0 && txtstoploss.Text.ToString().Length > 0 && txtlNiftyPercent.Text.ToString().Length > 0 && txtBankNiftyPercent.Text.ToString().Length > 0 && txtStopLossChange.Text.ToString().Length > 0 && txtupdatedStoplossChange.Text.ToString().Length > 0)
                    {
                        Match PeriodOfbar1, Profitpercent1, Stoploss1, NA1, NA2, NA3, NA4, NA5, NiftyPer1, Banknifty1, StopLossChanged1, UpdatedStopLossChanged1;
                        Validations(out PeriodOfbar1, out Profitpercent1, out Stoploss1, out NA1, out NA2, out NA3, out NA4, out NA5, out NiftyPer1, out Banknifty1, out StopLossChanged1, out UpdatedStopLossChanged1);
                        if (PeriodOfbar1.Success && Profitpercent1.Success && Stoploss1.Success && NA1.Success && NA2.Success && NA3.Success && NA4.Success && NA5.Success && NiftyPer1.Success && Banknifty1.Success && StopLossChanged1.Success && UpdatedStopLossChanged1.Success)
                        {
                            message = "Do you want to save the changes?";
                            buttons = MessageBoxButtons.YesNo;
                            dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                            if (dialog == DialogResult.Yes)
                            {
                                AddIntoINIFile();
                            }
                        }
                        else
                        {
                            string Logmessage = "";
                            string parameter = "";
                            if (!NA1.Success) { parameter += "\nQuantity"; Logmessage += "Quantity,"; }
                            if (!PeriodOfbar1.Success) { parameter += "\nPeriod Of Bar"; Logmessage += "Period Of Bar,"; }
                            if (!Profitpercent1.Success) { parameter += "\nProfit Percent"; Logmessage += "Profit Percent,"; }
                            if (!NA2.Success) { parameter += "\nHigh Low Percent"; Logmessage += "High Low Percent,"; }
                            if (!Stoploss1.Success) { parameter += "\nStoploss"; Logmessage += "Stoploss,"; }
                            if (!NA3.Success) { parameter += "\nUpdated Stoploss"; Logmessage += "Updated Stoploss,"; }
                            if (!NA4.Success) { parameter += "\nProfit Trail"; Logmessage += "Profit Trail,"; }
                            if (!NA5.Success) { parameter += "\nUpdated Profit Trail"; Logmessage += "Updated Profit Trail"; }
                            if (!NiftyPer1.Success) { parameter += "\nNifty Percent"; Logmessage += "Nifty Percent,"; }
                            if (!Banknifty1.Success) { parameter += "\nBankNifty Percent"; Logmessage += "nBankNifty Percent"; }
                            //Pratiksha::19-08-2021::2 New field StopLossChange and UpdatedStopLossChange
                            if (!StopLossChanged1.Success) { parameter += "\nStopLoss Changed"; Logmessage += "StopLoss Changed,"; }
                            if (!UpdatedStopLossChanged1.Success) { parameter += "\nUpdated StopLoss Changed"; Logmessage += "Updated StopLoss Changed"; }
                            message = "Please add valid values for " + parameter + ".";
                            buttons = MessageBoxButtons.OK;
                            dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                            WriteUniquelogs("SymbolSetting", "btnApply_Click : " + Logmessage, MessageType.Informational);
                        }
                    }
                    else
                    {
                        CheckNullValuesAndShowPopup();
                    }
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSetting", "btnApply_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void AddIntoINIFile(bool IsDisplay)
        {
            bool issaved = false;
            bool isAdded = false;
            int j = 0;
            int counter = 0;
            int k = dataGridView.Rows.Count;
            bool flagSymbolname = false;
            var re = @"^[0-9]{2}[a-zA-Z]{3}[0-9]{2}$";
            INIFile iniObj = new INIFile(inisymbolList);
            DialogResult dialog;
            try
            {
                foreach (DataGridViewRow row in dataGridView.Rows)
                {
                    flagSymbolname = false;
                    if ((row.Cells["TradingSymbol"].Value.ToString() == null) || (row.Cells["Quantity"].Value.ToString() == null) || (row.Cells["PeriodOfBar"].Value.ToString() == null) || (row.Cells["ProfitPercent"].Value.ToString() == null) || (row.Cells["highLowPercent"].Value.ToString() == null) || (row.Cells["Stoploss"].Value.ToString() == null) || (row.Cells["updatedStoploss"].Value.ToString() == null) || (row.Cells["profitTrail"].Value.ToString() == null) || (row.Cells["Updatedprofittrail"].Value.ToString() == null) || (row.Cells["NiftyPercent"].Value.ToString() == null) || (row.Cells["BankNiftyPercent"].Value.ToString() == null) || (row.Cells["StopLossChange"].Value.ToString() == null) || (row.Cells["UpdatedStopLossChange"].Value.ToString() == null))
                    {
                    }
                    else
                    {
                        //Pratiksha::19-08-2021::2 New field StopLossChange and UpdatedStopLossChange
                        if (TradingSymbolList.Text.Length > 0 && txtNA1.Text.ToString().Length > 0 && txtNA2.Text.ToString().Length > 0 && txtNA3.Text.ToString().Length > 0 && txtNA4.Text.ToString().Length > 0 && txtNA5.Text.ToString().Length > 0 && txtperiodOfbar.Text.ToString().Length > 0 && txtprofitpercent.Text.ToString().Length > 0 && txtstoploss.Text.ToString().Length > 0 && txtlNiftyPercent.Text.ToString().Length > 0 && txtBankNiftyPercent.Text.ToString().Length > 0 && txtStopLossChange.Text.ToString().Length > 0 && txtupdatedStoplossChange.Text.ToString().Length > 0)
                        {
                            string symbol = GetExchangeOnSymbolBasis(TradingSymbolList.Text);
                            if (symbol == row.Cells["TradingSymbol"].Value.ToString())
                            {
                                //Pratiksha::19-08-2021::2 New field StopLossChange and UpdatedStopLossChange
                                if ((row.Cells[0].Value.Equals(symbol)) && (row.Cells[1].Value.Equals(txtNA1.Text)) && (row.Cells[2].Value.Equals(txtperiodOfbar.Text.ToString())) && (row.Cells[3].Value.Equals(txtprofitpercent.Text.ToString())) && (row.Cells[4].Value.Equals(txtNA2.Text.ToString())) && (row.Cells[5].Value.Equals(txtstoploss.Text.ToString())) && (row.Cells[6].Value.Equals(txtNA3.Text.ToString())) && (row.Cells[7].Value.Equals(txtNA4.Text.ToString())) && (row.Cells[8].Value.Equals(txtNA5.Text.ToString())) && (row.Cells[9].Value.Equals(txtlNiftyPercent.Text.ToString())) && (row.Cells[10].Value.Equals(txtBankNiftyPercent.Text.ToString())) && (row.Cells[11].Value.Equals(txtStopLossChange.Text.ToString())) && (row.Cells[12].Value.Equals(txtupdatedStoplossChange.Text.ToString())))
                                {
                                    if (AddUpdateRowinDataGrid(0, false) == true)
                                    {
                                        k += 1;
                                        TradingSymbolList.SelectedIndex = -1;
                                        ClearFields();
                                        issaved = true;
                                        isAdded = true;
                                    }
                                    else
                                    {
                                        issaved = false;
                                    }
                                }
                                else
                                {
                                    foreach (DataGridViewRow r in dataGridView.SelectedRows)
                                    {
                                        string sym = GetExchangeOnSymbolBasis(TradingSymbolList.Text);
                                        r.Cells["TradingSymbol"].Value = sym;
                                        r.Cells["Quantity"].Value = txtNA1.Text;
                                        r.Cells["PeriodOfBar"].Value = txtperiodOfbar.Text;
                                        r.Cells["ProfitPercent"].Value = txtprofitpercent.Text;
                                        r.Cells["highLowPercent"].Value = txtNA2.Text;
                                        r.Cells["Stoploss"].Value = txtstoploss.Text;
                                        r.Cells["updatedStoploss"].Value = txtNA3.Text;
                                        r.Cells["profitTrail"].Value = txtNA4.Text;
                                        r.Cells["Updatedprofittrail"].Value = txtNA5.Text;
                                        r.Cells["NiftyPercent"].Value = txtlNiftyPercent.Text;
                                        r.Cells["BankNiftyPercent"].Value = txtBankNiftyPercent.Text;
                                        //Pratiksha::19-08-2021::2 New field StopLossChange and UpdatedStopLossChange
                                        r.Cells["StopLossChange"].Value = txtStopLossChange.Text;
                                        r.Cells["UpdatedStopLossChange"].Value = txtupdatedStoplossChange.Text;
                                        TradingSymbolList.SelectedIndex = -1;
                                        ClearFields();
                                        issaved = true;
                                    }
                                }
                            }
                            else
                            {
                                if (isAdded == false)
                                {
                                    bool isFound = false;
                                    foreach (DataGridViewRow r in dataGridView.Rows)
                                    {
                                        string sym = GetExchangeOnSymbolBasis(TradingSymbolList.Text);
                                        if (sym == row.Cells["TradingSymbol"].Value.ToString())
                                        {
                                            isFound = true;
                                            break;
                                        }
                                        else
                                        {
                                            isFound = false;
                                        }
                                    }
                                    if (isFound == false)
                                    {
                                        if (AddUpdateRowinDataGrid(0, false) == true)
                                        {
                                            k += 1;
                                            TradingSymbolList.SelectedIndex = -1;
                                            ClearFields();
                                            issaved = true;
                                            isAdded = true;
                                        }
                                        else
                                        {
                                            issaved = false;
                                        }
                                    }
                                }
                            }
                        }
                        if (((row.Cells["TradingSymbol"].Value.ToString().Length == 0) || (row.Cells["Quantity"].Value.ToString().Length == 0) || (row.Cells["PeriodOfBar"].Value.ToString().Length == 0) || (row.Cells["ProfitPercent"].Value.ToString().Length == 0) || (row.Cells["highLowPercent"].Value.ToString().Length == 0) || (row.Cells["Stoploss"].Value.ToString().Length == 0) || (row.Cells["updatedStoploss"].Value.ToString().Length == 0) || (row.Cells["profitTrail"].Value.ToString().Length == 0) || (row.Cells["Updatedprofittrail"].Value.ToString().Length == 0) || (row.Cells["NiftyPercent"].Value.ToString().Length == 0) || (row.Cells["BankNiftyPercent"].Value.ToString().Length == 0) ||(row.Cells["StopLossChange"].Value.ToString().Length == 0) || (row.Cells["UpdatedStopLossChange"].Value.ToString().Length == 0)))
                        {
                            message = "Not able to add this because null value is present.";
                            buttons = MessageBoxButtons.OK;
                            dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                            WriteUniquelogs("SymbolSetting", "AddIntoINIFile : Null value present at :" + row.Cells["TradingSymbol"].Value.ToString() + row.Cells["Quantity"].Value.ToString() + (row.Cells["PeriodOfBar"].Value.ToString().Length == 0) + (row.Cells["ProfitPercent"].Value.ToString().Length == 0) + (row.Cells["highLowPercent"].Value.ToString().Length == 0) + (row.Cells["Stoploss"].Value.ToString().Length == 0) + (row.Cells["updatedStoploss"].Value.ToString().Length == 0) + (row.Cells["profitTrail"].Value.ToString().Length == 0) + (row.Cells["Updatedprofittrail"].Value.ToString().Length == 0) + (row.Cells["NiftyPercent"].Value.ToString().Length == 0) + (row.Cells["BankNiftyPercent"].Value.ToString().Length == 0) + (row.Cells["StopLossChange"].Value.ToString().Length == 0) + (row.Cells["UpdatedStopLossChange"].Value.ToString().Length == 0) + ". Not able to add this value.", MessageType.Informational);
                        }
                        else
                        {
                            j++;
                        }
                    }
                }

                if ((j == k) && (issaved = true))
                {
                    if (Settings == null)
                    {
                        Settings = new IRDSStategyExecutorMultiAccount.ReadSettings(logger);
                    }
                    iniObj.clearTestingSymbol("TRADINGSYMBOL", null, null, inisymbolList);
                    foreach (DataGridViewRow row in dataGridView.Rows)
                    {
                        Settings.writeINIFileWithPath("TRADINGSYMBOL", row.Cells["TradingSymbol"].Value.ToString().ToUpper() + "," + (row.Cells["Quantity"].Value.ToString().ToUpper()) + "," + (row.Cells["PeriodOfBar"].Value.ToString().ToUpper()) + /*"," + Strikevalue + */"," + (row.Cells["ProfitPercent"].Value.ToString()) + "," + (row.Cells["highLowPercent"].Value.ToString()) + "," + (row.Cells["Stoploss"].Value.ToString()) + "," + (row.Cells["updatedStoploss"].Value.ToString()) + "," + (row.Cells["profitTrail"].Value.ToString()) + "," + (row.Cells["Updatedprofittrail"].Value.ToString()) + "," + (row.Cells["NiftyPercent"].Value.ToString()) + "," + (row.Cells["BankNiftyPercent"].Value.ToString()) + "," + (row.Cells["StopLossChange"].Value.ToString()) + "," + (row.Cells["UpdatedStopLossChange"].Value.ToString()), inisymbolList);
                        counter++;
                        //string rowOutout = row.Cells["TradingSymbol"].Value.ToString().ToUpper() + "," + (row.Cells["NA1"].Value.ToString().ToUpper()) + "," + (row.Cells["PeriodOfBar"].Value.ToString().ToUpper()) +/* "," + Strikevalue +*/ "," + (row.Cells["ProfitPercent"].Value.ToString()) + "," + (row.Cells["NA2"].Value.ToString()) + "," + (row.Cells["Stoploss"].Value.ToString()) + "," + (row.Cells["NA3"].Value.ToString()) + "," + (row.Cells["NA4"].Value.ToString()) + "," + (row.Cells["NA5"].Value.ToString());
                        string rowOutout = row.Cells["TradingSymbol"].Value.ToString().ToUpper() + "," + (row.Cells["Quantity"].Value.ToString().ToUpper()) + "," + (row.Cells["PeriodOfBar"].Value.ToString().ToUpper()) +/* "," + Strikevalue +*/ "," + (row.Cells["ProfitPercent"].Value.ToString()) + "," + (row.Cells["highLowPercent"].Value.ToString()) + "," + (row.Cells["Stoploss"].Value.ToString()) + "," + (row.Cells["updatedStoploss"].Value.ToString()) + "," + (row.Cells["profitTrail"].Value.ToString()) + "," + (row.Cells["Updatedprofittrail"].Value.ToString()) +","+ (row.Cells["NiftyPercent"].Value.ToString()) + ","+ (row.Cells["BankNiftyPercent"].Value.ToString()) + "," + (row.Cells["StopLossChange"].Value.ToString()) + "," + (row.Cells["UpdatedStopLossChange"].Value.ToString());
                        WriteUniquelogs("SymbolSetting", "AddIntoINIFile :  Details saved " + rowOutout, MessageType.Informational);
                    }
                    if (IsDisplay == true)
                    {
                        message = "Changes updated successfully!!!";
                        buttons = MessageBoxButtons.OK;
                        dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                    }
                    WriteUniquelogs("SymbolSetting", "AddIntoINIFile : " + message, MessageType.Informational);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSetting", "AddIntoINIFile : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void ClearFields()
        {
            try
            {
                txtNA1.Text = "";
                txtNA2.Text = "";
                txtNA3.Text = "";
                txtNA4.Text = "";
                txtNA5.Text = "";
                txtperiodOfbar.Text = "";
                txtprofitpercent.Text = "";
                txtstoploss.Text = "";
                txtlNiftyPercent.Text = "";
                txtBankNiftyPercent.Text = "";
                //Pratiksha::19-08-2021::2 New field StopLossChange and UpdatedStopLossChange
                txtStopLossChange.Text = "";
                txtupdatedStoplossChange.Text = "";
                TradingSymbolList.SelectedIndex = -1;
                TradingSymbolList.Text = "";
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSetting", "ClearFields : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }
        private void btnDeleteselectedrow_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridView.Rows.Count > 0)
                {
                    if (dataGridView.SelectedRows.Count == 1)
                    {
                        foreach (DataGridViewRow row in dataGridView.SelectedRows)
                        {
                            string symbol = row.Cells[0].Value.ToString();
                            //IRDSPM::Pratiksha::04-06-2021::To delete perticular row
                            message = "Are you sure you want to delete the " + symbol + " ?";
                            buttons = MessageBoxButtons.YesNo;
                            dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);

                            if (dialog == DialogResult.Yes)
                            {
                                dataGridView.Rows.RemoveAt(row.Index);
                                WriteUniquelogs("SymbolSetting", "btnDeleteselectedrow_Click : Row deleted at :" + row.Cells[0].Value + " position.", MessageType.Informational);
                                //IRDSPM::Pratiksha::04-06-2021::For clearing details for deleted symbol
                                ClearFields();
                                AddIntoINIFile(true);
                                var value =Settings.TradingSymbolsInfoList.Find(r => r.TradingSymbol == symbol.Split('.')[0]);
                                if (value != null)
                                {
                                    Settings.TradingSymbolsInfoList.Remove(value);
                                }
                            }
                        }
                    }
                    else
                    {
                        message = "Please select the row first.";
                        buttons = MessageBoxButtons.OK;
                        dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                    }
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSetting", "btnDeleteselectedrow_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }
        public void WriteUniquelogs(string fileName, string message, MessageType msgType)
        {
            try
            {
                SymbolLogs log = new SymbolLogs();
                log.createFile(fileName);
                log.LogMessage(message, MessageType.Informational);
                log = null;
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSetting", "WriteUniquelogs : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                bool found = false;
                //Pratiksha::19-08-2021::2 New field StopLossChange and UpdatedStopLossChange
                if (txtNA1.Text.ToString().Length > 0 && txtNA2.Text.ToString().Length > 0 && txtNA3.Text.ToString().Length > 0 && txtNA4.Text.ToString().Length > 0 && txtNA5.Text.ToString().Length > 0 && txtperiodOfbar.Text.ToString().Length > 0 && txtprofitpercent.Text.ToString().Length > 0 && txtstoploss.Text.ToString().Length > 0 && txtlNiftyPercent.Text.ToString().Length > 0 && txtBankNiftyPercent.Text.ToString().Length > 0 && txtStopLossChange.Text.ToString().Length > 0 && txtupdatedStoplossChange.Text.ToString().Length > 0)
                {
                    Match PeriodOfbar1, Profitpercent1, Stoploss1, NA1, NA2, NA3, NA4, NA5, NiftyPer1, Banknifty1, StopLossChanged1, UpdatedStopLossChanged1;
                    Validations(out PeriodOfbar1, out Profitpercent1, out Stoploss1, out NA1, out NA2, out NA3, out NA4, out NA5, out NiftyPer1, out Banknifty1, out StopLossChanged1, out UpdatedStopLossChanged1);
                    if (PeriodOfbar1.Success && Profitpercent1.Success && Stoploss1.Success && NA1.Success && NA2.Success && NA3.Success && NA4.Success && NA5.Success && NiftyPer1.Success && Banknifty1.Success && StopLossChanged1.Success && UpdatedStopLossChanged1.Success)
                    { 
                        foreach (DataGridViewRow row in this.dataGridView.Rows)
                        {
                            string sym = GetExchangeOnSymbolBasis(TradingSymbolList.Text);
                            if (row.Cells[0].Value.Equals(sym))
                            {
                                found = true;
                                break;
                            }
                        }
                        if (found == false)
                        {
                            AddUpdateRowinDataGrid(0, true);
                        }
                        else
                        {
                            message = "This entry is already exist.";
                            buttons = MessageBoxButtons.OK;
                            dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                            WriteUniquelogs("SymbolSetting", "btnAdd_Click : " + message, MessageType.Informational);
                            ClearFields();
                            TradingSymbolList.SelectedIndex = -1;
                        }
                    }
                    else
                    {
                        string Logmessage = "";
                        string parameter = "";
                        if (!NA1.Success) { parameter += "\nQuantity"; Logmessage += "Quantity,"; }
                        if (!PeriodOfbar1.Success) { parameter += "\nPeriod Of Bar"; Logmessage += "Period Of Bar,"; }
                        if (!Profitpercent1.Success) { parameter += "\nProfit Percent"; Logmessage += "Profit Percent,"; }
                        if (!NA2.Success) { parameter += "\nHigh Low Percent"; Logmessage += "High Low Percent,"; }
                        if (!Stoploss1.Success) { parameter += "\nStoploss"; Logmessage += "Stoploss,"; }
                        if (!NA3.Success) { parameter += "\nUpdated Stoploss"; Logmessage += "Updated Stoploss,"; }
                        if (!NA4.Success) { parameter += "\nProfit Trail"; Logmessage += "Profit Trail,"; }
                        if (!NA5.Success) { parameter += "\nUpdated Profit Trail"; Logmessage += "Updated Profit Trail"; }
                        if (!NiftyPer1.Success) { parameter += "\nNifty Percent"; Logmessage += "Nifty Percent,"; }
                        if (!Banknifty1.Success) { parameter += "\nBankNifty Percent"; Logmessage += "BankNifty Percent"; }
                        //Pratiksha::19-08-2021::2 New field StopLossChange and UpdatedStopLossChange
                        if (!StopLossChanged1.Success) { parameter += "\nStopLoss Changed"; Logmessage += "StopLoss Changed,"; }
                        if (!UpdatedStopLossChanged1.Success) { parameter += "\nUpdated StopLoss Changed"; Logmessage += "Updated StopLoss Changed"; }
                        message = "Please add valid values for " + parameter + ".";
                        buttons = MessageBoxButtons.OK;
                        dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                        WriteUniquelogs("SymbolSetting", "btnAdd_Click : " + Logmessage, MessageType.Informational);
                    }
                }
                else
                {
                    CheckNullValuesAndShowPopup();
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSetting", "btnAdd_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private bool AddUpdateRowinDataGrid(int choice, bool SavedIniFile)
        {
            bool IsAdded = false;
            try
            {
                bool found = false;
                string symbol = GetExchangeOnSymbolBasis(TradingSymbolList.Text);
                foreach (DataGridViewRow row in this.dataGridView.Rows)
                {
                    if ((row.Cells[0].Value.ToString().Equals(symbol)) && (row.Cells[1].Value.ToString().Equals(txtNA1.Text.ToString())) && (row.Cells[2].Value.ToString().Equals(txtperiodOfbar.Text.ToString())) && (row.Cells[3].Value.ToString().Equals(txtprofitpercent.Text.ToString())) && (row.Cells[4].Value.ToString().Equals(txtNA2.Text.ToString())) && (row.Cells[5].Value.ToString().Equals(txtstoploss.Text.ToString())) && (row.Cells[6].Value.ToString().Equals(txtNA3.Text.ToString())) && (row.Cells[7].Value.ToString().Equals(txtNA4.Text.ToString())) && (row.Cells[8].Value.ToString().Equals(txtNA5.Text.ToString())))
                    {
                        found = true;
                        break;
                    }
                }

                if (!found)
                {
                    if (choice == 0)
                    {

                        string OutputRow = TradingSymbolList.Text.ToString() + "," + txtNA1.Text + "," + txtperiodOfbar.Text + "," + txtprofitpercent.Text + "," + txtNA2.Text + "," + txtstoploss.Text + "," + txtNA3.Text + "," + txtNA4.Text + "," + txtNA5.Text;
                        WriteUniquelogs("SymbolSetting", "AddUpdateRowinDataGrid : Row added in datagrid : " + OutputRow, MessageType.Informational);
                        dataGridView.Rows.Add(symbol, txtNA1.Text, txtperiodOfbar.Text, txtprofitpercent.Text, txtNA2.Text, txtstoploss.Text, txtNA3.Text, txtNA4.Text, txtNA5.Text, txtlNiftyPercent.Text, txtBankNiftyPercent.Text, txtStopLossChange.Text, txtupdatedStoplossChange.Text);
                        IsAdded = true;
                        ClearFields();
                    }
                    else
                    {
                        foreach (DataGridViewRow r in dataGridView.SelectedRows)
                        {
                            string sym = GetExchangeOnSymbolBasis(TradingSymbolList.Text);
                            r.Cells["TradingSymbol"].Value = sym;
                            //r.Cells["NA1"].Value = txtNA1.Text;
                            //r.Cells["PeriodOfBar"].Value = txtperiodOfbar.Text;
                            //r.Cells["ProfitPercent"].Value = txtprofitpercent.Text;
                            //r.Cells["NA2"].Value = txtNA2.Text;
                            //r.Cells["Stoploss"].Value = txtstoploss.Text;
                            //r.Cells["NA3"].Value = txtNA3.Text;
                            //r.Cells["NA4"].Value = txtNA4.Text;
                            //r.Cells["NA5"].Value = txtNA5.Text;

                            r.Cells["Quantity"].Value = txtNA1.Text;
                            r.Cells["PeriodOfBar"].Value = txtperiodOfbar.Text;
                            r.Cells["ProfitPercent"].Value = txtprofitpercent.Text;
                            r.Cells["highLowPercent"].Value = txtNA2.Text;
                            r.Cells["Stoploss"].Value = txtstoploss.Text;
                            r.Cells["updatedStoploss"].Value = txtNA3.Text;
                            r.Cells["profitTrail"].Value = txtNA4.Text;
                            r.Cells["Updatedprofittrail"].Value = txtNA5.Text;
                            r.Cells["NiftyPercent"].Value = txtlNiftyPercent.Text;
                            r.Cells["BankNiftyPercent"].Value = txtBankNiftyPercent.Text;
                            //Pratiksha::19-08-2021::2 New field StopLossChange and UpdatedStopLossChange
                            r.Cells["StopLossChange"].Value = txtStopLossChange.Text;
                            r.Cells["UpdatedStopLossChange"].Value = txtupdatedStoplossChange.Text;
                        }
                    }
                    if (SavedIniFile == true)
                    {
                        AddIntoINIFile();
                        IsAdded = true;
                        WriteUniquelogs("SymbolSetting", "AddUpdateRowinDataGrid : Row added.", MessageType.Informational);
                    }
                    ClearFields();
                }
                else
                {
                    message = "This entry is already exist.";
                    buttons = MessageBoxButtons.OK;
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                    WriteUniquelogs("SymbolSetting", "AddUpdateRowinDataGrid : " + message, MessageType.Informational);
                    IsAdded = false;
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSetting", "AddUpdateRowinDataGrid : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
            return IsAdded;
        }
        private void GetSymbolsFromDB(AlgoOMS obj)
        {
            try
            {
                if (Settings == null)
                {
                    Settings = new IRDSStategyExecutorMultiAccount.ReadSettings(logger);
                }
                string userid = Settings.username;
                m_futureNameForTable = obj.FetchTableName(userid);
                finalNSEList = obj.GetInstrumentsSymbols(Constants.EXCHANGE_NSE.ToString());
                finalNFOList = obj.GetInstrumentsSymbols(Constants.EXCHANGE_NFO);

                WriteUniquelogs("SymbolSetting", "GetSymbolsFromDB : Added expiry dates into list.", MessageType.Informational);
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSetting", "GetSymbolsFromDB : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }


        private void FetchDetailsFromINI(string Path)
        {
            try
            {
                string filePath = Path + @"\ChanbreakoutStrategy_" + m_userID + ".ini";
                inisymbolList = filePath;
                int rank = 0;
                if (File.Exists(inisymbolList))
                {
                    try
                    {
                        if (Settings == null)
                        {
                            Settings = new IRDSStategyExecutorMultiAccount.ReadSettings(logger);
                        }
                        Settings.ReadChanBreakoutConfigFile(inisymbolList);
                        foreach (var item in Settings.TradingSymbolsInfoList)
                        {
                            try
                            {
                                string TradingSymbol = item.SymbolWithExchange.ToString(); 
                                string PeriodofBar = item.BarCount.ToString();
                                string ProfitPercent = item.ProfitPercent.ToString();
                                string Stoploss = item.Stoploss.ToString();
                                //string NA1 = item.NA1;
                                //string NA2 = item.NA2;
                                //string NA3 = item.NA3;
                                //string NA4 = item.NA4;
                                //string NA5 = item.NA5;
                                string NA1 = item.Quantity.ToString();
                                string NA2 = item.HighLowPercent.ToString();
                                string NA3 = item.UpdatedStopLoss.ToString();
                                string NA4 = item.ProfitTrail.ToString();
                                string NA5 = item.UpdatedProfitTrail.ToString();
                                //Pratiksha::17-08-2021::Added 2 new fields for percentage
                                string niftyBank = item.NiftyPercent.ToString();
                                string bankniftyBank = item.BankNiftyPercent.ToString();
                                //Pratiksha::19-08-2021::2 New field StopLossChange and UpdatedStopLossChange
                                string StopLossChange = item.StopLossChange.ToString();
                                string UpdatedStopLossChange = item.UpdatedStopLossChange.ToString();
                                string sym = GetExchangeOnSymbolBasis(item.TradingSymbol.ToString());
                                dataGridView.Rows.Add(new object[] { sym, NA1, PeriodofBar, ProfitPercent, NA2, Stoploss, NA3, NA4, NA5, niftyBank, bankniftyBank , StopLossChange, UpdatedStopLossChange });
                                rank++;
                            }
                            catch (Exception ex)
                            {
                                WriteUniquelogs("SymbolSetting", "FetchDetailsFromINI : Exception Error Message = " + ex.Message, MessageType.Exception);
                            }
                        }
                        dataGridView.AllowUserToAddRows = false;
                    }
                    catch (Exception ex)
                    {
                        WriteUniquelogs("SymbolSetting", "FetchDetailsFromINI : Exception Error Message = " + ex.Message, MessageType.Exception);
                    }
                }
                else
                {
                    WriteUniquelogs("SymbolSetting", "FetchDetailsFromINI : INI file not present.", MessageType.Informational);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSetting", "FetchDetailsFromINI : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                OnCancelClickSaveChanges();
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSetting", "btnCancel_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void dataGridView_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0)
                {
                    dataGridView.Rows[e.RowIndex].Selected = true;
                    DataGridViewRow row = dataGridView.Rows[e.RowIndex];
                    string[] symbol = row.Cells[0].Value.ToString().Split('.');
                    TradingSymbolList.Text = symbol[0];
                    txtNA1.Text = row.Cells[1].Value.ToString();
                    txtperiodOfbar.Text = row.Cells[2].Value.ToString();
                    txtprofitpercent.Text = row.Cells[3].Value.ToString();
                    txtNA2.Text = row.Cells[4].Value.ToString();
                    txtstoploss.Text = row.Cells[5].Value.ToString();
                    txtNA3.Text = row.Cells[6].Value.ToString();
                    txtNA4.Text = row.Cells[7].Value.ToString();
                    txtNA5.Text = row.Cells[8].Value.ToString();
                    txtlNiftyPercent.Text = row.Cells[9].Value.ToString();
                    txtBankNiftyPercent.Text = row.Cells[10].Value.ToString();
                    //Pratiksha::19-08-2021::2 New field StopLossChange and UpdatedStopLossChange
                    txtStopLossChange.Text = row.Cells[11].Value.ToString();
                    txtupdatedStoplossChange.Text = row.Cells[12].Value.ToString();
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSetting", "dataGridView_CellMouseClick : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridView.SelectedRows.Count == 1)
                {
                    //Pratiksha::19-08-2021::2 New field StopLossChange and UpdatedStopLossChange
                    if (TradingSymbolList.Text.Length > 0 && txtNA1.Text.ToString().Length > 0 && txtNA2.Text.ToString().Length > 0 && txtNA3.Text.ToString().Length > 0 && txtNA4.Text.ToString().Length > 0 && txtNA5.Text.ToString().Length > 0 && txtperiodOfbar.Text.ToString().Length > 0 && txtprofitpercent.Text.ToString().Length > 0 && txtstoploss.Text.ToString().Length > 0 && txtlNiftyPercent.Text.ToString().Length > 0 && txtBankNiftyPercent.Text.ToString().Length > 0 && txtStopLossChange.Text.ToString().Length > 0 && txtupdatedStoplossChange.Text.ToString().Length > 0)
                    {
                        Match PeriodOfbar1, Profitpercent1, Stoploss1, NA1, NA2, NA3, NA4, NA5, NiftyPer1, Banknifty1, StopLossChanged1, UpdatedStopLossChanged1;
                        Validations(out PeriodOfbar1, out Profitpercent1, out Stoploss1, out NA1, out NA2, out NA3, out NA4, out NA5, out NiftyPer1, out Banknifty1, out StopLossChanged1, out UpdatedStopLossChanged1);
                        if (PeriodOfbar1.Success && Profitpercent1.Success && Stoploss1.Success && NA1.Success && NA2.Success && NA3.Success && NA4.Success && NA5.Success && NiftyPer1.Success && Banknifty1.Success && StopLossChanged1.Success && UpdatedStopLossChanged1.Success)
                        {
                            AddIntoINIFile();
                        }
                        else
                        {
                            string Logmessage = "";
                            string parameter = "";
                            if (!NA1.Success) { parameter += "\nQuantity"; Logmessage += "Quantity,"; }
                            if (!PeriodOfbar1.Success) { parameter += "\nPeriod Of Bar"; Logmessage += "Period Of Bar,"; }
                            if (!Profitpercent1.Success) { parameter += "\nProfit Percent"; Logmessage += "Profit Percent,"; }
                            if (!NA2.Success) { parameter += "\nHigh Low Percent"; Logmessage += "High Low Percent,"; }
                            if (!Stoploss1.Success) { parameter += "\nStoploss"; Logmessage += "Stoploss,"; }
                            if (!NA3.Success) { parameter += "\nUpdated Stoploss"; Logmessage += "Updated Stoploss,"; }
                            if (!NA4.Success) { parameter += "\nProfit Trail"; Logmessage += "Profit Trail,"; }
                            if (!NA5.Success) { parameter += "\nUpdated Profit Trail"; Logmessage += "Updated Profit Trail"; }
                            if (!NiftyPer1.Success) { parameter += "\nNifty Percent"; Logmessage += "Nifty Percent,"; }
                            if (!Banknifty1.Success) { parameter += "\nBankNifty Percent"; Logmessage += "BankNifty Percent"; }
                            //Pratiksha::19-08-2021::2 New field StopLossChange and UpdatedStopLossChange
                            if (!StopLossChanged1.Success) { parameter += "\nStopLoss Changed"; Logmessage += "StopLoss Changed,"; }
                            if (!UpdatedStopLossChanged1.Success) { parameter += "\nUpdated StopLoss Changed"; Logmessage += "Updated StopLoss Changed"; }
                            message = "Please add valid values for " + parameter + ".";
                            buttons = MessageBoxButtons.OK;
                            dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                            WriteUniquelogs("SymbolSetting", "btnUpdate_Click : " + Logmessage, MessageType.Informational);
                        }
                    }
                    else
                    {
                        CheckNullValuesAndShowPopup();
                    }
                }
                else
                {
                    message = "Please select the row first.";
                    buttons = MessageBoxButtons.OK;
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSetting", "btnUpdate_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }
        private void CheckNullValuesAndShowPopup()
        {
            try
            {
                string Logmessage = "Please add Proper values for ";
                string parameter = "";
                //if (TradingSymbolList.SelectedItem == null) { parameter += "\nSymbol"; }
                //if (txtNA1.Text.ToString().Length == 0) { parameter += "\nNA1"; }
                //if (txtperiodOfbar.Text.ToString().Length == 0) { parameter += "\nPeriod Of Bar"; }
                //if (txtprofitpercent.Text.ToString().Length == 0) { parameter += "\nProfit Percent"; }
                //if (txtNA2.Text.ToString().Length == 0) { parameter += "\nNA2"; }
                //if (txtstoploss.Text.ToString().Length == 0) { parameter += "\nStoploss"; }
                //if (txtNA3.Text.ToString().Length == 0) { parameter += "\nNA3"; }
                //if (txtNA4.Text.ToString().Length == 0) { parameter += "\nNA4"; }
                //if (txtNA5.Text.ToString().Length == 0) { parameter += "\nNA5"; }

                if (TradingSymbolList.Text == null) { parameter += "\nSymbol"; }
                if (txtNA1.Text.ToString().Length == 0) { parameter += "\nQuantity"; }
                if (txtperiodOfbar.Text.ToString().Length == 0) { parameter += "\nPeriod Of Bar"; }
                if (txtprofitpercent.Text.ToString().Length == 0) { parameter += "\nProfit Percent"; }
                if (txtNA2.Text.ToString().Length == 0) { parameter += "\nHigh Low Percent"; }
                if (txtstoploss.Text.ToString().Length == 0) { parameter += "\nStoploss"; }
                if (txtNA3.Text.ToString().Length == 0) { parameter += "\nUpdated Stoploss"; }
                if (txtNA4.Text.ToString().Length == 0) { parameter += "\nProfit Trail"; }
                if (txtNA5.Text.ToString().Length == 0) { parameter += "\nUpdated Profit Trail"; }
                if (txtlNiftyPercent.Text.ToString().Length == 0) { parameter += "\nNifty Percent"; }
                if (txtBankNiftyPercent.Text.ToString().Length == 0) { parameter += "\nBankNifty Percent"; }
                //Pratiksha::19-08-2021::2 New field StopLossChange and UpdatedStopLossChange
                if (txtStopLossChange.Text.ToString().Length == 0) { parameter += "\nStopLoss Change"; }
                if (txtupdatedStoplossChange.Text.ToString().Length == 0) { parameter += "\nUpdated Stoploss Change"; }
                message = "Please add proper values for " + parameter + ".";
                buttons = MessageBoxButtons.OK;
                dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                WriteUniquelogs("SymbolSetting", "CheckNullValuesAndShowPopup : " + Logmessage, MessageType.Informational);
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSetting", "CheckNullValuesAndShowPopup : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }
        //Pratiksha::19-08-2021::2 New field StopLossChange and UpdatedStopLossChange
        private void Validations(out Match PeriodOfbar1, out Match Profitpercent1, out Match Stoploss1, out Match NA1, out Match NA2, out Match NA3, out Match NA4, out Match NA5, out Match Niftyper1, out Match Banknifty1, out Match StopLossChange1, out Match UpdatedStoplossChange1)
        {
            string Decpattern = "^[0-9]+(.|,)?[0-9]*?$";
            Regex r2 = new Regex(Decpattern);
            PeriodOfbar1 = r2.Match(txtperiodOfbar.Text.ToString());
            Profitpercent1 = r2.Match(txtprofitpercent.Text.ToString());
            Stoploss1 = r2.Match(txtstoploss.Text.ToString());
            NA1 = r2.Match(txtNA1.Text.ToString());
            NA2 = r2.Match(txtNA2.Text.ToString());
            NA3 = r2.Match(txtNA3.Text.ToString());
            NA4 = r2.Match(txtNA4.Text.ToString());
            NA5 = r2.Match(txtNA5.Text.ToString());
            Niftyper1 = r2.Match(txtlNiftyPercent.Text.ToString());
            Banknifty1 = r2.Match(txtBankNiftyPercent.Text.ToString());
            //Pratiksha::19-08-2021::2 New field StopLossChange and UpdatedStopLossChange
            StopLossChange1 = r2.Match(txtStopLossChange.Text.ToString());
            UpdatedStoplossChange1 = r2.Match(txtupdatedStoplossChange.Text.ToString());
        }

        private bool OnCancelClickSaveChanges()
        {
            bool formCloseStatus = false;
            bool changeFound = false;
            try
            {
                //Pratiksha::19-08-2021::2 New field StopLossChange and UpdatedStopLossChange
                if (TradingSymbolList.Text.Length > 0 && txtNA1.Text.ToString().Length > 0 || txtNA2.Text.ToString().Length > 0 || txtNA3.Text.ToString().Length > 0 || txtNA4.Text.ToString().Length > 0 || txtNA5.Text.ToString().Length > 0 || txtperiodOfbar.Text.ToString().Length > 0 || txtprofitpercent.Text.ToString().Length > 0 || txtstoploss.Text.ToString().Length > 0 || txtlNiftyPercent.Text.ToString().Length > 0 || txtBankNiftyPercent.Text.ToString().Length > 0 || txtStopLossChange.Text.ToString().Length > 0 || txtupdatedStoplossChange.Text.ToString().Length > 0)
                {
                    foreach (DataGridViewRow row in dataGridView.Rows)
                    {
                        string symbol = "";
                        if (TradingSymbolList.Text.Length > 0)
                        {
                            if (finalNFOList.Contains(TradingSymbolList.Text))
                            {
                                symbol = TradingSymbolList.Text.ToString() + "." + Constants.EXCHANGE_NFO;
                            }
                            else if(finalNSEList.Contains(TradingSymbolList.Text))
                            {
                                symbol = TradingSymbolList.Text.ToString() + "." + Constants.EXCHANGE_NSE;
                            }
                            if (row.Cells[0].Value.ToString().Equals(symbol))
                            {
                                if (((row.Cells[1].Value.ToString().Equals(txtNA1.Text))) && (row.Cells[2].Value.ToString().Equals(txtperiodOfbar.Text)) && (row.Cells[3].Value.ToString().Equals(txtprofitpercent.Text)) && (row.Cells[4].Value.ToString().Equals(txtNA2.Text)) && (row.Cells[5].Value.ToString().Equals(txtstoploss.Text)) && (row.Cells[6].Value.ToString().Equals(txtNA3.Text)) && (row.Cells[7].Value.ToString().Equals(txtNA4.Text)) && (row.Cells[8].Value.ToString().Equals(txtNA5.Text)) && (row.Cells[9].Value.ToString().Equals(txtlNiftyPercent.Text)) && (row.Cells[10].Value.ToString().Equals(txtBankNiftyPercent.Text)) && (row.Cells[11].Value.ToString().Equals(txtStopLossChange.Text)) && (row.Cells[12].Value.ToString().Equals(txtupdatedStoplossChange.Text)))
                                {
                                    changeFound = false;
                                    isexit = true;
                                    break;
                                }
                                else
                                {
                                    changeFound = true;
                                }
                            }
                            else
                            {
                                changeFound = true;
                            }
                        }
                    }
                    if (changeFound == true)
                    {
                        message = "You have some unsaved changes, do you want to save those changes?";
                        buttons = MessageBoxButtons.YesNo;
                        dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                        if (DialogResult.Yes == dialog)
                        {
                            //Pratiksha::19-08-2021::2 New field StopLossChange and UpdatedStopLossChange
                            if (txtNA1.Text.ToString().Length > 0 && txtNA2.Text.ToString().Length > 0 && txtNA3.Text.ToString().Length > 0 && txtNA4.Text.ToString().Length > 0 && txtNA5.Text.ToString().Length > 0 && txtperiodOfbar.Text.ToString().Length > 0 && txtprofitpercent.Text.ToString().Length > 0 && txtstoploss.Text.ToString().Length > 0 && txtlNiftyPercent.Text.ToString().Length > 0 && txtBankNiftyPercent.Text.ToString().Length > 0 && txtStopLossChange.Text.ToString().Length > 0 && txtupdatedStoplossChange.Text.ToString().Length > 0)
                            {
                                Match PeriodOfbar1, Profitpercent1, Stoploss1, NA1, NA2, NA3, NA4, NA5, NiftyPer1, Banknifty1, StopLossChanged1, UpdatedStopLossChanged1;
                                Validations(out PeriodOfbar1, out Profitpercent1, out Stoploss1, out NA1, out NA2, out NA3, out NA4, out NA5, out NiftyPer1, out Banknifty1, out StopLossChanged1, out UpdatedStopLossChanged1);
                                if (PeriodOfbar1.Success && Profitpercent1.Success && Stoploss1.Success && NA1.Success && NA2.Success && NA3.Success && NA4.Success && NA5.Success && NiftyPer1.Success && Banknifty1.Success && StopLossChanged1.Success && UpdatedStopLossChanged1.Success)
                                {
                                    AddIntoINIFile();
                                    isexit = true;
                                    this.Close();
                                }
                                else
                                {
                                    string Logmessage = "";
                                    string parameter = "";
                                    if (!NA1.Success) { parameter += "\nQuantity"; Logmessage += "Quantity,"; }
                                    if (!PeriodOfbar1.Success) { parameter += "\nPeriod Of Bar"; Logmessage += "Period Of Bar,"; }
                                    if (!Profitpercent1.Success) { parameter += "\nProfit Percent"; Logmessage += "Profit Percent,"; }
                                    if (!NA2.Success) { parameter += "\nHigh Low Percent"; Logmessage += "High Low Percent,"; }
                                    if (!Stoploss1.Success) { parameter += "\nStoploss"; Logmessage += "Stoploss,"; }
                                    if (!NA3.Success) { parameter += "\nUpdated Stoploss"; Logmessage += "Updated Stoploss,"; }
                                    if (!NA4.Success) { parameter += "\nProfit Trail"; Logmessage += "Profit Trail,"; }
                                    if (!NA5.Success) { parameter += "\nUpdated Profit Trail"; Logmessage += "Updated Profit Trail"; }
                                    if (!NiftyPer1.Success) { parameter += "\nNifty Percent"; Logmessage += "Nifty Percent,"; }
                                    if (!Banknifty1.Success) { parameter += "\nBankNifty Percent"; Logmessage += "BankNifty Percent"; }
                                    //Pratiksha::19-08-2021::2 New field StopLossChange and UpdatedStopLossChange
                                    if (!StopLossChanged1.Success) { parameter += "\nStopLoss Changed"; Logmessage += "StopLoss Changed,"; }
                                    if (!UpdatedStopLossChanged1.Success) { parameter += "\nUpdated StopLoss Changed"; Logmessage += "Updated StopLoss Changed"; }
                                    message = "Please add valid values for " + parameter + ".";
                                    buttons = MessageBoxButtons.OK;
                                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                                    WriteUniquelogs("SymbolSetting", "OnCancelClickSaveChanges : " + Logmessage, MessageType.Informational);
                                }
                            }
                            else
                            {
                                CheckNullValuesAndShowPopup();
                                formCloseStatus = false;
                            }
                        }
                        else
                        {
                            isexit = true;
                            this.Close();
                            WriteUniquelogs("SymbolSetting", "OnCancelClickSaveChanges : You clicked on No.", MessageType.Informational);
                        }
                    }
                    else
                    {
                        isexit = true;
                        this.Close();
                        WriteUniquelogs("SymbolSetting", "OnCancelClickSaveChanges : No change found so close the form.", MessageType.Informational);
                    }
                }
                else
                {
                    this.Close();
                    WriteUniquelogs("SymbolSetting", "OnCancelClickSaveChanges : No change found so close the form.", MessageType.Informational);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSetting", "OnCancelClickSaveChanges : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
            return formCloseStatus;
        }

        private void AddIntoINIFile()
        {
            bool issaved = false;
            bool isAdded = false;
            int j = 0;
            int counter = 0;
            int k = dataGridView.Rows.Count;
            bool flagSymbolname = false;
            var re = @"^[0-9]{2}[a-zA-Z]{3}[0-9]{2}$";
            INIFile iniObj = new INIFile(inisymbolList);
            DialogResult dialog;
            try
            {
                foreach (DataGridViewRow row in dataGridView.Rows)
                {
                    flagSymbolname = false;
                    if (((row.Cells["TradingSymbol"].Value.ToString().Length == 0) || (row.Cells["Quantity"].Value.ToString().Length == 0) || (row.Cells["PeriodOfBar"].Value.ToString().Length == 0) || (row.Cells["ProfitPercent"].Value.ToString().Length == 0) || (row.Cells["highLowPercent"].Value.ToString().Length == 0) || (row.Cells["Stoploss"].Value.ToString().Length == 0) || (row.Cells["updatedStoploss"].Value.ToString().Length == 0) || (row.Cells["profitTrail"].Value.ToString().Length == 0) || (row.Cells["Updatedprofittrail"].Value.ToString().Length == 0) || (row.Cells["NiftyPercent"].Value.ToString().Length == 0) || (row.Cells["BankNiftyPercent"].Value.ToString().Length == 0) || (row.Cells["StopLossChange"].Value.ToString().Length == 0) || (row.Cells["UpdatedStopLossChange"].Value.ToString().Length == 0)))
                    {
                    }
                    else
                    {
                        if (TradingSymbolList.Text.Length > 0 && txtNA1.Text.ToString().Length > 0 || txtNA2.Text.ToString().Length > 0 || txtNA3.Text.ToString().Length > 0 || txtNA4.Text.ToString().Length > 0 || txtNA5.Text.ToString().Length > 0 || txtperiodOfbar.Text.ToString().Length > 0 || txtprofitpercent.Text.ToString().Length > 0 || txtstoploss.Text.ToString().Length > 0 || txtlNiftyPercent.Text.ToString().Length > 0 || txtBankNiftyPercent.Text.ToString().Length > 0 || txtStopLossChange.Text.ToString().Length > 0 || txtupdatedStoplossChange.Text.ToString().Length > 0)
                        {
                            string Finalsymbol = GetExchangeOnSymbolBasis(TradingSymbolList.Text);
                            if (Finalsymbol == row.Cells["TradingSymbol"].Value.ToString())
                            {
                                if ((row.Cells[0].Value.Equals(Finalsymbol)) && (row.Cells[1].Value.Equals(txtNA1.Text)) && (row.Cells[2].Value.Equals(txtperiodOfbar.Text.ToString())) && (row.Cells[3].Value.Equals(txtprofitpercent.Text.ToString())) && (row.Cells[4].Value.Equals(txtNA2.Text.ToString())) && (row.Cells[5].Value.Equals(txtstoploss.Text.ToString())) && (row.Cells[6].Value.Equals(txtNA3.Text.ToString())) && (row.Cells[7].Value.Equals(txtNA4.Text.ToString())) && (row.Cells[8].Value.Equals(txtNA5.Text.ToString())) && (row.Cells[9].Value.Equals(txtlNiftyPercent.Text.ToString())) && (row.Cells[10].Value.Equals(txtBankNiftyPercent.Text.ToString()) && (row.Cells[11].Value.ToString().Equals(txtStopLossChange.Text)) && (row.Cells[12].Value.ToString().Equals(txtupdatedStoplossChange.Text))))
                                {
                                    if (AddUpdateRowinDataGrid(0, false) == true)
                                    {
                                        k += 1;
                                        ClearFields();
                                        issaved = true;
                                        isAdded = true;
                                    }
                                    else
                                    {
                                        issaved = false;
                                    }
                                }
                                else
                                {
                                    foreach (DataGridViewRow r in dataGridView.SelectedRows)
                                    {
                                        string symbol = GetExchangeOnSymbolBasis(TradingSymbolList.Text);
                                        r.Cells["TradingSymbol"].Value = symbol;
                                        r.Cells["Quantity"].Value = txtNA1.Text;
                                        r.Cells["PeriodOfBar"].Value = txtperiodOfbar.Text;
                                        r.Cells["ProfitPercent"].Value = txtprofitpercent.Text;
                                        r.Cells["highLowPercent"].Value = txtNA2.Text;
                                        r.Cells["Stoploss"].Value = txtstoploss.Text;
                                        r.Cells["updatedStoploss"].Value = txtNA3.Text;
                                        r.Cells["profitTrail"].Value = txtNA4.Text;
                                        r.Cells["Updatedprofittrail"].Value = txtNA5.Text;
                                        r.Cells["NiftyPercent"].Value = txtlNiftyPercent.Text;
                                        r.Cells["BankNiftyPercent"].Value = txtBankNiftyPercent.Text;
                                        //Pratiksha::19-08-2021::2 New field StopLossChange and UpdatedStopLossChange
                                        r.Cells["StopLossChange"].Value = txtStopLossChange.Text;
                                        r.Cells["UpdatedStopLossChange"].Value = txtupdatedStoplossChange.Text;
                                        TradingSymbolList.SelectedIndex = -1;
                                        ClearFields();
                                        issaved = true;
                                    }
                                }
                            }
                            else
                            {
                                if (isAdded == false)
                                {
                                    bool isFound = false;
                                    foreach (DataGridViewRow r in dataGridView.Rows)
                                    {
                                        string symbol = GetExchangeOnSymbolBasis(TradingSymbolList.Text);
                                        if (symbol == r.Cells["TradingSymbol"].Value.ToString())
                                        {
                                            isFound = true;
                                            break;
                                        }
                                        else
                                        {
                                            isFound = false;
                                        }
                                    }
                                    if (isFound == false)
                                    {
                                        if (AddUpdateRowinDataGrid(0, false) == true)
                                        {
                                            k += 1;
                                            ClearFields();
                                            issaved = true;
                                            isAdded = true;
                                        }
                                        else
                                        {
                                            issaved = false;
                                        }
                                    }
                                }
                            }
                        }
                        if (((row.Cells["TradingSymbol"].Value.ToString().Length == 0) || (row.Cells["Quantity"].Value.ToString().Length == 0) || (row.Cells["PeriodOfBar"].Value.ToString().Length == 0) || (row.Cells["ProfitPercent"].Value.ToString().Length == 0) || (row.Cells["highLowPercent"].Value.ToString().Length == 0) || (row.Cells["Stoploss"].Value.ToString().Length == 0) || (row.Cells["updatedStoploss"].Value.ToString().Length == 0) || (row.Cells["profitTrail"].Value.ToString().Length == 0) || (row.Cells["Updatedprofittrail"].Value.ToString().Length == 0) || (row.Cells["NiftyPercent"].Value.ToString().Length == 0) || (row.Cells["BankNiftyPercent"].Value.ToString().Length == 0) || (row.Cells["StopLossChange"].Value.ToString().Length == 0) || (row.Cells["UpdatedStopLossChange"].Value.ToString().Length == 0)))
                        //if ((row.Cells["SymbolName"].Value.ToString().Length == 0) || (row.Cells["Exchange"].Value.ToString().Length == 0) || (row.Cells["ExpiryDate"].Value.ToString().Length == 0) || (row.Cells["ExpiryPeriod"].Value.ToString().Length == 0) || (row.Cells["OrderType"].Value.ToString().Length == 0) || (row.Cells["MarketLotSize"].Value.ToString().Length == 0) || (row.Cells["TotalNumberofLotSize"].Value.ToString().Length == 0) || (row.Cells["RoundOff"].Value.ToString().Length == 0) || (row.Cells["CallStrikePrice"].Value.ToString().Length == 0) || (row.Cells["PutStrikePrice"].Value.ToString().Length == 0) || (row.Cells["MarginPerLot"].Value.ToString().Length == 0) || (row.Cells["Stoploss"].Value.ToString().Length == 0))
                        {
                            message = "Not able to add this because null value is present.";
                            buttons = MessageBoxButtons.OK;
                            dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                            WriteUniquelogs("SymbolSetting", "AddIntoINIFile : Null value present at :" + row.Cells["SymbolName"].Value.ToString() + row.Cells["Exchange"].Value.ToString() + (row.Cells["ExpiryDate"].Value.ToString().Length == 0) + (row.Cells["ExpiryPeriod"].Value.ToString().Length == 0) + (row.Cells["OrderType"].Value.ToString().Length == 0) + (row.Cells["MarketLotSize"].Value.ToString().Length == 0) + (row.Cells["TotalNumberofLotSize"].Value.ToString().Length == 0) + (row.Cells["RoundOff"].Value.ToString().Length == 0) + (row.Cells["MarginPerLot"].Value.ToString().Length == 0) + (row.Cells["Stoploss"].Value.ToString().Length == 0) + ". Not able to add this value.", MessageType.Informational);
                        }
                        else
                        {
                            j++;
                        }
                    }
                }

                if ((j == k) && (issaved = true))
                {
                    if (Settings == null)
                    {
                        Settings = new IRDSStategyExecutorMultiAccount.ReadSettings(logger);
                    }
                    iniObj.clearTestingSymbol("TRADINGSYMBOL", null, null, inisymbolList);
                    foreach (DataGridViewRow row in dataGridView.Rows)
                    {
                        Settings.writeINIFileWithPath("TRADINGSYMBOL", row.Cells["TradingSymbol"].Value.ToString().ToUpper() + "," + (row.Cells["Quantity"].Value.ToString().ToUpper()) + "," + (row.Cells["PeriodOfBar"].Value.ToString().ToUpper()) + /*"," + Strikevalue + */"," + (row.Cells["ProfitPercent"].Value.ToString()) + "," + (row.Cells["highLowPercent"].Value.ToString()) + "," + (row.Cells["Stoploss"].Value.ToString()) + "," + (row.Cells["updatedStoploss"].Value.ToString()) + "," + (row.Cells["profitTrail"].Value.ToString()) + "," + (row.Cells["Updatedprofittrail"].Value.ToString()) + "," + (row.Cells["NiftyPercent"].Value.ToString()) + "," + (row.Cells["BankNiftyPercent"].Value.ToString()) + "," + (row.Cells["StopLossChange"].Value.ToString()) + "," + (row.Cells["UpdatedStopLossChange"].Value.ToString()), inisymbolList);
                        counter++;
                        //string rowOutout = row.Cells["TradingSymbol"].Value.ToString().ToUpper() + "," + (row.Cells["NA1"].Value.ToString().ToUpper()) + "," + (row.Cells["PeriodOfBar"].Value.ToString().ToUpper()) +/* "," + Strikevalue +*/ "," + (row.Cells["ProfitPercent"].Value.ToString()) + "," + (row.Cells["NA2"].Value.ToString()) + "," + (row.Cells["Stoploss"].Value.ToString()) + "," + (row.Cells["NA3"].Value.ToString()) + "," + (row.Cells["NA4"].Value.ToString()) + "," + (row.Cells["NA5"].Value.ToString());
                        string rowOutout = row.Cells["TradingSymbol"].Value.ToString().ToUpper() + "," + (row.Cells["Quantity"].Value.ToString().ToUpper()) + "," + (row.Cells["PeriodOfBar"].Value.ToString().ToUpper()) +/* "," + Strikevalue +*/ "," + (row.Cells["ProfitPercent"].Value.ToString()) + "," + (row.Cells["highLowPercent"].Value.ToString()) + "," + (row.Cells["Stoploss"].Value.ToString()) + "," + (row.Cells["updatedStoploss"].Value.ToString()) + "," + (row.Cells["profitTrail"].Value.ToString()) + "," + (row.Cells["Updatedprofittrail"].Value.ToString()) + "," + (row.Cells["NiftyPercent"].Value.ToString()) + ","+ (row.Cells["BankNiftyPercent"].Value.ToString()) + "," + (row.Cells["StopLossChange"].Value.ToString()) + "," + (row.Cells["UpdatedStopLossChange"].Value.ToString());
                        WriteUniquelogs("SymbolSetting", "AddIntoINIFile :  Details saved " + rowOutout, MessageType.Informational);
                    }

                    message = "Changes updated successfully!!!";
                    buttons = MessageBoxButtons.OK;
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);

                    WriteUniquelogs("SymbolSetting", "AddIntoINIFile : " + message, MessageType.Informational);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSetting", "AddIntoINIFile : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private string GetExchangeOnSymbolBasis(string symbol)
        {
            string Finalsymbol = "";
            if (symbol.Length > 0)
            {
                if (finalNSEList.Contains(symbol))
                {
                    Finalsymbol = symbol.ToString() + "." + Constants.EXCHANGE_NSE;
                }
                else
                {
                    if (symbol.Contains(m_futureNameForTable))
                    {
                        if (finalNFOList.Contains(symbol))
                        {
                            string sym = symbol.Substring(0,symbol.Length - 8);
                            Finalsymbol = sym.ToString() + "." + Constants.EXCHANGE_NFO;
                        }
                    }
                    else
                    {
                        string symbol1 = symbol + m_futureNameForTable;
                        if (finalNFOList.Contains(symbol1))
                        {
                            Finalsymbol = symbol.ToString() + "." + Constants.EXCHANGE_NFO;
                        }
                    }
                }
            }
            return Finalsymbol;
        }

        private void btnClearAll_Click(object sender, EventArgs e)
        {
            try
            {
                ClearFields();
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSetting", "btnClearAll_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void RBEquity_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                TradingSymbolList.Items.Clear();
                foreach (string listItem in finalNSEList)
                {
                    TradingSymbolList.Items.Add(listItem);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSetting", "RBEquity_CheckedChanged : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void RBFuture_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                TradingSymbolList.Items.Clear();
                foreach (string listItem in finalNFOList)
                {
                    TradingSymbolList.Items.Add(listItem);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSetting", "RBFuture_CheckedChanged : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }
        private void btnExport_Click(object sender, EventArgs e)
        {
            INIFile iniObj = new INIFile(inisymbolList);
            try
            {
                //AddIntoINIFile(false);
                if (Settings == null)
                {
                    Settings = new IRDSStategyExecutorMultiAccount.ReadSettings(logger);
                }
                iniObj.clearTestingSymbol("TRADINGSYMBOL", null, null, inisymbolList);
                foreach (DataGridViewRow row in dataGridView.Rows)
                {
                    //10-AUG-2021::Pratiksha::Facing issue while writing so add new method
                    Settings.writeINIFileWithPath("TRADINGSYMBOL", row.Cells["TradingSymbol"].Value.ToString().ToUpper() + "," + (row.Cells["Quantity"].Value.ToString().ToUpper()) + "," + (row.Cells["PeriodOfBar"].Value.ToString().ToUpper()) + /*"," + Strikevalue + */"," + (row.Cells["ProfitPercent"].Value.ToString()) + "," + (row.Cells["highLowPercent"].Value.ToString()) + "," + (row.Cells["Stoploss"].Value.ToString()) + "," + (row.Cells["updatedStoploss"].Value.ToString()) + "," + (row.Cells["profitTrail"].Value.ToString()) + "," + (row.Cells["Updatedprofittrail"].Value.ToString()) + "," + (row.Cells["NiftyPercent"].Value.ToString()) + "," + (row.Cells["BankNiftyPercent"].Value.ToString()) + "," + (row.Cells["StopLossChange"].Value.ToString()) + "," + (row.Cells["UpdatedStopLossChange"].Value.ToString()), inisymbolList);
                    string rowOutout = row.Cells["TradingSymbol"].Value.ToString().ToUpper() + "," + (row.Cells["Quantity"].Value.ToString().ToUpper()) + "," + (row.Cells["PeriodOfBar"].Value.ToString().ToUpper()) +/* "," + Strikevalue +*/ "," + (row.Cells["ProfitPercent"].Value.ToString()) + "," + (row.Cells["highLowPercent"].Value.ToString()) + "," + (row.Cells["Stoploss"].Value.ToString()) + "," + (row.Cells["updatedStoploss"].Value.ToString()) + "," + (row.Cells["profitTrail"].Value.ToString()) + "," + (row.Cells["Updatedprofittrail"].Value.ToString()) + "," + (row.Cells["NiftyPercent"].Value.ToString()) + "," + (row.Cells["BankNiftyPercent"].Value.ToString()) + "," + (row.Cells["StopLossChange"].Value.ToString()) + "," + (row.Cells["UpdatedStopLossChange"].Value.ToString());
                    WriteUniquelogs("SymbolSetting", "AddIntoINIFile :  Details saved " + rowOutout, MessageType.Informational);
                }

                string pathname = "";
                string strPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                SaveFDExportFile.InitialDirectory = @strPath; //@"C:\";
                SaveFDExportFile.DefaultExt = "txt";
                SaveFDExportFile.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                SaveFDExportFile.FilterIndex = 2;
                if (SaveFDExportFile.ShowDialog() == DialogResult.OK)
                {
                    pathname = SaveFDExportFile.FileName;

                    iniObj.clearTestingSymbol("TRADINGSYMBOL", null, null, pathname);
                    foreach (DataGridViewRow row in dataGridView.Rows)
                    {
                        Settings.writeINIFileWithPath("TRADINGSYMBOL", row.Cells["TradingSymbol"].Value.ToString().ToUpper() + "," + (row.Cells["Quantity"].Value.ToString().ToUpper()) + "," + (row.Cells["PeriodOfBar"].Value.ToString().ToUpper()) + /*"," + Strikevalue + */"," + (row.Cells["ProfitPercent"].Value.ToString()) + "," + (row.Cells["highLowPercent"].Value.ToString()) + "," + (row.Cells["Stoploss"].Value.ToString()) + "," + (row.Cells["updatedStoploss"].Value.ToString()) + "," + (row.Cells["profitTrail"].Value.ToString()) + "," + (row.Cells["Updatedprofittrail"].Value.ToString()) + "," + (row.Cells["NiftyPercent"].Value.ToString()) + "," + (row.Cells["BankNiftyPercent"].Value.ToString()) + "," + (row.Cells["StopLossChange"].Value.ToString()) + "," + (row.Cells["UpdatedStopLossChange"].Value.ToString()), pathname);
                        string rowOutout = row.Cells["TradingSymbol"].Value.ToString().ToUpper() + "," + (row.Cells["Quantity"].Value.ToString().ToUpper()) + "," + (row.Cells["PeriodOfBar"].Value.ToString().ToUpper()) +/* "," + Strikevalue +*/ "," + (row.Cells["ProfitPercent"].Value.ToString()) + "," + (row.Cells["highLowPercent"].Value.ToString()) + "," + (row.Cells["Stoploss"].Value.ToString()) + "," + (row.Cells["updatedStoploss"].Value.ToString()) + "," + (row.Cells["profitTrail"].Value.ToString()) + "," + (row.Cells["Updatedprofittrail"].Value.ToString()) + "," + (row.Cells["NiftyPercent"].Value.ToString()) + "," + (row.Cells["BankNiftyPercent"].Value.ToString()) + "," + (row.Cells["StopLossChange"].Value.ToString()) + "," + (row.Cells["UpdatedStopLossChange"].Value.ToString());
                        WriteUniquelogs("SymbolSetting", "AddIntoINIFile :  Details saved " + rowOutout, MessageType.Informational);
                    }
                    message = "File exported successfully!!!";
                    buttons = MessageBoxButtons.OK;
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                    WriteUniquelogs("SymbolSettingLogs", "btnExport_Click : " + message, MessageType.Informational);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs", "btnExport_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void BtnImport_Click(object sender, EventArgs e)
        {
            try
            {
                INIFile iniObj = new INIFile(inisymbolList);
                string pathname = "";

                //Pratiksha::12-08-2021::Change directory to Desktop
                string strPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                OpenFDImportFile.InitialDirectory = @strPath;//@"C:\";
                OpenFDImportFile.DefaultExt = "txt";
                OpenFDImportFile.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                if (OpenFDImportFile.ShowDialog() == DialogResult.OK)
                {
                    pathname = OpenFDImportFile.FileName;
                    dataGridView.Rows.Clear();
                    if (Settings == null)
                    {
                        Settings = new IRDSStategyExecutorMultiAccount.ReadSettings(logger);
                    }
                    Settings.ReadTradingSymbolList(pathname);
                    foreach (var item in Settings.TradingSymbolsInfoList)
                    {
                        try
                        {
                            string TradingSymbol = item.SymbolWithExchange.ToString();
                            string PeriodofBar = item.BarCount.ToString();
                            string ProfitPercent = item.ProfitPercent.ToString();
                            string Stoploss = item.Stoploss.ToString();
                            string NA1 = item.Quantity.ToString();
                            string NA2 = item.HighLowPercent.ToString();
                            string NA3 = item.UpdatedStopLoss.ToString();
                            string NA4 = item.ProfitTrail.ToString();
                            string NA5 = item.UpdatedProfitTrail.ToString();
                            //Pratiksha::17-08-2021::Added 2 new fields for percentage
                            string niftyBank = item.NiftyPercent.ToString();
                            string bankniftyBank = item.BankNiftyPercent.ToString();
                            //Pratiksha::19-08-2021::2 New field StopLossChange and UpdatedStopLossChange
                            string StopLossChange = item.StopLossChange.ToString();
                            string UpdatedStopLossChange = item.UpdatedStopLossChange.ToString();
                            string sym = GetExchangeOnSymbolBasis(item.TradingSymbol.ToString());
                            dataGridView.Rows.Add(new object[] { sym, NA1, PeriodofBar, ProfitPercent, NA2, Stoploss, NA3, NA4, NA5, niftyBank, bankniftyBank, StopLossChange, UpdatedStopLossChange });
                        }
                        catch (Exception ex)
                        {
                            WriteUniquelogs("SymbolSettingLogs", "BtnImport_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
                        }
                    }
                   // iniObj = new INIFile(inisymbolList);
                    iniObj.clearTestingSymbol("TRADINGSYMBOL", null, null, inisymbolList);
                    AddIntoINIFile(true);
                    dataGridView.AllowUserToAddRows = false;
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs", "BtnImport_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }
    }
}
