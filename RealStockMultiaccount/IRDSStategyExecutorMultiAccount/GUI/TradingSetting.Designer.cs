﻿
namespace IRDSStategyExecutorMultiAccount.GUI
{
    partial class TradingSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtStopLossChangeTime = new System.Windows.Forms.DateTimePicker();
            this.lblStopLossChangeTime = new System.Windows.Forms.Label();
            this.txtStartTime = new System.Windows.Forms.DateTimePicker();
            this.txtEndTime = new System.Windows.Forms.DateTimePicker();
            this.txtTimeLimitToPlaceOrder = new System.Windows.Forms.DateTimePicker();
            this.txtTimeForExitAllOpenPosition = new System.Windows.Forms.DateTimePicker();
            this.pnlPercentQuantity = new System.Windows.Forms.Panel();
            this.txtPercentQuantity = new System.Windows.Forms.TextBox();
            this.pnltotalmoney = new System.Windows.Forms.Panel();
            this.txttotalmoney = new System.Windows.Forms.TextBox();
            this.pnlBarInterval = new System.Windows.Forms.Panel();
            this.txtBarInterval = new System.Windows.Forms.TextBox();
            this.pnlMaxStrategyOrderCount = new System.Windows.Forms.Panel();
            this.txtMaxStrategyOrderCount = new System.Windows.Forms.TextBox();
            this.pnlindiviualCount = new System.Windows.Forms.Panel();
            this.txtindiviualCount = new System.Windows.Forms.TextBox();
            this.pnldaysForHistroricalData = new System.Windows.Forms.Panel();
            this.txtdaysForHistroricalData = new System.Windows.Forms.TextBox();
            this.pnlPriceDiffPercent = new System.Windows.Forms.Panel();
            this.txtPriceDiffPercent = new System.Windows.Forms.TextBox();
            this.pnlPlacedOrderPriceDiffPercent = new System.Windows.Forms.Panel();
            this.txtPlacedOrderPriceDiffPercent = new System.Windows.Forms.TextBox();
            this.pnlRiskPercentForStock = new System.Windows.Forms.Panel();
            this.txtRiskPercentForStock = new System.Windows.Forms.TextBox();
            this.pnlRiskPercentForFuture = new System.Windows.Forms.Panel();
            this.txtRiskPercentForFuture = new System.Windows.Forms.TextBox();
            this.pnlBreakEvenStop = new System.Windows.Forms.Panel();
            this.TruereadDataFromDB = new System.Windows.Forms.RadioButton();
            this.FalsereadDataFromDB = new System.Windows.Forms.RadioButton();
            this.pnlAutoDownloadHistoricalData = new System.Windows.Forms.Panel();
            this.TrueAutoDownloadHistoricalData = new System.Windows.Forms.RadioButton();
            this.FalseAutoDownloadHistoricalData = new System.Windows.Forms.RadioButton();
            this.pnlIsNeedtoParallel = new System.Windows.Forms.Panel();
            this.TrueIsNeedToParallel = new System.Windows.Forms.RadioButton();
            this.FalseIsNeedToParallel = new System.Windows.Forms.RadioButton();
            this.pnlMaxLossValue = new System.Windows.Forms.Panel();
            this.txtMaxLossValue = new System.Windows.Forms.TextBox();
            this.pnlMaxLossPercent = new System.Windows.Forms.Panel();
            this.txtMaxLossPercent = new System.Windows.Forms.TextBox();
            this.pnlOverallLoss = new System.Windows.Forms.Panel();
            this.txtOverallLoss = new System.Windows.Forms.TextBox();
            this.lblIntervl = new System.Windows.Forms.Label();
            this.lblIsNeedToParallel = new System.Windows.Forms.Label();
            this.pnlOverallProfitAmt = new System.Windows.Forms.Panel();
            this.txtOverallProfitAmt = new System.Windows.Forms.TextBox();
            this.lblAutoDownloadHistoricalData = new System.Windows.Forms.Label();
            this.lblTotalOpenPositions = new System.Windows.Forms.Label();
            this.lblRiskPercentForStock = new System.Windows.Forms.Label();
            this.pnlOverallProfitPercent = new System.Windows.Forms.Panel();
            this.txtOverallProfitPercent = new System.Windows.Forms.TextBox();
            this.pnlTotalOpenPositions = new System.Windows.Forms.Panel();
            this.txtTotalOpenPositions = new System.Windows.Forms.TextBox();
            this.lblOverallProfitPercent = new System.Windows.Forms.Label();
            this.lblOverallProfitAmt = new System.Windows.Forms.Label();
            this.lblPriceDiffPercent = new System.Windows.Forms.Label();
            this.lblMaxLossPercent = new System.Windows.Forms.Label();
            this.lbldaysForHistroricalData = new System.Windows.Forms.Label();
            this.lblOverallLoss = new System.Windows.Forms.Label();
            this.lblindiviualCount = new System.Windows.Forms.Label();
            this.lblPlacedOrderPriceDiffPercent = new System.Windows.Forms.Label();
            this.lblMaxLossValue = new System.Windows.Forms.Label();
            this.lblRiskPercentForFuture = new System.Windows.Forms.Label();
            this.lblMaxStrategyOrderCount = new System.Windows.Forms.Label();
            this.lblTimeForExitAllOpenPosition = new System.Windows.Forms.Label();
            this.lbltotalmoney = new System.Windows.Forms.Label();
            this.lblreadDataFromDB = new System.Windows.Forms.Label();
            this.lblPercentQuantity = new System.Windows.Forms.Label();
            this.lblTimeLimitToPlaceOrder = new System.Windows.Forms.Label();
            this.lblEndTime = new System.Windows.Forms.Label();
            this.lblStartTime = new System.Windows.Forms.Label();
            this.lblNoOfLots = new System.Windows.Forms.Label();
            this.btnApply = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOpenSymbolSetting = new System.Windows.Forms.Button();
            this.BtnImport = new System.Windows.Forms.Button();
            this.btnExport = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.pnlPercentQuantity.SuspendLayout();
            this.pnltotalmoney.SuspendLayout();
            this.pnlBarInterval.SuspendLayout();
            this.pnlMaxStrategyOrderCount.SuspendLayout();
            this.pnlindiviualCount.SuspendLayout();
            this.pnldaysForHistroricalData.SuspendLayout();
            this.pnlPriceDiffPercent.SuspendLayout();
            this.pnlPlacedOrderPriceDiffPercent.SuspendLayout();
            this.pnlRiskPercentForStock.SuspendLayout();
            this.pnlRiskPercentForFuture.SuspendLayout();
            this.pnlBreakEvenStop.SuspendLayout();
            this.pnlAutoDownloadHistoricalData.SuspendLayout();
            this.pnlIsNeedtoParallel.SuspendLayout();
            this.pnlMaxLossValue.SuspendLayout();
            this.pnlMaxLossPercent.SuspendLayout();
            this.pnlOverallLoss.SuspendLayout();
            this.pnlOverallProfitAmt.SuspendLayout();
            this.pnlOverallProfitPercent.SuspendLayout();
            this.pnlTotalOpenPositions.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtStopLossChangeTime);
            this.groupBox1.Controls.Add(this.lblStopLossChangeTime);
            this.groupBox1.Controls.Add(this.txtStartTime);
            this.groupBox1.Controls.Add(this.txtEndTime);
            this.groupBox1.Controls.Add(this.txtTimeLimitToPlaceOrder);
            this.groupBox1.Controls.Add(this.txtTimeForExitAllOpenPosition);
            this.groupBox1.Controls.Add(this.pnlPercentQuantity);
            this.groupBox1.Controls.Add(this.pnltotalmoney);
            this.groupBox1.Controls.Add(this.pnlBarInterval);
            this.groupBox1.Controls.Add(this.pnlMaxStrategyOrderCount);
            this.groupBox1.Controls.Add(this.pnlindiviualCount);
            this.groupBox1.Controls.Add(this.pnldaysForHistroricalData);
            this.groupBox1.Controls.Add(this.pnlPriceDiffPercent);
            this.groupBox1.Controls.Add(this.pnlPlacedOrderPriceDiffPercent);
            this.groupBox1.Controls.Add(this.pnlRiskPercentForStock);
            this.groupBox1.Controls.Add(this.pnlRiskPercentForFuture);
            this.groupBox1.Controls.Add(this.pnlBreakEvenStop);
            this.groupBox1.Controls.Add(this.pnlAutoDownloadHistoricalData);
            this.groupBox1.Controls.Add(this.pnlIsNeedtoParallel);
            this.groupBox1.Controls.Add(this.pnlMaxLossValue);
            this.groupBox1.Controls.Add(this.pnlMaxLossPercent);
            this.groupBox1.Controls.Add(this.pnlOverallLoss);
            this.groupBox1.Controls.Add(this.lblIntervl);
            this.groupBox1.Controls.Add(this.lblIsNeedToParallel);
            this.groupBox1.Controls.Add(this.pnlOverallProfitAmt);
            this.groupBox1.Controls.Add(this.lblAutoDownloadHistoricalData);
            this.groupBox1.Controls.Add(this.lblTotalOpenPositions);
            this.groupBox1.Controls.Add(this.lblRiskPercentForStock);
            this.groupBox1.Controls.Add(this.pnlOverallProfitPercent);
            this.groupBox1.Controls.Add(this.pnlTotalOpenPositions);
            this.groupBox1.Controls.Add(this.lblOverallProfitPercent);
            this.groupBox1.Controls.Add(this.lblOverallProfitAmt);
            this.groupBox1.Controls.Add(this.lblPriceDiffPercent);
            this.groupBox1.Controls.Add(this.lblMaxLossPercent);
            this.groupBox1.Controls.Add(this.lbldaysForHistroricalData);
            this.groupBox1.Controls.Add(this.lblOverallLoss);
            this.groupBox1.Controls.Add(this.lblindiviualCount);
            this.groupBox1.Controls.Add(this.lblPlacedOrderPriceDiffPercent);
            this.groupBox1.Controls.Add(this.lblMaxLossValue);
            this.groupBox1.Controls.Add(this.lblRiskPercentForFuture);
            this.groupBox1.Controls.Add(this.lblMaxStrategyOrderCount);
            this.groupBox1.Controls.Add(this.lblTimeForExitAllOpenPosition);
            this.groupBox1.Controls.Add(this.lbltotalmoney);
            this.groupBox1.Controls.Add(this.lblreadDataFromDB);
            this.groupBox1.Controls.Add(this.lblPercentQuantity);
            this.groupBox1.Controls.Add(this.lblTimeLimitToPlaceOrder);
            this.groupBox1.Controls.Add(this.lblEndTime);
            this.groupBox1.Controls.Add(this.lblStartTime);
            this.groupBox1.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1107, 508);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Trade Details";
            // 
            // txtStopLossChangeTime
            // 
            this.txtStopLossChangeTime.CustomFormat = "HH:mm";
            this.txtStopLossChangeTime.Font = new System.Drawing.Font("Arial", 12F);
            this.txtStopLossChangeTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txtStopLossChangeTime.Location = new System.Drawing.Point(875, 457);
            this.txtStopLossChangeTime.Name = "txtStopLossChangeTime";
            this.txtStopLossChangeTime.ShowUpDown = true;
            this.txtStopLossChangeTime.Size = new System.Drawing.Size(200, 30);
            this.txtStopLossChangeTime.TabIndex = 84;
            // 
            // lblStopLossChangeTime
            // 
            this.lblStopLossChangeTime.AutoSize = true;
            this.lblStopLossChangeTime.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblStopLossChangeTime.Location = new System.Drawing.Point(558, 463);
            this.lblStopLossChangeTime.Name = "lblStopLossChangeTime";
            this.lblStopLossChangeTime.Size = new System.Drawing.Size(177, 19);
            this.lblStopLossChangeTime.TabIndex = 85;
            this.lblStopLossChangeTime.Text = "StopLoss Change Time";
            // 
            // txtStartTime
            // 
            this.txtStartTime.CustomFormat = "HH:mm";
            this.txtStartTime.Font = new System.Drawing.Font("Arial", 12F);
            this.txtStartTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txtStartTime.Location = new System.Drawing.Point(338, 24);
            this.txtStartTime.Name = "txtStartTime";
            this.txtStartTime.ShowUpDown = true;
            this.txtStartTime.Size = new System.Drawing.Size(200, 30);
            this.txtStartTime.TabIndex = 1;
            // 
            // txtEndTime
            // 
            this.txtEndTime.CustomFormat = "HH:mm";
            this.txtEndTime.Font = new System.Drawing.Font("Arial", 12F);
            this.txtEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txtEndTime.Location = new System.Drawing.Point(338, 64);
            this.txtEndTime.Name = "txtEndTime";
            this.txtEndTime.ShowUpDown = true;
            this.txtEndTime.Size = new System.Drawing.Size(200, 30);
            this.txtEndTime.TabIndex = 2;
            // 
            // txtTimeLimitToPlaceOrder
            // 
            this.txtTimeLimitToPlaceOrder.CustomFormat = "HH:mm";
            this.txtTimeLimitToPlaceOrder.Font = new System.Drawing.Font("Arial", 12F);
            this.txtTimeLimitToPlaceOrder.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txtTimeLimitToPlaceOrder.Location = new System.Drawing.Point(338, 103);
            this.txtTimeLimitToPlaceOrder.Name = "txtTimeLimitToPlaceOrder";
            this.txtTimeLimitToPlaceOrder.ShowUpDown = true;
            this.txtTimeLimitToPlaceOrder.Size = new System.Drawing.Size(200, 30);
            this.txtTimeLimitToPlaceOrder.TabIndex = 3;
            // 
            // txtTimeForExitAllOpenPosition
            // 
            this.txtTimeForExitAllOpenPosition.CustomFormat = "HH:mm";
            this.txtTimeForExitAllOpenPosition.Font = new System.Drawing.Font("Arial", 12F);
            this.txtTimeForExitAllOpenPosition.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txtTimeForExitAllOpenPosition.Location = new System.Drawing.Point(338, 145);
            this.txtTimeForExitAllOpenPosition.Name = "txtTimeForExitAllOpenPosition";
            this.txtTimeForExitAllOpenPosition.ShowUpDown = true;
            this.txtTimeForExitAllOpenPosition.Size = new System.Drawing.Size(200, 30);
            this.txtTimeForExitAllOpenPosition.TabIndex = 4;
            // 
            // pnlPercentQuantity
            // 
            this.pnlPercentQuantity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPercentQuantity.Controls.Add(this.txtPercentQuantity);
            this.pnlPercentQuantity.Location = new System.Drawing.Point(338, 182);
            this.pnlPercentQuantity.Name = "pnlPercentQuantity";
            this.pnlPercentQuantity.Size = new System.Drawing.Size(200, 32);
            this.pnlPercentQuantity.TabIndex = 5;
            this.pnlPercentQuantity.TabStop = true;
            // 
            // txtPercentQuantity
            // 
            this.txtPercentQuantity.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPercentQuantity.Font = new System.Drawing.Font("Arial", 13F);
            this.txtPercentQuantity.Location = new System.Drawing.Point(7, 2);
            this.txtPercentQuantity.Name = "txtPercentQuantity";
            this.txtPercentQuantity.Size = new System.Drawing.Size(188, 25);
            this.txtPercentQuantity.TabIndex = 6;
            // 
            // pnltotalmoney
            // 
            this.pnltotalmoney.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnltotalmoney.Controls.Add(this.txttotalmoney);
            this.pnltotalmoney.Location = new System.Drawing.Point(338, 223);
            this.pnltotalmoney.Name = "pnltotalmoney";
            this.pnltotalmoney.Size = new System.Drawing.Size(200, 32);
            this.pnltotalmoney.TabIndex = 7;
            this.pnltotalmoney.TabStop = true;
            // 
            // txttotalmoney
            // 
            this.txttotalmoney.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txttotalmoney.Font = new System.Drawing.Font("Arial", 13F);
            this.txttotalmoney.Location = new System.Drawing.Point(7, 2);
            this.txttotalmoney.Name = "txttotalmoney";
            this.txttotalmoney.Size = new System.Drawing.Size(188, 25);
            this.txttotalmoney.TabIndex = 8;
            // 
            // pnlBarInterval
            // 
            this.pnlBarInterval.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlBarInterval.Controls.Add(this.txtBarInterval);
            this.pnlBarInterval.Location = new System.Drawing.Point(338, 264);
            this.pnlBarInterval.Name = "pnlBarInterval";
            this.pnlBarInterval.Size = new System.Drawing.Size(200, 32);
            this.pnlBarInterval.TabIndex = 9;
            this.pnlBarInterval.TabStop = true;
            // 
            // txtBarInterval
            // 
            this.txtBarInterval.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtBarInterval.Font = new System.Drawing.Font("Arial", 13F);
            this.txtBarInterval.Location = new System.Drawing.Point(7, 2);
            this.txtBarInterval.Name = "txtBarInterval";
            this.txtBarInterval.Size = new System.Drawing.Size(188, 25);
            this.txtBarInterval.TabIndex = 10;
            // 
            // pnlMaxStrategyOrderCount
            // 
            this.pnlMaxStrategyOrderCount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlMaxStrategyOrderCount.Controls.Add(this.txtMaxStrategyOrderCount);
            this.pnlMaxStrategyOrderCount.Location = new System.Drawing.Point(338, 303);
            this.pnlMaxStrategyOrderCount.Name = "pnlMaxStrategyOrderCount";
            this.pnlMaxStrategyOrderCount.Size = new System.Drawing.Size(200, 32);
            this.pnlMaxStrategyOrderCount.TabIndex = 11;
            this.pnlMaxStrategyOrderCount.TabStop = true;
            // 
            // txtMaxStrategyOrderCount
            // 
            this.txtMaxStrategyOrderCount.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtMaxStrategyOrderCount.Font = new System.Drawing.Font("Arial", 13F);
            this.txtMaxStrategyOrderCount.Location = new System.Drawing.Point(7, 2);
            this.txtMaxStrategyOrderCount.Name = "txtMaxStrategyOrderCount";
            this.txtMaxStrategyOrderCount.Size = new System.Drawing.Size(188, 25);
            this.txtMaxStrategyOrderCount.TabIndex = 12;
            // 
            // pnlindiviualCount
            // 
            this.pnlindiviualCount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlindiviualCount.Controls.Add(this.txtindiviualCount);
            this.pnlindiviualCount.Location = new System.Drawing.Point(338, 341);
            this.pnlindiviualCount.Name = "pnlindiviualCount";
            this.pnlindiviualCount.Size = new System.Drawing.Size(200, 32);
            this.pnlindiviualCount.TabIndex = 13;
            this.pnlindiviualCount.TabStop = true;
            // 
            // txtindiviualCount
            // 
            this.txtindiviualCount.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtindiviualCount.Font = new System.Drawing.Font("Arial", 13F);
            this.txtindiviualCount.Location = new System.Drawing.Point(7, 2);
            this.txtindiviualCount.Name = "txtindiviualCount";
            this.txtindiviualCount.Size = new System.Drawing.Size(188, 25);
            this.txtindiviualCount.TabIndex = 14;
            // 
            // pnldaysForHistroricalData
            // 
            this.pnldaysForHistroricalData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnldaysForHistroricalData.Controls.Add(this.txtdaysForHistroricalData);
            this.pnldaysForHistroricalData.Location = new System.Drawing.Point(338, 381);
            this.pnldaysForHistroricalData.Name = "pnldaysForHistroricalData";
            this.pnldaysForHistroricalData.Size = new System.Drawing.Size(200, 32);
            this.pnldaysForHistroricalData.TabIndex = 15;
            this.pnldaysForHistroricalData.TabStop = true;
            // 
            // txtdaysForHistroricalData
            // 
            this.txtdaysForHistroricalData.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtdaysForHistroricalData.Font = new System.Drawing.Font("Arial", 13F);
            this.txtdaysForHistroricalData.Location = new System.Drawing.Point(7, 2);
            this.txtdaysForHistroricalData.Name = "txtdaysForHistroricalData";
            this.txtdaysForHistroricalData.Size = new System.Drawing.Size(188, 25);
            this.txtdaysForHistroricalData.TabIndex = 16;
            // 
            // pnlPriceDiffPercent
            // 
            this.pnlPriceDiffPercent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPriceDiffPercent.Controls.Add(this.txtPriceDiffPercent);
            this.pnlPriceDiffPercent.Location = new System.Drawing.Point(338, 419);
            this.pnlPriceDiffPercent.Name = "pnlPriceDiffPercent";
            this.pnlPriceDiffPercent.Size = new System.Drawing.Size(200, 32);
            this.pnlPriceDiffPercent.TabIndex = 17;
            this.pnlPriceDiffPercent.TabStop = true;
            // 
            // txtPriceDiffPercent
            // 
            this.txtPriceDiffPercent.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPriceDiffPercent.Font = new System.Drawing.Font("Arial", 13F);
            this.txtPriceDiffPercent.Location = new System.Drawing.Point(7, 2);
            this.txtPriceDiffPercent.Name = "txtPriceDiffPercent";
            this.txtPriceDiffPercent.Size = new System.Drawing.Size(188, 25);
            this.txtPriceDiffPercent.TabIndex = 18;
            // 
            // pnlPlacedOrderPriceDiffPercent
            // 
            this.pnlPlacedOrderPriceDiffPercent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPlacedOrderPriceDiffPercent.Controls.Add(this.txtPlacedOrderPriceDiffPercent);
            this.pnlPlacedOrderPriceDiffPercent.Location = new System.Drawing.Point(338, 459);
            this.pnlPlacedOrderPriceDiffPercent.Name = "pnlPlacedOrderPriceDiffPercent";
            this.pnlPlacedOrderPriceDiffPercent.Size = new System.Drawing.Size(200, 32);
            this.pnlPlacedOrderPriceDiffPercent.TabIndex = 19;
            this.pnlPlacedOrderPriceDiffPercent.TabStop = true;
            // 
            // txtPlacedOrderPriceDiffPercent
            // 
            this.txtPlacedOrderPriceDiffPercent.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPlacedOrderPriceDiffPercent.Font = new System.Drawing.Font("Arial", 13F);
            this.txtPlacedOrderPriceDiffPercent.Location = new System.Drawing.Point(7, 2);
            this.txtPlacedOrderPriceDiffPercent.Name = "txtPlacedOrderPriceDiffPercent";
            this.txtPlacedOrderPriceDiffPercent.Size = new System.Drawing.Size(192, 25);
            this.txtPlacedOrderPriceDiffPercent.TabIndex = 20;
            // 
            // pnlRiskPercentForStock
            // 
            this.pnlRiskPercentForStock.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlRiskPercentForStock.Controls.Add(this.txtRiskPercentForStock);
            this.pnlRiskPercentForStock.Location = new System.Drawing.Point(875, 30);
            this.pnlRiskPercentForStock.Name = "pnlRiskPercentForStock";
            this.pnlRiskPercentForStock.Size = new System.Drawing.Size(200, 32);
            this.pnlRiskPercentForStock.TabIndex = 21;
            this.pnlRiskPercentForStock.TabStop = true;
            // 
            // txtRiskPercentForStock
            // 
            this.txtRiskPercentForStock.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtRiskPercentForStock.Font = new System.Drawing.Font("Arial", 13F);
            this.txtRiskPercentForStock.Location = new System.Drawing.Point(7, 2);
            this.txtRiskPercentForStock.Name = "txtRiskPercentForStock";
            this.txtRiskPercentForStock.Size = new System.Drawing.Size(192, 25);
            this.txtRiskPercentForStock.TabIndex = 22;
            // 
            // pnlRiskPercentForFuture
            // 
            this.pnlRiskPercentForFuture.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlRiskPercentForFuture.Controls.Add(this.txtRiskPercentForFuture);
            this.pnlRiskPercentForFuture.Location = new System.Drawing.Point(875, 70);
            this.pnlRiskPercentForFuture.Name = "pnlRiskPercentForFuture";
            this.pnlRiskPercentForFuture.Size = new System.Drawing.Size(200, 32);
            this.pnlRiskPercentForFuture.TabIndex = 23;
            this.pnlRiskPercentForFuture.TabStop = true;
            // 
            // txtRiskPercentForFuture
            // 
            this.txtRiskPercentForFuture.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtRiskPercentForFuture.Font = new System.Drawing.Font("Arial", 13F);
            this.txtRiskPercentForFuture.Location = new System.Drawing.Point(7, 2);
            this.txtRiskPercentForFuture.Name = "txtRiskPercentForFuture";
            this.txtRiskPercentForFuture.Size = new System.Drawing.Size(192, 25);
            this.txtRiskPercentForFuture.TabIndex = 24;
            // 
            // pnlBreakEvenStop
            // 
            this.pnlBreakEvenStop.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlBreakEvenStop.Controls.Add(this.TruereadDataFromDB);
            this.pnlBreakEvenStop.Controls.Add(this.FalsereadDataFromDB);
            this.pnlBreakEvenStop.Location = new System.Drawing.Point(875, 111);
            this.pnlBreakEvenStop.Name = "pnlBreakEvenStop";
            this.pnlBreakEvenStop.Size = new System.Drawing.Size(200, 30);
            this.pnlBreakEvenStop.TabIndex = 25;
            this.pnlBreakEvenStop.TabStop = true;
            // 
            // TruereadDataFromDB
            // 
            this.TruereadDataFromDB.AutoSize = true;
            this.TruereadDataFromDB.Location = new System.Drawing.Point(23, 3);
            this.TruereadDataFromDB.Name = "TruereadDataFromDB";
            this.TruereadDataFromDB.Size = new System.Drawing.Size(62, 23);
            this.TruereadDataFromDB.TabIndex = 26;
            this.TruereadDataFromDB.TabStop = true;
            this.TruereadDataFromDB.Text = "True";
            this.TruereadDataFromDB.UseVisualStyleBackColor = true;
            // 
            // FalsereadDataFromDB
            // 
            this.FalsereadDataFromDB.AutoSize = true;
            this.FalsereadDataFromDB.Location = new System.Drawing.Point(108, 3);
            this.FalsereadDataFromDB.Name = "FalsereadDataFromDB";
            this.FalsereadDataFromDB.Size = new System.Drawing.Size(69, 23);
            this.FalsereadDataFromDB.TabIndex = 27;
            this.FalsereadDataFromDB.TabStop = true;
            this.FalsereadDataFromDB.Text = "False";
            this.FalsereadDataFromDB.UseVisualStyleBackColor = true;
            // 
            // pnlAutoDownloadHistoricalData
            // 
            this.pnlAutoDownloadHistoricalData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlAutoDownloadHistoricalData.Controls.Add(this.TrueAutoDownloadHistoricalData);
            this.pnlAutoDownloadHistoricalData.Controls.Add(this.FalseAutoDownloadHistoricalData);
            this.pnlAutoDownloadHistoricalData.Location = new System.Drawing.Point(875, 149);
            this.pnlAutoDownloadHistoricalData.Name = "pnlAutoDownloadHistoricalData";
            this.pnlAutoDownloadHistoricalData.Size = new System.Drawing.Size(200, 30);
            this.pnlAutoDownloadHistoricalData.TabIndex = 28;
            this.pnlAutoDownloadHistoricalData.TabStop = true;
            // 
            // TrueAutoDownloadHistoricalData
            // 
            this.TrueAutoDownloadHistoricalData.AutoSize = true;
            this.TrueAutoDownloadHistoricalData.Location = new System.Drawing.Point(23, 3);
            this.TrueAutoDownloadHistoricalData.Name = "TrueAutoDownloadHistoricalData";
            this.TrueAutoDownloadHistoricalData.Size = new System.Drawing.Size(62, 23);
            this.TrueAutoDownloadHistoricalData.TabIndex = 29;
            this.TrueAutoDownloadHistoricalData.TabStop = true;
            this.TrueAutoDownloadHistoricalData.Text = "True";
            this.TrueAutoDownloadHistoricalData.UseVisualStyleBackColor = true;
            // 
            // FalseAutoDownloadHistoricalData
            // 
            this.FalseAutoDownloadHistoricalData.AutoSize = true;
            this.FalseAutoDownloadHistoricalData.Location = new System.Drawing.Point(108, 3);
            this.FalseAutoDownloadHistoricalData.Name = "FalseAutoDownloadHistoricalData";
            this.FalseAutoDownloadHistoricalData.Size = new System.Drawing.Size(69, 23);
            this.FalseAutoDownloadHistoricalData.TabIndex = 30;
            this.FalseAutoDownloadHistoricalData.TabStop = true;
            this.FalseAutoDownloadHistoricalData.Text = "False";
            this.FalseAutoDownloadHistoricalData.UseVisualStyleBackColor = true;
            // 
            // pnlIsNeedtoParallel
            // 
            this.pnlIsNeedtoParallel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlIsNeedtoParallel.Controls.Add(this.TrueIsNeedToParallel);
            this.pnlIsNeedtoParallel.Controls.Add(this.FalseIsNeedToParallel);
            this.pnlIsNeedtoParallel.Location = new System.Drawing.Point(875, 189);
            this.pnlIsNeedtoParallel.Name = "pnlIsNeedtoParallel";
            this.pnlIsNeedtoParallel.Size = new System.Drawing.Size(200, 30);
            this.pnlIsNeedtoParallel.TabIndex = 31;
            this.pnlIsNeedtoParallel.TabStop = true;
            // 
            // TrueIsNeedToParallel
            // 
            this.TrueIsNeedToParallel.AutoSize = true;
            this.TrueIsNeedToParallel.Location = new System.Drawing.Point(23, 3);
            this.TrueIsNeedToParallel.Name = "TrueIsNeedToParallel";
            this.TrueIsNeedToParallel.Size = new System.Drawing.Size(62, 23);
            this.TrueIsNeedToParallel.TabIndex = 32;
            this.TrueIsNeedToParallel.TabStop = true;
            this.TrueIsNeedToParallel.Text = "True";
            this.TrueIsNeedToParallel.UseVisualStyleBackColor = true;
            // 
            // FalseIsNeedToParallel
            // 
            this.FalseIsNeedToParallel.AutoSize = true;
            this.FalseIsNeedToParallel.Location = new System.Drawing.Point(108, 3);
            this.FalseIsNeedToParallel.Name = "FalseIsNeedToParallel";
            this.FalseIsNeedToParallel.Size = new System.Drawing.Size(69, 23);
            this.FalseIsNeedToParallel.TabIndex = 33;
            this.FalseIsNeedToParallel.TabStop = true;
            this.FalseIsNeedToParallel.Text = "False";
            this.FalseIsNeedToParallel.UseVisualStyleBackColor = true;
            // 
            // pnlMaxLossValue
            // 
            this.pnlMaxLossValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlMaxLossValue.Controls.Add(this.txtMaxLossValue);
            this.pnlMaxLossValue.Location = new System.Drawing.Point(875, 225);
            this.pnlMaxLossValue.Name = "pnlMaxLossValue";
            this.pnlMaxLossValue.Size = new System.Drawing.Size(200, 32);
            this.pnlMaxLossValue.TabIndex = 34;
            this.pnlMaxLossValue.TabStop = true;
            // 
            // txtMaxLossValue
            // 
            this.txtMaxLossValue.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtMaxLossValue.Font = new System.Drawing.Font("Arial", 13F);
            this.txtMaxLossValue.Location = new System.Drawing.Point(7, 2);
            this.txtMaxLossValue.Name = "txtMaxLossValue";
            this.txtMaxLossValue.Size = new System.Drawing.Size(192, 25);
            this.txtMaxLossValue.TabIndex = 35;
            // 
            // pnlMaxLossPercent
            // 
            this.pnlMaxLossPercent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlMaxLossPercent.Controls.Add(this.txtMaxLossPercent);
            this.pnlMaxLossPercent.Location = new System.Drawing.Point(875, 263);
            this.pnlMaxLossPercent.Name = "pnlMaxLossPercent";
            this.pnlMaxLossPercent.Size = new System.Drawing.Size(200, 32);
            this.pnlMaxLossPercent.TabIndex = 36;
            this.pnlMaxLossPercent.TabStop = true;
            // 
            // txtMaxLossPercent
            // 
            this.txtMaxLossPercent.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtMaxLossPercent.Font = new System.Drawing.Font("Arial", 13F);
            this.txtMaxLossPercent.Location = new System.Drawing.Point(7, 2);
            this.txtMaxLossPercent.Name = "txtMaxLossPercent";
            this.txtMaxLossPercent.Size = new System.Drawing.Size(192, 25);
            this.txtMaxLossPercent.TabIndex = 37;
            // 
            // pnlOverallLoss
            // 
            this.pnlOverallLoss.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlOverallLoss.Controls.Add(this.txtOverallLoss);
            this.pnlOverallLoss.Location = new System.Drawing.Point(875, 303);
            this.pnlOverallLoss.Name = "pnlOverallLoss";
            this.pnlOverallLoss.Size = new System.Drawing.Size(200, 32);
            this.pnlOverallLoss.TabIndex = 38;
            this.pnlOverallLoss.TabStop = true;
            // 
            // txtOverallLoss
            // 
            this.txtOverallLoss.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtOverallLoss.Font = new System.Drawing.Font("Arial", 13F);
            this.txtOverallLoss.Location = new System.Drawing.Point(7, 2);
            this.txtOverallLoss.Name = "txtOverallLoss";
            this.txtOverallLoss.Size = new System.Drawing.Size(192, 25);
            this.txtOverallLoss.TabIndex = 39;
            // 
            // lblIntervl
            // 
            this.lblIntervl.AutoSize = true;
            this.lblIntervl.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblIntervl.Location = new System.Drawing.Point(15, 270);
            this.lblIntervl.Name = "lblIntervl";
            this.lblIntervl.Size = new System.Drawing.Size(92, 19);
            this.lblIntervl.TabIndex = 83;
            this.lblIntervl.Text = "Bar Interval";
            // 
            // lblIsNeedToParallel
            // 
            this.lblIsNeedToParallel.AutoSize = true;
            this.lblIsNeedToParallel.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblIsNeedToParallel.Location = new System.Drawing.Point(558, 194);
            this.lblIsNeedToParallel.Name = "lblIsNeedToParallel";
            this.lblIsNeedToParallel.Size = new System.Drawing.Size(144, 19);
            this.lblIsNeedToParallel.TabIndex = 68;
            this.lblIsNeedToParallel.Text = "Is Need To Parallel";
            // 
            // pnlOverallProfitAmt
            // 
            this.pnlOverallProfitAmt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlOverallProfitAmt.Controls.Add(this.txtOverallProfitAmt);
            this.pnlOverallProfitAmt.Location = new System.Drawing.Point(875, 341);
            this.pnlOverallProfitAmt.Name = "pnlOverallProfitAmt";
            this.pnlOverallProfitAmt.Size = new System.Drawing.Size(200, 32);
            this.pnlOverallProfitAmt.TabIndex = 40;
            this.pnlOverallProfitAmt.TabStop = true;
            // 
            // txtOverallProfitAmt
            // 
            this.txtOverallProfitAmt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtOverallProfitAmt.Font = new System.Drawing.Font("Arial", 13F);
            this.txtOverallProfitAmt.Location = new System.Drawing.Point(7, 2);
            this.txtOverallProfitAmt.Name = "txtOverallProfitAmt";
            this.txtOverallProfitAmt.Size = new System.Drawing.Size(192, 25);
            this.txtOverallProfitAmt.TabIndex = 41;
            // 
            // lblAutoDownloadHistoricalData
            // 
            this.lblAutoDownloadHistoricalData.AutoSize = true;
            this.lblAutoDownloadHistoricalData.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblAutoDownloadHistoricalData.Location = new System.Drawing.Point(558, 154);
            this.lblAutoDownloadHistoricalData.Name = "lblAutoDownloadHistoricalData";
            this.lblAutoDownloadHistoricalData.Size = new System.Drawing.Size(229, 19);
            this.lblAutoDownloadHistoricalData.TabIndex = 55;
            this.lblAutoDownloadHistoricalData.Text = "Auto Download Historical Data\r\n";
            // 
            // lblTotalOpenPositions
            // 
            this.lblTotalOpenPositions.AutoSize = true;
            this.lblTotalOpenPositions.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblTotalOpenPositions.Location = new System.Drawing.Point(558, 423);
            this.lblTotalOpenPositions.Name = "lblTotalOpenPositions";
            this.lblTotalOpenPositions.Size = new System.Drawing.Size(156, 19);
            this.lblTotalOpenPositions.TabIndex = 81;
            this.lblTotalOpenPositions.Text = "Total Open Positions";
            // 
            // lblRiskPercentForStock
            // 
            this.lblRiskPercentForStock.AutoSize = true;
            this.lblRiskPercentForStock.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblRiskPercentForStock.Location = new System.Drawing.Point(558, 36);
            this.lblRiskPercentForStock.Name = "lblRiskPercentForStock";
            this.lblRiskPercentForStock.Size = new System.Drawing.Size(178, 19);
            this.lblRiskPercentForStock.TabIndex = 65;
            this.lblRiskPercentForStock.Text = "Risk Percent For Stock";
            // 
            // pnlOverallProfitPercent
            // 
            this.pnlOverallProfitPercent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlOverallProfitPercent.Controls.Add(this.txtOverallProfitPercent);
            this.pnlOverallProfitPercent.Location = new System.Drawing.Point(875, 379);
            this.pnlOverallProfitPercent.Name = "pnlOverallProfitPercent";
            this.pnlOverallProfitPercent.Size = new System.Drawing.Size(200, 32);
            this.pnlOverallProfitPercent.TabIndex = 42;
            this.pnlOverallProfitPercent.TabStop = true;
            // 
            // txtOverallProfitPercent
            // 
            this.txtOverallProfitPercent.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtOverallProfitPercent.Font = new System.Drawing.Font("Arial", 13F);
            this.txtOverallProfitPercent.Location = new System.Drawing.Point(7, 2);
            this.txtOverallProfitPercent.Name = "txtOverallProfitPercent";
            this.txtOverallProfitPercent.Size = new System.Drawing.Size(192, 25);
            this.txtOverallProfitPercent.TabIndex = 43;
            // 
            // pnlTotalOpenPositions
            // 
            this.pnlTotalOpenPositions.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlTotalOpenPositions.Controls.Add(this.txtTotalOpenPositions);
            this.pnlTotalOpenPositions.Location = new System.Drawing.Point(875, 417);
            this.pnlTotalOpenPositions.Name = "pnlTotalOpenPositions";
            this.pnlTotalOpenPositions.Size = new System.Drawing.Size(200, 32);
            this.pnlTotalOpenPositions.TabIndex = 44;
            this.pnlTotalOpenPositions.TabStop = true;
            // 
            // txtTotalOpenPositions
            // 
            this.txtTotalOpenPositions.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTotalOpenPositions.Font = new System.Drawing.Font("Arial", 13F);
            this.txtTotalOpenPositions.Location = new System.Drawing.Point(7, 2);
            this.txtTotalOpenPositions.Name = "txtTotalOpenPositions";
            this.txtTotalOpenPositions.Size = new System.Drawing.Size(192, 25);
            this.txtTotalOpenPositions.TabIndex = 45;
            // 
            // lblOverallProfitPercent
            // 
            this.lblOverallProfitPercent.AutoSize = true;
            this.lblOverallProfitPercent.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblOverallProfitPercent.Location = new System.Drawing.Point(558, 386);
            this.lblOverallProfitPercent.Name = "lblOverallProfitPercent";
            this.lblOverallProfitPercent.Size = new System.Drawing.Size(164, 19);
            this.lblOverallProfitPercent.TabIndex = 79;
            this.lblOverallProfitPercent.Text = "Overall Profit Percent";
            // 
            // lblOverallProfitAmt
            // 
            this.lblOverallProfitAmt.AutoSize = true;
            this.lblOverallProfitAmt.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblOverallProfitAmt.Location = new System.Drawing.Point(558, 347);
            this.lblOverallProfitAmt.Name = "lblOverallProfitAmt";
            this.lblOverallProfitAmt.Size = new System.Drawing.Size(134, 19);
            this.lblOverallProfitAmt.TabIndex = 76;
            this.lblOverallProfitAmt.Text = "Overall Profit Amt";
            // 
            // lblPriceDiffPercent
            // 
            this.lblPriceDiffPercent.AutoSize = true;
            this.lblPriceDiffPercent.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblPriceDiffPercent.Location = new System.Drawing.Point(15, 424);
            this.lblPriceDiffPercent.Name = "lblPriceDiffPercent";
            this.lblPriceDiffPercent.Size = new System.Drawing.Size(141, 19);
            this.lblPriceDiffPercent.TabIndex = 63;
            this.lblPriceDiffPercent.Text = "Price Diff Percent";
            // 
            // lblMaxLossPercent
            // 
            this.lblMaxLossPercent.AutoSize = true;
            this.lblMaxLossPercent.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblMaxLossPercent.Location = new System.Drawing.Point(558, 270);
            this.lblMaxLossPercent.Name = "lblMaxLossPercent";
            this.lblMaxLossPercent.Size = new System.Drawing.Size(139, 19);
            this.lblMaxLossPercent.TabIndex = 70;
            this.lblMaxLossPercent.Text = "Max Loss Percent";
            // 
            // lbldaysForHistroricalData
            // 
            this.lbldaysForHistroricalData.AutoSize = true;
            this.lbldaysForHistroricalData.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lbldaysForHistroricalData.Location = new System.Drawing.Point(15, 388);
            this.lbldaysForHistroricalData.Name = "lbldaysForHistroricalData";
            this.lbldaysForHistroricalData.Size = new System.Drawing.Size(194, 19);
            this.lbldaysForHistroricalData.TabIndex = 61;
            this.lbldaysForHistroricalData.Text = "Days For Histrorical Data";
            // 
            // lblOverallLoss
            // 
            this.lblOverallLoss.AutoSize = true;
            this.lblOverallLoss.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblOverallLoss.Location = new System.Drawing.Point(558, 309);
            this.lblOverallLoss.Name = "lblOverallLoss";
            this.lblOverallLoss.Size = new System.Drawing.Size(97, 19);
            this.lblOverallLoss.TabIndex = 74;
            this.lblOverallLoss.Text = "Overall Loss";
            // 
            // lblindiviualCount
            // 
            this.lblindiviualCount.AutoSize = true;
            this.lblindiviualCount.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblindiviualCount.Location = new System.Drawing.Point(15, 349);
            this.lblindiviualCount.Name = "lblindiviualCount";
            this.lblindiviualCount.Size = new System.Drawing.Size(125, 19);
            this.lblindiviualCount.TabIndex = 59;
            this.lblindiviualCount.Text = "Individual Count";
            // 
            // lblPlacedOrderPriceDiffPercent
            // 
            this.lblPlacedOrderPriceDiffPercent.AutoSize = true;
            this.lblPlacedOrderPriceDiffPercent.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblPlacedOrderPriceDiffPercent.Location = new System.Drawing.Point(15, 465);
            this.lblPlacedOrderPriceDiffPercent.Name = "lblPlacedOrderPriceDiffPercent";
            this.lblPlacedOrderPriceDiffPercent.Size = new System.Drawing.Size(243, 19);
            this.lblPlacedOrderPriceDiffPercent.TabIndex = 78;
            this.lblPlacedOrderPriceDiffPercent.Text = "Placed Order Price Diff Percent";
            // 
            // lblMaxLossValue
            // 
            this.lblMaxLossValue.AutoSize = true;
            this.lblMaxLossValue.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblMaxLossValue.Location = new System.Drawing.Point(558, 231);
            this.lblMaxLossValue.Name = "lblMaxLossValue";
            this.lblMaxLossValue.Size = new System.Drawing.Size(122, 19);
            this.lblMaxLossValue.TabIndex = 68;
            this.lblMaxLossValue.Text = "Max Loss Value";
            // 
            // lblRiskPercentForFuture
            // 
            this.lblRiskPercentForFuture.AutoSize = true;
            this.lblRiskPercentForFuture.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblRiskPercentForFuture.Location = new System.Drawing.Point(558, 77);
            this.lblRiskPercentForFuture.Name = "lblRiskPercentForFuture";
            this.lblRiskPercentForFuture.Size = new System.Drawing.Size(184, 19);
            this.lblRiskPercentForFuture.TabIndex = 71;
            this.lblRiskPercentForFuture.Text = "Risk Percent For Future";
            // 
            // lblMaxStrategyOrderCount
            // 
            this.lblMaxStrategyOrderCount.AutoSize = true;
            this.lblMaxStrategyOrderCount.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblMaxStrategyOrderCount.Location = new System.Drawing.Point(15, 310);
            this.lblMaxStrategyOrderCount.Name = "lblMaxStrategyOrderCount";
            this.lblMaxStrategyOrderCount.Size = new System.Drawing.Size(199, 19);
            this.lblMaxStrategyOrderCount.TabIndex = 57;
            this.lblMaxStrategyOrderCount.Text = "Max Strategy Order Count";
            // 
            // lblTimeForExitAllOpenPosition
            // 
            this.lblTimeForExitAllOpenPosition.AutoSize = true;
            this.lblTimeForExitAllOpenPosition.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblTimeForExitAllOpenPosition.Location = new System.Drawing.Point(15, 151);
            this.lblTimeForExitAllOpenPosition.Name = "lblTimeForExitAllOpenPosition";
            this.lblTimeForExitAllOpenPosition.Size = new System.Drawing.Size(232, 19);
            this.lblTimeForExitAllOpenPosition.TabIndex = 66;
            this.lblTimeForExitAllOpenPosition.Text = "Time For Exit All Open Position";
            // 
            // lbltotalmoney
            // 
            this.lbltotalmoney.AutoSize = true;
            this.lbltotalmoney.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lbltotalmoney.Location = new System.Drawing.Point(15, 230);
            this.lbltotalmoney.Name = "lbltotalmoney";
            this.lbltotalmoney.Size = new System.Drawing.Size(95, 19);
            this.lbltotalmoney.TabIndex = 60;
            this.lbltotalmoney.Text = "Total Money";
            // 
            // lblreadDataFromDB
            // 
            this.lblreadDataFromDB.AutoSize = true;
            this.lblreadDataFromDB.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblreadDataFromDB.Location = new System.Drawing.Point(558, 118);
            this.lblreadDataFromDB.Name = "lblreadDataFromDB";
            this.lblreadDataFromDB.Size = new System.Drawing.Size(157, 19);
            this.lblreadDataFromDB.TabIndex = 53;
            this.lblreadDataFromDB.Text = "Read Data From DB";
            // 
            // lblPercentQuantity
            // 
            this.lblPercentQuantity.AutoSize = true;
            this.lblPercentQuantity.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblPercentQuantity.Location = new System.Drawing.Point(15, 188);
            this.lblPercentQuantity.Name = "lblPercentQuantity";
            this.lblPercentQuantity.Size = new System.Drawing.Size(131, 19);
            this.lblPercentQuantity.TabIndex = 54;
            this.lblPercentQuantity.Text = "Percent Quantity";
            // 
            // lblTimeLimitToPlaceOrder
            // 
            this.lblTimeLimitToPlaceOrder.AutoSize = true;
            this.lblTimeLimitToPlaceOrder.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblTimeLimitToPlaceOrder.Location = new System.Drawing.Point(15, 111);
            this.lblTimeLimitToPlaceOrder.Name = "lblTimeLimitToPlaceOrder";
            this.lblTimeLimitToPlaceOrder.Size = new System.Drawing.Size(196, 19);
            this.lblTimeLimitToPlaceOrder.TabIndex = 59;
            this.lblTimeLimitToPlaceOrder.Text = "Time Limit To Place Order";
            // 
            // lblEndTime
            // 
            this.lblEndTime.AutoSize = true;
            this.lblEndTime.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblEndTime.Location = new System.Drawing.Point(15, 70);
            this.lblEndTime.Name = "lblEndTime";
            this.lblEndTime.Size = new System.Drawing.Size(77, 19);
            this.lblEndTime.TabIndex = 61;
            this.lblEndTime.Text = "End Time";
            // 
            // lblStartTime
            // 
            this.lblStartTime.AutoSize = true;
            this.lblStartTime.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblStartTime.Location = new System.Drawing.Point(15, 34);
            this.lblStartTime.Name = "lblStartTime";
            this.lblStartTime.Size = new System.Drawing.Size(82, 19);
            this.lblStartTime.TabIndex = 62;
            this.lblStartTime.Text = "Start Time";
            // 
            // lblNoOfLots
            // 
            this.lblNoOfLots.AutoSize = true;
            this.lblNoOfLots.Location = new System.Drawing.Point(20, 251);
            this.lblNoOfLots.Name = "lblNoOfLots";
            this.lblNoOfLots.Size = new System.Drawing.Size(186, 19);
            this.lblNoOfLots.TabIndex = 57;
            this.lblNoOfLots.Text = "Total Number of Lot Size";
            // 
            // btnApply
            // 
            this.btnApply.Font = new System.Drawing.Font("Arial", 11F);
            this.btnApply.Location = new System.Drawing.Point(719, 526);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(150, 40);
            this.btnApply.TabIndex = 20;
            this.btnApply.Text = "Apply";
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("Arial", 11F);
            this.btnCancel.Location = new System.Drawing.Point(937, 526);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(150, 40);
            this.btnCancel.TabIndex = 21;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOpenSymbolSetting
            // 
            this.btnOpenSymbolSetting.Font = new System.Drawing.Font("Arial", 11F);
            this.btnOpenSymbolSetting.Location = new System.Drawing.Point(12, 526);
            this.btnOpenSymbolSetting.Name = "btnOpenSymbolSetting";
            this.btnOpenSymbolSetting.Size = new System.Drawing.Size(221, 40);
            this.btnOpenSymbolSetting.TabIndex = 19;
            this.btnOpenSymbolSetting.Text = "Symbol Setting";
            this.btnOpenSymbolSetting.UseVisualStyleBackColor = true;
            this.btnOpenSymbolSetting.Click += new System.EventHandler(this.btnOpenSymbolSetting_Click);
            // 
            // BtnImport
            // 
            this.BtnImport.Font = new System.Drawing.Font("Arial", 10.5F);
            this.BtnImport.Location = new System.Drawing.Point(490, 526);
            this.BtnImport.Name = "BtnImport";
            this.BtnImport.Size = new System.Drawing.Size(150, 40);
            this.BtnImport.TabIndex = 29;
            this.BtnImport.Text = "Import";
            this.BtnImport.UseVisualStyleBackColor = true;
            this.BtnImport.Click += new System.EventHandler(this.BtnImport_Click);
            // 
            // btnExport
            // 
            this.btnExport.Font = new System.Drawing.Font("Arial", 10.5F);
            this.btnExport.Location = new System.Drawing.Point(277, 526);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(150, 40);
            this.btnExport.TabIndex = 28;
            this.btnExport.Text = "Export";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // TradingSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1131, 578);
            this.Controls.Add(this.BtnImport);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnOpenSymbolSetting);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.btnCancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "TradingSetting";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Trading Setting";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.pnlPercentQuantity.ResumeLayout(false);
            this.pnlPercentQuantity.PerformLayout();
            this.pnltotalmoney.ResumeLayout(false);
            this.pnltotalmoney.PerformLayout();
            this.pnlBarInterval.ResumeLayout(false);
            this.pnlBarInterval.PerformLayout();
            this.pnlMaxStrategyOrderCount.ResumeLayout(false);
            this.pnlMaxStrategyOrderCount.PerformLayout();
            this.pnlindiviualCount.ResumeLayout(false);
            this.pnlindiviualCount.PerformLayout();
            this.pnldaysForHistroricalData.ResumeLayout(false);
            this.pnldaysForHistroricalData.PerformLayout();
            this.pnlPriceDiffPercent.ResumeLayout(false);
            this.pnlPriceDiffPercent.PerformLayout();
            this.pnlPlacedOrderPriceDiffPercent.ResumeLayout(false);
            this.pnlPlacedOrderPriceDiffPercent.PerformLayout();
            this.pnlRiskPercentForStock.ResumeLayout(false);
            this.pnlRiskPercentForStock.PerformLayout();
            this.pnlRiskPercentForFuture.ResumeLayout(false);
            this.pnlRiskPercentForFuture.PerformLayout();
            this.pnlBreakEvenStop.ResumeLayout(false);
            this.pnlBreakEvenStop.PerformLayout();
            this.pnlAutoDownloadHistoricalData.ResumeLayout(false);
            this.pnlAutoDownloadHistoricalData.PerformLayout();
            this.pnlIsNeedtoParallel.ResumeLayout(false);
            this.pnlIsNeedtoParallel.PerformLayout();
            this.pnlMaxLossValue.ResumeLayout(false);
            this.pnlMaxLossValue.PerformLayout();
            this.pnlMaxLossPercent.ResumeLayout(false);
            this.pnlMaxLossPercent.PerformLayout();
            this.pnlOverallLoss.ResumeLayout(false);
            this.pnlOverallLoss.PerformLayout();
            this.pnlOverallProfitAmt.ResumeLayout(false);
            this.pnlOverallProfitAmt.PerformLayout();
            this.pnlOverallProfitPercent.ResumeLayout(false);
            this.pnlOverallProfitPercent.PerformLayout();
            this.pnlTotalOpenPositions.ResumeLayout(false);
            this.pnlTotalOpenPositions.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        //private System.Windows.Forms.Label lblPutStrikeValue;
        //private System.Windows.Forms.Label lblCallStrikeValue;
        private System.Windows.Forms.Label lblNoOfLots;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Panel pnlBreakEvenStop;
        private System.Windows.Forms.RadioButton FalsereadDataFromDB;
        private System.Windows.Forms.RadioButton TruereadDataFromDB;
        //private System.Windows.Forms.Panel panel5;
        //private System.Windows.Forms.TextBox txtEndEntryTime;
        //private System.Windows.Forms.Panel panel6;
        //private System.Windows.Forms.TextBox txtStartExitTime;
        //private System.Windows.Forms.Panel pnlNoOfLots;
        //private System.Windows.Forms.TextBox txtNoOfLots;
        //private System.Windows.Forms.Panel pnlInterval;
        //private System.Windows.Forms.TextBox txtInterval;
        //private System.Windows.Forms.Panel panel7;
        //private System.Windows.Forms.TextBox txtEndExitTime;
        //private System.Windows.Forms.Panel pnlCallStrikeValue;
        // private System.Windows.Forms.TextBox txtCallStrikeValue;
        private System.Windows.Forms.Panel pnlPercentQuantity;
        private System.Windows.Forms.TextBox txtPercentQuantity;
        private System.Windows.Forms.DateTimePicker txtStartTime;
        private System.Windows.Forms.DateTimePicker txtEndTime;
        private System.Windows.Forms.DateTimePicker txtTimeLimitToPlaceOrder;
        private System.Windows.Forms.Button btnOpenSymbolSetting;
        protected System.Windows.Forms.Label lblTimeLimitToPlaceOrder;
        protected System.Windows.Forms.Label lblEndTime;
        protected System.Windows.Forms.Label lblStartTime;
        protected System.Windows.Forms.Label lblreadDataFromDB;
        protected System.Windows.Forms.Label lblPercentQuantity;
        private System.Windows.Forms.DateTimePicker txtTimeForExitAllOpenPosition;
        protected System.Windows.Forms.Label lblTimeForExitAllOpenPosition;
        private System.Windows.Forms.Panel pnlindiviualCount;
        private System.Windows.Forms.TextBox txtindiviualCount;
        protected System.Windows.Forms.Label lblindiviualCount;
        private System.Windows.Forms.Panel pnltotalmoney;
        private System.Windows.Forms.TextBox txttotalmoney;
        private System.Windows.Forms.Panel pnlMaxStrategyOrderCount;
        private System.Windows.Forms.TextBox txtMaxStrategyOrderCount;
        protected System.Windows.Forms.Label lblMaxStrategyOrderCount;
        protected System.Windows.Forms.Label lbltotalmoney;
        private System.Windows.Forms.Panel pnlPriceDiffPercent;
        private System.Windows.Forms.TextBox txtPriceDiffPercent;
        protected System.Windows.Forms.Label lblPriceDiffPercent;
        private System.Windows.Forms.Panel pnldaysForHistroricalData;
        private System.Windows.Forms.TextBox txtdaysForHistroricalData;
        protected System.Windows.Forms.Label lbldaysForHistroricalData;
        private System.Windows.Forms.Panel pnlRiskPercentForStock;
        private System.Windows.Forms.TextBox txtRiskPercentForStock;
        protected System.Windows.Forms.Label lblRiskPercentForStock;
        private System.Windows.Forms.Panel pnlIsNeedtoParallel;
        private System.Windows.Forms.RadioButton TrueIsNeedToParallel;
        private System.Windows.Forms.RadioButton FalseIsNeedToParallel;
        protected System.Windows.Forms.Label lblIsNeedToParallel;
        private System.Windows.Forms.Panel pnlAutoDownloadHistoricalData;
        private System.Windows.Forms.RadioButton TrueAutoDownloadHistoricalData;
        private System.Windows.Forms.RadioButton FalseAutoDownloadHistoricalData;
        protected System.Windows.Forms.Label lblAutoDownloadHistoricalData;
        private System.Windows.Forms.Panel pnlTotalOpenPositions;
        private System.Windows.Forms.TextBox txtTotalOpenPositions;
        private System.Windows.Forms.Panel pnlOverallProfitAmt;
        private System.Windows.Forms.TextBox txtOverallProfitAmt;
        protected System.Windows.Forms.Label lblTotalOpenPositions;
        private System.Windows.Forms.Panel pnlMaxLossPercent;
        private System.Windows.Forms.TextBox txtMaxLossPercent;
        private System.Windows.Forms.Panel pnlOverallProfitPercent;
        private System.Windows.Forms.TextBox txtOverallProfitPercent;
        protected System.Windows.Forms.Label lblOverallProfitPercent;
        protected System.Windows.Forms.Label lblOverallProfitAmt;
        protected System.Windows.Forms.Label lblMaxLossPercent;
        private System.Windows.Forms.Panel pnlPlacedOrderPriceDiffPercent;
        private System.Windows.Forms.TextBox txtPlacedOrderPriceDiffPercent;
        private System.Windows.Forms.Panel pnlOverallLoss;
        private System.Windows.Forms.TextBox txtOverallLoss;
        protected System.Windows.Forms.Label lblOverallLoss;
        protected System.Windows.Forms.Label lblPlacedOrderPriceDiffPercent;
        private System.Windows.Forms.Panel pnlRiskPercentForFuture;
        private System.Windows.Forms.TextBox txtRiskPercentForFuture;
        private System.Windows.Forms.Panel pnlMaxLossValue;
        private System.Windows.Forms.TextBox txtMaxLossValue;
        protected System.Windows.Forms.Label lblMaxLossValue;
        protected System.Windows.Forms.Label lblRiskPercentForFuture;
        private System.Windows.Forms.Panel pnlBarInterval;
        private System.Windows.Forms.TextBox txtBarInterval;
        protected System.Windows.Forms.Label lblIntervl;
        private System.Windows.Forms.DateTimePicker txtStopLossChangeTime;
        protected System.Windows.Forms.Label lblStopLossChangeTime;
        private System.Windows.Forms.Button BtnImport;
        private System.Windows.Forms.Button btnExport;
    }
}