﻿using IRDSAlgoOMS;
using IRDSStategyExecutorMultiAccount;
using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace IRDSStategyExecutorMultiAccount.GUI
{
    public partial class TradingSetting : Form
    {
        [field: NonSerialized()]
        AlgoOMS AlgoOMSObj;
        //sanika::25-sep-2020::added for resume 
        [field: NonSerialized()]
        IRDSStategyExecutorMultiAccount.ReadSettings readSettings;
        //sanika::25-sep-2020::added for resume 
        [field: NonSerialized()]
        Logger logger;
        string DirPath = System.IO.Directory.GetCurrentDirectory() + "\\Configuration\\";

        string message = "";
        string title = "IRDS Strategy Executor";
        MessageBoxButtons btnOK = MessageBoxButtons.OK;
        MessageBoxButtons btnYesNo = MessageBoxButtons.YesNo;
        DialogResult dialog;
        INIFile iniObj = null;
        string m_userID = null;
        string m_FrmTitle = "";
        public TradingSetting(Logger logger, AlgoOMS obj, string FrmTitle, string userID)
        {
            m_FrmTitle = FrmTitle;
            m_userID = userID;
            Assembly myAssembly = Assembly.GetExecutingAssembly();
            Stream IconStream = null;
            IconStream = myAssembly.GetManifestResourceStream("IRDSStategyExecutorMultiAccount.Resources." + m_FrmTitle + ".ico");
            this.Icon = new System.Drawing.Icon(IconStream);
            InitializeComponent();
            try
            {
                groupBox1.Text = "Trading details for " + userID;
                this.AlgoOMSObj = obj;
                this.logger = logger;
                if (readSettings == null)
                {
                    readSettings = new IRDSStategyExecutorMultiAccount.ReadSettings(logger);
                }
                FetchDetailsFromINI();
            }
            catch (Exception ex)
            {
                WriteUniquelogs("TradingSetting", "InitializeComponent : Exception Error Message = " + ex.Message, MessageType.Informational);
            }
        }

        private void FetchDetailsFromINI()
        {
            try
            {
                string filePath = DirPath + @"ChanbreakoutStrategy_" + m_userID + ".ini";
                GetDetails(filePath);
            }
            catch (Exception ex)
            {
                WriteUniquelogs("TradingSetting", "FetchDetailsFromINI : Exception Error Message = " + ex.Message, MessageType.Informational);
            }
        }

        private void GetDetails(string filePath)
        {
            if (File.Exists(filePath))
            {
                if (readSettings == null)
                {
                    readSettings = new IRDSStategyExecutorMultiAccount.ReadSettings(logger);
                }
                readSettings.ReadChanBreakoutConfigFile(filePath);
                txtStartTime.Value = Convert.ToDateTime(readSettings.startTime);
                txtEndTime.Value = Convert.ToDateTime(readSettings.endTime);
                txtTimeLimitToPlaceOrder.Value = Convert.ToDateTime(readSettings.TimeLimitToPlaceOrder);
                txtTimeForExitAllOpenPosition.Value = Convert.ToDateTime(readSettings.TimeForExitAllOpenPosition);
                txtPercentQuantity.Text = readSettings.m_PercentQuantity.ToString();
                txttotalmoney.Text = readSettings.totalMoney.ToString();
                txtBarInterval.Text = readSettings.m_BarInterval.ToString();
                txtMaxStrategyOrderCount.Text = readSettings.m_MaxStrategyOrderCount.ToString();
                txtindiviualCount.Text = readSettings.individualCount.ToString();
                txtdaysForHistroricalData.Text = readSettings.m_DaysForHistroricalData.ToString();
                txtPriceDiffPercent.Text = readSettings.priceDiffPercent.ToString();
                txtPlacedOrderPriceDiffPercent.Text = readSettings.PlacedOrderPriceDiffPercent.ToString();
                txtRiskPercentForStock.Text = readSettings.m_RiskPercentForStock.ToString();
                txtRiskPercentForFuture.Text = readSettings.m_RiskPercentForFuture.ToString();

                if (readSettings.isReadDataFromDB.ToString().ToLower() == "true")
                {
                    TruereadDataFromDB.Checked = true;
                }
                else
                {
                    FalsereadDataFromDB.Checked = true;
                }

                if (readSettings.m_AutoDownloadHistoricalData.ToString().ToLower() == "true")
                {
                    TrueAutoDownloadHistoricalData.Checked = true;
                }
                else
                {
                    FalseAutoDownloadHistoricalData.Checked = true;
                }

                if (readSettings.m_IsNeedToRunParallel.ToString().ToLower() == "true")
                {
                    TrueIsNeedToParallel.Checked = true;
                }
                else
                {
                    FalseIsNeedToParallel.Checked = true;
                }
                txtMaxLossValue.Text = readSettings.m_MaxLossValueForIndividualSymbol.ToString();
                txtMaxLossPercent.Text = readSettings.m_MaxLossPercentForIndividualSymbol.ToString();
                txtOverallLoss.Text = readSettings.m_OverallLoss.ToString();
                txtOverallProfitAmt.Text = readSettings.m_OverallProfitAmt.ToString();
                txtOverallProfitPercent.Text = readSettings.m_OverallProfitPercent.ToString();
                txtTotalOpenPositions.Text = readSettings.m_TotalOpenPositions.ToString();
                //Pratiksha::19-08-2021::New field StopLossChangeTime
                txtStopLossChangeTime.Text = readSettings.m_StopLossChangeTime.ToString();
                WriteUniquelogs("TradingSetting", "FetchDetailsFromINI : Reading details from INI file.", MessageType.Informational);
            }
            else
            {
                Clearfields();
                WriteUniquelogs("TradingSetting", "FetchDetailsFromINI : INI file not exist.", MessageType.Informational);
            }
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtStartTime.Text != "" && txtEndTime.Text != "" && txtTimeLimitToPlaceOrder.Text != "" && txtTimeForExitAllOpenPosition.Text != "" && txtPercentQuantity.Text != ""
                    && txttotalmoney.Text != "" && txtBarInterval.Text != "" && txtMaxStrategyOrderCount.Text != "" && txtindiviualCount.Text != "" &&
                    txtdaysForHistroricalData.Text != "" && txtPriceDiffPercent.Text != "" && txtPlacedOrderPriceDiffPercent.Text != "" && txtRiskPercentForStock.Text != "" &&
                    txtRiskPercentForFuture.Text != "" && txtMaxLossValue.Text != "" && txtMaxLossPercent.Text != "" && txtOverallLoss.Text != "" && txtOverallProfitAmt.Text != "" &&
                    txtOverallProfitPercent.Text != "" && txtTotalOpenPositions.Text != "" && txtStopLossChangeTime.Text != "")
                {
                    Match PercentQuantity, totalmoney, BarInterval, MaxStrategyOrderCount, indiviualCount, daysForHistroricalData, PriceDiffPercent,
                       PlacedOrderPriceDiffPercent, RiskPercentForStock, RiskPercentForFuture, MaxLossValue, MaxLossPercent, OverallLoss,
                       OverallProfitAmt, OverallProfitPercent, TotalOpenPositions;
                    Validations(out PercentQuantity, out totalmoney, out BarInterval, out MaxStrategyOrderCount, out indiviualCount,
                        out daysForHistroricalData, out PriceDiffPercent, out PlacedOrderPriceDiffPercent, out RiskPercentForStock,
                        out RiskPercentForFuture, out MaxLossValue, out MaxLossPercent, out OverallLoss, out OverallProfitAmt,
                        out OverallProfitPercent, out TotalOpenPositions);
                    if (PercentQuantity.Success && totalmoney.Success && BarInterval.Success &&
                        MaxStrategyOrderCount.Success && indiviualCount.Success && daysForHistroricalData.Success &&
                        PriceDiffPercent.Success && PlacedOrderPriceDiffPercent.Success && RiskPercentForStock.Success &&
                        RiskPercentForFuture.Success && MaxLossValue.Success && MaxLossPercent.Success &&
                        OverallLoss.Success && OverallProfitAmt.Success && OverallProfitPercent.Success && TotalOpenPositions.Success)
                    {
                        message = "Do you want to save the changes?";
                        dialog = System.Windows.Forms.MessageBox.Show(message, title, btnYesNo, MessageBoxIcon.Warning);
                        if (dialog == DialogResult.Yes)
                        {
                            string filePath = DirPath + @"ChanbreakoutStrategy_" + m_userID + ".ini";
                            SaveInIniFile(filePath);
                            message = "Saved changes successfully!!!";
                            dialog = System.Windows.Forms.MessageBox.Show(message, title, btnOK, MessageBoxIcon.Information);
                            if (dialog == DialogResult.OK)
                            {
                                this.Close();
                            }

                            WriteUniquelogs("TradingSetting", "btnApply_Click :  Details saved in ini file.", MessageType.Informational);
                        }
                        else
                        {
                            this.Close();
                            WriteUniquelogs("TradingSetting", "btnApply_Click : You have clicked No, no data updated in trade setting.ini file.", MessageType.Informational);
                        }
                    }
                    else
                    {
                        ValidationSuccessOrFail(PercentQuantity, totalmoney, BarInterval, MaxStrategyOrderCount, indiviualCount, daysForHistroricalData, PriceDiffPercent, PlacedOrderPriceDiffPercent, RiskPercentForStock, RiskPercentForFuture, MaxLossValue, MaxLossPercent, OverallLoss, OverallProfitAmt, OverallProfitPercent, TotalOpenPositions);
                    }
                }
                else
                {
                    DisplayMsgIfFoundNull();
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("TradingSetting", "btnApply_Click : Exception Error Message = " + ex.Message, MessageType.Informational);
            }
        }

        private void DisplayMsgIfFoundNull()
        {
            string parameter = "";
            if (txtStartTime.Text == "") { parameter += "\nStart Time"; }
            if (txtEndTime.Text == "") { parameter += "\nEnd Time"; }
            if (txtTimeLimitToPlaceOrder.Text == "") { parameter += "\nTime Limit To Place Order"; }
            if (txtTimeForExitAllOpenPosition.Text == "") { parameter += "\nTime For Exit All Open Position"; }
            if (txtPercentQuantity.Text == "") { parameter += "\nPercent Quantity"; }
            if (txttotalmoney.Text == "") { parameter += "\nTotal Money"; }
            if (txtBarInterval.Text == "") { parameter += "\nBar Interval"; }
            if (txtMaxStrategyOrderCount.Text == "") { parameter += "\nMax Strategy Order Count"; }
            if (txtindiviualCount.Text == "") { parameter += "\nIndiviual Count"; }
            if (txtPriceDiffPercent.Text == "") { parameter += "\nPrice Diff Percent"; }
            if (txtPlacedOrderPriceDiffPercent.Text == "") { parameter += "\nPlaced Order Price Diff Percent"; }
            if (txtRiskPercentForStock.Text == "") { parameter += "\nRisk Percent For Stock"; }
            if (txtRiskPercentForFuture.Text == "") { parameter += "\nRisk Percent For Future"; }
            if (txtMaxLossValue.Text == "") { parameter += "\nMax Loss Value"; }
            if (txtMaxLossPercent.Text == "") { parameter += "\nMax Loss Percent"; }
            if (txtOverallLoss.Text == "") { parameter += "\nOverall Loss"; }
            if (txtOverallProfitAmt.Text == "") { parameter += "\nOverall Profit Amt"; }
            if (txtOverallProfitPercent.Text == "") { parameter += "\nOverall Profit Percent"; }
            if (txtTotalOpenPositions.Text == "") { parameter += "\nTotal Open Positions"; }
            //Pratiksha::19-08-2021::New field StopLossChangeTime
            if (txtStopLossChangeTime.Text == "") { parameter += "\nStopLoss Change Time"; }
            if (txtdaysForHistroricalData.Text == "") { parameter += "\nDays For Histrorical Data"; }
            message = "Please enter proper values for following field: " + parameter;
            dialog = System.Windows.Forms.MessageBox.Show(message, title, btnOK, MessageBoxIcon.Information);
            WriteUniquelogs("TradingSetting", "btnApply_Click : Please enter proper values.", MessageType.Informational);
        }

        private void SaveInIniFile(string filePath)
        {
            if (iniObj == null)
            {
                iniObj = new INIFile(filePath);
            }
            string[] key = new string[24];
            string[] value = new string[24];
            key[0] = "startTime";
            key[1] = "endTime";
            key[2] = "TimeLimitToPlaceOrder";
            key[3] = "TimeForExitAllOpenPosition";
            key[4] = "PercentQuantity";
            key[5] = "totalmoney";
            key[6] = "BarInterval";
            key[7] = "maxStrategyOrderCount";
            key[8] = "individualCount";
            key[9] = "daysForHistroricalData";
            key[10] = "PriceDiffPercent";
            key[11] = "PlacedOrderPriceDiffPercent";
            key[12] = "RiskPercentForStock";
            key[13] = "RiskPercentForFuture";
            key[14] = "readDataFromDB";
            key[15] = "AutoDownloadHistoricalData";
            key[16] = "IsNeedToParallel";
            key[17] = "MaxLossValue";
            key[18] = "MaxLossPercent";
            key[19] = "OverallLoss";
            key[20] = "OverallProfitAmt";
            key[21] = "OverallProfitPercent";
            key[22] = "TotalOpenPositions";
            //Pratiksha::19-08-2021::New field StopLossChangeTime
            key[23] = "StopLossChangeTime";

            value[0] = txtStartTime.Text;
            value[1] = txtEndTime.Text;
            value[2] = txtTimeLimitToPlaceOrder.Text;
            value[3] = txtTimeForExitAllOpenPosition.Text;
            value[4] = txtPercentQuantity.Text;
            value[5] = txttotalmoney.Text;
            value[6] = txtBarInterval.Text;
            value[7] = txtMaxStrategyOrderCount.Text;
            value[8] = txtindiviualCount.Text;
            value[9] = txtdaysForHistroricalData.Text;
            value[10] = txtPriceDiffPercent.Text;
            value[11] = txtPlacedOrderPriceDiffPercent.Text;
            value[12] = txtRiskPercentForStock.Text;
            value[13] = txtRiskPercentForFuture.Text;

            if (TruereadDataFromDB.Checked == true)
            {
                value[14] = "true";
            }
            else if (FalsereadDataFromDB.Checked == true)
            {
                value[14] = "false";
            }

            if (TrueAutoDownloadHistoricalData.Checked == true)
            {
                value[15] = "true";
            }
            else if (FalseAutoDownloadHistoricalData.Checked == true)
            {
                value[15] = "false";
            }

            if (TrueIsNeedToParallel.Checked == true)
            {
                value[16] = "true";
            }
            else if (FalseIsNeedToParallel.Checked == true)
            {
                value[16] = "false";
            }

            value[17] = txtMaxLossValue.Text;
            value[18] = txtMaxLossPercent.Text;
            value[19] = txtOverallLoss.Text;
            value[20] = txtOverallProfitAmt.Text;
            value[21] = txtOverallProfitPercent.Text;
            value[22] = txtTotalOpenPositions.Text;
            //Pratiksha::19-08-2021::New field StopLossChangeTime
            value[23] = txtStopLossChangeTime.Text;

            for (int i = 0; i < 24; i++)
            {
                iniObj.clearTestingSymbol("SDATA", key[i], value[i], filePath);
                WriteUniquelogs("TradingSetting", "btnApply_Click : " + key[i] + " = " + value[i], MessageType.Informational);
            }
        }

        public void WriteUniquelogs(string symbol, string message, MessageType msgType)
        {
            try
            {
                SymbolLogs log = new SymbolLogs();
                log.createFile(symbol);
                log.LogMessage(message, MessageType.Informational);
                log = null;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : " + e.Message);
                Trace.WriteLine("Exception : " + e.Message);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
                WriteUniquelogs("TradingSetting", "btnCancel_Click : Cancel click.", MessageType.Informational);
            }
            catch (Exception ex)
            {
                WriteUniquelogs("TradingSetting", "btnCancel_Click : Exception Error Message = " + ex.Message, MessageType.Informational);
            }
        }

        private void Clearfields()
        {
            try
            {
                txtStartTime.Text = null;
                txtEndTime.Text = null;
                txtTimeLimitToPlaceOrder.Text = null;
                txtTimeForExitAllOpenPosition.Text = null;
                txtPercentQuantity.Text = null;
                txttotalmoney.Text = null;
                txtBarInterval.Text = null;
                txtMaxStrategyOrderCount.Text = null;
                txtindiviualCount.Text = null;
                txtdaysForHistroricalData.Text = null;
                txtPriceDiffPercent.Text = null;
                txtPlacedOrderPriceDiffPercent.Text = null;
                txtRiskPercentForStock.Text = null;
                txtRiskPercentForFuture.Text = null;
                FalsereadDataFromDB.Checked = true;
                FalseAutoDownloadHistoricalData.Checked = true;
                FalseIsNeedToParallel.Checked = true;
                txtMaxLossValue.Text = null;
                txtMaxLossPercent.Text = null;
                txtOverallLoss.Text = null;
                txtOverallProfitAmt.Text = null;
                txtOverallProfitPercent.Text = null;
                txtTotalOpenPositions.Text = null;
                //Pratiksha::19-08-2021::New field StopLossChangeTime
                txtStopLossChangeTime.Text = null;
                WriteUniquelogs("TradingSetting", "Clearfields : All fields cleared.", MessageType.Informational);
            }
            catch (Exception ex)
            {
                WriteUniquelogs("TradingSetting", "Clearfields : Exception Error Message = " + ex.Message, MessageType.Informational);
            }
        }

        private void btnOpenSymbolSetting_Click(object sender, EventArgs e)
        {
            try
            {
                SymbolSetting symbolObj = new SymbolSetting(this.AlgoOMSObj, readSettings, m_FrmTitle, m_userID);
                symbolObj.ShowDialog();
            }
            catch (Exception ex)
            {
                WriteUniquelogs("TradingSetting", "btnOpenSymbolSetting_Click : Exception Error Message = " + ex.Message, MessageType.Informational);
            }
        }

        private void Validations(out Match PercentQuantity, out Match totalmoney, out Match BarInterval, out Match MaxStrategyOrderCount,
            out Match indiviualCount, out Match daysForHistroricalData, out Match PriceDiffPercent, out Match PlacedOrderPriceDiffPercent,
            out Match RiskPercentForStock, out Match RiskPercentForFuture, out Match MaxLossValue, out Match MaxLossPercent,
            out Match OverallLoss, out Match OverallProfitAmt, out Match OverallProfitPercent, out Match TotalOpenPositions)
        {
            string Decpattern = "^[0-9]+(.|,)?[0-9]*?$";
            Regex r1 = new Regex(Decpattern);
            PercentQuantity = r1.Match(txtPercentQuantity.Text.ToString());
            totalmoney = r1.Match(txttotalmoney.Text.ToString());
            BarInterval = r1.Match(txtBarInterval.Text.ToString());
            MaxStrategyOrderCount = r1.Match(txtMaxStrategyOrderCount.Text.ToString());
            indiviualCount = r1.Match(txtindiviualCount.Text.ToString());
            daysForHistroricalData = r1.Match(txtdaysForHistroricalData.Text.ToString());
            PriceDiffPercent = r1.Match(txtPriceDiffPercent.Text.ToString());
            PlacedOrderPriceDiffPercent = r1.Match(txtPlacedOrderPriceDiffPercent.Text.ToString());
            RiskPercentForStock = r1.Match(txtRiskPercentForStock.Text.ToString());
            RiskPercentForFuture = r1.Match(txtRiskPercentForFuture.Text.ToString());
            MaxLossValue = r1.Match(txtMaxLossValue.Text.ToString());
            MaxLossPercent = r1.Match(txtMaxLossPercent.Text.ToString());
            OverallLoss = r1.Match(txtOverallLoss.Text.ToString());
            OverallProfitAmt = r1.Match(txtOverallProfitAmt.Text.ToString());
            OverallProfitPercent = r1.Match(txtOverallProfitPercent.Text.ToString());
            TotalOpenPositions = r1.Match(txtTotalOpenPositions.Text.ToString());
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog SaveFDExportFile = new System.Windows.Forms.SaveFileDialog();
                string filePath = DirPath + @"ChanbreakoutStrategy_" + m_userID + ".ini";
                SaveInIniFile(filePath);
                FetchDetailsFromINI();
                Match PercentQuantity, totalmoney, BarInterval, MaxStrategyOrderCount, indiviualCount, daysForHistroricalData, PriceDiffPercent,
                    PlacedOrderPriceDiffPercent, RiskPercentForStock, RiskPercentForFuture, MaxLossValue, MaxLossPercent, OverallLoss,
                    OverallProfitAmt, OverallProfitPercent, TotalOpenPositions;
                Validations(out PercentQuantity, out totalmoney, out BarInterval, out MaxStrategyOrderCount, out indiviualCount,
                    out daysForHistroricalData, out PriceDiffPercent, out PlacedOrderPriceDiffPercent, out RiskPercentForStock,
                    out RiskPercentForFuture, out MaxLossValue, out MaxLossPercent, out OverallLoss, out OverallProfitAmt,
                    out OverallProfitPercent, out TotalOpenPositions);
                if (PercentQuantity.Success && totalmoney.Success && BarInterval.Success &&
                    MaxStrategyOrderCount.Success && indiviualCount.Success && daysForHistroricalData.Success &&
                    PriceDiffPercent.Success && PlacedOrderPriceDiffPercent.Success && RiskPercentForStock.Success &&
                    RiskPercentForFuture.Success && MaxLossValue.Success && MaxLossPercent.Success &&
                    OverallLoss.Success && OverallProfitAmt.Success && OverallProfitPercent.Success && TotalOpenPositions.Success)
                {
                    string pathname = "";
                    string strPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                    SaveFDExportFile.InitialDirectory = @strPath;
                    SaveFDExportFile.DefaultExt = "txt";
                    SaveFDExportFile.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                    SaveFDExportFile.Title = "Export File";
                    SaveFDExportFile.FilterIndex = 2;
                    if (SaveFDExportFile.ShowDialog() == DialogResult.OK)
                    {
                        pathname = SaveFDExportFile.FileName;
                        SaveInIniFile(pathname);
                        message = "File exported successfully!!!";
                        dialog = System.Windows.Forms.MessageBox.Show(message, title, btnOK, MessageBoxIcon.Information);
                        WriteUniquelogs("TradingSettingLogs", "btnExport_Click : " + message, MessageType.Informational);
                    }
                }
                else
                {
                    ValidationSuccessOrFail(PercentQuantity, totalmoney, BarInterval, MaxStrategyOrderCount, indiviualCount, daysForHistroricalData, PriceDiffPercent, PlacedOrderPriceDiffPercent, RiskPercentForStock, RiskPercentForFuture, MaxLossValue, MaxLossPercent, OverallLoss, OverallProfitAmt, OverallProfitPercent, TotalOpenPositions);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("TradingSettingLogs", "btnExport_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void ValidationSuccessOrFail(Match PercentQuantity, Match totalmoney, Match BarInterval, Match MaxStrategyOrderCount, Match indiviualCount, Match daysForHistroricalData, Match PriceDiffPercent, Match PlacedOrderPriceDiffPercent, Match RiskPercentForStock, Match RiskPercentForFuture, Match MaxLossValue, Match MaxLossPercent, Match OverallLoss, Match OverallProfitAmt, Match OverallProfitPercent, Match TotalOpenPositions)
        {
            string Logmessage = "";
            string parameter = "";
            if (!PercentQuantity.Success) { parameter += "\nPercent Quantity,"; Logmessage += "Percent Quantity,"; }
            if (!totalmoney.Success) { parameter += "\nTotal Money"; Logmessage += "Total Money,"; }
            if (!BarInterval.Success) { parameter += "\nBar Interval"; Logmessage += "Bar Interval,"; }
            if (!MaxStrategyOrderCount.Success) { parameter += "\nMax Strategy Order Count"; Logmessage += "Max Strategy Order Count,"; }
            if (!indiviualCount.Success) { parameter += "\nIndiviual Count"; Logmessage += "Indiviual Count,"; }
            if (!daysForHistroricalData.Success) { parameter += "\nDays For Histrorical Data"; Logmessage += "Days For Histrorical Data,"; }
            if (!PriceDiffPercent.Success) { parameter += "\nPrice Diff Percent"; Logmessage += "Price Diff Percent,"; }
            if (!PlacedOrderPriceDiffPercent.Success) { parameter += "\nPlaced Order Price Diff Percent"; Logmessage += "Placed Order Price Diff Percent,"; }
            if (!RiskPercentForStock.Success) { parameter += "\nRisk Percent For Stock"; Logmessage += "Risk Percent For Stock,"; }
            if (!RiskPercentForFuture.Success) { parameter += "\nRisk Percent For Future"; Logmessage += "Risk Percent For Future,"; }
            if (!MaxLossValue.Success) { parameter += "\nMax Loss Value"; Logmessage += "Max Loss Value,"; }
            if (!MaxLossPercent.Success) { parameter += "\nMax Loss Percent"; Logmessage += "Max Loss Percent,"; }
            if (!OverallLoss.Success) { parameter += "\nOverall Loss"; Logmessage += "Overall Loss,"; }
            if (!OverallProfitAmt.Success) { parameter += "\nOverall Profit Amt"; Logmessage += "Overall Profit Amt,"; }
            if (!OverallProfitPercent.Success) { parameter += "\nOverall Profit Percent"; Logmessage += "Overall Profit Percent,"; }
            if (!TotalOpenPositions.Success) { parameter += "\nTotal Open Positions,"; Logmessage += "Total Open Positions"; }
            message = "Please add valid values for " + parameter + ".";
            dialog = System.Windows.Forms.MessageBox.Show(message, title, btnOK, MessageBoxIcon.Information);
            WriteUniquelogs("SymbolSettingLogs", "btnApply_Click : " + Logmessage, MessageType.Informational);
        }

        private void BtnImport_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog OpenFDImportFile = new OpenFileDialog();
                string pathname = "";
                string strPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                string filePath = DirPath + "\\ChanbreakoutStrategy_" + m_userID + ".ini";
                OpenFDImportFile.InitialDirectory = @strPath; // "C:\";
                OpenFDImportFile.DefaultExt = "txt";
                OpenFDImportFile.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                if (OpenFDImportFile.ShowDialog() == DialogResult.OK)
                {
                    INIFile iniObj = new INIFile(pathname);
                    pathname = OpenFDImportFile.FileName;
                    if (readSettings == null)
                    {
                        readSettings = new IRDSStategyExecutorMultiAccount.ReadSettings(logger);
                    }
                    readSettings.ReadTradingDetails(pathname);

                    try
                    {
                        GetDetails(pathname);
                    }
                    catch (Exception ex)
                    {
                        WriteUniquelogs("TradingSettingLogs", "BtnImport_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
                    }
                    SaveInIniFile(filePath);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("TradingSettingLogs", "BtnImport_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }
    }
}

