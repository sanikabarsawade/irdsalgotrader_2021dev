﻿using IRDSAlgoOMS;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Linq;

namespace IRDSStategyExecutorMultiAccount
{   
    public  class ReadSettings
    {
        static INIFile inif;
        public  string username = "";
        public double CommanLotSize = 0;
        public  decimal overallProfit = 0;
        public  decimal overallLoss = 0;
        public string startTime = "";
        public string interval = "";
        public string maxCount = "";
        public string endTime = " ";
        public  string totalMoney = "";
        public string TimeLimitToPlaceOrder = "";
        public string TimeForExitAllOpenPosition = "";
        public decimal Slippage = 0;
        public int barCount = 0;
        public string individualCount = "";
        public List<string> tradingSymbolListWithAllData = new List<string>();
        public TradingSymbolInfo tradingSymbolInfo;
        public List<TradingSymbolInfo> TradingSymbolsInfoList = new List<TradingSymbolInfo>();
        public List<string> TradingsymbolList = new List<string>();
        public List<string> TradingsymbolListWithFutureName = new List<string>();
        Logger logger;
        AlgoOMS AlgoOMS;
        public bool isReadDataFromDB = true;
        public double Percent = 0;
        public double ProfitTrail = 0;
        public int daysForHistroricalData = 0;
        public bool m_AutoDowbloadHistoricalData = false;
        public double priceDiffPercent = 0;
        public double m_RiskPercentForStock = 0;
        public double m_RiskPercentForFuture = 0;
        public double m_MaxLossPercentForIndividualSymbol = 0;
        public double m_MaxLossValueForIndividualSymbol = 0;
        public double PlacedOrderPriceDiffPercent = 0;
        public double m_OverallLoss = 0;
        public double m_OverallProfitAmt = 0;
        public double m_OverallProfitPercent = 0;
        public bool m_IsNeedToRunParallel = false;
        public int m_TotalOpenPositions = 0;
        //Pratiksha::19-08-2021::New field StopLossChangeTime
        public string m_StopLossChangeTime = "";

        //IRDSPM::PRatiksha::22-04-2021::For symbol list
        public List<string> RealtimeSymbolfromsymbolini = new List<string>();
        public List<string> SymbolListFromsymbolini = new List<string>();
        List<string> DecryptDetListComposite;
        List<string> EncDetListComposite;

        public string root = "";
        public string appKey = "";
        public string secretKey = "";
        public string marketDataAppKey = "";
        public string marketDataSecretKey = "";
        public bool showGUIForOpenPositions = false;

        List<string> PlainDetListZerodha;
        List<string> EncDetListZerodha;
        List<string> decryptDetListZerodha;
        //Zerodha

        public string Zroot = "";
        public string Zuserid = "";
        public string ZappKey = "";
        public string ZsecretKey = "";
        public string ZPassword = "";
        public string ZPin = "";
        public string Zlogin = "";
        public List<string> tradingSymbolListWithAllDataChan_sandipini = new List<string>();

        List<string> PlainDetList;
        //public double highLowPercent = 0;


        public ReadSettings(Logger logger)
        {
            this.logger = logger;
        }
        public ReadSettings(Logger logger,AlgoOMS AlgoOMS )
        {
            if(this.logger == null)
                this.logger = logger;
            this.AlgoOMS = AlgoOMS;
        }
        public string m_BrokerName = "";
        //08-AUG-2021::Pratiksha::Added title and version change
        public string m_Version = "";
        public string m_Title = "";
        public  bool readOHL_SandipConfigFile(string filename)
        {
            bool isload = true;
            try
            {
                logger.LogMessage("In readConfigFile", MessageType.Informational);
                inif = new INIFile(filename);                
                Percent = Convert.ToDouble(inif.IniReadValue("SDATA", "percent"));
                overallLoss = Convert.ToDecimal(inif.IniReadValue("SDATA", "overall_loss"));
                overallProfit = Convert.ToDecimal(inif.IniReadValue("SDATA", "overall_profit"));
                totalMoney = inif.IniReadValue("SDATA", "totalmoney");
                startTime = inif.IniReadValue("SDATA", "startTime");
                endTime = inif.IniReadValue("SDATA", "endTime");
                interval = inif.IniReadValue("SDATA", "interval");
                maxCount = inif.IniReadValue("SDATA", "maxCount");
                individualCount = inif.IniReadValue("SDATA", "indiviualCount");
                Slippage = Convert.ToDecimal(inif.IniReadValue("SDATA", "slippage"));
                TimeLimitToPlaceOrder = inif.IniReadValue("SDATA", "TimeLimitToPlaceOrder");
                TimeForExitAllOpenPosition = inif.IniReadValue("SDATA", "TimeForExitAllOpenPosition");                
                tradingSymbolListWithAllData = inif.GetKeyValues("TRADINGSYMBOL");
                TradingsymbolList.Clear();
                TradingsymbolListWithFutureName.Clear();
                foreach (var symbol in tradingSymbolListWithAllData)
                {
                    string[] values = symbol.Split(',');
                    if (!(values.Length < 7))
                    {
                        tradingSymbolInfo = new TradingSymbolInfo();
                        TradingsymbolList.Add(values[0]);
                        tradingSymbolInfo.SymbolWithExchange = values[0];
                        tradingSymbolInfo.Quantity = Convert.ToInt32(values[1]);
                        tradingSymbolInfo.Volume = Convert.ToDecimal(values[2]);
                        tradingSymbolInfo.ProfitPercent = Convert.ToDouble(values[3]);
                        tradingSymbolInfo.Stoploss = Convert.ToDouble(values[4]);
                        tradingSymbolInfo.Difference1 = Convert.ToDecimal(values[5]);
                        tradingSymbolInfo.Difference2 = Convert.ToDecimal(values[6]);                        
                        string exchnage = values[0].Split('.')[1];
                        tradingSymbolInfo.Exchange = exchnage;
                        string TradingSymbol = values[0].Split('.')[0];
                        tradingSymbolInfo.TradingSymbol = TradingSymbol;
                        if (exchnage == "NFO")
                        {
                            TradingSymbol = this.AlgoOMS.GetFutureSymbolName(username, TradingSymbol);
                        }
                        TradingsymbolListWithFutureName.Add(TradingSymbol);
                    }
                    else
                    {
                        isload = false;
                        logger.LogMessage("Some values are missing in TRADINGSYMBOL section.. Please check ini file", MessageType.Informational);
                        break;
                    }

                    TradingSymbolsInfoList.Add(tradingSymbolInfo);
                }
                logger.LogMessage("readConfigFile - End", MessageType.Informational);
            }
            catch(Exception e)
            {
                logger.LogMessage("readConfigFile - exception"+e.Message, MessageType.Informational);
            }
            return isload;
        }

        //public void writeINIFile(string section,string key,string value)
        //{
        //    logger.LogMessage("In writeINIFile", MessageType.Informational);
        //    inif.IniWriteValue(section, key, value);
        //    logger.LogMessage("writeINIFile - End", MessageType.Informational);
        //}
        public string readUserId(string filepath)
        {
            string userId = "";
            try
            {
                //logger.LogMessage("In readUserId", MessageType.Informational);
                inif = new INIFile(filepath);
                userId = inif.IniReadValue("credentials", "MyUserId");
                List<string> list = new List<string>();
                list.Add(userId);
                decryptDet(list);
                if (PlainDetList.Count > 0)
                    username = userId = PlainDetList[0];
                else
                    username = userId;
                //logger.LogMessage("readUserId - End", MessageType.Informational);
            }
            catch(Exception e)
            {
                logger.LogMessage("readUserId : Exception Error Message = "+e.Message, MessageType.Exception);
            }
            return userId;
        }

        public void readTradingSymbols(string filepath)
        {
            try
            {
                TradingsymbolList.Clear();
                //logger.LogMessage("In readTradingSymbols", MessageType.Informational);
                inif = new INIFile(filepath);
                List<string> AllData = inif.GetKeyValues("TRADINGSYMBOL");
                foreach (var symbol in AllData)
                {
                    string[] str = symbol.Split(',');
                    TradingsymbolList.Add((str[0]));
                }
            }
            catch (Exception e)
            {
                logger.LogMessage("readTradingSymbols : Exception Error Message = "+e.Message, MessageType.Informational);
            }
        }

        public void readDayLimit(string filepath)
        {
            try
            {
                //logger.LogMessage("In readDayLimit", MessageType.Informational);
                inif = new INIFile(filepath);
                daysForHistroricalData = Convert.ToInt32(inif.IniReadValue("SDATA", "daysForHistroricalData"));
                //logger.LogMessage("readDayLimit - End", MessageType.Informational);
            }
            catch (Exception e)
            {
                logger.LogMessage("readDayLimit : Exception Error Message = " + e.Message, MessageType.Informational);
            }
        }

        //sanika::10-dec-2020::Added check to auto download historical data
        public bool ReadAutoDownloadFlag(string filepath)
        {
            try
            {
                //logger.LogMessage("In readDayLimit", MessageType.Informational);
                inif = new INIFile(filepath);
                m_AutoDowbloadHistoricalData = Convert.ToBoolean(inif.IniReadValue("SDATA", "AutoDownloadHistoricalData"));
                //logger.LogMessage("readDayLimit - End", MessageType.Informational);
            }
            catch (Exception e)
            {
                logger.LogMessage("readDayLimit : Exception Error Message = " + e.Message, MessageType.Informational);
            }

            return m_AutoDowbloadHistoricalData;
        }
        public double m_PercentQuantity = 0;
        public string m_Ctotalmoney = "";
        public string m_BarInterval = "";
        public string m_MaxStrategyOrderCount = "";
        public string m_DaysForHistroricalData = "";
        public bool m_AutoDownloadHistoricalData = false;
        //sanika::29-Jul-2021::slippage changes 
        public double m_SlippageValue = 0;
        public bool ReadChanBreakoutConfigFile(string filename)
        {
            bool isload = true;
            try
            {
                ReadTradingDetails(filename);
                ReadTradingSymbolList(filename);
            }
            catch (Exception e)
            {
                logger.LogMessage("readConfigFile : Exception Error Message =" + e.Message, MessageType.Informational);
            }
            return isload;
        }

        public void ReadTradingSymbolList(string filename)
        {
            inif = new INIFile(filename);
            TradingSymbolsInfoList.Clear();
            tradingSymbolListWithAllData.Clear();
            tradingSymbolListWithAllData = inif.GetKeyValues("TRADINGSYMBOL");
            TradingsymbolList.Clear();
            TradingsymbolListWithFutureName.Clear();
            foreach (var symbol in tradingSymbolListWithAllData)
            {
                string[] values = symbol.Split(',');
                if (!(values.Length < 8))
                {
                    tradingSymbolInfo = new TradingSymbolInfo();
                    TradingsymbolList.Add(values[0]);
                    tradingSymbolInfo.SymbolWithExchange = values[0];
                    tradingSymbolInfo.Quantity = Convert.ToDouble(values[1]);
                    tradingSymbolInfo.BarCount = Convert.ToDouble(values[2]);
                    tradingSymbolInfo.ProfitPercent = Convert.ToDouble(values[3]);
                    tradingSymbolInfo.HighLowPercent = Convert.ToDouble(values[4]);
                    tradingSymbolInfo.Stoploss = Convert.ToDouble(values[5]);
                    tradingSymbolInfo.UpdatedStopLoss = Convert.ToDouble(values[6]);
                    tradingSymbolInfo.ProfitTrail = Convert.ToDouble(values[7]);
                    tradingSymbolInfo.UpdatedProfitTrail = Convert.ToDouble(values[8]);
                    //Pratiksha::17-08-2021::Added 2 new fields for percentage
                    tradingSymbolInfo.NiftyPercent = Convert.ToDouble(values[9]);
                    tradingSymbolInfo.BankNiftyPercent = Convert.ToDouble(values[10]);
                    //Pratiksha::19-08-2021::2 New field StopLossChange and UpdatedStopLossChange
                    tradingSymbolInfo.StopLossChange = Convert.ToDouble(values[11]);
                    tradingSymbolInfo.UpdatedStopLossChange = Convert.ToDouble(values[12]);
                    string exchnage = values[0].Split('.')[1];
                    tradingSymbolInfo.Exchange = exchnage;
                    string TradingSymbol = values[0].Split('.')[0];
                    tradingSymbolInfo.TradingSymbol = TradingSymbol;
                    if (exchnage == "NFO")
                    {
                        TradingSymbol = this.AlgoOMS.GetFutureSymbolName(username, TradingSymbol);
                    }
                    TradingsymbolListWithFutureName.Add(TradingSymbol);
                }
                else
                {
                    logger.LogMessage("Some values are missing in TRADINGSYMBOL section.. Please check ini file", MessageType.Informational);
                    break;
                }

                //var value = TradingSymbolsInfoList.Find(r => r.TradingSymbol == tradingSymbolInfo.TradingSymbol);
                //if (value == null)
                //{
                TradingSymbolsInfoList.Add(tradingSymbolInfo);
                //}

            }
        }

        public void ReadTradingDetails(string filename)
        {
            inif = new INIFile(filename);
            startTime = inif.IniReadValue("SDATA", "startTime");
            endTime = inif.IniReadValue("SDATA", "endTime");
            TimeLimitToPlaceOrder = inif.IniReadValue("SDATA", "TimeLimitToPlaceOrder");
            TimeForExitAllOpenPosition = inif.IniReadValue("SDATA", "TimeForExitAllOpenPosition");
            m_PercentQuantity = Convert.ToDouble(inif.IniReadValue("SDATA", "PercentQuantity"));
            totalMoney = inif.IniReadValue("SDATA", "totalmoney");
            m_BarInterval = inif.IniReadValue("SDATA", "BarInterval");
            m_MaxStrategyOrderCount = inif.IniReadValue("SDATA", "maxStrategyOrderCount");
            individualCount = inif.IniReadValue("SDATA", "individualCount");
            m_DaysForHistroricalData = inif.IniReadValue("SDATA", "daysForHistroricalData");
            priceDiffPercent = Convert.ToDouble(inif.IniReadValue("SDATA", "PriceDiffPercent"));
            PlacedOrderPriceDiffPercent = Convert.ToDouble(inif.IniReadValue("SDATA", "PlacedOrderPriceDiffPercent"));
            m_RiskPercentForStock = Convert.ToDouble(inif.IniReadValue("SDATA", "RiskPercentForStock"));
            m_RiskPercentForFuture = Convert.ToDouble(inif.IniReadValue("SDATA", "RiskPercentForFuture"));
            isReadDataFromDB = Convert.ToBoolean(inif.IniReadValue("SDATA", "readDataFromDB"));
            m_AutoDownloadHistoricalData = Convert.ToBoolean(inif.IniReadValue("SDATA", "AutoDownloadHistoricalData"));
            m_IsNeedToRunParallel = Convert.ToBoolean(inif.IniReadValue("SDATA", "IsNeedToParallel"));
            m_MaxLossValueForIndividualSymbol = Convert.ToDouble(inif.IniReadValue("SDATA", "MaxLossValue"));
            m_MaxLossPercentForIndividualSymbol = Convert.ToDouble(inif.IniReadValue("SDATA", "MaxLossPercent"));
            m_OverallLoss = Convert.ToDouble(inif.IniReadValue("SDATA", "OverallLoss"));
            m_OverallProfitAmt = Convert.ToDouble(inif.IniReadValue("SDATA", "OverallProfitAmt"));
            m_OverallProfitPercent = Convert.ToDouble(inif.IniReadValue("SDATA", "OverallProfitPercent"));
            m_TotalOpenPositions = Convert.ToInt32(inif.IniReadValue("SDATA", "TotalOpenPositions"));
            //Pratiksha::19-08-2021::New field StopLossChangeTime
            m_StopLossChangeTime = inif.IniReadValue("SDATA", "StopLossChangeTime");
            //sanika::29-Jul-2021::slippage changes 
            //m_SlippageValue = Convert.ToDouble(inif.IniReadValue("SDATA", "SlippageValue"));
        }

        public bool readEMA_SandipConfigFile(string filename)
        {
            bool isload = true;
            try
            {
                //logger.LogMessage("In readConfigFile", MessageType.Informational);
                inif = new INIFile(filename);                
                Percent = Convert.ToDouble(inif.IniReadValue("SDATA", "percent"));
                overallLoss = Convert.ToDecimal(inif.IniReadValue("SDATA", "overall_loss"));
                overallProfit = Convert.ToDecimal(inif.IniReadValue("SDATA", "overall_profit"));
                totalMoney = inif.IniReadValue("SDATA", "totalmoney");
                startTime = inif.IniReadValue("SDATA", "startTime");
                endTime = inif.IniReadValue("SDATA", "endTime");
                interval = inif.IniReadValue("SDATA", "interval");
                maxCount = inif.IniReadValue("SDATA", "maxCount");
                individualCount = inif.IniReadValue("SDATA", "indiviualCount");
                TimeLimitToPlaceOrder = inif.IniReadValue("SDATA", "TimeLimitToPlaceOrder");
                TimeForExitAllOpenPosition = inif.IniReadValue("SDATA", "TimeForExitAllOpenPosition");
                isReadDataFromDB = Convert.ToBoolean(inif.IniReadValue("SDATA", "readDataFromDB"));
                CommanLotSize = Convert.ToDouble(inif.IniReadValue("SDATA", "commonLotSize"));
                ProfitTrail = Convert.ToDouble(inif.IniReadValue("SDATA", "profitTrail"));
                tradingSymbolListWithAllData = inif.GetKeyValues("TRADINGSYMBOL");
                TradingsymbolList.Clear();
                TradingsymbolListWithFutureName.Clear();
                foreach (var symbol in tradingSymbolListWithAllData)
                {
                    string[] values = symbol.Split(',');
                    if (!(values.Length < 7))
                    {
                        tradingSymbolInfo = new TradingSymbolInfo();
                        TradingsymbolList.Add(values[0]);
                        tradingSymbolInfo.SymbolWithExchange = values[0];
                        tradingSymbolInfo.Quantity = Convert.ToInt32(values[1]);
                        tradingSymbolInfo.BarCount = Convert.ToInt32(values[2]);
                        tradingSymbolInfo.MaxPeriodEMA = Convert.ToInt32(values[3]);
                        tradingSymbolInfo.MinPeriodEMA = Convert.ToInt32(values[4]);
                        tradingSymbolInfo.ProfitPercent = Convert.ToDouble(values[5]);
                        tradingSymbolInfo.Stoploss = Convert.ToDouble(values[6]);
                        string exchnage = values[0].Split('.')[1];
                        tradingSymbolInfo.Exchange = exchnage;
                        string TradingSymbol = values[0].Split('.')[0];
                        tradingSymbolInfo.TradingSymbol = TradingSymbol;
                        if (exchnage == "NFO")
                        {
                            TradingSymbol = this.AlgoOMS.GetFutureSymbolName(username, TradingSymbol);
                        }
                        TradingsymbolListWithFutureName.Add(TradingSymbol);
                    }
                    else
                    {
                        isload = false;
                        logger.LogMessage("Some values are missing in TRADINGSYMBOL section.. Please check ini file", MessageType.Informational);
                        break;
                    }

                    TradingSymbolsInfoList.Add(tradingSymbolInfo);
                }
                //logger.LogMessage("readConfigFile - End", MessageType.Informational);
            }
            catch (Exception e)
            {
                logger.LogMessage("readConfigFile:  Exception Error Message =" + e.Message, MessageType.Informational);
            }
            return isload;
        }

        public void decryptDet(List<string> EncDetList)
        {
            try
            {
                if(PlainDetList == null)
                    PlainDetList = new List<string>();

                PlainDetList.Clear();
                for (int i = 0; i < EncDetList.Count; i++)
                {
                    string plaintext = DecryptData(EncDetList[i], "IrdsalgoTrader");
                    PlainDetList.Add(plaintext);
                }
            }
            catch (Exception e)
            {
                logger.LogMessage("decryptDet : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        public static string DecryptData(string strCryptTxt, string strKey)
        {
            strCryptTxt = strCryptTxt.Replace(" ", "+");

            byte[] bytesBuff = Convert.FromBase64String(strCryptTxt);

            using (Aes aes__1 = Aes.Create())
            {
                Rfc2898DeriveBytes crypto = new Rfc2898DeriveBytes(strKey, new byte[] {
                0x49,
                0x76,
                0x61,
                0x6e,
                0x20,
                0x4d,
                0x65,
                0x64,
                0x76,
                0x65,
                0x64,
                0x65,
                0x76
            });
                aes__1.Key = crypto.GetBytes(32);
                aes__1.IV = crypto.GetBytes(16);

                using (MemoryStream mStream = new MemoryStream())
                {
                    using (CryptoStream cStream = new CryptoStream(mStream, aes__1.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cStream.Write(bytesBuff, 0, bytesBuff.Length);
                        cStream.Close();
                    }

                    strCryptTxt = Encoding.Unicode.GetString(mStream.ToArray());
                }
            }
            return strCryptTxt;
        }
        public void writeINIFileSymbolSetting(string section, string key, string value)
        {
            inif.IniWriteValueSymbolSetting(section, key, value);
        }
        public void writeINIFileWithPath(string section, string key, string value)
        {
            inif.IniWriteValueWithpath(section, key, value);
        }
        
        public List<string> ReadCompositeAccountNumbers(string path)
        {
            List<string> accountNumbers = new List<string>();
            List<string> values = new List<string>();
            inif = new INIFile(path);
            values = inif.GetKeyValues("CompositeAccountNumbers");
            if (values.Count > 0)
            {
                string line = values.ElementAt(0);
                string[] accountNo = line.Split(',');
                accountNumbers = accountNo.ToList();
            }
            return accountNumbers;
        }
        //IRDSPM::pratiksha::17-06-2021::For zerodha accounts reading
        public List<string> ReadZerodhaAccountNumbers(string path)
        {
            List<string> accountNumbers = new List<string>();
            List<string> values = new List<string>();
            inif = new INIFile(path);
            values = inif.GetKeyValues("ZerodhaAccountNumbers");
            if (values.Count > 0)
            {
                string line = values.ElementAt(0);
                string[] accountNo = line.Split(',');
                accountNumbers = accountNo.ToList();
            }
            return accountNumbers;
        }
        //IRDSPM::Pratiksha::07-07-2021::For reading broker name
        public void ReadBrokerName(string filename)
        {
            inif = new INIFile(filename);
            m_BrokerName = inif.IniReadValue("BROKER", "brokername");
            //08-AUG-2021::Pratiksha::Added title and version change
            m_Title = inif.IniReadValue("Title", "title");
            m_Version = inif.IniReadValue("Title", "Version");
        }
        public void readConfigFileZerodha(string filename)
        {
            //PlainDetListZerodha = new List<string>();
            EncDetListZerodha = new List<string>();
            decryptDetListZerodha = new List<string>();
            try
            {
                logger.LogMessage("In readConfigFileZerodha", MessageType.Informational);
                inif = new INIFile(filename);
                Zroot = inif.IniReadValue("credentials", "root");
                Zlogin = inif.IniReadValue("credentials", "login");
                string api = inif.IniReadValue("credentials", "MyAPIKey");
                string secret = inif.IniReadValue("credentials", "MySecret");
                string id = inif.IniReadValue("credentials", "MyUserId");
                string pwd = inif.IniReadValue("credentials", "MyPassword");
                string pin = inif.IniReadValue("credentials", "Pin");


                if (api != "XXXXXX" && secret != "XXXXXX" && id != "XXXXXX" && pwd != "XXXXXX" && pin != "XXXXXX")
                {
                    EncDetListZerodha.Add(api); EncDetListZerodha.Add(secret); EncDetListZerodha.Add(id); EncDetListZerodha.Add(pwd); EncDetListZerodha.Add(pin);
                    decryptDetZerodha(EncDetListZerodha);
                    ZappKey = decryptDetListZerodha[0];
                    ZsecretKey = decryptDetListZerodha[1];
                    Zuserid = decryptDetListZerodha[2];
                    ZPassword = decryptDetListZerodha[3];
                    ZPin = decryptDetListZerodha[4];
                }
                else
                {
                    ZappKey = api;
                    ZsecretKey = secret;
                    Zuserid = id;
                    ZPassword = pwd;
                    ZPin = pin;
                }
            }
            catch (Exception e)
            {
                logger.LogMessage("readConfigFile - exception" + e.Message, MessageType.Informational);
            }
        }

        //Pratiksha::01-01-2021::For Composite
        public void decryptDetComposite(List<string> EncDetList)
        {
            try
            {
                DecryptDetListComposite = new List<string>();
                for (int i = 0; i < EncDetList.Count; i++)
                {
                    string plaintext = DecryptData(EncDetList[i], "IrdsalgoTrader");
                    DecryptDetListComposite.Add(plaintext);
                }
            }
            catch (Exception e)
            {
               // WriteUniquelogs("ReadConfigSettings", "decryptDetComposite : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }
        public void readCompositeConfigFile(string filename)
        {
            try
            {
                EncDetListComposite = new List<string>();
                inif = new INIFile(filename);
                root = inif.IniReadValue("credentials", "root");
                //string user_name = inif.IniReadValue("credentials", "username");
                string CName = inif.IniReadValue("credentials", "MyUserId");
                string CappKey = inif.IniReadValue("credentials", "appKey");
                string CsecretKey = inif.IniReadValue("credentials", "secretKey");
                string CmarketDataAppKey = inif.IniReadValue("credentials", "MarketDataappKey");
                string CmarketDataSecretKey = inif.IniReadValue("credentials", "MarketDatasecretKey");
                showGUIForOpenPositions = Convert.ToBoolean(inif.IniReadBoolValue("credentials", "showGUIForOpenPositions"));
                if (CName != "XXXXXX" && CappKey != "XXXXXX" && CsecretKey != "XXXXXX" && CmarketDataAppKey != "XXXXXX" && CmarketDataSecretKey != "XXXXXX")
                {
                    EncDetListComposite.Add(CName); EncDetListComposite.Add(CappKey); EncDetListComposite.Add(CsecretKey);
                    EncDetListComposite.Add(CmarketDataAppKey); EncDetListComposite.Add(CmarketDataSecretKey);
                    decryptDetComposite(EncDetListComposite);

                    username = DecryptDetListComposite[0];
                    appKey = DecryptDetListComposite[1];
                    secretKey = DecryptDetListComposite[2];
                    marketDataAppKey = DecryptDetListComposite[3];
                    marketDataSecretKey = DecryptDetListComposite[4];
                }
                else
                {
                    username = CName;
                    appKey = CappKey;
                    secretKey = CsecretKey;
                    marketDataAppKey = CmarketDataAppKey;
                    marketDataSecretKey = CmarketDataSecretKey;
                }
            }
            catch (Exception e)
            {
                //WriteUniquelogs("ReadConfigSettings", "readCompositeConfigFile : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }
        public void decryptDetZerodha(List<string> EncDetList)
        {
            try
            {
                decryptDetListZerodha = new List<string>();
                for (int i = 0; i < EncDetList.Count; i++)
                {
                    string plaintext = DecryptData(EncDetList[i], "IrdsalgoTrader");
                    decryptDetListZerodha.Add(plaintext);
                }
            }
            catch (Exception e)
            {
               // WriteUniquelogs("ReadConfigSettings", "decryptDetComposite : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }
    }
}
