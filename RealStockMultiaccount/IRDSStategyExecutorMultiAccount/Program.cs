﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IRDSStategyExecutorMultiAccount
{
    static class Program
    {
        static public System.Threading.Mutex mtex1 = null;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //bool instanceCountOne = false;
            //mtex1 = new System.Threading.Mutex(true, "IRDS_ROBO_Mutex1", out instanceCountOne);
            //if (!instanceCountOne)
            //{
            //    MessageBox.Show("An application instance is already running", "IntellirichOHLSystem");
            //    Application.Exit();
            //    return;
            //}
            //Application.Run(new form_autoTrading());
            Application.Run(new AutoTradingForm());
        }
    }
}
