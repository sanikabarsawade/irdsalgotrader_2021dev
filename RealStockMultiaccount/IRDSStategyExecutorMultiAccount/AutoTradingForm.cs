﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using IRDSAlgoOMS;
using IRDSStategyExecutorMultiAccount;

namespace IRDSStategyExecutorMultiAccount
{
    public partial class AutoTradingForm : Form
    {
        AlgoOMS AlgoOMS = null;
        public int m_StrategyCount = 1;
        Dictionary<string, form_autoTrading> m_StrategyObjects = null;
        List<string> m_CompositeAccountNumbers = new List<string>();
        List<string> m_ZerodhaAccountNumbers = new List<string>();
        Logger logger = Logger.Instance;
        ReadSettings Settings = null;
        string m_MsgTitle = "IRDS Strategy Executor ";
        string m_TabTitle = "IRDS Strategy ";
        string message = "";
        DialogResult dialog;
        string version = "";
        string m_FrmTitle = "";
        TabPage Tab1;
        List<TabPage> m_ListTabPages = new List<TabPage>();
        MessageBoxButtons btnOk = MessageBoxButtons.OK;
        MessageBoxButtons btnYesNo = MessageBoxButtons.YesNo;
        string m_BrokerName = "";
        string FilePath = Directory.GetCurrentDirectory() + @"\Configuration\";
        string FileDatabaseSettings = "DatabaseSettings.ini";
        string FileTradingSetting = "TradingSetting.ini";
        string FileSetting = "Setting.ini";
        string FileAccountNumber = "AccountNumber.ini";
        public AutoTradingForm()
        {
            try
            {
                if (File.Exists(FilePath + FileDatabaseSettings) && File.Exists(FilePath + FileTradingSetting) && File.Exists(FilePath + FileSetting) && File.Exists(FilePath + FileAccountNumber))
                {
                    if (Settings == null)
                    {
                        Settings = new ReadSettings(logger);
                    }
                    Settings.ReadBrokerName(FilePath + FileSetting);
                    m_BrokerName = Settings.m_BrokerName;
                    version = Settings.m_Version;
                    m_FrmTitle = Settings.m_Title;
                    this.Text = m_MsgTitle + version;
                    Assembly myAssembly = Assembly.GetExecutingAssembly();
                    Stream IconStream = null;
                    IconStream = myAssembly.GetManifestResourceStream("IRDSStategyExecutorMultiAccount.Resources." + m_FrmTitle + ".ico");
                    this.Icon = new System.Drawing.Icon(IconStream);
                    InitializeComponent();
                    NetworkChange.NetworkAvailabilityChanged +=
                     new NetworkAvailabilityChangedEventHandler(OnNetworkAvailabilityChanged);

                    m_StrategyObjects = new Dictionary<string, form_autoTrading>();
                    AlgoOMS = new AlgoOMS();
                    ReadSettings readSettings = new ReadSettings(null);
                    if (File.Exists(FilePath + FileAccountNumber))
                    {
                        m_CompositeAccountNumbers = readSettings.ReadCompositeAccountNumbers(FilePath + FileAccountNumber);
                        m_ZerodhaAccountNumbers = readSettings.ReadZerodhaAccountNumbers(FilePath + FileAccountNumber);
                    }
                    else
                    {
                        MessageBox.Show("AccountNumber.ini file does not exist!!", m_MsgTitle, btnOk, MessageBoxIcon.Warning);
                    }
                    if (m_BrokerName.ToLower() == "composite")
                    {
                        if (m_CompositeAccountNumbers.Count > 0)
                        {
                            for (int i = 0; i < m_CompositeAccountNumbers.Count; i++)
                            {
                                TabPage tp = CreateTab(i);
                                TabRealStockStrategy.TabPages[i].Text = /*m_TabTitle + (i + 1) + " " +*/ m_CompositeAccountNumbers[i];
                                if (i == 0)
                                {
                                    Tab1 = tp;
                                }
                            }
                        }
                    }
                    else if (m_BrokerName.ToLower() == "zerodha")
                    {
                        for (int i = 0; i < m_ZerodhaAccountNumbers.Count; i++)
                        {
                            TabPage tp = CreateTab(i);
                            TabRealStockStrategy.TabPages[i].Text = /*m_TabTitle + (i + 1) + " " +*/ m_ZerodhaAccountNumbers[i];
                            if (i == 0)
                            {
                                Tab1 = tp;
                            }
                        }
                    }
                    ShowForm(Tab1, "1");
                }
                else
                {
                    string parameter = "";
                    string Log = "";
                    if (!File.Exists(FilePath + FileDatabaseSettings)) { parameter += FileDatabaseSettings + "\n"; Log += FileDatabaseSettings + ","; }
                    if (!File.Exists(FilePath + FileTradingSetting)) { parameter += FileTradingSetting + "\n"; Log += FileTradingSetting + ","; }
                    if (!File.Exists(FilePath + FileSetting)) { parameter += FileSetting + "\n"; Log += FileSetting + ","; }
                    if (!File.Exists(FilePath + FileAccountNumber)) { parameter += FileAccountNumber + "\n"; Log += FileAccountNumber + ","; }
                    message = "Please add below ini files:\n" + parameter;
                    WriteUniquelogs("StrategySelection", "AutoTradingForm : " + "Please add below ini files:" + Log, MessageType.Informational);
                    dialog = System.Windows.Forms.MessageBox.Show(message, m_MsgTitle, btnOk, MessageBoxIcon.Warning);
                    Environment.Exit(0);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("StrategySelection", "AutoTradingForm: Exception Error Message " + ex.Message, MessageType.Exception);
            }
        }

        private TabPage CreateTab(int i)
        {
            TabPage tp = null;
            try
            {
                tp = new TabPage(m_TabTitle + (i + 1));
                TabRealStockStrategy.TabPages.Add(tp);
                tp.Location = new System.Drawing.Point(4, 41);
                tp.Name = m_TabTitle + i;
                tp.Padding = new System.Windows.Forms.Padding(3);
                tp.Size = new System.Drawing.Size(1766, 905);
                tp.TabIndex = 0;
                tp.UseVisualStyleBackColor = true;
                m_ListTabPages.Add(tp);
            }
            catch (Exception ex)
            {
                WriteUniquelogs("StrategySelection", "CreateTab: Exception Error Message " + ex.Message, MessageType.Exception);
            }
            return tp;
        }

        private void ShowForm(TabPage tp, string TabNumber)
        {
            try
            {
                if (Application.OpenForms[m_TabTitle + TabNumber] == null)
                {
                    TableLayoutPanel panel = new TableLayoutPanel();
                    panel.ColumnCount = 1;
                    panel.RowCount = 2;
                    panel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
                    panel.RowStyles.Add(new RowStyle(SizeType.Percent, 15F));
                    panel.RowStyles.Add(new RowStyle(SizeType.Percent, 85F));
                    panel.BringToFront();
                    panel.Location = new System.Drawing.Point(4, 41);
                    panel.Name = m_TabTitle + TabNumber;
                    panel.Padding = new System.Windows.Forms.Padding(3);
                    panel.Size = new System.Drawing.Size(1766, 905);
                    panel.TabIndex = 0;
                    panel.Dock = DockStyle.Fill;
                    tp.Controls.Add(panel);
                    string FileINISettings = "";
                    string FileStrategySetting = "";
                    string accountNumber = "";
                    if (Settings == null)
                    {
                        Settings = new ReadSettings(logger);
                    }
                    int index = 0;
                    if (m_BrokerName == "Composite")
                    {
                        index = Convert.ToInt32(TabNumber) - 1;
                        accountNumber = m_CompositeAccountNumbers.ElementAt(index);
                        WriteUniquelogs("StrategySelection", "ShowForm : Clicked on " + TabNumber + " strategy accountNumber " + accountNumber, MessageType.Informational);
                        FileINISettings = "CompositeSettings_" + accountNumber + ".ini";
                    }
                    else if (m_BrokerName == "Zerodha")
                    {
                        index = Convert.ToInt32(TabNumber) - 1;
                        accountNumber = m_ZerodhaAccountNumbers.ElementAt(index);
                        WriteUniquelogs("StrategySelection", "ShowForm: Clicked on" + TabNumber + " strategy accountNumber " + accountNumber, MessageType.Informational);
                        FileINISettings = "ZerodhaSettings_" + accountNumber + ".ini";
                    }
                    FileStrategySetting = "ChanbreakoutStrategy_" + accountNumber + ".ini"; 
                    if (AlgoOMS.GetLoginStatus(accountNumber) == false)
                    {
                        if (File.Exists(FilePath + FileINISettings) && File.Exists(FilePath + FileStrategySetting))
                        {
                            if (Application.OpenForms[m_TabTitle + TabNumber] == null)
                            {
                                IRDSAlgoOMS.MainWindow m_MainWindowobj = new IRDSAlgoOMS.MainWindow();
                                form_autoTrading m_StrategyFormobj = new form_autoTrading(accountNumber, m_MainWindowobj, m_FrmTitle, m_BrokerName, m_MsgTitle);
                                string Name = m_TabTitle + TabNumber;
                                m_StrategyFormobj.TopLevel = false;
                                m_StrategyFormobj.Dock = DockStyle.Fill;
                                m_StrategyFormobj.Text = Name;
                                m_StrategyFormobj.Name = Name;
                                m_StrategyFormobj.Show();
                                panel.Controls.Add(m_StrategyFormobj, 0, 0);
                                panel.AutoScroll = true;
                                m_MainWindowobj.TopLevel = false;
                                m_MainWindowobj.Dock = DockStyle.Fill;
                                m_MainWindowobj.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                                m_MainWindowobj.Text = Name;
                                m_MainWindowobj.Name = Name;
                                m_MainWindowobj.Show();
                                panel.Controls.Add(m_MainWindowobj, 0, 1);
                            }
                        }
                        else
                        {
                            string parameter = "";
                            string log = "";
                            if (!File.Exists(FilePath + FileINISettings)) { parameter += m_BrokerName + "Settings_" + accountNumber + ".ini" + "\n"; log += m_BrokerName + "Settings_" + accountNumber + ".ini" + ","; }
                            if (!File.Exists(FilePath + FileStrategySetting)) { parameter += "ChanbreakoutStrategy_" + accountNumber + ".ini" + "\n"; log += "StrategySetting_" + accountNumber + ".ini"; }

                            message = "Account not set on strategy " + TabNumber + ", because below files are missing:\n" + parameter;
                            string logMessage = message = "Account not set on strategy " + TabNumber + ", because below files are missing:" + log;
                            dialog = System.Windows.Forms.MessageBox.Show(message, m_MsgTitle, btnOk, MessageBoxIcon.Warning);
                            WriteUniquelogs("StrategySelection", "ShowForm: " + logMessage, MessageType.Informational);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("StrategySelection", "ShowForm: Exception Error Message " + ex.Message, MessageType.Exception);
            }
        }
        private void OnNetworkAvailabilityChanged(object sender, NetworkAvailabilityEventArgs e)
        {
            try
            {
                if (e.IsAvailable)
                {
                    Console.WriteLine("Network has become available");
                }
                else
                {
                    bool flag = false;
                    try
                    {
                        using (var client = new WebClient())
                        using (client.OpenRead("http://clients3.google.com/generate_204"))
                        {
                            flag = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        flag = false;
                        WriteUniquelogs("StrategySelection", "OnNetworkAvailabilityChanged: Exception Error Message " + ex.Message, MessageType.Exception);
                    }
                    if (!flag)
                    {
                        MessageBox.Show("Please check internet connection!!", m_MsgTitle, btnOk, MessageBoxIcon.Warning);
                    }
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("StrategySelection", "OnNetworkAvailabilityChanged: Exception Error Message " + ex.Message, MessageType.Exception);
            }
        }

        private void AutoTradingForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                string message = "Do you really want to exit?";
                dialog = System.Windows.Forms.MessageBox.Show(message, m_MsgTitle, btnYesNo, MessageBoxIcon.Warning);
                if (dialog == DialogResult.Yes)
                {
                    Environment.Exit(0);
                }
                else
                {
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("StrategySelection", "AutoTradingForm_FormClosing: Exception Error Message " + ex.Message, MessageType.Exception);
            }
        }

        private void accountSettingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Logger logger = Logger.Instance;
                FrmConfigSettings obj = new FrmConfigSettings(logger, m_FrmTitle);
                WriteUniquelogs("StrategySelection", "accountSettingToolStripMenuItem_Click: FrmConfigSettings Form Opens", MessageType.Informational);
                obj.ShowDialog();
            }
            catch (Exception ex)
            {
                WriteUniquelogs("StrategySelection", "accountSettingToolStripMenuItem_Click: Exception Error Message " + ex.Message, MessageType.Exception);
            }
        }

        public void WriteUniquelogs(string symbol, string message, MessageType msgType)
        {
            try
            {
                SymbolLogs log = new SymbolLogs();
                log.createFile(symbol);
                log.LogMessage(message, MessageType.Informational);
                log = null;
            }
            catch (Exception e)
            {
                Console.WriteLine("WriteUniquelogs : Exception : " + e.Message);
            }
        }

        private void TabRealStockStrategy_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string[] account = (TabRealStockStrategy.SelectedTab.Name).Split(' ');
                int index = Convert.ToInt32(account[2]);
                ShowForm(m_ListTabPages.ElementAt(index), (index + 1).ToString());
            }
            catch (Exception ex)
            {
                WriteUniquelogs("StrategySelection", "TabRealStockStrategy_SelectedIndexChanged: Exception Error Message " + ex.Message, MessageType.Exception);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                string message = "Do you really want to exit?";
                dialog = System.Windows.Forms.MessageBox.Show(message, m_MsgTitle, btnYesNo, MessageBoxIcon.Warning);
                if (dialog == DialogResult.Yes)
                {
                    WriteUniquelogs("StrategySelection", "btnClose_Click: You clicked Yes on Form Exit messageBox.", MessageType.Informational);
                    Environment.Exit(0);
                }
                else
                {
                    WriteUniquelogs("StrategySelection", "btnClose_Click: You clicked No on Form Exit messageBox.", MessageType.Informational);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("StrategySelection", "btnClose_Click: Exception Error Message " + ex.Message, MessageType.Exception);
            }
        }

        //26-July-2021::Pratiksha::Change the backcolor of tab Header
        private void TabRealStockStrategy_DrawItem(object sender, DrawItemEventArgs e)
        {
            try
            {
                Graphics g = e.Graphics;
                TabPage tp = TabRealStockStrategy.TabPages[e.Index];

                StringFormat sf = new StringFormat();
                sf.Alignment = StringAlignment.Center;
                sf.LineAlignment = StringAlignment.Center;
                // This is the rectangle to draw "over" the tabpage title
                RectangleF headerRect = new RectangleF(e.Bounds.X, e.Bounds.Y + 2, e.Bounds.Width, e.Bounds.Height - 2);

                // This is the default colour to use for the non-selected tabs
                SolidBrush sb = new SolidBrush(Color.DarkGray);

                // This changes the colour if we're trying to draw the selected tabpage
                if (TabRealStockStrategy.SelectedIndex == e.Index)
                    sb.Color = Color.Transparent;

                // Colour the header of the current tabpage based on what we did above
                g.FillRectangle(sb, e.Bounds);

                //Remember to redraw the text - I'm always using black for title text
                g.DrawString(tp.Text, TabRealStockStrategy.Font, new SolidBrush(Color.Black), headerRect, sf);
                TabRealStockStrategy.Font = new Font("Arial", 11, FontStyle.Bold);
            }
            catch (Exception ex)
            {
                WriteUniquelogs("StrategySelection", "tabControl1_DrawItem: Exception Error Message " + ex.Message, MessageType.Exception);
            }
        }
    }
}