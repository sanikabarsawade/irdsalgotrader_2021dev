﻿using IRDSStategyExecutorMultiAccount;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IRDSStategyExecutorMultiAccount
{
    public partial class frmStopLossPercent : Form
    {
        AutomationWithFile m_automation = null;
        public frmStopLossPercent(AutomationWithFile automationWithFile)
        {
            InitializeComponent();
            m_automation = automationWithFile;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if(txtStopLossPercent.Text != "")
            {
                if(m_automation !=null)
                {
                    m_automation.ModifyOrder(Convert.ToDouble(txtStopLossPercent.Text));
                    this.Close();
                }
            }
        }
    }
}
