﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using IRDSAlgoOMS;
using IRDSStategyExecutorMultiAccount;
using IRDSStategyExecutorMultiAccount.GUI;

namespace IRDSStategyExecutorMultiAccount
{
    public partial class form_autoTrading : Form
    {
        private IRDSAlgoOMS.AlgoOMS AlgoOMS = new IRDSAlgoOMS.AlgoOMS();
        AutomationWithFile automationWithFile;
        EMAAutomation eMAAutomation;
        Automation automation;
        bool login = false;
        bool automationStart = false;
        Logger logger = Logger.Instance;
        string userID = " ";
        ReadSettings Settings;
        bool isEMAstrategySelect = false;
        bool isChanbreakstrategySelect = false;
        bool isOpenHighStrategySelect = false;
        public bool isChanbreakStopped = false;
        public bool isOpenHighStopped = false;
        string m_BrokerName = "Zerodha";
        string m_FrmTitle = "";
        public delegate void UpdateControlsDelegate();
        string m_MsgTitle = "";
        MessageBoxButtons btnOK = MessageBoxButtons.OK;
        MessageBoxButtons btnYesNo = MessageBoxButtons.YesNo;
        string message = "";
        public form_autoTrading(string iniUsername, MainWindow obj, string FrmTitle, string BrokerName, string Msgtitle)
        {
            userID = iniUsername;
            m_FrmTitle = FrmTitle;
            m_MsgTitle = Msgtitle;
            m_BrokerName = BrokerName;
            Assembly myAssembly = Assembly.GetExecutingAssembly();
            Stream IconStream = null;
            IconStream = myAssembly.GetManifestResourceStream("IRDSStategyExecutorMultiAccount.Resources." + m_FrmTitle + ".ico");
            this.Icon = new System.Drawing.Icon(IconStream);
            InitializeComponent();
            string path = Directory.GetCurrentDirectory();
            path += @"\Configuration\Setting.ini";
            INIFile iNIFile = new INIFile(path);
            if (m_BrokerName == "Zerodha")
            {
                path = Directory.GetCurrentDirectory();
                string iniFile = path + @"\Configuration\" + "ZerodhaSettings_" + iniUsername + ".ini";
                if (File.Exists(iniFile))
                {
                    Settings = new ReadSettings(logger, AlgoOMS);
                    userID = Settings.readUserId(iniFile);
                    GPMain.Text = "For " + userID;
                }
                else
                {
                    message = "ZerodhaSettings_" + iniUsername + ".ini" + " file does not exist!!";
                    MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Information);
                }
                AlgoOMS.SetBroker(0);
                AlgoOMS.CreateKiteConnectObject(userID, "ZerodhaSettings_" + iniUsername + ".ini", obj);
            }
            else if (m_BrokerName == "Samco")
            {
                path = Directory.GetCurrentDirectory();
                string iniFile = path + @"\Configuration\" + "SamcoSettings.ini";
                if (File.Exists(iniFile))
                {
                    Settings = new ReadSettings(logger, AlgoOMS);
                }
                else
                {
                    message = "SamcoSettings.ini" + " file does not exist!!";
                    MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Information);
                }
                AlgoOMS.SetBroker(1);
                AlgoOMS.CreateKiteConnectObject(userID, "SamcoSettings.ini", obj);
            }
            else if (m_BrokerName == "Composite")
            {
                path = Directory.GetCurrentDirectory();
                string iniFile = path + @"\Configuration\" + "CompositeSettings_" + iniUsername + ".ini";
                if (File.Exists(iniFile))
                {
                    Settings = new ReadSettings(logger, AlgoOMS);
                    userID = Settings.readUserId(iniFile);
                    GPMain.Text = "For " + userID;
                }
                else
                {
                    message = "CompositeSettings_" + iniUsername + ".ini" + " file does not exist!!";
                    MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Information);
                }
                AlgoOMS.SetBroker(2);
                AlgoOMS.CreateKiteConnectObject(userID, "CompositeSettings_" + iniUsername + ".ini", obj);
            }
        }


        private void OnNetworkAvailabilityChanged(object sender, NetworkAvailabilityEventArgs e)
        {
            if (e.IsAvailable)
            {
                Console.WriteLine("Network has become available");
            }
            else
            {
                bool flag = false;
                try
                {
                    using (var client = new WebClient())
                    using (client.OpenRead("http://clients3.google.com/generate_204"))
                    {
                        flag = true;
                    }
                }
                catch
                {
                    flag = false;
                }
                if(!flag)
                {
                    message = "Please check internet connection!!";
                    MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Warning);
                }
                
            }
        }

        private void form_autoTrading_Load(object sender, EventArgs e)
        {
            try
            {
                NetworkChange.NetworkAvailabilityChanged +=
                 new NetworkAvailabilityChangedEventHandler(OnNetworkAvailabilityChanged);
                loginToAccount();
            }
            catch (Exception er)
            {
                Trace.WriteLine("exception from form load : " + er.Message);
            }
        }

        public void loginToAccount()
        {            
            if (ChanBreakStrategy.Checked)
                isChanbreakstrategySelect = true;
            if (OpenHighStrategy.Checked)
                isOpenHighStrategySelect = true;
            if (EMAStrategy.Checked)
                isEMAstrategySelect = true;


            if (isChanbreakstrategySelect == false && isOpenHighStrategySelect == false && isEMAstrategySelect == false)
            {
                message = "Please Select strategy first!!!";
                MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Warning);
            }
            else
            {
                btnLogin.Enabled = false;
                OpenHighStrategy.Enabled = false;
                ChanBreakStrategy.Enabled = false;
                EMAStrategy.Enabled = false;
                Cursor.Current = Cursors.WaitCursor;
                string error = "";
                int loginCount = 0;
                try
                {
                    logger.LogMessage("Clicked on login button", MessageType.Informational);
                    AlgoOMS.Connect(userID);
                    while (loginCount < 3)
                    {
                        if (AlgoOMS.kiteWrapperConnect != null || AlgoOMS.samcoWrapperConnect != null || AlgoOMS.compositeWrapperConnect != null)
                        {
                            if (AlgoOMS.GetLoginStatus(userID))
                            {
                                login = true;
                                if (isEMAstrategySelect)
                                {
                                    eMAAutomation = new EMAAutomation(AlgoOMS, logger, userID);

                                    eMAAutomation.loadObject(this);

                                    eMAAutomation.readTradingSymbol(Settings);
                                    //eMAAutomation.loadOpenPositions(userID, AlgoOMS, logger);
                                }
                                if (isChanbreakstrategySelect)
                                {
                                    automationWithFile = new AutomationWithFile(AlgoOMS, logger, userID);

                                    automationWithFile.loadObject(this);

                                    automationWithFile.readTradingSymbol(Settings);
                                    //automationWithFile.loadOpenPositions(userID, AlgoOMS, logger);

                                }
                                if (isOpenHighStrategySelect)
                                {
                                    automation = new Automation();

                                    automation.loadObject(this);
                                    automation.readTradingSymbol(Settings);
                                    //automation.loadOpenPositions(userID, AlgoOMS, logger);

                                }
                                break;
                            }
                            else
                            {
                                login = false;
                                logger.LogMessage("Not able to login trying " + loginCount, MessageType.Informational);
                                //AlgoOMS.ForeceFulRelogin(userID);
                            }
                        }
                        loginCount++;
                    }                    

                }
                catch (Exception er)
                {
                    error = er.Message;
                    if (error.Contains("empty") && error.Contains("api_key") && error.Contains("access_token"))
                    {
                        message = "Error : Please check account credentials";
                        MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Error);
                        logger.LogMessage("Error : " + er.Message, MessageType.Exception);
                    }
                    else
                    {
                        message = "Error : " + er.Message;
                        MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Error);
                        logger.LogMessage("Error : " + er.Message, MessageType.Exception);
                    }
                }
                finally
                {
                    if (error.Contains("System.Data.SQLite"))
                    {
                        message = "Please add dll for sqlite.";
                        MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Error);
                        logger.LogMessage("Please add dll for sqlite", MessageType.Informational);
                        //bool res = automation.StopThread();
                        this.Close();
                    }
                }                
                Cursor.Current = Cursors.Default;
            }
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            loginToAccount();
            if (!login)
            {
                message = "Not able to login. Please try with forcefully login!!";
                MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Warning);
                logger.LogMessage("Not able to login. Please try with forcefully login", MessageType.Informational);
            }
            else if (login)
            {
                message = "Logged in successfully!!";
                MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Information);
                logger.LogMessage("Logged on successfully", MessageType.Informational);
            }
        }


        private void btnStart_Click(object sender, EventArgs e)
        {
            StartAutomation();  
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            try
            {               
                logger.LogMessage("Clicked on stop button", MessageType.Informational);
                if (login)
                {
                    if(automationStart)
                    {
                        bool res = false;
                        if (isChanbreakstrategySelect)
                        {
                            res = automationWithFile.StopThread();                           
                        }
                        if (isEMAstrategySelect)
                        {
                            res = eMAAutomation.StopThread();
                        }
                        if (isOpenHighStrategySelect)
                        {
                            res = automation.StopThread();                           
                        }
                        if (res)
                        {
                            //IRDSPM::Pratiksha::29-07-2020::Change the title
                            message = "Automation stopped.";
                            MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Information);
                            logger.LogMessage("Automation stopped.", MessageType.Informational);
                            automationStart = false;
                            btnStart.Enabled = true;
                            ChanBreakStrategy.Enabled = true;
                            OpenHighStrategy.Enabled = true;
                        }
                        else
                        {
                            message = "Unable to stop automation.";
                            MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Information);
                            logger.LogMessage("Unable to stop automation.", MessageType.Informational);
                        }

                    }
                    else
                    {
                        message = "To perform Stop Automation, First you need to Start the automation.";
                        MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Information);
                        logger.LogMessage("To perform stop. first Start the automation", MessageType.Informational);
                    }
                }
                else
                {
                    message = "Stop operation not able to perform.";
                    MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Warning);
                    logger.LogMessage("Stop operation not able to perform", MessageType.Informational);
                }
            }
            catch(Exception er)
            {
                message = "Error : " + er.Message;
                MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Error);
                logger.LogMessage("Error : " + er.Message, MessageType.Informational);
            }
        }


        private void btnRefresh_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            logger.LogMessage("clicked on refresh button  ", MessageType.Informational);
            if (login)
            {
                try
                {
                    if (isChanbreakstrategySelect)
                    {
                        automationWithFile.readTradingSymbol(Settings);
                        //automationWithFile.loadOpenPositions(userID, AlgoOMS);
                    }
                    if (isOpenHighStrategySelect)
                    {
                        automation.readTradingSymbol(Settings);
                        //automation.loadOpenPositions(userID, AlgoOMS);
                    }
                }
                catch (Exception ew)
                {
                    logger.LogMessage("Exception from clicking refresh button : " + ew.Message, MessageType.Exception);
                }
            }
            else
            {
                logger.LogMessage("Please login first to check open positions", MessageType.Informational); 
                message = "Please login first to check open positions.";
                MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Information);
            }
            Cursor.Current = Cursors.Default;
        }

        public void enableButton()
        {           
            InvokeUpdateControls();
        }

        public void InvokeUpdateControls()
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new UpdateControlsDelegate(UpdateControls));
            }
            else
            {
                UpdateControls();
            }
        }

        private void UpdateControls()
        {
            if(isChanbreakStopped && isOpenHighStopped)
            {
                btnStart.Enabled = true;
            }            
        }
        

        private void ChanBreakStrategy_CheckedChanged_1(object sender, EventArgs e)
        {
            isChanbreakstrategySelect = true;
        }

        private void OpenHighStrategy_CheckedChanged(object sender, EventArgs e)
        {
            isOpenHighStrategySelect = true;
        }

        private void btnStopCloseOrder_Click(object sender, EventArgs e)
        {
            try
            {
                logger.LogMessage("Clicked on stop automation and close all order button", MessageType.Informational);
                if (login)
                {
                    if (automationStart)
                    {
                        bool res = false;
                        if (isEMAstrategySelect)
                        {
                            res = eMAAutomation.StopThread();
                            eMAAutomation.CloseAllOrder();
                        }
                        if (isChanbreakstrategySelect)
                        {
                            AlgoOMS.AddNotificationInQueue(userID, "Client : " + userID + "\nAutomation stopped and closed open orders. Clicked on Stop Automation and close all orders button\n"+DateTime.Now);
                            res = automationWithFile.StopThread();
                            automationWithFile.CloseAllOrder();
                        }
                        if (isOpenHighStrategySelect)
                        {
                            res = automation.StopThread();
                            automation.CloseAllOrder();
                        }
                        if (res)
                        {
                            message = "Automation stopped.";
                            MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Information);
                            logger.LogMessage("Automation stopped", MessageType.Informational);
                            automationStart = false;
                            btnStart.Enabled = true;
                            ChanBreakStrategy.Enabled = true;
                            OpenHighStrategy.Enabled = true;
                            EMAStrategy.Enabled = true;
                        }
                        else
                        {
                            message = "Unable to stop automation.";
                            MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Warning);
                            logger.LogMessage("Unable to stop automation", MessageType.Informational);
                        }

                    }
                    else
                    {
                        message = "To perform Stop Automation, First you need to start the automation.";
                        MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Information);
                        logger.LogMessage("To perform stop. first Start the automation", MessageType.Informational);
                    }
                }
                else
                {
                    message = "Stop operation not able to perform.";
                    MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Warning);
                    logger.LogMessage("Stop operation not able to perform", MessageType.Informational);
                }
            }
            catch (Exception er)
            {
                message = "Error : " + er.Message;
                MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Error);
                logger.LogMessage("Error : " + er.Message, MessageType.Informational);
            }
        }

        public void DownloadData()
        {
            logger.LogMessage("Started Download", MessageType.Informational);
            var path = Directory.GetCurrentDirectory();
            string iniFile = path + @"\Configuration\";
            if (isChanbreakstrategySelect)
            {                
                Settings.readTradingSymbols(iniFile + "ChanbreakoutStrategy_" + userID+".ini");
                Settings.readDayLimit(iniFile + "ChanbreakoutStrategy_" + userID + ".ini");
                AlgoOMS.DownloadData(userID, Settings.TradingsymbolList, Settings.daysForHistroricalData);
            }
            if (isEMAstrategySelect)
            {
                Settings.readTradingSymbols(iniFile + "EMA_Sandip.ini");
                Settings.readDayLimit(iniFile + "EMA_Sandip.ini");
                AlgoOMS.DownloadData(userID, Settings.TradingsymbolList, Settings.daysForHistroricalData);
            }
        }

        private void btnHistorical_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                if (login)
                {
                    
                    Cursor.Current = Cursors.Default;     
                    DownloadData();
                    logger.LogMessage("Stopped Download.", MessageType.Informational);
                    //IRDSPM::Pratiksha::29-07-2020::Change the title
                    message = "Successfully Downloaded.";
                    MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Information);
                    //else if(isOpenHighStrategySelect)
                    //{
                    //    Settings.readTradingSymbols(iniFile + "OHL_Sandip.ini");
                    //    Settings.readDayLimit(iniFile + "ChanbreakoutStrategy_" + userID+".ini");
                    //    AlgoOMS.DownloadData(userID,Settings.TradingsymbolList);
                    //}
                }
                else
                {
                    logger.LogMessage("Please login first to download data", MessageType.Informational);
                    message = "Please login first to download data.";
                    MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Information);
                }
            }
            catch(Exception ex)
            {
                logger.LogMessage("error in  btnHistorical_Click "+ex.Message, MessageType.Informational);
            }
        }

        private void EMAStrategy_CheckedChanged(object sender, EventArgs e)
        {
            isEMAstrategySelect = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AlgoOMS.AddNotificationInQueue(userID, "Client : " + userID + "\nExe closed.\n"+DateTime.Now);
            this.Close();
            //form_autoTrading_FormClosing(sender,e);
        }

        //sanika::25-sep-2020::added for resume testing
        public OpenOrderInfo GetObj()
        {
            if(automationWithFile != null)
                return automationWithFile.m_OrderInfo;
            return null;
        }

        //sanika::25-sep-2020::added for resume testing
        public void SaveBin()
        {
            var path = Directory.GetCurrentDirectory();
            string FileName = path+ @"\\Chanbreak.bin";//sanika::12-Feb-2021::changed file name
            Stream SaveFileStream = File.Create(FileName);
            BinaryFormatter serializer = new BinaryFormatter();
            //sanika::22-dec-2020::Added for exception
            OpenOrderInfo openOrderInfo =  GetObj();
            if(openOrderInfo != null)
                serializer.Serialize(SaveFileStream, openOrderInfo);
            SaveFileStream.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            PipeServer pipe = new PipeServer();
            pipe.serverStartMethod();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //PipeClient pipe = new PipeClient();
            PipeServer pipe = new PipeServer();
            pipe.StartClientMethod();
        }

        private void form_autoTrading_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                message = "Do you really want to exit from application?";
                DialogResult dialog = MessageBox.Show(message, m_MsgTitle, btnYesNo, MessageBoxIcon.Warning);
                if (dialog == DialogResult.Yes)
                {
                    logger.LogMessage("Message box display and clicked on yes!!", MessageType.Informational);
                    //sanika::25-sep-2020::added for resume testing
                    SaveBin();
                    logger.LogMessage("clicked on close form", MessageType.Informational);
                    if (login)
                    {
                        bool res = false;
                        if (AlgoOMS.stopThread())
                        {
                            logger.LogMessage("All threads are closed", MessageType.Informational);
                        }
                        if (isChanbreakstrategySelect)
                        {
                            //automationWithFile.SaveBin();
                            res = automationWithFile.StopThread();
                            if (res)
                            {
                                logger.LogMessage("Chanbreak threads are closed", MessageType.Informational);

                            }
                            else
                                logger.LogMessage("Threads are not able to close", MessageType.Informational);
                        }
                        if (isOpenHighStrategySelect)
                        {
                            res = automation.StopThread();
                            if (res)
                            {
                                logger.LogMessage("Open-High threads are closed", MessageType.Informational);
                                Environment.Exit(0);
                            }
                            else
                                logger.LogMessage("Threads are not able to close", MessageType.Informational);
                        }
                        Environment.Exit(0);
                    }
                }
                else
                {
                    e.Cancel = true;
                    logger.LogMessage("Message box display and clicked on No!!", MessageType.Informational);
                }

            }
            catch (Exception er)
            {
                message = "Error : " + er.Message;
                MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Error);
                logger.LogMessage("Error : " + er.Message, MessageType.Exception);
            }
        }

        string GetBroker(INIFile iNIFile)
        {
            string brokerName = "";
            try
            {
                brokerName = iNIFile.IniReadValue("BROKER", "brokername");
            }
            catch (Exception e)
            {
                Trace.WriteLine("exception " + e.Message);
            }
            if (brokerName == "")
            {
                brokerName = "Zerodha";
            }
            return brokerName;
        }

        private void btnModifyOrder_Click(object sender, EventArgs e)
        {
            if(automationStart)
            {
                if(automationWithFile != null)
                {
                    frmStopLossPercent frmStopLossPercent = new frmStopLossPercent(automationWithFile);
                    frmStopLossPercent.Show();
                }
            }
            else
            {
                message = "Please start automation first to modify orders.";
                MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Warning);
            }
        }

        public void StartAutomation()
        {
            btnStart.Enabled = false;
            ChanBreakStrategy.Enabled = false;
            OpenHighStrategy.Enabled = false;
            EMAStrategy.Enabled = false;
            try
            {
                logger.LogMessage("Clicked on start button", MessageType.Informational);
                if (login)
                {
                    automationStart = true;
                    if (isEMAstrategySelect)
                    {
                        DownloadData();
                        eMAAutomation.loadObject(this);
                        bool res = eMAAutomation.startThread(userID);
                        if (!res)
                        {
                            btnStart.Enabled = true;
                            message = "Some values are missing in TRADINGSYMBOL section. Please check ini file.";
                            MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Exclamation);
                        }
                    }
                    if (isChanbreakstrategySelect)
                    {
                        //sanika::10-dec-2020::Added check to auto download historical data
                        var path = Directory.GetCurrentDirectory();
                        string iniFile = path + @"\Configuration\";
                        bool autoDownload = Settings.ReadAutoDownloadFlag(iniFile + "ChanbreakoutStrategy_" + userID + ".ini");
                        if (autoDownload)
                        {
                            logger.LogMessage("autoDownload flag for historical data is true", MessageType.Informational);
                            DownloadData();
                        }
                        else
                        {
                            logger.LogMessage("autoDownload flag for historical data is false", MessageType.Informational);
                        }
                        automationWithFile.loadObject(this);
                        bool res = automationWithFile.startThread(userID);                        
                        if (!res)
                        {
                            btnStart.Enabled = true;
                            message = "Some values are missing in TRADINGSYMBOL section. Please check ini file.";
                            MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Exclamation);
                        }

                    }
                    if (isOpenHighStrategySelect)
                    {
                        bool res = automation.startThread(userID);
                        if (!res)
                        {
                            btnStart.Enabled = true;
                            message = "Some values are missing in TRADINGSYMBOL section. Please check ini file.";
                            MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Exclamation);
                        }
                    }
                }
                else
                {
                    if (isOpenHighStrategySelect == false && isChanbreakstrategySelect == false && isEMAstrategySelect == false)
                    {
                        message = "Please Select strategy first.";
                        MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Warning);
                        btnStart.Enabled = true;
                        ChanBreakStrategy.Enabled = true;
                        OpenHighStrategy.Enabled = true;
                        EMAStrategy.Enabled = true;
                    }
                    else
                    {
                        AlgoOMS.CreateKiteConnectObject(userID, "APISetting.ini",true);
                        login = true;
                        btnStart.Enabled = false;
                        // MessageBox.Show("Login successfully!!", "Successful", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        logger.LogMessage("Login successfully", MessageType.Informational);
                        automationStart = true;
                        btnLogin.Enabled = false;
                        if (isChanbreakstrategySelect)
                        {
                            automationWithFile = new AutomationWithFile(AlgoOMS, logger, userID);
                            automationWithFile.loadObject(this);
                            automationWithFile.readTradingSymbol(Settings);
                            //automationWithFile.loadOpenPositions(userID, AlgoOMS, logger);
                            bool res = automationWithFile.startThread(userID);
                            if (!res)
                            {
                                btnStart.Enabled = true;
                                ChanBreakStrategy.Enabled = true;
                                OpenHighStrategy.Enabled = true;
                                EMAStrategy.Enabled = true;
                                message = "Some values are missing in TRADINGSYMBOL section. Please check ini file.";
                                MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Exclamation);
                            }
                        }
                        if (isOpenHighStrategySelect)
                        {
                            automation = new Automation();
                            automation.loadObject(this);

                            automation.readTradingSymbol(Settings);
                            //automation.loadOpenPositions(userID, AlgoOMS, logger);
                            bool res = automation.startThread(userID);
                            if (!res)
                            {
                                btnStart.Enabled = true;
                                ChanBreakStrategy.Enabled = true;
                                OpenHighStrategy.Enabled = true;
                                EMAStrategy.Enabled = true;
                                message = "Some values are missing in TRADINGSYMBOL section. Please check ini file.";
                                MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Exclamation);
                            }
                        }
                    }
                }
            }
            catch (Exception er)
            {
                message = "Error : " + er.Message;
                MessageBox.Show(message, m_MsgTitle, btnOK, MessageBoxIcon.Error);
                logger.LogMessage("Error : " + er.Message, MessageType.Exception);
            }
        }


        private void btnTradingSetting_Click(object sender, EventArgs e)
        {
            Logger logger = Logger.Instance;
            TradingSetting obj = new TradingSetting(logger, AlgoOMS, m_FrmTitle,userID);
            obj.ShowDialog();
        }

        private void btnSymbolSetting_Click(object sender, EventArgs e)
        {
            IRDSStategyExecutorMultiAccount.GUI.SymbolSetting obj = new IRDSStategyExecutorMultiAccount.GUI.SymbolSetting(AlgoOMS, Settings, m_FrmTitle, userID);
            obj.ShowDialog();
        }

        private void btnStart_EnabledChanged(object sender, EventArgs e)
        {
            if (btnStart.Enabled == false)
            {
                btnStart.BackColor = Color.DarkGray;
            }
            else
            {
                btnStart.BackColor = Color.Gainsboro;
            }
        }
    }
}
