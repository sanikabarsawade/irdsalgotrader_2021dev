﻿namespace IRDSStategyExecutorMultiAccount
{
    partial class form_autoTrading
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GPMain = new System.Windows.Forms.GroupBox();
            this.DPAutomationSetting = new System.Windows.Forms.GroupBox();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnStopCloseOrder = new System.Windows.Forms.Button();
            this.GPTradingSetting = new System.Windows.Forms.GroupBox();
            this.btnSymbolSetting = new System.Windows.Forms.Button();
            this.btnTradingSetting = new System.Windows.Forms.Button();
            this.btnHistorical = new System.Windows.Forms.Button();
            this.btnModifyOrder = new System.Windows.Forms.Button();
            this.btnLogin = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.EMAStrategy = new System.Windows.Forms.CheckBox();
            this.ChanBreakStrategy = new System.Windows.Forms.CheckBox();
            this.OpenHighStrategy = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.GPMain.SuspendLayout();
            this.DPAutomationSetting.SuspendLayout();
            this.GPTradingSetting.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // GPMain
            // 
            this.GPMain.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.GPMain.Controls.Add(this.DPAutomationSetting);
            this.GPMain.Controls.Add(this.GPTradingSetting);
            this.GPMain.Controls.Add(this.groupBox3);
            this.GPMain.Font = new System.Drawing.Font("Arial", 10.2F);
            this.GPMain.Location = new System.Drawing.Point(7, -1);
            this.GPMain.Name = "GPMain";
            this.GPMain.Size = new System.Drawing.Size(1546, 115);
            this.GPMain.TabIndex = 0;
            this.GPMain.TabStop = false;
            // 
            // DPAutomationSetting
            // 
            this.DPAutomationSetting.Controls.Add(this.btnStop);
            this.DPAutomationSetting.Controls.Add(this.btnStart);
            this.DPAutomationSetting.Controls.Add(this.btnStopCloseOrder);
            this.DPAutomationSetting.Font = new System.Drawing.Font("Arial", 10.2F);
            this.DPAutomationSetting.Location = new System.Drawing.Point(488, 20);
            this.DPAutomationSetting.Name = "DPAutomationSetting";
            this.DPAutomationSetting.Size = new System.Drawing.Size(445, 80);
            this.DPAutomationSetting.TabIndex = 13;
            this.DPAutomationSetting.TabStop = false;
            this.DPAutomationSetting.Text = "Automation Setting";
            // 
            // btnStop
            // 
            this.btnStop.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStop.Location = new System.Drawing.Point(156, 19);
            this.btnStop.Margin = new System.Windows.Forms.Padding(4);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(134, 47);
            this.btnStop.TabIndex = 15;
            this.btnStop.Text = "Stop Automation";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnStart
            // 
            this.btnStart.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStart.Location = new System.Drawing.Point(11, 19);
            this.btnStart.Margin = new System.Windows.Forms.Padding(4);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(134, 47);
            this.btnStart.TabIndex = 14;
            this.btnStart.Text = "Start Automation";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.EnabledChanged += new System.EventHandler(this.btnStart_EnabledChanged);
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnStopCloseOrder
            // 
            this.btnStopCloseOrder.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStopCloseOrder.Location = new System.Drawing.Point(303, 19);
            this.btnStopCloseOrder.Margin = new System.Windows.Forms.Padding(4);
            this.btnStopCloseOrder.Name = "btnStopCloseOrder";
            this.btnStopCloseOrder.Size = new System.Drawing.Size(134, 47);
            this.btnStopCloseOrder.TabIndex = 16;
            this.btnStopCloseOrder.Text = "Stop & Close Orders";
            this.btnStopCloseOrder.UseVisualStyleBackColor = true;
            this.btnStopCloseOrder.Click += new System.EventHandler(this.btnStopCloseOrder_Click);
            // 
            // GPTradingSetting
            // 
            this.GPTradingSetting.Controls.Add(this.btnSymbolSetting);
            this.GPTradingSetting.Controls.Add(this.btnTradingSetting);
            this.GPTradingSetting.Controls.Add(this.btnHistorical);
            this.GPTradingSetting.Controls.Add(this.btnModifyOrder);
            this.GPTradingSetting.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GPTradingSetting.Location = new System.Drawing.Point(941, 20);
            this.GPTradingSetting.Margin = new System.Windows.Forms.Padding(4);
            this.GPTradingSetting.Name = "GPTradingSetting";
            this.GPTradingSetting.Padding = new System.Windows.Forms.Padding(4);
            this.GPTradingSetting.Size = new System.Drawing.Size(593, 80);
            this.GPTradingSetting.TabIndex = 11;
            this.GPTradingSetting.TabStop = false;
            this.GPTradingSetting.Text = "Trading Setting";
            // 
            // btnSymbolSetting
            // 
            this.btnSymbolSetting.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSymbolSetting.Location = new System.Drawing.Point(298, 19);
            this.btnSymbolSetting.Margin = new System.Windows.Forms.Padding(4);
            this.btnSymbolSetting.Name = "btnSymbolSetting";
            this.btnSymbolSetting.Size = new System.Drawing.Size(134, 47);
            this.btnSymbolSetting.TabIndex = 9;
            this.btnSymbolSetting.Text = "Symbol Setting";
            this.btnSymbolSetting.UseVisualStyleBackColor = true;
            this.btnSymbolSetting.Click += new System.EventHandler(this.btnSymbolSetting_Click);
            // 
            // btnTradingSetting
            // 
            this.btnTradingSetting.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTradingSetting.Location = new System.Drawing.Point(152, 18);
            this.btnTradingSetting.Margin = new System.Windows.Forms.Padding(4);
            this.btnTradingSetting.Name = "btnTradingSetting";
            this.btnTradingSetting.Size = new System.Drawing.Size(134, 47);
            this.btnTradingSetting.TabIndex = 8;
            this.btnTradingSetting.Text = "Trading Setting";
            this.btnTradingSetting.UseVisualStyleBackColor = true;
            this.btnTradingSetting.Click += new System.EventHandler(this.btnTradingSetting_Click);
            // 
            // btnHistorical
            // 
            this.btnHistorical.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHistorical.Location = new System.Drawing.Point(8, 19);
            this.btnHistorical.Margin = new System.Windows.Forms.Padding(4);
            this.btnHistorical.Name = "btnHistorical";
            this.btnHistorical.Size = new System.Drawing.Size(134, 47);
            this.btnHistorical.TabIndex = 5;
            this.btnHistorical.Text = "Historical Data";
            this.btnHistorical.UseVisualStyleBackColor = true;
            this.btnHistorical.Click += new System.EventHandler(this.btnHistorical_Click);
            // 
            // btnModifyOrder
            // 
            this.btnModifyOrder.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModifyOrder.Location = new System.Drawing.Point(444, 18);
            this.btnModifyOrder.Margin = new System.Windows.Forms.Padding(4);
            this.btnModifyOrder.Name = "btnModifyOrder";
            this.btnModifyOrder.Size = new System.Drawing.Size(134, 47);
            this.btnModifyOrder.TabIndex = 17;
            this.btnModifyOrder.Text = "Modify Orders";
            this.btnModifyOrder.UseVisualStyleBackColor = true;
            this.btnModifyOrder.Click += new System.EventHandler(this.btnModifyOrder_Click);
            // 
            // btnLogin
            // 
            this.btnLogin.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogin.Location = new System.Drawing.Point(926, 93);
            this.btnLogin.Margin = new System.Windows.Forms.Padding(4);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(120, 47);
            this.btnLogin.TabIndex = 0;
            this.btnLogin.Text = "Login";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Visible = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.EMAStrategy);
            this.groupBox3.Controls.Add(this.ChanBreakStrategy);
            this.groupBox3.Controls.Add(this.OpenHighStrategy);
            this.groupBox3.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(10, 20);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox3.Size = new System.Drawing.Size(471, 80);
            this.groupBox3.TabIndex = 12;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Strategy Selection";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(9, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 22);
            this.label1.TabIndex = 5;
            this.label1.Text = "Strategies:";
            // 
            // EMAStrategy
            // 
            this.EMAStrategy.AutoSize = true;
            this.EMAStrategy.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EMAStrategy.Location = new System.Drawing.Point(386, 31);
            this.EMAStrategy.Margin = new System.Windows.Forms.Padding(4);
            this.EMAStrategy.Name = "EMAStrategy";
            this.EMAStrategy.Size = new System.Drawing.Size(66, 23);
            this.EMAStrategy.TabIndex = 4;
            this.EMAStrategy.Text = "EMA";
            this.EMAStrategy.UseVisualStyleBackColor = true;
            // 
            // ChanBreakStrategy
            // 
            this.ChanBreakStrategy.AutoSize = true;
            this.ChanBreakStrategy.Checked = true;
            this.ChanBreakStrategy.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ChanBreakStrategy.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChanBreakStrategy.Location = new System.Drawing.Point(248, 31);
            this.ChanBreakStrategy.Margin = new System.Windows.Forms.Padding(4);
            this.ChanBreakStrategy.Name = "ChanBreakStrategy";
            this.ChanBreakStrategy.Size = new System.Drawing.Size(113, 23);
            this.ChanBreakStrategy.TabIndex = 3;
            this.ChanBreakStrategy.Text = "ChanBreak";
            this.ChanBreakStrategy.UseVisualStyleBackColor = true;
            // 
            // OpenHighStrategy
            // 
            this.OpenHighStrategy.AutoSize = true;
            this.OpenHighStrategy.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OpenHighStrategy.Location = new System.Drawing.Point(113, 31);
            this.OpenHighStrategy.Margin = new System.Windows.Forms.Padding(4);
            this.OpenHighStrategy.Name = "OpenHighStrategy";
            this.OpenHighStrategy.Size = new System.Drawing.Size(109, 23);
            this.OpenHighStrategy.TabIndex = 2;
            this.OpenHighStrategy.Text = "Open-High";
            this.OpenHighStrategy.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(798, 93);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(120, 47);
            this.button1.TabIndex = 7;
            this.button1.Text = "Close";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            // 
            // form_autoTrading
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1570, 140);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnLogin);
            this.Controls.Add(this.GPMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "form_autoTrading";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "IntellirichOHLSystem";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.form_autoTrading_FormClosing);
            this.Load += new System.EventHandler(this.form_autoTrading_Load);
            this.GPMain.ResumeLayout(false);
            this.DPAutomationSetting.ResumeLayout(false);
            this.GPTradingSetting.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GPMain;
        private System.Windows.Forms.GroupBox GPTradingSetting;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnHistorical;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox DPAutomationSetting;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnStopCloseOrder;
        private System.Windows.Forms.Button btnModifyOrder;
        private System.Windows.Forms.Button btnSymbolSetting;
        private System.Windows.Forms.Button btnTradingSetting;
        private System.Windows.Forms.CheckBox EMAStrategy;
        private System.Windows.Forms.CheckBox ChanBreakStrategy;
        private System.Windows.Forms.CheckBox OpenHighStrategy;
        private System.Windows.Forms.Label label1;
    }
}

