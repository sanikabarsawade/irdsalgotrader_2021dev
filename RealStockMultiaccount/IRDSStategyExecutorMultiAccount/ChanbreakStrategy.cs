﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using IRDSAlgoOMS;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;

namespace IntellirichOHLSystem
{
    //sanika::25-sep-2020::added for resume 
    [Serializable()]
    public class ChanbreakStrategy
    {
        static string path = Directory.GetCurrentDirectory();
        string iniFile = path + @"\Configuration\" + "Chan_Sandip.ini";
        string FilePath = path + "\\Data\\ZerodhaData\\";
        Dictionary<string, string[]> FileData = new Dictionary<string, string[]>();
        //sanika::25-sep-2020::added for resume 
        [field: NonSerialized()]
        AlgoOMS AlgoOMS;
        //sanika::25-sep-2020::added for resume 
        [field: NonSerialized()]
        ReadSettings readSettings;
        //sanika::25-sep-2020::added for resume 
        [field: NonSerialized()]
        Logger logger;
        string userID;
        int OrderCount = 1;
        int MaxOrderCount = 0;
        int IndividualOrderCount = 0;
        string StartTime = "";
        string EndTime = "";
        int Interval = 0;
        string FirstTickEnd = "";
        string StrategyStartTime = "";
        string End = "";
        string TimeForExitAllOpenPosition = "";
        string TimeLimitToPlaceOrder = "";
        double OverallProfit = 0;
        double OverallLoss = 0;
        List<TradingSymbolInfo> ListOfTradingSymbolsInfo;
        //sanika::25-sep-2020::added for resume 
        [field: NonSerialized()]
        Thread automation = null;
        //int individualOrderCount = 0;
        //sanika::25-sep-2020::added for resume 
        [field: NonSerialized()]
        form_autoTrading form_Auto;
        //List<string> ListForGridView = new List<string>();
        double Percent = 0;
        double TotalMoney = 0;
        double ProfitTrail = 0;
        public OpenOrderInfo m_OrderInfo;
        bool m_isCloseAllOrder = false;
        string m_BinFileName = path +@"\\Chanbreak.bin";//sanika::12-Feb-2021::changed / to \\
        bool m_isAllowSLMOrder = true;
        double PriceDiffPercent = 0;
        double PlacedOrderPriceDiffPercent = 0;
        double m_RiskPercentForFuture = 0;
        double m_RiskPercentForStock = 0;
        double m_MaxLossValueForIndividualSymbol = 0;
        double m_MaxLossPercentForIndividualSymbol = 0;
        bool isFirstOrderPlaced = false;
        bool m_IsThreadStop = false;
        bool m_IsRiskManagementCheckAllow = false;
        public double m_OverallLoss = 0;
        public double m_OverallProfitAmt = 0;
        public double m_OverallProfitPercent = 0;
        bool m_OverallProfitAmtExceeds = false;
        //sanika::26-Nov-2020::Added for ajay sir changes
        // bool m_GetTodaysHighLow = true;      //sanika::4-dec-2020::Commented bacause handled by barcount 
        //bool deepLog = false;
        public ChanbreakStrategy()
        {

        }
        public ChanbreakStrategy(AlgoOMS AlgoOMS, Logger logger, string userID)
        {
            this.AlgoOMS = AlgoOMS;
            this.userID = userID;
            this.logger = logger;
            m_OrderInfo = new OpenOrderInfo(this);
            //sanika::28-sep-2020::added new function to load old structure 
            LoadStructure(m_OrderInfo);  
        }

        public void readCSVFile(string path)
        {
            try
            {
                FileData.Clear();
                int idx = path.LastIndexOf('\\');
                string symbol = (path.Substring(idx + 1)).Split('.')[0];
                string[] csvRows = System.IO.File.ReadAllLines(path);
                FileData.Add(symbol, csvRows);
            }
            catch (Exception e)
            {
                logger.LogMessage("Exception from readCSVFile " + e.Message, MessageType.Informational);
            }
        }

        public void getHighLowValues(string TradingSymbol, int barCount, out double high, out double low, out double middle)
        {
            try
            {
                string currentTime = DateTime.Now.ToString("HH:mm:ss");
                int interval = 0;
                double dHigh = 0;
                double dLow = 0;
                string date = DateTime.Now.ToString("M-d-yyyy");
                string Time = "";
                if (FileData.ContainsKey(TradingSymbol))
                {
                    string[] csvRows = FileData[TradingSymbol];
                    for (int i = csvRows.Length - 1; i >= 0; i--)
                    {
                        string row = csvRows[i];
                        if (row.Contains("Date"))
                        {
                            continue;
                        }

                        if (interval == barCount)
                        {
                            break;
                        }
                        string[] columnValues = row.Split(',');
                        string d = columnValues[0];
                        string time = columnValues[1];
                        DateTime datetime = DateTime.Parse(time, System.Globalization.CultureInfo.CurrentCulture);
                        Time = datetime.ToString("HH:mm:ss");
                        if ((TimeSpan.Parse(currentTime) < TimeSpan.Parse(Time)) && (date == d))
                        {
                            continue;
                        }
                        if (dHigh == 0 && dLow == 0)
                        {
                            dHigh = Convert.ToDouble(columnValues[3]);
                            dLow = Convert.ToDouble(columnValues[4]);
                        }

                        if (dHigh < Convert.ToDouble(columnValues[3]))
                        {
                            dHigh = Convert.ToDouble(columnValues[3]);
                        }

                        if (dLow > Convert.ToDouble(columnValues[4]))
                        {
                            dLow = Convert.ToDouble(columnValues[4]);
                        }
                        interval++;
                    }
                }
                high = dHigh;
                low = dLow;
                middle = (dHigh + dLow) / 2;
            }
            catch (Exception e)
            {
                high = 0;
                low = 0;
                middle = 0;
                WriteUniquelogs(TradingSymbol + " " + userID, "getHighLowValues : Exception Error Message =  " + e.Message, MessageType.Exception);
            }

        }

        public void getHighLowValuesFromDB(string TradingSymbol, int barCount, out double high, out double low, out double middle)
        {
            try
            {
                double dHigh = 0;
                double dLow = 0;
                AlgoOMS.GetHighLow(userID, TradingSymbol, barCount, out dHigh, out dLow);
                //WriteUniquelogs(TradingSymbol + " " + userID, "getHighLowValuesFromDB : before round up dHigh " + dHigh + " dLow " + dLow, MessageType.Informational);
                if (dHigh % 0.5 != 0)
                {
                    high = AlgoOMS.RoundOff(userID, TradingSymbol, Convert.ToDecimal(dHigh), true);
                }
                else
                {
                    high = dHigh;
                }
                if (dLow % 0.5 != 0)
                {
                    low = AlgoOMS.RoundOff(userID, TradingSymbol, Convert.ToDecimal(dLow), false);
                }
                else
                {
                    low = dLow;
                }
                if (dHigh != 0 && dLow != 0)
                {
                    middle = (dHigh + dLow) / 2;
                }
                else
                {
                    WriteUniquelogs(TradingSymbol + " " + userID, "getHighLowValuesFromDB : table not exist " + TradingSymbol, MessageType.Informational);
                    middle = 0;
                }

            }
            catch (Exception e)
            {
                high = 0;
                low = 0;
                middle = 0;
                WriteUniquelogs(TradingSymbol + " " + userID, "getHighLowValuesFRomdb : Exception Error Message =  " + e.Message, MessageType.Exception);
            }
        }
        void RunSystemLoop(string TickCurrentTime)
        {
            double dHigh = 0;
            double dLow = 0;
            double dMiddle = 0;
            DateTime dateTime1 = DateTime.Now;
            foreach (var symbolInfo in ListOfTradingSymbolsInfo) //loop for the trading symbol from ini file
            {               
                int Quantity = 0;
                string TradingSymbol = symbolInfo.getSymbol();
                int BarCount = symbolInfo.getBarCount();
                double ProfitPercent = symbolInfo.getProfitPercent();
                double StopLoss = symbolInfo.getStopLoss();
                string Exchange = symbolInfo.getExchange();
                double highLowPercent = symbolInfo.getHighLowPercent();
                string symbol = TradingSymbol;
                double riskPercent = m_RiskPercentForStock;
                if (Exchange == Constants.EXCHANGE_NFO)
                {
                    TradingSymbol = AlgoOMS.GetFutureSymbolName(userID, TradingSymbol);
                    riskPercent = m_RiskPercentForFuture;
                }
                Trace.WriteLine("&&&&Tuesday TradingSymbol "+ TradingSymbol +" " + DateTime.Now + "\n");               
                //sanika::16-dec-2020::Added to check risk percent on close and last price
                if (m_IsRiskManagementCheckAllow == true && (!AlgoOMS.RiskManagement(userID,TradingSymbol,Exchange,riskPercent)))
                {
                    WriteUniquelogs(TradingSymbol + " " + userID, "Automation : ignore for this symbol because RiskManagement returns false", MessageType.Informational);
                    AlgoOMS.CloseOrder(userID, TradingSymbol, Exchange);
                    continue;
                }              
                double highPercent = 0,lowPercent = 0;
                
                //condition for check ignore value for tradingsymbol
                if (m_OrderInfo.GetIgnoreValue(TradingSymbol) == 1)
                {
                    WriteUniquelogs(TradingSymbol + " " + userID, "Automation : ignore for this symbol", MessageType.Informational);
                    continue;
                }                

                //get quantity               
                Quantity = m_OrderInfo.GetQuantity(TradingSymbol, Exchange);

                //reset the values
                dHigh = 0;
                dLow = 0;
                dMiddle = 0;
               
                //sanika::4-dec-2020::Added condition for barcount to get high and low values
                if (BarCount != 0)
                {
                    //condition for way to read data i.e. database or file
                    getHighLowValuesFromDB(TradingSymbol, BarCount, out dHigh, out dLow, out dMiddle);
                }
                else if(BarCount == 0)
                {
                    //condition for way to read data i.e. database or file
                    GetTodaysHighLow(TradingSymbol, out dHigh, out dLow, out dMiddle);
                }
                

                if (dHigh == 0 || dLow == 0 || dMiddle == 0) 
                {
                    WriteUniquelogs(TradingSymbol + " " + userID, "Automation : Not able to fetch latest high and low also add in ignore list ", MessageType.Informational);
                    //m_OrderInfo.AddIgnoreValue(TradingSymbol, 1);
                    continue;
                }

                //get open price according to order id
                double openPrice = 0;
                string lOrderId = "";                 
                lOrderId = m_OrderInfo.GetOpenOrderId(TradingSymbol, Constants.TRANSACTION_TYPE_BUY);
                if (lOrderId != "")
                {
                    openPrice = AlgoOMS.GetOpenPostionPricebyOrderID(userID, TradingSymbol, Exchange, lOrderId);
                }

                if (openPrice == 0)
                {
                    lOrderId = m_OrderInfo.GetOpenOrderId(TradingSymbol, Constants.TRANSACTION_TYPE_SELL);
                    if (lOrderId != "")
                    {
                        openPrice = AlgoOMS.GetOpenPostionPricebyOrderID(userID, TradingSymbol, Exchange, lOrderId);
                    }
                }

                //get open price according to order id
                string status = "";
                string OrderId = "";
                OrderId = m_OrderInfo.GetOpenOrderId(TradingSymbol, Constants.TRANSACTION_TYPE_BUY);
                if (OrderId != "")
                {
                    status = AlgoOMS.GetOrderStatusByOrderID(userID, TradingSymbol, Exchange, OrderId);
                }

                if (status == "ordernotupdated" || status == "")
                {
                    OrderId = m_OrderInfo.GetOpenOrderId(TradingSymbol, Constants.TRANSACTION_TYPE_SELL);
                    if (OrderId != "")
                    {
                        status = AlgoOMS.GetOrderStatusByOrderID(userID, TradingSymbol, Exchange, OrderId);
                    }
                }

                //get lastprice
                double lastPrice = AlgoOMS.GetLastPrice(userID, TradingSymbol);
                
                //time condition cancel both pending order after TimeLimitToPlaceOrder
                if ((TimeSpan.Parse(TickCurrentTime) > TimeSpan.Parse(TimeLimitToPlaceOrder)))
                {
                    WriteUniquelogs(TradingSymbol + " " + userID, "Time condition true for cancel pending orders", MessageType.Informational);
                    if(CancelPendingOrders(TradingSymbol, Exchange))
                    {
                        continue;
                    }
                }
               

                //every time check for market order
                if (openPrice == 0 && m_isAllowSLMOrder == false && (TimeSpan.Parse(TickCurrentTime) >= TimeSpan.Parse(StrategyStartTime)))
                {                
                    if ((TimeSpan.Parse(TickCurrentTime) <= TimeSpan.Parse(TimeLimitToPlaceOrder)))
                    {
                        Trace.WriteLine("&&&&Tuesday Start placing order " + TradingSymbol + " " + DateTime.Now + "\n");
                        if (IsPercentInRange(TradingSymbol,TickCurrentTime, highLowPercent, Constants.TRANSACTION_TYPE_BUY))
                        {
                            WriteUniquelogs(TradingSymbol + " " + userID, "dHigh " + dHigh + " dLow " + dLow + " LTP " + lastPrice, MessageType.Informational);
                            WriteUniquelogs(TradingSymbol + " " + userID, "***** placing market order for buy", MessageType.Informational);
                            placeEntryMarketOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_BUY, Quantity, Convert.ToDecimal(dHigh), Convert.ToDecimal(StopLoss), Convert.ToDecimal(ProfitPercent));
                        }
                        else
                        {
                            WriteUniquelogs(TradingSymbol + " " + userID, "High percent condition not match", MessageType.Informational);
                        }
                        if (IsPercentInRange(TradingSymbol,TickCurrentTime, highLowPercent, Constants.TRANSACTION_TYPE_SELL) && AlgoOMS.GetOpenPostionPricebyOrderID(userID, TradingSymbol, Exchange, m_OrderInfo.GetOpenOrderId(TradingSymbol, Constants.TRANSACTION_TYPE_BUY)) == 0)
                        {
                            WriteUniquelogs(TradingSymbol + " " + userID, "dHigh " + dHigh + " dLow " + dLow + " LTP " + lastPrice, MessageType.Informational);
                            WriteUniquelogs(TradingSymbol + " " + userID, "***** placing market order for sell ", MessageType.Informational);
                            placeEntryMarketOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_SELL, Quantity, Convert.ToDecimal(dLow), Convert.ToDecimal(StopLoss), Convert.ToDecimal(ProfitPercent));
                        }
                        else
                        {
                            WriteUniquelogs(TradingSymbol + " " + userID, "Low percent condition not match or buy order placed already", MessageType.Informational);
                        }
                        Trace.WriteLine("&&&&Tuesday Ends placing order " + TradingSymbol + " " + DateTime.Now + "\n");
                    }                    
                }

                        //time condition according to interval i.e. 1min
                if ((TimeSpan.Parse(TickCurrentTime) >= TimeSpan.Parse(FirstTickEnd)) && (TimeSpan.Parse(TickCurrentTime) < TimeSpan.Parse(End)))
                {
                    WriteUniquelogs(TradingSymbol + " " + userID, "Automation : dHigh " + dHigh + " dLow " + dLow + " dMiddle " + dMiddle + "ltp "+ lastPrice+" openprice "+ openPrice +" tickTime "+ TickCurrentTime, MessageType.Informational);

                    if ((TimeSpan.Parse(TickCurrentTime) <= TimeSpan.Parse(TimeLimitToPlaceOrder)) && m_isAllowSLMOrder == true)
                    {                        
                        if (openPrice == 0 && (AlgoOMS.IsPendingStopOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_BUY) == false))
                        {
                            //sanika::10-12-2020::Added for issue of double order
                            if (AlgoOMS.GetStopPrice(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_BUY) == 0)
                            {
                                if (IsPercentInRange(TradingSymbol,TickCurrentTime, highLowPercent, Constants.TRANSACTION_TYPE_BUY) && IsPriceDiffInRange(TradingSymbol, lastPrice, dHigh))
                                {
                                    //sanika::3-Feb-2021::Added for duplicate order issue
                                    if (AlgoOMS.GetOrderIdFromMasterList(userID, TradingSymbol) == "NA" && status != "UPDATE")                                        {
                                           
                                        //place buy stop order
                                        WriteUniquelogs(TradingSymbol + " " + userID, "***** placing stop order for buy with price " + dHigh, MessageType.Informational);
                                        placeEntryStopOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_BUY, Quantity, Convert.ToDecimal(dHigh), Convert.ToDecimal(StopLoss), Convert.ToDecimal(ProfitPercent));
                                    }
                                    else
                                    {
                                        WriteUniquelogs(TradingSymbol + " " + userID, "Already buy order placed or status is update", MessageType.Informational);
                                    }
                                }
                                else
                                {
                                    WriteUniquelogs(TradingSymbol + " " + userID, "High percent condition not match or IsPriceDiffInRange returns false", MessageType.Informational);
                                }
                            }
                            else
                            {
                                WriteUniquelogs(TradingSymbol + " " + userID, "@@Found stop is not equal to 0", MessageType.Informational);
                            }
                        }
                        if (openPrice == 0 && (AlgoOMS.IsPendingStopOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_SELL) == false))
                            {
                                //sanika::10-12-2020::Added for issue of double order issue
                                if (AlgoOMS.GetStopPrice(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_SELL) == 0)
                                {
                                    if (IsPercentInRange(TradingSymbol,TickCurrentTime, highLowPercent, Constants.TRANSACTION_TYPE_SELL) && IsPriceDiffInRange(TradingSymbol, lastPrice, dLow))
                                    {
                                        //sanika::3-Feb-2021::Added for duplicate order
                                        if (AlgoOMS.GetOrderIdFromMasterList(userID, TradingSymbol) == "NA" && status != "UPDATE")
                                        {
                                            isFirstOrderPlaced = true;
                                            //place sell stop order
                                            WriteUniquelogs(TradingSymbol + " " + userID, "***** placing stop order for sell with price " + dLow, MessageType.Informational);
                                            placeEntryStopOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_SELL, Quantity, Convert.ToDecimal(dLow), Convert.ToDecimal(StopLoss), Convert.ToDecimal(ProfitPercent));
                                        }
                                        else
                                        {
                                            WriteUniquelogs(TradingSymbol + " " + userID, "Already sell order placed or status is update", MessageType.Informational);
                                        }
                                    }
                                    else
                                    {
                                        WriteUniquelogs(TradingSymbol + " " + userID, "Low percent condition not match or IsPriceDiffInRange returns false", MessageType.Informational);
                                    }
                                }
                                else
                                {
                                    WriteUniquelogs(TradingSymbol + " " + userID, "@@Found stop is not equal to 0", MessageType.Informational);
                                }
                            }
                        
                        

                        //condition for modifying order after 1min  (means both orders are pending)
                        //sanika::22-oct-2020::seperate out direction condition for modifying
                        if (openPrice == 0 && (AlgoOMS.IsPendingStopOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_BUY) == true))
                        {
                            ModifyOrderAfterOneMin(TradingSymbol, Exchange, dHigh, dLow, openPrice, lastPrice, Quantity, StopLoss, ProfitPercent);
                            if (!IsPlacedOrderPriceDiffInRange(TradingSymbol, lastPrice, Constants.TRANSACTION_TYPE_BUY, Exchange))
                            {
                                AlgoOMS.RemoveOrderIdFromMasterList(userID, TradingSymbol);
                                m_OrderInfo.SubtractStopOrderCounter(TradingSymbol);
                                AlgoOMS.CancelPendingStopOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_BUY);
                            }
                        }
                        if(openPrice == 0 && (AlgoOMS.IsPendingStopOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_SELL) == true))
                        {
                            ModifyOrderAfterOneMin(TradingSymbol, Exchange, dHigh, dLow, openPrice, lastPrice, Quantity, StopLoss, ProfitPercent);
                            if (!IsPlacedOrderPriceDiffInRange(TradingSymbol, lastPrice, Constants.TRANSACTION_TYPE_SELL, Exchange))
                            {
                                AlgoOMS.RemoveOrderIdFromMasterList(userID, TradingSymbol);
                                m_OrderInfo.SubtractStopOrderCounter(TradingSymbol);
                                AlgoOMS.CancelPendingStopOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_SELL);
                            }
                        }
                    }                    
                }
                else if (openPrice != 0)
                {                   

                    //place opposite stoploss order to executed order if not found                   
                    PlaceStopOrderIfNotPresent(TradingSymbol, Exchange, openPrice, lastPrice, StopLoss, Quantity);

                    //close order - BUY & SELL
                    CloseOrder(TradingSymbol, Exchange, lastPrice, dMiddle, openPrice);

                    //change stoploss(trigger price) to stoploss order (profittrail)
                    if (CheckForPendingStopOrder(TradingSymbol, Exchange) && ProfitTrail > 0)
                    {
                        ChangeStopLossPrice(TradingSymbol, Exchange, lastPrice, openPrice, StopLoss);
                    }
                    //sanika::08-Mar-2021::changed profitpercent condition to profittrail
                    else if (ProfitTrail == 0) //place profit order //sanika::4-dec-2020::Commented as for placing profit order
                    {
                        PlaceProfitOrder(TradingSymbol, Exchange, openPrice, lastPrice, ProfitPercent, Quantity);
                    }

                    CheckIndividualProfitLoss(TradingSymbol,Exchange,StopLoss);

                    //sanika::2-Mar-2021::UnCommented because clearing storage after closing position
                    if (!CheckForOpenPosition(TradingSymbol, Exchange))
                    {
                        continue;
                    }


                }                
                Thread.Sleep(10);
                Trace.WriteLine("&&&&Tuesday TradingSymbol " + TradingSymbol + " " + DateTime.Now + "\n");
            }
            DateTime dateTime2 = DateTime.Now;
            double sec = dateTime2.Subtract(dateTime1).TotalMilliseconds;
            Trace.WriteLine("&&&&Tuesday total milliseconds " + sec + "\n");
            if (isFirstOrderPlaced)
                logger.LogMessage("Total milliseconds for placing all entry orders " + sec.ToString(), MessageType.Informational);
        }

        public bool CancelPendingOrders(string TradingSymbol,string Exchange)
        {
            bool isOrderCancelled = false;
            try
            {                
                //sanika::21-oct-2020::Added to cancel order 
                if (AlgoOMS.IsPendingStopOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_BUY) == true && AlgoOMS.GetOpenPostionPricebyOrderID(userID, TradingSymbol, Exchange, m_OrderInfo.GetOpenOrderId(TradingSymbol, Constants.TRANSACTION_TYPE_SELL)) == 0)
                {
                    WriteUniquelogs(TradingSymbol + " " + userID, "-----CancelPendingOrders : Going to cancel BUY pending orders", MessageType.Informational);
                    AlgoOMS.CancelPendingStopOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_BUY);
                    isOrderCancelled = true;
                }
                else if (AlgoOMS.IsPendingStopOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_SELL) == true && AlgoOMS.GetOpenPostionPricebyOrderID(userID, TradingSymbol, Exchange, m_OrderInfo.GetOpenOrderId(TradingSymbol, Constants.TRANSACTION_TYPE_BUY)) == 0)
                {
                    WriteUniquelogs(TradingSymbol + " " + userID, "-----CancelPendingOrders : Going to cancel SELL pending orders", MessageType.Informational);
                    AlgoOMS.CancelPendingStopOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_SELL);
                    isOrderCancelled = true;
                }                   
                else
                {
                    //WriteUniquelogs(TradingSymbol + " " + userID, "-----CancelPendingOrders : Both orders are not found as pending", MessageType.Informational);
                }

            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + userID, "-----CancelPendingOrders : Exception Error Message = "+e.Message, MessageType.Exception);
            }
            return isOrderCancelled;
        }

        public bool CheckForPendingStopOrder(string TradingSymbol,string Exchange)
        {
            int buyOrsell = m_OrderInfo.GetBuyOrSell(TradingSymbol);
            double buyStopOrderPrice = AlgoOMS.GetStopPricebyOrderID(userID, TradingSymbol, Exchange, m_OrderInfo.GetOpenOrderId(TradingSymbol, Constants.TRANSACTION_TYPE_BUY));
            double sellStopOrderPrice = AlgoOMS.GetStopPricebyOrderID(userID, TradingSymbol, Exchange, m_OrderInfo.GetOpenOrderId(TradingSymbol, Constants.TRANSACTION_TYPE_SELL));
            double sellOpenPrice = AlgoOMS.GetOpenPostionPricebyOrderID(userID, TradingSymbol, Exchange, m_OrderInfo.GetOpenOrderId(TradingSymbol, Constants.TRANSACTION_TYPE_SELL));
            double buyOpenPrice = AlgoOMS.GetOpenPostionPricebyOrderID(userID, TradingSymbol, Exchange, m_OrderInfo.GetOpenOrderId(TradingSymbol, Constants.TRANSACTION_TYPE_BUY));
            WriteUniquelogs(TradingSymbol + " " + userID, "CheckForPendingStopOrder : buyOrsell " + buyOrsell + " buyStopOrderPrice "+ buyStopOrderPrice + " sellStopOrderPrice " + sellStopOrderPrice + " sellOpenPrice "+ sellOpenPrice + " buyOpenPrice "+ buyOpenPrice, MessageType.Informational);
            if (buyOrsell > 0 && buyStopOrderPrice != 0)
            {
                if (sellOpenPrice == 0)
                {
                    WriteUniquelogs(TradingSymbol + " " + userID, "CheckForPendingStopOrder : Returning false sellOpenPrice = "+ sellOpenPrice +" found pending order", MessageType.Informational);
                    return false;
                }
            }
            else if (buyOrsell < 0 && sellStopOrderPrice != 0)
            {
                if (buyOpenPrice == 0)
                {
                    WriteUniquelogs(TradingSymbol + " " + userID, "CheckForPendingStopOrder : Returning false buyOpenPrice = " + buyOpenPrice +" found pending order", MessageType.Informational);
                    return false;
                }
            }
            WriteUniquelogs(TradingSymbol + " " + userID, "CheckForPendingStopOrder : Returning true ", MessageType.Informational);
            return true;
        }

        public bool CheckForOpenPosition(string TradingSymbol,string Exchange)
        {
            //checking position open or not                     
            if (!AlgoOMS.IsPositionOpen(userID, TradingSymbol, Exchange))
            {
                Thread.Sleep(2000);               
                //wait for updating order from server to local array
                if (!AlgoOMS.IsPositionOpen(userID, TradingSymbol, Exchange))
                {
                    if (AlgoOMS.IsPendingStopOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_BUY) && (AlgoOMS.GetOpenPostionPricebyOrderID(userID, TradingSymbol, Exchange, m_OrderInfo.GetOpenOrderId(TradingSymbol, Constants.TRANSACTION_TYPE_SELL)) == 0))
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "CheckForOpenPosition : buy pending order and sell is open", MessageType.Informational);
                    }
                    else if (AlgoOMS.IsPendingStopOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_SELL) && (AlgoOMS.GetOpenPostionPricebyOrderID(userID, TradingSymbol, Exchange, m_OrderInfo.GetOpenOrderId(TradingSymbol, Constants.TRANSACTION_TYPE_BUY)) == 0))
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "CheckForOpenPosition : sell pending order and buy is open", MessageType.Informational);
                    }
                    else
                    {
                        if (!AlgoOMS.IsPositionOpen(userID, TradingSymbol, Exchange) && AlgoOMS.GetOrderStatusByOrderID(userID,TradingSymbol,Exchange,m_OrderInfo.GetStopLossOrderId(TradingSymbol)) != Constants.ORDER_STATUS_PENDING)
                        {
                            WriteUniquelogs(TradingSymbol + " " + userID, "CheckForOpenPosition : Reset order id", MessageType.Informational);
                            m_OrderInfo.ResetOrderId(TradingSymbol);
                            //sanika::3-nov-2020::remove last order entry from master dictionary 
                            AlgoOMS.RemoveSymbolLastOrder(userID, TradingSymbol, "");
                            AlgoOMS.RemoveOrderIdFromMasterList(userID, TradingSymbol);
                            WriteUniquelogs(TradingSymbol + " " + userID, "CheckForOpenPosition : Remove last entry of order with transaction type from master list ", MessageType.Informational);
                        }
                    }
                    return false;
                }
                else
                {
                    WriteUniquelogs(TradingSymbol + " " + userID, "CheckForOpenPosition : Open position found", MessageType.Informational);
                }
            }
            return true;
        }


        public void ExecuteSyncLogic()
        {
            bool isStopThread = false;
            loadINIValues();
            CheckTradingSymbolPresentOrNot();

            while (!m_IsThreadStop) //Sanika::4-Feb-2021::Added to stop automation after click on stop and close all orders
            {
                Thread.Sleep(1000);
                //if (isFirstOrderPlaced)
                //{
                //    Thread.Sleep(1000);
                //    isFirstOrderPlaced = false;
                //}
                try
                {
                    string TickCurrentTime = AlgoOMS.getTickCurrentTime();
                    //condition for TickCurrentTime 
                    if (TickCurrentTime == "")
                    {
                        logger.LogMessage("TickCurrentTime not recieved", MessageType.Error);
                        continue;
                    }                    
                    //condition for calculateProfit 
                    if (CalculateProfitOrLoss())
                    {
                        logger.LogMessage("Calculate profit hit!!", MessageType.Error);
                        isStopThread = true;
                        StopThread();
                        break;
                    }
                    //condition for stop automation
                    if (TimeSpan.Parse(TickCurrentTime) >= TimeSpan.Parse(EndTime))
                    {
                        logger.LogMessage("ExecuteSyncLogic :Stopped because of endtime is " + EndTime, MessageType.Informational);
                        isStopThread = true;
                        form_Auto.isChanbreakStopped = true;
                        form_Auto.enableButton();
                        StopThread();                       
                        break;
                    }
                    //condition for exit all orders after 3:10
                    if (TimeSpan.Parse(TickCurrentTime) >= TimeSpan.Parse(TimeForExitAllOpenPosition))
                    {
                        if(m_isCloseAllOrder == false)
                        {
                            logger.LogMessage("ExecuteSyncLogic : condition true for close all position", MessageType.Informational);
                            CloseAllOrder();
                            m_isCloseAllOrder = true;
                            //sanika::28-sep-2020::call function to delete bin file
                            DeleteBinFile();
                        }                        
                    }
                    else
                    {
                        RunSystemLoop(TickCurrentTime);
                        changeTime(TickCurrentTime);
                    }
                }
                catch (Exception e)
                {
                    logger.LogMessage("ExecuteSyncLogic :Exception Error Message =  " + e.Message, MessageType.Exception);
                }
                finally
                {
                    if (isStopThread) //condition for stop automation if calculate profit condition is true
                    {
                        form_Auto.isChanbreakStopped = true;
                        form_Auto.enableButton();
                    }
                }
            }

            if(m_IsThreadStop)
            {
                logger.LogMessage("ExecuteSyncLogic : Stopped thread because m_IsThreadStop is true", MessageType.Informational);
            }
        }

        //modifying order after 1min  (means both orders are pending)
        public void ModifyOrderAfterOneMin(string TradingSymbol, string Exchange, double dHigh, double dLow, double openPrice, double lastPrice,int Quantity,double stoploss,double profitpercent)
        {
            if (AlgoOMS.IsPendingStopOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_BUY) == true)
            {
                double stopPrice = AlgoOMS.GetStopPrice(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_BUY);
                if (stopPrice > dHigh)
                {
                    if (m_OrderInfo.GetModifyBuyOrderCounter(TradingSymbol) < 10)
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "##### ModifyOrderAfterOneMin :openPrice   " + openPrice + " and lastPrice " + lastPrice, MessageType.Informational);
                        WriteUniquelogs(TradingSymbol + " " + userID, "stopPrice   " + stopPrice + " and dHigh " + dHigh, MessageType.Informational);
                        if(AlgoOMS.ModifyStopOrder(userID, TradingSymbol, Constants.TRANSACTION_TYPE_BUY, Convert.ToDecimal(dHigh)))
                        {
                            WriteUniquelogs(TradingSymbol + " " + userID, "##### ModifyOrderAfterOneMin :Modified buy order\n", MessageType.Informational);
                            m_OrderInfo.ModifyBuyOrderCounter(TradingSymbol);
                        }
                        ////loadOpenPositions();
                    }
                    else
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "##### ModifyOrderAfterOneMin :Condition true for cancelling previous order and placing new stop orders\n", MessageType.Informational);
                        CancelPreviousOrderAndPlaceNew(userID, TradingSymbol, Exchange, dHigh, dLow, Quantity,stoploss,profitpercent);                        
                    }

                }
                else
                {
                    WriteUniquelogs(TradingSymbol + " " + userID, "##### ModifyOrderAfterOneMin :stop price and high are same \n", MessageType.Informational);
                }
            }
            else
            {
                //WriteUniquelogs(TradingSymbol + " " + userID, "##### ModifyOrderAfterOneMin :pending order not found ", MessageType.Informational);
            }

            //modify open  sell order
            if (AlgoOMS.IsPendingStopOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_SELL) == true)
            {
                double stopPrice = AlgoOMS.GetStopPrice(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_SELL);
                if (stopPrice < dLow)
                {
                    if (m_OrderInfo.GetModifySellOrderCounter(TradingSymbol) < 10)                    
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "##### ModifyOrderAfterOneMin :openPrice   " + openPrice + " and lastPrice " + lastPrice, MessageType.Informational);
                        WriteUniquelogs(TradingSymbol + " " + userID, "ModifyOrderAfterOneMin :stopPrice   " + stopPrice + " and dLow " + dLow, MessageType.Informational);
                        if(AlgoOMS.ModifyStopOrder(userID, TradingSymbol, Constants.TRANSACTION_TYPE_SELL, Convert.ToDecimal(dLow)))
                        {
                            WriteUniquelogs(TradingSymbol + " " + userID, "##### ModifyOrderAfterOneMin :Modified sell order\n", MessageType.Informational);
                            m_OrderInfo.ModifySellOrderCounter(TradingSymbol);
                        }
                       // //loadOpenPositions();
                    }
                    else
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "##### ModifyOrderAfterOneMin :Condition true for cancelling previous order and placing new stop orders\n", MessageType.Informational);
                        CancelPreviousOrderAndPlaceNew(userID, TradingSymbol, Exchange, dHigh, dLow, Quantity, stoploss, profitpercent);
                    }
                }
                else
                {
                    WriteUniquelogs(TradingSymbol + " " + userID, "##### ModifyOrderAfterOneMin :stop price and low are same\n ", MessageType.Informational);
                }
            }
            else
            {
                //WriteUniquelogs(TradingSymbol + " " + userID, "##### ModifyOrderAfterOneMin :pending order not found ", MessageType.Informational);
            }

        }

        public void CancelPreviousOrderAndPlaceNew(string userID, string TradingSymbol, string Exchange,double dHigh, double dLow,int Quantity,double stoploss,double profitpercent)
        {
            try
            {                
                if(AlgoOMS.CancelPendingStopOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_BUY))
                {
                    WriteUniquelogs(TradingSymbol + " " + userID, "##### CancelPreviousOrderAndPlaceNew : Cancelled previous buy orders", MessageType.Informational);
                    m_OrderInfo.SubtractStopOrderCounter(TradingSymbol);
                    Thread.Sleep(200);
                    placeEntryStopOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_BUY, Quantity, Convert.ToDecimal(dHigh), Convert.ToDecimal(stoploss), Convert.ToDecimal(profitpercent));
                    m_OrderInfo.ResetModifyBUYCounter(TradingSymbol);
                    WriteUniquelogs(TradingSymbol + " " + userID, "##### CancelPreviousOrderAndPlaceNew : Reset Modify buy counter", MessageType.Informational);
                    WriteUniquelogs(TradingSymbol + " " + userID, "##### CancelPreviousOrderAndPlaceNew : Placing buy stop order with price " + dHigh+"\n", MessageType.Informational);
                }
                if (AlgoOMS.CancelPendingStopOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_SELL))
                {
                    WriteUniquelogs(TradingSymbol + " " + userID, "##### CancelPreviousOrderAndPlaceNew : Cancelled previous sell orders", MessageType.Informational);
                    m_OrderInfo.SubtractStopOrderCounter(TradingSymbol);
                    Thread.Sleep(200);
                    placeEntryStopOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_SELL, Quantity, Convert.ToDecimal(dLow), Convert.ToDecimal(stoploss), Convert.ToDecimal(profitpercent));
                    m_OrderInfo.ResetModifySELLCounter(TradingSymbol);
                    WriteUniquelogs(TradingSymbol + " " + userID, "##### CancelPreviousOrderAndPlaceNew : Reset Modify sell counter", MessageType.Informational);
                    WriteUniquelogs(TradingSymbol + " " + userID, "##### CancelPreviousOrderAndPlaceNew : Placing sell stop order with price " + dLow+"\n", MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                 WriteUniquelogs(TradingSymbol + " " + userID, "Error CancelPreviousOrderAndPlaceNew "+e.Message, MessageType.Error);
            }
        }

        //place stop order 
        public void placeEntryStopOrder(string userID, string TradingSymbol, string Exchange, string TransactionType, int Quantity, decimal Price, decimal StopLoss, decimal ProfitPercent)
        {            
            double lastPrice = AlgoOMS.GetLastPrice(userID, TradingSymbol);
            string orderId = "NA";
            int counter = m_OrderInfo.GetStopOrderCounter(TradingSymbol);
            if (counter < IndividualOrderCount)
            {
                if (TransactionType == Constants.TRANSACTION_TYPE_SELL)
                {
                    //place stop order
                    WriteUniquelogs(TradingSymbol + " " + userID, "placeEntryStopOrder : placed sell Stoploss order", MessageType.Informational);
                    orderId = AlgoOMS.PlaceStopOrder(userID, Exchange, TradingSymbol, TransactionType, Quantity, 0, Price);
                    if (orderId == "ErrorMessageForStopOrder")
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "placeEntryStopOrder : Exception error occure for trigger price. Placing Market order", MessageType.Informational);
                        orderId = AlgoOMS.PlaceMarketOrder(userID, Exchange, TradingSymbol, TransactionType, Quantity);
                    }                   
                }
                else if (TransactionType == Constants.TRANSACTION_TYPE_BUY)
                {                   
                    //place stop order
                    WriteUniquelogs(TradingSymbol + " " + userID, "placeEntryStopOrder : placed buy Stoploss order", MessageType.Informational);
                    orderId = AlgoOMS.PlaceStopOrder(userID, Exchange, TradingSymbol, TransactionType, Quantity, 0, Price);
                    if (orderId == "ErrorMessageForStopOrder")
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "placeEntryStopOrder : Exception error occure for trigger price. Placing Market order", MessageType.Informational);
                        orderId = AlgoOMS.PlaceMarketOrder(userID, Exchange, TradingSymbol, TransactionType, Quantity);
                    }
                }

                if (orderId != "NA")
                {
                    OrderCount++;
                    m_OrderInfo.StopOrderCounter(TradingSymbol);//increament individual counter
                    m_OrderInfo.AddOrUpdateOrderInfo(TradingSymbol, TransactionType, orderId);//added order id into storage
                    AlgoOMS.AddOrderIdInMasterList(userID, TradingSymbol, orderId);
                    WriteUniquelogs(TradingSymbol + " " + userID, "placeEntryStopOrder : placed successfully with orderId = " + orderId+"\n", MessageType.Informational);
                }
                else
                {
                    WriteUniquelogs(TradingSymbol + " " + userID, "placeEntryStopOrder : Not placed\n", MessageType.Informational);
                }
            }
            else
            {
                WriteUniquelogs(TradingSymbol + " " + userID, "placeEntryStopOrder : individualCount condition false counter = " + counter+"\n", MessageType.Informational);
            }
        }

        
        //place market order 
        public void placeEntryMarketOrder(string userID, string TradingSymbol, string Exchange, string TransactionType, int Quantity, decimal Price, decimal StopLoss, decimal ProfitPercent)
        {
            double lastPrice = AlgoOMS.GetLastPrice(userID, TradingSymbol);
            string orderId = "NA";
            int counter = m_OrderInfo.GetStopOrderCounter(TradingSymbol);
            if (counter < IndividualOrderCount)
            {
                if (TransactionType == Constants.TRANSACTION_TYPE_SELL)
                {                                        
                    //market order condition sell
                    if (lastPrice < Convert.ToDouble(Price))
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "placeEntryMarketOrder : Placing sell Market order m_isAllowSLMOrder " + m_isAllowSLMOrder + " lastPrice "+ lastPrice + " Price "+ Price, MessageType.Informational);
                        orderId = AlgoOMS.PlaceMarketOrder(userID, Exchange, TradingSymbol, TransactionType, Quantity);
                    }
                    else
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "placeEntryMarketOrder : Order not placed lastprice condition not match lastPrice = "+ lastPrice + " Price = "+ Price, MessageType.Informational);
                    }
                    
                }
                else if (TransactionType == Constants.TRANSACTION_TYPE_BUY)
                {
                    //market order condition buy
                    if (lastPrice > Convert.ToDouble(Price))
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "placeEntryMarketOrder : Placing BUY Market order m_isAllowSLMOrder = " + m_isAllowSLMOrder + " lastPrice " + lastPrice + " Price " + Price, MessageType.Informational);
                        orderId = AlgoOMS.PlaceMarketOrder(userID, Exchange, TradingSymbol, TransactionType, Quantity);
                    }  
                    else
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "placeEntryMarketOrder : Order not placed lastprice condition not match lastPrice = " + lastPrice + " Price = " + Price, MessageType.Informational);
                    }
                }

                if (orderId != "NA")
                {
                    OrderCount++;
                    m_OrderInfo.StopOrderCounter(TradingSymbol);//increament individual counter
                    m_OrderInfo.AddOrUpdateOrderInfo(TradingSymbol, TransactionType, orderId);//added order id into storage
                    WriteUniquelogs(TradingSymbol + " " + userID, "placeEntryMarketOrder : placed successfully with orderId = " + orderId + "\n", MessageType.Informational);
                }
                else
                {
                    WriteUniquelogs(TradingSymbol + " " + userID, "placeEntryMarketOrder : Not placed\n", MessageType.Informational);
                }
            }
            else
            {
                WriteUniquelogs(TradingSymbol + " " + userID, "placeEntryMarketOrder : individualCount condition false counter = " + counter + "\n", MessageType.Informational);
            }
        }


        //change stoploss(trigger price)
        public void ChangeStopLossPrice(string TradingSymbol, string Exchange, double lastPrice, double openPrice, double StopLoss)
        {
            if (m_OrderInfo.GetIsOrderPlaced(TradingSymbol))
            {
                //checking for open buy order
                if (lastPrice > openPrice && AlgoOMS.IsPendingStopOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_SELL) == true)//check for pending order
                {
                    double lastopenPrice = m_OrderInfo.GetOpenPriceForProfitTrail(TradingSymbol);
                    if(lastopenPrice !=0)
                    {
                        openPrice = lastopenPrice;
                    }
                    if (openPrice != 0)//check for open price
                    {
                        double percentCalculatedForOpenPrice = (openPrice * ProfitTrail) / 100;
                        double ProfitTrailValue = percentCalculatedForOpenPrice + openPrice;                        
                        if (lastPrice >= (ProfitTrailValue))
                        {
                            if (m_OrderInfo.GetProfitTrailSellOrderCounter(TradingSymbol) < 10)
                            {
                                WriteUniquelogs(TradingSymbol + " " + userID, " %%%%% ChangeStopLossPrice : ProfitTrailValue = " + ProfitTrailValue, MessageType.Informational);
                                decimal calculatedPrice = 0;
                                double percentStopLossValue = StopLoss * lastPrice / 100;
                                double calStopLossPrice = lastPrice - percentStopLossValue;
                                calculatedPrice = Convert.ToDecimal(AlgoOMS.RoundOff(userID, TradingSymbol, Convert.ToDecimal(calStopLossPrice), true));
                                WriteUniquelogs(TradingSymbol + " " + userID, "lastPrice " + lastPrice + " openprice " + openPrice, MessageType.Informational);
                                WriteUniquelogs(TradingSymbol + " " + userID, "New stoploss value " + percentStopLossValue + " calStopLossPrice " + calStopLossPrice, MessageType.Informational);
                                WriteUniquelogs(TradingSymbol + " " + userID, "Trigger Price = " + calculatedPrice, MessageType.Informational);
                                WriteUniquelogs(TradingSymbol + " " + userID, "%%%%% ChangeStopLossPrice : Change stop price - profittrail condition true for sell order \n", MessageType.Informational);
                                if (AlgoOMS.ModifyStopOrder(userID, TradingSymbol, Constants.TRANSACTION_TYPE_SELL, Convert.ToDecimal(calculatedPrice)))
                                {
                                    m_OrderInfo.AddOpenPriceForProfitTrail(TradingSymbol, lastPrice);
                                    m_OrderInfo.ProfitTrailSellOrderCounter(TradingSymbol);
                                }
                            }
                            else
                            {
                                WriteUniquelogs(TradingSymbol + " " + userID, "%%%%% ChangeStopLossPrice : Modify counter condition false \n", MessageType.Informational);
                            }
                        }
                        else
                        {
                            WriteUniquelogs(TradingSymbol + " " + userID, "%%%%% ChangeStopLossPrice :condition not match lastPrice " + lastPrice + " ProfitTrailValue " + ProfitTrailValue + "\n", MessageType.Informational);
                        }
                    }
                    else
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "%%%%% ChangeStopLossPrice :open price is 0\n", MessageType.Informational);
                    }
                }
                else
                {
                    WriteUniquelogs(TradingSymbol + " " + userID, "%%%%% ChangeStopLossPrice :condition not match lastPrice "+ lastPrice+ " openPrice "+ openPrice +" Or not found pending order\n", MessageType.Informational);
                }
                //checking for open sell order
                if (lastPrice < openPrice && AlgoOMS.IsPendingStopOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_BUY) == true)//check for pending order
                {
                    double lastopenPrice = m_OrderInfo.GetOpenPriceForProfitTrail(TradingSymbol);
                    if (lastopenPrice != 0)
                    {
                        openPrice = lastopenPrice;
                    }
                    if (openPrice != 0) //check for open price
                    {
                        double percentCalculatedForOpenPrice = (openPrice * ProfitTrail) / 100;
                        double ProfitTrailValue = openPrice - percentCalculatedForOpenPrice;
                        if (lastPrice <= (ProfitTrailValue))
                        {
                            if (m_OrderInfo.GetProfitTrailBuyOrderCounter(TradingSymbol) < 10)
                            {
                                WriteUniquelogs(TradingSymbol + " " + userID, " %%%%% ChangeStopLossPrice : ProfitTrailValue = " + ProfitTrailValue, MessageType.Informational);
                                decimal calculatedPrice = 0;
                                double percentStopLossValue = StopLoss * lastPrice / 100;
                                double calStopLossPrice = lastPrice + percentStopLossValue;
                                calculatedPrice = Convert.ToDecimal(AlgoOMS.RoundOff(userID, TradingSymbol, Convert.ToDecimal(calStopLossPrice), false));
                                WriteUniquelogs(TradingSymbol + " " + userID, "lastPrice " + lastPrice + " openprice " + openPrice, MessageType.Informational);
                                WriteUniquelogs(TradingSymbol + " " + userID, "New stoploss value " + percentStopLossValue + " calStopLossPrice " + calStopLossPrice, MessageType.Informational);
                                WriteUniquelogs(TradingSymbol + " " + userID, "Trigger Price = " + calculatedPrice, MessageType.Informational);
                                WriteUniquelogs(TradingSymbol + " " + userID, "%%%%% ChangeStopLossPrice : Change stop price - profittrail condition true for buy order\n", MessageType.Informational);
                                if (AlgoOMS.ModifyStopOrder(userID, TradingSymbol, Constants.TRANSACTION_TYPE_BUY, Convert.ToDecimal(calculatedPrice)))
                                {
                                    m_OrderInfo.AddOpenPriceForProfitTrail(TradingSymbol, lastPrice);
                                    m_OrderInfo.ProfitTrailBuyOrderCounter(TradingSymbol);
                                }
                            }
                            else
                            {
                                WriteUniquelogs(TradingSymbol + " " + userID, "%%%%% ChangeStopLossPrice : Modify counter condition false \n", MessageType.Informational);
                            }
                        }
                        else
                        {
                            WriteUniquelogs(TradingSymbol + " " + userID, "%%%%% ChangeStopLossPrice :condition not match lastPrice "+ lastPrice + " ProfitTrailValue "+ ProfitTrailValue+"\n", MessageType.Informational);
                        }
                    }
                    else
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "%%%%% ChangeStopLossPrice :open price is 0\n", MessageType.Informational);
                    }
                }
                else
                {
                    WriteUniquelogs(TradingSymbol + " " + userID, "%%%%% ChangeStopLossPrice :condition not match lastPrice " + lastPrice + " openPrice " + openPrice + " Or not found pending order\n", MessageType.Informational);
                }

            }            
        }

        //close order
        public void CloseOrder(string TradingSymbol, string Exchange, double lastPrice, double dMiddle, double openPrice)
        {
            //close order - BUY
            if (AlgoOMS.IsPendingStopOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_BUY) == true && openPrice != 0)
            {
                if (lastPrice >= dMiddle)
                {
                    WriteUniquelogs(TradingSymbol + " " + userID, "$$$$$ Automation :condition true for close buy order lastPrice " + lastPrice + " middle " + dMiddle, MessageType.Informational);
                    AlgoOMS.CloseOrder(userID, TradingSymbol, Exchange);
                    WriteUniquelogs(TradingSymbol + " " + userID, "\n", MessageType.Informational);
                }
            }

            //close order - SELL
            if (AlgoOMS.IsPendingStopOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_SELL) == true && openPrice != 0)
            {
                if (lastPrice <= dMiddle)
                {
                    WriteUniquelogs(TradingSymbol + " " + userID, "$$$$$ Automation :condition true for close sell order lastPrice " + lastPrice + " middle " + dMiddle, MessageType.Informational);
                    AlgoOMS.CloseOrder(userID, TradingSymbol, Exchange);
                    WriteUniquelogs(TradingSymbol + " " + userID, "\n", MessageType.Informational);
                }
            }
        }

        //modify order to place stoploss order to executed order
        public void ModifyOrderForPlacingStopLossOrder(string TradingSymbol, string Exchange, double openPrice, double lastPrice, double StopLoss)
        {
            //modify order after executing order
            if ((AlgoOMS.IsPendingStopOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_BUY) == true) && (AlgoOMS.IsPendingStopOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_SELL)) == false)
            {
                if (openPrice != 0 && (!m_OrderInfo.GetIsOrderPlaced(TradingSymbol)))//check for openprice and need to modify or not
                {
                    WriteUniquelogs(TradingSymbol + " " + userID, "@@@@@ ModifyOrderForPlacingStopLossOrder : openPrice " + openPrice, MessageType.Informational);
                    decimal calculatedPrice = 0;
                    double PercentValue = StopLoss * openPrice / 100;
                    double CalPrice = openPrice + PercentValue;
                    calculatedPrice = Convert.ToDecimal(AlgoOMS.RoundOff(userID, TradingSymbol, Convert.ToDecimal(CalPrice), true));
                    WriteUniquelogs(TradingSymbol + " " + userID, "PercentValue " + PercentValue+ " CalPrice = " + CalPrice+ " Trigger Price = " + calculatedPrice, MessageType.Informational);
                    if(AlgoOMS.ModifyStopOrder(userID, TradingSymbol, Constants.TRANSACTION_TYPE_BUY, Convert.ToDecimal(calculatedPrice)))
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "@@@@@ ModifyOrderForPlacingStopLossOrder : Modified buy order\n", MessageType.Informational);
                    }
                    else
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "@@@@@ ModifyOrderForPlacingStopLossOrder : Not able to modify buy order\n", MessageType.Informational);
                    }
                    m_OrderInfo.AddOrUpdateBuyOrSell(TradingSymbol, Constants.TRANSACTION_TYPE_SELL);
                    Thread.Sleep(2000);
                }
            }

            //modify order after executing order - SELL
            if ((AlgoOMS.IsPendingStopOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_BUY) == false) && (AlgoOMS.IsPendingStopOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_SELL)) == true)
            {
                if (openPrice != 0 && (!m_OrderInfo.GetIsOrderPlaced(TradingSymbol)))//check for openprice and need to modify or not
                {
                    WriteUniquelogs(TradingSymbol + " " + userID, "@@@@@ ModifyOrderForPlacingStopLossOrder :openPrice " + openPrice, MessageType.Informational);
                    decimal calculatedPrice = 0;
                    double PercentValue = StopLoss * openPrice / 100;
                    double CalPrice = openPrice - PercentValue;
                    calculatedPrice =  Convert.ToDecimal(AlgoOMS.RoundOff(userID, TradingSymbol, Convert.ToDecimal(CalPrice), false));
                    WriteUniquelogs(TradingSymbol + " " + userID, "PercentValue " + PercentValue + " CalPrice = " + CalPrice + " Trigger Price = " + calculatedPrice, MessageType.Informational);
                   
                    if(AlgoOMS.ModifyStopOrder(userID, TradingSymbol, Constants.TRANSACTION_TYPE_SELL, Convert.ToDecimal(calculatedPrice)))
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "@@@@@ ModifyOrderForPlacingStopLossOrder : Modified sell order\n", MessageType.Informational);
                    }
                    else
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "@@@@@ ModifyOrderForPlacingStopLossOrder : Not able to modify sell order\n", MessageType.Informational);
                    }
                    m_OrderInfo.AddOrUpdateBuyOrSell(TradingSymbol, Constants.TRANSACTION_TYPE_BUY);
                    Thread.Sleep(2000);
                }
            }            
        }


        //place stoploss order opposite to executed order if not present
        public void PlaceStopOrderIfNotPresent(string TradingSymbol, string Exchange, double openPrice, double lastPrice, double StopLoss, int Quantity)
        {
            //condition for stop order counter
            int counter = m_OrderInfo.GetStopOrderCounter(TradingSymbol);
            if (counter < IndividualOrderCount)
            {
                string lastTransactionType = AlgoOMS.GetSymbolLastOrder(userID, TradingSymbol);
                string orderId = "NA";
                string transactionType = AlgoOMS.GetTransactionType(userID, TradingSymbol, Exchange);
                //sanika::3-Nov-2020::changed condition for duplicate slm order
                if (transactionType == Constants.TRANSACTION_TYPE_BUY && lastTransactionType != Constants.TRANSACTION_TYPE_SELL)//condition for transaction type
                {
                    if (CheckForPendingStopOrder(TradingSymbol,Exchange))
                    {
                        if (AlgoOMS.IsPendingStopOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_SELL) == false )//condition to check pending order
                        {
                            WriteUniquelogs(TradingSymbol + " " + userID, ">>>>> PlaceStopOrderIfNotPresent :openPrice " + openPrice, MessageType.Informational);
                            decimal calculatedPrice = 0;
                            double PercentValue = StopLoss * openPrice / 100;
                            double CalPrice = openPrice - PercentValue;
                            calculatedPrice = Convert.ToDecimal(AlgoOMS.RoundOff(userID, TradingSymbol, Convert.ToDecimal(CalPrice), false));
                            WriteUniquelogs(TradingSymbol + " " + userID, "PercentValue " + PercentValue+ " CalPrice = " + CalPrice+ " Trigger Price = " + calculatedPrice, MessageType.Informational);
                            WriteUniquelogs(TradingSymbol + " " + userID, ">>>>> PlaceStopOrderIfNotPresent : Placed sell Stop Order because opposite stop order not present\n", MessageType.Informational);
                            if (AlgoOMS.GetStopPricebyOrderID(userID, TradingSymbol, Exchange, m_OrderInfo.GetOpenOrderId(TradingSymbol, Constants.TRANSACTION_TYPE_SELL)) == 0)//sanika::20-oct-2020::changed != to ==
                            {
                                orderId = AlgoOMS.PlaceStopOrder(userID, Exchange, TradingSymbol, Constants.TRANSACTION_TYPE_SELL, Quantity, 0, Convert.ToDecimal(calculatedPrice));
                                m_OrderInfo.AddOrUpdateBuyOrSell(TradingSymbol, Constants.TRANSACTION_TYPE_SELL);//sanika::20-oct-2020::added bacause modify after placing order
                                AlgoOMS.SetSymbolLastOrder(userID, TradingSymbol, Constants.TRANSACTION_TYPE_SELL);//sanika::3-Nov-2020::added transaction type into master list
                            }
                            else
                            {
                                WriteUniquelogs(TradingSymbol + " " + userID, "Stop price of sell order = 0", MessageType.Informational);
                            }
                        }
                    }
                    else
                    {
                        //WriteUniquelogs(TradingSymbol + " " + userID, "***** PlaceStopOrderIfNotPresent :openPrice is not greater than zero", MessageType.Informational);
                    }
                }
                //sanika::3-Nov-2020::changed condition for duplicate slm order
                else if (transactionType == Constants.TRANSACTION_TYPE_SELL && lastTransactionType != Constants.TRANSACTION_TYPE_BUY)//condition for transaction type
                {
                    if (CheckForPendingStopOrder(TradingSymbol,Exchange))
                    {
                        if (AlgoOMS.IsPendingStopOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_BUY) == false)//condition to check pending order
                        {
                            WriteUniquelogs(TradingSymbol + " " + userID, ">>>>> PlaceStopOrderIfNotPresent :openPrice " + openPrice, MessageType.Informational);
                            decimal calculatedPrice = 0;
                            double PercentValue = StopLoss * openPrice / 100;
                            double CalPrice = openPrice + PercentValue;
                            calculatedPrice = Convert.ToDecimal(AlgoOMS.RoundOff(userID, TradingSymbol, Convert.ToDecimal(CalPrice), false));
                            WriteUniquelogs(TradingSymbol + " " + userID, "PercentValue" + PercentValue+ " CalPrice "+ CalPrice+ " Trigger Price = " + calculatedPrice, MessageType.Informational);
                            WriteUniquelogs(TradingSymbol + " " + userID, ">>>>> PlaceStopOrderIfNotPresent : Placed buy Stop Order because opposite stop order not present\n", MessageType.Informational);
                            if (AlgoOMS.GetStopPricebyOrderID(userID, TradingSymbol, Exchange, m_OrderInfo.GetOpenOrderId(TradingSymbol, Constants.TRANSACTION_TYPE_BUY)) == 0)//sanika::20-oct-2020::changed != to ==
                            {
                                orderId = AlgoOMS.PlaceStopOrder(userID, Exchange, TradingSymbol, Constants.TRANSACTION_TYPE_BUY, Quantity, 0, Convert.ToDecimal(calculatedPrice));
                                m_OrderInfo.AddOrUpdateBuyOrSell(TradingSymbol, Constants.TRANSACTION_TYPE_BUY);//sanika::20-oct-2020::added bacause modify after placing order
                                AlgoOMS.SetSymbolLastOrder(userID, TradingSymbol, Constants.TRANSACTION_TYPE_BUY);//sanika::3-Nov-2020::added transaction type into master list
                            }
                            else
                            {
                                WriteUniquelogs(TradingSymbol + " " + userID, "Stop price of buy order = 0", MessageType.Informational);
                            }
                        }
                    }
                    else
                    {
                        //WriteUniquelogs(TradingSymbol + " " + userID, "***** PlaceStopOrderIfNotPresent :openPrice is not greater than zero", MessageType.Informational);
                    }
                }
                if (orderId != "NA")
                {
                    OrderCount++;
                    m_OrderInfo.StopOrderCounter(TradingSymbol);
                    m_OrderInfo.AddOrUpdateStopOrderId(TradingSymbol,orderId);
                    WriteUniquelogs(TradingSymbol + " " + userID, ">>>>> PlaceStopOrderIfNotPresent : placed successfully with orderId = " + orderId+"\n", MessageType.Informational);
                }
                else
                {
                    //WriteUniquelogs(TradingSymbol + " " + userID, "***** placeStopOrder : Not placed", MessageType.Informational);
                }

            }
            else
            {
                //WriteUniquelogs(TradingSymbol + " " + userID, ">>>>> PlaceStopOrderIfNotPresent : individualCount condition false counter = " + counter, MessageType.Informational);
            }           
        }

        //load values from ini file
        public void loadINIValues()
        {
            try
            {
                string currentTime = DateTime.Now.ToString("HH:mm") + ":00";
                MaxOrderCount = Convert.ToInt32(readSettings.maxCount);
                IndividualOrderCount = Convert.ToInt32(readSettings.individualCount);
                StartTime = readSettings.startTime;                
                OverallLoss = Convert.ToDouble(readSettings.overallLoss);
                OverallProfit = Convert.ToDouble(readSettings.overallProfit);
                Percent = readSettings.Percent;
                TotalMoney = Convert.ToDouble(readSettings.totalMoney);
                ProfitTrail = readSettings.ProfitTrail;
                Interval = Convert.ToInt32(readSettings.interval);
                EndTime = readSettings.endTime;
                PriceDiffPercent = readSettings.priceDiffPercent;
                PlacedOrderPriceDiffPercent = readSettings.PlacedOrderPriceDiffPercent;
                m_RiskPercentForStock = readSettings.m_RiskPercentForStock;
                m_RiskPercentForFuture = readSettings.m_RiskPercentForFuture;
                if (TimeSpan.Parse(StartTime) < TimeSpan.Parse(currentTime))
                {
                    StartTime = currentTime;
                }
                DateTime dt1 = DateTime.ParseExact(StartTime, "HH:mm:ss", null);
                StartTime = dt1.AddSeconds(60 * (Interval*-1)).ToString("HH:mm:ss");
                DateTime dt = DateTime.ParseExact(StartTime, "HH:mm:ss", null);
                FirstTickEnd = dt.AddSeconds(60 * Interval).ToString("HH:mm:ss");
                StrategyStartTime = FirstTickEnd;
                End = dt.AddSeconds(60 * (Interval + 1)).ToString("HH:mm:ss");
                TimeForExitAllOpenPosition = readSettings.TimeForExitAllOpenPosition;
                TimeLimitToPlaceOrder = readSettings.TimeLimitToPlaceOrder;
                ListOfTradingSymbolsInfo = readSettings.TradingSymbolsInfoList;
                m_MaxLossPercentForIndividualSymbol = readSettings.m_MaxLossPercentForIndividualSymbol;
                m_MaxLossValueForIndividualSymbol = readSettings.m_MaxLossValueForIndividualSymbol;
                m_OverallLoss = readSettings.m_OverallLoss;
                m_OverallProfitAmt = readSettings.m_OverallProfitAmt;
                m_OverallProfitPercent = readSettings.m_OverallProfitPercent;
            }
            catch (Exception e)
            {
                logger.LogMessage("loadINIValues - Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        //function to start thread
        public bool startThread(string userID)
        {
            readSettings = new ReadSettings(this.logger,AlgoOMS);
            bool res = readSettings.readChan_SandipConfigFile(iniFile);
            if (!res)
            {
                return res;
            }
            loadINIValues();
            if (automation == null)
            {
                logger.LogMessage("Strating StartAutomation thread", MessageType.Informational);
                automation = new Thread(() => ExecuteSyncLogic());
                automation.Start();
            }
            return true;
        }

        //add symbols to inticker 
        public void CheckTradingSymbolPresentOrNot()
        {
            try
            {
                AlgoOMS.CheckTradingSymbolPresentOrNot(userID, readSettings.TradingsymbolList);
            }
            catch (Exception e)
            {
                logger.LogMessage("CheckTradingSymbolPresentOrNot : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }


        //function to calculateProfit 
        public bool calculateProfit()
        {
            bool isProfitLossHit = false;
            try
            {
                isProfitLossHit = AlgoOMS.IsOverallAllProfitLossHit(userID);
            }
            catch (Exception e)
            {
                logger.LogMessage("calculateProfit : Exception Error Message =  " + e.Message, MessageType.Exception);
            }
            return isProfitLossHit;
        }

        //function to stop thread
        public bool StopThread()
        {
            m_IsThreadStop = true;
            // if (AlgoOMS.stopThread())
            {
                if (automation != null)
                {
                    if (automation.IsAlive)
                    {
                        logger.LogMessage("Stopped Thread", MessageType.Informational);
                        automation.Abort();
                        Thread.Sleep(1000);
                        if (!automation.IsAlive)
                            return true;
                    }
                }
            }
            return true;
        }

        //function to copy object of form_Auto
        public void loadObject(form_autoTrading form_Auto)
        {
            this.form_Auto = form_Auto;
        }

        //function for write logs
        public void WriteUniquelogs(string symbol, string message, MessageType msgType)
        {
            try
            {
                SymbolLogs log = new SymbolLogs();
                //log = SymbolLogs.InstanceSymbolLogs;
                log.createFile(symbol);
                log.LogMessage(message, MessageType.Informational);
                log = null;
            }
            catch (Exception e)
            {
                Console.WriteLine("WriteUniquelogs : Exception : " + e.Message);
            }
        }

        ////function to add symbols in gridview(gui)
        //public void addIntoDataGridView(string TradingSymbol, string TransactionType, string Quantity, string Price)
        //{
        //    try
        //    {
        //        mydt.Rows.Add(new object[] { TradingSymbol, TransactionType, Quantity, Price });
        //        ListForGridView.Add(TradingSymbol);
        //    }
        //    catch (Exception e)
        //    {
        //        WriteUniquelogs(TradingSymbol + " " + userID, "addIntoDataGridView : Exception Error Message = " + e.Message, MessageType.Exception);
        //    }
        //}

        ////function to update values from gridview(gui)
        //public void updateIntoDataGridView(string TradingSymbol, string TransactionType, string Quantity, string Price)
        //{
        //    try
        //    {
        //        foreach (DataRow dr in mydt.Rows)
        //        {
        //            if ((dr["Symbol"]).ToString() == TradingSymbol)
        //            {
        //                WriteUniquelogs(TradingSymbol + " " + userID, "updateIntoDataGridView : Found symbol " + TradingSymbol, MessageType.Informational);
        //                dr["Transaction Type"] = TransactionType;
        //                dr["Quantity"] = Quantity;
        //                dr["Price"] = Price;
        //                break;
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        WriteUniquelogs(TradingSymbol + " " + userID, "updateIntoDataGridView : Exception Error Message = " + e.Message, MessageType.Exception);
        //    }
        //}

        //add or update symbols with there values on gui
        //public void loadOpenPositions(string user = "", AlgoOMS AlgoOMS = null, Logger logger = null)
        //{
        //    if (user != "")
        //        userID = user;
        //    if (AlgoOMS != null)
        //        this.AlgoOMS = AlgoOMS;
        //    if (logger != null)
        //        this.logger = logger;
        //    try
        //    {
        //        PositionResponse positionResponseForOverall = this.AlgoOMS.GetOpenPosition(userID);
        //        if (positionResponseForOverall.Net.Count != 0)
        //        {
        //            foreach (var Position in positionResponseForOverall.Net)
        //            {
        //                if (readSettings.TradingsymbolListWithFutureName.Contains(Position.TradingSymbol))
        //                {

        //                    string transactionType = "";
        //                    string price = "";
        //                    if (Position.Quantity > 0)
        //                    {
        //                        transactionType = Constants.TRANSACTION_TYPE_BUY;
        //                        price = Position.BuyPrice.ToString();
        //                    }
        //                    else if (Position.Quantity < 0)
        //                    {
        //                        transactionType = Constants.TRANSACTION_TYPE_SELL;
        //                        price = Position.SellPrice.ToString();
        //                    }
        //                    //  var filtered = mydt.AsEnumerable().Where(r => r.Field<String>("Symbol").Contains(Position.TradingSymbol));
        //                    //if (!ListForGridView.Contains(Position.TradingSymbol))
        //                    //{
        //                    //    addIntoDataGridView(Position.TradingSymbol, transactionType, Position.Quantity.ToString(), price);
        //                    //}
        //                    //else
        //                    //{
        //                    //    updateIntoDataGridView(Position.TradingSymbol, transactionType, Position.Quantity.ToString(), price);
        //                    //}

        //                }
        //            }
        //        }
        //    }
        //    catch (Exception er)
        //    {
        //        this.logger.LogMessage("LoadOpenPostion : Exception Error Message =  " + er.Message, MessageType.Exception);
        //    }
        //}

        //read trading symbols from ini
        public void readTradingSymbol(ReadSettings readSettings)
        {
            if (readSettings == null)
                this.readSettings = readSettings;
            if (readSettings.TradingsymbolList.Count == 0)
                readSettings.readChan_SandipConfigFile(iniFile);
        }

        //function change time after interval
        public void changeTime(string tickCurrentTime)
        {
            //logger.LogMessage("tickCurrentTime " + tickCurrentTime, MessageType.Informational);
            if ((TimeSpan.Parse(tickCurrentTime) >= TimeSpan.Parse(FirstTickEnd)))
            {
                //logger.LogMessage("Before change Time - FirstTickEnd " + FirstTickEnd, MessageType.Informational);
                //logger.LogMessage("Before change Time - End " + End, MessageType.Informational);
                DateTime dt1 = DateTime.ParseExact(FirstTickEnd, "HH:mm:ss", null);
                FirstTickEnd = dt1.AddSeconds(60 * Convert.ToInt32(Interval)).ToString("HH:mm:ss");
                End = dt1.AddSeconds(60 * ((Convert.ToInt32(Interval)) + 1)).ToString("HH:mm:ss");
                //logger.LogMessage("After change Time - FirstTickEnd " + FirstTickEnd, MessageType.Informational);
                //logger.LogMessage("After change Time - End " + End, MessageType.Informational);
            }
        }

        //function calculate quantity according to lotsize
        public int GetQuantity(string TradingSymbol, string Exchange)
        {
            int quantity = 0;
            double lastPrice = AlgoOMS.GetLastPrice(userID, TradingSymbol);
            try
            {                
                //sanika::19-oct-2020::added for nfo symbols
                if (Exchange == Constants.EXCHANGE_NFO)
                {
                    quantity = AlgoOMS.GetLotSize(userID, TradingSymbol, Exchange);
                }
                else if(Percent == 0)
                {
                    quantity = 1;
                }
                else
                {
                    //temp commented
                    int lotSize = 0;// AlgoOMS.GetLotSize(userID, TradingSymbol, Exchange);
                    if (lotSize != 0)
                    {
                        quantity = (lotSize * Convert.ToInt32(Percent)) / 100;
                    }
                    if (lotSize == 0 || quantity == 0)
                    {
                        //sanika::25-sep-2020::added condition for exception of divide by zero
                        if (lastPrice != 0)
                        {
                            WriteUniquelogs(TradingSymbol + " " + userID, "GetQuantity : lotSize is 0", MessageType.Informational);
                            decimal value = ((Convert.ToDecimal(TotalMoney) * Convert.ToDecimal(Percent)) / 100);
                            quantity = Convert.ToInt32(value / Convert.ToDecimal(lastPrice));
                            WriteUniquelogs(TradingSymbol + " " + userID, "GetQuantity : TotalMoney " + TotalMoney + " Percent " + Percent + " lastPrice " + lastPrice + " quantity " + quantity, MessageType.Informational);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + userID, "GetQuantity : Exception Error Message =  " + e.Message, MessageType.Exception);
            }
            //WriteUniquelogs(TradingSymbol + " " + userID, "GetQuantity : quantity " + quantity, MessageType.Informational);
            return quantity;
        }

        //place profit order
        public void PlaceProfitOrder(string TradingSymbol, string Exchange, double OpenPrice, double LastPrice, double ProfitPercent, int Quantity)
        {
            //sanika::19-Nov-2020::issue of transaction type to place profit order
            string transactionType = AlgoOMS.GetTransactionType(userID, TradingSymbol, Exchange);
            //condition to check pending order and order already placed or not (sell)
            if ((!m_OrderInfo.GetIsProfitOrderPlaced(TradingSymbol)) && transactionType == Constants.TRANSACTION_TYPE_BUY)
            {
                WriteUniquelogs(TradingSymbol + " " + userID, "@#@#@#PlaceProfitOrder :price " + OpenPrice, MessageType.Informational);
                decimal calculatedPrice = 0;
                double PercentValue = ProfitPercent * OpenPrice / 100;
                double CalPrice = OpenPrice + PercentValue;
                calculatedPrice = Convert.ToDecimal(AlgoOMS.RoundOff(userID, TradingSymbol, Convert.ToDecimal(CalPrice), false));
                WriteUniquelogs(TradingSymbol + " " + userID, " PercentValue " + PercentValue + " CalPrice " + CalPrice + " Trigger Price = " + calculatedPrice, MessageType.Informational);
                WriteUniquelogs(TradingSymbol + " " + userID, "@#@#@# PlaceProfitOrder : place sell profit order\n", MessageType.Informational);
                string orderId = AlgoOMS.PlaceLimitOrder(userID, Exchange, TradingSymbol, Constants.TRANSACTION_TYPE_SELL, Quantity, calculatedPrice);
                if (orderId != "NA")
                {
                    m_OrderInfo.AddOrUpdateIsProfitOrderPlaced(TradingSymbol, true);
                    m_OrderInfo.AddOrUpdateProfitOrder(TradingSymbol, Constants.TRANSACTION_TYPE_SELL, orderId);
                }
            }
            //sanika::19-Nov-2020::issue of transaction type to place profit order
            //condition to check pending order and order already placed or not(buy)
            else if ((!m_OrderInfo.GetIsProfitOrderPlaced(TradingSymbol)) && transactionType == Constants.TRANSACTION_TYPE_SELL)
            {
                WriteUniquelogs(TradingSymbol + " " + userID, "@#@#@# PlaceProfitOrder :price " + OpenPrice, MessageType.Informational);
                decimal calculatedPrice = 0;
                double PercentValue = ProfitPercent * OpenPrice / 100;
                double CalPrice = OpenPrice - PercentValue;
                calculatedPrice = Convert.ToDecimal(AlgoOMS.RoundOff(userID, TradingSymbol, Convert.ToDecimal(CalPrice), false));
                WriteUniquelogs(TradingSymbol + " " + userID, "PercentValue " + PercentValue + " CalPrice " + CalPrice + "Trigger Price = " + calculatedPrice, MessageType.Informational);
                WriteUniquelogs(TradingSymbol + " " + userID, "@#@#@# PlaceProfitOrder : place buy profit order\n", MessageType.Informational);
                string orderId = AlgoOMS.PlaceLimitOrder(userID, Exchange, TradingSymbol, Constants.TRANSACTION_TYPE_BUY, Quantity, calculatedPrice);
                if (orderId != "NA")
                {
                    m_OrderInfo.AddOrUpdateIsProfitOrderPlaced(TradingSymbol, true);
                    m_OrderInfo.AddOrUpdateProfitOrder(TradingSymbol, Constants.TRANSACTION_TYPE_BUY, orderId);
                }
            }
        }

        //close all order after clikcing stop automation and close order
        public bool CloseAllOrder()
        {
            bool isCloseAllOrder = false;
            try
            {
                foreach (var symbolInfo in ListOfTradingSymbolsInfo) //loop for symbols from ini file
                {
                    string TradingSymbol = symbolInfo.getSymbol();
                    string Exchange = symbolInfo.getExchange();
                    if(AlgoOMS.CloseOrder(userID, TradingSymbol, Exchange))
                    {
                        isCloseAllOrder = true;
                    }
                    else
                    {
                        isCloseAllOrder = false;
                    }
                    
                }
                logger.LogMessage("CloseAllOrder : closed all order", MessageType.Informational);
            }
            catch (Exception e)
            {
                logger.LogMessage("CloseAllOrder : Exception Error Message = " + e.Message, MessageType.Exception);
            }

            return isCloseAllOrder;
        }

        //sanika::28-sep-2020::added to save bin file
        public void SaveBinFile()
        {
            try
            {
                Stream SaveFileStream = File.Create(m_BinFileName);
                BinaryFormatter serializer = new BinaryFormatter();
                serializer.Serialize(SaveFileStream, m_OrderInfo);
                SaveFileStream.Close();
                WriteUniquelogs("RestoreStructure", "SaveBinFile : File saved sucessfully", MessageType.Informational);
            }
            catch(Exception e)
            {
                WriteUniquelogs("RestoreStructure", "SaveBinFile : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        //sanika::28-sep-2020::added to delete bin file
        public void DeleteBinFile()
        {
            try
            {
                if (File.Exists(m_BinFileName))
                {
                    File.Delete(m_BinFileName);
                    WriteUniquelogs("RestoreStructure", "DeleteBinFile : File deleted sucessfully", MessageType.Informational);
                }
                else
                {
                    WriteUniquelogs("RestoreStructure", "DeleteBinFile : File not exists", MessageType.Informational);
                }
            }
            catch(Exception e)
            {
                WriteUniquelogs("RestoreStructure", "DeleteBinFile : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        //sanika::28-sep-2020::added new function to load old structure 
        public bool LoadStructure(OpenOrderInfo openOrderInfo)
        {
            try
            {
                readSettings = new ReadSettings(this.logger, AlgoOMS);
                bool res = readSettings.readChan_SandipConfigFile(iniFile);
                if (!res)
                {
                    WriteUniquelogs("RestoreStructure", "LoadStructure : Not able to read time", MessageType.Informational);
                    return res;
                }
                loadINIValues();
                WriteUniquelogs("RestoreStructure", "LoadStructure : Read time from ini file", MessageType.Informational);
                DateTime startDateTime = DateTime.ParseExact(StrategyStartTime, "HH:mm:ss", null);
                DateTime endTime = DateTime.ParseExact("15:30:00", "HH:mm:ss", null);//sanika::22-Jan-2021::Added check for after market hrs pop should not be shown
                if (DateTime.Now.ToOADate() > startDateTime.ToOADate() && DateTime.Now.ToOADate() < endTime.ToOADate())
                {
                    WriteUniquelogs("RestoreStructure", "LoadStructure : Time condition for restore structure true", MessageType.Informational);
                    DialogResult dialogResult = MessageBox.Show("Do you want to restore last execution?", "IRDSStrategyExecutor", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        WriteUniquelogs("RestoreStructure", "LoadStructure : Click on Yes", MessageType.Informational);
                        //do something
                        //sanika::25-sep-2020::added for resume exe
                        if (File.Exists(m_BinFileName))
                        {
                            Console.WriteLine("Reading saved file");
                            Stream openFileStream = File.OpenRead(m_BinFileName);
                            if (openFileStream.Length != 0)
                            {
                                BinaryFormatter deserializer = new BinaryFormatter();
                                m_OrderInfo = (OpenOrderInfo)deserializer.Deserialize(openFileStream);
                                if(m_OrderInfo != null)
                                {
                                    m_OrderInfo.AddInformationIntoLog(this);
                                }
                                WriteUniquelogs("RestoreStructure", "LoadStructure : Successfully restored structure", MessageType.Informational);
                            }
                            else
                            {
                                WriteUniquelogs("RestoreStructure", "LoadStructure : bin file is empty", MessageType.Informational);
                            }
                            openFileStream.Close();
                        }
                        else
                        {
                            WriteUniquelogs("RestoreStructure", "LoadStructure : Bin file not exists", MessageType.Informational);
                        }
                    }
                    else if (dialogResult == DialogResult.No)
                    {
                        WriteUniquelogs("RestoreStructure", "LoadStructure : Click on No", MessageType.Informational);
                    }

                }
                else
                {
                    WriteUniquelogs("RestoreStructure", "LoadStructure : Time condition not match to restore data StrategyStartTime " + StrategyStartTime, MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs("RestoreStructure", "LoadStructure : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return true;
        }

        public bool GetChangeInPercent(string TradingSymbol,string tickTime, out double highPercent, out double lowPercent)
        {
            highPercent = 0;
            lowPercent = 0;
            try
            {
                DateTime dt = DateTime.ParseExact("09:15:00", "HH:mm:ss", null);
                string start = DateTime.Now.ToString("dd-MM-yyyy") + " " + dt.AddSeconds(60 * (Interval * -5)).ToString("HH:mm:ss");
                dt = DateTime.ParseExact(start, "dd-MM-yyyy HH:mm:ss", null);
                double startT = dt.ToOADate();

                string end = DateTime.Now.ToString("dd-MM-yyyy") +" "+ readSettings.startTime;
                dt = DateTime.ParseExact(end, "dd-MM-yyyy HH:mm:ss", null);
                double endT = dt.ToOADate();
                WriteUniquelogs(TradingSymbol + " " + userID, "GetChangeInPercent : startT = "+ start + " end = "+ end, MessageType.Informational);
                AlgoOMS.GetChangeInPercent(userID, TradingSymbol, tickTime, out highPercent,out lowPercent);
                WriteUniquelogs(TradingSymbol + " " + userID, "GetChangeInPercent : highPercent = " + highPercent + " lowPercent = " + lowPercent, MessageType.Informational);
                return true;
            }
            catch(Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + userID, "GetChangeInPercent : Exception Error Message =  " + e.Message, MessageType.Exception);
            }
            return false;
        }

        //
        public bool IsPriceDiffInRange(string TradingSymbol,double lastPrice,double orderPrice)
        {
            
            try
            {
                if (PriceDiffPercent != 0)
                {
                    double CalculatedPercentOfPrice = (lastPrice * PriceDiffPercent) / 100;
                    double priceDiff = Math.Abs(lastPrice - orderPrice);

                    if (priceDiff <= CalculatedPercentOfPrice)
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "IsPricePercentInRange : lastPrice " + lastPrice + " PriceDiffPercent " + PriceDiffPercent + " CalculatedPercentOfPrice " + CalculatedPercentOfPrice + " orderPrice " + orderPrice + " priceDiff " + priceDiff, MessageType.Informational);
                        WriteUniquelogs(TradingSymbol + " " + userID, "IsPricePercentInRange : Returning true", MessageType.Informational);
                        return true;
                    }
                    else
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "IsPricePercentInRange : lastPrice " + lastPrice + " PriceDiffPercent " + PriceDiffPercent + " CalculatedPercentOfPrice " + CalculatedPercentOfPrice + " orderPrice " + orderPrice + " priceDiff " + priceDiff, MessageType.Informational);
                        WriteUniquelogs(TradingSymbol + " " + userID, "IsPricePercentInRange : Returning false", MessageType.Informational);
                        return false;
                    }
                }
                else
                {
                    WriteUniquelogs(TradingSymbol + " " + userID, "IsPricePercentInRange : Returning true because PriceDiffPercent is 0", MessageType.Informational);
                    return true;
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + userID, "IsPricePercentInRange : Exception Error Message =  " + e.Message, MessageType.Exception);
            }
            return false;
        }

        public bool IsPercentInRange(string TradingSymbol,string tickTime, double highLowPercent, string TransactionType)
        {
            double highPercent = 0;
            double lowPercent = 0;
            try
            {               
                    AlgoOMS.GetChangeInPercent(userID, TradingSymbol, tickTime, out highPercent, out lowPercent);
                    //WriteUniquelogs(TradingSymbol + " " + userID, "GetChangeInPercent : highPercent = " + highPercent + " lowPercent = " + lowPercent, MessageType.Informational);
                    if (highLowPercent == 0)
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "IsPercentInRange : m_HighLowPercent is zero", MessageType.Informational);
                        return true;
                    }
                    else
                    {
                        if (TransactionType == Constants.TRANSACTION_TYPE_BUY)
                        {
                            WriteUniquelogs(TradingSymbol + " " + userID, "IsPercentInRange : highPercent = " + highPercent + " highLowPercent = " + highLowPercent, MessageType.Informational);
                            return (highPercent > highLowPercent);
                        }
                        else if (TransactionType == Constants.TRANSACTION_TYPE_SELL)
                        {
                            WriteUniquelogs(TradingSymbol + " " + userID, "IsPercentInRange : lowPercent = " + lowPercent + " highLowPercent = " + highLowPercent, MessageType.Informational);
                            return (lowPercent > highLowPercent);
                        }
                    }                                
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + userID, "IsPercentInRange : Exception Error Message =  " + e.Message, MessageType.Exception);
            }
            return false;
        }

        //sanika::26-Nov-2020::Added for ajay sir's strategy
        public void GetTodaysHighLow(string TradingSymbol, out double high, out double low, out double middle)
        {
            try
            {
                double dHigh = 0;
                double dLow = 0;
                string startTime = "09:15:00";
                string endTime = DateTime.Now.ToString("HH:mm") + ":00";
                AlgoOMS.GetHighLowFromDB(userID, TradingSymbol, startTime, endTime, out dHigh, out dLow);
                //WriteUniquelogs(TradingSymbol + " " + userID, "getHighLowValuesFromDB : before round up dHigh " + dHigh + " dLow " + dLow, MessageType.Informational);
                if (dHigh % 0.5 != 0)
                {
                    high = AlgoOMS.RoundOff(userID, TradingSymbol, Convert.ToDecimal(dHigh), true);
                }
                else
                {
                    high = dHigh;
                }
                if (dLow % 0.5 != 0)
                {
                    low = AlgoOMS.RoundOff(userID, TradingSymbol, Convert.ToDecimal(dLow), false);
                }
                else
                {
                    low = dLow;
                }

                if (dHigh != 0 && dLow != 0)
                {
                    middle = (dHigh + dLow) / 2;
                }
                else
                {
                    WriteUniquelogs(TradingSymbol + " " + userID, "GetTodaysHighLowClose : table not exist " + TradingSymbol, MessageType.Informational);
                    middle = 0;
                }

            }
            catch (Exception e)
            {
                high = 0;
                low = 0;
                middle = 0;
                WriteUniquelogs(TradingSymbol + " " + userID, "GetTodaysHighLowClose : Exception Error Message =  " + e.Message, MessageType.Exception);
            }
        }

        public void CheckIndividualProfitLoss(string TradingSymbol,string Exchange,double stoploss)
        {
            try
            {
                double profitLoss = AlgoOMS.GetIndividualProfitLoss(userID, TradingSymbol, Exchange);
                if(profitLoss != 0 && profitLoss < 0)
                {
                    double lastPrice = AlgoOMS.GetLastPrice(userID, TradingSymbol);
                    string transactionType = AlgoOMS.GetTransactionType(userID, TradingSymbol, Exchange);                    
                    double percentValue = 0;
                    if (m_OrderInfo.GetStopOrderCounter(TradingSymbol) < 3)
                    {
                        percentValue = stoploss * m_MaxLossValueForIndividualSymbol + m_MaxLossPercentForIndividualSymbol;
                        percentValue = percentValue / 2;
                        WriteUniquelogs(TradingSymbol + " " + userID, "CheckIndividualProfitLoss : Checking individual loss for first order", MessageType.Informational);
                    }
                    else
                    {
                        percentValue = stoploss * m_MaxLossValueForIndividualSymbol + m_MaxLossPercentForIndividualSymbol;
                        WriteUniquelogs(TradingSymbol + " " + userID, "CheckIndividualProfitLoss : Checking individual loss for second order", MessageType.Informational);
                    }
                    int quantity = m_OrderInfo.GetQuantity(TradingSymbol, Exchange);
                    double calculatedProfitLoss = (lastPrice * percentValue/100)* quantity;                       
                    if (Math.Abs(profitLoss) > calculatedProfitLoss)
                    {
                        string orderid = m_OrderInfo.GetStopLossOrderId(TradingSymbol);
                        if (AlgoOMS.GetOrderStatusByOrderID(userID, TradingSymbol, Exchange, orderid) == Constants.ORDER_STATUS_PENDING)
                        {
                            AlgoOMS.CloseOrder(userID, TradingSymbol,Exchange);
                            m_OrderInfo.AddIgnoreValue(TradingSymbol, 1);
                            WriteUniquelogs(TradingSymbol + " " + userID, "CheckIndividualProfitLoss : Closed order and add into ignore list", MessageType.Informational);
                            WriteUniquelogs(TradingSymbol + " " + userID, "CheckIndividualProfitLoss : profitLoss " + profitLoss + " lastPrice " + lastPrice + " transactionType " + transactionType + " stoploss " + stoploss + " m_MaxLossValueForIndividualSymbol " + m_MaxLossValueForIndividualSymbol + " m_MaxLossPercentForIndividualSymbol " + m_MaxLossPercentForIndividualSymbol + " calculatedProfitLoss " + calculatedProfitLoss + " quantity " + quantity, MessageType.Informational);
                        }
                        else
                        {
                            WriteUniquelogs(TradingSymbol + " " + userID, "CheckIndividualProfitLoss : Order already closed",MessageType.Informational);
                        }
                    }
                    else
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "CheckIndividualProfitLoss : Not match condition of individual profit loss", MessageType.Informational);
                        WriteUniquelogs(TradingSymbol + " " + userID, "CheckIndividualProfitLoss : profitLoss " + profitLoss + " lastPrice " + lastPrice + " transactionType " + transactionType + " stoploss " + stoploss + " m_MaxLossValueForIndividualSymbol " + m_MaxLossValueForIndividualSymbol + " m_MaxLossPercentForIndividualSymbol " + m_MaxLossPercentForIndividualSymbol + " calculatedProfitLoss " + calculatedProfitLoss+ " quantity "+ quantity, MessageType.Informational);
                    }                   
                }
                else
                {
                    WriteUniquelogs(TradingSymbol + " " + userID, "CheckIndividualProfitLoss : symbol is in profit profitLoss "+ profitLoss, MessageType.Informational);
                }
            }
            catch(Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + userID, "CheckIndividualProfitLoss : Exception Error Message =  " + e.Message, MessageType.Exception);
            }
        }

        public bool IsPlacedOrderPriceDiffInRange(string TradingSymbol, double LastPrice,string TransactionType,string Exchange)
        {

            try
            {
                if (PlacedOrderPriceDiffPercent != 0)
                {
                    double orderPrice = AlgoOMS.GetStopPricebyOrderID(userID, TradingSymbol, Exchange, m_OrderInfo.GetOpenOrderId(TradingSymbol, TransactionType));
                    double CalculatedPercentOfPrice = (LastPrice * PlacedOrderPriceDiffPercent) / 100;
                    double priceDiff = Math.Abs(LastPrice - orderPrice);

                    if (priceDiff <= CalculatedPercentOfPrice)
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "IsPlacedOrderPriceDiffInRange : lastPrice " + LastPrice + " PriceDiffPercent " + PriceDiffPercent + " CalculatedPercentOfPrice " + CalculatedPercentOfPrice + " orderPrice " + orderPrice + " priceDiff " + priceDiff, MessageType.Informational);
                        WriteUniquelogs(TradingSymbol + " " + userID, "IsPlacedOrderPriceDiffInRange : Returning true", MessageType.Informational);
                        return true;
                    }
                    else
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "IsPlacedOrderPriceDiffInRange : lastPrice " + LastPrice + " PriceDiffPercent " + PriceDiffPercent + " CalculatedPercentOfPrice " + CalculatedPercentOfPrice + " orderPrice " + orderPrice + " priceDiff " + priceDiff, MessageType.Informational);
                        WriteUniquelogs(TradingSymbol + " " + userID, "IsPlacedOrderPriceDiffInRange : Returning false,cancelling pending order", MessageType.Informational);
                        return false;
                    }
                }
                else
                {
                    WriteUniquelogs(TradingSymbol + " " + userID, "IsPlacedOrderPriceDiffInRange : Returning true because PriceDiffPercent is 0", MessageType.Informational);
                    return true;
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + userID, "IsPlacedOrderPriceDiffInRange : Exception Error Message =  " + e.Message, MessageType.Exception);
            }
            return false;
        }

        //sanika::5-Apr-2021::Added function for overall loss and overall profit trail (with profit amt and percent)
        public bool CalculateProfitOrLoss()
        {
            try
            {
                double profitLoss = 0;
                foreach (var symbolInfo in ListOfTradingSymbolsInfo) //loop for symbols from ini file
                {
                    string TradingSymbol = symbolInfo.getSymbol();
                    string Exchange = symbolInfo.getExchange();
                    profitLoss += AlgoOMS.GetIndividualProfitLoss(userID, TradingSymbol, Exchange);
                }
                profitLoss = Math.Round(profitLoss, 2);
                Trace.WriteLine("@@profit profitLoss " + profitLoss);
                if (CheckOverallLoss(profitLoss))
                    return true;
                CheckOverallProfitAmt(profitLoss);
               if(m_OverallProfitAmtExceeds)
               {
                   if (CheckOverallProfitPercent(profitLoss))
                       return true;
               }
            }
            catch(Exception e)
            {
                logger.LogMessage("CalculateProfitOrLoss : Exception Error Message =  " + e.Message, MessageType.Exception);
            }
            return false;
        }

        //sanika::5-Apr-2021::Added function for overall loss and overall profit trail (with profit amt and percent)
        public bool CheckOverallLoss(double profitLoss)
        {
            bool isLossHit = false;
            try
            {
                double overallLoss = m_OverallLoss; 
                if (profitLoss < 0 && Math.Abs(profitLoss) > overallLoss)
                {
                    isLossHit = true;
                    CloseAllOrder();
                    logger.LogMessage("CheckOverallLoss : overallLoss " + overallLoss + " profitLoss " + profitLoss + " overall loss hit ", MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                logger.LogMessage("CheckOverallLoss : Exception Error Message =  " + e.Message, MessageType.Exception);
            }
            return isLossHit;
        }

        //sanika::5-Apr-2021::Added function for overall loss and overall profit trail (with profit amt and percent)
        public void CheckOverallProfitAmt(double profitLoss)
        {          
            try
            {
                double overallProfit = m_OverallProfitAmt;               
                if (profitLoss >= overallProfit)
                {
                    m_OverallProfitAmtExceeds = true;
                    logger.LogMessage("CheckOverallLoss : overallProfit "+ overallProfit+" profitLoss " + profitLoss + " overall profit amt hit ", MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                logger.LogMessage("CheckOverallProfitAmt : Exception Error Message =  " + e.Message, MessageType.Exception);
            }
        }

        //sanika::5-Apr-2021::Added function for overall loss and overall profit trail (with profit amt and percent)
        public bool CheckOverallProfitPercent(double profitLoss)
        {
            bool isProfitHit = false;
            try
            {
                double percentValue = (m_OverallProfitAmt * m_OverallProfitPercent) / 100;
                double calcuatedProfitLossAmt = m_OverallProfitAmt - percentValue;
                if(calcuatedProfitLossAmt < profitLoss)
                {
                    isProfitHit = true;
                    CloseAllOrder();
                    logger.LogMessage("CheckOverallProfitPercent : profitLoss " + profitLoss + " m_OverallProfitAmt " + m_OverallProfitAmt + " m_OverallProfitPercent "+ m_OverallProfitPercent + " calcuatedProfitLossAmt " + calcuatedProfitLossAmt + " overall profit hit ", MessageType.Informational);
                }
            }
            catch(Exception e)
            {
                logger.LogMessage("CheckOverallProfitPercent : Exception Error Message =  " + e.Message, MessageType.Exception);
            }
            return isProfitHit;
        }

        //sanika::5-Apr-2021::Added function to modify all orders
        public void ModifyOrder(double StopLossPercent)
        {
            try
            {
                foreach (var symbolInfo in ListOfTradingSymbolsInfo)
                {
                    string TradingSymbol = symbolInfo.getSymbol();
                    string Exchange = symbolInfo.getExchange();
                    if (AlgoOMS.IsPendingStopOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_BUY) == true && AlgoOMS.IsPositionOpen(userID,TradingSymbol,Exchange))
                    {
                       // double openPrice = 0;
                        double stopPrice = AlgoOMS.GetStopPrice(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_BUY);
                        double openPrice = AlgoOMS.GetOpenPostionPricebyOrderID(userID, TradingSymbol, Exchange, m_OrderInfo.GetOpenOrderId(TradingSymbol, Constants.TRANSACTION_TYPE_SELL));
                        if (openPrice != 0)
                        {
                            double percentValue = openPrice * StopLossPercent / 100;
                            double price = openPrice + percentValue;
                            double calculatedPrice = AlgoOMS.RoundOff(userID, TradingSymbol, Convert.ToDecimal(price), true);
                            WriteUniquelogs(TradingSymbol + " " + userID, "##### ModifyOrder :calculatedPrice "+ calculatedPrice +" percentValue " + percentValue +" StopLossPercent " + StopLossPercent +" openPrice " + openPrice + " and stopPrice " + stopPrice, MessageType.Informational);
                            if (AlgoOMS.ModifyStopOrder(userID, TradingSymbol, Constants.TRANSACTION_TYPE_BUY, Convert.ToDecimal(calculatedPrice)))
                            {
                                WriteUniquelogs(TradingSymbol + " " + userID, "##### ModifyOrder :Modified buy order\n", MessageType.Informational);
                            }
                        }
                    }
                    else
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "##### ModifyOrder :pending order not found ", MessageType.Informational);
                    }

                    //modify open  sell order
                    if (AlgoOMS.IsPendingStopOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_SELL) == true && AlgoOMS.IsPositionOpen(userID,TradingSymbol,Exchange))
                    {
                        //double openPrice = 0;
                        double stopPrice = AlgoOMS.GetStopPrice(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_SELL);
                        double openPrice = AlgoOMS.GetOpenPostionPricebyOrderID(userID, TradingSymbol, Exchange, m_OrderInfo.GetOpenOrderId(TradingSymbol, Constants.TRANSACTION_TYPE_BUY));
                        if (openPrice != 0)
                        {
                            double percentValue = openPrice * StopLossPercent / 100;
                            double price = openPrice - percentValue;
                            double calculatedPrice = AlgoOMS.RoundOff(userID, TradingSymbol, Convert.ToDecimal(price), false);
                            WriteUniquelogs(TradingSymbol + " " + userID, "##### ModifyOrder :calculatedPrice "+ calculatedPrice+" percentValue " + percentValue+ " StopLossPercent "+ StopLossPercent +" openPrice   " + openPrice + " and stopPrice " + stopPrice, MessageType.Informational);
                            if (AlgoOMS.ModifyStopOrder(userID, TradingSymbol, Constants.TRANSACTION_TYPE_SELL, Convert.ToDecimal(calculatedPrice)))
                            {
                                WriteUniquelogs(TradingSymbol + " " + userID, "##### ModifyOrder :Modified sell order\n", MessageType.Informational);                               
                            }
                        }
                    }
                    else
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "##### ModifyOrder :pending order not found ", MessageType.Informational);
                    }
                }
            }
            catch(Exception e)
            {
                logger.LogMessage("ModifyOrder : Exception Error Message =  " + e.Message, MessageType.Exception);
            }
        }
    }
}
