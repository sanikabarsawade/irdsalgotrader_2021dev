﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRDSAlgoOMS
{
    public class PositionInfoSamco
    {
        public PositionSamco dayPosition { get; set; }
        public PositionSamco netPosition { get; set; }
        public DateTime LastUpdateTime { get; set; }
        //public PositionInfoSamco() { }
    }
}
