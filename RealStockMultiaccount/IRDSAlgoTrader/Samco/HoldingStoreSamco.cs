﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRDSAlgoOMS
{
    class HoldingStoreSamco
    {
        Dictionary<string, HoldingSamco> dictionaryForHoldings;
        public HoldingStoreSamco()
        {
            dictionaryForHoldings = new Dictionary<string, HoldingSamco>();
        }

        public void AddOrUpdateHoldingInfo(HoldingSamco holding)
        {
            try
            {
                string key = holding.TradingSymbol + "_" + holding.Product + "|" + holding.Exchange;
                if (dictionaryForHoldings.ContainsKey(key))
                {
                    dictionaryForHoldings[key] = holding;
                }
                else
                {
                    dictionaryForHoldings.Add(key, holding);
                }
            }
            catch (Exception e)
            {

            }
        }

        public HoldingSamco GetHoldingBySymbol(string TradingSymbol, string Product, string Exchange)
        {
            HoldingSamco holdingRes = new HoldingSamco();
            try
            {
                string key = TradingSymbol + "_" + Product + "|" + Exchange;
                if (dictionaryForHoldings.ContainsKey(key))
                {
                    holdingRes = dictionaryForHoldings[key];
                }
            }
            catch (Exception e)
            {

            }
            return holdingRes;
        }

        public bool GetAllHoldings(out Dictionary<string, HoldingSamco> AllHoldings)
        {
            AllHoldings = new Dictionary<string, HoldingSamco>();
            bool res = false;
            if (dictionaryForHoldings.Count > 0)
            {
                AllHoldings = dictionaryForHoldings;
                res = true;
            }
            return res;
        }

    }
}
