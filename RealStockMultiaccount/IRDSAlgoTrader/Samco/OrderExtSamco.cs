﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRDSAlgoOMS
{
    public class OrderExtSamco
    {
        public OrderSamco order { get; set; }
        public DateTime LastUpdateTime { get; set; }

        public OrderExtSamco() { }

        public OrderExtSamco(OrderSamco order1)
        {
            order = order1;
            LastUpdateTime = DateTime.Now;
        }
    }

}
