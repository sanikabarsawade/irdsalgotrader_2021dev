﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRDSAlgoOMS
{
    public class SymbolWiseOrderStoreSamco
    {
        public SymbolWiseOrderStoreSamco()
        { }

        private Dictionary<string, OrderExtSamco> orderStoreByOrderId = new Dictionary<string, OrderExtSamco>();

        public void AddOrUpdateOrder(OrderExtSamco newOrder)
        {
            if (orderStoreByOrderId.ContainsKey(newOrder.order.OrderId))
            {
                newOrder.LastUpdateTime = DateTime.Now;
                orderStoreByOrderId[newOrder.order.OrderId] = newOrder;
            }
            else
            {
                orderStoreByOrderId.Add(newOrder.order.OrderId, newOrder);
            }
        }

        public bool GetOrder(string orderId, out OrderExtSamco existingOrder)
        {
            orderStoreByOrderId.TryGetValue(orderId, out existingOrder);
            if (existingOrder != null)
            {
                return true;
            }
            else
            {
                existingOrder = new OrderExtSamco();
                return false;
            }
        }
        public List<OrderSamco> GetAllOrder()
        {
            List<OrderSamco> orderhistorybysymbol = new List<OrderSamco>();
            var orderList = orderStoreByOrderId.Select(o => o.Value).OrderBy(o => o.LastUpdateTime);
            var reverseList = orderList.Reverse();

            foreach (var order1 in reverseList)
            {
                if (order1.order.Status == Constants.ORDER_STATUS_PENDING || order1.order.Status == Constants.ORDER_STATUS_OPEN)
                //if (order1.order.Status == "Trigger Pending" || order1.order.Status == "Open")
                {
                    orderhistorybysymbol.Add(order1.order);
                }
            }
            return orderhistorybysymbol;
        }

        public List<OrderSamco> GetAllOpenOrder()
        {
            List<OrderSamco> orderhistorybysymbol = new List<OrderSamco>();
            var orderList = orderStoreByOrderId.Select(o => o.Value).OrderBy(o => o.LastUpdateTime);
            var reverseList = orderList.Reverse();

            foreach (var order1 in reverseList)
            {
                if (order1.order.Status == Constants.ORDER_STATUS_COMPLETE && order1.order.OrderType == Constants.ORDER_TYPE_MARKET)
                {
                    orderhistorybysymbol.Add(order1.order);
                }
            }
            return orderhistorybysymbol;
        }

        public bool GetLatestOrder(string buyOrSell, out OrderExtSamco existingOrder)
        {
            var orderList = orderStoreByOrderId.Select(o => o.Value).OrderBy(o => o.LastUpdateTime);
            var reverseList = orderList.Reverse();

            foreach (var order in reverseList)
            {
                if (order.order.TransactionType == buyOrSell)
                {
                    existingOrder = order;
                    return true;
                }
            }

            existingOrder = new OrderExtSamco();
            return false;
        }
    }
}
