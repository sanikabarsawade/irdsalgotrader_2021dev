﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRDSAlgoOMS
{
    //class StructuresSamco
    //{
        /*public struct PositionResponseSamco
        {
            public string serverTime { get; }
            public string msgId { get; }
            public string status { get; }
            public string statusMessage { get; }
            public PositionSummary positionSummary { get; }

            public List<PositionDetails> positionDetails { get; }
        }

        public struct PositionSummary
        {
            public string gainingTodayCount { get; }
            public string losingTodayCount { get; }
            public string totalGainAndLossAmount { get; }
            public string dayGainAndLossAmount { get; }
        }

        public class PositionDetails
        {
            public string averagePrice { get; }
            public string exchange { get; }
            public string markToMarketPrice { get; }
            public string lastTradedPrice { get; }
            public string previousClose { get; }
            public string productCode { get; }
            public string tradingSymbol { get; }
            public string calculatedNetQuantity { get; }
            public string averageBuyPrice { get; }
            public string averageSellPrice { get; }
            public string boardLotQuantity { get; }
            public string boughtPrice { get; }
            public string buyQuantity { get; }
            public string carryForwardQuantity { get; }
            public string carryForwardValue { get; }
            public string multiplier { get; }
            public string netPositionValue { get; }
            public string netQuantity { get; }
            public string netValue { get; }
            public string positionType { get; }
            public string[] positionConversions { get; }
            public string soldValue { get; }
            public string transactionType { get; }
            public string realizedGainAndLoss { get; }
            public string unrealizedGainAndLoss { get; }
            public string companyName { get; }
        }*/

        public struct OrderBook
        {
            public string serverTime { get; }
            public string msgId { get; }
            public string status { get; }
            public string statusMessage { get; }
            public List<OrderDetails> orderBookDetails { get; }
        }

        public struct OrderDetails
        {
            public string orderNumber { get; }
            public string exchange { get; }
            public string tradingSymbol { get; }
            public string transactionType { get; }
            public string productCode { get; }
            public string orderType { get; }
            public string orderPrice { get; }
            public string triggerPrice { get; }
            public string orderValidity { get; }
            public string orderStatus { get; }
            public string orderValue { get; }
            public string orderTime { get; }
            public string userId { get; }
            public string filledQuantity { get; }
            public string fillPrice { get; }
            public string averagePrice { get; }
            public string exchangeConfirmationTime { get; }
            public string coverOrderPercentage { get; }
            public string orderRemarks { get; }
            public string exchangeOrderNumber { get; }
            public string symbol { get; }
            public string displayStrikePrice { get; }
            public string displayNetQuantity { get; }
            public string status { get; }
            public string exchangeStatus { get; }
            public string expiry { get; }
            public string pendingQuantity { get; }
            public string totalQuanity { get; }
            public string optionType { get; }
            public string orderPlaceBy { get; }
        }

        public struct PositionSamco
        {
            public PositionSamco(Dictionary<string, dynamic> data)
            {
                try
                {
                    Exchange = data["exchange"];
                    markToMarketPrice = data["markToMarketPrice"];
                    LastPrice = data["lastTradedPrice"];
                    ClosePrice = Convert.ToDecimal(data["previousClose"]);
                    Product = data["productCode"];
                    TradingSymbol = data["tradingSymbol"];
                    Quantity = (int)Convert.ToDouble(data["calculatedNetQuantity"]);
                    BuyPrice = data["averageBuyPrice"];
                    SellPrice = data["averageSellPrice"];
                    AveragePrice = (Quantity == 0) ? 0 : Convert.ToDecimal(data["averagePrice"]); 
                    boardLotQuantity = data["boardLotQuantity"];
                    BuyValue = data["boughtPrice"];
                    buyQuantity = data["buyQuantity"];
                    carryForwardQuantity = data["carryForwardQuantity"];
                    carryForwardValue = data["carryForwardValue"];
                    multiplier = data["multiplier"];
                    netPositionValue = data["netPositionValue"];
                    netQuantity = data["netQuantity"];
                    netValue = data["netValue"];
                    positionType = data["positionType"];
                    positionConversions = new string[0];
                //Array.Copy(data["positionConversions"], positionConversions, data["positionConversions"].length);
                    SellValue = data["soldValue"];
                    transactionType = data["transactionType"];
                    PNL = (Convert.ToDecimal(data["realizedGainAndLoss"]) != 0) ? Convert.ToDecimal(data["realizedGainAndLoss"]) : Convert.ToDecimal(data["unrealizedGainAndLoss"]);
                    unrealizedGainAndLoss = data["unrealizedGainAndLoss"];
                    companyName = data["companyName"];
                }
                catch (Exception)
                {
                    throw new DataException("Unable to parse data. " + Utils.JsonSerialize(data));
                }

            }

            public decimal AveragePrice { get; }
            public string Exchange { get; }
            public string markToMarketPrice { get; }
            public string LastPrice { get; }
            public decimal ClosePrice { get; }
            public string Product { get; }
            public string TradingSymbol { get; }
            public int Quantity { get; }
            public string BuyPrice { get; }
            public string SellPrice { get; }
            public string boardLotQuantity { get; }
            public string BuyValue { get; }
            public string buyQuantity { get; }
            public string carryForwardQuantity { get; }
            public string carryForwardValue { get; }
            public string multiplier { get; }
            public string netPositionValue { get; }
            public string netQuantity { get; }
            public string netValue { get; }
            public string positionType { get; }
            public string[] positionConversions { get; }
            public string SellValue { get; }
            public string transactionType { get; }
            public decimal PNL { get; }
            public string unrealizedGainAndLoss { get; }
            public string companyName { get; }
        }

        /// <summary>
        /// Position response structure
        /// </summary>
        public struct PositionResponseSamco
        {
            public PositionResponseSamco(Dictionary<string, dynamic> dataNet, Dictionary<string, dynamic> dataDay)
            {
                Day = new List<PositionSamco>();
                Net = new List<PositionSamco>();

                if (dataDay.ContainsKey("positionDetails"))
                {
                    foreach (Dictionary<string, dynamic> item in dataDay["positionDetails"])
                        Day.Add(new PositionSamco(item));
                }
                if (dataNet.ContainsKey("positionDetails"))
                {
                    foreach (Dictionary<string, dynamic> item in dataNet["positionDetails"])
                        Net.Add(new PositionSamco(item));
                }
            }

            public List<PositionSamco> Day { get; }
            public List<PositionSamco> Net { get; }
        }

    public struct OrderSamco
    {
        public OrderSamco(Dictionary<string, dynamic> data)
        {
            try
            {
                OrderId = data["orderNumber"];
                Exchange = data["exchange"];
                Tradingsymbol = data["tradingSymbol"];
                TransactionType = data["transactionType"];
                Product = data["productCode"];
                OrderType = data["orderType"];
                Price = Convert.ToDecimal(data["orderPrice"]);
                TriggerPrice = Convert.ToDecimal(data["triggerPrice"]);
                orderValidity = data["orderValidity"];
                orderStatus = data["orderStatus"];
                orderValue = data["orderValue"];
                OrderTimestamp = DateTime.ParseExact((data["orderTime"]), "dd-MMM-yyyy HH:mm:ss", null);
                userId = data["userId"];
                FilledQuantity = data["filledQuantity"];
                fillPrice = data["fillPrice"];
                AveragePrice = Convert.ToDecimal(data["averagePrice"]);
                exchangeConfirmationTime = data["exchangeConfirmationTime"];
                coverOrderPercentage = data["coverOrderPercentage"];
                StatusMessage = data["orderRemarks"];
                if (data.ContainsKey("rejectionReason"))
                    StatusMessage = data["rejectionReason"];
                exchangeOrderNumber = data["exchangeOrderNumber"];
                symbol = data["symbol"];
                displayStrikePrice = data["displayStrikePrice"];
                displayNetQuantity = data["displayNetQuantity"];
                Status = data["status"].ToUpper();
                exchangeStatus = data["exchangeStatus"];
                expiry = data["expiry"];
                pendingQuantity = data["pendingQuantity"];
                Quantity = Convert.ToInt32(data["totalQuanity"]);
                optionType = data["optionType"];
                orderPlaceBy = data["orderPlaceBy"];
                ParentOrderId = null;
            }
            catch (Exception)
            {
                throw new DataException("Unable to parse data. " + Utils.JsonSerialize(data));
            }
        }
        public string OrderId { get; set; }
        public string Exchange { get; set; }
        public string Tradingsymbol { get; set; }
        public string TransactionType { get; set; }
        public string Product { get; set; }
        public string OrderType { get; set; }
        public decimal Price { get; set; }
        public decimal TriggerPrice { get; set; }
        public string orderValidity { get; set; }
        public string orderStatus { get; set; }
        public string orderValue { get; set; }
        public DateTime OrderTimestamp { get; set; }
        public string userId { get; set; }
        public string FilledQuantity { get; set; }
        public string fillPrice { get; set; }
        public decimal AveragePrice { get; set; }
        public string exchangeConfirmationTime { get; set; }
        public string coverOrderPercentage { get; set; }
        public string StatusMessage { get; set; }
        public string exchangeOrderNumber { get; set; }
        public string symbol { get; set; }
        public string displayStrikePrice { get; set; }
        public string displayNetQuantity { get; set; }
        public string Status { get; set; }
        public string exchangeStatus { get; set; }
        public string expiry { get; set; }
        public string pendingQuantity { get; set; }
        public int Quantity { get; set; }
        public string optionType { get; set; }
        public string orderPlaceBy { get; set; }
        public string ParentOrderId { get; set; }
    }

    /// <summary>
    /// Holding structure
    /// </summary>
    public struct HoldingSamco
    {
        public HoldingSamco(Dictionary<string, dynamic> data)
        {
            try
            {
                Product = data["productCode"];
                Exchange = data["exchange"];
                Price = (decimal)Convert.ToDouble(data["lastTradedPrice"]);
                LastPrice = (decimal)Convert.ToDouble(data["lastTradedPrice"]);
                CollateralQuantity = Convert.ToInt32(data["collateralQuantity"]);
                PNL = (decimal)Convert.ToDouble(data["totalGainAndLoss"]);
                ClosePrice = (decimal)Convert.ToDouble(data["previousClose"]);
                AveragePrice = (decimal)Convert.ToDouble(data["averagePrice"]);
                TradingSymbol = data["tradingSymbol"];
                //ISIN = data["isin"];
                Quantity = (int)Convert.ToDouble(data["holdingsQuantity"]);
                //markToMarketPrice = data["markToMarketPrice"];
                symbolDescription = data["symbolDescription"];
                holdingsValue = data["holdingsValue"];
                holdingsQuantity = data["holdingsQuantity"];
                sellableQuantity = data["sellableQuantity"];
                //totalMarketToMarketPrice = data["totalMarketToMarketPrice"];
            }
            catch (Exception)
            {
                throw new DataException("Unable to parse data. " + Utils.JsonSerialize(data));
            }
        }

        public string Product { get; set; }
        public string Exchange { get; set; }
        public decimal Price { get; set; }
        public decimal LastPrice { get; set; }
        public int CollateralQuantity { get; set; }
        public decimal PNL { get; set; }
        public decimal ClosePrice { get; set; }
        public decimal AveragePrice { get; set; }
        public string TradingSymbol { get; set; }
       // public string ISIN { get; set; }
        public int Quantity { get; set; }
       // public string markToMarketPrice { get; set; }
        public string symbolDescription { get; set;  }
        public string holdingsValue { get; set; }
        public string holdingsQuantity { get; set; }
        public string sellableQuantity { get; set; }
       // public string totalMarketToMarketPrice { get; set; }
    }

    public struct InstrumentSamco
    {
        public InstrumentSamco(Dictionary<string, dynamic> data)
        {
            try
            {
                InstrumentToken = data["symbolCode"];
                TradingSymbol = data["tradingSymbol"];
                Name = data["name"];
                LastPrice = Convert.ToDecimal(data["lastPrice"]);
                //TickSize = Convert.ToDecimal(data["tickSize"]);
                Expiry = Utils.StringToDate(data["expiryDate"]);
                InstrumentType = data["instrument"];
                Segment = data["exchangeSegment"];
                Exchange = data["exchange"];
                if (data["strikePrice"] == "")
                    Strike = 0;
                else if (data["strikePrice"].Contains("E"))
                    Strike = Decimal.Parse(data["strikePrice"], System.Globalization.NumberStyles.Float);
                else
                    Strike = Convert.ToDecimal(data["strikePrice"]);

                if (data["tickSize"].Contains("E"))
                    TickSize = Decimal.Parse(data["tickSize"], System.Globalization.NumberStyles.Float);
                else
                    TickSize = Convert.ToDecimal(data["tickSize"]);

                LotSize = Convert.ToUInt32(data["lotSize"]);
            }
            catch (Exception)
            {
                throw new DataException("Unable to parse data. " + Utils.JsonSerialize(data));
            }

        }

        public InstrumentSamco(string v) : this()
        {
        }

        public string InstrumentToken { get; set; }
        public string TradingSymbol { get; set; }
        public string Name { get; set; }
        public decimal LastPrice { get; set; }
        public decimal TickSize { get; set; }
        public DateTime? Expiry { get; set; }
        public string InstrumentType { get; set; }
        public string Segment { get; set; }
        public string Exchange { get; set; }
        public decimal Strike { get; set; }
        public UInt32 LotSize { get; set; }

    }

    /// <summary>
    /// Historical structure
    /// </summary>
    public struct HistoricalSamco
    {
        public HistoricalSamco(Dictionary<string, dynamic> data)
        {
            TimeStamp = Convert.ToDateTime(data["dateTime"]);
            Open = Convert.ToDecimal(data["open"]);
            High = Convert.ToDecimal(data["high"]);
            Low = Convert.ToDecimal(data["low"]);
            Close = Convert.ToDecimal(data["close"]);
            Volume = Convert.ToUInt32(data["volume"]);
        }

        public DateTime TimeStamp { get; }
        public decimal Open { get; }
        public decimal High { get; }
        public decimal Low { get; }
        public decimal Close { get; }
        public UInt32 Volume { get; }
    }

    /// <summary>
    /// UserMargin structure
    /// </summary>
    public struct UserMarginSamco
    {
        public UserMarginSamco(Dictionary<string, dynamic> data)
        {
            try
            {
                Net = Convert.ToDecimal(data["netAvailableMargin"]);
                Available = Convert.ToDecimal(data["grossAvailableMargin"]);
                PayInToday = Convert.ToDecimal(data["payInToday"]);
                NotionalCash = Convert.ToDecimal(data["notionalCash"]);
                Utilised = Convert.ToDecimal(data["marginUsed"]);
            }
            catch (Exception)
            {
                throw new DataException("Unable to parse data. " + Utils.JsonSerialize(data));
            }
        }

        public decimal Net { get; set; }
        public decimal Available { get; set; }
        public decimal Utilised { get; set; }
        public decimal PayInToday { get; set; }
        public decimal NotionalCash { get; set; }


    }

    /// <summary>
    /// User margins response structure
    /// </summary>
    public struct UserMarginsResponseSamco
    {
        public UserMarginsResponseSamco(Dictionary<string, dynamic> data)
        {
            try
            {
                Equity = new UserMarginSamco(data["equityLimit"]);
                Commodity = new UserMarginSamco(data["commodityLimit"]);
            }
            catch (Exception)
            {
                throw new DataException("Unable to parse data. " + Utils.JsonSerialize(data));
            }
        }
        public UserMarginSamco Equity { get; set; }
        public UserMarginSamco Commodity { get; set; }
    }

    public struct OrdersPlacedSamco
    {
        public string TradingSymbol { get; set; }
        public string Exchange { get; set; }
        public string TransactionType { get; set; }
        public decimal TriggerPrice { get; set; }
        public decimal Price { get; set; }
        public string Product { get; set; }
        public int Quantity { get; set; }
        public string OrderType { get; set; }
    }

    //}
}
