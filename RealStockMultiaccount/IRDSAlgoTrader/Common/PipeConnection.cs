﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IRDSAlgoOMS.Common
{
    class PipeConnection
    {
        //*************server*************//
        private static int numThreads = 1;
        public static bool stopThread = false;
        public void serverStartMethod()
        {
            Thread servers = new Thread(() => ServerThread());
            servers.Start();
            Trace.WriteLine("serverStartMethod starts ***\n");
        }

        private static void ServerThread()
        {
            NamedPipeServerStream pipeServer = null;
            try
            {
                while (!stopThread)
                {
                    try
                    {
                        if (pipeServer == null)
                        {
                            pipeServer = new NamedPipeServerStream("testpipe", PipeDirection.InOut, numThreads);
                            Trace.WriteLine("Created new pipe");
                        }
                        // Wait for a client to connect
                        if (pipeServer.IsConnected == false)
                        {
                            pipeServer.WaitForConnection();
                            Trace.WriteLine("Client connected");
                        }
                        else
                        {
                            Trace.WriteLine("Client already connected");
                        }

                        if (pipeServer.IsConnected == true)
                        {
                            StreamString ss = new StreamString(pipeServer);
                            string line = ss.ReadString();
                            WPFMainControl.m_BridgeConnection = line;
                            Trace.WriteLine("********** WPFMainControl.m_BridgeConnection" + WPFMainControl.m_BridgeConnection);
                            Trace.WriteLine(line + " line from client");

                            //sanika::9-oct-2020::For write sqroff flag into pipe
                            var swrite = new StreamString(pipeServer);
                            ss.WriteString(WPFMainControl.m_isSqrOff.ToString());
                            Trace.WriteLine("flag write into pipe " + WPFMainControl.m_isSqrOff.ToString());
                        }
                    }
                    catch (Exception e)
                    {
                        if(pipeServer !=null)
                        {
                            Trace.WriteLine("Server excpetion " + e.Message);
                            if (pipeServer.IsConnected == true)
                            {
                                pipeServer.WaitForPipeDrain();
                                pipeServer.Disconnect();
                                Trace.WriteLine("Disconnecting server");
                            }
                            pipeServer.Close();
                            pipeServer.Dispose();
                            pipeServer = null;
                            WPFMainControl.m_BridgeConnection = "Disconnected";
                        }
                    }
                    Thread.Sleep(500);
                }
                if (stopThread == true)
                {
                    Trace.WriteLine("Server Thread stoppped");
                }
            }
            catch (Exception e)
            {
                Trace.WriteLine("Server excpetion " + e.Message);
                if (pipeServer != null)
                {
                    if (pipeServer.IsConnected == true)
                    {
                        pipeServer.WaitForPipeDrain();
                        pipeServer.Disconnect();
                        Trace.WriteLine("Disconnecting server");
                    }
                    pipeServer.Close();
                    pipeServer.Dispose();
                    pipeServer = null;
                    WPFMainControl.m_BridgeConnection = "Disconnected";
                }
            }
            finally
            {
                if (pipeServer != null)
                {
                    if (pipeServer.IsConnected == true)
                    {
                        pipeServer.WaitForPipeDrain();
                        pipeServer.Disconnect();
                    }
                    pipeServer.Close();
                    pipeServer.Dispose();
                    pipeServer = null;
                    Trace.WriteLine("Server closed");
                }
            }

        }


        //***********client*******************//       

        public void StartClientMethod()
        {
            Thread client = new Thread(() => ClientThread());
            client.Start();
            Trace.WriteLine("StartClientMethod startes ***\n");
        }

        public void ClientThread()
        {
            try
            {
                NamedPipeClientStream pipeClient = null;
                while (!stopThread)
                {
                    if (pipeClient == null)
                    {
                        Trace.WriteLine("Created new client pipe\n");
                        pipeClient = new NamedPipeClientStream(".", "testpipe",
                            PipeDirection.InOut, PipeOptions.None,
                            TokenImpersonationLevel.Impersonation);
                    }

                    Trace.WriteLine("Connecting to server...\n");
                    if (pipeClient.IsConnected == false)
                    {
                        Trace.WriteLine("Client connected to server \n");
                        pipeClient.Connect();
                    }
                    else
                    {
                        Trace.WriteLine("Client already connected to server \n");
                    }
                    while (!stopThread)
                    {
                        try
                        {
                            var ss = new StreamString(pipeClient);
                            ss.WriteString("Connected");
                            Thread.Sleep(4000);

                            //sanika::9-oct-2020::read sqroff symbol from pipe
                            StreamString ssread = new StreamString(pipeClient);
                            string line = ssread.ReadString();
                            if (line != null)
                            {
                                KiteConnectWrapper.m_isSqrOff = Convert.ToBoolean(line);
                                Trace.WriteLine("@@@From server " + line);
                            }
                            else
                            {
                                Trace.WriteLine("@@@From server " + line);
                            }
                        }
                        catch (Exception e)
                        {
                            Trace.WriteLine("Client exception " + e.Message);
                            pipeClient.Close();
                            pipeClient.Dispose();
                            pipeClient = null;
                            break;
                        }
                    }
                    if (pipeClient != null)
                    {
                        pipeClient.Close();
                        pipeClient.Dispose();
                    }
                    Trace.WriteLine("Closed client pipe");
                }
            }
            catch (Exception e)
            {
                Trace.WriteLine("Client exception " + e.Message);                
            }
        }
    }

    // Defines the data protocol for reading and writing strings on our stream
    public class StreamString
    {
        private Stream ioStream;
        private UnicodeEncoding streamEncoding;

        public StreamString(Stream ioStream)
        {
            this.ioStream = ioStream;
            streamEncoding = new UnicodeEncoding();
        }

        public string ReadString()
        {
            int len = 0;

            len = ioStream.ReadByte() * 256;
            len += ioStream.ReadByte();
            byte[] inBuffer = new byte[len];
            ioStream.Read(inBuffer, 0, len);
            Trace.WriteLine("############from clinet = " + streamEncoding.GetString(inBuffer));
            return streamEncoding.GetString(inBuffer);
        }

        public int WriteString(string outString)
        {
            byte[] outBuffer = streamEncoding.GetBytes(outString);
            int len = outBuffer.Length;
            if (len > UInt16.MaxValue)
            {
                len = (int)UInt16.MaxValue;
            }
            ioStream.WriteByte((byte)(len / 256));
            ioStream.WriteByte((byte)(len & 255));
            ioStream.Write(outBuffer, 0, len);
            ioStream.Flush();

            return outBuffer.Length + 2;
        }
    }
}
