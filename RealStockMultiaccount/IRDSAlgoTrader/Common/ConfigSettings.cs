﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;

namespace IRDSAlgoOMS
{
    public  class ConfigSettings
    {
        //IRDSPM::PRatiksha::19-09-2020::For reading encrpted file
        [DllImport("kernel32.dll", EntryPoint = "GetPrivateProfileStringA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int GetPrivateProfileStringreg(string lpApplicationName, string lpKeyName, string lpDefault, System.Text.StringBuilder lpReturnedString, int nSize, string lpFileName);
        static INIFile inif;

        public  string apiKey = "";
        public  string apiSecret = "";
        public  string username = "";
        public  string password = "";
        public  string root = "";
        public  string login = "";
        public  string ManualLogin = "";
        public string autoLogin = "";
        public string Pin = "";
        public List<string> Symbol = new List<string>();       
        public string StartTimeNSE = "";
        public string StartTimeNFO = "";
        public string StartTimeMCX = "";
        public string EndTimeNSE = "";
        public string EndTimeNFO = "";
        public string EndTimeMCX = "";
        public string TimeLimitToPlaceOrder = "";
        //public string TimeToExistAllOrder = "";
        public bool isShowGUI = false;
        public double m_overallProfit = 0;
        public double m_overallLoss = 0;
        public string m_brokerName = "";
        //IRDSPM::Pratiksha::10-11-2020::For New fields
        //sanika::22-Dec-2020::changed data type string to bool
        public bool CheckNSE = false;
        public bool CheckNFO = false;
        public bool CheckMCX = false;
        public bool CheckOptions = false;
        public bool CheckCDS = false;
        //Pratiksha:::01-12-2020::For new symbol
        public string StartTimeCDS = "";
        public string EndTimeCDS = "";
        //Pratiksha::02-12-2020::for exitTime
        public string ExitTimeNSE = "";
        public string ExitTimeNFO = "";
        public string ExitTimeMCX = "";
        public string ExitTimeCDS = "";

        //IRDSPM::Pratiksha::14-08-2020::Added new variables for Trader setting
        public bool isCalculateProfitLoss = false;
        public bool storeDataMysqlDB = false;
        public bool storeDataSqliteDB = false;
        public bool storeDataCSV = false;
        public bool historicalDataInMysqlDB = false;

        //sanika::8-dec-2020::Added for interval
        public int interval = 1;

        //IRDSPM::Pratiksha::20-10-2020::For Risk management on trader setting
        public bool isRiskmanagement = false;
        public string brokerQuantity = "";
        public string ManualQuantity = "";

        //IRDSPM::Pratiksha::26-10-2020::Added for Trading status
        public bool isTradingStatus = false;

        //IRDSPM::Pratiksha::23-11-2020::For Auto/manual Status for downalod token
        public string DownloadTokenWay = "";

        //IRDS::03-Jul-2020::Jyoti::added for database settings from ini file
        public string dbServer = "";
        public string dbUserid = "";
        public string dbPassword = "";
        public string databaseName = "";
        public string yob = "";

        //composite
        public string appKey = "";
        public string secretKey = "";
        public string marketDataAppKey = "";
        public string marketDataSecretKey = "";
        public int OrdersPerSymbolLimit = 0;
        public int OverallOrdersLimit = 0;
        public int SameOrdersLimit = 0;

        //IRDS::03-Jul-2020::Jyoti::added for Ajay Sir Changes
        public bool isTickMCXDataDownloader = false;

        //IRDS::31-Aug-2020::Sanika::For historical data settings
        public string apiKeyHistorical = "";
        public string apiSecretHistorical = "";
        public string usernameHistorical = "";
        public string passwordHistorical = "";
        public string rootHistorical = "";
        public string loginHistorical = "";
        public string PinHistorical = "";

        //IRDSPM::Pratiksha::07-09-2020::For setting ini file
        public string bgcolorname = "";
        public string fontcolorname = "";
        public string fontdet = "";
        public string fontstyle = "";
        public bool toastNotification = false;
        //IRDSPM::PRatiksha::16-09-2020::For minimized and maximized screen
        public string maximizedScreen = "";
        //IRDSPM::Pratiksha::16-09-2020::For reading market symbol
        public string mwsymbolname = "";
        public string mwltp = "";
        public string mwbid = "";
        public string mwask = "";
        public string mwvolume = "";
        //IRDSPM::Pratiksha::24-09-2020::for newly added variable
        public string mwOpen = "";
        public string mwHigh = "";
        public string mwLow = "";
        public string mwClose = "";
        public string mwchangeinper = "";
        //IRDSPM::Pratiksha::08-10-2020::Added new column "Net Change"
        public string mwNetChange = "";

        public bool toastAudio = false;
        //IRDSPM::PRatiksha::21-10-2020::For sound path
        public string toastAudioPath = "";
       // public bool showGUI = false;
        Logger m_Logger = null;
        //IRDSPM::Pratiksha::16-09-2020::For reading Open position
        public string opExchange = "";
        public string opSymbol = "";
        public string opTime = "";
        public string opOrderId = "";
        public string opProduct = "";
        public string opQuantity = "";
        public string opLTP = "";
        public string opPrice = "";
        public string opPL = "";

        //IRDSPM::Pratiksha::16-09-2020::For reading Order Book
        public string obEXG = "";
        public string obSymbol = "";
        public string obOrderId = "";
        public string obTime = "";
        public string obType = "";
        public string obOrderType = "";
        public string obProduct = "";
        public string obPrice = "";
        public string obTriggerPrice = "";
        public string obQTY = "";
        public string obStatus = "";
        public string obFilledQty = "";
        public string obFilledPrice = "";
        public string obRejectionOrderRemark = "";
        public string strExpiryDate = "";
        public string strName = "";
        //added for read date from registration.info
        public string strDate = "";

        //IRDSPM::Pratiksha::24-09-2020::For reading Order Holding
        public string ohEXG = "";
        public string ohSymbol = "";
        public string ohProduct = "";
        public string ohQuantity = "";
        public string ohcloseprice = "";
        public string ohavgprice = "";
        public string ohPL = "";

        //IRDSPM::Pratiksha::24-11-2020::New list for encrypt and decrypt
        List<string> DecryptDetList;
        List<string> EncDetList;
        //IRDSPM::Pratiksha::24-11-2020::add new list for enc data Samco
        List<string> DecryptDetListSamco;
        List<string> EncDetListSamco;
        //Pratiksha::18-01-2021::Composite
        List<string> DecryptDetListComposite;
        List<string> EncDetListComposite;
        public bool showGUIForOpenPositions = false;

        //IRDSPM::Pratiksha::12-05-2021::For notifications
        public bool isSend = false;
        public bool isSmsEnable = false;
        public bool isEmailEnable = false;
        public string smsNumber = "";
        public string emailid = "";
        public string sendinterval = "";
        //03-AUG-2021::Pratiksha::FOr other notification field
        public bool isOtherNotification = false;

        //IRDSPM::Pratiksha::08-02-2021::For title
        public string m_Title = "";

        //Pratiksha::15-07-2021::For smtp details
        public string m_senderName = "";
        public string m_senderemail = "";
        public string m_senderpassword = "";
        public string m_senderhost = "";
        public string m_senderPort = "";

        //IRDSPM::Pratiksha::15-07-2021::For smtp encrypt and decrypt
        List<string> SMTPDecryptDetList;
        List<string> SMTPEncDetList;
        public ConfigSettings()
        {
        }
        public ConfigSettings(Logger logger)
        {
            m_Logger = logger;
        }

        public  bool readConfigFile(string filename)
        {
            EncDetList = new List<string>();
            bool isRead = false;
            try
            {
                inif = new INIFile(filename);

                root = inif.IniReadValue("credentials", "root");
                login = inif.IniReadValue("credentials", "login");

                //IRDSPM::Pratiksha::24-11-2020::For reading and decrypting details --start
                string api = inif.IniReadValue("credentials", "MyAPIKey");
                string secret = inif.IniReadValue("credentials", "MySecret");
                string userid = inif.IniReadValue("credentials", "MyUserId");
                string pwd = inif.IniReadValue("credentials", "MyPassword");
                string pin = inif.IniReadValue("credentials", "Pin");

                if (api != "XXXXXX" && secret != "XXXXXX" && userid != "XXXXXX" && pwd != "XXXXXX" && pin != "XXXXXX")
                {
                    EncDetList.Add(api); EncDetList.Add(secret); EncDetList.Add(userid); EncDetList.Add(pwd); EncDetList.Add(pin);
                    decryptDet(EncDetList);
                    apiKey = DecryptDetList[0];
                    apiSecret = DecryptDetList[1];
                    username = DecryptDetList[2];
                    password = DecryptDetList[3];
                    Pin = DecryptDetList[4];
                }
                else
                {
                    apiKey = api;
                    apiSecret = secret;
                    username = userid;
                    password = pwd;
                    Pin = pin;
                }
                
                //IRDSPM::Pratiksha::24-11-2020::For reading and decrypting details --end

                //IRDSPM::Pratiksha::10-11-2020::For Autologin
                autoLogin = inif.IniReadValue("credentials", "autoLogin");                      
                //IRDSPM::PRatiksha::02-10-2020::this flaag not available not available on apisetting form
               // isShowGUI = Convert.ToBoolean(inif.IniReadBoolValue("credentials", "showGUIForOpenPositions"));                              
                isTickMCXDataDownloader = Convert.ToBoolean(inif.IniReadBoolValue("credentials", "isTickMCXDataDownloader"));
                List<string> iromsRealtimeSymbol = new List<string>(); 
                if (isTickMCXDataDownloader)
                {
                    ReadSymbolList();
                }
                else
                {
                    ReadSymbolList();
                }
                isRead = true;

                WriteUniquelogs("ReadConfigSettings", "readTradingINIFile : isTickMCXDataDownloader = " + isTickMCXDataDownloader + " autoLogin= " + autoLogin, MessageType.Informational);
            }
            catch(Exception e)
            {
                WriteUniquelogs("ReadConfigSettings","readConfigFile : Exception Error Message = " +e.Message, MessageType.Exception);
                isRead = false;
            }
            return isRead;
        }
        public void writeINIFile(string section,string key,string value)
        {
            try
            {
                inif.IniWriteValue(section, key, value);
            }
            catch(Exception e)
            {
                WriteUniquelogs("ReadConfigSettings","writeINIFile : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        public void readCredentailsForHistoricalData(string filename)
        {
            bool isRead = false;
            try
            {
                inif = new INIFile(filename);
                rootHistorical = inif.IniReadValue("credentials", "root");
                loginHistorical = inif.IniReadValue("credentials", "login");                
                apiKeyHistorical = inif.IniReadValue("credentials", "MyAPIKey");
                apiSecretHistorical = inif.IniReadValue("credentials", "MySecret");
                usernameHistorical = inif.IniReadValue("credentials", "MyUserId");
                passwordHistorical = inif.IniReadValue("credentials", "MyPassword");
                PinHistorical = inif.IniReadValue("credentials", "Pin");
                isRead = true;
            }
            catch (Exception e)
            {
                isRead = false;
                WriteUniquelogs("ReadConfigSettings", "readCredentailsForHistoricalData : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        public void readCompositeConfigFile (string filename)
        {
            bool isRead = false;
            try
            {
                EncDetListComposite = new List<string>();
                inif = new INIFile(filename);
                root = inif.IniReadValue("credentials", "root");
                //username = inif.IniReadValue("credentials", "username");
                //appKey = inif.IniReadValue("credentials", "appKey");
                //secretKey = inif.IniReadValue("credentials", "secretKey");
                //marketDataAppKey = inif.IniReadValue("credentials", "MarketDataappKey");
                //marketDataSecretKey = inif.IniReadValue("credentials", "MarketDatasecretKey");
                //Pratiksha::01-01-2021::For Composite -start
                string CName = inif.IniReadValue("credentials", "MyUserId");
                string CappKey = inif.IniReadValue("credentials", "appKey");
                string CsecretKey = inif.IniReadValue("credentials", "secretKey");
                string CmarketDataAppKey = inif.IniReadValue("credentials", "MarketDataappKey");
                string CmarketDataSecretKey = inif.IniReadValue("credentials", "MarketDatasecretKey");
                showGUIForOpenPositions = Convert.ToBoolean(inif.IniReadBoolValue("credentials", "showGUIForOpenPositions"));
                if (CName != "XXXXXX" && CappKey != "XXXXXX" && CsecretKey != "XXXXXX" && CmarketDataAppKey != "XXXXXX" && CmarketDataSecretKey != "XXXXXX")
                {
                    EncDetListComposite.Add(CName); EncDetListComposite.Add(CappKey); EncDetListComposite.Add(CsecretKey);
                    EncDetListComposite.Add(CmarketDataAppKey); EncDetListComposite.Add(CmarketDataSecretKey);
                    decryptDetComposite(EncDetListComposite);

                    username = DecryptDetListComposite[0];
                    appKey = DecryptDetListComposite[1];
                    secretKey = DecryptDetListComposite[2];
                    marketDataAppKey = DecryptDetListComposite[3];
                    marketDataSecretKey = DecryptDetListComposite[4];
                }
                else
                {
                    username = CName;
                    appKey = CappKey;
                    secretKey = CsecretKey;
                    marketDataAppKey = CmarketDataAppKey;
                    marketDataSecretKey = CmarketDataSecretKey;
                }
                ReadSymbolList();
                isRead = true;
            }
            catch(Exception e)
            {
                isRead = false;
                WriteUniquelogs("ReadConfigSettings","readCompositeConfigFile : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        public void readSamcoConfigFile(string filename)
        {
            bool isRead = false;
            try
            {
                inif = new INIFile(filename);
                EncDetListSamco = new List<string>();
                root = inif.IniReadValue("credentials", "root");
                //username = inif.IniReadValue("credentials", "MyUserId");
                //password = inif.IniReadValue("credentials", "MyPassword");
                //yob = inif.IniReadValue("credentials", "YOB");
                //ManualLogin = inif.IniReadValue("credentials", "manualLogin");
                //IRDSPM::Pratiksha::27-11-2020::For encryption change samco
                string user = inif.IniReadValue("credentials", "MyUserId");
                string pwd = inif.IniReadValue("credentials", "MyPassword");
                string pin = inif.IniReadValue("credentials", "Pin");
                if (user != "XXXXXX" && pwd != "XXXXXX" && pin != "XXXXXX")
                {
                    EncDetListSamco.Add(user); EncDetListSamco.Add(pwd); EncDetListSamco.Add(pin);
                    decryptDetSamco(EncDetListSamco);
                    username = DecryptDetListSamco[0];
                    password = DecryptDetListSamco[1];
                    yob = DecryptDetListSamco[2];
                }
                else
                {
                    username = user;
                    password = pwd;
                    yob = pin;
                }
                isTickMCXDataDownloader = Convert.ToBoolean(inif.IniReadBoolValue("credentials", "isTickMCXDataDownloader"));               
                List<string> iromsRealtimeSymbol = new List<string>();
                if (isTickMCXDataDownloader)
                {
                    ReadSymbolList();
                }
                else
                {
                    ReadSymbolList();
                }
                isRead = true;
            }
            catch (Exception e)
            {
                isRead = false;
                WriteUniquelogs("ReadConfigSettings","readSamcoConfigFile : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }
        public void decryptDetSamco(List<string> EncDetListSamco)
        {
            DecryptDetListSamco = new List<string>();
            for (int i = 0; i < EncDetListSamco.Count; i++)
            {
                string plaintext = DecryptData(EncDetListSamco[i], "IrdsalgoTrader");
                DecryptDetListSamco.Add(plaintext);
            }
        }
        public void readTradingINIFile(string filename)
        {
            bool isRead = false;
            try
            {
                inif = new INIFile(filename);
                //NSE
                StartTimeNSE = inif.IniReadValue("TRADINGSETTING", "startTimeNSE");
                EndTimeNSE = inif.IniReadValue("TRADINGSETTING", "endTimeNSE");
                ExitTimeNSE = inif.IniReadValue("TRADINGSETTING", "exitTimeNSE");
                //NFO
                StartTimeNFO = inif.IniReadValue("TRADINGSETTING", "startTimeNFO");
                EndTimeNFO = inif.IniReadValue("TRADINGSETTING", "endTimeNFO");
                ExitTimeNFO = inif.IniReadValue("TRADINGSETTING", "exitTimeNFO");
                //MCX
                StartTimeMCX = inif.IniReadValue("TRADINGSETTING", "startTimeMCX");
                EndTimeMCX = inif.IniReadValue("TRADINGSETTING", "endTimeMCX");
                ExitTimeMCX = inif.IniReadValue("TRADINGSETTING", "exitTimeMCX");

                //Pratikha::01-12-2020::For new symbol CDS
                StartTimeCDS = inif.IniReadValue("TRADINGSETTING", "startTimeCDS");
                EndTimeCDS = inif.IniReadValue("TRADINGSETTING", "endTimeCDS");
                ExitTimeCDS = inif.IniReadValue("TRADINGSETTING", "exitTimeCDS");

                TimeLimitToPlaceOrder = inif.IniReadValue("TRADINGSETTING", "TimeLimitToPlaceOrder");
                //TimeToExistAllOrder = inif.IniReadValue("TRADINGSETTING", "TimeForExitAllOpenPosition");    
                //IRDSPM::Pratiksha::28-07-2020::Assign the values to overallloss to loss       
                m_overallLoss = Convert.ToDouble(inif.IniReadDoubleValue("TRADINGSETTING", "overallLoss"));
                m_overallProfit = Convert.ToDouble(inif.IniReadDoubleValue("TRADINGSETTING", "overallProfit"));
                //IRDSPM::Pratiksha::18-08-2020::New symbols for trader setting
                isCalculateProfitLoss = Convert.ToBoolean(inif.IniReadBoolValue("TRADINGSETTING", "calculateProfitLoss"));
                storeDataMysqlDB = Convert.ToBoolean(inif.IniReadBoolValue("TRADINGSETTING", "storeDataMysqlDB"));
                storeDataSqliteDB = Convert.ToBoolean(inif.IniReadBoolValue("TRADINGSETTING", "storeDataSqliteDB"));
                storeDataCSV = Convert.ToBoolean(inif.IniReadBoolValue("TRADINGSETTING", "storeDataCSV"));
                historicalDataInMysqlDB = Convert.ToBoolean(inif.IniReadBoolValue("TRADINGSETTING", "historicalDataInMysqlDB"));


                //sanika::8-Dec-2020::Added for interval of data
                interval = Convert.ToInt32(inif.IniReadDoubleValue("TRADINGSETTING", "Interval"));

                //IRDSPM::Pratiksha::20-10-2020::For Risk management on trader setting
                isRiskmanagement = Convert.ToBoolean(inif.IniReadBoolValue("TRADINGSETTING", "riskManagement"));
                brokerQuantity = inif.IniReadValue("TRADINGSETTING", "brokerQuantity");
                ManualQuantity = inif.IniReadValue("TRADINGSETTING", "ManualQuantity");

                //IRDSPM::Pratiksha::26-10-2020::Added for Trading status
                isTradingStatus = Convert.ToBoolean(inif.IniReadBoolValue("TRADINGSETTING", "TradingStatus"));

                //IRDSPM::Pratiksha::23-11-2020::For Auto/manual Status for downalod token
                DownloadTokenWay = inif.IniReadValue("TRADINGSETTING", "downloadTokenWay");
                //IRDSPM::Pratiksha::10-11-2020::For new fields              
                CheckNSE = Convert.ToBoolean(inif.IniReadBoolValue("TRADINGSETTING", "CheckNSE"));
                CheckNFO = Convert.ToBoolean(inif.IniReadBoolValue("TRADINGSETTING", "CheckNFO"));
                CheckMCX = Convert.ToBoolean(inif.IniReadBoolValue("TRADINGSETTING", "CheckMCX"));
                CheckOptions = Convert.ToBoolean(inif.IniReadBoolValue("TRADINGSETTING", "CheckOptions"));
                CheckCDS = Convert.ToBoolean(inif.IniReadBoolValue("TRADINGSETTING", "CheckCDS"));
                if (StartTimeNSE == "")
                    StartTimeNSE = "09:15:00";
                if(StartTimeNFO == "")
                    StartTimeNFO = "09:15:00";
                if (StartTimeMCX == "")
                    StartTimeMCX = "08:55:00";
                //sanika::18-Nov-2020::changed end time exchange wise
                if (EndTimeNSE == "")
                    EndTimeNSE = "15:30:00";
                if (EndTimeNFO == "")
                    EndTimeNFO = "15:30:00";
                if (EndTimeMCX == "")
                    EndTimeMCX = "23:59:00";
                if (TimeLimitToPlaceOrder == "")
                    TimeLimitToPlaceOrder = "15:15:00";
                if (ExitTimeNSE == "")
                    ExitTimeNSE = "15:19:00";
                isRead = true;

                WriteUniquelogs("ReadConfigSettings", "readTradingINIFile : StartTimeNSE = " + StartTimeNSE + " StartTimeNFO= "+ StartTimeNFO+ " StartTimeMCX= "+ StartTimeMCX+ " EndTimeNSE = " + EndTimeNSE + " EndTimeNFO "+ EndTimeNFO + " EndTimeMCX "+ EndTimeMCX +" TimeLimitToPlaceOrder = " + TimeLimitToPlaceOrder+ " ExitTimeNSE " + ExitTimeNSE, MessageType.Informational);
                WriteUniquelogs("ReadConfigSettings", "readTradingINIFile : m_overallLoss = " + m_overallLoss + " m_overallProfit= " + m_overallProfit + " isCalculateProfitLoss= " + isCalculateProfitLoss + " historicalDataInMysqlDB = " + historicalDataInMysqlDB, MessageType.Informational);
                WriteUniquelogs("ReadConfigSettings", "readTradingINIFile : storeDataMysqlDB = " + storeDataMysqlDB + " storeDataSqliteDB= " + storeDataSqliteDB + " storeDataCSV= " + storeDataCSV , MessageType.Informational);

                OrdersPerSymbolLimit = Convert.ToInt32(inif.IniReadDoubleValue("TRADINGSETTING", "OrdersPerSymbol"));
                OverallOrdersLimit = Convert.ToInt32(inif.IniReadDoubleValue("TRADINGSETTING", "OverallOrders"));
                SameOrdersLimit = Convert.ToInt32(inif.IniReadDoubleValue("TRADINGSETTING", "SameOrdersLimit"));

            }
            catch (Exception e)
            {
                isRead = false;
                WriteUniquelogs("ReadConfigSettings","readTradingINIFile : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        public bool readDBSettingINIFile(string filename)
        {
            bool isRead = false;
            try
            {
                //IRDS::03-Jul-2020::Jyoti::added for database settings from ini file
                inif = new INIFile(filename);
                dbServer = inif.IniReadValue("databaseSettings", "server");
                dbUserid = inif.IniReadValue("databaseSettings", "userid");
                dbPassword = inif.IniReadValue("databaseSettings", "password");
                databaseName = inif.IniReadValue("databaseSettings", "database");
                WriteUniquelogs("ReadConfigSettings", "readDBSettingINIFile : dbServer = " + dbServer + " dbUserid= " + dbUserid + " dbPassword= " + dbPassword + " databaseName = " + databaseName , MessageType.Informational);
                isRead = true;
            }
            catch(Exception e)
            {
                isRead = true;
                WriteUniquelogs("ReadConfigSettings", "Data Downloaded", MessageType.Informational);
            }
            return isRead;
        }

        //public bool readHistoricalDataSettingINIFile(string filename)
        //{
        //    bool isRead = false;
        //    try
        //    {
        //        inif = new INIFile(filename);
        //        rootHistorical = inif.IniReadValue("credentials", "root");
        //        loginHistorical = inif.IniReadValue("credentials", "login");
        //        apiKeyHistorical = inif.IniReadValue("credentials", "MyAPIKey");
        //        apiSecretHistorical = inif.IniReadValue("credentials", "MySecret");
        //        usernameHistorical = inif.IniReadValue("credentials", "MyUserId");
        //        passwordHistorical = inif.IniReadValue("credentials", "MyPassword");
        //        PinHistorical = inif.IniReadValue("credentials", "Pin");
        //        isRead = true;
        //    }
        //    catch (Exception e)
        //    {
        //        isRead = true;
        //    }
        //    return isRead;
        //}

        public void WriteUniquelogs(string symbol, string message, MessageType msgType)
        {
            try
            {
                SymbolLogs log = new SymbolLogs();
                //log = SymbolLogs.InstanceSymbolLogs;
                log.createFile(symbol);
                log.LogMessage(message, MessageType.Informational);
                log = null;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : " + e.Message);
                Trace.WriteLine("Exception : " + e.Message);
            }
        }
        //IRDSPM::Pratiksha::07-09-2020::To read setting ini file --start
        public bool ReadSettingConfigFile(string filename)
        {
            SMTPEncDetList = new List<string>();
            bool isload = true;
            try
            {
                inif = new INIFile(filename);
                toastNotification = Convert.ToBoolean(inif.IniReadBoolValue("TOASTSETTING", "toastNotification"));
                toastAudio = Convert.ToBoolean(inif.IniReadBoolValue("TOASTSETTING", "toastAudio"));
                toastAudioPath = inif.IniReadValue("TOASTSETTING", "toastAudioPath");
                isSend = Convert.ToBoolean(inif.IniReadBoolValue("Notifications", "isSend"));
                isSmsEnable = Convert.ToBoolean(inif.IniReadBoolValue("Notifications", "isSmsEnable"));
                isEmailEnable = Convert.ToBoolean(inif.IniReadBoolValue("Notifications", "isEmailEnable"));
                smsNumber = inif.IniReadValue("Notifications", "smsNumber");
                emailid = inif.IniReadValue("Notifications", "emailid");
                sendinterval = inif.IniReadValue("Notifications", "interval");
                //03-AUG-2021::Pratiksha::FOr other notification field
                isOtherNotification = Convert.ToBoolean(inif.IniReadBoolValue("Notifications", "OtherNotification"));

                isShowGUI = Convert.ToBoolean(inif.IniReadBoolValue("GUI", "showGUIForOpenPositions"));
                m_brokerName = inif.IniReadValue("BROKER", "brokername");
                maximizedScreen = inif.IniReadValue("screenMode", "screenMode");

                //Market watch
                mwsymbolname = inif.IniReadValue("MarketWatchColumn", "Symbol");
                mwltp = inif.IniReadValue("MarketWatchColumn", "LTP");
                mwbid = inif.IniReadValue("MarketWatchColumn", "BidPrice");
                mwask = inif.IniReadValue("MarketWatchColumn", "AskPrice");
                mwvolume = inif.IniReadValue("MarketWatchColumn", "Volume");
                mwOpen = inif.IniReadValue("MarketWatchColumn", "Open");
                mwHigh = inif.IniReadValue("MarketWatchColumn", "High");
                mwLow = inif.IniReadValue("MarketWatchColumn", "Low");
                mwClose = inif.IniReadValue("MarketWatchColumn", "Close");
                mwchangeinper = inif.IniReadValue("MarketWatchColumn", "Changein%");
                mwNetChange = inif.IniReadValue("MarketWatchColumn", "NetChange");

                //Open position
                opExchange = inif.IniReadValue("OpenPositionColumn", "Exchange");
                opSymbol = inif.IniReadValue("OpenPositionColumn", "Symbol");
                opProduct = inif.IniReadValue("OpenPositionColumn", "Product");
                opQuantity = inif.IniReadValue("OpenPositionColumn", "Quantity");
                opLTP = inif.IniReadValue("OpenPositionColumn", "LTP");
                opPrice = inif.IniReadValue("OpenPositionColumn", "Price");
                opPL = inif.IniReadValue("OpenPositionColumn", "P&L");

                //Order Book
                obEXG = inif.IniReadValue("OrderBookColumn", "EXG");
                obSymbol = inif.IniReadValue("OrderBookColumn", "Symbol");
                obOrderId = inif.IniReadValue("OrderBookColumn", "OrderId");
                obTime = inif.IniReadValue("OrderBookColumn", "Time");
                obType = inif.IniReadValue("OrderBookColumn", "Type");
                obOrderType = inif.IniReadValue("OrderBookColumn", "OrderType");
                obProduct = inif.IniReadValue("OrderBookColumn", "Product");
                obPrice = inif.IniReadValue("OrderBookColumn", "Price");
                obTriggerPrice = inif.IniReadValue("OrderBookColumn", "TriggerPrice");
                obQTY = inif.IniReadValue("OrderBookColumn", "QTY");
                obStatus = inif.IniReadValue("OrderBookColumn", "Status");
                obFilledQty = inif.IniReadValue("OrderBookColumn", "FilledQty");
                obFilledPrice = inif.IniReadValue("OrderBookColumn", "FilledPrice");
                obRejectionOrderRemark = inif.IniReadValue("OrderBookColumn", "RejectionOrderRemark");

                //open Holding
                ohEXG = inif.IniReadValue("OpenHoldingsColumn", "Exchange");
                ohSymbol = inif.IniReadValue("OpenHoldingsColumn", "Symbol");
                ohProduct = inif.IniReadValue("OpenHoldingsColumn", "Product");
                ohQuantity = inif.IniReadValue("OpenHoldingsColumn", "Quantity");
                ohcloseprice = inif.IniReadValue("OpenHoldingsColumn", "closePrice");
                ohavgprice = inif.IniReadValue("OpenHoldingsColumn", "avgPrice");
                ohPL = inif.IniReadValue("OpenHoldingsColumn", "P&L");
                //IRDSPM::Pratiksha::08-02-2021::For title
                m_Title = inif.IniReadValue("Title", "title");

                //IRDSPM::Pratiksha::29-06-2021::For smtp details
                string name = inif.IniReadValue("SMTP", "senderName");
                string email = inif.IniReadValue("SMTP", "senderemail");
                string pwd = inif.IniReadValue("SMTP", "senderpassword");
                string host = inif.IniReadValue("SMTP", "senderhost");
                string port = inif.IniReadValue("SMTP", "senderPort");

                if (name != "XXXXXX" && email != "XXXXXX" && pwd != "XXXXXX" && host != "XXXXXX" && port != "XXXXXX")
                {
                    SMTPEncDetList.Add(name); SMTPEncDetList.Add(email); SMTPEncDetList.Add(pwd); SMTPEncDetList.Add(host); SMTPEncDetList.Add(port);
                    decryptDetSMTP(SMTPEncDetList);
                    m_senderName = SMTPDecryptDetList[0];
                    m_senderemail = SMTPDecryptDetList[1];
                    m_senderpassword = SMTPDecryptDetList[2];
                    m_senderhost = SMTPDecryptDetList[3];
                    m_senderPort = SMTPDecryptDetList[4];
                }
                else
                {
                    m_senderName = name;
                    m_senderemail = email;
                    m_senderpassword = pwd;
                    m_senderhost = host;
                    m_senderPort = port;
                }

                //m_senderName = inif.IniReadValue("SMTP", "senderName");
                //m_senderemail = inif.IniReadValue("SMTP", "senderemail");
                //m_senderpassword = inif.IniReadValue("SMTP", "senderpassword");
                //m_senderhost = inif.IniReadValue("SMTP", "senderhost");
                //m_senderPort = inif.IniReadValue("SMTP", "senderPort");
            }
            catch (Exception ex)
            {
                WriteUniquelogs("ReadSettingConfigFile", "read Setting ConfigFile - exception" + ex.Message, MessageType.Informational);
            }
            return isload;
        }

        //IRDS::Jyoti::12-Sept-20::Added for symbol settings form
        public void ReadSymbolList()
        {
            List<string> iromsRealtimeSymbol = new List<string>();

            var path = Directory.GetCurrentDirectory();
            string iniFile = path + @"\Configuration\" + "symbollist.ini";
            INIFile inifSymbols = new INIFile(iniFile);
            iromsRealtimeSymbol = inifSymbols.GetKeyValues("RealTimeSymbols");

            for (int i = 0; i < iromsRealtimeSymbol.Count; i++)
            {
                string temp = inifSymbols.IniReadValue("RealTimeSymbols", i.ToString());
                Symbol.Add(temp);

            }
        }

        //IRDS::Jyoti::12-Sept-20::Added for symbol settings form
        public void WriteSymbolList(string section, string key, string value)
        {
            try
            {
                var path = Directory.GetCurrentDirectory();
                string iniFile = path + @"\Configuration\" + "symbollist.ini";
                INIFile inifSymbols = new INIFile(iniFile);
                inifSymbols.IniWriteValue(section, key, value);
            }
            catch (Exception e)
            {
                WriteUniquelogs("ReadConfigSettings", "writeINIFile : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        //IRDSPM::Pratiksha::19-09-2020::For decrypting data for registration form
        public static string DecryptData(string strCryptTxt, string strKey)
        {
            strCryptTxt = strCryptTxt.Replace(" ", "+");

            byte[] bytesBuff = Convert.FromBase64String(strCryptTxt);

            using (Aes aes__1 = Aes.Create())
            {
                Rfc2898DeriveBytes crypto = new Rfc2898DeriveBytes(strKey, new byte[] {
                0x49,
                0x76,
                0x61,
                0x6e,
                0x20,
                0x4d,
                0x65,
                0x64,
                0x76,
                0x65,
                0x64,
                0x65,
                0x76
            });
                aes__1.Key = crypto.GetBytes(32);
                aes__1.IV = crypto.GetBytes(16);

                using (MemoryStream mStream = new MemoryStream())
                {
                    using (CryptoStream cStream = new CryptoStream(mStream, aes__1.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cStream.Write(bytesBuff, 0, bytesBuff.Length);
                        cStream.Close();
                    }

                    strCryptTxt = Encoding.Unicode.GetString(mStream.ToArray());
                }
            }
            return strCryptTxt;
        }
        public void ReadRegistrationInfoFile()
        {
            string filename = "RegCredentials.INFO";
            inif = new INIFile(filename);

            // root = inif.IniReadValue("credentials", "root");
            var path = Directory.GetCurrentDirectory();
            string strIniFilePath = path + @"\Configuration\" + filename;
            if (System.IO.File.Exists(strIniFilePath))
            {
                strExpiryDate = DecryptData(ReadINI(strIniFilePath, "X1eaJ0kA6ril", "LFY8V3Y0YQe", ""), "IrdsalgoTrader").Trim();
                strName = DecryptData(ReadINI(strIniFilePath, "X1eaJ0kA6ril", "0xiW6AjUO8", ""), "IrdsalgoTrader").Trim();
                strDate = DecryptData(ReadINI(strIniFilePath, "X1eaJ0kA6ril", "0xiW6Ajwsa", ""), "IrdsalgoTrader").Trim();
            }
        }
        public string ReadINI(string INIFile, string Section, string Key, string sDefault)
        {
            string strReturnReadINI = string.Empty;
            int intCharCount = 0;
            System.Text.StringBuilder objResult = new System.Text.StringBuilder(256);

            intCharCount = GetPrivateProfileStringreg(Section, Key, sDefault, objResult, objResult.Capacity, INIFile);

            if (intCharCount > 0) strReturnReadINI = objResult.ToString().Substring(0, intCharCount);
            else strReturnReadINI = string.Empty;

            return strReturnReadINI;
        }
        //IRDSPM::Pratiksha::24-11-2020::to decypt details
        public void decryptDet(List<string> EncDetList)
        {
            try
            {
                DecryptDetList = new List<string>();
                for (int i = 0; i < EncDetList.Count; i++)
                {
                    string plaintext = DecryptData(EncDetList[i], "IrdsalgoTrader");
                    DecryptDetList.Add(plaintext);
                }
            }
            catch(Exception e)
            {
                WriteUniquelogs("ReadConfigSettings", "decryptDet : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }


        //IRDSPM::Pratiksha::24-11-2020::to decypt details
        public void decryptDetSMTP(List<string> EncDetList)
        {
            try
            {
                SMTPDecryptDetList = new List<string>();
                for (int i = 0; i < EncDetList.Count; i++)
                {
                    string plaintext = DecryptData(EncDetList[i], "IrdsalgoTrader");
                    SMTPDecryptDetList.Add(plaintext);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs("ReadConfigSettings", "decryptDet : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }


        //Pratiksha::01-01-2021::For Composite
        public void decryptDetComposite(List<string> EncDetList)
        {
            try
            {
                DecryptDetListComposite = new List<string>();
                for (int i = 0; i < EncDetList.Count; i++)
                {
                    string plaintext = DecryptData(EncDetList[i], "IrdsalgoTrader");
                    DecryptDetListComposite.Add(plaintext);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs("ReadConfigSettings", "decryptDetComposite : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }
    }
}
