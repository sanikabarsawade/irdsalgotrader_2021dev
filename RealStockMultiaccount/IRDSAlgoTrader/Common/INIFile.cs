﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace IRDSAlgoOMS
{
    class INIFile
    {
        public string path;
        public Dictionary<string, string> dictionary = new Dictionary<string, string>();

        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);

        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);

        [DllImport("KERNEL32.DLL", EntryPoint = "GetPrivateProfileSectionW",
            SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        private static extern int GetPrivateProfileSection(string lpAppName, string lpReturnedString, int nSize, string lpFileName);

        public INIFile(string v)
        {
            path = v;
        }

        private static Logger log;

        /// Write Data to the INI File
        public void IniWriteValue(string Section, string Key, string Value)
        {
            WritePrivateProfileString(Section, Key, Value, this.path);
        }

        /// Read Data Value From the Ini File
        public string IniReadValue(string Section, string Key)
        {
            StringBuilder temp = new StringBuilder(255);
            int i = GetPrivateProfileString(Section, Key, "", temp, 255, this.path);
            return temp.ToString();
        }

        public string IniReadDoubleValue(string Section, string Key)
        {
            StringBuilder temp = new StringBuilder(255);
            int i = GetPrivateProfileString(Section, Key, "0", temp, 255, this.path);
            return temp.ToString();
        }
        public string IniReadBoolValue(string Section, string Key)
        {
            StringBuilder temp = new StringBuilder(255);
            int i = GetPrivateProfileString(Section, Key, "false", temp, 255, this.path);
            return temp.ToString();
        }


        private static readonly string[] m_strLineComments = new string[] { "#", ";" };
        public Dictionary<string, string> ReadAllSection(string strFileSource, string strSection)
        {

            //Grab Contents of section

            string strContents = GetSection(ReadFile(strFileSource), strSection);

            string[] strKeyPairs, strKeyValuePair;

            //Grab index of the section

            int intSectionPos = strContents.IndexOf("[" + strSection + "]", StringComparison.InvariantCulture);

            if (intSectionPos == -1)

                throw new Exception("Section strFileSource Found");

            strKeyPairs = strContents.Substring(("[" + strSection + "]").Length).Split(new[] { "\r", "\n" }, StringSplitOptions.None);

            foreach (string strKeyPair in strKeyPairs)
            {

                strKeyValuePair = strKeyPair.Split(new[] { "=" }, StringSplitOptions.RemoveEmptyEntries);

                if (strKeyValuePair.Length > 0)

                    if (strKeyValuePair.Length == 2)

                        dictionary[strKeyValuePair[0]] = strKeyValuePair[1];

                    else

                        dictionary[strKeyValuePair[0]] = string.Empty;
            }
            return dictionary;
        }

        public Dictionary<string, string> GetKeyValuesPair(string section)
        {
            log = Logger.Instance;
            List<string> tlist = GetKeyValues(section);
            Dictionary<string, string> tdic = new Dictionary<string, string>();
            if (tlist == null || !tlist.Any()) return tdic;
            foreach (var str in tlist)
            {
                if (string.IsNullOrEmpty(str)) continue;
                var vl = str.Split('=');
                if (tdic.ContainsKey(vl[0]))
                {
                    log.LogMessage("\nDuplicate value found in Symbol", MessageType.Exception);
                }
                else
                {
                    tdic.Add(vl[0], vl[1]);
                }
            }
            return tdic;
        }

        public Dictionary<string, string> WriteKeyValuePair(string section, string key)
        {
            log = Logger.Instance;
            List<string> tlist = GetKeyValues(section);
            Dictionary<string, string> tdic = new Dictionary<string, string>();
            if (tlist == null || !tlist.Any()) return tdic;
            foreach (var str in tlist)
            {
                if (string.IsNullOrEmpty(str)) continue;
                var vl = key;
                if (tdic.ContainsKey(vl))
                {
                    log.LogMessage("\nDuplicate value found in Symbol", MessageType.Exception);
                }
                else
                {
                    tdic.Add(vl, "1");
                    WritePrivateProfileString(section, key, "1", this.path);
                }
            }
            return tdic;
        }

        public List<string> GetKeyValues(string section)
        {
            string returnString = new string(' ', 65536);
            GetPrivateProfileSection(section, returnString, 65536, this.path);
            List<string> result = new List<string>(returnString.Split('\0'));
            result.RemoveRange(result.Count - 2, 2);
            return result;
        }

        private string ReadFile(string strFilePath)
        {

            StringBuilder sbInput = new StringBuilder();

            using (StreamReader sr = new StreamReader(strFilePath))
            {
                sbInput.Append(sr.ReadToEnd());
            }

            return CleanComments(sbInput.ToString()); //And clean the comments
        }

        private string CleanComments(string strSource)
        {

            StringBuilder sbResponse = new StringBuilder();

            string[] strBuffer;

            //Propigate Line

            string[] strLines = strSource.Split(new[] { "\r", "\n" }, StringSplitOptions.RemoveEmptyEntries);

            //Process Each Item in array

            foreach (string strLine in strLines)
            {

                if (!m_strLineComments.Contains(strLine.Trim()[0].ToString(CultureInfo.InvariantCulture)))
                {

                    strBuffer = strLine.Split(m_strLineComments, StringSplitOptions.RemoveEmptyEntries);

                    if (strBuffer.Length > 1)
                    {
                        sbResponse.Append(strBuffer[0]);
                    }
                    else
                    {
                        sbResponse.Append(strLine);
                    }

                    sbResponse.Append("\r\n");
                }
            }
            return sbResponse.ToString();
        }

        private string GetSection(string strSource, string strSection)
        {

            //Grab index of the section

            int intSectionPos = strSource.IndexOf("[" + strSection + "]", StringComparison.InvariantCulture);

            if (intSectionPos == -1)

                throw new Exception("Section Not Found");

            //We know the end of the section, either by another [, or by the EOF

            int intEndSectionPos = strSource.IndexOf("[", intSectionPos + 1, StringComparison.InvariantCulture);

            if (intEndSectionPos == -1)

                intEndSectionPos = strSource.Length;
            else
                intEndSectionPos -= 1;

            return strSource.Substring(intSectionPos, intEndSectionPos - intSectionPos);
        }
        //IRDSPM::Pratiksha::18-09-2020::For writing data into ini file for trading setting
        public void TradingIniWriteValue(string Section, string Key, string Value, string pathvalue)
        {
            WritePrivateProfileString(Section, Key, Value, pathvalue);
        }
    }
}
