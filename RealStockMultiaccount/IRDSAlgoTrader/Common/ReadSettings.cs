﻿using IRDSAlgoOMS;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace IRDSAlgoOMS
{
    public  class ReadSettings
    {
        static INIFile inif;
        public  string username = ""; 
        public List<string> TradingsymbolList = new List<string>();
        //IRDSPM::Pratiksha::01-07-2020::for real time symbols
        public List<string> iromsRealtimeSymbol = new List<string>();
        public List<string> FinaliromsRealtimeSymbol = new List<string>();
        //IRDSPM::Pratiksha::18-07-2020
        public List<string> RealtimeSymbolfromsymbolini = new List<string>();
        public List<string> SymbolListFromsymbolini = new List<string>();

        Logger logger;
        AlgoOMS signalProcessor; 

        //IRDSPM::Pratiksha::22-4-2020::For Getting OROMS details start
        public string root = "";
        public string login = "";
        public string MyAPIKey = "";
        public string MySecret = "";
        public string MyUserId = "";
        public string MyPassword = "";
        public string Pin = "";        
        //public bool manualLogin = false;       
        public bool autoLogin = false;
        public bool showGUIForOpenPositions = false;

        //IRDSPM::Pratiksha::31-08-2020::New symbol
        public bool isTickMCXDataDownloader = false;
        //IRDSPM::Pratiksha::14-08-2020::Added new variables
        public double overallProfit = 0;
        public string YOB = ""; 
        public bool manualLoginsamco = false;
        public bool showGUIForOpenPositionssamco = false;
        public bool calculateProfitLosssamco = false;
        public bool storeDataMysqlDBsamco = false;
        public bool storeDataSqliteDBsamco = false;
        public bool storeDataCSVsamco = false;
       
        //IRDSPM::Pratiksha::24-11-2020::add new list for enc data
        List<string> PlainDetList;
        List<string> EncDetList;
        //IRDSPM::Pratiksha::24-11-2020::add new list for enc data Samco
        List<string> PlainDetListSamco;
        List<string> EncDetListSamco;
        public ReadSettings(Logger logger)
        {
            this.logger = logger;
        }
        public ReadSettings(Logger logger, AlgoOMS signalProcessor )
        {
            if(this.logger == null)
                this.logger = logger;
            this.signalProcessor = signalProcessor;
        }
      

        public void writeINIFile(string section,string key,string value)
        {
         //IRDSPM::Pratiksha::28-05-2021::increases log size
            //logger.LogMessage("In writeINIFile", MessageType.Informational);
            inif.IniWriteValue(section, key, value);
            //logger.LogMessage("writeINIFile - End", MessageType.Informational);
        }
        //Pratiksha::28-01-2021::Writing data into composite fields
        public void writeINIFileWithPath(string section, string key, string value, string path)
        {
            if (inif == null)
            {
                inif = new INIFile(path);
            }

            //IRDSPM::Pratiksha::28-05-2021::increases log size
            // logger.LogMessage("In writeINIFile", MessageType.Informational);
            inif.TradingIniWriteValue(section, key, value, path);
            //logger.LogMessage("writeINIFile - End", MessageType.Informational);
        }
        //public string readUserId(string filepath)
        //{
        //    string userId = "";
        //    try
        //    {
        //        //logger.LogMessage("In readUserId", MessageType.Informational);
        //        inif = new INIFile(filepath);
        //        userId = inif.IniReadValue("SDATA", "MyUserId");
        //        //logger.LogMessage("readUserId - End", MessageType.Informational);
        //        return userId;
        //    }
        //    catch(Exception e)
        //    {
        //        logger.LogMessage("readUserId : Exception Error Message = "+e.Message, MessageType.Exception);
        //    }
        //    return userId;
        //}

        public void readTradingSymbols(string filepath)
        {
            try
            {
                TradingsymbolList.Clear();
                //logger.LogMessage("In readTradingSymbols", MessageType.Informational);
                inif = new INIFile(filepath);
                List<string> AllData = inif.GetKeyValues("TRADINGSYMBOL");
                foreach (var symbol in AllData)
                {
                    string[] str = symbol.Split(',');
                    TradingsymbolList.Add((str[0]));
                }
            }
            catch (Exception e)
            {
                logger.LogMessage("readTradingSymbols : Exception Error Message = "+e.Message, MessageType.Informational);
            }
        }
       
     
        //IRDSPM::Pratiksha::24-11-2020::Read encrypted data for userid
        public string readUserId(string filepath)
        {
            string userId = "";
            logger.LogMessage("In readUserId", MessageType.Informational);
            inif = new INIFile(filepath);
            string encUserId = inif.IniReadValue("credentials", "MyUserId");
            if (encUserId != "XXXXXX")
            {
                userId = DecryptData(encUserId, "IrdsalgoTrader");
            }
            else
            {
                userId = encUserId;
            }
            logger.LogMessage("readUserId - End", MessageType.Informational);
            return userId;
        }
        //IRDSPM::Pratiksha::22-4-2020::For getting userID of OROMS ini file End

        public bool readConfigFileOROMS(string filename)
        {
            EncDetList = new List<string>();
            bool isload = true;
            try
            {
                logger.LogMessage("In readConfigFile", MessageType.Informational);
                inif = new INIFile(filename);
                root = inif.IniReadValue("credentials", "root");
                login = inif.IniReadValue("credentials", "login");
                //IRDSPM::Pratiksha::24-11-2020::Added change to decrypt details-- start
                string api = inif.IniReadValue("credentials", "MyAPIKey");
                string secret = inif.IniReadValue("credentials", "MySecret");
                string id = inif.IniReadValue("credentials", "MyUserId");
                string pwd = inif.IniReadValue("credentials", "MyPassword");
                string pin = inif.IniReadValue("credentials", "Pin");

                
                if (api != "XXXXXX" && secret != "XXXXXX" && id != "XXXXXX" && pwd != "XXXXXX" && pin != "XXXXXX")
                {
                    EncDetList.Add(api); EncDetList.Add(secret); EncDetList.Add(id); EncDetList.Add(pwd); EncDetList.Add(pin);
                    decryptDet(EncDetList);
                    MyAPIKey = PlainDetList[0];
                    MySecret = PlainDetList[1];
                    MyUserId = PlainDetList[2];
                    MyPassword = PlainDetList[3];
                    Pin = PlainDetList[4];
                }
                else
                {
                    MyAPIKey = api;
                    MySecret = secret;
                    MyUserId = id;
                    MyPassword = pwd;
                    Pin = pin;
                }
                //IRDSPM::Pratiksha::24-11-2020::Added change to decrypt details --ends
                autoLogin = Convert.ToBoolean(inif.IniReadBoolValue("credentials", "autoLogin"));
                //showGUIForOpenPositions = Convert.ToBoolean(inif.IniReadBoolValue("credentials", "showGUIForOpenPositions"));
                isTickMCXDataDownloader = Convert.ToBoolean(inif.IniReadBoolValue("credentials", "isTickMCXDataDownloader"));
                //IRDSPM::Pratiksha::01-07-2020::for reading real time symbol --start
                iromsRealtimeSymbol = inif.GetKeyValues("RealTimeSymbols");
                for(int i=0;i< iromsRealtimeSymbol.Count; i++)
                {
                   string temp = inif.IniReadValue("RealTimeSymbols", i.ToString());
                    FinaliromsRealtimeSymbol.Add(temp);
                }
                //IRDSPM::Pratiksha::01-07-2020::for reading real time symbol --end
            }
            catch (Exception e)
            {
                logger.LogMessage("readConfigFile - exception" + e.Message, MessageType.Informational);
            }
            return isload;
        }

        //IRDSPM::Pratiksha::24-11-2020::Decrypt details after reading
        public void decryptDet(List<string> EncDetList)
        {
            PlainDetList = new List<string>();
            for (int i = 0; i < EncDetList.Count; i++)
            {
                string plaintext = DecryptData(EncDetList[i], "IrdsalgoTrader");
                PlainDetList.Add(plaintext);
            }
        }
        //IRDSPM::Pratiksha::25-11-2020::Decrypt details after reading Samco
        public void decryptDetSamco(List<string> EncDetListSamco)
        {
            PlainDetListSamco = new List<string>();
            for (int i = 0; i < EncDetListSamco.Count; i++)
            {
                string plaintext = DecryptData(EncDetListSamco[i], "IrdsalgoTrader");
                PlainDetListSamco.Add(plaintext);
            }
        }

        //IRDSPM::Pratiksha::18-07-2020::Read data from symbollist ini file
        public bool readConfigFilesymbollist(string filename)
        {
            bool isload = true;
            try
            {
                logger.LogMessage("In readsymbolConfigFile", MessageType.Informational);
                inif = new INIFile(filename);
                RealtimeSymbolfromsymbolini = inif.GetKeyValues("RealTimeSymbols");
                for (int i = 0; i < RealtimeSymbolfromsymbolini.Count; i++)
                {
                    string temp = inif.IniReadValue("RealTimeSymbols", i.ToString());
                    SymbolListFromsymbolini.Add(temp);
                }
                //IRDSPM::Pratiksha::01-07-2020::for reading real time symbol --end
            }
            catch (Exception e)
            {
                logger.LogMessage("readsymbolConfigFile - exception" + e.Message, MessageType.Informational);
            }
            return isload;
        }
        //IRDSPM::Pratiksha::14-08-2020::Added for samco
        public bool readConfigFileSAMCO(string filename)
        {
            bool isload = true;
            try
            {
                EncDetListSamco = new List<string>();               
                inif = new INIFile(filename);
                root = inif.IniReadValue("credentials", "root");              
                string user = inif.IniReadValue("credentials", "MyUserId");
                string pwd = inif.IniReadValue("credentials", "MyPassword");
                string pin = inif.IniReadValue("credentials", "Pin");

                if (user != "XXXXXX" && pwd != "XXXXXX" && pin != "XXXXXX")
                {
                    EncDetListSamco.Add(user); EncDetListSamco.Add(pwd); EncDetListSamco.Add(pin);
                    decryptDetSamco(EncDetListSamco);
                    MyUserId = PlainDetListSamco[0];
                    MyPassword = PlainDetListSamco[1];
                    YOB = PlainDetListSamco[2];
                }
                else
                {
                    MyUserId = user;
                    MyPassword = pwd;
                    YOB = pin;
                }
                logger.LogMessage("readConfigFile - MyUserId "+ MyUserId+ " MyPassword "+ MyPassword + " YOB "+ YOB + " root "+ root, MessageType.Informational);
            }
            catch (Exception e)
            {
                logger.LogMessage("readConfigFile - Exception Error Message" + e.Message, MessageType.Exception);
            }
            return isload;
        }        

        //IRDSPM::Pratiksha::24-11-2020:: decryption code
        public static string DecryptData(string strCryptTxt, string strKey)
        {
            strCryptTxt = strCryptTxt.Replace(" ", "+");

            byte[] bytesBuff = Convert.FromBase64String(strCryptTxt);

            using (Aes aes__1 = Aes.Create())
            {
                Rfc2898DeriveBytes crypto = new Rfc2898DeriveBytes(strKey, new byte[] {
                0x49,
                0x76,
                0x61,
                0x6e,
                0x20,
                0x4d,
                0x65,
                0x64,
                0x76,
                0x65,
                0x64,
                0x65,
                0x76
            });
                aes__1.Key = crypto.GetBytes(32);
                aes__1.IV = crypto.GetBytes(16);

                using (MemoryStream mStream = new MemoryStream())
                {
                    using (CryptoStream cStream = new CryptoStream(mStream, aes__1.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cStream.Write(bytesBuff, 0, bytesBuff.Length);
                        cStream.Close();
                    }

                    strCryptTxt = Encoding.Unicode.GetString(mStream.ToArray());
                }
            }
            return strCryptTxt;
        }
    }
}
