﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRDSAlgoOMS
{
    public sealed class Logger
    {
        public static string symbol = "";
        public string logFilePath = "";
        //private string dirName = "";
        public string directoryName = "";
        public StreamWriter swDefault;

        private static Logger instance = null;
        private static readonly object padlock = new object();

        public Logger()
        {
            //IRDS::Bhagyashri::13-Feb-2018::Log folder creation and fileName with DDMMYYYYHHMMSS format creation 

            string todaysDate = string.Format("{0:dd-MM-yyyy}", DateTime.Now);
            string fileName = string.Format("{0:hh-mm-ss-tt}.txt", DateTime.Now);
            //var fileName = DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss") + ".txt";

            //var currentDir = Directory.GetCurrentDirectory();
            var path = Directory.GetCurrentDirectory();
            directoryName = path + "\\" + "Logs" + "\\" + todaysDate;

            if (!Directory.Exists(directoryName))
                Directory.CreateDirectory(directoryName);

            logFilePath = directoryName + "\\" + fileName;
            swDefault = new StreamWriter(logFilePath, true);
            //sw.Close();
        }

        public static Logger Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (padlock)
                    {
                        if (instance == null)
                        {
                            instance = new Logger();
                        }
                    }
                }
                return instance;
            }
        }

        public string GetFormattedString(string message, MessageType msgType)
        {
            string msgTypeString = "";
            switch (msgType)
            {
                case MessageType.Informational:
                    {
                        msgTypeString = "Informational";
                    }
                    break;

                case MessageType.Error:
                    {
                        msgTypeString = "Error";
                    }
                    break;

                case MessageType.Exception:
                    {
                        msgTypeString = "Exception";
                    }
                    break;
            }

            var formattetString = DateTime.Now.ToString() + Constants.LogFileSeperator + msgTypeString + Constants.LogFileSeperator + message;
            return formattetString;
        }

        public bool LogMessage(string message, MessageType msgType)
        {
            bool bReturn = false;
            string strException = string.Empty;
            try
            {
                swDefault.WriteLine(GetFormattedString(message, msgType));

                swDefault.Flush();

                bReturn = true;
            }
            catch (Exception)
            {
                bReturn = false;
                swDefault.Close();
            }
            return bReturn;
        }

        ~Logger()
        {
            //if(swDefault != null)
            //swDefault.Close();
        }
    }
}
