﻿using IRDSAlgoOMS.Common;
using IRDSAlgoOMS.GUI;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using static IRDSAlgoOMS.StructuresComposite;

namespace IRDSAlgoOMS
{
    public class CompositeConnectWrapper
    {
        Thread m_CalculateProfit = null;
        static readonly object _ForceFullyRelogin = new object();
        bool m_bneedToForcefullyLogin = false;
        //sanika::28-Jul-2021::remove static from login status
        bool m_LoginStatus = false;
        string m_futureName = "";
        List<string> m_RealTimeSymbols = new List<string>();
        private Composite composite;
        //TickerComposite ticker = null;
        bool m_showGUI = false;
        bool m_StoreDataInMysql = true;
        bool m_StoreDataInSqlite = false;
        bool m_HistoricalDataInMysqlDB = false;
        double m_overallProfit = 0;
        double m_overallLoss = 0;
        public ConfigSettings objConfigSettings;
        SQLiteConnection sql_con;
        SQLiteConnection sql_conLotSize;
        SQLiteConnection sql_conData;
        SQLiteCommand sql_cmd;
        SQLiteDataReader sql_read;
        SQLiteTransaction tr;
        SQLiteConnection vol_sql_con;
        //20-July-2021: sandip:global database namr
        string tokenDBName = "composite.db";
        string insFileName = "instrumentsComposite.csv";
        string getCurrentMonth;
        string getCurrentYear;
        string findElement = "FUT";
        //public SpinLock spinLock = new SpinLock(true);
        bool bGetCurrentPositionError = false;
        private static Logger logger = null;
        // DateTime dt = DateTime.Now;
        int getFutSymbolLotSize;
        string getFutSymbol;
        decimal getFutSymboltickSize;
        //20-July-2021: sandip:reuse dictionary across five strategy.
        static Dictionary<string, int> futureSymbolAndLotSize = new Dictionary<string, int>();
        static Dictionary<string, decimal> dictSymbolwithtickSize = new Dictionary<string, decimal>();
        private string m_UserID;
        string m_path = Directory.GetCurrentDirectory();
        OrderStoreComposite m_GlobalOrderStore;
        PositionStoreComposite m_GlobalpositionStore;
        HoldingStoreSamco m_GlobalholdingStore;
        SQLiteConnection Sql_Datacon;
        Dictionary<string, string> m_DictionaryForOrderId = new Dictionary<string, string>();
        Dictionary<string, int> DatabaseCreatedMysql = new Dictionary<string, int>();
        string m_IniFilePath = "";
        bool m_isConnected;
        List<string> m_ListOfTableExistOrNot = new List<string>();
        Dictionary<string, string> m_OrderListWithSymbolAndDirection = new Dictionary<string, string>();
        Dictionary<string, int> m_OrderListWithSymbolAndQuantity = new Dictionary<string, int>();
        TickDataComposite m_TickData;
        int m_Interval = 1;
        List<string> m_InsertQueries = new List<string>();
        public int m_OrdersPerSymbolLimit = 0;
        public int m_OverallOrdersLimit = 0;
        public int m_SameOrdersLimit = 0;
        Dictionary<string, List<OrdersPlaced>> m_DictOrdersPlaced = new Dictionary<string, List<OrdersPlaced>>();
        bool m_StopThread = false;
        System.Timers.Timer m_Timer;
        Thread m_UpdatingStructures = null;
        Thread m_SendResponse = null;
        public int m_ExceptionCounter = 0;
        LocalOrderStructureInfo m_LocalOrderStructureInfo = null;
        static bool m_StopBridgeThread = false;
        //20-July-2021: sandip:use dictionary across 5 strategy
        static List<string> m_AllSymbolsList = new List<string>();//sanika::28-Jul-2021::changed variable name
        public bool m_isCalculateProfitLoss = true;
        public bool m_isSqrOffNSE = false;
        public bool m_isSqrOffNFO = false;
        public bool m_isSqrOffMCX = false;
        public bool m_isSqrOffCDS = false;
        bool m_isCloseAllNSEOrder = false;
        bool m_isCloseAllNFOOrder = false;
        bool m_isCloseAllMCXOrder = false;
        bool m_isCloseAllCDSOrder = false;
        public static bool m_isSqrOff = false;
        public static bool m_isFromEXEForBridgeConnection = true;
        public static bool m_isFromEXEForshowGUI = false;
        string m_StartTime = "";
        string m_TimeLimitToPlaceOrder = "";
        public string m_TimeToExitAllNSEOrders = "";
        string m_TimeToExitAllNFOOrders = "";
        string m_TimeToExitAllMCXOrders = "";
        string m_TimeToExitAllCDSOrders = "";

        //sanika::3-dec-2020::Added for diff exchanges
        DateTime m_StartTimeForPlaceOrder;
        DateTime m_EndTimeForPlaceOrder;
        DateTime m_TimeToExitAllNSEOrder;
        DateTime m_TimeToExitAllNFOOrder;
        DateTime m_TimeToExitAllMCXOrder;
        DateTime m_TimeToExitAllCDSOrder;
        double m_TotalProfitLoss = 0;
        public bool m_waitingStatus = false;
        MySQLConnectZerodha m_MySqlConnection = null;
        bool m_isMysqlconnected = false;
        public bool isDownloadInstrumentsNow = false;
        public int m_CounterProgressBar = 0;
        public List<string> marketWatchdeletelistmain = new List<string>();
        int m_PreviousOrderCounter = 0;
        List<string> m_ListForSymbols = new List<string>();
        bool isTickMCXDataDownloader = false;
        string ConnString = "";
        //sanika::1-Feb-2021::Added for risk management
        int m_BrokerQuantity = 0;
        int m_ManualQuantity = 0;
        bool m_RiskManagmentFlag = false;
        public bool m_StartedinstrumentDownloading = false;
        string DownloadTokenWay = "";
        string m_apiKey = "";
        string m_secretKey = "";
        string m_userid = "";
        string m_marketapiKey = "";
        string m_marketsecretKey = "";
        APISettingWindowsForm m_ApiSettingObj = null;
        string m_ServerUrl = "algotrader.caejfnk6d1lx.ap-south-1.rds.amazonaws.com";
        string m_username = "admin";
        string m_password = "IRDS2020!";
        string m_Dbname = "Zerodha";
        public string m_Title = "";
        Dictionary<string, double> m_IndividualProfitLoss = new Dictionary<string, double>();
        dynamic userMargin = null;
        double m_TimeToUpdatePNLInFile = 0;
        //sanika::8-July-2021::Added to update margin after every 5 min
        double m_TimeToUpdateMargin = 0;
        string m_PNLFilePath = Directory.GetCurrentDirectory() + "\\PNL.csv";
        //20-July-2021: sandip:optimization of objects
        // public static List<string> m_OptionSymbolListFromDB = new List<string>();//sanika::28-Jul-2021::changed variable name
        Notification m_Notification = null;
        private static readonly object m_Timerlock = new object();
        //Pratiksha::27-04-2021::For New details
        public static List<string> m_OptionSymbols = new List<string>();//sanika::28-Jul-2021::changed variable name and assigned static 
        //sanika::20-July-2021::Added static variable to download instruments only once if multiple accounts are set
        static bool m_isDownloadedTokensSuccessfully = false;
        public CompositeConnectWrapper(string iniFilePath)
        {
            ConnString = "server=" + m_ServerUrl + "; user id=" + m_username + ";password=" + m_password + ";database=" + m_Dbname + ";";
            var path = Directory.GetCurrentDirectory();
            string iniFile = path + @"\Configuration\" + iniFilePath;
            m_IniFilePath = iniFile;
            objConfigSettings = new ConfigSettings();
            objConfigSettings.readCompositeConfigFile(iniFile);
            objConfigSettings.readDBSettingINIFile(path + @"\Configuration\DatabaseSettings.ini");
            objConfigSettings.readTradingINIFile(path + @"\Configuration\TradingSetting.ini");
            objConfigSettings.ReadSettingConfigFile(path + @"\Configuration\Setting.ini");
            m_UserID = objConfigSettings.username;
            //IRDSPM::Pratiksha::16-02-2021::For manual login title changes
            m_Title = objConfigSettings.m_Title;
            logger = Logger.Instance;
            logger.LogMessage("SetSessionExpiryHook successful.", MessageType.Informational);
            m_GlobalpositionStore = new PositionStoreComposite();
            m_GlobalOrderStore = new OrderStoreComposite();
            m_GlobalholdingStore = new HoldingStoreSamco();
            m_Notification = new Notification();
            m_showGUI = objConfigSettings.isShowGUI;
            //m_StoreDataInMysql = objConfigSettings.storeDataMysqlDB;
            m_RealTimeSymbols = objConfigSettings.Symbol;
            m_TickData = new TickDataComposite();
            m_Interval = objConfigSettings.interval;
            m_overallLoss = objConfigSettings.m_overallLoss;
            m_overallProfit = objConfigSettings.m_overallProfit;
            m_OrdersPerSymbolLimit = objConfigSettings.OrdersPerSymbolLimit;
            m_OverallOrdersLimit = objConfigSettings.OverallOrdersLimit;
            m_SameOrdersLimit = objConfigSettings.SameOrdersLimit;
            m_HistoricalDataInMysqlDB = objConfigSettings.historicalDataInMysqlDB;
            //m_EndTime = objConfigSettings.EndTime;
            m_StartTime = objConfigSettings.StartTimeNSE;
            m_TimeLimitToPlaceOrder = objConfigSettings.TimeLimitToPlaceOrder;
            //sanika::3-dec-2020::Added for different exchanges
            m_TimeToExitAllNSEOrders = objConfigSettings.ExitTimeNSE;
            m_TimeToExitAllNFOOrders = objConfigSettings.ExitTimeNFO;
            m_TimeToExitAllMCXOrders = objConfigSettings.ExitTimeMCX;
            m_TimeToExitAllCDSOrders = objConfigSettings.ExitTimeCDS;

            m_StartTimeForPlaceOrder = DateTime.ParseExact(m_StartTime, "HH:mm:ss", null);
            m_EndTimeForPlaceOrder = DateTime.ParseExact(m_TimeLimitToPlaceOrder, "HH:mm:ss", null);

            //sanika::3-dec-2020::Added for different exchanges
            m_TimeToExitAllNSEOrder = DateTime.ParseExact(m_TimeToExitAllNSEOrders, "HH:mm:ss", null);
            m_TimeToExitAllNFOOrder = DateTime.ParseExact(m_TimeToExitAllNFOOrders, "HH:mm:ss", null);
            m_TimeToExitAllMCXOrder = DateTime.ParseExact(m_TimeToExitAllMCXOrders, "HH:mm:ss", null);
            m_TimeToExitAllCDSOrder = DateTime.ParseExact(m_TimeToExitAllCDSOrders, "HH:mm:ss", null);
            m_LocalOrderStructureInfo = new LocalOrderStructureInfo();
            m_isCalculateProfitLoss = objConfigSettings.isCalculateProfitLoss;
            DownloadTokenWay = objConfigSettings.DownloadTokenWay;
            m_apiKey = objConfigSettings.appKey;
            m_secretKey = objConfigSettings.secretKey;
            m_userid = objConfigSettings.username;
            m_marketapiKey = objConfigSettings.marketDataAppKey;
            m_marketsecretKey = objConfigSettings.marketDataSecretKey;
            //delete old GUILOG file 
            //string todaysDate = string.Format("{0:dd-MM-yyyy}", DateTime.Now);
            //if (File.Exists(path + @"\Logs\" + todaysDate + "\\GUILogs.txt"))
            //{
            //    File.Delete(path + @"\Logs\" + todaysDate + "\\GUILogs.txt");
            //}
        }
        private string GenerateOrderTag()
        {
            string id = Guid.NewGuid().ToString();
            if (id.Length > 20)
            {
                id = id.Substring(0, 20);
            }

            return id;
        }

        //Sanika::2-Sep-2020::Added method to return fromexe flag
        public bool GetFromEXEValue()//sanika::28-Jul-2021::remove static
        {
            return m_isFromEXEForBridgeConnection;
        }

        public bool Connect()
        {
            try
            {
                m_waitingStatus = true;
                //IRDSPM::Pratiksha::27-11-2020::For XXXXXX check
                if (m_apiKey.Contains("XXXXXX") || m_userid.Contains("XXXXXX") || m_marketapiKey.Contains("XXXXXX") || m_secretKey.Contains("XXXXXX") || m_marketsecretKey.Contains("XXXXXX"))
                {
                    //sanika::8-sep-2020::Added check for showing GUI
                    if (m_showGUI == true)
                    {
                        //IRDSPM::PRatiksha::01-09-2020::If userId XXXXXX then open brokr form                        
                        //IRDSPM::Pratiksha::18-09-2020::For Open new api setting form
                        if (m_ApiSettingObj == null)
                        {
                            m_ApiSettingObj = new APISettingWindowsForm(this, m_LoginStatus.ToString(), m_Title, m_userid);
                        }
                        m_ApiSettingObj.LoadKiteConnectObject(this, m_LoginStatus.ToString());
                        //20-July-2021: sandip: why 7 sec? change to 1
                        Thread.Sleep(1000);
                        m_ApiSettingObj.ShowDialog();
                        //IRDSPM::Pratiksha::27-11-2020::Read values and check values not contain XXXXXX
                        objConfigSettings.readConfigFile(m_IniFilePath);
                        if (objConfigSettings.apiKey != "XXXXXX" || objConfigSettings.apiSecret != "XXXXXX" || objConfigSettings.username != "XXXXXX" || objConfigSettings.password != "XXXXXX" || objConfigSettings.Pin != "XXXXXX")
                        {
                            logger.LogMessage("Connect : Read values properly", MessageType.Informational);
                        }
                        else
                        {
                            logger.LogMessage("Connect : Invalid ini values", MessageType.Informational);
                            return false;
                        }
                    }
                    else if (m_isFromEXEForshowGUI)
                    {//IRDSPM::Pratiksha::06-02-2021::Added change
                        m_ApiSettingObj = null;
                        //IRDSPM::PRatiksha::01-09-2020::If userId XXXXXX then open brokr form                       
                        if (m_ApiSettingObj == null)
                        {
                            m_ApiSettingObj = new APISettingWindowsForm(this, m_LoginStatus.ToString(), m_Title, m_userid);
                        }
                        m_ApiSettingObj.LoadKiteConnectObject(this, m_LoginStatus.ToString());
                        //20-July-2021: sandip: why 7 sec sleep?
                        Thread.Sleep(1000);
                        m_ApiSettingObj.ShowDialog();
                        logger.LogMessage("Connect : Invalid ini values", MessageType.Informational);
                    }
                }
                //IRDSPM::Pratiksha::06-02-2021::If XXXXXX then do not open webbrowser
                if (objConfigSettings.apiKey != "XXXXXX" || objConfigSettings.apiSecret != "XXXXXX" || objConfigSettings.username != "XXXXXX" || objConfigSettings.password != "XXXXXX" || objConfigSettings.Pin != "XXXXXX")
                {
                    if (composite == null)
                    {
                        composite = new Composite(objConfigSettings, Debug: true);
                    }
                    composite.SetSessionExpiryHook(composite.OnTokenExpire);
                    composite.initializeLoggerObject(logger, m_GlobalOrderStore, m_TickData);

                    //IRDS::Jyoti::26-Jul-2021::Added to get tokens everytime as suggested by sir
                    if (true)// || !getCreadentials())
                    {
                        logger.LogMessage("Connect : Going for login", MessageType.Informational);
                        composite.initSeesion();
                        logger.LogMessage("Connect : After login", MessageType.Informational);
                        if (composite.LoginFlag)
                        {
                            m_LoginStatus = true;
                            WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " Connected");
                            m_ExceptionCounter++;
                            logger.LogMessage("Connect : initSeesion successful.", MessageType.Informational);
                        }
                        else
                        {
                            logger.LogMessage("Connect : initSeesion Unsuccessful.", MessageType.Informational);
                            m_LoginStatus = false;
                            WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + composite.m_ExceptionError);
                            m_ExceptionCounter++;
                            if (composite.m_ExceptionError != "")
                                System.Windows.Forms.MessageBox.Show(new Form { TopMost = true }, composite.m_ExceptionError);
                        }

                        //sanika::18-sep-2020::added to download instrument
                        if (m_LoginStatus)
                        {
                            if (DownloadTokenWay.ToLower() == "auto")
                            {
                                //sanika::20-July-2021::Added condition to download instruments only once if multiple accounts are set
                                if (!CheckDownloadInstrumentCSV() && m_isDownloadedTokensSuccessfully == false)
                                {
                                    isDownloadInstrumentsNow = true;
                                    m_StartedinstrumentDownloading = true;
                                    m_isDownloadedTokensSuccessfully = true;
                                    Thread.Sleep(2000);
                                    DateTime dt = DateTime.Now;
                                    IntrumentsCSVToDB();
                                    DateTime dt1 = DateTime.Now;
                                    double diff = dt.Subtract(dt1).TotalMinutes;
                                    File.AppendAllText("time.txt", "Time to download instruments " + diff);
                                    m_StartedinstrumentDownloading = false;
                                    WriteDateIntoFile();
                                    //IRDSPM::Pratiksha::20-10-2020::Progressbar counter
                                    m_CounterProgressBar = 101;

                                }
                            }
                        }
                    }
                    //else
                    //{
                    //    logger.LogMessage("Connect : Credential file present", MessageType.Informational);

                    //    int counter = 0;
                    //    while (counter < 5)
                    //    {
                    //        //IRDS::Jyoti::22-Jul-21::Added sleep as per sir's suggestions
                    //        //20-July-2021: sandip:added simple function for api working
                    //        RequestNUpdatePositioInfoLogin();
                    //        //RequestNUpdatePositioInfo();
                    //        //    Thread.Sleep(1000);
                    //        if (m_bneedToForcefullyLogin == true)
                    //        {
                    //            lock (_ForceFullyRelogin)
                    //            {
                    //                logger.LogMessage("Connect : Going for Forcefully login", MessageType.Informational);
                    //                ForceFulReLogin();
                    //                m_LoginStatus = true;
                    //                WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " Connected");
                    //                m_ExceptionCounter++;
                    //                m_bneedToForcefullyLogin = false;
                    //    Thread.Sleep(1000);
                    //            }
                    //        }
                    //        else
                    //        {
                    //            m_LoginStatus = true;
                    //            WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " Connected");
                    //            m_ExceptionCounter++;
                    //            break;
                    //        }
                    //        counter++;
                    //    }


                    //}
                    //20-July-2021: sandip: call after session creates.
                    if (m_LoginStatus)
                    {
                        ValidateTokenFileDatabase();
                        //20-July-2021: sandip: lets thread call this function
                        //RequestNUpdateOrderInfo();
                        //Thread.Sleep(1000);
                        composite.initTicker();
                        logger.LogMessage("initTicker successful.", MessageType.Informational);
                    }
                }
                else
                {
                    logger.LogMessage("Connect : Got values as XXXXX", MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("Exception is " + e.Message);
                logger.LogMessage("Connect : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            //20-July-2021: sandip: call after session initialize
            m_futureName = FetchTableName();


            if (m_LoginStatus)
            {
                composite.SetAccessToken(composite.accessToken);
                logger.LogMessage("Connect : SetAccessToken successful.", MessageType.Informational);
                sql_con = null;
                sql_conData = null;
                //04-August-2021: sandip:memory issue
                //if (ticker == null)
                //{
                //    ticker = new TickerComposite();
                //}

                //composite.initTicker();
                logger.LogMessage("Connect : initTicker successful.", MessageType.Informational);
                LoadSymbolWithLotandTickInDict();

                if (m_CalculateProfit == null)
                {
                    m_CalculateProfit = new Thread(() => CalculateProfitThread());
                    m_CalculateProfit.Start();
                }

                //sanika::5-sep-2020::Started thread 
                if (m_UpdatingStructures == null)
                {
                    m_UpdatingStructures = new Thread(() => UpdateStructures());
                    m_UpdatingStructures.Start();
                }
                //20-July-2021: sandip:pipe mechanism commented
                //if (m_SendResponse == null)
                //{
                //    m_SendResponse = new Thread(() => SendResponse());
                //    m_SendResponse.Start();
                //}

                //Pratiksha::26-04-2021::Get Options symbol
                getSymbolsFromOptions();

                //IRDSPM::Pratiksha::14-07-2021::Sometimes in thread all symbol not getting
                GetAllSymbolsFromDBdata();

                if (m_Timer == null)
                {
                    //04-August-2021: sandip:memory issue
                    m_Timer = new System.Timers.Timer();
                    m_Timer.Elapsed += _OnTimerExecute;
                    m_Timer.Interval = 60000; // checks connection every second
                    m_Timer.Enabled = true;
                    logger.LogMessage("connect : started new System.Timers.Timer()", MessageType.Informational);
                }
            }
            else
            {
                logger.LogMessage("Connect : Value of m_LoginStatus is false", MessageType.Informational);
            }
            logger.LogMessage("Connect : compositeConnectWrapper finished.", MessageType.Informational);

            return m_LoginStatus;
        }
        //20-July-2021: sandip: added function to check instrument conditoons.
        public void ValidateTokenFileDatabase()
        {
            logger.LogMessage("Connect : ValidateTokenFileDatabase start", MessageType.Informational);
            //sanika::18-sep-2020::added to download instrument                   
            {
                if (DownloadTokenWay.ToLower() == "auto")
                {
                    logger.LogMessage("Connect : ValidateTokenFileDatabase auto " + m_isDownloadedTokensSuccessfully.ToString(), MessageType.Informational);
                    //sanika::20-July-2021::Added condition to download instruments only once if multiple accounts are set
                    if (!CheckDownloadInstrumentCSV() && m_isDownloadedTokensSuccessfully == false)
                    {
                        isDownloadInstrumentsNow = true;
                        m_isDownloadedTokensSuccessfully = true;
                        logger.LogMessage("Connect : Going to download instruments", MessageType.Informational);
                        m_StartedinstrumentDownloading = true;
                        Thread.Sleep(2000);
                        DateTime dt = DateTime.Now;
                        IntrumentsCSVToDB();
                        DateTime dt1 = DateTime.Now;
                        double diff = dt.Subtract(dt1).TotalMilliseconds;
                        File.AppendAllText("time.txt", "Time to download instruments " + diff);
                        m_StartedinstrumentDownloading = false;
                        WriteDateIntoFile();
                        //IRDSPM::Pratiksha::20-10-2020::Progressbar counter
                        m_CounterProgressBar = 101;
                    }
                    else
                    {
                        logger.LogMessage("Connect : CheckDownloadInstrumentCSV condition fail auto m_isDownloadedTokensSuccessfully " + m_isDownloadedTokensSuccessfully.ToString(), MessageType.Informational);
                    }
                }
            }
        }
        public string GetFilePath()
        {
            return m_IniFilePath;
        }

        public bool GetShowGUIFlag()
        {
            return m_showGUI;
        }

        public bool GetRealTimeSymbols(out List<string> symbolsList)
        {
            symbolsList = m_RealTimeSymbols;
            return true;
        }

        //sanika::19-Feb-2021::Added missing function to reload trading settings
        public void isReloadTradingSettingINI()
        {
            var path = Directory.GetCurrentDirectory();
            objConfigSettings.readTradingINIFile(path + @"\Configuration\TradingSetting.ini");
            m_overallLoss = objConfigSettings.m_overallLoss;
            m_overallProfit = objConfigSettings.m_overallProfit;
            m_isCalculateProfitLoss = objConfigSettings.isCalculateProfitLoss;
            m_StartTime = objConfigSettings.StartTimeNSE;
            //m_EndTime = objConfigSettings.EndTime;
            m_TimeLimitToPlaceOrder = objConfigSettings.TimeLimitToPlaceOrder;
            //sanika::3-dec-2020::Added for diff exchanges
            m_TimeToExitAllNSEOrders = objConfigSettings.ExitTimeNSE;
            m_TimeToExitAllNFOOrders = objConfigSettings.ExitTimeNFO;
            m_TimeToExitAllMCXOrders = objConfigSettings.ExitTimeMCX;
            m_TimeToExitAllCDSOrders = objConfigSettings.ExitTimeCDS;
            m_StartTimeForPlaceOrder = DateTime.ParseExact(m_StartTime, "HH:mm:ss", null);
            m_EndTimeForPlaceOrder = DateTime.ParseExact(m_TimeLimitToPlaceOrder, "HH:mm:ss", null);

            //sanika::3-dec-2020::Added for diff exchanges
            m_TimeToExitAllNSEOrder = DateTime.ParseExact(m_TimeToExitAllNSEOrders, "HH:mm:ss", null);
            m_TimeToExitAllNFOOrder = DateTime.ParseExact(m_TimeToExitAllNFOOrders, "HH:mm:ss", null);
            m_TimeToExitAllMCXOrder = DateTime.ParseExact(m_TimeToExitAllMCXOrders, "HH:mm:ss", null);
            m_TimeToExitAllCDSOrder = DateTime.ParseExact(m_TimeToExitAllCDSOrders, "HH:mm:ss", null);

            //sanika::8-dec-2020::added for risk managment 
            m_BrokerQuantity = Convert.ToInt32(objConfigSettings.brokerQuantity);
            m_ManualQuantity = Convert.ToInt32(objConfigSettings.ManualQuantity);
            m_RiskManagmentFlag = objConfigSettings.isRiskmanagement;
        }
        public void CalculateProfit()
        {
            try
            {
                PositionResponseComposite positionResponseForOverall = GetPositions();
                if (positionResponseForOverall.Net.Count != 0)
                {
                    double overallProfit = m_overallProfit;
                    double overallLoss = m_overallLoss;
                    int quantity = 0;
                    string product = "";
                    double profitLoss = 0;
                    foreach (var symbol in m_RealTimeSymbols)
                    {
                        double lastPrice = 0;
                        string tradingSymbol = symbol.Split('.')[0];
                        string exchange = symbol.Split('.')[1];
                        if (exchange == Constants.EXCHANGE_NFO)
                        {
                            tradingSymbol = tradingSymbol + FetchTableName();
                        }

                        foreach (var position in positionResponseForOverall.Net)
                        {
                            if (position.TradingSymbol == tradingSymbol)
                            {
                                quantity = position.Quantity;
                                product = position.Product;
                            }
                        }
                        double OpenPrice = GetOpenPostionPrice(tradingSymbol, exchange, product);
                        if (OpenPrice != 0)
                        {
                            lastPrice = Convert.ToDouble(getLastTradedPrice(tradingSymbol));
                            double profitOrLoss = 0; ;
                            if (quantity > 0)
                            {
                                profitOrLoss = (lastPrice - OpenPrice) * quantity;
                            }
                            else if (quantity < 0)
                            {
                                profitOrLoss = (OpenPrice - lastPrice) * quantity;
                            }
                            logger.LogMessage(tradingSymbol + "calculateProfit : profitOrLoss " + profitOrLoss, MessageType.Informational);
                            profitLoss += profitOrLoss;
                        }
                        else
                        {
                            //logger.LogMessage("calculateProfit : Open price is zero", MessageType.Informational);
                        }
                    }

                    if (profitLoss > overallProfit || profitLoss < overallLoss)
                    {
                        //logger.LogMessage("calculateProfit : Condition true for closeallpostion", MessageType.Informational);
                        logger.LogMessage("calculateProfit : profitLoss " + profitLoss + " overallProfit " + overallProfit + " overallLoss " + overallLoss + "-- Condition true", MessageType.Informational);
                        foreach (var symbol in m_RealTimeSymbols)
                        {
                            string tradingSymbol = symbol.Split('.')[0];
                            string exchange = symbol.Split('.')[1];
                            if (exchange == Constants.EXCHANGE_NFO)
                            {
                                tradingSymbol = tradingSymbol + FetchTableName();
                            }
                            CloseOrder(tradingSymbol, exchange, product);
                        }
                        logger.LogMessage("calculateProfit : closed all position successfully", MessageType.Informational);

                    }
                    else
                    {
                        //logger.LogMessage("calculateProfit : condition not match", MessageType.Informational);
                    }
                }
            }
            catch (Exception e)
            {
                logger.LogMessage("calculateProfit : Exception Error Message =  " + e.Message, MessageType.Exception);
            }
        }

        public bool GetConnectionStatus()
        {
            if (m_LoginStatus)
            {
                return true;
            }
            return false;
        }


        public string GetUserID()
        {
            return m_UserID;
        }

        public void RollOver(string TradingSymbol, string Exchange, string Product)
        {
            try
            {
                string symbol = TradingSymbol + FetchTableName();
                DateTime LastThusdaydateTime = GetRollOverDate();
                WriteUniquelogs(TradingSymbol + " " + m_UserID, " RollOver : LastThusdaydateTime = " + LastThusdaydateTime, MessageType.Informational);
                DateTime todayDate = DateTime.ParseExact(DateTime.Now.ToString("dd-MM-yyyy"), "dd-MM-yyyy", null);
                if (LastThusdaydateTime == todayDate)
                {
                    double openPrice = GetOpenPostionPrice(symbol, Exchange, Product);
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, " RollOver : openPrice = " + openPrice, MessageType.Informational);
                    if (openPrice != 0)
                    {
                        string direction = GetOpenPostionDirection(symbol, Exchange, Product);
                        int quantity = GetOpenPostionQuantity(symbol, Exchange, Product);
                        WriteUniquelogs(TradingSymbol + " " + m_UserID, " RollOver : direction = " + direction + "quantity = " + quantity, MessageType.Informational);
                        if (CloseOrder(symbol, Exchange, Product))
                        {
                            string name = GetNextMonthSymbolName();
                            int Lotsize = GetMonthLotSize(TradingSymbol, Exchange, name);
                            if (Lotsize > 0)
                            {
                                quantity = Lotsize;
                            }
                            string newSymbolName = TradingSymbol + name;
                            WriteUniquelogs(TradingSymbol + " " + m_UserID, " RollOver : Closed previous order and new symbol name = " + newSymbolName, MessageType.Informational);
                            WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + "Closed previous order and new symbol name = " + newSymbolName);
                            m_ExceptionCounter++;
                            string orderId = PlaceOrder(Exchange, newSymbolName, direction, quantity, "MKT", Product: Product);
                            if (orderId != "NA")
                            {
                                WriteUniquelogs(TradingSymbol + " " + m_UserID, "RollOver : order placed successfully ", MessageType.Informational);
                                WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + TradingSymbol + " Order Placed successfully!! " + orderId);
                                m_ExceptionCounter++;
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
        }

        public string GetNextMonthSymbolName()
        {
            string name = "";
            try
            {
                string year = DateTime.Now.ToString("yy");
                string currentMonth = DateTime.Now.ToString("MMM");
                if (currentMonth.Equals("Dec"))
                {
                    int yr = Convert.ToInt32(year) + 1;
                    year = yr.ToString();
                }
                name = year + DateTime.Now.AddMonths(1).ToString("MMM").ToUpper() + "FUT";
            }
            catch (Exception e)
            {

            }
            logger.LogMessage(" GetNextMonthSymbolName : name = " + name, MessageType.Informational);
            return name;
        }
        public DateTime GetRollOverDate()
        {
            DateTime date = DateTime.Now;
            int year = DateTime.Now.Year;
            int month = DateTime.Now.Month;
            try
            {
                var tmpDate = new DateTime(year, month, 1).AddMonths(1).AddDays(-1);
                while (tmpDate.DayOfWeek != DayOfWeek.Thursday)
                {
                    tmpDate = tmpDate.AddDays(-1);
                }
                date = tmpDate;
            }
            catch (Exception e)
            {

            }
            return date;
        }

        public void WriteDateIntoFile()
        {
            try
            {
                string currentDate = DateTime.Now.ToString("M-d-yyyy");
                var path_File = Directory.GetCurrentDirectory();
                string FilePath = path_File + @"\Configuration\" + "instrument.txt";
                string line = currentDate;
                using (StreamWriter streamWriter = new StreamWriter(FilePath, false))
                {
                    streamWriter.WriteLine(line);
                }
                m_CounterProgressBar = 100;
            }
            catch (Exception e)
            {

            }
        }

        public bool CheckDownloadInstrumentCSV()
        {
            try
            {
                //20-July-2021: sandip:add db not present conditions
                var path_DB = Directory.GetCurrentDirectory();
                string directoryName = path_DB + "\\" + "Database";
                string DBFilePath = directoryName + "\\" + tokenDBName;

                if (!File.Exists(DBFilePath))
                {
                    logger.LogMessage("Connect :CheckDownloadInstrumentCSV db file not exist " + DBFilePath, MessageType.Informational);
                    return false;
                }
                //IRDSPM::Pratiksha::05-02-2020::For download instruments on friday only
                if (DateTime.Now.DayOfWeek.ToString().ToLower() == "friday")
                {
                    var path_File = Directory.GetCurrentDirectory();
                    string FilePath = path_File + @"\Configuration\" + "instrument.txt";
                    if (!File.Exists(FilePath))
                    {
                        Console.WriteLine("instrument.txt file does not exist");
                        return false;
                    }
                    else
                    {
                        string lastLine = System.IO.File.ReadLines(FilePath).Last();
                        string currentDate = DateTime.Now.ToString("M-d-yyyy");
                        if (lastLine == currentDate)
                        {
                            return true;
                        }
                    }
                }
                else
                {
                    return true;
                }
            }
            catch (Exception e)
            {
            }
            return false;
        }

        public bool getCreadentials()
        {
            bool res = false;
            try
            {
                var path_File = Directory.GetCurrentDirectory();
                string FilePath = path_File + @"\Configuration\" + "credentialsComposite_" + m_UserID + ".txt";
                if (!File.Exists(FilePath))
                {
                    Console.WriteLine("credentialsComposite.txt file does not exist");
                    res = false;
                }
                else
                {
                    string lastLine = System.IO.File.ReadLines(FilePath).Last();
                    string[] splittedString = lastLine.Split(' ');
                    string currentDate = DateTime.Now.ToString("M-d-yyyy");
                    if (splittedString[0] == currentDate && splittedString[1] == m_UserID)
                    {
                        composite.accessToken = splittedString[2];
                        //composite.MyPublicToken = splittedString[3];
                        composite.LoginFlag = true;
                        res = true;
                    }
                }
                FilePath = path_File + @"\Configuration\" + "credentialsCompositeMarketData_" + m_UserID + ".txt";
                if (!File.Exists(FilePath))
                {
                    Console.WriteLine("credentialsCompositeMarketData.txt file does not exist");
                    res = false;
                }
                else
                {
                    string lastLine = System.IO.File.ReadLines(FilePath).Last();
                    string[] splittedString = lastLine.Split(' ');
                    string currentDate = DateTime.Now.ToString("M-d-yyyy");
                    if (splittedString[0] == currentDate && splittedString[1] == m_UserID)
                    {
                        composite.accessMarketDataToken = splittedString[2];
                        //composite.MyPublicToken = splittedString[3];
                        composite.LoginFlag = true;
                        res = true;
                    }
                }
            }
            catch (Exception e)
            {
                logger.LogMessage("getCreadentials : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return res;
        }

        public bool StopThread()
        {
            try
            {
                StopTimer();
                m_StopThread = true;
                m_StopBridgeThread = true;
                //sanika::16-July-2021::added to reset thread variable to null
                m_UpdatingStructures = null;
                m_SendResponse = null;
                m_CalculateProfit = null;
                Thread.Sleep(500);
                if (composite != null)//sanika::2-Dec-2020::Added condition without login closed then gives exception
                    composite.StopThread();
                return true;
            }
            catch (Exception e)
            {
                logger.LogMessage("StopThread : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return false;
        }

        public void StopTimer()
        {
            try
            {
                if (m_Timer != null)
                {
                    //sanika::23-Nov-2020::First close then timer then dispose
                    m_Timer.Stop();
                    m_Timer.Close();
                    m_Timer.Dispose();
                    logger.LogMessage("StopTimer : Timer stopped", MessageType.Informational);
                }
                Thread.Sleep(500);
            }
            catch (Exception e)
            {
                logger.LogMessage("StopTimer : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        public CompositeConnectWrapper()
        {
        }

        public bool IntrumentsCSVToDB()
        {
            WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " Downloading Instrument Tokens!!");
            m_ExceptionCounter++;
            bool result = false;
            logger.LogMessage("### Started for writing csv = " + DateTime.Now, MessageType.Informational);
            var path_DB = Directory.GetCurrentDirectory();
            string directoryDBName = path_DB + "\\" + "Database";

            if (!Directory.Exists(directoryDBName))
                Directory.CreateDirectory(directoryDBName);
            //20-July-2021: sandip: global db name used.
            string DBFilePath = directoryDBName + "\\" + tokenDBName;

            var path = Directory.GetCurrentDirectory();
            string directoryName = path + "\\" + "InstrumentList";

            if (!Directory.Exists(directoryName))
                Directory.CreateDirectory(directoryName);

            string insFilePath = directoryName + "\\" + insFileName;
            if (File.Exists(insFilePath))
            {
                //Delete old instrument file
                File.Delete(insFilePath);
                logger.LogMessage("Previous Instruments.csv file is deleted", MessageType.Informational);
                DownloadInstrumentTokens(insFilePath);
            }
            else
            {
                logger.LogMessage("### Inside Else part of writing csv = " + DateTime.Now, MessageType.Informational);
                DownloadInstrumentTokens(insFilePath);
                logger.LogMessage("### Done writing csv = " + DateTime.Now, MessageType.Informational);
            }
            logger.LogMessage("### End for writing csv = " + DateTime.Now, MessageType.Informational);
            DateTime dt = DateTime.Now;
            result = FetchInstrumentDetailsFromCSV();
            DateTime dt1 = DateTime.Now;
            double diff = dt1.Subtract(dt).TotalMinutes;
            File.AppendAllText("time.txt", "\nTime to insert data into db " + diff);
            logger.LogMessage("### End for Read csv = " + DateTime.Now, MessageType.Informational);
            LoadSymbolWithLotandTickInDict();
            logger.LogMessage("### End for LoadSymbolWithLotandTickInDict csv = " + DateTime.Now, MessageType.Informational);
            WriteGUIlogs(m_ExceptionCounter, DateTime.Now + "  Instrument Tokens Downloading Completed!!");
            m_ExceptionCounter++;
            return result;
        }

        private void DownloadInstrumentTokens(string insFilePath)
        {
            //create a new instrument file
            List<string> list = new List<string>();

            if (objConfigSettings.CheckNSE == true)
            {
                list.Add("NSECM");
            }
            if (objConfigSettings.CheckNFO == true)
            {
                list.Add("NSEFO");
            }
            if (objConfigSettings.CheckCDS == true)
            {
                list.Add("NSECD");
            }

            //list.Add("NSECD");
            //list.Add("MCXFO");
            //list.Add("BSECM");
            //list.Add("BSEFO");
            string instruments = "";
            if (list.Count > 0)//sanika::19-Feb-2021::Added condition for index exception
            {
                DateTime dt = DateTime.Now;
                instruments = composite.GetInstruments(list);
                DateTime dt1 = DateTime.Now;
                double diff = dt1.Subtract(dt).TotalMinutes;
                File.AppendAllText("time.txt", "\nTime to download instruments from server " + diff);
            }
            DateTime dt11 = DateTime.Now;
            using (StreamWriter streamWriter = new StreamWriter(insFilePath, true))
            {
                streamWriter.WriteLine(instruments);
                streamWriter.Close();
            }
            DateTime dt2 = DateTime.Now;
            double diff1 = dt2.Subtract(dt11).TotalMinutes;
            File.AppendAllText("time.txt", "\nWrite in csv file " + diff1);
        }

        public bool GetLatestOrderDetails(string TradingSymbol, string Exchange, string TransactionType, string orderStatus, out OrderComposite latestOrder)
        {
            OrderComposite order = new OrderComposite();
            try
            {
                List<OrderComposite> orderInfo = m_GlobalOrderStore.GetOrderbySymbol(TradingSymbol, Exchange);
                if (orderInfo.Count == 0)
                {
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, " GetLatestOrderDetails : order info count is zero", MessageType.Informational);
                    latestOrder = order;
                    return false;
                }
                //sanika::19-Jam-2021::checked order type also because stop order and limit order has same status
                string status = "";
                string orderType = "";
                if (orderStatus.Contains("_"))
                {
                    orderType = orderStatus.Split('_')[0];
                    status = orderStatus.Split('_')[1];
                }
                for (int i = 0; i < orderInfo.Count(); i++)
                {
                    order = orderInfo[i];
                    if (order.TransactionType == TransactionType && order.OrderType == orderType)
                    {
                        if (order.Status == status || order.Status == "PARTIALLYFILLED")
                        {
                            break;
                        }
                    }
                }
                //WriteUniquelogs(TradingSymbol + " " + m_UserID, " GetLatestOrderDetails : return true", MessageType.Informational);
                latestOrder = order;
                return true;
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, " GetLatestOrderDetails : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            latestOrder = order;
            return false;
        }

        public bool ModifyLimitOrder(string TradingSymbol, string TransactionType, decimal Price, string Exchange = Constants.EXCHANGE_NSE, string Product = Constants.PRODUCT_MIS, string Variety = Constants.VARIETY_REGULAR)
        {
            bool isModify = false;
            try
            {
                OrderComposite latestOrder;
                string orderQtyInString;
                string orderID;
                string parentOrderID;
                string orderType;
                Dictionary<string, dynamic> response = new Dictionary<string, dynamic>();
                if (GetLatestOrderDetails(TradingSymbol, Exchange, TransactionType, Constants.ORDER_STATUS_OPEN, out latestOrder))
                {
                    orderID = latestOrder.OrderId;
                    orderQtyInString = (latestOrder.displayNetQuantity).ToString();
                    orderType = latestOrder.OrderType;
                    parentOrderID = latestOrder.ParentOrderId;

                    if (orderType == Constants.ORDER_TYPE_LIMIT)
                    {
                        if (latestOrder.Status == Constants.ORDER_STATUS_OPEN)
                        {
                            response = composite.ModifyOrder(
                                                    OrderId: orderID,
                                                    ParentOrderId: parentOrderID,
                                                    Exchange: Exchange,
                                                    TradingSymbol: TradingSymbol,
                                                    TransactionType: TransactionType,
                                                    Quantity: orderQtyInString,
                                                    Price: Price,
                                                    OrderType: Constants.ORDER_TYPE_LIMIT,
                                                    Product: Product,
                                                    Variety: Variety);

                            isModify = true;
                            var orderId = response["data"]["order_id"];
                            var logMessage = "Order modified for " + Constants.PRODUCT_MIS + " Successfully. OrderId = " + orderId;
                            WriteUniquelogs(TradingSymbol + " " + m_UserID, logMessage, MessageType.Informational);
                        }
                    }
                }
                else
                {
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, "ModifyLimitOrder : Not able to fetch latest order", MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "ModifyLimitOrder : Exception Error Message" + e.Message, MessageType.Exception);
            }
            return isModify;
        }

        public bool ModifyStopOrder(string TradingSymbol, string TransactionType, decimal TriggerPrice, string Exchange = Constants.EXCHANGE_NSE, string Product = Constants.PRODUCT_MIS)
        {
            bool isModify = false;
            try
            {
                OrderComposite latestOrder;
                string orderQtyInString;
                string orderID;
                string orderType;
                Dictionary<string, dynamic> response = new Dictionary<string, dynamic>();
                if (GetLatestOrderDetails(TradingSymbol, Exchange, TransactionType, Constants.ORDER_STATUS_OPEN, out latestOrder))
                {
                    orderID = latestOrder.OrderId;
                    orderQtyInString = (latestOrder.Quantity).ToString();
                    orderType = latestOrder.OrderType;

                    if (orderType == "STOPMARKET")
                    {
                        if (latestOrder.Status == Constants.ORDER_STATUS_OPEN)
                        {
                            response = composite.ModifyOrder(
                                                    OrderId: orderID,
                                                    Exchange: Exchange,
                                                    TradingSymbol: TradingSymbol,
                                                    TransactionType: TransactionType,
                                                    Quantity: orderQtyInString,
                                                    TriggerPrice: TriggerPrice,
                                                    OrderType: "STOPMARKET",
                                                    Product: Product,
                                                    Price: latestOrder.Price,
                                                    DisclosedQuantity: latestOrder.displayNetQuantity);

                            isModify = true;
                            long orderId = response["result"]["AppOrderID"];
                            var logMessage = "Order modified for " + Constants.PRODUCT_MIS + " Successfully. OrderId = " + orderId;
                            WriteUniquelogs(TradingSymbol + " " + m_UserID, logMessage, MessageType.Informational);
                        }
                    }
                }
                else
                {
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, "ModifyStopOrder : Not able to fetch latest order", MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "ModifyStopOrder :  Exception Error Message" + e.Message, MessageType.Exception);
            }
            return isModify;
        }

        //IRDS::Jyoti::09-Aug-21::Added modify function to convert to MARKET order
        public bool ModifyStopOrderToMarket(string TradingSymbol, string TransactionType, decimal Price, decimal TriggerPrice, string OrderId, int Quantity, string Exchange = Constants.EXCHANGE_NSE, string Product = Constants.PRODUCT_MIS)
        {
            bool isModify = false;
            try
            {
                OrderComposite latestOrder;
                string orderQtyInString;
                string orderID;
                string orderType;
                Dictionary<string, dynamic> response = new Dictionary<string, dynamic>();
                //sanika::19-Jam-2021::passed order type because stop order and limit order has same status
                if (GetLatestOrderDetails(TradingSymbol, Exchange, TransactionType, "STOPLIMIT_" + Constants.ORDER_STATUS_OPEN, out latestOrder))
                {
                    orderID = OrderId;
                    orderQtyInString = Quantity.ToString();
                    orderType = latestOrder.OrderType;

                    if (orderType == "STOPLIMIT")
                    {
                        if (latestOrder.Status == Constants.ORDER_STATUS_OPEN || latestOrder.Status == "PARTIALLYFILLED")
                        {
                            WriteUniquelogs(TradingSymbol + " " + m_UserID, "orderID " + orderID + " Exchange " + Exchange + " TradingSymbol " + TradingSymbol + " TransactionType " + TransactionType + " orderQtyInString " + orderQtyInString + " TriggerPrice " + TriggerPrice + " Product " + Product, MessageType.Informational);
                            response = composite.ModifyOrder(
                                                    OrderId: orderID,
                                                    Exchange: Exchange,
                                                    TradingSymbol: TradingSymbol,
                                                    TransactionType: TransactionType,
                                                    Quantity: orderQtyInString,
                                                    TriggerPrice: TriggerPrice,
                                                    OrderType: "MARKET",
                                                    Product: Product,
                                                    Price: Price,
                                                    DisclosedQuantity: latestOrder.displayNetQuantity);

                            if (response != null)
                            {
                                isModify = true;
                                long orderId = response["result"]["AppOrderID"];
                                var logMessage = "Order modified for " + Constants.PRODUCT_MIS + " Successfully. OrderId = " + orderId;
                                WriteUniquelogs(TradingSymbol + " " + m_UserID, logMessage, MessageType.Informational);
                            }
                            else
                            {
                                isModify = false;
                                WriteUniquelogs(TradingSymbol + " " + m_UserID, "Not able to modify order", MessageType.Informational);
                            }
                        }
                        else
                        {
                            WriteUniquelogs(TradingSymbol + " " + m_UserID, "ModifyStopOrderToMarket : status found mismatch " + latestOrder.Status, MessageType.Informational);
                        }

                    }
                    else
                    {
                        WriteUniquelogs(TradingSymbol + " " + m_UserID, "ModifyStopOrderToMarket : Order Type LIMIT mismatch " + orderType, MessageType.Informational);
                    }
                }
                else
                {
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, "ModifyStopOrderToMarket : Not able to fetch latest order", MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "ModifyStopOrder :  Exception Error Message" + e.Message, MessageType.Exception);
            }
            return isModify;
        }

        public bool ModifyLimitOrderToMarket(string TradingSymbol, string TransactionType, decimal Price, decimal TriggerPrice, string OrderId, int Quantity, string Exchange = Constants.EXCHANGE_NSE, string Product = Constants.PRODUCT_MIS)
        {
            bool isModify = false;
            try
            {
                OrderComposite latestOrder;
                string orderQtyInString;
                string orderID;
                string orderType;
                Dictionary<string, dynamic> response = new Dictionary<string, dynamic>();
                //sanika::19-Jam-2021::passed order type because stop order and limit order has same status
                if (GetLatestOrderDetails(TradingSymbol, Exchange, TransactionType, "LIMIT_" + Constants.ORDER_STATUS_OPEN, out latestOrder))
                {
                    orderID = OrderId;
                    orderQtyInString = Quantity.ToString();
                    orderType = latestOrder.OrderType;

                    if (orderType == Constants.ORDER_TYPE_LIMIT)
                    {
                        if (latestOrder.Status == Constants.ORDER_STATUS_OPEN || latestOrder.Status == "PARTIALLYFILLED")
                        {
                            WriteUniquelogs(TradingSymbol + " " + m_UserID, "orderID " + orderID + " Exchange " + Exchange + " TradingSymbol " + TradingSymbol + " TransactionType " + TransactionType + " orderQtyInString " + orderQtyInString + " TriggerPrice " + TriggerPrice + " Product " + Product, MessageType.Informational);
                            response = composite.ModifyOrder(
                                                    OrderId: orderID,
                                                    Exchange: Exchange,
                                                    TradingSymbol: TradingSymbol,
                                                    TransactionType: TransactionType,
                                                    Quantity: orderQtyInString,
                                                    TriggerPrice: TriggerPrice,
                                                    OrderType: "MARKET",
                                                    Product: Product,
                                                    Price: Price,
                                                    DisclosedQuantity: latestOrder.displayNetQuantity);

                            if (response != null)
                            {
                                isModify = true;
                                long orderId = response["result"]["AppOrderID"];
                                var logMessage = "Order modified for " + Constants.PRODUCT_MIS + " Successfully. OrderId = " + orderId;
                                WriteUniquelogs(TradingSymbol + " " + m_UserID, logMessage, MessageType.Informational);
                            }
                            else
                            {
                                isModify = false;
                                WriteUniquelogs(TradingSymbol + " " + m_UserID, "Not able to modify order", MessageType.Informational);
                            }
                        }
                        else
                        {
                            WriteUniquelogs(TradingSymbol + " " + m_UserID, "ModifyLimitOrderToMarket : status found mismatch " + latestOrder.Status, MessageType.Informational);
                        }

                    }
                    else
                    {
                        WriteUniquelogs(TradingSymbol + " " + m_UserID, "ModifyLimitOrderToMarket : Order Type LIMIT mismatch " + orderType, MessageType.Informational);
                    }
                }
                else
                {
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, "ModifyLimitOrderToMarket : Not able to fetch latest order", MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "ModifyLimitOrderToMarket :  Exception Error Message" + e.Message, MessageType.Exception);
            }
            return isModify;
        }

        public bool ModifyStopOrderWithOrderId(string TradingSymbol, string TransactionType, decimal TriggerPrice, string OrderId, int Quantity, string Exchange = Constants.EXCHANGE_NSE, string Product = Constants.PRODUCT_MIS)
        {
            bool isModify = false;
            try
            {
                OrderComposite latestOrder;
                string orderQtyInString;
                string orderID;
                string orderType;
                Dictionary<string, dynamic> response = new Dictionary<string, dynamic>();
                //sanika::19-Jam-2021::passed order type because stop order and limit order has same status
                if (GetLatestOrderDetails(TradingSymbol, Exchange, TransactionType, "STOPMARKET_" + Constants.ORDER_STATUS_OPEN, out latestOrder))
                {
                    orderID = OrderId;
                    orderQtyInString = Quantity.ToString();
                    orderType = latestOrder.OrderType;

                    if (orderType == "STOPMARKET")
                    {
                        if (latestOrder.Status == Constants.ORDER_STATUS_OPEN)
                        {
                            WriteUniquelogs(TradingSymbol + " " + m_UserID, "orderID " + orderID + " Exchange " + Exchange + " TradingSymbol " + TradingSymbol + " TransactionType " + TransactionType + " orderQtyInString " + orderQtyInString + " TriggerPrice " + TriggerPrice + " Product " + Product, MessageType.Informational);
                            response = composite.ModifyOrder(
                                                    OrderId: orderID,
                                                    Exchange: Exchange,
                                                    TradingSymbol: TradingSymbol,
                                                    TransactionType: TransactionType,
                                                    Quantity: orderQtyInString,
                                                    TriggerPrice: TriggerPrice,
                                                    OrderType: "STOPMARKET",
                                                    Product: Product,
                                                    Price: latestOrder.Price,
                                                    DisclosedQuantity: latestOrder.displayNetQuantity);

                            if (response != null)
                            {
                                isModify = true;
                                long orderId = response["result"]["AppOrderID"];
                                var logMessage = "Order modified for " + Constants.PRODUCT_MIS + " Successfully. OrderId = " + orderId;
                                WriteUniquelogs(TradingSymbol + " " + m_UserID, logMessage, MessageType.Informational);
                            }
                            else
                            {
                                isModify = false;
                                WriteUniquelogs(TradingSymbol + " " + m_UserID, "Not able to modify order", MessageType.Informational);
                            }
                        }
                    }
                }
                else
                {
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, "ModifyStopOrder : Not able to fetch latest order", MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "ModifyStopOrder :  Exception Error Message" + e.Message, MessageType.Exception);
            }
            return isModify;
        }

        //sanika::26-Apr-2021::Added to modify sl order with order id
        public bool ModifySLOrderWithOrderId(string TradingSymbol, string TransactionType, decimal Price, decimal TriggerPrice, string OrderId, int Quantity, string Exchange = Constants.EXCHANGE_NSE, string Product = Constants.PRODUCT_MIS)
        {
            bool isModify = false;
            try
            {
                OrderComposite latestOrder;
                string orderQtyInString;
                string orderID;
                string orderType;
                Dictionary<string, dynamic> response = new Dictionary<string, dynamic>();
                //sanika::19-Jam-2021::passed order type because stop order and limit order has same status
                if (GetLatestOrderDetails(TradingSymbol, Exchange, TransactionType, "STOPLIMIT_" + Constants.ORDER_STATUS_OPEN, out latestOrder))
                {
                    orderID = OrderId;
                    orderQtyInString = Quantity.ToString();
                    orderType = latestOrder.OrderType;

                    if (orderType == "STOPLIMIT")
                    {
                        if (latestOrder.Status == Constants.ORDER_STATUS_OPEN)
                        {
                            WriteUniquelogs(TradingSymbol + " " + m_UserID, "orderID " + orderID + " Exchange " + Exchange + " TradingSymbol " + TradingSymbol + " TransactionType " + TransactionType + " orderQtyInString " + orderQtyInString + " TriggerPrice " + TriggerPrice + " Product " + Product, MessageType.Informational);
                            response = composite.ModifyOrder(
                                                    OrderId: orderID,
                                                    Exchange: Exchange,
                                                    TradingSymbol: TradingSymbol,
                                                    TransactionType: TransactionType,
                                                    Quantity: orderQtyInString,
                                                    TriggerPrice: TriggerPrice,
                                                    OrderType: "STOPLIMIT",
                                                    Product: Product,
                                                    Price: Price,
                                                    DisclosedQuantity: latestOrder.displayNetQuantity);

                            if (response != null)
                            {
                                isModify = true;
                                long orderId = response["result"]["AppOrderID"];
                                var logMessage = "Order modified for " + Constants.PRODUCT_MIS + " Successfully. OrderId = " + orderId;
                                WriteUniquelogs(TradingSymbol + " " + m_UserID, logMessage, MessageType.Informational);
                            }
                            else
                            {
                                isModify = false;
                                WriteUniquelogs(TradingSymbol + " " + m_UserID, "Not able to modify order", MessageType.Informational);
                            }
                        }
                    }
                }
                else
                {
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, "ModifyStopOrder : Not able to fetch latest order", MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "ModifyStopOrder :  Exception Error Message" + e.Message, MessageType.Exception);
            }
            return isModify;
        }

        public bool ModifyBOStopOrder(string TradingSymbol, string TransactionType, decimal TriggerPrice, string Exchange, string Product)
        {
            bool isModify = false;
            try
            {
                OrderComposite latestOrder;
                string orderQtyInString;
                string orderID;
                string orderType;
                string parentOrderID;
                Dictionary<string, dynamic> response = new Dictionary<string, dynamic>();
                if (GetLatestOrderDetails(TradingSymbol, Exchange, TransactionType, Constants.ORDER_STATUS_PENDING, out latestOrder))
                {
                    orderID = latestOrder.OrderId;
                    parentOrderID = latestOrder.ParentOrderId;
                    orderQtyInString = (latestOrder.Quantity).ToString();
                    orderType = latestOrder.OrderType;

                    if (orderType == Constants.ORDER_TYPE_SL)
                    {
                        if (latestOrder.Status == Constants.ORDER_STATUS_PENDING)
                        {
                            response = composite.ModifyOrder(
                                                    OrderId: orderID,
                                                    ParentOrderId: parentOrderID,
                                                    Exchange: Exchange,
                                                    TradingSymbol: TradingSymbol,
                                                    TransactionType: TransactionType,
                                                    Quantity: orderQtyInString,
                                                    TriggerPrice: TriggerPrice,
                                                    OrderType: Constants.ORDER_TYPE_SL,
                                                    Product: Product);

                            isModify = true;
                            var orderId = response["data"]["order_id"];
                            var logMessage = "Order modified for " + Constants.PRODUCT_MIS + " Successfully. OrderId = " + orderId;
                            WriteUniquelogs(TradingSymbol + " " + m_UserID, logMessage, MessageType.Informational);
                        }
                    }
                }
                else
                {
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, "ModifyBOStopOrder : Not able to fetch latest order", MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "ModifyBOStopOrder : Exception Error Message" + e.Message, MessageType.Exception);
            }
            return isModify;
        }

        public bool CancelAllPendingOrder(string TradingSymbol = "", string Exchange = "")
        {
            //IRDS::Jyoti::02-Aug-21::Added to avoid market order if cancel order failed
            bool flagToReturn = false;
            try
            {
                List<OrderComposite> orderInfo = new List<OrderComposite>();
                if (TradingSymbol != "" && Exchange != "")//Sanika::19-Jan-2021::Added exchange in condition because orderinfo gets 0
                {
                    orderInfo = m_GlobalOrderStore.GetOrderbySymbol(TradingSymbol, Exchange);
                }
                else
                {
                    orderInfo = m_GlobalOrderStore.GetAllOrder();
                }

                if (orderInfo.Count == 0)
                {
                    string errorMsg = "CancelAllPendingOrder: Not Found any previous order - " + orderInfo.Count.ToString();
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, errorMsg, MessageType.Error);

                    //latestOrder = new IRDSAlgoOMS.OrderComposite();
                    return true;
                }
                for (int iCount = 0; iCount < orderInfo.Count; iCount++)
                {
                    flagToReturn = false;
                    OrderComposite latestOrder = orderInfo[iCount];
                    if (latestOrder.Status == Constants.ORDER_STATUS_CANCELLED || latestOrder.Status == Constants.ORDER_STATUS_COMPLETE || latestOrder.Status == Constants.ORDER_STATUS_REJECTED)
                    {
                        //WriteUniquelogs(TradingSymbol + " " + m_UserID, "CancelAllPendingOrder: " + TradingSymbol + " Previous Order Status : " + latestOrder.Status + " Cancel or Complete or Rejected Status", MessageType.Informational);
                        flagToReturn = true;
                    }
                    else
                    {
                        string orderID = latestOrder.OrderId;
                        string ProductType = latestOrder.Product.ToLower();
                        if (ProductType != "bo" && ProductType != "co")
                        {
                            ProductType = Constants.VARIETY_REGULAR;
                        }
                        var getCancelOrder = CancelOrderEX(orderID, ProductType);
                        //sanika::09-July-2021::added condition
                        if (getCancelOrder != null)
                            flagToReturn = true;
                    }
                }
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "CancelAllPendingOrder : Return true", MessageType.Informational);
                WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + TradingSymbol + " Cancel Order successfully!!");
                m_ExceptionCounter++;
                return flagToReturn;
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "CancelAllPendingOrder :  Exception Error Message " + e.Message, MessageType.Exception);
                WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + TradingSymbol + " " + e.Message);
                //m_ExceptionDictionary.Add(m_ExceptionCounter, DateTime.Now + " " + "CancelOrder - " + TradingSymbol + " " + e.Message);
                m_ExceptionCounter++;
            }
            return false;
        }

        public bool CancelOrder(string orderId, string productType, string symbol)
        {
            bool isCancelOrder = false;
            try
            {
                string ProductType = productType.ToLower();
                if (ProductType != "bo" && ProductType != "co")
                {
                    ProductType = Constants.VARIETY_REGULAR;
                }
                var getCancelOrder = CancelOrderEX(orderId, ProductType);
                //sanika::29-Jul-2021::added check for reaponse to return bool
                if (getCancelOrder == null)
                    isCancelOrder = false;
                else
                    isCancelOrder = true;
                WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + symbol + " Cancelled successfully!");
                m_ExceptionCounter++;
                //isCancelOrder = true;
            }
            catch (Exception e)
            {
                WriteUniquelogs(symbol + " " + m_UserID, "CancelOrder :  Exception Error Message " + e.Message, MessageType.Exception);
                WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + e.Message);
                m_ExceptionCounter++;
            }

            return isCancelOrder;
        }


        public void ForceFulReLogin()
        {
            try
            {
                composite.initSeesion();
                if (composite.m_ForceFulLoginFlag)
                {
                    logger.LogMessage(m_UserID + "Forceful Relogin initSeesion successful.", MessageType.Informational);

                    //IRDS::9-oct-2019::Sanika::Added if login failed then also call this two methods
                    WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + "Re-connected!!");
                    m_ExceptionCounter++;
                    composite.SetAccessToken(composite.accessToken);
                    logger.LogMessage("SetAccessToken successful.", MessageType.Informational);
                    //04-August-2021: sandip:memory issue
                    //// initialize ticker
                    //if (ticker != null)
                    //{
                    //    ticker = new TickerComposite();
                    //}

                }
                else
                {
                    logger.LogMessage(m_UserID + "Forceful Relogin initSeesion Unsuccessful.", MessageType.Informational);
                    // m_ExceptionDictionary.Add(m_ExceptionCounter, DateTime.Now + " " + "Not able to Re-connected!!");
                    WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + "Not able to Re-connected!!");
                    m_ExceptionCounter++;
                }
            }
            catch (Exception e)
            {
                logger.LogMessage(m_UserID + "Forceful Relogin initSeesion unsuccessful.", MessageType.Exception);
            }
        }

        public void WriteUniquelogs(string symbol, string message, MessageType msgType)
        {
            try
            {
                SymbolLogs log = new SymbolLogs();
                //log = SymbolLogs.InstanceSymbolLogs;
                log.createFile(symbol);
                log.LogMessage(message, MessageType.Informational);
                log = null;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : " + e.Message);
                Trace.WriteLine("Exception : " + e.Message);
            }
        }
        //04-August-2021: sandip:memory issue
        public void WriteUniqueMemorylogs(string symbol, string message, MessageType msgType)
        {
            try
            {
                SymbolLogs log = new SymbolLogs();
                //log = SymbolLogs.InstanceSymbolLogs;
                log.createFile(symbol);
                log.LogMemoryMessage(message, MessageType.Informational, true);
                log = null;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : " + e.Message);
                Trace.WriteLine("Exception : " + e.Message);
            }
        }
        public bool CloseBOOrder(string TradingSymbol, string Exchange)
        {

            bool isClose = false;
            try
            {
                List<OrderComposite> orderInfo = m_GlobalOrderStore.GetOrderbySymbol(TradingSymbol, Exchange);

                if (orderInfo.Count == 0)
                {
                    string errorMsg = "CloseBOOrder: Not Found any previous order - " + orderInfo.Count.ToString();
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, errorMsg, MessageType.Informational);
                    //sanika::28-Jul-2021::No need to create global variable
                    OrderComposite latestOrder = new OrderComposite();
                    latestOrder.Status = Constants.ORDER_STATUS_CANCELLED;
                    return true;
                }
                for (int iCount = 0; iCount < orderInfo.Count; iCount++)
                {
                    OrderComposite latestOrder = orderInfo[iCount];
                    if (latestOrder.Status == Constants.ORDER_STATUS_CANCELLED || latestOrder.Status == Constants.ORDER_STATUS_COMPLETE || latestOrder.Status == Constants.ORDER_STATUS_REJECTED)
                    {
                        //nothing to do
                        isClose = true;
                    }
                    else
                    {
                        string orderID = latestOrder.OrderId;
                        var getCancelOrder = composite.CancelOrder(orderID, latestOrder.userId, latestOrder.UniqueId);
                        List<OrderComposite> orderInfoAfterExit = m_GlobalOrderStore.GetOrderbySymbol(TradingSymbol, Exchange);
                        if (orderInfoAfterExit.Count != 0)
                        {
                            for (int iCounter = 0; iCounter < orderInfoAfterExit.Count; iCounter++)
                            {
                                OrderComposite latestOrderAfterExit = orderInfoAfterExit[iCounter];
                            }
                        }
                        isClose = true;
                        WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseBOOrder :" + TradingSymbol + "Successfuly exit", MessageType.Informational);
                    }
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseBOOrder : Exception Error Message " + e.Message, MessageType.Informational);
            }
            return isClose;
        }

        //public bool CloseAllOrder()
        //{
        //    bool isClose = false;

        //    List<OrderComposite> orderInfo = composite.GetOrderHistory();
        //    if (orderInfo.Count == 0)
        //    {
        //        //latestOrder = new IRDSAlgoOMS.OrderComposite();
        //        //latestOrder.Status = Constants.ORDER_STATUS_CANCELLED;
        //        latestOrder.Status = "Cancelled";
        //        return true;
        //    }
        //    for (int iCount = 0; iCount < orderInfo.Count; iCount++)
        //    {
        //        try
        //        {
        //            OrderComposite latestOrder = orderInfo[iCount];

        //            //market
        //            if (latestOrder.OrderType == Constants.ORDER_TYPE_SL || latestOrder.OrderType == "MKT" || latestOrder.OrderType == Constants.ORDER_TYPE_SLM)
        //            {
        //                if (latestOrder.Status == "Cancelled" || latestOrder.Status == "Rejected")
        //                {
        //                }
        //                else
        //                {
        //                    string TradingSymbol = latestOrder.Tradingsymbol;
        //                    string Product = latestOrder.Product;
        //                    string Exchange = latestOrder.Exchange;
        //                    if (CloseOrder(TradingSymbol, Exchange, Product))
        //                    {
        //                        isClose = true;
        //                    }
        //                    else
        //                    {
        //                        //WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseAllOrder :"+ TradingSymbol + "Not able to exit", MessageType.Informational);
        //                    }
        //                }
        //            }
        //            else //bo
        //            {
        //                if (latestOrder.Status == "Cancelled" || latestOrder.Status == "Complete" || latestOrder.Status == "Rejected")
        //                {
        //                    isClose = true;
        //                }
        //                else
        //                {
        //                    string orderID = latestOrder.OrderId;
        //                    string TradingSymbol = latestOrder.Tradingsymbol;
        //                    string Exchange = latestOrder.Exchange;
        //                    var getCancelOrder = composite.CancelOrder(orderID, latestOrder.userId, latestOrder.UniqueId);
        //                    List<OrderComposite> orderInfoAfterExit = m_GlobalOrderStore.GetOrderbySymbol(TradingSymbol, Exchange);
        //                    if (orderInfoAfterExit.Count != 0)
        //                    {
        //                        for (int iCounter = 0; iCounter < orderInfoAfterExit.Count; iCounter++)
        //                        {
        //                            OrderComposite latestOrderAfterExit = orderInfoAfterExit[iCounter];
        //                            //WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseAllOrder : " + TradingSymbol + "After exit Previous Order Status : " + latestOrderAfterExit.Status + " Order is cancelled", MessageType.Informational);
        //                        }
        //                    }
        //                    isClose = true;
        //                    WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseAllOrder :" + TradingSymbol + "Successfuly exit", MessageType.Informational);
        //                }
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            //WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseAllOrder :" + TradingSymbol + "Successfuly exit", MessageType.Informational);
        //        }
        //    }
        //    return isClose;
        //}

        //sanika::4-Feb-2021::Parameters order was wrong
        // public bool CloseOrder(string TradingSymbol, string Product, string Exchange)
        public bool CloseOrder(string TradingSymbol, string Exchange, string Product)
        {
            try
            {

                string newOrderBuyOrSell = "";
                int newOrderQty = 0;
                string mappedSymbol = TradingSymbol + "|" + Exchange;
                //sanika::15-Jun-2021::exchange missing in parameter , passed exchange 
                if (CancelAllPendingOrder(TradingSymbol, Exchange))
                {
                    PositionInfoComposite currentPosition;
                    if (!GetCurrentPosition(mappedSymbol, Product, out currentPosition))
                    {
                        string errorMsg = string.Format("CloseOrder : Not found current position for symbol : {0}", mappedSymbol);
                        WriteUniquelogs(TradingSymbol + " " + m_UserID, errorMsg, MessageType.Error);

                        if (bGetCurrentPositionError)
                        {
                            return false;
                        }
                    }
                    else
                    {
                        string direction = "";
                        string logMessage = string.Format("CloseOrder : Current position fetched from server for mapped symbol: {0} Net Position: {1}",
                                                      mappedSymbol, currentPosition.netPosition.Quantity);
                        WriteUniquelogs(TradingSymbol + " " + m_UserID, logMessage, MessageType.Informational);
                        if ((currentPosition.netPosition.Quantity > 0))
                        {
                            newOrderBuyOrSell = Constants.TRANSACTION_TYPE_SELL;
                            direction = Constants.TRANSACTION_TYPE_BUY;
                        }
                        else if ((currentPosition.netPosition.Quantity < 0))
                        {
                            newOrderBuyOrSell = Constants.TRANSACTION_TYPE_BUY;
                            direction = Constants.TRANSACTION_TYPE_SELL;
                        }

                        //IRDS::17-JUL-2020 :: Add comment because day position quantity got wrong :: sandip/sanika

                        //if (Math.Abs(currentPosition.dayPosition.Quantity) > Math.Abs(currentPosition.netPosition.Quantity))
                        //    newOrderQty = Math.Abs(currentPosition.dayPosition.Quantity);
                        //else

                        newOrderQty = Math.Abs(currentPosition.netPosition.Quantity);

                        if (newOrderQty != 0)
                        {
                            WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseOrder :newOrderQty is not zero" + newOrderQty, MessageType.Informational);
                            string orderID = PlaceOrder(Exchange, TradingSymbol, newOrderBuyOrSell, newOrderQty, Constants.ORDER_TYPE_MARKET, Product: Product);
                            if (orderID != "NA")
                            {
                                double openPrice = GetOpenPostionPricebyOrderID(TradingSymbol, Exchange, orderID);
                                WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseOrder : Order Complete at price = " + openPrice, MessageType.Informational);
                                WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseOrder : placed order successfully", MessageType.Informational);
                                WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + TradingSymbol + " " + direction + " Order Closed successfully!! ");
                                m_ExceptionCounter++;
                                DateTime dateTime = DateTime.Now;
                                m_LocalOrderStructureInfo.AddOrUpdateOrderInformation(TradingSymbol: TradingSymbol, Exchange: Exchange, ClosedTransactionType: newOrderBuyOrSell, Product: Product, ClosedQuantity: newOrderQty, ClosedOrderDateTime: dateTime, ClosedOrderId: orderID);
                                return true;
                            }
                            else
                            {
                                WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseOrder : order not closed", MessageType.Informational);
                                // m_ExceptionDictionary.Add(m_ExceptionCounter, DateTime.Now + " " + "CloseOrder - " + TradingSymbol + " Order not Closed");
                                //sanika::28-sep-2020::added direction in logs/toast
                                WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + TradingSymbol + " " + direction + " Order not Closed");
                                m_ExceptionCounter++;
                            }
                        }
                        else
                        {
                            WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseOrder :newOrderQty is zero", MessageType.Informational);
                            // m_ExceptionDictionary.Add(m_ExceptionCounter, DateTime.Now + " " + "CloseOrder - " + TradingSymbol + " Order closed already!!");
                            WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + TradingSymbol + " Order closed already!!");
                            m_ExceptionCounter++;
                        }
                    }
                }
                else
                {
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseOrder :  Unable to cancel order so did not proceed to close order", MessageType.Exception);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseOrder :  Exception Error Message " + e.Message, MessageType.Exception);
            }
            return false;
        }

        public bool CloseOrderWithQuantity(string TradingSymbol, string Exchange, string Product, int Quantity)
        {
            try
            {
                string newOrderBuyOrSell = "";
                int newOrderQty = 0;
                string mappedSymbol = TradingSymbol + "|" + Exchange;
                if (CancelAllPendingOrder(TradingSymbol, Exchange))
                {
                    PositionInfoComposite currentPosition;
                    if (!GetCurrentPosition(mappedSymbol, Product, out currentPosition))
                    {
                        string errorMsg = string.Format("CloseOrderWithQuantity : Not found current position for symbol : {0}", mappedSymbol);
                        WriteUniquelogs(TradingSymbol + " " + m_UserID, errorMsg, MessageType.Error);

                        if (bGetCurrentPositionError)
                        {
                            return false;
                        }
                    }
                    else
                    {
                        string logMessage = string.Format("CloseOrderWithQuantity : Current position fetched from server for mapped symbol: {0}, Net Position: {1}",
                                                      mappedSymbol, currentPosition.netPosition.Quantity);
                        WriteUniquelogs(TradingSymbol + " " + m_UserID, logMessage, MessageType.Informational);
                        if ((currentPosition.netPosition.Quantity > 0))
                        {
                            newOrderBuyOrSell = Constants.TRANSACTION_TYPE_SELL;
                        }
                        else if ((currentPosition.netPosition.Quantity < 0))
                        {
                            newOrderBuyOrSell = Constants.TRANSACTION_TYPE_BUY;
                        }

                        //IRDS::17-JUL-2020 :: Add comment because day position quantity got wrong :: sandip/sanika

                        //if (Math.Abs(currentPosition.dayPosition.Quantity) > Math.Abs(currentPosition.netPosition.Quantity))
                        //    newOrderQty = Math.Abs(currentPosition.dayPosition.Quantity);
                        //else
                        if (Quantity == 0)
                        {
                            newOrderQty = Math.Abs(currentPosition.netPosition.Quantity);
                        }
                        else if (Quantity > Math.Abs(currentPosition.netPosition.Quantity))
                        {
                            newOrderQty = Math.Abs(currentPosition.netPosition.Quantity);
                        }
                        else
                        {
                            newOrderQty = Quantity;
                        }

                        if (newOrderQty != 0)
                        {
                            WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseOrderWithQuantity :newOrderQty is not zero" + newOrderQty, MessageType.Informational);
                            string orderID = PlaceOrder(Exchange, TradingSymbol, newOrderBuyOrSell, newOrderQty, Constants.ORDER_TYPE_MARKET, Product: Product);
                            if (orderID != "NA")
                            {
                                double openPrice = GetOpenPostionPricebyOrderID(TradingSymbol, Exchange, orderID);
                                WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseOrderWithQuantity : Order Complete at price = " + openPrice, MessageType.Informational);
                                WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseOrderWithQuantity : placed order successfully", MessageType.Informational);
                                //m_ExceptionDictionary.Add(m_ExceptionCounter, DateTime.Now + " " + "CloseOrderWithQuantity - " + TradingSymbol + " Order Closed successfully!! ");
                                WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + TradingSymbol + " Order Closed successfully!! ");
                                m_ExceptionCounter++;

                                DateTime dateTime = DateTime.Now;
                                m_LocalOrderStructureInfo.AddOrUpdateOrderInformation(TradingSymbol: TradingSymbol, Exchange: Exchange, ClosedTransactionType: newOrderBuyOrSell, Product: Product, ClosedQuantity: newOrderQty, ClosedOrderDateTime: dateTime, ClosedOrderId: orderID);
                                return true;
                            }
                        }
                        else
                        {
                            WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseOrderWithQuantity :newOrderQty is zero", MessageType.Informational);
                            //m_ExceptionDictionary.Add(m_ExceptionCounter, DateTime.Now + " " + "CloseOrderWithQuantity - " + TradingSymbol + " Order closed already!!");
                            WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + TradingSymbol + " Order closed already!!");
                            m_ExceptionCounter++;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseOrderWithQuantity :  Exception Error Message " + e.Message, MessageType.Exception);
            }
            return false;
        }

        public string PlaceOrder(string Exchange,
                               string TradingSymbol,
                               string TransactionType,
                               int Quantity,
                               string OrderType,
                               decimal? Price = null,
                               string Product = Constants.PRODUCT_MIS,
                               string Validity = Constants.VALIDITY_DAY,
                               int? DisclosedQuantity = null,
                               decimal? TriggerPrice = null,
                               decimal? SquareOffValue = null,
                               decimal? StoplossValue = null,
                               decimal? TrailingStoploss = null,
                               string Variety = Constants.VARIETY_REGULAR,
                               string Tag = "")
        {
            Trace.WriteLine("Time#### place order main function start " + DateTime.Now);
            string orderId = "NA";
            string errorMessage = "";
            try
            {
                if (m_isSqrOff == false)
                {
                    if (CheckRiskQuantity(TradingSymbol, TransactionType) == false)
                    {
                        WriteUniquelogs(TradingSymbol + " " + m_UserID, "PlaceOrder : Exchange " + Exchange + " TradingSymbol " + TradingSymbol + " TransactionType " + TransactionType + "Quantity " + Quantity + " TriggerPrice " + TriggerPrice + " StoplossValue " + StoplossValue + " Variety " + Variety, MessageType.Informational);
                        WriteUniquelogs(TradingSymbol + " " + m_UserID, "PlaceOrder : OrderType " + OrderType + " Price " + Price + " Product " + Product + "Validity " + Validity + " SquareOffValue " + SquareOffValue + " TrailingStoploss " + TrailingStoploss + " Tag " + Tag, MessageType.Informational);
                        Dictionary<string, dynamic> response = new Dictionary<string, dynamic>();
                        if (Price == null)
                            Price = 0;
                        if (TriggerPrice == null)
                            TriggerPrice = 0;
                        OrdersPlaced order = new OrdersPlaced();
                        order.TradingSymbol = TradingSymbol;
                        order.Exchange = Exchange;
                        order.TransactionType = TransactionType;
                        order.TriggerPrice = (decimal)TriggerPrice;
                        order.Price = (decimal)Price;
                        order.Product = Product;
                        order.Quantity = Quantity;
                        order.OrderType = OrderType;

                        if (IsOrderLimitsValid(order))
                        {
                            Trace.WriteLine("Time#### get time " + DateTime.Now);
                            DateTime date1 = DateTime.Now;
                            Trace.WriteLine("Time#### end time " + DateTime.Now);
                            Trace.WriteLine("Time#### Composite api call " + DateTime.Now);
                            WriteUniquelogs(TradingSymbol + " " + m_UserID, "Composite api call", MessageType.Informational);
                            response = composite.PlaceOrder(Exchange: Exchange,
                                                        TradingSymbol: TradingSymbol,
                                                        TransactionType: TransactionType,
                                                        Quantity: Quantity,
                                                        OrderType: OrderType,
                                                        Price: Price,
                                                        Product: Product,
                                                        Validity: Validity,
                                                        DisclosedQuantity: DisclosedQuantity,
                                                        TriggerPrice: TriggerPrice,
                                                        SquareOffValue: SquareOffValue,
                                                        StoplossValue: StoplossValue,
                                                        TrailingStoploss: TrailingStoploss,
                                                        Variety: Variety,
                                                        orderUniqueIdentifier: GenerateOrderTag());
                            WriteUniquelogs(TradingSymbol + " " + m_UserID, "Composite api end", MessageType.Informational);
                            Trace.WriteLine("Time#### composite api end " + DateTime.Now);
                            //sanika::29-Apr-2021::Added condition for if response fetch as null
                            long Id = 0;
                            if (response != null)
                                Id = response["result"]["AppOrderID"];
                            orderId = Id.ToString();
                            WriteUniquelogs(TradingSymbol + " " + m_UserID, "orderId " + orderId, MessageType.Informational);
                            Trace.WriteLine("Time#### get time2 " + DateTime.Now);
                            DateTime date2 = DateTime.Now;
                            Trace.WriteLine("Time#### end time2 " + DateTime.Now);
                            Trace.WriteLine("Time#### milliseconds calculation " + DateTime.Now);
                            double millisecond = date2.Subtract(date1).TotalMilliseconds;
                            Trace.WriteLine("Time#### milliseconds calculation " + DateTime.Now);
                            WriteUniquelogs(TradingSymbol + " " + m_UserID, "Time to place order in milliseconds " + millisecond, MessageType.Informational);
                            WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + TradingSymbol + " " + TransactionType + " " + orderId + " Order Placed successfully!! ");
                            m_ExceptionCounter++;
                            int counter = 0;
                            OrderComposite latestOrder = new OrderComposite();
                            date1 = DateTime.Now;
                            GetLatestOrder(TradingSymbol, Exchange, orderId, out latestOrder);
                            //IRDS::Jyoti::02-Aug-21::Removed latestOrder.OrderId == null condition as getting null reference exception
                            while (latestOrder == null && counter < 5)
                            {
                                GetLatestOrder(TradingSymbol, Exchange, orderId, out latestOrder);
                                Thread.Sleep(10);
                                counter++;
                            }
                            date2 = DateTime.Now;
                            millisecond = date2.Subtract(date1).TotalMilliseconds;
                            WriteUniquelogs(TradingSymbol + " " + m_UserID, "Time to get order status in milliseconds " + millisecond, MessageType.Informational);

                            //     Trace.WriteLine("Time#### end status fetching " + DateTime.Now);
                            string orderStatus = "";
                            if (latestOrder != null)
                                orderStatus = latestOrder.Status;
                            //   Trace.WriteLine("placeorder ended");
                            if (orderStatus == Constants.ORDER_STATUS_REJECTED || orderStatus == Constants.ORDER_STATUS_CANCELLED)
                            {
                                WriteUniquelogs(TradingSymbol + " " + m_UserID, "order status rejected", MessageType.Informational);
                                return orderId;
                            }
                            else
                            {
                                if (m_DictOrdersPlaced.ContainsKey(TradingSymbol + "_" + Exchange))
                                {
                                    m_DictOrdersPlaced[TradingSymbol + "_" + Exchange].Add(order);
                                }
                                else
                                {
                                    List<OrdersPlaced> orders = new List<OrdersPlaced>();
                                    orders.Add(order);
                                    m_DictOrdersPlaced.Add(TradingSymbol + "_" + Exchange, orders);
                                }
                            }
                            //sanika::8-dec-2020::Added to add counter in master list
                            if (m_RiskManagmentFlag == true)
                            {
                                AddManuallyOrderQuantity(TradingSymbol, Quantity);
                            }

                            Trace.WriteLine("Time#### end order information adding " + DateTime.Now);
                        }
                        else
                        {
                            WriteUniquelogs(TradingSymbol + " " + m_UserID, "PlaceOrder : IsOrderLimitsValid returns false as order limit exceeds ", MessageType.Informational);
                        }
                    }
                    else
                    {
                        WriteUniquelogs(TradingSymbol + " " + m_UserID, "PlaceOrder : order not placed!! \n Risk managment condition applied ", MessageType.Informational);
                        // m_ExceptionDictionary.Add(m_ExceptionCounter, DateTime.Now + " " + "PlaceOrder - " + TradingSymbol + " order not placed!! Trading Stopped!!");
                        WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + TradingSymbol + " order not placed!!");
                        m_ExceptionCounter++;
                    }
                }
                else
                {
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, "PlaceOrder : order not placed!! You haved clicked on SqrOff", MessageType.Informational);
                    // m_ExceptionDictionary.Add(m_ExceptionCounter, DateTime.Now + " " + "PlaceOrder - " + TradingSymbol + " order not placed!! Trading Stopped!!");
                    WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + TradingSymbol + " order not placed!! Trading Stopped!!");
                    m_ExceptionCounter++;
                }
                //sanika::13-July-2021::set flag of margin updation after placing order
                m_IsMarginLoad = true;
                composite.SetOrderUpdateStatus(true);
                return orderId;
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, " Exception Error Message = " + e.Message, MessageType.Exception);
                errorMessage = e.Message;
            }
            finally
            {
                if (orderId == "" && errorMessage != "")
                {
                    if (errorMessage.Contains("Trigger price") && errorMessage.Contains("stoploss") && (errorMessage.Contains("lower") || errorMessage.Contains("higher")) && errorMessage.Contains("last traded price") && errorMessage.Contains("Try limit order"))
                    {
                        orderId = "ErrorMessageForStopOrder";
                    }
                }
            }
            Trace.WriteLine("Time#### place order main function End " + DateTime.Now);
            return orderId;
        }

        public bool IsPendingOrder(string TradingSymbol, string Exchange, string TransactionType, string status, string Product, bool isPendingOrderToOpenOrder)
        {
            bool flagToReturn = false;
            try
            {
                List<OrderComposite> orderInfo = m_GlobalOrderStore.GetOrderbySymbol(TradingSymbol, Exchange);
                if (orderInfo.Count == 0)
                {
                    //WriteUniquelogs(TradingSymbol + " " + m_UserID, "IsPendingOrder : order info count is zero", MessageType.Informational);
                    return false;
                }
                //sanika::19-Jan-2021::checked order type also because stop and limit order has same order status
                string orderType = "";
                string orderStatus = "";
                if (status.Contains("_"))
                {
                    orderType = status.Split('_')[0];
                    orderStatus = status.Split('_')[1];
                }
                else
                {
                    orderStatus = status;
                }
                //sanika::28-Jul-2021::No need to create global variable
                OrderComposite latestOrder = new OrderComposite();
                for (int i = 0; i < orderInfo.Count(); i++)
                {
                    latestOrder = orderInfo[i];
                    if (latestOrder.TransactionType == TransactionType && latestOrder.Product == Product && (latestOrder.OrderType == orderType || orderType == ""))
                    {
                        if (latestOrder.Status == orderStatus)
                        {
                            //if ((!isPendingOrderToOpenOrder) && (latestOrder.ParentOrderId == null))
                            //{
                            //    flagToReturn = true;
                            //    break;
                            //}
                            //else if ((isPendingOrderToOpenOrder) && (latestOrder.ParentOrderId != null))
                            //{
                            //    flagToReturn = true;
                            //    break;
                            //}

                            flagToReturn = true;
                            break;
                        }
                    }
                }
                // WriteUniquelogs(TradingSymbol + " " + m_UserID, "IsPendingOrder : Return " + flagToReturn + " for "+ TransactionType + " order ", MessageType.Informational);
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "IsPendingOrder : Exception Error Message " + e.Message, MessageType.Exception);
            }
            return flagToReturn;
        }

        public bool cancelPendingOrderWithTransactionType(string TradingSymbol, string Exchange, string TransactionType, string variety, string Status, bool isCancelToOpenOrder)
        {
            bool flagToReturn = false;
            try
            {
                List<OrderComposite> orderInfo = m_GlobalOrderStore.GetOrderbySymbol(TradingSymbol, Exchange);

                if (orderInfo.Count == 0)
                {
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, "cancel_Pending_Order_WithTransactionType : order info count is zero, TradingSymbol : " + TradingSymbol + " Exchange: " + Exchange, MessageType.Informational);
                    return false;
                }
                //sanika::22-Jan-2021::limit and pending order has same status
                string orderStatus = "";
                string orderType = "";
                if (Status.Contains("_"))
                {
                    orderType = Status.Split('_')[0];
                    orderStatus = Status.Split('_')[1];
                }
                else
                {
                    orderStatus = Status;
                }
                //sanika::28-Jul-2021::No need to create global variable
                OrderComposite latestOrder = new OrderComposite();
                for (int i = 0; i < orderInfo.Count(); i++)
                {
                    latestOrder = orderInfo[i];
                    if (latestOrder.TransactionType == TransactionType)
                    {
                        //if (latestOrder.Status == Constants.ORDER_STATUS_PENDING || latestOrder.Status == Constants.ORDER_STATUS_OPEN)
                        if (latestOrder.Status == orderStatus && ((orderType == latestOrder.OrderType) || (orderType == "")))
                        {
                            if ((!isCancelToOpenOrder) && (latestOrder.ParentOrderId == ""))
                            {
                                string orderId = latestOrder.OrderId;
                                string ProductType = variety;// latestOrder.Product.ToLower();
                                var getCancelOrder = composite.CancelOrder(orderId, latestOrder.userId, latestOrder.UniqueId);
                                //sanika::09-July-2021::added condition
                                if (getCancelOrder != null)
                                    flagToReturn = true;
                                //break;
                            }
                            else if ((isCancelToOpenOrder) && (latestOrder.ParentOrderId != ""))
                            {
                                string orderId = latestOrder.OrderId;
                                string ProductType = latestOrder.Product.ToLower();
                                var getCancelOrder = composite.CancelOrder(orderId, latestOrder.userId, latestOrder.UniqueId);
                                //sanika::09-July-2021::added condition
                                if (getCancelOrder != null)
                                    flagToReturn = true;
                                //break;
                            }
                        }
                    }
                }
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "cancel_Pending_Order_WithTransactionType : Return " + flagToReturn, MessageType.Informational);
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "cancel_Pending_Order_WithTransactionType : Exception" + e.Message, MessageType.Exception);
            }
            return flagToReturn;
        }

        public bool SendEmail()
        {
            try
            {
                MailMessage message = new MailMessage();

                SmtpClient smtp = new SmtpClient();

                string SmtpAddress = "smtp.gmail.com";//fileinformation.IniReadValue(filepath, "MAILSETTINGS", "SMTPAddress");
                string portNumber = "587";//fileinformation.IniReadValue(filepath, "MAILSETTINGS", "PortNumber");
                string EmailFrom = "irdsdevteam@gmail.com";//fileinformation.IniReadValue(filepath, "MAILSETTINGS", "EmailFrom");
                string EmailPassword = "Irds2017";//fileinformation.IniReadValue(filepath, "MAILSETTINGS", "Password");
                string EmailTo = "barsawade.sanika15@gmail.com,sanika.shaonomy@gmail.com";//fileinformation.IniReadValue(filepath, "MAILSETTINGS", "EmailTo");
                string EmailCc = "";//fileinformation.IniReadValue(filepath, "MAILSETTINGS", "EmailCC");
                string EmailBcc = ""; //fileinformation.IniReadValue(filepath, "MAILSETTINGS", "EmailBcc");
                string EmailSubject = "Testing Purpose";//fileinformation.IniReadValue(filepath, "MAILSETTINGS", "Subject");
                string EmailBody = "Testing Purpose";
                if (EmailPassword == "")
                {
                    //
                }
                else
                {
                    smtp.EnableSsl = true;
                    if (SmtpAddress == "")
                    {
                        smtp.Host = "smtp.gmail.com";
                    }
                    else
                    {
                        smtp.Host = SmtpAddress;
                    }
                    if (portNumber == "")
                    {
                        smtp.Port = 25;
                    }
                    else
                    {
                        smtp.Port = Convert.ToInt32(portNumber);
                    }
                    if (EmailFrom == "")
                    {

                    }
                    else
                    {
                        message.From = new MailAddress(EmailFrom);
                    }
                    if (EmailTo == "")
                    {

                    }
                    else
                    {
                        message.To.Add(EmailTo);

                    }
                    if (EmailCc == "")
                    {
                    }
                    else
                    {
                        message.CC.Add(new MailAddress(EmailCc));
                    }
                    if (EmailBcc == "")
                    {
                    }
                    else
                    {
                        message.Bcc.Add(new MailAddress(EmailBcc));
                    }
                    if (EmailSubject != "")
                    {
                        message.Subject = EmailSubject;
                    }
                    else
                    {
                        message.Subject = "ODIN Order details";
                    }
                    if (EmailBody != "")
                    {
                        message.Body = EmailBody;
                    }
                    else
                    {

                    }
                    smtp.UseDefaultCredentials = false;


                    if (EmailFrom != "" && EmailPassword != "")
                    {
                        smtp.Credentials = new NetworkCredential(EmailFrom, EmailPassword);
                    }
                    try
                    {
                        smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

                        if (EmailTo != "")
                        {
                            smtp.Send(message);
                            return true;
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }

            }
            catch (Exception ex)
            {

            }
            return false;
        }

        public bool SendSMS()
        {
            string mobno = "918668729535,919689568385,919730070018";
            string sms = "Testing-sample1";

            string url = "http://203.212.70.200/smpp/sendsms?username=CN13336&password=Admin@13336&to=" + mobno + "&from=ISFTIS&text=" + sms + "&category=bulk";
            WebRequest request = WebRequest.Create(url);
            request.Credentials = CredentialCache.DefaultCredentials;
            WebResponse response = request.GetResponse();
            if (((HttpWebResponse)response).StatusDescription == "OK")
            {
                using (Stream dataStream = response.GetResponseStream())
                {
                    // Open the stream using a StreamReader for easy access.  
                    StreamReader reader = new StreamReader(dataStream);
                    // Read the content.  
                    string responseFromServer = reader.ReadToEnd();
                    // Display the content.  
                    if (responseFromServer.Contains("sent"))
                    {
                        response.Close();
                        return true;
                    }
                }
                response.Close();
                //return true;
            }
            response.Close();
            return false;
        }

        public bool GetLatestOrder(string TradingSymbol, string Exchange, string orderId, out OrderComposite latestOrder)
        {
            try
            {
                OrderExtComposite orderExt;
                m_GlobalOrderStore.GetOrderbyID(TradingSymbol, Exchange, orderId, out orderExt);
                if (orderExt != null)
                {
                    latestOrder = orderExt.order;

                    //WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetLatestOrder : Return true ", MessageType.Informational);
                    return true;
                }
                else
                {
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetLatestOrder : orderExt is null", MessageType.Informational);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetLatestOrder : Exception Error Message " + ex.Message, MessageType.Exception);
            }
            latestOrder = new OrderComposite();
            return false;
        }

        public bool GetLatestOrderStatus(string TradingSymbol, string Exchange, out OrderComposite latestOrder)
        {
            string symbolName = TradingSymbol;
            List<OrderComposite> orderInfo = m_GlobalOrderStore.GetOrderbySymbol(TradingSymbol, Exchange);

            if (orderInfo.Count == 0)
            {
                string errorMsg = "GetLatestOrderStatus: Not Found any previous order - " + orderInfo.Count.ToString();
                WriteUniquelogs(symbolName + " " + m_UserID, errorMsg, MessageType.Error);

                latestOrder = new OrderComposite();
                //latestOrder.Status = Constants.ORDER_STATUS_REJECTED;
                latestOrder.Status = "Rejected";
                return true;
            }

            latestOrder = orderInfo[orderInfo.Count - 1];
            WriteUniquelogs(symbolName + " " + m_UserID, "GetLatestOrderStatus : Return true ", MessageType.Informational);
            return true;
        }
        int m_counterForForcefullyLogin = 0;



        private void _OnTimerExecute(object sender, System.Timers.ElapsedEventArgs e)
        {
            //sanika::16-July-2021::added lock as per sir's suggestion
            lock (m_Timerlock)
            {
                try
                {
                    if (m_UpdatingStructures != null)
                    {
                        if (m_UpdatingStructures.IsAlive)
                        {
                            Thread.Sleep(1000);
                        }
                        else
                        {
                            m_UpdatingStructures.Start();
                        }
                    }
                    else
                    {
                        if (m_UpdatingStructures == null)
                        {
                            m_UpdatingStructures = new Thread(() => UpdateStructures());
                            m_UpdatingStructures.Start();
                        }
                    }
                }
                catch (Exception ex)
                {
                    logger.LogMessage("_OnTimerExecute : UpdateStructures thread Exception Error Message = " + ex.Message + " " + m_userid, MessageType.Exception);
                }
                //20-July-2021: sandip:name mechanism commented
                //try
                //{
                //    if (m_SendResponse != null)
                //    {
                //        if (m_SendResponse.IsAlive)
                //        {
                //            Thread.Sleep(1000);
                //        }
                //        else
                //        {
                //            m_SendResponse.Start();
                //        }
                //    }
                //    else
                //    {
                //        if (m_SendResponse == null)
                //        {
                //            m_SendResponse = new Thread(() => SendResponse());
                //            m_SendResponse.Start();
                //        }
                //    }
                //}
                //catch (Exception er)
                //{
                //    logger.LogMessage("_OnTimerExecute : SendResponse thread Exception Error Message = " + er.Message + " " + m_userid, MessageType.Exception);
                //}
                try
                {
                    //check sqroff and closed order flag to abort calculate profitLoss thread
                    if (m_isCalculateProfitLoss)
                    {
                        //sanika::3-dec-2020::Change condition for close orders flag according exchange
                        if (m_isSqrOff == true || (m_isSqrOffNSE == true && m_isSqrOffNFO == true && m_isSqrOffMCX == true && m_isSqrOffCDS == true))
                        {
                            if (m_CalculateProfit != null)
                            {
                                if (m_CalculateProfit.IsAlive)
                                {
                                    logger.LogMessage("_OnTimerExecute : Stopping thread of calculate profit/loss because exit time condition matched or overall profit/loss condition matched", MessageType.Informational);
                                    m_CalculateProfit.Abort();
                                }
                            }
                        }//sanika::3-dec-2020::Added condition for all exchanges flags
                        else if (m_isCloseAllNSEOrder == true && m_isCloseAllNFOOrder == true && m_isCloseAllMCXOrder == true && m_isCloseAllCDSOrder == true)
                        {
                            if (m_CalculateProfit != null)
                            {
                                if (m_CalculateProfit.IsAlive)
                                {
                                    logger.LogMessage("_OnTimerExecute : Stopping thread of calculate profit/loss because exit time condition matched", MessageType.Informational);
                                    m_CalculateProfit.Abort();
                                }
                            }
                        }
                    }

                }
                catch (Exception er)
                {
                    logger.LogMessage("_OnTimerExecute : m_CalculateProfit thread Exception Error Message = " + er.Message + " " + m_userid, MessageType.Exception);
                }
                Thread.Sleep(1000);
            }
        }
        //20-July-2021: sandip:declared dictionary to store open positions and orders.
        Dictionary<string, int> UniqueOrderWrite = new Dictionary<string, int>();
        Dictionary<string, int> UniquePositionWrite = new Dictionary<string, int>();

        //20-July-2021: sandip:optimization of objects
        private void RequestNUpdateOrderInfo()
        {
            m_ListForSymbols.Clear();
            try
            {
                //sanika::4-Aug-2021::removed reference of list as not used
                var orderData = composite.GetOrderHistoryVar();
                if (orderData != null)
                {
                    bool bSubscribeData = false;
                    foreach (Dictionary<string, dynamic> item in orderData["result"])
                    {
                        string Exchange = "";
                        if (item["ExchangeSegment"] == "NSECM")
                        {
                            Exchange = "NSE";
                        }
                        if (item["ExchangeSegment"] == "NSEFO")
                        {
                            Exchange = "NFO";
                        }
                        //sanika::4-Aug-2021::added change for memory leak 
                        decimal Price = ((item["OrderPrice"] == 0) && (item["OrderAverageTradedPrice"] != "") ? Convert.ToDecimal(item["OrderAverageTradedPrice"]) : Convert.ToDecimal(item["OrderPrice"]));
                        //04-August-2021: sandip:memory issue
                        //sanika::03-Aug-2021::change the sequence of function call as trading symbol passed as empty in log
                        string Tradingsymbol1 = composite.getInstrumentName(item["ExchangeInstrumentID"].ToString(), Exchange);
                        //IRDS::Jyoti::12-Aug-21::Added Filled Quantity to update PARTIALLYFILLED orders
                        string temp1 = Tradingsymbol1 + item["OrderQuantity"].ToString() + item["OrderType"].ToUpper() + item["AppOrderID"].ToString() + item["OrderSide"].ToUpper() + item["OrderStopPrice"].ToString() + Price + item["OrderStatus"] + item["CumulativeQuantity"] + item["CancelRejectReason"];
                        if (!UniqueOrderWrite.ContainsKey(temp1))
                        {
                            UniqueOrderWrite.Add(temp1, 1);
                            OrderComposite objOrder = new OrderComposite(item);
                            
                            //sanika::03-Aug-2021::change the sequence of function call as trading symbol passed as empty in log
                            objOrder.Tradingsymbol = composite.getInstrumentName(objOrder.InstrumentToken, objOrder.Exchange);
                            string temp = "RequestNUpdateOrderInfo : token = " + objOrder.InstrumentToken + " symbol = " + objOrder.Tradingsymbol + " quantity " + objOrder.Quantity + " order type " + objOrder.OrderType + " order id " + objOrder.OrderId + " direction " + objOrder.TransactionType + " trigger price " + objOrder.TriggerPrice + " price " + objOrder.Price + " status " + objOrder.Status + " Filled Quantity " + objOrder.FilledQuantity + " Rejection Reason " + objOrder.StatusMessage;
                            //if (UniqueOrderWrite.ContainsKey(temp) == false)
                            {
                                bSubscribeData = true;
                                WriteUniquelogs("OrderInfo" + " " + m_UserID, temp, MessageType.Informational);
                                //UniqueOrderWrite[temp] = 0;

                                m_GlobalOrderStore.AddOrUpdateOrder(objOrder);

                                m_GlobalOrderStore.AddOrder(objOrder);
                                if (objOrder.Status == Constants.ORDER_STATUS_COMPLETE)
                                {
                                    m_GlobalpositionStore.AddOrderIdAndTime(objOrder);
                                }
                                if (!isTickMCXDataDownloader)
                                {
                                    //sanika::2-Nov-2020::changed condition for mcx symbols
                                    if (objOrder.Exchange != Constants.EXCHANGE_NSE)
                                    {
                                        if (!m_ListForSymbols.Contains(objOrder.Tradingsymbol + "." + objOrder.Exchange)) //sanika::2-Nov-2020::changed condition for mcx symbols 
                                        {
                                            //IRDS::Jyoti::9-Sept-20::Added for options symbols
                                            if (objOrder.Tradingsymbol.EndsWith("CE") || objOrder.Tradingsymbol.EndsWith("PE"))
                                                m_ListForSymbols.Add(objOrder.Tradingsymbol + "." + Constants.EXCHANGE_NFO_OPT);
                                            else
                                                m_ListForSymbols.Add(objOrder.Tradingsymbol + "." + objOrder.Exchange); //sanika::2-Nov-2020::changed for mcx symbols
                                        }
                                    }
                                    else
                                    {
                                        if (!m_ListForSymbols.Contains(objOrder.Tradingsymbol + "." + Constants.EXCHANGE_NSE))
                                        {
                                            m_ListForSymbols.Add(objOrder.Tradingsymbol + "." + Constants.EXCHANGE_NSE);
                                        }
                                    }
                                }
                            }
                            //else
                            //{
                            //    objOrder.Clear();
                            //    objOrder = null;
                            //}
                        }
                    }
                    orderData = null;
                    if (bSubscribeData)
                        CheckTradingSymbolPresentOrNot(m_ListForSymbols);

                    //GC.Collect();
                }
                //composite.GetOrderHistory(ref orderInfo);
                //for (int i = orderInfo.Count - 1; i >= 0; i--)
                //{
                //    OrderComposite objOrder = orderInfo[i];
                //    objOrder.Tradingsymbol = composite.getInstrumentName(orderInfo[i].InstrumentToken, orderInfo[i].Exchange);
                //    //OrderExtComposite objOrderExt = new OrderExtComposite(objOrder);
                //    //m_GlobalOrderStore.AddOrUpdateOrder(objOrderExt);
                //    m_GlobalOrderStore.AddOrUpdateOrder(objOrder);
                //    //20-July-2021: sandip: unique postions 
                //    //sanika::14-Jun-2021::added logs
                //    string temp = "RequestNUpdateOrderInfo : symbol = " + objOrder.Tradingsymbol + " quantity " + objOrder.Quantity + " order type " + objOrder.OrderType + " order id " + objOrder.OrderId + " direction " + objOrder.TransactionType + " trigger price " + objOrder.TriggerPrice + " price " + objOrder.Price + " status " + objOrder.Status;
                //    if (UniqueOrderWrite.ContainsKey(temp) == false)
                //    {
                //        WriteUniquelogs("OrderInfo" + " " + m_UserID, temp, MessageType.Informational);
                //        UniqueOrderWrite[temp] = 0;
                //    }


                //    m_GlobalOrderStore.AddOrder(objOrder);
                //    if (objOrder.Status == Constants.ORDER_STATUS_COMPLETE)
                //    {
                //        m_GlobalpositionStore.AddOrderIdAndTime(objOrder);
                //    }
                //    if (!isTickMCXDataDownloader)
                //    {
                //        //sanika::2-Nov-2020::changed condition for mcx symbols
                //        if (objOrder.Exchange != Constants.EXCHANGE_NSE)
                //        {
                //            if (!m_ListForSymbols.Contains(objOrder.Tradingsymbol + "." + objOrder.Exchange)) //sanika::2-Nov-2020::changed condition for mcx symbols 
                //            {
                //                //IRDS::Jyoti::9-Sept-20::Added for options symbols
                //                if (objOrder.Tradingsymbol.EndsWith("CE") || objOrder.Tradingsymbol.EndsWith("PE"))
                //                    m_ListForSymbols.Add(objOrder.Tradingsymbol + "." + Constants.EXCHANGE_NFO_OPT);
                //                else
                //                    m_ListForSymbols.Add(objOrder.Tradingsymbol + "." + objOrder.Exchange); //sanika::2-Nov-2020::changed for mcx symbols
                //            }
                //        }
                //        else
                //        {
                //            if (!m_ListForSymbols.Contains(objOrder.Tradingsymbol + "." + Constants.EXCHANGE_NSE))
                //            {
                //                m_ListForSymbols.Add(objOrder.Tradingsymbol + "." + Constants.EXCHANGE_NSE);
                //            }
                //        }
                //    }
                //}
                //if (orderInfo.Count > m_PreviousOrderCounter)
                //{
                //    CheckTradingSymbolPresentOrNot(m_ListForSymbols);
                //    m_PreviousOrderCounter = orderInfo.Count;
                //}
                //WriteUniquelogs("OrderInfo" + " " + m_UserID, "RequestNUpdateOrderInfo : End", MessageType.Informational);
            }
            catch (Exception e)
            {
                //sanika::6-July-2021::added notification for exception in order
                AddNotificationInQueue("Not able to get order information " + m_UserID + " " + DateTime.Now.ToString("HH:mm:ss"));
                WriteUniquelogs("OrderInfo" + " " + m_UserID, "RequestNUpdateOrderInfo : Exception Error Message " + e.Message, MessageType.Exception);
                //throw new DataException("Data Exception is occured ... Unable to parse data");
            }
        }
        private void RequestNUpdateHoldings()
        {
            var holdingResponse = composite.GetHoldings();
            foreach (var holding in holdingResponse)
            {
                m_GlobalholdingStore.AddOrUpdateHoldingInfo(holding);
            }
        }
        //20-July-2021: sandip:dummmy function for api working
        private void RequestNUpdatePositioInfoLogin()
        {
            try
            {
                var positionResponse = composite.GetPositions();

            }
            catch (Exception e)
            {
                string message = e.Message;
                if (message.Contains("Incorrect") && message.Contains("api_key") && message.Contains("access_token"))
                {
                    m_bneedToForcefullyLogin = true;
                }
                //sanika::6-July-2021::added notification for exception in day position
                AddNotificationInQueue("Not able to get positions " + m_UserID + " " + DateTime.Now.ToString("HH:mm:ss"));
                WriteUniquelogs("Positions" + " " + m_UserID, "RequestNUpdatePositioInfoLogin : Exception Error Message" + e.Message, MessageType.Exception);
            }

            //IsPositionOpen("IOC", Constants.EXCHANGE_NSE);
        }
        //20-July-2021: sandip:optimization of objects
        private void RequestNUpdatePositioInfo(Dictionary<string, dynamic> param, Dictionary<string, dynamic> positionsDataDay)
        {
            try
            {
                //WriteUniquelogs("Positions" + " " + m_UserID, "RequestNUpdatePositioInfo : Call", MessageType.Informational);
                //m_GlobalpositionStore.Clear();
                var positionResponse = composite.GetPositions(param, positionsDataDay);
                try
                {
                    //04-August-2021: sandip:position memory leak issue
                    if (positionResponse.Day != null)
                    {
                        foreach (var position in positionResponse.Day)
                        {
                            position.TradingSymbol = composite.getInstrumentName(position.InstrumentToken, position.Exchange);
                            string symbolName = position.TradingSymbol;
                            //sanika::28-Apr-2021::Added condition for option symbols
                            if (position.Exchange == Constants.EXCHANGE_NFO && !position.TradingSymbol.Contains(m_futureName) && ((!(position.TradingSymbol.Length > 10)) && (!(position.TradingSymbol.EndsWith("CE") || position.TradingSymbol.EndsWith("PE")))))
                            {
                                symbolName = position.TradingSymbol + m_futureName;
                            }
                            //20-July-2021: sandip: write unique orders
                            //string temp = "RequestNUpdatePositioInfo : symbol " + position.TradingSymbol + " Quanitity " + position.Quantity + " price " + position.AveragePrice;
                            string temp = "RequestNUpdatePositioInfo : symbol " + position.TradingSymbol + "_" + position.Quantity + "_" + position.AveragePrice + " " + position.Exchange + "_" + position.Product + "" + position.BuyPrice + "_" + position.SellPrice + "" + position.buyQuantity + "_" + position.sellQuantity;
                            m_GlobalpositionStore.AddOrUpdatePositionInfo(symbolName + "_" + position.Product, position.Exchange, position, true);
                            if (UniquePositionWrite.ContainsKey(temp) == false)
                            {
                                WriteUniquelogs("Positions" + " " + m_UserID, temp, MessageType.Informational);
                                UniquePositionWrite[temp] = 0;
                            }
                        }
                    }

                }
                catch (Exception e)
                {
                    //sanika::6-July-2021::added notification for exception in day position
                    AddNotificationInQueue("Not able to get day positions " + m_UserID + " " + DateTime.Now.ToString("HH:mm:ss"));
                    WriteUniquelogs("Positions" + " " + m_UserID, "RequestNUpdatePositioInfo : position day Exception Error Message " + e.Message, MessageType.Exception);
                }
                try
                {
                    //04-August-2021: sandip:position memory leak issue
                    if (positionResponse.Net != null)
                    {
                        foreach (var position in positionResponse.Net)
                        {
                            position.TradingSymbol = composite.getInstrumentName(position.InstrumentToken, position.Exchange);
                            string symbolName = position.TradingSymbol;
                            //sanika::28-Apr-2021::Added condition for option symbols
                            if (position.Exchange == Constants.EXCHANGE_NFO && !position.TradingSymbol.Contains(m_futureName) && ((!(position.TradingSymbol.Length > 10)) && (!(position.TradingSymbol.EndsWith("CE") || position.TradingSymbol.EndsWith("PE")))))
                            {
                                symbolName = position.TradingSymbol + m_futureName;
                            }
                            //Sanika::20-July-2021::Added unique for net positions
                            string temp = "RequestNUpdatePositioInfo : symbol " + position.TradingSymbol + " Quanitity " + position.Quantity + " price " + position.AveragePrice;
                            m_GlobalpositionStore.AddOrUpdatePositionInfo(symbolName + "_" + position.Product, position.Exchange, position, false);
                            if (UniquePositionWrite.ContainsKey(temp) == false)
                            {
                                WriteUniquelogs("Positions" + " " + m_UserID, temp, MessageType.Informational);
                                UniquePositionWrite[temp] = 0;
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    //sanika::6-July-2021::added notification for exception in day position
                    AddNotificationInQueue("Not able to get day positions " + m_UserID + " " + DateTime.Now.ToString("HH:mm:ss"));
                    WriteUniquelogs("Positions" + " " + m_UserID, "RequestNUpdatePositioInfo : position day Exception Error Message " + e.Message, MessageType.Exception);
                }
            }
            catch (Exception e)
            {
                string message = e.Message;
                if (message.Contains("Incorrect") && message.Contains("api_key") && message.Contains("access_token"))
                {
                    m_bneedToForcefullyLogin = true;
                }
                //sanika::6-July-2021::added notification for exception in day position
                AddNotificationInQueue("Not able to get positions " + m_UserID + " " + DateTime.Now.ToString("HH:mm:ss"));
                WriteUniquelogs("Positions" + " " + m_UserID, "RequestNUpdatePositioInfo : Exception Error Message" + e.Message, MessageType.Exception);
            }

            //IsPositionOpen("IOC", Constants.EXCHANGE_NSE);
        }

        public bool GetCurrentPosition(string mappedSymbol, string Exchange, out PositionInfoComposite currentPosition)
        {
            bGetCurrentPositionError = false;
            PositionInfoComposite pos = new PositionInfoComposite();
            var symbol = mappedSymbol.Split('|');
            var newSymbol = "";

            try
            {
                if (symbol.Count() < 1)
                {
                    currentPosition = pos; // Assigning default values
                    bGetCurrentPositionError = true;
                    return false;
                }
                newSymbol = symbol[0] + "_" + Exchange + "|" + symbol[1];
                if (!m_GlobalpositionStore.GetPosition(newSymbol, out currentPosition))
                {
                    WriteUniquelogs(mappedSymbol, "GetCurrentPosition : Return false", MessageType.Informational);
                    return false;
                }
            }
            catch (DataException e)
            {
                bGetCurrentPositionError = true;
                currentPosition = pos; // Assigning default values
                WriteUniquelogs(symbol[0] + " " + m_UserID, "\nCan't get positions from server", MessageType.Exception);
                return false;
            }
            return true;
        }

        public bool isReloadINI(string iniFilePath)
        {
            Composite composite = new Composite();
            if (objConfigSettings == null)
            {
                objConfigSettings = new ConfigSettings();
            }
            var path = Directory.GetCurrentDirectory();
            string iniFile = path + @"\Configuration\" + iniFilePath;
            objConfigSettings.readConfigFile(iniFile);
            return true;
        }

        public double GetOpenPostionPrice(string TradingSymbol, string Exchange, string Product)
        {
            //WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPostionPrice :  In function", MessageType.Informational);
            double openprice = 0;
            try
            {
                string newSymbol = TradingSymbol + "_" + Product + "|" + Exchange;
                PositionInfoComposite currentPosition;
                if (m_GlobalpositionStore.GetPosition(newSymbol, out currentPosition))
                {
                    if (currentPosition.dayPosition != null && currentPosition.dayPosition.TradingSymbol == TradingSymbol && currentPosition.dayPosition.Quantity != 0)
                    {
                        if (currentPosition.dayPosition.Quantity > 0)
                        {
                            //buy
                            openprice = Convert.ToDouble(currentPosition.dayPosition.BuyValue);
                        }
                        else if (currentPosition.dayPosition.Quantity < 0)
                        {
                            //sell
                            openprice = Convert.ToDouble(currentPosition.dayPosition.SellValue);
                        }
                    }
                    if (currentPosition.netPosition != null && currentPosition.netPosition.TradingSymbol == TradingSymbol && currentPosition.netPosition.Quantity != 0 && openprice == 0)
                    {

                        if (currentPosition.netPosition.Quantity > 0)
                        {
                            //buy // sanika::7-May-2021::changed buyvalue to buy price as price fetch as long number
                            openprice = Convert.ToDouble(currentPosition.netPosition.BuyPrice);
                        }
                        else if (currentPosition.netPosition.Quantity < 0)
                        {
                            //sell sanika::7-May-2021::changed sellvalue to sell price as price fetch as long number
                            openprice = Convert.ToDouble(currentPosition.netPosition.SellPrice);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPostionPrice :  Exception Error Message = " + e.Message, MessageType.Exception);
            }
            //WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPostionPrice :  openprice = " + openprice, MessageType.Informational);
            return openprice;
        }


        public string GetOpenPostionDirection(string TradingSymbol, string Exchange, String Product)
        {
            //WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPostionDirection :  In function", MessageType.Informational);
            string direction = "";
            try
            {
                string newSymbol = TradingSymbol + "_" + Product + "|" + Exchange;
                PositionInfoComposite currentPosition;
                if (m_GlobalpositionStore.GetPosition(newSymbol, out currentPosition))
                {
                    if (currentPosition.dayPosition.TradingSymbol == TradingSymbol && currentPosition.dayPosition.Quantity != 0)
                    {
                        if (currentPosition.dayPosition.Quantity > 0)
                        {
                            //buy
                            direction = "BUY";
                        }
                        else if (currentPosition.dayPosition.Quantity < 0)
                        {
                            //sell
                            direction = "SELL";
                        }
                    }
                    if (currentPosition.netPosition.TradingSymbol == TradingSymbol && currentPosition.netPosition.Quantity != 0 && direction == "")
                    {

                        if (currentPosition.netPosition.Quantity > 0)
                        {
                            //buy
                            direction = "BUY";
                        }
                        else if (currentPosition.netPosition.Quantity < 0)
                        {
                            //sell
                            direction = "SELL";
                        }
                    }
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPostionDirection :  Exception Error Message = " + e.Message, MessageType.Exception);
            }
            //WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPostionDirection :  direction = " + direction, MessageType.Informational);
            return direction;
        }

        public int GetOpenPostionQuantity(string TradingSymbol, string Exchange, String Product)
        {
            //WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPostionQuantity :  In function", MessageType.Informational);
            int quantity = 0;
            try
            {
                string newSymbol = TradingSymbol + "_" + Product + "|" + Exchange;
                PositionInfoComposite currentPosition;
                if (m_GlobalpositionStore.GetPosition(newSymbol, out currentPosition))
                {
                    if (currentPosition.dayPosition.TradingSymbol == TradingSymbol && currentPosition.dayPosition.Quantity != 0)
                    {
                        quantity = currentPosition.dayPosition.Quantity;
                    }
                    if (currentPosition.netPosition.TradingSymbol == TradingSymbol && currentPosition.netPosition.Quantity != 0 && quantity == 0)
                    {

                        quantity = currentPosition.netPosition.Quantity;

                    }
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPostionQuantity :  Exception Error Message = " + e.Message, MessageType.Exception);
            }
            WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPostionQuantity :  quantity = " + quantity, MessageType.Informational);
            return quantity;
        }

        public string FetchTableName()
        {
            return composite.FetchTableName();
        }

        public string getTickCurrentTime()
        {
            if (composite != null)
                return composite.tickCurrentTime;
            else
                return "";
        }

        public Dictionary<string, List<string>> getFinalDictionary()
        {
            return composite.FinalZerodhaData;
        }

        public string getLastTradedPrice(string TradingSymbol)
        {
            //if (composite.LastTradedPrice.ContainsKey(TradingSymbol))
            //    return composite.LastTradedPrice[TradingSymbol];
            return "";
        }
        public bool GetHighLow(string TradingSymbol, int barCount, out double dHigh, out double dLow)
        {
            dHigh = 0;
            dLow = 0;
            composite.GetHighLow(TradingSymbol, barCount, out dHigh, out dLow);
            return true;
        }

        /*public bool GetCloseValuesList(string TradingSymbol, int barCount, out List<double> closeValues, out List<string> dateTimevalues)
        {
            //composite.GetCloseValuesList(TradingSymbol, barCount, out closeValues, out dateTimevalues);
            return true;
        }*/
        public PositionResponseComposite GetPositions()
        {
            return composite.GetPositions();
        }

        public bool OpenConnection()
        {
            //db created at absolute path
            var path_DB = Directory.GetCurrentDirectory();
            try
            {
                string directoryName = path_DB + "\\" + "Database";

                if (!Directory.Exists(directoryName))
                    Directory.CreateDirectory(directoryName);
                //20-July-2021: sandip:common name used
                string DBFilePath = directoryName + "\\" + tokenDBName;

                if (!File.Exists(DBFilePath))
                {
                    SQLiteConnection.CreateFile(DBFilePath);
                    Thread.Sleep(100);
                }

                string connectionString = String.Format("Data Source={0};Version=3;New=False;Compress=True;PRAGMA locking_mode=EXCLUSIVE;", DBFilePath);
                sql_con = new SQLiteConnection(connectionString);
                //open sqlite connection
                sql_con.Open();
            }
            catch (Exception e)
            {
                logger.LogMessage("OpenConnection() Path: " + path_DB + e.Message, MessageType.Exception);
                return false;
            }

            return true;
        }

        bool CloseConnection()
        {
            if (sql_con == null)
            {
                return false;
            }
            sql_con.Close();
            return true;
        }

        public bool TableExists(string Table_Name)
        {
            using (sql_cmd = new SQLiteCommand(sql_con))
            {
                sql_cmd.CommandText = "SELECT * FROM sqlite_master WHERE type='table' AND name=@name";
                sql_cmd.Parameters.AddWithValue("@name", Table_Name);

                using (sql_read = sql_cmd.ExecuteReader())
                {
                    if (sql_read != null && sql_read.HasRows)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }
        public bool TableDataExists(string Table_Name)
        {
            SQLiteCommand sql_cmd;
            using (sql_cmd = new SQLiteCommand(sql_conData))
            {
                sql_cmd.CommandText = "SELECT * FROM sqlite_master WHERE type='table' AND name=@name";
                sql_cmd.Parameters.AddWithValue("@name", Table_Name);

                using (sql_read = sql_cmd.ExecuteReader())
                {
                    if (sql_read != null && sql_read.HasRows)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }

        public void FuturetableCreation(string tableName, string symbolValue, int lotsize, decimal tick_Size, string instrumentToken, string exchange, SqliteConnectZerodha sqliteConnectZerodha)
        {
            //check if table is already exists or not
            if (!m_ListOfTableExistOrNot.Contains(tableName))
            {
                if (sqliteConnectZerodha.IsTableExists(tableName) == false)
                {//sanika::21-Jul-2021::removed price column
                    string strQuery = "CREATE TABLE '" + tableName + "' (symbol varchar(20) NOT NULL, lotsize int NOT NULL, tickSize real NOT NULL,InstrumentToken read PRIMARY KEY,exchange varchar(10) NOT NULL)";
                    sqliteConnectZerodha.ExecuteNonQueryCommand(strQuery);
                }
                m_ListOfTableExistOrNot.Add(tableName);
            }
            //20-July-2021: sandip:check token already exists.
            if (sqliteConnectZerodha.IsTokenExists(instrumentToken, tableName, symbolValue) == false)
            {
                //sanika::10-Jun-2021::Changed query to replace updated values
                //sanika::21-Jul-2021::removed price column
                string insertSQLQuery = "INSERT OR REPLACE into '" + tableName + "' (symbol, lotsize, tickSize, InstrumentToken, exchange) values ('" + symbolValue + "','" + lotsize + "','" + tick_Size + "','" + instrumentToken + "', 'NFO')";
                if (!m_InsertQueries.Contains(insertSQLQuery))
                    m_InsertQueries.Add(insertSQLQuery);
            }
        }

        //sanika::20-July-2021::removed price column
        public void optionstableCreation(string tableName, string symbolValue, string optSymbolValue, int lotsize, decimal tick_Size, string instrumentToken, string expiry_date, SqliteConnectZerodha sqliteConnectZerodha)
        {
            //check if table is already exists or not
            if (!m_ListOfTableExistOrNot.Contains(tableName))
            {
                if (sqliteConnectZerodha.IsTableExists(tableName) == false)
                {
                    //sanika::10-Jun-2021::Added primary key to token
                    //sanika::20-July-2021::removed price column
                    string sql_new = "CREATE TABLE '" + tableName + "'(symbol varchar(20) NOT NULL, optionsSymbol varchar(20) NOT NULL, lotsize int NOT NULL, tickSize real NOT NULL,InstrumentToken read PRIMARY KEY,expiryDate varchar(25) NOT NULL)";
                    sqliteConnectZerodha.ExecuteNonQueryCommand(sql_new);
                }

                m_ListOfTableExistOrNot.Add(tableName);
            }
            //20-July-2021: sandip: avoid duplicate entry
            if (sqliteConnectZerodha.IsTokenExists(instrumentToken, tableName, symbolValue, optSymbolValue) == false)
            {
                //sanika::10-Jun-2021::Changed query to replace updated values
                //sanika::20-July-2021::removed price column
                string insertSQLQuery = "INSERT OR REPLACE into '" + tableName + "' (symbol, optionsSymbol, lotsize, tickSize, InstrumentToken, expiryDate) values ('" + symbolValue + "','" + optSymbolValue + "','" + lotsize + "','" + tick_Size + "','" + instrumentToken + "','" + expiry_date + "')";
                if (!m_InsertQueries.Contains(insertSQLQuery))
                    m_InsertQueries.Add(insertSQLQuery);
            }
        }

        public void LoadSymbolWithLotandTickInDict()
        {
            try
            {
                if (futureSymbolAndLotSize.Count == 0 && dictSymbolwithtickSize.Count == 0 && OpenConnection())
                {
                    DateTime dt = DateTime.Now;
                    //IRDS::Sandip::5-July-18::clear the dictionary
                    futureSymbolAndLotSize.Clear();
                    dictSymbolwithtickSize.Clear();
                    getCurrentMonth = dt.ToString("MMM").ToUpper();
                    getCurrentYear = dt.ToString("yy");
                    string getFromFutTableName = getCurrentYear + getCurrentMonth + findElement;
                    string sql_select = "";

                    //IRDS::05-July-2018::Archana::Need to check for first time when DB created with no tables
                    if (TableExists(getFromFutTableName))
                    {
                        sql_select = "select symbol, lotsize, tickSize from '" + getFromFutTableName + "'";
                        sql_cmd = new SQLiteCommand(sql_select, sql_con);
                        sql_read = sql_cmd.ExecuteReader();

                        if (sql_read != null && sql_read.HasRows)
                        {
                            while (sql_read.Read())
                            {
                                getFutSymbol = (string)sql_read["symbol"];
                                getFutSymbolLotSize = (int)sql_read["lotsize"];

                                //IRDS::04-July-2018::Bhagyashri::Getting tickSize from future table
                                getFutSymboltickSize = Convert.ToDecimal((double)sql_read["tickSize"]);

                                if (!futureSymbolAndLotSize.ContainsKey(getFutSymbol))
                                {
                                    futureSymbolAndLotSize.Add(getFutSymbol, getFutSymbolLotSize);
                                }

                                //IRDS::04-July-2018::Bhagyashri::Added tickSize into dictSymbolwithtickSize from future table
                                if (!dictSymbolwithtickSize.ContainsKey(getFutSymbol))
                                {
                                    dictSymbolwithtickSize.Add(getFutSymbol, getFutSymboltickSize);
                                }
                            }
                        }
                    }
                    else
                    {
                        logger.LogMessage(getFromFutTableName + " table does not exists", MessageType.Error);
                        string sql_new = "CREATE TABLE '" + getFromFutTableName + "'(symbol varchar(20) NOT NULL, lotsize int NOT NULL, price real NOT NULL, tickSize real NOT NULL,InstrumentToken read NOT NULL)";
                        sql_cmd = new SQLiteCommand(sql_new, sql_con);
                        sql_cmd.ExecuteNonQuery();
                        logger.LogMessage(getFromFutTableName + " table created", MessageType.Informational);
                    }

                    if (TableExists("instruments"))
                    {
                        sql_select = "select symbol, tickSize from instruments";
                        sql_cmd = new SQLiteCommand(sql_select, sql_con);
                        sql_read = sql_cmd.ExecuteReader();

                        if (sql_read != null && sql_read.HasRows)
                        {
                            while (sql_read.Read())
                            {
                                getFutSymbol = (string)sql_read["symbol"];
                                getFutSymboltickSize = Convert.ToDecimal((double)sql_read["tickSize"]);

                                if (!dictSymbolwithtickSize.ContainsKey(getFutSymbol))
                                {
                                    dictSymbolwithtickSize.Add(getFutSymbol, getFutSymboltickSize);
                                }
                            }
                        }
                    }
                    else
                    {
                        //table created without primary key
                        //IRDS :: Nayana :: 17-Apr-2019 :: Added one more column InstumentToken 
                        string sql = "CREATE TABLE instruments (symbol varchar(20) NOT NULL, lotsize int NOT NULL, price real NOT NULL, tickSize real NOT NULL , InstrumentToken read NOT NULL)";
                        sql_cmd = new SQLiteCommand(sql, sql_con);
                        sql_cmd.ExecuteNonQuery();
                        logger.LogMessage("instruments table created", MessageType.Informational);
                    }

                    if (sql_read != null) //IRDS::Sandip::5-July-18::close after use
                        sql_read.Close();

                    if (sql_con != null)
                        sql_con.Close();
                }
                else if (futureSymbolAndLotSize.Count > 0 && dictSymbolwithtickSize.Count > 0)
                {
                    logger.LogMessage(m_UserID + " Future Symbol dictionary already loaded", MessageType.Informational);
                }
                else
                {
                    logger.LogMessage(m_UserID + " SQL Connection is not opened", MessageType.Informational);
                }
            }

            catch (Exception e)
            {
                logger.LogMessage("LoadSymbolWithLotandTickInDict() " + e.Message, MessageType.Exception);
            }
        }

        public decimal RoundStopLossValueOfAFL(string futSymbol, decimal normalValue, bool flag)
        {
            decimal tickSizeForFutSymbol = 0.05m;
            decimal newRoundedPrice;
            decimal precisionCalcPrice;

            if (dictSymbolwithtickSize.ContainsKey(futSymbol))
            {
                dictSymbolwithtickSize.TryGetValue(futSymbol, out tickSizeForFutSymbol);
            }
            else
            {
                WriteUniquelogs(futSymbol + " " + m_UserID, "RoundStopLossValueOfAFL : Symbol is not present in the dictionary : " + futSymbol, MessageType.Error);
                // return normalValue;
            }

            //IRDS::06-July-2018::Bhagyashri::True for BUY i.e. RoundUp the market value
            if (flag == true)
            {
                if (tickSizeForFutSymbol > 0)
                    precisionCalcPrice = tickSizeForFutSymbol * Math.Round((tickSizeForFutSymbol + normalValue) / tickSizeForFutSymbol);
                else
                    precisionCalcPrice = normalValue;

                newRoundedPrice = Math.Abs(precisionCalcPrice);
            }
            //IRDS::06-July-2018::Bhagyashri::else for SELL i.e. RoundDown the market value
            else
            {
                if (tickSizeForFutSymbol > 0)
                    precisionCalcPrice = tickSizeForFutSymbol * Math.Floor((tickSizeForFutSymbol - normalValue) / tickSizeForFutSymbol);
                else
                    precisionCalcPrice = normalValue;

                newRoundedPrice = Math.Abs(precisionCalcPrice);
            }

            return newRoundedPrice;
        }

        public void CheckTradingSymbolPresentOrNot(List<string> list)
        {
            composite.CheckTradingSymbolPresentOrNot(list);
        }

        public List<string> getOHCLValues(string TradingSymbol)
        {
            try
            {
                if (composite.FinalZerodhaData.ContainsKey(TradingSymbol))
                {
                    return composite.FinalZerodhaData[TradingSymbol];
                }
                return null;
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return null;
        }

        public string getVariety(string TradingSymbol, string Exchange)
        {
            List<OrderComposite> orderInfo = m_GlobalOrderStore.GetOrderbySymbol(TradingSymbol, Exchange);
            if (orderInfo.Count == 0)
            {
                string errorMsg = "GetLatestOrder : Failed to get order history for TradingSymbol - " + TradingSymbol;
                WriteUniquelogs(TradingSymbol + " " + m_UserID, errorMsg, MessageType.Error);
                return null;
            }

            OrderComposite latestOrder = orderInfo[orderInfo.Count - 1];
            WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetLatestOrder : ", MessageType.Informational);
            //return latestOrder.Variety;
            return "";
        }

        public Dictionary<string, List<string>> getOHCLDictionary()
        {
            if (composite.FinalZerodhaData.Count > 0)
            {
                return composite.FinalZerodhaData;
            }
            else
            {
                return null;
            }
        }
        public double GetOpenPostionPricebyOrderID(string TradingSymbol, string Exchange, string orderId)
        {
            double Price = 0;
            try
            {
                OrderExtComposite orderExt;
                if (m_GlobalOrderStore.GetOpenOrderbyID(TradingSymbol, Exchange, orderId, out orderExt))
                {
                    if (orderExt == null)
                    {
                        WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPostionPricebyOrderID : order info count is zero", MessageType.Informational);
                        return 0;
                    }
                    if (orderExt.order.Status == "FILLED" || orderExt.order.Status == Constants.ORDER_STATUS_COMPLETE)
                    {
                        Price = Convert.ToDouble(orderExt.order.Price);
                    }
                    //WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPostionPricebyOrderID : Price " + Price, MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPostionPricebyOrderID : Exception Error Message = " + e.Message, MessageType.Informational);
            }
            return Price;
        }

        public double GetStopPricebyOrderID(string TradingSymbol, string Exchange, string orderId)
        {
            double Price = 0;
            try
            {
                OrderExtComposite orderExt;
                if (m_GlobalOrderStore.GetOpenOrderbyID(TradingSymbol, Exchange, orderId, out orderExt))
                {
                    if (orderExt == null)
                    {
                        WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPostionPricebyOrderID : order info count is zero", MessageType.Informational);
                        return 0;
                    }
                    //if (orderExt.order.Status == Constants.ORDER_STATUS_PENDING)
                    if (orderExt.order.Status == "OPEN")
                    {
                        Price = Convert.ToDouble(orderExt.order.TriggerPrice);
                    }
                    //WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPostionPricebyOrderID : Price " + Price, MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPostionPricebyOrderID : Exception Error Message = " + e.Message, MessageType.Informational);
            }
            return Price;
        }

        public string GetOpenPostionDirectionByOrderId(string TradingSymbol, string Exchange, string orderId)
        {
            string direction = "directionnotfound";//sanika::18-Feb-2021::Assign Intial value
            try
            {
                OrderExtComposite orderExt;
                if (m_GlobalOrderStore.GetOpenOrderbyID(TradingSymbol, Exchange, orderId, out orderExt))
                {
                    if (orderExt == null)
                    {
                        WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPostionDirectionByOrderId : order info count is zero", MessageType.Informational);
                        return "";
                    }
                    //if (orderExt.order.Status == Constants.ORDER_STATUS_COMPLETE)
                    //if (orderExt.order.Status == "Complete")
                    {
                        direction = orderExt.order.TransactionType;
                    }
                    //WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPostionDirectionByOrderId : direction " + direction, MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPostionDirectionByOrderId : Exception Error Message = " + e.Message, MessageType.Informational);
            }
            return direction;
        }

        //sanika::03-Feb-2021::Changed function as per zerodha
        public string GetOrderStatusByOrderID(string TradingSymbol, string Exchange, string orderId)
        {
            string status = "ordernotupdated";//sanika::18-Feb-2021::Assign Intial value
            try
            {
                //sanika::7-May-2021::Added codntion for exception
                if (orderId != null)
                {
                    int counter = 0;
                    OrderExtComposite orderExt;
                    while (counter <= 1)
                    {
                        if (m_GlobalOrderStore.GetOpenOrderbyID(TradingSymbol, Exchange, orderId, out orderExt))
                        {
                            if (orderExt == null)
                            {
                                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOrderStatusByOrderID : order info count is zero", MessageType.Informational);
                                return "";
                            }
                            if (orderExt.order.Status != "" && orderExt.order.Status != null)
                            {
                                status = orderExt.order.Status;
                            }
                            //WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOrderStatusByOrderID :  order type " + orderExt.order.OrderType + " status " + status, MessageType.Informational);
                        }
                        if (status != "ordernotupdated")
                        {
                            break;
                        }
                        Thread.Sleep(10);
                        counter++;
                    }
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOrderStatusByOrderID : Exception Error Message = " + e.Message, MessageType.Informational);
            }
            return status;
        }

        public int GetOrderPendingQuatityByOrderID(string TradingSymbol, string Exchange, string orderId)
        {
            int quantity = 0;//sanika::18-Feb-2021::Assign Intial value
            try
            {
                //sanika::7-May-2021::Added codntion for exception
                if (orderId != null)
                {
                    int counter = 0;
                    OrderExtComposite orderExt;
                    //while (counter <= 1)
                    {
                        if (m_GlobalOrderStore.GetOpenOrderbyID(TradingSymbol, Exchange, orderId, out orderExt))
                        {
                            if (orderExt == null)
                            {
                                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOrderPendingQuatityByOrderID : order info count is zero", MessageType.Informational);
                                return 0;
                            }
                            quantity = orderExt.order.Quantity - orderExt.order.FilledQuantity;
                            //WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOrderStatusByOrderID :  order type " + orderExt.order.OrderType + " status " + status, MessageType.Informational);
                        }
                        //if (status != "ordernotupdated")
                        //{
                        //    break;
                        //}
                        //Thread.Sleep(10);
                        //counter++;
                    }
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOrderStatusByOrderID : Exception Error Message = " + e.Message, MessageType.Informational);
            }
            return quantity;
        }

        public double GetOpenPostionPricebySymbol(string TradingSymbol, string Exchange)
        {
            double Price = 0;
            try
            {
                //OrderExt orderExt;
                ////if (m_GlobalOrderStore.GetOpenOrderbyID(TradingSymbol, Exchange, orderId.ToString(), out orderExt))
                //{
                //    if (orderExt == null)
                //    {
                //        WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPostionPricebyOrderID : order info count is zero", MessageType.Informational);
                //        return 0;
                //    }
                //    if (orderExt.order.Status == Constants.ORDER_STATUS_COMPLETE)
                //    {
                //        Price = Convert.ToDouble(orderExt.order.AveragePrice);
                //    }
                //    WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPostionPricebyOrderID : Price " + Price, MessageType.Informational);
                //}
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPostionPricebyOrderID : Exception Error Message = " + e.Message, MessageType.Informational);
            }
            return Price;
        }
        public double GetStopPrice(string TradingSymbol, string Exchange, string TransactionType)
        {
            double stopPrice = 0;
            List<OrderComposite> orderInfo = m_GlobalOrderStore.GetOrderbySymbol(TradingSymbol, Exchange);

            if (orderInfo.Count == 0)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetStopPrice : order info count is zero", MessageType.Informational);
                return 0;
            }
            //sanika::28-Jul-2021::No need to create global variable
            OrderComposite latestOrder = new OrderComposite();
            for (int i = 0; i < orderInfo.Count(); i++)
            {
                latestOrder = orderInfo[i];
                if (latestOrder.TransactionType == TransactionType)
                {
                    //if (latestOrder.Status == Constants.ORDER_STATUS_PENDING)
                    if (latestOrder.Status == "OPEN")
                    {
                        stopPrice = Convert.ToDouble(latestOrder.TriggerPrice);
                        break;
                    }
                }
            }
            WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetStopPrice : stopPrice " + stopPrice, MessageType.Informational);
            return stopPrice;
        }

        public double GetLimitPrice(string TradingSymbol, string Exchange, string TransactionType)
        {
            double limitPrice = 0;
            List<OrderComposite> orderInfo = m_GlobalOrderStore.GetOrderbySymbol(TradingSymbol, Exchange);

            if (orderInfo.Count == 0)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetLimitPrice :  order info count is zero", MessageType.Informational);
                return 0;
            }
            OrderComposite latestOrder = new OrderComposite();
            for (int i = 0; i < orderInfo.Count(); i++)
            {
                latestOrder = orderInfo[i];
                if (latestOrder.TransactionType == TransactionType)
                {
                    if (latestOrder.Status == Constants.ORDER_STATUS_OPEN)
                    {
                        limitPrice = Convert.ToDouble(latestOrder.Price);
                        break;
                    }
                }
            }
            WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetLimitPrice : limitPrice " + limitPrice, MessageType.Informational);
            return limitPrice;
        }

        public string GetTransactionType(string TradingSymbol, string Exchange)
        {
            string BuyOrSell = null; ;
            try
            {
                string newSymbol = TradingSymbol + "_" + Constants.PRODUCT_MIS + "|" + Exchange;
                PositionInfoComposite currentPosition;
                if (m_GlobalpositionStore.GetPosition(newSymbol, out currentPosition))
                    if (currentPosition.netPosition.TradingSymbol == TradingSymbol && currentPosition.netPosition.Quantity != 0)
                    {
                        if (currentPosition.netPosition.Quantity > 0)
                        {
                            //buy
                            BuyOrSell = Constants.TRANSACTION_TYPE_BUY;
                        }
                        else if (currentPosition.netPosition.Quantity < 0)
                        {
                            //sell
                            BuyOrSell = Constants.TRANSACTION_TYPE_SELL;
                        }
                    }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetTransactionType :  Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return BuyOrSell;
        }
        private static readonly object m_TokenDBLock = new object();

        public bool OpenConnectionForLOtSize()
        {
            try
            {
                lock (m_TokenDBLock)
                {
                    if (sql_conLotSize == null)
                    {
                        //db created at absolute path
                        var path_DB = Directory.GetCurrentDirectory();
                        string directoryName = path_DB + "\\" + "Database";

                        if (!Directory.Exists(directoryName))
                            Directory.CreateDirectory(directoryName);
                        //20-July-2021: sandip:use common name
                        string DBFilePath = directoryName + "\\" + tokenDBName;

                        if (!File.Exists(DBFilePath))
                        {
                            SQLiteConnection.CreateFile(DBFilePath);
                            Thread.Sleep(100);
                        }

                        string connectionString = String.Format("Data Source={0};Version=3;New=False;Compress=True;PRAGMA locking_mode=EXCLUSIVE;", DBFilePath);
                        sql_conLotSize = new SQLiteConnection(connectionString);
                        //open sqlite connection
                        sql_conLotSize.Open();
                    }
                }
            }
            catch (Exception e)
            {
                logger.LogMessage("OpenConnectionForLOtSize : Exception Message Message " + e.Message, MessageType.Exception);
                return false;
            }

            return true;
        }

        public int GetLotSize(string TradingSymbol, string Exchange)
        {
            int lotsize = 0;
            try
            {
                if (sql_conLotSize == null)
                {
                    OpenConnectionForLOtSize();
                }

                string tableName = FetchTableName();
                string symbol = TradingSymbol;
                if (Exchange == "NFO" && (!(TradingSymbol.EndsWith("CE") || TradingSymbol.EndsWith("PE"))))
                {
                    symbol = TradingSymbol + tableName;
                }
                else
                {
                    tableName = "OPTIONS";
                    symbol = TradingSymbol;
                }
                //sanika::26-Apr-2021::Fetch lot size from optionsymbol name
                string sql = "SELECT lotsize from '" + tableName + "' WHERE optionsSymbol = '" + symbol + "'";
                sql_cmd = new SQLiteCommand(sql, sql_conLotSize);
                SQLiteDataReader sql_read = sql_cmd.ExecuteReader();

                if (sql_read != null && sql_read.HasRows)
                {
                    if (sql_read.Read())
                    {
                        var lots = sql_read["lotsize"].ToString();
                        lotsize = Convert.ToInt32(lots);
                    }
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetLotSize :  Exception " + e.Message, MessageType.Exception);
            }
            WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetLotSize :  lotsize " + lotsize, MessageType.Informational);
            return lotsize;
        }

        public int GetMonthLotSize(string TradingSymbol, string Exchange, string MonthFutureName)
        {
            int lotsize = 0;
            try
            {
                if (sql_conLotSize == null)
                {
                    OpenConnectionForLOtSize();
                }

                string symbol = TradingSymbol + MonthFutureName;

                string sql = "SELECT lotsize from '" + MonthFutureName + "' WHERE symbol = '" + symbol + "'";
                sql_cmd = new SQLiteCommand(sql, sql_conLotSize);
                sql_read = sql_cmd.ExecuteReader();

                if (sql_read != null && sql_read.HasRows)
                {
                    if (sql_read.Read())
                    {
                        Console.WriteLine("lotsize: " + sql_read[0]);
                        lotsize = (int)sql_read["lotsize"];
                    }
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetMonthLotSize :  Exception " + e.Message, MessageType.Exception);
            }
            WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetMonthLotSize :  lotsize " + lotsize, MessageType.Informational);
            return lotsize;
        }

        Dictionary<string, int> DatabaseCreated = new Dictionary<string, int>();

        private static readonly object databaselock = new object();
        public void WriteDataIntoFile(string TradingSymbol, string Datetime, double Open, double High, double Low, double Close, double Volume, bool bWriteinDB, bool bCommit)
        {
            try
            {
                int firstSpaceIndex = Datetime.IndexOf(" ");
                string d = Datetime.Substring(0, firstSpaceIndex); // INAGX4
                string ti = Datetime.Substring(firstSpaceIndex + 1); // Agatti Island
                string[] spliitedDateTime = Datetime.Split(' ');
                string date = Convert.ToDateTime(d).ToString("yyyy-MM-dd");
                DateTime datetime = DateTime.Parse(ti, System.Globalization.CultureInfo.CurrentCulture);
                string t = datetime.ToString("HH:mm:00");
                string path = m_path + "//Data//" + TradingSymbol + ".txt";
                Open = Convert.ToDouble(RoundStopLossValueOfAFL(TradingSymbol, Convert.ToDecimal(Open), true));
                High = Convert.ToDouble(RoundStopLossValueOfAFL(TradingSymbol, Convert.ToDecimal(High), false));
                Low = Convert.ToDouble(RoundStopLossValueOfAFL(TradingSymbol, Convert.ToDecimal(Low), true));
                Close = Convert.ToDouble(RoundStopLossValueOfAFL(TradingSymbol, Convert.ToDecimal(Close), false));

                string values = TradingSymbol + "," + date + "," + t + "," + Open + "," + High + "," + Low + "," + Close + "," + Volume;
                using (StreamWriter streamWriter = File.AppendText(path))
                {
                    streamWriter.WriteLine(values);
                }
                //m_CounterProgressBar = 100;
                Thread.Sleep(2000);
                //if (bWriteinDB)
                //WriteDataIntoDB(TradingSymbol, date, t, Open, High, Low, Close, Volume, bCommit);
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "WriteDataIntoFile :  Exception Error Message " + e.Message, MessageType.Informational);
            }
        }

        public void WriteDataIntoDB(string TradingSymbol, string Date, string Time, double Open, double High, double Low, double Close, double Volume, bool bCommit)
        {
            List<String> DBInsertQueries = new List<string>();
            SQLiteCommand sql_cmdlocal;
            try
            {

                string dateTime = Date + " " + Time;
                if (sql_conData == null)
                {
                    lock (databaselock)
                    {
                        OpenConnectionForZerdhaData();
                    }
                }

                if (DatabaseCreated.ContainsKey(TradingSymbol) == false)
                {
                    lock (databaselock)
                    {
                        if (TableDataExists(TradingSymbol) == false)
                        {
                            string sql_new = "CREATE TABLE '" + TradingSymbol + "'(DateTime varchar(20) PRIMARY KEY, Open real NOT NULL, High real NOT NULL,Low real NOT NULL,Close real NOT NULL,Volume real NOT NULL)";
                            sql_cmdlocal = new SQLiteCommand(sql_new, sql_conData);
                            sql_cmdlocal.ExecuteNonQuery();
                        }
                    }

                }
                else
                {
                    DatabaseCreated[TradingSymbol] = 1;//
                }

                string sql_select = "SELECT * FROM '" + TradingSymbol + "' WHERE DateTime = '" + dateTime + "'";
                sql_cmdlocal = new SQLiteCommand(sql_select, sql_conData);
                sql_read = sql_cmdlocal.ExecuteReader();

                if (!(sql_read != null && sql_read.HasRows))
                {
                    lock (databaselock)
                    {
                        string insertSQLQuery = "insert into '" + TradingSymbol + "' (DateTime, Open, High, Low, Close, Volume) values ('" + dateTime + "','" + Open + "','" + High + "','" + Low + "','" + Close + "','" + Volume + "')";
                        DBInsertQueries.Add(insertSQLQuery);
                        if (bCommit)
                        {
                            using (SQLiteTransaction mytransaction = sql_conData.BeginTransaction())
                            {
                                using (SQLiteCommand mycommand = new SQLiteCommand(sql_conData))
                                {
                                    SQLiteParameter myparam = new SQLiteParameter();
                                    int n;
                                    for (n = 0; n < DBInsertQueries.Count(); n++)
                                    {
                                        //myparam.Value = n + 1;
                                        mycommand.CommandText = DBInsertQueries[n];// "INSERT INTO [MyTable] ([MyId]) VALUES(?)";
                                        mycommand.ExecuteNonQuery();
                                    }
                                }
                                mytransaction.Commit();
                                DBInsertQueries.Clear();
                            }
                        }
                    }

                }

            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "WriteDataIntoDB :  Exception Error Message " + e.Message, MessageType.Exception);
            }
        }

        public bool OpenConnectionForZerdhaData()
        {
            try
            {
                //db created at absolute path
                var path_DB = Directory.GetCurrentDirectory();
                string directoryName = path_DB + "\\" + "Database";

                if (!Directory.Exists(directoryName))
                    Directory.CreateDirectory(directoryName);

                string DBFilePath = directoryName + "\\" + "zerodhadata.db";

                if (!File.Exists(DBFilePath))
                {
                    SQLiteConnection.CreateFile(DBFilePath);
                    Thread.Sleep(100);
                }

                string connectionString = String.Format("Data Source={0};Version=3;New=False;Compress=True;PRAGMA locking_mode=EXCLUSIVE;", DBFilePath);
                sql_conData = new SQLiteConnection(connectionString);
                //open sqlite connection
                sql_conData.Open();
            }
            catch (Exception e)
            {
                logger.LogMessage(e.Message, MessageType.Exception);
                return false;
            }

            return true;
        }

        public void DownloadData(List<string> TradingSymbolList, int daysForHistroricalData)
        {
            if (m_StoreDataInMysql)
            {
                DownloadDataMysql(TradingSymbolList, daysForHistroricalData);
            }
            if (daysForHistroricalData > 0)
            {
                SqliteConnectZerodha sqliteConnectZerodha = new SqliteConnectZerodha();
                bool bCommit = false;
                SQLiteCommand sql_cmdlocal;
                try
                {
                    bool isconnected = sqliteConnectZerodha.Connect("zerodhadata.db");
                    DateTime dateTimeForTimeCondition = DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd") + " 15:29:00", "yyyy-MM-dd HH:mm:ss", null);
                    string dateTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm") + ":00";
                    //string dateTime = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
                    DateTime todateTime = DateTime.ParseExact(dateTime, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture).AddMinutes(1);
                    DateTime startdateTime = todateTime.AddDays(-daysForHistroricalData);
                    todateTime = todateTime.AddDays(1);
                    WriteUniquelogs("DownloadData", "startdateTime = " + startdateTime + " todateTime = " + todateTime, MessageType.Informational);
                    List<String> DBInsertQueries = new List<string>();
                    int exchange = 1;
                    foreach (var name in TradingSymbolList)
                    {
                        string[] strAarray = name.Split('.');
                        string TradingSymbol = strAarray[0];
                        if (strAarray[1] == Constants.EXCHANGE_NFO || strAarray[1] == Constants.EXCHANGE_CDS)
                        {
                            exchange = 2;
                            TradingSymbol += composite.FetchTableName();
                        }
                        if (DatabaseCreated.ContainsKey(TradingSymbol) == false)
                        {
                            lock (databaselock)
                            {
                                if (sqliteConnectZerodha.IsTableExists(TradingSymbol) == false)
                                {
                                    sqliteConnectZerodha.ExecuteNonQueryCommand("CREATE TABLE '" + TradingSymbol + "'(TickDateTime DateTime PRIMARY KEY, Open real NOT NULL, High real NOT NULL,Low real NOT NULL,Close real NOT NULL,Volume real NOT NULL,DoubleDate real NOT NULL)");
                                }
                            }
                        }
                        else
                        {
                            DatabaseCreated[TradingSymbol] = 1;
                        }

                        string token = composite.GetToken(name);
                        string historicalList = composite.GetHistoricalData(token, startdateTime, todateTime, "minute", exchange.ToString());
                        int counter = 0;
                        foreach (var historical in historicalList)
                        {
                            counter++;
                            string timestamp = null;// historical.TimeStamp.ToString("yyyy-MM-dd HH:mm:ss").TrimStart();
                            DateTime timeStamp = DateTime.ParseExact(timestamp, "yyyy-MM-dd HH:mm:ss", null);

                            lock (databaselock)
                            {
                                if (timeStamp <= dateTimeForTimeCondition)
                                {
                                    string insertSQLQuery = "INSERT OR IGNORE into '" + TradingSymbol + "' (TickDateTime, Open, High, Low, Close, Volume, DoubleDate) values ('" + historical + "','" + historical + "','" + historical + "','" + historical + "','" + historical + "','" + historical + "','" + historical + "')";
                                    DBInsertQueries.Add(insertSQLQuery);
                                }
                                if (counter == historicalList.Length - 1 && DBInsertQueries.Count > 0)
                                {
                                    sqliteConnectZerodha.InsertDataMultipleValues(TradingSymbol, DBInsertQueries);
                                    DBInsertQueries.Clear();
                                }
                            }
                        }
                    }
                    WriteUniquelogs("DownloadData", "Data Downloaded", MessageType.Informational);
                }
                catch (Exception er)
                {
                    WriteUniquelogs("DownloadData", "Exception Error Message " + er.Message, MessageType.Exception);
                }
            }
        }

        public bool IsPositionOpen(string TradingSymbol, string Exchange)
        {
            bool isOpen = false;
            try
            {
                string newSymbol = TradingSymbol + "_" + Constants.PRODUCT_MIS + "|" + Exchange;
                PositionInfoComposite currentPosition;
                if (m_GlobalpositionStore.GetPosition(newSymbol, out currentPosition))
                    if (currentPosition.netPosition.TradingSymbol == TradingSymbol && currentPosition.netPosition.Quantity != 0)
                    {
                        if (currentPosition.netPosition.Quantity > 0)
                        {
                            //buy
                            isOpen = true;
                        }
                        else if (currentPosition.netPosition.Quantity < 0)
                        {
                            //sell
                            isOpen = true;
                        }
                    }
                //WriteUniquelogs(TradingSymbol + " " + m_UserID, "isOpen = "+ isOpen, MessageType.Exception);
            }
            catch (Exception e)
            {
                isOpen = false;
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "IsPositionOpen :  Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return isOpen;
        }

        public void AddOrderIdInMasterList(string TradingSymbol, string OrderId)
        {
            string symbol = "";
            if (TradingSymbol.Contains("_"))
                symbol = TradingSymbol.Split('_')[1];
            else
                symbol = TradingSymbol;
            try
            {
                if (m_DictionaryForOrderId.ContainsKey(TradingSymbol))
                {
                    m_DictionaryForOrderId[TradingSymbol] = OrderId;
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, "AddOrderIdInMasterList :  Updated order id in dictionary " + OrderId, MessageType.Informational);
                }
                else
                {
                    m_DictionaryForOrderId.Add(TradingSymbol, OrderId);
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, "AddOrderIdInMasterList :  Added order id in dictionary " + OrderId, MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "AddOrderIdInMasterList :  Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        public void RemoveOrderIdFromMasterList(string TradingSymbol)
        {
            string symbol = "";
            if (TradingSymbol.Contains("_"))
                symbol = TradingSymbol.Split('_')[1];
            else
                symbol = TradingSymbol;
            try
            {
                if (m_DictionaryForOrderId.ContainsKey(TradingSymbol))
                {
                    m_DictionaryForOrderId.Remove(TradingSymbol);
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, "RemoveOrderIdFromMasterList :  Remove orderId from dictionary", MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "RemoveOrderIdFromMasterList :  Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        public string GetOrderIdFromMasterList(string TradingSymbol)
        {
            string orderId = "NA";
            string symbol = "";
            if (TradingSymbol.Contains("_"))
                symbol = TradingSymbol.Split('_')[1];
            else
                symbol = TradingSymbol;
            try
            {
                if (m_DictionaryForOrderId.ContainsKey(symbol))
                {
                    orderId = m_DictionaryForOrderId[symbol];
                    //sanika::28-sep-2020::temp commented as per sandip sir's suggestion
                    //if (!CheckOrderStatus(TradingSymbol, orderId))
                    //{
                    //    orderId = "NA";
                    //    RemoveOrderIdFromMasterList(symbol);
                    //    WriteUniquelogs(symbol + " " + m_UserID, "GetOrderIdFromMasterList :  order is closed " + orderId, MessageType.Informational);
                    //}
                    WriteUniquelogs(symbol + " " + m_UserID, "GetOrderIdFromMasterList :  Retrived order from dictionary " + orderId, MessageType.Informational);
                }
                else
                {
                    WriteUniquelogs(symbol + " " + m_UserID, "GetOrderIdFromMasterList :  Dictionary not contains the symbol", MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOrderIdFromMasterList :  Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return orderId;
        }

        public bool CheckOrderStatus(string TradingSymbol, string orderId)
        {
            Trace.WriteLine("***********Started time " + DateTime.Now);
            bool isOpen = false;
            string Symbol = "";
            if (TradingSymbol.Contains("_"))
                Symbol = TradingSymbol.Split('_')[1];
            else
                Symbol = TradingSymbol;
            try
            {
                OrderExtComposite orderExt;
                int counter = 0;
                while (counter <= 1)
                {
                    if (m_GlobalOrderStore.GetOrderbyID(Symbol, Constants.EXCHANGE_NSE, orderId, out orderExt))
                    {
                        if (orderExt != null)
                        {
                            //sanika::28-sep-2020::checked 1 min time after placing order
                            DateTime orderTime = (DateTime)orderExt.order.OrderTimestamp;
                            if (DateTime.Now.Subtract(orderTime).TotalMinutes < 1)
                            {
                                WriteUniquelogs(Symbol + " " + m_UserID, "CheckOrderStatus : order time is less than 1 min", MessageType.Informational);
                                isOpen = true;
                            }
                            string mappedSymbol = orderExt.order.Tradingsymbol + "|" + orderExt.order.Exchange;
                            PositionInfoComposite currentPosition;
                            if (GetCurrentPosition(mappedSymbol, orderExt.order.Product, out currentPosition))
                            {
                                if (currentPosition.netPosition.Quantity != 0)
                                {
                                    WriteUniquelogs(Symbol + " " + m_UserID, "CheckOrderStatus : order is open", MessageType.Informational);
                                    isOpen = true;
                                    break;
                                }
                                else
                                {
                                    WriteUniquelogs(Symbol + " " + m_UserID, "CheckOrderStatus : order is not open", MessageType.Informational);
                                    isOpen = false;
                                    break;
                                }
                            }
                            else
                            {
                                WriteUniquelogs(Symbol + " " + m_UserID, "CheckOrderStatus : Not get any current position", MessageType.Informational);
                            }
                        }
                        else
                        {
                            WriteUniquelogs(Symbol + " " + m_UserID, "CheckOrderStatus : order count is zero", MessageType.Informational);
                        }
                    }
                    else if (m_GlobalOrderStore.GetOrderbyID(Symbol, Constants.EXCHANGE_NFO, orderId, out orderExt))
                    {
                        if (orderExt != null)
                        {
                            //sanika::28-sep-2020::checked 1 min time after placing order
                            DateTime orderTime = (DateTime)orderExt.order.OrderTimestamp;
                            if (DateTime.Now.Subtract(orderTime).TotalMinutes < 1)
                            {
                                WriteUniquelogs(Symbol + " " + m_UserID, "CheckOrderStatus : order time is less than 1 min", MessageType.Informational);
                                isOpen = true;
                            }

                            string mappedSymbol = orderExt.order.Tradingsymbol + "|" + orderExt.order.Exchange;
                            PositionInfoComposite currentPosition;
                            if (GetCurrentPosition(mappedSymbol, orderExt.order.Product, out currentPosition))
                            {
                                if (currentPosition.netPosition.Quantity != 0)
                                {
                                    WriteUniquelogs(Symbol + " " + m_UserID, "CheckOrderStatus : order is open", MessageType.Informational);
                                    isOpen = true;
                                    break;
                                }
                                else
                                {
                                    WriteUniquelogs(Symbol + " " + m_UserID, "CheckOrderStatus : order is not open", MessageType.Informational);
                                    isOpen = false;
                                    break;
                                }
                            }
                            else
                            {
                                WriteUniquelogs(Symbol + " " + m_UserID, "CheckOrderStatus : Not get any current position", MessageType.Informational);
                            }
                        }
                        else
                        {
                            WriteUniquelogs(Symbol + " " + m_UserID, "CheckOrderStatus : order count is zero", MessageType.Informational);
                        }
                    }
                    else
                    {
                        WriteUniquelogs(Symbol + " " + m_UserID, "CheckOrderStatus : order not updated yet or not present ", MessageType.Informational);
                    }
                    counter++;
                    Thread.Sleep(1000);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(Symbol + " " + m_UserID, "CheckOrderStatus :Error Exception Message = " + e.Message, MessageType.Exception);
            }
            Trace.WriteLine("***********End time " + DateTime.Now);
            return isOpen;
        }


        public bool CheckWithinTime(string strDateTime, int interval, string key)
        {
            try
            {
                WriteUniquelogs(key, "CheckWithinTime : strDateTime = " + strDateTime + " interval= " + interval + " key = " + key, MessageType.Informational);
                DateTime oDate = DateTime.Parse(strDateTime);
                WriteUniquelogs(key, "CheckWithinTime : oDate = " + oDate, MessageType.Informational);
                WriteUniquelogs(key, "CheckWithinTime : Passed Date : " + oDate, MessageType.Informational);
                DateTime currDate = DateTime.Now;
                WriteUniquelogs(key, "CheckWithinTime : current Date : " + currDate, MessageType.Informational);
                int diff = Math.Abs(Convert.ToInt32((oDate - currDate).TotalMinutes));
                WriteUniquelogs(key, "CheckWithinTime : Difference : " + diff, MessageType.Informational);
                if (diff <= interval)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(key, "CheckWithinTime : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return false;
        }

        public void InsertMessageDetails(string datestr, int signal, string name, string key)
        {
            try
            {
                WriteUniquelogs(key, "InsertMessageDetails : datestr :" + datestr + " signal = " + signal + " name = " + name + " key " + key, MessageType.Informational);
                DateTime Fdate = DateTime.Parse(datestr);
                WriteUniquelogs(key, "InsertMessageDetails : Fdate :" + Fdate, MessageType.Informational);
                //DateTime Fdate;
                //bool isSuccess5 = DateTime.TryParseExact(datestr, "yyyy-MM-dd h:mm:ss tt", CultureInfo.InvariantCulture, DateTimeStyles.None, out Fdate);
                //int firstSpaceIndex = datestr.IndexOf(" ");
                //string d = datestr.Substring(0, firstSpaceIndex); // INAGX4
                //string ti = datestr.Substring(firstSpaceIndex + 1); // Agatti Island
                //string[] spliitedDateTime = datestr.Split(' ');
                //string date = Convert.ToDateTime(d).ToString("yyyy-MM-dd");
                //DateTime datetime = DateTime.Parse(ti, System.Globalization.CultureInfo.CurrentCulture);
                //string t = datetime.ToString("h:mm:ss tt");
                //string dt = date + " " + t;
                //DateTime Fdate = DateTime.ParseExact(dt, "yyyy-MM-dd h:mm:ss tt", null);// Convert.ToDateTime(datestr.ToString("yyyy-MM-dd"),null);

                if (OpenConnection())
                {
                    if (TableExistsforKeyTable(key) == false)
                    {
                        string sql_new = "CREATE TABLE '" + key + "'(DateTimeFormat DATETIME NOT NULL PRIMARY KEY, Signal int NOT NULL,  Name varchar(20) NOT NULL, Key varchar(20) NOT NULL)";
                        sql_cmd = new SQLiteCommand(sql_new, sql_con);
                        sql_cmd.ExecuteNonQuery();
                    }
                    try
                    {
                        string insertSQLQuery = "insert into " + key + " (DateTimeFormat, Signal, Name, Key) values ('" + Fdate + "'," + signal + ",'" + name + "','" + key + "');";
                        sql_cmd = new SQLiteCommand(insertSQLQuery, sql_con);
                        sql_cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        WriteUniquelogs(key, "InsertMessageDetails : Exception Message :" + ex, MessageType.Exception);
                    }
                }
                else
                {
                    WriteUniquelogs(key, "InsertMessageDetails : Failed to establish connection", MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(key, "InsertMessageDetails : Exception Message " + e.Message, MessageType.Exception);
            }
        }

        public bool TableExistsforKeyTable(string Table_Name)
        {
            try
            {
                using (sql_cmd = new SQLiteCommand(sql_con))
                {
                    sql_cmd.CommandText = "SELECT * FROM sqlite_master WHERE type='table' AND name=@name";
                    sql_cmd.Parameters.AddWithValue("@name", Table_Name);

                    using (sql_read = sql_cmd.ExecuteReader())
                    {
                        if (sql_read != null && sql_read.HasRows)
                        {
                            WriteUniquelogs(Table_Name, "TableExistsforKeyTable : table exist", MessageType.Informational);
                            return true;
                        }
                        else
                        {
                            WriteUniquelogs(Table_Name, "TableExistsforKeyTable : table not exist", MessageType.Informational);
                            return false;
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return false;
        }

        public void GetMessagePosition(string tablename)
        {
            SQLiteConnection Sql_con;
            SQLiteCommand Sql_cmd;
            SQLiteDataReader Sql_read;
            SQLiteTransaction Tr;
            var path_DB = Directory.GetCurrentDirectory();
            string directoryName = path_DB + "\\" + "Database";
            //20-July-2021: sandip:use common name
            string DBFilePath = directoryName + "\\" + tokenDBName;
            try
            {
                WriteUniquelogs(tablename, "GetMessagePosition : tablename : " + tablename, MessageType.Informational);
                string connectionString = String.Format("Data Source={0};Version=3;New=False;Compress=True;PRAGMA locking_mode=EXCLUSIVE;", DBFilePath);
                if (TableExistsforKeyTable(tablename) == false)
                {
                    WriteUniquelogs(tablename, "GetMessagePosition : tablename not exist: " + tablename, MessageType.Informational);
                    return;
                }
                using (Sql_con = new SQLiteConnection(connectionString))
                {
                    Sql_con.Open();

                    using (Tr = Sql_con.BeginTransaction())
                    {
                        string query = "SELECT * FROM " + tablename + " order by DateTimeFormat desc  LIMIT 1;";
                        Sql_cmd = new SQLiteCommand(query, Sql_con);
                        Sql_read = Sql_cmd.ExecuteReader();
                        if (Sql_read != null && Sql_read.HasRows)
                        {
                            while (Sql_read.Read())
                            {
                                string name = (string)Sql_read["Name"];
                                string key = (string)Sql_read["Key"];
                                WriteUniquelogs(tablename, "key : " + key + "Name : " + name + "Signal : " + (int)Sql_read["Signal"], MessageType.Informational);
                            }
                        }
                        Tr.Commit();
                    }
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(tablename, "GetMessagePosition : Exception : " + e.Message, MessageType.Exception);
            }
        }

        //IRDS::03-Jul-2020::Jyoti::added for historical data download in mysql
        public void DownloadDataMysql(List<string> TradingSymbolList, int daysForHistroricalData)
        {
            if (daysForHistroricalData > 0)
            {
                bool bCommit = false;
                SQLiteCommand sql_cmdlocal;
                try
                {
                    //sanika::28-Jul-2021::changed object name as double objects are created
                    if (m_MySqlConnection == null)
                    {
                        m_MySqlConnection = new MySQLConnectZerodha();
                        if (m_MySqlConnection.Connect(logger, "localhost", "root", "root", "composite"))
                        {
                            m_isConnected = true;
                        }
                    }
                    DateTime dateTimeForTimeCondition = DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd") + " 15:29:00", "yyyy-MM-dd HH:mm:ss", null);
                    string dateTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm") + ":00";
                    //string dateTime = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
                    DateTime todateTime = DateTime.ParseExact(dateTime, "yyyy-MM-dd HH:mm:ss", null).AddMinutes(1);
                    DateTime startdateTime = todateTime.AddDays(-daysForHistroricalData);
                    todateTime = todateTime.AddDays(1);
                    WriteUniquelogs("DownloadData", "startdateTime = " + startdateTime + " todateTime = " + todateTime, MessageType.Informational);
                    int exchange = 1;
                    foreach (var name in TradingSymbolList)
                    {
                        string[] strAarray = name.Split('.');
                        string TradingSymbol = strAarray[0];
                        if (strAarray[1] == Constants.EXCHANGE_NFO)
                        {
                            exchange = 2;
                            TradingSymbol += composite.FetchTableName();
                        }
                        try
                        {
                            m_MySqlConnection.beginTran();

                            if (DatabaseCreated.ContainsKey(TradingSymbol) == false)
                            {
                                if (m_MySqlConnection.IsTableExists(TradingSymbol, "composite"))
                                {
                                    //m_Mysql.CleanDatabase(TradingSymbol);
                                }
                                else
                                {
                                    m_MySqlConnection.CreateTable(TradingSymbol, "Symbol varchar(50) NOT NULL, LastTradeTime datetime NOT NULL, Open double NOT NULL, High double NOT NULL,Low double NOT NULL,Close double NOT NULL,Volume BIGINT NOT NULL", "LastTradeTime");
                                }
                            }
                            else
                            {
                                DatabaseCreated[TradingSymbol] = 1;
                            }

                            string token = composite.GetToken(name);
                            //List<Historicalcomposite> historicalList = 
                            string historicalDataAll = composite.GetHistoricalData(token, startdateTime, todateTime, "60", exchange.ToString());
                            string[] historicalList = historicalDataAll.Split(',');
                            int counter = 0;
                            string insertData = "";
                            foreach (var historical in historicalList)
                            {
                                counter++;
                                string[] values = historical.Split('|');
                                string timestamp = (new DateTime(1970, 1, 1)).AddSeconds(Convert.ToDouble(values[0])).ToString("yyyy-MM-dd HH:mm:ss");
                                Console.WriteLine(timestamp);
                                DateTime timeStamp = DateTime.ParseExact(timestamp, "yyyy-MM-dd HH:mm:ss", null);
                                string strQuery = "SELECT * FROM " + TradingSymbol + " WHERE LastTradeTime = '" + timestamp + "'";
                                MySqlCommand cmd = new MySqlCommand(strQuery, m_MySqlConnection.mysqlconnDBManipulation);//create command and assign the query and connection from the constructor
                                MySqlDataReader dataReader = cmd.ExecuteReader(); //Create a data reader and Execute the command

                                if (!(dataReader != null && dataReader.HasRows))
                                {
                                    if (timeStamp <= dateTimeForTimeCondition)
                                    {
                                        insertData = insertData + "('" + TradingSymbol + "', STR_TO_DATE('" + timestamp + "', '%Y-%m-%d %H:%i:%s'),'" + values[1] + "', '" + values[2] + "', '" + values[3] + "', '" + values[4] + "', '" + values[5] + "'),";
                                    }
                                }
                                dataReader.Close();
                            }
                            if (insertData.Length > 1)
                            {
                                WriteUniquelogs("DownloadData", "Msql Data Downloaded for " + TradingSymbol, MessageType.Informational);
                                insertData = insertData.Remove(insertData.Length - 1, 1);
                                m_MySqlConnection.InsertHistDataMultipleValues(TradingSymbol, insertData);
                            }
                        }
                        catch (Exception er)
                        {
                            WriteUniquelogs("DownloadData", "DownloadDataMysql : Exception Error Message =  " + TradingSymbol + " :: " + er.Message, MessageType.Exception);
                        }
                        finally
                        {
                            m_MySqlConnection.CommitTran();
                        }
                    }
                    WriteUniquelogs("DownloadData", "Data Downloaded", MessageType.Informational);
                }
                catch (Exception er)
                {
                    Console.WriteLine(er.Message);
                    WriteUniquelogs("DownloadData", "Exception Error Message " + er.Message, MessageType.Exception);
                }
            }
        }
        //IRDSPM::Pratiksha::-01-07-2020:: Fetching tablename and symbolname method call --start
        public List<string> FetchDetails(string exchange)
        {
            if (composite != null)
            {
                return composite.GetDataforTradingsymbol(exchange);
            }
            else
            {
                return null;
            }
        }
        //IRDSPM::Pratiksha::-01-07-2020:: Fetching tablename and symbolname method call --end
        //IRDSPM::Pratiksha::17-07-2020::Added Db connection code --start
        public bool dbConnectionAndInsert(string name, string email, string number, string clientid, string adminid)
        {
            Boolean bolUserInformation = false;
            try
            {
                //Connection is created with database
                MySqlConnection MysqlConn = new MySqlConnection(ConnString);

                //opens connection
                MysqlConn.Open();
                int id = AutoGenerationID();
                //Insert Query is fired
                MySqlCommand Mysqlcmd = new MySqlCommand("insert into userDet values(" + id + ",'" + name + "','" + email + "','" + number + "','" + clientid + "','" + adminid + "')", MysqlConn);
                if (Mysqlcmd.ExecuteNonQuery() == 1) bolUserInformation = true;
                //Connection is Closed
                MysqlConn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("In exception : " + ex);
            }

            return bolUserInformation;
        }
        public int AutoGenerationID()
        {
            int CountForAutoGenerate = 1;
            try
            {
                if (m_MySqlConnection == null)
                {
                    if (logger == null)
                    {
                        logger = Logger.Instance;
                    }
                    m_MySqlConnection = new MySQLConnectZerodha();
                    m_isMysqlconnected = m_MySqlConnection.Connect(logger, m_ServerUrl, m_username, m_password, m_Dbname);
                    WriteUniquelogs("RegistrationProcess", "m_isMysqlconnected : " + m_isMysqlconnected, MessageType.Informational);
                }
                if (m_isMysqlconnected)
                {
                    if (!m_MySqlConnection.IsTableExists("UserDetails", m_Dbname))
                    {
                        m_MySqlConnection.ExecuteNonQueryCommand("Create Table UserDetails(ID int(11),Name varchar(255),Usertype varchar(255),EmailId varchar(255),MobileNo varchar(255) ,UserName varchar(255),Password varchar(255),UserAddress varchar(255),City varchar(255),UserState varchar(255),ClientID varchar(255),ExpiryDate double,Remark varchar(255),Status varchar(255),OnlineInfo varchar(255),AdminId varchar(255),AffiliateID varchar(255),BlockStatus bool, lastlogindate double, PRIMARY KEY(ClientID))");
                    }

                    //IRDSPM::Pratiksha::21-12:2020::Changed the query
                    string retriveCmd = "SELECT ID FROM UserDetails ORDER BY ID DESC LIMIT 1";
                    object obj = m_MySqlConnection.ExecuteScalar(retriveCmd);
                    if (obj == null)
                    {
                        string countCmd = "SELECT MAX(ID) FROM UserDetails";
                        object obj1 = m_MySqlConnection.ExecuteScalar(countCmd);
                        if (obj1.GetType() != typeof(DBNull))
                        {
                            CountForAutoGenerate = Convert.ToInt32(obj) + 1;
                        }
                    }
                    else
                    {
                        if (obj.GetType() != typeof(DBNull))
                        {
                            CountForAutoGenerate = Convert.ToInt32(obj) + 1;
                        }
                    }
                    //else
                    //{
                    //    CountForAutoGenerate = Convert.ToInt32(obj);
                    //    CountForAutoGenerate = CountForAutoGenerate + 1;
                    //}
                    WriteUniquelogs("RegistrationProcess", "AutoGenerationID : " + CountForAutoGenerate, MessageType.Informational);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("RegistrationProcess", "AutoGenerationID : Exception Error Message = " + ex.Message, MessageType.Exception);
            }

            return CountForAutoGenerate;
        }

        public int RetrieveUserdetails(string clientid)
        {
            int count = 0;
            try
            {
                string retriveCmd = "Select count(*) from UserDetails where ClientID= '" + clientid + "';";
                if (m_MySqlConnection == null)
                {
                    if (logger == null)
                    {
                        logger = Logger.Instance;
                    }
                    m_MySqlConnection = new MySQLConnectZerodha();
                    m_isMysqlconnected = m_MySqlConnection.Connect(logger, m_ServerUrl, m_username, m_password, m_Dbname);
                    WriteUniquelogs("RegistrationProcess", "RetrieveUserdetails :  m_isMysqlconnected = " + m_isMysqlconnected, MessageType.Informational);
                }
                if (m_isMysqlconnected)
                {
                    if (!m_MySqlConnection.IsTableExists("UserDetails", m_Dbname))
                    {
                        WriteUniquelogs("RegistrationProcess", "RetrieveUserdetails :  IsTableExists = Table not exist", MessageType.Informational);
                        //create table 
                        m_MySqlConnection.ExecuteNonQueryCommand("Create Table UserDetails(ID int(11),Name varchar(255),Usertype varchar(255),EmailId varchar(255),MobileNo varchar(255) ,UserName varchar(255),Password varchar(255),UserAddress varchar(255),City varchar(255),UserState varchar(255),ClientID varchar(255),ExpiryDate double,Remark varchar(255),Status varchar(255),OnlineInfo varchar(255),AdminId varchar(255),AffiliateID varchar(255),BlockStatus bool, lastlogindate double, PRIMARY KEY(ClientID))");
                    }
                    object obj = m_MySqlConnection.ExecuteScalar(retriveCmd);
                    if (obj.GetType() != typeof(DBNull))
                    {
                        count = Convert.ToInt32(obj);
                    }
                    WriteUniquelogs("RegistrationProcess", "RetrieveUserdetails :  found user count with client id you have given = " + count, MessageType.Exception);
                }
            }
            catch (Exception ex)
            {
                count = -1;
                WriteUniquelogs("RegistrationProcess", "RetrieveUserdetails :  Exception Error Message = " + ex.Message, MessageType.Exception);
            }

            return count;
        }
        public bool UpdateUserInformation(string name, string Usertype, string email, string mob, string password, string address, string city, string state, string clientid, double expirydate, string remark, string status, string online, string adminid, string AffiliateID, double LastloginDate, bool blockStatus = false)
        {
            bool bolUpdateInfo = false;
            if (m_MySqlConnection == null)
            {
                m_MySqlConnection = new MySQLConnectZerodha();
                m_isMysqlconnected = m_MySqlConnection.Connect(logger, m_ServerUrl, m_username, m_password, m_Dbname);
            }

            if (m_isMysqlconnected)
            {
                string selectCmd = "update UserDetails set Name = '" + name + "',UserType = '" + Usertype + "',EmailId = '" + email + "',MobileNo = '" + mob + "',Password = '" + password + "',UserAddress = '" + address + "',City = '" + city + "',UserState = '" + state + "',ExpiryDate = " + expirydate + ",Remark = '" + remark + "',Status = '" + status + "',OnlineInfo = '" + online + "',AdminId = '" + adminid + "',BlockStatus = " + blockStatus + ",lastlogindate = " + LastloginDate + " where ClientID = '" + clientid + "' and AffiliateID = '" + AffiliateID + "'";
                m_MySqlConnection.ExecuteNonQueryCommand(selectCmd);
                bolUpdateInfo = true;
            }
            return bolUpdateInfo;
        }

        public int GetOptionsLotSize(string OptionsSymbol, string Exchange)
        {
            int lotsize = 0;
            try
            {
                if (sql_conLotSize == null)
                {
                    OpenConnectionForLOtSize();
                }

                string tableName = Exchange;
                //string symbol = TradingSymbol + tableName;

                string sql = "SELECT lotSize from '" + tableName + "' WHERE optionsSymbol = '" + OptionsSymbol + "'";
                sql_cmd = new SQLiteCommand(sql, sql_conLotSize);
                sql_read = sql_cmd.ExecuteReader();

                if (sql_read != null && sql_read.HasRows)
                {
                    if (sql_read.Read())
                    {
                        lotsize = (int)sql_read["lotsize"];
                    }
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(OptionsSymbol + " " + m_UserID, "GetLotSize :  Exception " + e.Message, MessageType.Exception);
            }
            WriteUniquelogs(OptionsSymbol + " " + m_UserID, "GetLotSize :  lotsize " + lotsize, MessageType.Informational);
            return lotsize;
        }

        public string getOptionsInstrumentToken(string symbolName, string tableName)
        {
            string token = "";
            try
            {
                if (sql_conLotSize == null)
                {
                    /*var path_DB = Directory.GetCurrentDirectory();
                    string directoryName = path_DB + "\\" + "Database";

                    string DBFilePath = directoryName + "\\" + "composite.db";
                    string connectionString = String.Format("Data Source={0};Version=3;New=False;Compress=True;PRAGMA locking_mode=EXCLUSIVE;", DBFilePath);
                    sql_con = new SQLiteConnection(connectionString);
                    sql_con.Open();*/
                    if (OpenConnectionForLOtSize())
                    {
                        //isConnected = true;
                    }
                    else
                    {
                        WriteGUIlogs(m_ExceptionCounter, "Error : DB is not open");
                        sql_conLotSize = null;
                    }
                }
                //20-July-2021: sandip: check object present
                if (sql_conLotSize != null)
                {
                    SQLiteTransaction TrToken;
                    SQLiteCommand Sql_cmdToken;
                    SQLiteDataReader Sql_readToken;

                    string query = "select InstrumentToken from OPTIONS where optionsSymbol = '" + symbolName + "'";
                    Sql_cmdToken = new SQLiteCommand(query, sql_conLotSize);
                    Sql_readToken = Sql_cmdToken.ExecuteReader();
                    if (Sql_readToken != null && Sql_readToken.HasRows)
                    {
                        while (Sql_readToken.Read())
                        {
                            token = (Sql_readToken["InstrumentToken"]).ToString();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                logger.LogMessage("getOptionsInstrumentToken : Exception Error Message = " + e.Message, MessageType.Exception);
                Console.WriteLine("Exception : " + e.Message);
            }
            return token;
        }

        public string GetToken(string symbol)
        {
            string token = composite.GetToken(symbol);
            return token;
        }
        //IRDSPM::Pratiksha::17-07-2020::Added Db connection code --end

        public bool GetBidAndAskPrice(long InstruementId, string Exchange, out double BidPrice, out double AskPrice)
        {
            bool result = false;
            BidPrice = 0;
            AskPrice = 0;
            try
            {
                int exchange = 0;
                int xtsMessageCode = 0;
                if (Exchange == "NSECM")
                {
                    exchange = (int)ExchangeSegment.NSECM;
                }
                else if (Exchange == "NSEFO")
                {
                    exchange = (int)ExchangeSegment.NSEFO;
                }
                else if (Exchange == "NSECD")
                {
                    exchange = (int)ExchangeSegment.NSECD;
                }

                var res = composite.GetQuote(InstruementId, exchange, (int)MarketDataPorts.marketDepthEvent);

                if (res.Count > 0)
                {
                    BidPrice = Convert.ToDouble(res["Bids"][0]["Price"]);
                    AskPrice = Convert.ToDouble(res["Asks"][0]["Price"]);
                    result = true;
                }

                return result;
            }
            catch (Exception e)
            {
                result = false;
            }
            return result;
        }

        private static readonly object sync = new Object();
        public void DownloadDataEx(List<string> TradingSymbolList, int daysForHistroricalData)
        {

            //TradingSymbolList = UpdateSymbolList(TradingSymbolList);
            //if (m_SotreDataInMysql || m_HistoricalDataInMysqlDB)
            //{
            //    DownloadDataMysql(TradingSymbolList, daysForHistroricalData);
            //}
            //sanika::28-Jul-2021::changed object name as double objects are created
            if (daysForHistroricalData > 0)
            {
                SqliteConnectZerodha sqliteConnectZerodha = new SqliteConnectZerodha();
                if (m_StoreDataInMysql || m_HistoricalDataInMysqlDB)
                {
                    if (m_MySqlConnection == null)
                    {
                        m_MySqlConnection = new MySQLConnectZerodha();
                        if (m_MySqlConnection.Connect(logger, composite.dbServer, composite.dbUserid, composite.dbPassword, composite.databaseName))
                        {
                            m_isConnected = true;
                        }
                    }
                }
                bool bCommit = false;
                try
                {
                    bool isconnected = sqliteConnectZerodha.Connect("compositedata.db");
                    string dateTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm") + ":00";
                    DateTime todateTime = DateTime.ParseExact(dateTime, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture).AddMinutes(1);
                    DateTime startdateTime = todateTime.AddDays(-daysForHistroricalData);
                    todateTime = todateTime.AddDays(1);
                    int Interval = 60;
                    WriteUniquelogs("DownloadData", "Started Time = " + DateTime.Now, MessageType.Informational);
                    WriteUniquelogs("DownloadData", "startdateTime = " + startdateTime + " todateTime = " + todateTime, MessageType.Informational);
                    //sanika::8-dec-2020::added for dynamic interval
                    string interval = GetInterval(m_Interval);
                    int exchange = 1;
                    foreach (var name in TradingSymbolList)
                    {
                        string[] strAarray = name.Split('.');
                        string TradingSymbol = strAarray[0];
                        if (strAarray[1] == Constants.EXCHANGE_NFO || strAarray[1] == Constants.EXCHANGE_CDS)
                        {
                            exchange = 2;
                            //IRDS::05-Sep-2020::Jyoti::Commented for saving original symbol
                            if (!TradingSymbol.Contains("-I"))
                                TradingSymbol += composite.FetchTableName();
                        }
                        string token = composite.GetToken(name);
                        CheckSqliteTableIsCreatedOrNot(TradingSymbol, sqliteConnectZerodha);
                        if (m_StoreDataInMysql || m_HistoricalDataInMysqlDB)
                            CheckMysqlTableExistOrNot(TradingSymbol);
                        GetHistoricalData(TradingSymbol, token, daysForHistroricalData, interval, startdateTime.ToString(), todateTime.ToString(), sqliteConnectZerodha, exchange);
                        WriteUniquelogs("DownloadData", "Data Downloaded for " + TradingSymbol, MessageType.Informational);
                    }
                    WriteUniquelogs("DownloadData", "Data Downloaded = " + DateTime.Now, MessageType.Informational);
                }
                catch (Exception er)
                {
                    WriteUniquelogs("DownloadData", "DownloadData : Exception Error Message " + er.Message, MessageType.Exception);
                }
            }
        }

        private bool GetHistoricalData(string strSymbolName, string token, int intBarIntervalQuantity, string strBarIntervalUnit, string strStartDate, string strEndDate, SqliteConnectZerodha sqliteConnectZerodha, int exchange)
        {
            Boolean bolGetHistoricalBarBackData = false;
            try
            {
                WriteUniquelogs("DownloadData", "GetHistoricalData : SymbolName: " + strSymbolName + " , BarIntervalQuantity: " + intBarIntervalQuantity.ToString() + " , BarIntervalUnit: " + strBarIntervalUnit + " , StartDate: " + strStartDate + " , EndDate: " + strEndDate, MessageType.Informational);

                Dictionary<double, string> lstHistoricialQuotes = new System.Collections.Generic.Dictionary<double, string>();

                Dictionary<double, string> lstHistoricialQuotes1 = new System.Collections.Generic.Dictionary<double, string>();
                string strErrorMessage = "";
                bool ContinueExecution = false;
                string Interval = "";
                string EndDate = Convert.ToDateTime(strEndDate).ToString();
                Interval = "30";

                System.Globalization.CultureInfo tempCultureInfo = System.Globalization.CultureInfo.InvariantCulture;
                string CurrentDate = DateTime.Now.AddMinutes(1).ToString("dd-MM-yyyy", tempCultureInfo);
                DateTime current = DateTime.ParseExact(CurrentDate, "dd-MM-yyyy", null);
                while (!ContinueExecution)
                {
                    lock (sync)
                    {
                        strEndDate = Convert.ToDateTime(strStartDate).AddDays(Convert.ToInt32(Interval)).ToString();
                        string NewEndDate = strEndDate;
                        string NewStartDate = strStartDate;

                        if (Convert.ToDateTime(NewStartDate) > current)
                        {
                            break;
                        }
                        else
                        {
                            if (Convert.ToDateTime(NewEndDate) > Convert.ToDateTime(EndDate))
                            {
                                strEndDate = EndDate;
                                ContinueExecution = true;
                            }
                        }
                        string historicalAll = composite.GetHistoricalData(token, Convert.ToDateTime(strStartDate), Convert.ToDateTime(strEndDate), strBarIntervalUnit, exchange.ToString());
                        string[] historicalList = historicalAll.Split(',');
                        InsertHistoricalDataIntoSqlite(historicalList, strSymbolName, sqliteConnectZerodha);
                        if (m_StoreDataInMysql || m_HistoricalDataInMysqlDB)
                            InsertHistoricalDataIntoMysql(historicalList, strSymbolName);
                        if (strBarIntervalUnit == "minute")
                        {
                            strStartDate = Convert.ToDateTime(strEndDate).AddMinutes(1).ToString();
                        }
                        else
                        {
                            strStartDate = strEndDate;
                        }
                    }
                }
                WriteUniquelogs("DownloadData", "GetHistoricalData : Data Downloaded", MessageType.Informational);
            }
            catch (Exception ex)
            {
                WriteUniquelogs("DownloadData", "GetHistoricalData : Exception Error Message = " + ex.Message, MessageType.Informational);
            }
            return bolGetHistoricalBarBackData;
        }

        //sanika::28-Oct-2020::Added for historical data
        public void InsertHistoricalDataIntoSqlite(string[] historicalList, string TradingSymbol, SqliteConnectZerodha sqliteConnectZerodha)
        {
            try
            {
                int counter = 0;
                List<String> DBInsertQueries = new List<string>();
                DateTime dateTimeForTimeCondition = DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd") + " 15:29:00", "yyyy-MM-dd HH:mm:ss", null);
                foreach (string historical in historicalList)
                {
                    string[] values = historical.Split('|');
                    counter++;
                    string timestamp = (new DateTime(1970, 1, 1)).AddSeconds(Convert.ToDouble(values[0])).ToString("yyyy-MM-dd HH:mm:ss");
                    int lastIndex = timestamp.LastIndexOf(":");
                    if (lastIndex != -1)
                    {
                        string time = timestamp.Substring(0, lastIndex);
                        timestamp = time + ":00";
                    }
                    DateTime timeStamp = DateTime.ParseExact(timestamp, "yyyy-MM-dd HH:mm:ss", null);

                    lock (databaselock)
                    {
                        if (timeStamp <= dateTimeForTimeCondition)
                        {
                            string insertSQLQuery = "INSERT OR IGNORE into '" + TradingSymbol + "' (TickDateTime, Open, High, Low, Close, Volume, DoubleDate) values ('" + timeStamp + "','" + values[1] + "','" + values[2] + "','" + values[3] + "','" + values[4] + "','" + values[5] + "','" + timeStamp.ToOADate() + "')";
                            DBInsertQueries.Add(insertSQLQuery);
                            //WriteUniquelogs("DownloadData", "Trading symbol = " + TradingSymbol + " " + insertSQLQuery, MessageType.Informational);
                        }
                        if (counter == historicalList.Length - 1 && DBInsertQueries.Count > 0)
                        {
                            WriteUniquelogs("DownloadData", "InsertHistoricalDataIntoSqlite : Data Downloaded for " + TradingSymbol, MessageType.Informational);
                            sqliteConnectZerodha.InsertDataMultipleValues(TradingSymbol, DBInsertQueries);
                            DBInsertQueries.Clear();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs("DownloadData", "InsertHistoricalDataIntoSqlite :  Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        //sanika::28-Oct-2020::Added for historical data
        public void InsertHistoricalDataIntoMysql(string[] historicalList, string TradingSymbol)
        {
            try
            {
                //sanika::28-Jul-2021::changed object name as double objects are created
                int counter = 0;
                string insertData = "";
                DateTime dateTimeForTimeCondition = DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd") + " 15:29:00", "yyyy-MM-dd HH:mm:ss", null);
                foreach (string historical in historicalList)
                {
                    string[] values = historical.Split('|');
                    counter++;
                    string timestamp = (new DateTime(1970, 1, 1)).AddSeconds(Convert.ToDouble(values[0])).ToString("yyyy-MM-dd HH:mm:ss");
                    int lastIndex = timestamp.LastIndexOf(":");
                    if (lastIndex != -1)
                    {
                        string time = timestamp.Substring(0, lastIndex);
                        timestamp = time + ":00";
                    }
                    DateTime timeStamp = DateTime.ParseExact(timestamp, "yyyy-MM-dd HH:mm:ss", null);

                    {
                        string strQuery = "SELECT * FROM `" + TradingSymbol + "` WHERE LastTradeTime = '" + timestamp + "'";
                        MySqlCommand cmd = new MySqlCommand(strQuery, m_MySqlConnection.mysqlconnDBManipulation);//create command and assign the query and connection from the constructor
                        MySqlDataReader dataReader = cmd.ExecuteReader(); //Create a data reader and Execute the command

                        if (!(dataReader != null && dataReader.HasRows))
                        {
                            //lock (databaselock)
                            {
                                if (timeStamp <= dateTimeForTimeCondition)
                                {
                                    insertData = insertData + "('" + TradingSymbol + "', STR_TO_DATE('" + timestamp + "', '%Y-%m-%d %H:%i:%s'),'" + values[1] + "', '" + values[2] + "', '" + values[3] + "', '" + values[4] + "', '" + values[5] + "'),";
                                }
                            }
                        }
                        dataReader.Close();
                    }
                }
                if (insertData.Length > 1)
                {
                    WriteUniquelogs("DownloadData", "InsertHistoricalDataIntoMysql : Msql Data Downloaded for " + TradingSymbol, MessageType.Informational);
                    insertData = insertData.Remove(insertData.Length - 1, 1);
                    m_MySqlConnection.InsertHistDataMultipleValues(TradingSymbol, insertData);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs("DownloadData", "InsertHistoricalDataIntoMysql :  Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        //sanika::28-Oct-2020::Added for historical data
        public void CheckSqliteTableIsCreatedOrNot(string TradingSymbol, SqliteConnectZerodha sqliteConnectZerodha)
        {
            try
            {
                if (DatabaseCreated.ContainsKey(TradingSymbol) == false)
                {
                    lock (databaselock)
                    {
                        if (sqliteConnectZerodha.IsTableExists(TradingSymbol) == false)
                        {
                            WriteUniquelogs("DownloadData", "CheckSqliteTableIsCreatedOrNot : Table is created for " + TradingSymbol, MessageType.Informational);
                            sqliteConnectZerodha.ExecuteNonQueryCommand("CREATE TABLE '" + TradingSymbol + "'(TickDateTime DateTime PRIMARY KEY, Open real NOT NULL, High real NOT NULL,Low real NOT NULL,Close real NOT NULL,Volume real NOT NULL,DoubleDate real NOT NULL)");
                        }
                    }
                }
                else
                {
                    DatabaseCreated[TradingSymbol] = 1;
                }
            }
            catch (Exception er)
            {
                WriteUniquelogs("DownloadData", "CheckSqliteTableIsCreatedOrNot : Exception Error Message " + er.Message, MessageType.Exception);
            }
        }

        //sanika::28-Oct-2020::Added for historical data
        public void CheckMysqlTableExistOrNot(string TradingSymbol)
        {
            try
            {
                //sanika::28-Jul-2021::changed object name as double objects are created
                if (DatabaseCreatedMysql.ContainsKey(TradingSymbol) == false)
                {
                    if (m_MySqlConnection.IsTableExists(TradingSymbol, objConfigSettings.databaseName))
                    {
                        //m_Mysql.CleanDatabase(TradingSymbol);
                    }
                    else
                    {
                        m_MySqlConnection.CreateTable(TradingSymbol, "Symbol varchar(50) NOT NULL, LastTradeTime datetime NOT NULL, Open double NOT NULL, High double NOT NULL,Low double NOT NULL,Close double NOT NULL,Volume BIGINT NOT NULL", "LastTradeTime");
                        WriteUniquelogs("DownloadData", "CheckMysqlTableExistOrNot : Table is created for " + TradingSymbol, MessageType.Informational);
                    }
                }
                else
                {
                    DatabaseCreatedMysql[TradingSymbol] = 1;
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs("DownloadData", "CheckMysqlTableExistOrNot : Exception Error Message " + e.Message, MessageType.Exception);
            }
        }

        public string CloseOrderEx(string TradingSymbol, string Exchange, string Product)
        {
            string orderId = "NA";
            try
            {
                string newOrderBuyOrSell = "";
                int newOrderQty = 0;
                string mappedSymbol = TradingSymbol + "|" + Exchange;
                if (CancelAllPendingOrder(TradingSymbol, Exchange))
                {
                    //nothing
                }
                PositionInfoComposite currentPosition;
                if (!GetCurrentPosition(mappedSymbol, Product, out currentPosition))
                {
                    string errorMsg = string.Format("CloseOrderEx : Not found current position for symbol : {0}", mappedSymbol);
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, errorMsg, MessageType.Error);

                    if (bGetCurrentPositionError)
                    {
                        return orderId;
                    }
                }
                else
                {
                    //sanika::28-sep-2020::added direction in logs/toast
                    string direction = "";
                    string logMessage = string.Format("CloseOrderEx : Current position fetched from server for mapped symbol: {0} Day Position: {1} , Net Position: {2}",
                                                  mappedSymbol, currentPosition.dayPosition.Quantity, currentPosition.netPosition.Quantity);
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, logMessage, MessageType.Informational);
                    if ((currentPosition.dayPosition.Quantity > 0 || currentPosition.netPosition.Quantity > 0))
                    {
                        newOrderBuyOrSell = Constants.TRANSACTION_TYPE_SELL;
                        //sanika::28-sep-2020::added direction in logs/toast
                        direction = Constants.TRANSACTION_TYPE_BUY; ;
                    }
                    else if ((currentPosition.dayPosition.Quantity < 0 || currentPosition.netPosition.Quantity < 0))
                    {
                        newOrderBuyOrSell = Constants.TRANSACTION_TYPE_BUY;
                        //sanika::28-sep-2020::added direction in logs/toast
                        direction = Constants.TRANSACTION_TYPE_SELL;
                    }

                    //IRDS::17-JUL-2020 :: Add comment because day position quantity got wrong :: sandip/sanika

                    //if (Math.Abs(currentPosition.dayPosition.Quantity) > Math.Abs(currentPosition.netPosition.Quantity))
                    //    newOrderQty = Math.Abs(currentPosition.dayPosition.Quantity);
                    //else

                    newOrderQty = Math.Abs(currentPosition.netPosition.Quantity);

                    if (newOrderQty != 0)
                    {
                        WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseOrderEx :newOrderQty is not zero" + newOrderQty, MessageType.Informational);
                        orderId = PlaceOrder(Exchange, TradingSymbol, newOrderBuyOrSell, newOrderQty, Constants.ORDER_TYPE_MARKET, Product: Product);
                        if (orderId != "NA")
                        {
                            double openPrice = GetOpenPostionPricebyOrderID(TradingSymbol, Exchange, orderId);
                            WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseOrderEx : Order Complete at price = " + openPrice, MessageType.Informational);
                            WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseOrderEx : order closed successfully", MessageType.Informational);
                            //m_ExceptionDictionary.Add(m_ExceptionCounter, DateTime.Now + " " + "CloseOrder - " + TradingSymbol + " Order Closed successfully!! ");
                            //sanika::16-oct-2020::add information after placing order
                            WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + TradingSymbol + " " + direction + " Order Closed successfully!! ");
                            m_ExceptionCounter++;
                            DateTime dateTime = DateTime.Now;
                            m_LocalOrderStructureInfo.AddOrUpdateOrderInformation(TradingSymbol: TradingSymbol, Exchange: Exchange, ClosedTransactionType: newOrderBuyOrSell, Product: Product, ClosedQuantity: newOrderQty, ClosedOrderDateTime: dateTime, ClosedOrderId: orderId);
                            return orderId;
                        }
                        else
                        {
                            WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseOrderEx : order not closed", MessageType.Informational);
                            WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + TradingSymbol + " " + direction + " Order not Closed");
                            m_ExceptionCounter++;
                        }
                    }
                    else
                    {
                        WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseOrderEx :newOrderQty is zero", MessageType.Informational);
                        WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + TradingSymbol + " Order closed already!!");
                        m_ExceptionCounter++;
                    }
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseOrderEx :  Exception Error Message " + e.Message, MessageType.Exception);
            }
            return orderId;
        }

        public Dictionary<string, dynamic> CancelOrderEX(string OrderId, string Variety = Constants.VARIETY_REGULAR, string ParentOrderId = null)
        {
            try
            {
                var getCancelOrder = composite.CancelOrder(OrderId, Variety, ParentOrderId);
                //sanika::13-July-2021::set flag of margin updation after cancelling order
                m_IsMarginLoad = true;
                //20-July-2021: sandip: update orders once change order placing.
                composite.SetOrderUpdateStatus(true);
                return getCancelOrder;
            }
            catch (Exception e)
            {

            }
            return null;
        }

        public bool SetSymbolLastOrder(string TradingSymbol, string TransactionType)
        {
            bool isSymbolSet = false;
            string symbol = "";
            if (TradingSymbol.Contains("_"))
                symbol = TradingSymbol.Split('_')[1];
            else
                symbol = TradingSymbol;
            try
            {
                if (m_OrderListWithSymbolAndDirection.ContainsKey(symbol))
                {
                    m_OrderListWithSymbolAndDirection[symbol] = TransactionType;
                }
                else
                {
                    m_OrderListWithSymbolAndDirection.Add(symbol, TransactionType);
                }
                WriteUniquelogs(symbol + " " + m_UserID, "SetSymbolLastOrder : Set symbol  " + TradingSymbol + " with transactiontype " + TransactionType, MessageType.Informational);
            }
            catch (Exception e)
            {
                WriteUniquelogs(symbol + " " + m_UserID, "SetSymbolLastOrder : Exception Error Message " + e.Message, MessageType.Exception);
            }
            return isSymbolSet;
        }

        public bool RemoveSymbolLastOrder(string TradingSymbol, string TransactionType)
        {
            bool isSymbolRemove = false;
            bool isSymbolSet = false;
            string symbol = "";
            if (TradingSymbol.Contains("_"))
                symbol = TradingSymbol.Split('_')[1];
            else
                symbol = TradingSymbol;
            try
            {
                if (m_OrderListWithSymbolAndDirection.ContainsKey(symbol))
                {
                    m_OrderListWithSymbolAndDirection.Remove(symbol);
                    WriteUniquelogs(symbol + " " + m_UserID, "RemoveSymbolLastOrder : Removed symbol from list " + TradingSymbol, MessageType.Informational);
                }
                else
                {
                    WriteUniquelogs(symbol + " " + m_UserID, "RemoveSymbolLastOrder :List not contains symbol " + TradingSymbol, MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(symbol + " " + m_UserID, "RemoveSymbolLastOrder : Exception Error Message " + e.Message, MessageType.Exception);
            }
            return isSymbolRemove;
        }

        public string GetSymbolLastOrder(string TradingSymbol)
        {
            string lastOrder = "NA";
            string symbol = "";
            if (TradingSymbol.Contains("_"))
                symbol = TradingSymbol.Split('_')[1];
            else
                symbol = TradingSymbol;
            try
            {
                if (m_OrderListWithSymbolAndDirection.ContainsKey(symbol))
                {
                    lastOrder = m_OrderListWithSymbolAndDirection[symbol];
                    WriteUniquelogs(symbol + " " + m_UserID, "GetSymbolLastOrder : symbol " + TradingSymbol + " transaction type " + lastOrder, MessageType.Informational);
                }
                else
                {
                    WriteUniquelogs(symbol + " " + m_UserID, "GetSymbolLastOrder : List not contains symbol " + TradingSymbol, MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(symbol + " " + m_UserID, "GetSymbolLastOrder : Exception Error Message " + e.Message, MessageType.Exception);
            }

            return lastOrder;
        }

        public void GetFirstTwoTick(string TradingSymbol, string FirstTickTime, string SecondTickTime, out double firstCandleHigh, out double firstCandleLow, out double secondCandleHigh, out double secondCandleLow)
        {
            firstCandleHigh = firstCandleLow = secondCandleHigh = secondCandleLow = 0;
            composite.GetFirstTwoTick(TradingSymbol, FirstTickTime, SecondTickTime, out firstCandleHigh, out firstCandleLow, out secondCandleHigh, out secondCandleLow);
        }

        public void GetCurrentHighLow(string TradingSymbol, string startTime, string endTime, int interval, out double dHigh, out double dLow)
        {
            string symbol = "";
            if (TradingSymbol.Contains('.'))
            {
                symbol = TradingSymbol.Split('.')[0];
            }
            else
            {
                symbol = TradingSymbol;
            }
            WriteUniquelogs(symbol + " " + m_UserID, "GetCurrentHighLow : startTime " + startTime + " endTime " + endTime + " interval " + interval, MessageType.Informational);

            int exchange = 1;
            if (TradingSymbol.Split('.')[1] == "NFO")
            {
                exchange = 2;
            }
            dHigh = dLow = 0;
            //if (m_TickData != null)
            //{
            //    dHigh = Convert.ToDouble(m_TickData.GetHighPrice(TradingSymbol));
            //    dLow = Convert.ToDouble(m_TickData.GetLowPrice(TradingSymbol));
            //}
            string token = composite.GetToken(TradingSymbol);
            string CurrentDate = DateTime.Now.ToString("yyyy-MM-dd");
            DateTime startDateTime = DateTime.ParseExact(CurrentDate + " " + startTime + ":00", "yyyy-MM-dd HH:mm:ss", null);
            DateTime endDateTime = DateTime.ParseExact(CurrentDate + " " + endTime + ":00", "yyyy-MM-dd HH:mm:ss", null);
            string period = GetInterval(interval);//sanika::03-Feb-2021::passed period 
            string historicalAll = composite.GetHistoricalData(token, startDateTime, endDateTime, period, exchange.ToString());
            string[] historicals = historicalAll.Split(',');
            if (historicals.Length > 0 && historicalAll.Length > 1)
            {
                string[] firstCandleValues = historicals[0].Split('|');
                string timestamp = (new DateTime(1970, 1, 1)).AddSeconds(Convert.ToDouble(firstCandleValues[0])).ToString("yyyy-MM-dd HH:mm:ss");
                dHigh = Convert.ToDouble(firstCandleValues[2]);
                dLow = Convert.ToDouble(firstCandleValues[3]);
                WriteUniquelogs(symbol + " " + m_UserID, "GetCurrentHighLow : dHigh " + dHigh + " dLow " + dLow, MessageType.Informational);
            }
            else
            {
                WriteUniquelogs(symbol + " " + m_UserID, "GetCurrentHighLow : historicals data length fetch as 0", MessageType.Informational);
            }
        }

        public void GetTwoTickFromHistoricalData(string TradingSymbol, string FirstTickTime, string SecondTickTime, int interval, out double firstCandleHigh, out double firstCandleLow, out double secondCandleHigh, out double secondCandleLow)
        {
            firstCandleHigh = firstCandleLow = secondCandleHigh = secondCandleLow = 0;
            string symbol = TradingSymbol;
            if (TradingSymbol.Contains('.'))
            {
                symbol = TradingSymbol.Split('.')[0];
            }
            try
            {
                int exchange = 1;
                if (TradingSymbol.Split('.')[1] == "NFO")
                {
                    exchange = 2;
                }
                firstCandleHigh = firstCandleLow = secondCandleHigh = secondCandleLow = 0;
                string token = composite.GetToken(TradingSymbol);
                //string CurrentDate = DateTime.Now.AddDays(-2).ToString("yyyy-MM-dd");
                string CurrentDate = DateTime.Now.ToString("yyyy-MM-dd");
                DateTime startDateTime = DateTime.ParseExact(CurrentDate + " " + FirstTickTime + ":00", "yyyy-MM-dd HH:mm:ss", null);
                DateTime endDateTime = DateTime.ParseExact(CurrentDate + " " + SecondTickTime + ":00", "yyyy-MM-dd HH:mm:ss", null);
                string period = GetInterval(interval);//sanika::03-Feb-2021::passed period 
                string historicalAll = composite.GetHistoricalData(token, startDateTime, endDateTime, period, exchange.ToString());
                string[] historicals = historicalAll.Split(',');
                if (historicals.Length > 0 && historicalAll.Length > 1)
                {
                    string[] firstCandleValues = historicals[0].Split('|');
                    string timestamp = (new DateTime(1970, 1, 1)).AddSeconds(Convert.ToDouble(firstCandleValues[0])).ToString("yyyy-MM-dd HH:mm:ss");
                    firstCandleHigh = Convert.ToDouble(firstCandleValues[2]);
                    firstCandleLow = Convert.ToDouble(firstCandleValues[3]);
                    string[] secondCandleValues = historicals[1].Split('|');
                    timestamp = (new DateTime(1970, 1, 1)).AddSeconds(Convert.ToDouble(secondCandleValues[0])).ToString("yyyy-MM-dd HH:mm:ss");
                    secondCandleHigh = Convert.ToDouble(secondCandleValues[2]);
                    secondCandleLow = Convert.ToDouble(secondCandleValues[2]);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(symbol + " " + m_UserID, "GetTwoTickFromHistoricalData : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        public string GetInterval(int period)
        {
            //"In1Minute: 60" "In2Minute : 120" "In3Minute : 180" "In5Minute : 300" "In10Minute : 600" "In15Minute : 900" "In30Minute : 1800" "In60Minute : 3600"
            string interval = "60";
            if (period == 1)
                interval = "60";
            else if (period == 2)
                interval = "120";
            else if (period == 3)
                interval = "180";
            else if (period == 5)
                interval = "300";
            else if (period == 10)
                interval = "600";
            else if (period == 15)
                interval = "900";
            else if (period == 30)
                interval = "1800";
            else if (period == 60)
                interval = "3600";
            return interval;
        }

        public bool FetchInstrumentDetailsFromCSV()
        {
            m_InsertQueries.Clear();
            bool isdbConnected = false;
            bool result = false;
            logger.LogMessage("$$$ Started for inserting data = " + DateTime.Now, MessageType.Informational);
            bool isopen = false;
            SqliteConnectZerodha sqliteConnectZerodha = new SqliteConnectZerodha();
            try
            {
                //20-July-2021: sandip:common name
                isdbConnected = sqliteConnectZerodha.Connect(tokenDBName);
                {
                    //file read from absolute path
                    var path_File = Directory.GetCurrentDirectory();
                    string directoryName = path_File + "\\" + "InstrumentList";

                    if (!Directory.Exists(directoryName))
                        Directory.CreateDirectory(directoryName);

                    string csvFilePath = directoryName + "\\" + insFileName;

                    if (!File.Exists(csvFilePath))
                    {
                        logger.LogMessage("FetchInstrumentDetailsFromCSV : Instruments.csv file does not exist", MessageType.Error);
                    }
                    else
                    {
                        //check table exist or not if not then create
                        if (!m_ListOfTableExistOrNot.Contains("instruments"))
                        {
                            if (isdbConnected)
                            {
                                if (sqliteConnectZerodha.IsTableExists("instruments") == false)
                                {
                                    //sanika::21-Jul-2021::removed price column
                                    string strQuery = "CREATE TABLE instruments (symbol varchar(20) NOT NULL, lotsize int NOT NULL,  tickSize real NOT NULL,InstrumentToken read PRIMARY KEY,exchange varchar(10) NOT NULL)";
                                    sqliteConnectZerodha.ExecuteNonQueryCommand(strQuery);
                                }
                                m_ListOfTableExistOrNot.Add("instruments");
                            }
                        }
                        StreamReader sr = new StreamReader(csvFilePath);
                        int count = 0;
                        string[] value = null;
                        int getSymbolLotSize = 0;
                        decimal tickSize = 0;
                        while (!sr.EndOfStream)
                        {
                            string line = sr.ReadLine();
                            count++;
                            //handled empty line exception
                            if (line != null)
                            {
                                value = line.Split(',');
                                if (value.Length >= 8)
                                {
                                    if (value[5] == "EQ" && objConfigSettings.CheckNSE == true)
                                    {
                                        if (m_ListOfTableExistOrNot.Contains("instruments"))
                                        {
                                            string InstrumentToken = value[1];
                                            //sanika::21-Jul-2021::removed price column
                                            //sanika::10-Jun-2021::Changed query to replace updated values
                                            string txtSQLQuery = "INSERT OR REPLACE into instruments (symbol, lotsize, tickSize,InstrumentToken,exchange) values ('" + value[3] + "','" + value[12] + "','" + value[11] + "','" + InstrumentToken + "','NSE')";
                                            //string txtSQLQuery = "INSERT OR IGNORE into instruments (symbol, lotsize, price, tickSize,InstrumentToken,exchange) values ('" + value[2] + "','" + value[8] + "','" + value[4] + "','" + value[7] + "','" + InstrumentToken + "','" + value[10] + "')";
                                            if (!m_InsertQueries.Contains(txtSQLQuery))
                                                m_InsertQueries.Add(txtSQLQuery);

                                        }
                                    }
                                    if ((value[5] == "FUTSTK" || value[5] == "FUTIDX") && objConfigSettings.CheckNFO == true)
                                    {
                                        string instrumentToken = "";
                                        string futureSymbolList = value[4];
                                        string futureSymbolsTableName = futureSymbolList.Substring(futureSymbolList.Length - 8);
                                        string futureSymbol = value[4];//futureSymbolList.Substring(0, futureSymbolList.Length - futureSymbolsTableName.Length);


                                        getSymbolLotSize = Convert.ToInt32(value[12]);
                                        //sanika::21-Jul-2021::commneted price column
                                        // price = Convert.ToDecimal(value[9]);
                                        tickSize = Convert.ToDecimal(value[11]);
                                        instrumentToken = value[1];

                                        FuturetableCreation(futureSymbolsTableName, futureSymbol, getSymbolLotSize, tickSize, instrumentToken, "NFO", sqliteConnectZerodha);
                                    }
                                    if ((value[5] == "OPTSTK" || value[5] == "OPTIDX") && objConfigSettings.CheckOptions == true)
                                    {
                                        //20-July-2021: sandip:handle paper and live account
                                        int tempcount = 16;//real account
                                        if (composite.root.Contains("developers"))
                                            tempcount = 15;//paper account.
                                        //if (value[15].Length == 19)
                                        if (value[tempcount].Length == 19)
                                        {
                                            string instrumentToken = "";
                                            string optionSymbolName = value[4];
                                            string optSymbolsTableName = "OPTIONS";//value[5];
                                            string optSymbol = value[3];//futureSymbolList.Substring(0, futureSymbolList.Length - futureSymbolsTableName.Length);

                                            //string expiry_date = value[15];//IRDS::Jyoti::06-May-21::Commented for live accounts
                                            string expiry_date = value[tempcount];

                                            getSymbolLotSize = Convert.ToInt32(value[12]);
                                            //sanika::20-July-2021::removed price column
                                            //price = Convert.ToDecimal(value[9]);
                                            tickSize = Convert.ToDecimal(value[11]);
                                            instrumentToken = value[1];
                                            //sanika::20-July-2021::removed price column
                                            optionstableCreation(optSymbolsTableName, optSymbol, optionSymbolName, getSymbolLotSize, tickSize, instrumentToken, expiry_date, sqliteConnectZerodha);
                                        }
                                        else
                                        {
                                            MessageBox.Show("Symphony API Updated! Contact to IRDS Team!");
                                            result = false;
                                            return result;
                                        }
                                    }
                                    //20-July-2021: sandip: insert values after every 2000 count.faster execution
                                    if (isdbConnected && count % 2000 == 0)
                                    {
                                        //Trace.WriteLine("Sandip  InsertDataMultipleValues ");
                                        sqliteConnectZerodha.InsertDataMultipleValues("", m_InsertQueries);
                                        m_InsertQueries.Clear();
                                    }

                                }
                            }
                        }
                        if (isdbConnected)
                            sqliteConnectZerodha.InsertDataMultipleValues("", m_InsertQueries);
                    }
                }
                result = true;
            }
            catch (Exception e)
            {
                result = false;
                Trace.WriteLine(e.Message);
                logger.LogMessage("FetchInstrumentDetailsFromCSV : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            finally
            {
                if (isdbConnected)
                {
                    sqliteConnectZerodha.CloseConnection();
                }
            }
            logger.LogMessage("$$$ End for inserting data = " + DateTime.Now, MessageType.Informational);
            return result;
        }

        public List<string> GetHighLowClose(string TradingSymbol, int interval, int period, string startTime, string endTime)
        {
            int exchange = 1;
            string symbol = TradingSymbol;
            if (TradingSymbol.Contains("."))
            {
                if (TradingSymbol.Split('.')[1] == "NFO")
                {
                    exchange = 2;
                }
                symbol = TradingSymbol.Split('.')[0];
            }
            List<string> list = new List<string>();
            try
            {
                string dateTime = DateTime.Now.ToString("yyyy-MM-dd");
                DateTime todateTime = DateTime.ParseExact(dateTime + " " + endTime + ":00", "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                DateTime startdateTime = DateTime.ParseExact(startTime + ":00", "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                todateTime = todateTime.AddDays(1);
                string intervalPeriod = GetInterval(interval);
                string token = composite.GetToken(TradingSymbol);

                string historicalAll = composite.GetHistoricalData(token, startdateTime, todateTime, intervalPeriod, exchange.ToString());
                if (historicalAll.Length > 1)
                {
                    string[] historicals = historicalAll.Split(',');
                    if (historicals.Length > 0 && historicalAll.Length > 1)
                    {
                        foreach (string data in historicals)
                        {
                            string[] firstCandleValues = data.Split('|');
                            string timestamp = (new DateTime(1970, 1, 1)).AddSeconds(Convert.ToDouble(firstCandleValues[0])).ToString("yyyy-MM-dd HH:mm:ss");
                            string d = firstCandleValues[2] + "," + firstCandleValues[3] + "," + firstCandleValues[4] + "," + timestamp;
                            list.Add(d);
                        }
                    }
                }
                else
                {
                    WriteUniquelogs(symbol + " " + m_UserID, "Historical Data Not fetched", MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(symbol + " " + m_UserID, "GetHighLowClose : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return list;
        }

        public bool IsOrderLimitsValid(OrdersPlaced orderToPlace)
        {
            bool isValid = false;
            //Sanika::1-Feb-2021::Added try catch
            try
            {
                if (m_DictOrdersPlaced.ContainsKey(orderToPlace.TradingSymbol + "_" + orderToPlace.Exchange))
                {
                    int SameOrdersCount = 0;
                    int TotalOrdersPerSymbol = m_DictOrdersPlaced[orderToPlace.TradingSymbol + "_" + orderToPlace.Exchange].Count;
                    int totalOrders = 0;
                    foreach (var list in m_DictOrdersPlaced.Values)
                    {
                        totalOrders = totalOrders + list.Count;
                    }
                    if (totalOrders < m_OverallOrdersLimit)
                    {
                        if (TotalOrdersPerSymbol < m_OrdersPerSymbolLimit)
                        {
                            for (int i = 0; i < TotalOrdersPerSymbol; i++)
                            {
                                OrdersPlaced order = new OrdersPlaced();
                                order = m_DictOrdersPlaced[orderToPlace.TradingSymbol + "_" + orderToPlace.Exchange][i];
                                if (order.Equals(orderToPlace))
                                {
                                    SameOrdersCount = SameOrdersCount + 1;
                                }
                            }
                            if (SameOrdersCount >= m_SameOrdersLimit)
                            {
                                WriteUniquelogs(orderToPlace.TradingSymbol + " " + m_UserID, "m_SameOrdersLimit = " + m_SameOrdersLimit + " Total SameOrdersCount = " + SameOrdersCount, MessageType.Informational);
                                WriteUniquelogs(orderToPlace.TradingSymbol + " " + m_UserID, "m_OrdersPerSymbolLimit = " + m_OrdersPerSymbolLimit + " TotalOrdersPerSymbol = " + TotalOrdersPerSymbol, MessageType.Informational);
                                isValid = false;
                            }
                            else
                            {
                                isValid = true;
                            }
                        }
                        Trace.WriteLine("Limits ended: " + totalOrders);
                    }
                    else
                    {
                        WriteUniquelogs(orderToPlace.TradingSymbol + " " + m_UserID, "Overall Order Limit = " + m_OverallOrdersLimit + " Total Orders Placed = " + totalOrders, MessageType.Informational);
                        isValid = false;
                    }
                }
                else
                {
                    isValid = true;
                }
            }
            catch (Exception e)
            {
                logger.LogMessage("IsOrderLimitsValid : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            //logger.LogMessage("IsOrderLimitsValid : Returns isValid " + isValid, MessageType.Informational);
            return isValid;
        }


        //sanika::20-July-2021::increased the sleep as per suggestion of sir 
        public void UpdateStructures()
        {
            Dictionary<string, dynamic> positionsDataDay = new Dictionary<string, dynamic>();
            DateTime m_TimeToUpdateStructureOrder = DateTime.Now;
            DateTime m_TimeToUpdateStructurePositions = DateTime.Now;
            DateTime m_TimeToUpdateStructureMemory = DateTime.Now;
            while (true && m_StopThread == false)
            {
                DateTime currentTime = DateTime.Now;
                TimeSpan differenceOrder = currentTime - m_TimeToUpdateStructureOrder;
                TimeSpan differencePosition = currentTime - m_TimeToUpdateStructurePositions;
                TimeSpan differenceMemory = currentTime - m_TimeToUpdateStructureMemory;
                try
                {
                    if (differencePosition.TotalMilliseconds > 1001)
                    {
                        var param = new Dictionary<string, dynamic>();
                        Utils.AddIfNotNull(param, "dayOrNet", "DayWise");
                        positionsDataDay.Clear();
                        RequestNUpdatePositioInfo(param, positionsDataDay);
                        m_TimeToUpdateStructurePositions = DateTime.Now;
                    }
                    if (differenceOrder.TotalMilliseconds > 1001)
                    {
                        //WriteUniquelogs("CompositeLogs", "calling get order history orders -- start", MessageType.Informational);
                        //sanika::4-Aug-2021::removed list as not used
                        RequestNUpdateOrderInfo();
                        //WriteUniquelogs("CompositeLogs", "calling get order history orders -- finish", MessageType.Informational);
                        composite.SetOrderUpdateStatus(false);
                        m_TimeToUpdateStructureOrder = DateTime.Now;
                    }
                    if (differenceMemory.TotalMilliseconds > 1001 * 60 * 2)
                    {
                        WriteUniqueMemorylogs("MemoryLogs", "Curent Memory ", MessageType.Informational);
                        m_TimeToUpdateStructureMemory = DateTime.Now;
                    }
                }
                catch (Exception e)
                {
                    logger.LogMessage("UpdateStructures : Exception Error Message = " + e.Message, MessageType.Exception);
                }

                Thread.Sleep(50);
            }
            logger.LogMessage("UpdateStructures : Stopped thread m_StopThread " + m_StopThread, MessageType.Informational);
        }
        //20-July-2021: sandip:optimization of objects,removed name pipe
        //public static void SendResponse()
        //{
        //    //Sanika::2-oct-2020::add seperate class for pipe
        //    PipeConnection pipeConnection = null;
        //    while (!m_StopBridgeThread)
        //    {

        //        if (m_isFromEXEForBridgeConnection == false)
        //        {
        //            Trace.WriteLine("SendResponse : m_isFromEXEForBridgeConnection =" + m_isFromEXEForBridgeConnection);
        //            if (m_LoginStatus)
        //            {
        //                Trace.WriteLine("SendResponse : Going to create client obj");
        //                if (pipeConnection == null)
        //                {
        //                    pipeConnection = new PipeConnection();
        //                    pipeConnection.StartClientMethod();
        //                }
        //            }
        //            else
        //            {
        //                Trace.WriteLine("SendResponse :Login not sucessfull");
        //                WPFMainControl.m_BridgeConnection = "Not Connected";
        //            }
        //        }
        //        //sanika::8-oct-2020::Added sleep to reduced cpu usage 
        //        Thread.Sleep(1000);
        //    }
        //}

        public void CalculateProfitThread()
        {
            //sanika::29-sep-2020::added stop thread condition
            while (true && m_StopThread == false)
            {
                Thread.Sleep(100);
                try
                {
                    CalculateProfitLossForOpenPosition();
                    //CalculateTotalProfitOrLoss();
                    //sanika::9-oct-2020::converted profitLoss into absolute
                    double profitLoss = GetTotalMTM();
                    double overallProfit = m_overallProfit;
                    //sanika::9-oct-2020::converted overall loss into absolute
                    double overallLoss = Math.Abs(m_overallLoss);
                    if (m_isCalculateProfitLoss)
                    {
                        if (profitLoss >= overallProfit)
                        {
                            logger.LogMessage("#### Profit Booked calculateProfit : profitLoss " + profitLoss + " overallProfit " + overallProfit + " overallLoss " + overallLoss + "-- Condition true", MessageType.Informational);
                            CloseAllOpenOrders();
                            logger.LogMessage("calculateProfit : closed all position successfully", MessageType.Informational);
                            WriteGUIlogs(m_ExceptionCounter, DateTime.Now + "Total PNL = " + profitLoss + "Overall profit hitted!!");
                            m_ExceptionCounter++;
                            m_isSqrOff = true;
                            logger.LogMessage("calculateProfit : m_isSqrOff set to true overall profit/loss condition matched", MessageType.Informational);

                        }
                        else if (profitLoss < 0 && Math.Abs(profitLoss) > overallLoss)
                        {
                            logger.LogMessage("#### Loss Booked calculateProfit : profitLoss " + profitLoss + " overallProfit " + overallProfit + " overallLoss " + overallLoss + "-- Condition true", MessageType.Informational);
                            CloseAllOpenOrders();
                            logger.LogMessage("calculateProfit : closed all position successfully", MessageType.Informational);
                            WriteGUIlogs(m_ExceptionCounter, DateTime.Now + "Total PNL = " + profitLoss + " Overall loss hitted!!");
                            m_ExceptionCounter++;
                            m_isSqrOff = true;
                            logger.LogMessage("calculateProfit : m_isSqrOff set to true overall profit/loss condition matched", MessageType.Informational);

                        }
                    }
                    //CheckExistTimeToCloseAllOrders();
                    Thread.Sleep(300);
                }
                catch (Exception e)
                {
                    logger.LogMessage("CalculateProfit : Exception Error Message : " + e.Message, MessageType.Exception);
                }
                UpdatePNLInFile();
                //sanika::8-July-2021::Added to update margin after every 5 sec
                UpdateMargin();
            }
            logger.LogMessage("calculateProfit : Stopped thread m_StopThread " + m_StopThread, MessageType.Informational);
        }
        //20-July-2021: sandip:added user id @ last for each message.
        public void WriteGUIlogs(int Counter, string message)
        {
            try
            {
                SymbolLogs log = new SymbolLogs();
                log.createFile("GUILogs_" + m_UserID);
                log.LogMessage(Counter + "|" + message + " : " + m_UserID, MessageType.Informational);
                log = null;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : " + e.Message);
                Trace.WriteLine("Exception : " + e.Message);
            }
        }

        public bool CloseAllOpenOrders()
        {
            bool isClose = false;
            try
            {
                Dictionary<string, PositionInfoComposite> allPositions = new Dictionary<string, PositionInfoComposite>();
                allPositions = m_GlobalpositionStore.GetAllPositions();
                if (allPositions.Count > 0)
                {
                    foreach (var position in allPositions)
                    {
                        var positionData = position.Value.netPosition;
                        if (positionData.Quantity != 0)
                        {
                            string newOrderBuyOrSell = "";
                            int newOrderQty = 0;
                            string TradingSymbol = positionData.TradingSymbol;
                            string Exchange = positionData.Exchange;
                            string Product = positionData.Product;
                            WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseAllOpenOrders : Found open position", MessageType.Informational);
                            if ((positionData.Quantity > 0 || positionData.Quantity > 0))
                            {
                                newOrderBuyOrSell = Constants.TRANSACTION_TYPE_SELL;
                            }
                            else if ((positionData.Quantity < 0 || positionData.Quantity < 0))
                            {
                                newOrderBuyOrSell = Constants.TRANSACTION_TYPE_BUY;
                            }

                            //IRDS::17-JUL-2020 :: Add comment because day position quantity got wrong :: sandip/sanika

                            //if (Math.Abs(currentPosition.dayPosition.Quantity) > Math.Abs(currentPosition.netPosition.Quantity))
                            //    newOrderQty = Math.Abs(currentPosition.dayPosition.Quantity);
                            //else

                            newOrderQty = Math.Abs(positionData.Quantity);
                            if (newOrderQty != 0)
                            {
                                WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseAllOpenOrders :newOrderQty is not zero" + newOrderQty, MessageType.Informational);
                                string orderID = PlaceOrder(Exchange, TradingSymbol, newOrderBuyOrSell, newOrderQty, Constants.ORDER_TYPE_MARKET, Product: Product);
                                if (orderID != "NA")
                                {
                                    double openPrice = GetOpenPostionPricebyOrderID(TradingSymbol, Exchange, orderID);
                                    WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseAllOpenOrders : Order Complete at price = " + openPrice, MessageType.Informational);
                                    WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseAllOpenOrders : order closed successfully", MessageType.Informational);
                                    //m_ExceptionDictionary.Add(m_ExceptionCounter, DateTime.Now + " " +  TradingSymbol + " Order Closed successfully!! ");
                                    WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + TradingSymbol + " Order Closed successfully!! ");
                                    m_ExceptionCounter++;

                                    //sanika::16-oct-2020::add information after placing order
                                    DateTime dateTime = DateTime.Now;
                                    m_LocalOrderStructureInfo.AddOrUpdateOrderInformation(TradingSymbol: TradingSymbol, Exchange: Exchange, ClosedTransactionType: newOrderBuyOrSell, Product: Product, ClosedQuantity: newOrderQty, ClosedOrderDateTime: dateTime, ClosedOrderId: orderID);
                                    isClose = true;
                                }
                                else
                                {
                                    WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseAllOpenOrders : order not closed", MessageType.Informational);
                                    //m_ExceptionDictionary.Add(m_ExceptionCounter, DateTime.Now + " " + TradingSymbol + " Order not closed!! ");
                                    WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + TradingSymbol + " Order not closed ");
                                }
                            }
                        }
                        else
                        {
                            isClose = true;
                        }
                    }
                }
                else
                {
                    logger.LogMessage("CloseAllOpenOrders : allPositions count found 0", MessageType.Informational);
                    isClose = true;
                }
            }
            catch (Exception e)
            {
                isClose = false;
                logger.LogMessage("CloseAllOpenOrders : Exception Error Message = " + e.Message, MessageType.Informational);
            }
            return isClose;
        }

        public List<string> GetAllSymbolsFromDBdata()
        {
            List<string> SymbolList = new List<string>();

            try
            {
                //20-July-2021: sandip:optimization of objects
                if (OpenConnection() == true && m_AllSymbolsList.Count == 0)
                {
                    if (composite == null)
                    {
                        composite = new Composite(objConfigSettings);
                    }
                    SymbolList = composite.fetchTablenamesForsearchboxList();
                    foreach (var item in SymbolList)
                    {
                        Console.WriteLine(item.ToString());
                        string tablenameforFetching = item.ToString();
                        try
                        {
                            string sql_select = "select symbol from '" + tablenameforFetching + "'";
                            sql_cmd = new SQLiteCommand(sql_select, sql_con);
                            sql_read = sql_cmd.ExecuteReader();

                            if (sql_read != null && sql_read.HasRows)
                            {
                                while (sql_read.Read())
                                {
                                    //Symbol
                                    getFutSymbol = (string)sql_read["symbol"];

                                    if (!m_AllSymbolsList.Contains(getFutSymbol))
                                    {
                                        m_AllSymbolsList.Add(getFutSymbol);
                                    }
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            logger.LogMessage("GetAllSymbolsFromDBdata : Exception Error Message = " + e.Message, MessageType.Exception);

                        }
                    }
                }
                else if (m_AllSymbolsList.Count > 0)
                {
                    logger.LogMessage("GetAllSymbolsFromDBdata : Symbol list already loaded", MessageType.Informational);
                }
                else
                {
                    logger.LogMessage("GetAllSymbolsFromDBdata : Table not exist", MessageType.Informational);
                }
            }
            catch (Exception ex)
            {
                logger.LogMessage("GetAllSymbolsFromDBdata : Exception Error Message = " + ex.Message, MessageType.Exception);
                //if (sql_con != null)
                //    sql_con.Close();
            }
            return m_AllSymbolsList;
        }


        // public List<string> getSymbolsFromOptions()
        public void getSymbolsFromOptions()
        {
            List<string> OptionSymbolList = new List<string>();
            m_OptionSymbols.Clear();
            List<string> SymbolList = new List<string>();
            if (OpenConnection() == true)
            {
                if (composite == null)
                {
                    composite = new Composite(objConfigSettings);
                }
                Trace.WriteLine("Symbol : " + "Connection to db");
                SymbolList.Add("OPTIONS");
                foreach (var item in SymbolList)
                {
                    Trace.WriteLine("Symbol : " + item);
                    Console.WriteLine(item.ToString());
                    string tablenameforFetching = item.ToString();
                    try
                    {
                        if (tablenameforFetching == "OPTIONS")
                        {
                            string sql_select = "select DISTINCT symbol from '" + tablenameforFetching + "' ORDER BY symbol ASC;";

                            Trace.WriteLine("Symbol : " + sql_select);
                            sql_cmd = new SQLiteCommand(sql_select, sql_con);
                            sql_read = sql_cmd.ExecuteReader();

                            if (sql_read != null && sql_read.HasRows)
                            {
                                Trace.WriteLine("Symbol : " + "Defore While");
                                while (sql_read.Read())
                                {
                                    //Symbol
                                    getFutSymbol = (string)sql_read["symbol"];

                                    if (!OptionSymbolList.Contains(getFutSymbol))
                                    {
                                        Trace.WriteLine("Symbol : " + getFutSymbol);
                                        OptionSymbolList.Add(getFutSymbol);
                                    }
                                }
                                m_OptionSymbols = OptionSymbolList;
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        logger.LogMessage("getSymbolsFromOptions : Exception Error Message = " + ex.Message, MessageType.Exception);
                    }
                }
            }
        }

        //Pratiksha::26-04-2021::get exchange from db 
        public List<string> GetSymbolexpiryDateFromDBdata(string symbol, string table)
        {
            List<string> expirydateList = new List<string>();
            string expirydate;
            try
            {
                if (OpenConnection() == true)
                {
                    try
                    {
                        //IRDSPM::PRatiksha::04-06-2021::Change the query
                        string sql_select = "select DISTINCT expiryDate from '" + table + "' where symbol = '" + symbol + "' AND expiryDate >= date()" + "  ORDER BY expiryDate ASC;";
                        sql_cmd = new SQLiteCommand(sql_select, sql_con);
                        sql_read = sql_cmd.ExecuteReader();

                        if (sql_read != null && sql_read.HasRows)
                        {
                            while (sql_read.Read())
                            {
                                expirydate = (string)sql_read["expiryDate"];
                                string Date = expirydate.Replace("T", " ");
                                string FullDate = Convert.ToDateTime(Date).ToString("dd-MMM-yy").Replace("-", string.Empty).ToUpper();
                                //string FinalDate = FullDate.Replace('-', '');
                                if (!expirydateList.Contains(FullDate))
                                {
                                    expirydateList.Add(FullDate);
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        logger.LogMessage("GetSymbolExchangeFromDBdata : Exception Error Message = " + e.Message, MessageType.Exception);
                    }
                }
                else
                {
                    logger.LogMessage("GetSymbolExchangeFromDBdata : Table not exist", MessageType.Informational);
                }
            }
            catch (Exception ex)
            {
                logger.LogMessage("GetAllSymbolsFromDBdata : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
            return expirydateList;
        }
        //IRDSPM::Pratiksha::07-06-2021::fetching lotsize from db
        public string GetLotsizeexpiryDateFromDBdata(string symbol, string table, string expiryDt)
        {
            string quantity = "0";
            try
            {
                if (OpenConnection() == true)
                {
                    try
                    {
                        //IRDSPM::PRatiksha::04-06-2021::Change the query
                        string sql_select = "select DISTINCT lotsize from '" + table + "' where symbol = '" + symbol + "' AND expiryDate = '" + expiryDt + "';";
                        sql_cmd = new SQLiteCommand(sql_select, sql_con);
                        sql_read = sql_cmd.ExecuteReader();

                        if (sql_read != null && sql_read.HasRows)
                        {
                            while (sql_read.Read())
                            {
                                quantity = Convert.ToString(sql_read["lotsize"]);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        logger.LogMessage("GetLotsizeexpiryDateFromDBdata : Exception Error Message = " + e.Message, MessageType.Exception);
                    }
                }
                else
                {
                    logger.LogMessage("GetLotsizeexpiryDateFromDBdata : Table not exist", MessageType.Informational);
                }
            }
            catch (Exception ex)
            {
                logger.LogMessage("GetLotsizeexpiryDateFromDBdata : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
            return quantity;
        }


        //IRDSPM::Pratiksha::14-09-2020::for returning list to buyForm
        public List<string> ReturnAllSymbolsFromDBdata()
        {
            //sanika::24-sep-2020::As sandip sir's suggestion 
            return m_AllSymbolsList;
        }

        public bool CheckTimeToExistAllCDSOrder()
        {
            try
            {
                if (m_TimeToExitAllCDSOrder != null)
                {
                    DateTime currentDateTime = DateTime.Now;
                    if ((currentDateTime.ToOADate() >= m_TimeToExitAllCDSOrder.ToOADate()))
                    {
                        logger.LogMessage("CheckTimeToExistAllCDSOrder: m_TimeToExitAllCDSOrder : " + m_TimeToExitAllCDSOrder.ToString() + " currentDateTime " + currentDateTime.ToString(), MessageType.Informational);
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception e)
            {
                logger.LogMessage("CheckTimeToExistAllCDSOrder: Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return false;
        }
        public bool CheckTimeToExistAllMCXOrder()
        {
            try
            {
                if (m_TimeToExitAllMCXOrder != null)
                {
                    DateTime currentDateTime = DateTime.Now;
                    if ((currentDateTime.ToOADate() >= m_TimeToExitAllMCXOrder.ToOADate()))
                    {
                        logger.LogMessage("CheckTimeToExistAllMCXOrder: m_TimeToExitAllMCXOrder : " + m_TimeToExitAllMCXOrder.ToString() + " currentDateTime " + currentDateTime.ToString(), MessageType.Informational);
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception e)
            {
                logger.LogMessage("CheckTimeToExistAllMCXOrder: Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return false;
        }
        public bool CheckTimeToExistAllNSEOrder()
        {
            try
            {
                if (m_TimeToExitAllNSEOrder != null)
                {
                    DateTime currentDateTime = DateTime.Now;
                    if ((currentDateTime.ToOADate() >= m_TimeToExitAllNSEOrder.ToOADate()))
                    {
                        logger.LogMessage("CheckTimeToExistAllNSEOrder: m_TimeToExitAllNSEOrder : " + m_TimeToExitAllNSEOrder.ToString() + " currentDateTime " + currentDateTime.ToString(), MessageType.Informational);
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception e)
            {
                logger.LogMessage("CheckTimeToExistAllNSEOrder: Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return false;
        }
        public bool CheckTimeToExistAllNFOOrder()
        {
            try
            {
                if (m_TimeToExitAllNFOOrder != null)
                {
                    DateTime currentDateTime = DateTime.Now;
                    if ((currentDateTime.ToOADate() >= m_TimeToExitAllNFOOrder.ToOADate()))
                    {
                        logger.LogMessage("CheckTimeToExistAllNFOOrder: m_TimeToExitAllNFOOrder : " + m_TimeToExitAllNFOOrder.ToString() + " currentDateTime " + currentDateTime.ToString(), MessageType.Informational);
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception e)
            {
                logger.LogMessage("CheckTimeToExistAllNFOOrder: Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return false;
        }

        public void CheckExistTimeToCloseAllOrders()
        {
            //sanika::9-oct-2020::move time code from m_isCalculateProfitLoss flag condition
            //close all orders for particular time
            if (CheckTimeToExistAllNSEOrder() && m_isCloseAllNSEOrder == false) //sanika::15-oct-2020::add flag condition to ignore toast many times 
            {
                logger.LogMessage("CheckToCloseOrders : Closed all orders bacause exit time ", MessageType.Informational);
                //sanika::26-Nov-2020::Call cancel order function to cancel pending order
                CancelAllPendingOrders(Constants.EXCHANGE_NSE);
                logger.LogMessage("CheckToCloseOrders : cancel all pending orders", MessageType.Informational);
                if (CloseAllOpenMISOrders(Constants.EXCHANGE_NSE)) //sanika::12-Nov-2020::Added close mis order seperate function for ajay sir
                {
                    m_isCloseAllNSEOrder = true;
                    m_isSqrOffNSE = true;
                    logger.LogMessage("CheckToCloseOrders : m_isSqrOff set to true exit time condition matched for NSE Symbols", MessageType.Informational);
                    WriteGUIlogs(m_ExceptionCounter, DateTime.Now + "Exist time condition executed for NSE Symbols!!");
                    m_ExceptionCounter++;
                }
                else
                {
                    logger.LogMessage("CheckToCloseOrders : Not able to close all orders!!", MessageType.Informational);
                }
            }

            if (CheckTimeToExistAllNFOOrder() && m_isCloseAllNFOOrder == false) //sanika::15-oct-2020::add flag condition to ignore toast many times 
            {
                logger.LogMessage("CheckToCloseOrders : Closed all orders bacause exit time ", MessageType.Informational);
                //sanika::26-Nov-2020::Call cancel order function to cancel pending order
                CancelAllPendingOrders(Constants.EXCHANGE_NFO);
                logger.LogMessage("CheckToCloseOrders : cancel all pending orders", MessageType.Informational);
                if (CloseAllOpenMISOrders(Constants.EXCHANGE_NFO)) //sanika::12-Nov-2020::Added close mis order seperate function for ajay sir
                {
                    m_isCloseAllNFOOrder = true;
                    m_isSqrOffNFO = true;
                    logger.LogMessage("CheckToCloseOrders : m_isSqrOff set to true exit time condition matched for NFO symbols", MessageType.Informational);
                    WriteGUIlogs(m_ExceptionCounter, DateTime.Now + "Exist time condition executed for NFO symbols!!");
                    m_ExceptionCounter++;
                }
                else
                {
                    logger.LogMessage("CheckToCloseOrders : Not able to close all orders!!", MessageType.Informational);
                }
            }

            if (CheckTimeToExistAllMCXOrder() && m_isCloseAllMCXOrder == false) //sanika::15-oct-2020::add flag condition to ignore toast many times 
            {
                logger.LogMessage("CheckToCloseOrders : Closed all orders bacause exit time ", MessageType.Informational);
                //sanika::26-Nov-2020::Call cancel order function to cancel pending order
                CancelAllPendingOrders(Constants.EXCHANGE_MCX);
                logger.LogMessage("CheckToCloseOrders : cancel all pending orders", MessageType.Informational);
                if (CloseAllOpenMISOrders(Constants.EXCHANGE_MCX)) //sanika::12-Nov-2020::Added close mis order seperate function for ajay sir
                {
                    m_isCloseAllMCXOrder = true;
                    m_isSqrOffMCX = true;
                    logger.LogMessage("CheckToCloseOrders : m_isSqrOff set to true exit time condition matched for MCX Symbols", MessageType.Informational);
                    WriteGUIlogs(m_ExceptionCounter, DateTime.Now + "Exist time condition executed for MCX Symbols!!");
                    m_ExceptionCounter++;
                }
                else
                {
                    logger.LogMessage("CheckToCloseOrders : Not able to close all orders!!", MessageType.Informational);
                }
            }

            if (CheckTimeToExistAllCDSOrder() && m_isCloseAllCDSOrder == false) //sanika::15-oct-2020::add flag condition to ignore toast many times 
            {
                logger.LogMessage("CheckToCloseOrders : Closed all orders bacause exit time ", MessageType.Informational);
                //sanika::26-Nov-2020::Call cancel order function to cancel pending order
                CancelAllPendingOrders(Constants.EXCHANGE_CDS);
                logger.LogMessage("CheckToCloseOrders : cancel all pending orders", MessageType.Informational);
                if (CloseAllOpenMISOrders(Constants.EXCHANGE_CDS)) //sanika::12-Nov-2020::Added close mis order seperate function for ajay sir
                {
                    m_isCloseAllCDSOrder = true;
                    m_isSqrOffCDS = true;
                    logger.LogMessage("CheckToCloseOrders : m_isSqrOff set to true exit time condition matched for CDS symbols", MessageType.Informational);
                    WriteGUIlogs(m_ExceptionCounter, DateTime.Now + "Exist time condition executed for CDS Symbols!!");
                    m_ExceptionCounter++;
                }
                else
                {
                    logger.LogMessage("CheckToCloseOrders : Not able to close all orders!!", MessageType.Informational);
                }
            }
        }

        public bool CloseAllOpenMISOrders(string exchange = "")//sanika::3-dec-2020::Added optional variable as exchange and added condition for that
        {
            bool isClose = false;
            try
            {
                Dictionary<string, PositionInfoComposite> allPositions = new Dictionary<string, PositionInfoComposite>();
                allPositions = m_GlobalpositionStore.GetAllPositions();
                if (allPositions.Count > 0)
                {
                    foreach (var position in allPositions)
                    {
                        var positionData = position.Value.netPosition;
                        if ((positionData.Quantity != 0 && positionData.Product != Constants.PRODUCT_NRML) && (exchange == "" || exchange == positionData.Exchange))
                        {
                            string newOrderBuyOrSell = "";
                            int newOrderQty = 0;
                            string TradingSymbol = positionData.TradingSymbol;
                            string Exchange = positionData.Exchange;
                            string Product = positionData.Product;
                            WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseAllOpenOrders : Found open position", MessageType.Informational);
                            if ((positionData.Quantity > 0 || positionData.Quantity > 0))
                            {
                                newOrderBuyOrSell = Constants.TRANSACTION_TYPE_SELL;
                            }
                            else if ((positionData.Quantity < 0 || positionData.Quantity < 0))
                            {
                                newOrderBuyOrSell = Constants.TRANSACTION_TYPE_BUY;
                            }

                            //IRDS::17-JUL-2020 :: Add comment because day position quantity got wrong :: sandip/sanika

                            //if (Math.Abs(currentPosition.dayPosition.Quantity) > Math.Abs(currentPosition.netPosition.Quantity))
                            //    newOrderQty = Math.Abs(currentPosition.dayPosition.Quantity);
                            //else

                            newOrderQty = Math.Abs(positionData.Quantity);
                            if (newOrderQty != 0)
                            {
                                WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseAllOpenOrders :newOrderQty is not zero" + newOrderQty, MessageType.Informational);
                                string orderID = PlaceOrder(Exchange, TradingSymbol, newOrderBuyOrSell, newOrderQty, Constants.ORDER_TYPE_MARKET, Product: Product);
                                if (orderID != "NA")
                                {
                                    double openPrice = GetOpenPostionPricebyOrderID(TradingSymbol, Exchange, orderID);
                                    WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseAllOpenOrders : Order Complete at price = " + openPrice, MessageType.Informational);
                                    WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseAllOpenOrders : order closed successfully", MessageType.Informational);
                                    //m_ExceptionDictionary.Add(m_ExceptionCounter, DateTime.Now + " " +  TradingSymbol + " Order Closed successfully!! ");
                                    WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + TradingSymbol + " Order Closed successfully!! ");
                                    m_ExceptionCounter++;

                                    //sanika::16-oct-2020::add information after placing order
                                    DateTime dateTime = DateTime.Now;
                                    m_LocalOrderStructureInfo.AddOrUpdateOrderInformation(TradingSymbol: TradingSymbol, Exchange: Exchange, ClosedTransactionType: newOrderBuyOrSell, Product: Product, ClosedQuantity: newOrderQty, ClosedOrderDateTime: dateTime, ClosedOrderId: orderID);
                                    isClose = true;
                                }
                                else
                                {
                                    WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseAllOpenOrders : order not closed", MessageType.Informational);
                                    //m_ExceptionDictionary.Add(m_ExceptionCounter, DateTime.Now + " " + TradingSymbol + " Order not closed!! ");
                                    WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + TradingSymbol + " Order not closed ");
                                }
                            }
                        }
                        else
                        {
                            isClose = true;
                        }
                    }
                }
                else
                {
                    logger.LogMessage("CloseAllOpenOrders : allPositions count found 0", MessageType.Informational);
                    isClose = true;
                }
            }
            catch (Exception e)
            {
                isClose = false;
                logger.LogMessage("CloseAllOpenOrders : Exception Error Message = " + e.Message, MessageType.Informational);
            }
            return isClose;
        }

        //sanika::3-dec-2020::Added optional variable as exchange and added condition for that
        public bool CancelAllPendingOrders(string Exchange = "")
        {
            bool isCancel = false;

            try
            {
                List<OrderComposite> allOrders = new List<OrderComposite>();
                allOrders = m_GlobalOrderStore.GetAllOrder();
                if (allOrders.Count > 0)
                {
                    foreach (var order in allOrders)
                    {
                        if ((order.Status == Constants.ORDER_STATUS_PENDING || order.Status == Constants.ORDER_STATUS_OPEN) && (Exchange == "" || Exchange == order.Exchange))
                        {
                            string orderId = order.OrderId;
                            string product = order.Product.ToLower();
                            if (product != "bo" && product != "co")
                            {
                                product = Constants.VARIETY_REGULAR;
                            }
                            var response = CancelOrderEX(orderId, product);
                            if (response != null)
                            {
                                isCancel = true;
                                WriteUniquelogs(order.Tradingsymbol + " " + m_UserID, "CancelAllPendingOrders : Return true", MessageType.Informational);
                                // m_ExceptionDictionary.Add(m_ExceptionCounter, DateTime.Now + " " + "CancelAllPendingOrders - " + order.Tradingsymbol + " Cancel Order successfully!!");
                                WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + order.Tradingsymbol + " Cancel Order successfully!!");
                                m_ExceptionCounter++;
                            }
                        }
                        else
                        {
                            isCancel = true;
                        }
                    }
                }
                else
                {
                    logger.LogMessage("CancelAllPendingOrders : allOrders count found as 0", MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                isCancel = false;
                logger.LogMessage("CancelAllPendingOrders : Exception Error Message = " + e.Message, MessageType.Exception);
            }

            return isCancel;
        }

        public void CalculateProfitLossForOpenPosition()
        {
            double pnl = 0;
            try
            {
                Trace.WriteLine("*****Calculate profit/loss started");
                Dictionary<string, PositionInfoComposite> allPositions = new Dictionary<string, PositionInfoComposite>();
                allPositions = m_GlobalpositionStore.GetAllPositions();
                if (allPositions.Count > 0)
                {
                    foreach (var p in allPositions)
                    {
                        var net = p.Value.netPosition;

                        if (net.Exchange == Constants.EXCHANGE_NFO && !net.TradingSymbol.Contains(m_futureName) && (!(net.TradingSymbol.EndsWith("PE") || net.TradingSymbol.EndsWith("CE")) && net.TradingSymbol.Length > 9))
                        {
                            net.TradingSymbol = net.TradingSymbol + m_futureName;
                        }
                        string ltp = GetLastTradedPrice(net.TradingSymbol);
                        double sellValue = Convert.ToDouble(net.SellValue);
                        double buyValue = Convert.ToDouble(net.BuyValue);
                        //sanika::23-Jul-2021::Changed condition because calculation calculated wrong with avg price 
                        if (ltp != "")
                        {
                            //ltp = net.AveragePrice.ToString();

                            double openValue = Math.Abs(Convert.ToDouble(ltp) * net.Quantity);
                            //WriteUniquelogs(net.TradingSymbol + " " + m_UserID, "ltp = " + ltp + " sellValue = " + sellValue + " buyValue = " + buyValue + " openValue = " + openValue,MessageType.Informational);
                            if (net.Quantity > 0)
                            {
                                sellValue += openValue;
                                double mtm = sellValue - buyValue;
                                //WriteUniquelogs(net.TradingSymbol + " " + m_UserID, "mtm = " + mtm, MessageType.Informational);
                                //Trace.WriteLine(net.TradingSymbol + " " + mtm);
                                pnl += mtm;
                                pnl = Math.Round(pnl, 2);
                                if (m_IndividualProfitLoss.ContainsKey(net.TradingSymbol + "_" + net.Exchange))
                                {
                                    m_IndividualProfitLoss[net.TradingSymbol + "_" + net.Exchange] = mtm;
                                }
                                else
                                {
                                    m_IndividualProfitLoss.Add(net.TradingSymbol + "_" + net.Exchange, mtm);
                                }
                            }
                            else if (net.Quantity < 0)
                            {
                                buyValue += openValue;
                                double mtm = sellValue - buyValue;
                                //WriteUniquelogs(net.TradingSymbol + " " + m_UserID, "mtm = " + mtm, MessageType.Informational);
                                // Trace.WriteLine(net.TradingSymbol + " " + mtm);
                                pnl += mtm;
                                pnl = Math.Round(pnl, 2);
                                if (m_IndividualProfitLoss.ContainsKey(net.TradingSymbol + "_" + net.Exchange))
                                {
                                    m_IndividualProfitLoss[net.TradingSymbol + "_" + net.Exchange] = mtm;
                                }
                                else
                                {
                                    m_IndividualProfitLoss.Add(net.TradingSymbol + "_" + net.Exchange, mtm);
                                }
                            }
                            else
                            {
                                double mtm = Convert.ToDouble(net.PNL);
                                double mtmTest = sellValue - buyValue;
                                //WriteUniquelogs(net.TradingSymbol + " " + m_UserID, "mtm = " + mtm + " mtmTest = "+ mtmTest, MessageType.Informational);
                                //Trace.WriteLine(net.TradingSymbol + " " + mtm);
                                pnl += mtm;
                                pnl = Math.Round(pnl, 2);
                                if (m_IndividualProfitLoss.ContainsKey(net.TradingSymbol + "_" + net.Exchange))
                                {
                                    m_IndividualProfitLoss[net.TradingSymbol + "_" + net.Exchange] = mtm;
                                }
                                else
                                {
                                    m_IndividualProfitLoss.Add(net.TradingSymbol + "_" + net.Exchange, mtm);
                                }
                            }
                        }

                    }
                }
                m_TotalProfitLoss = pnl;
                Trace.WriteLine("*****Calculate profit/loss Stopped");
                //logger.LogMessage("CalculateProfitLossForOpenPosition : m_TotalProfitLoss = " + m_TotalProfitLoss, MessageType.Informational);
            }
            catch (Exception e)
            {
                logger.LogMessage("CalculateProfitLossForOpenPosition : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }
        public string GetLastTradedPrice(string TradingSymbol)
        {
            string ltp = "";
            try
            {
                if (m_TickData != null)
                {
                    decimal lastPrice = m_TickData.GetLastPrice(TradingSymbol);
                    if (lastPrice == 0)
                        ltp = "";
                    else
                    {
                        ltp = lastPrice.ToString();
                    }
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetLastTradedPrice : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return ltp;
        }

        public double GetTotalMTM()
        {
            return m_TotalProfitLoss;
        }

        public void loadIsFromEXEValue(bool isFromEXE)
        {
            Trace.WriteLine("m_isFromEXEForBridgeConnection " + m_isFromEXEForBridgeConnection);
            m_isFromEXEForshowGUI = isFromEXE;
            m_isFromEXEForBridgeConnection = isFromEXE;
            Trace.WriteLine("m_isFromEXEForBridgeConnection " + m_isFromEXEForBridgeConnection);
        }

        public bool UpdateDateinDB(string ClientId)
        {
            bool bolUpdateInfo = false;
            double dCurrentDate;
            DateTime dtCurrentDate;
            try
            {
                if (m_MySqlConnection == null)
                {
                    m_MySqlConnection = new MySQLConnectZerodha();
                    m_isMysqlconnected = m_MySqlConnection.Connect(logger, m_ServerUrl, m_username, m_password, m_Dbname);
                }

                if (m_isMysqlconnected)
                {
                    dtCurrentDate = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));
                    dCurrentDate = dtCurrentDate.ToOADate();
                    string updatecmd = "update UserDetails set lastlogindate = " + dCurrentDate + " where ClientID = '" + ClientId + "'";
                    m_MySqlConnection.ExecuteNonQueryCommand(updatecmd);
                    bolUpdateInfo = true;
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("RegistrationProcess", "UpdateDateinDB :  Exception Error Message = " + ex.Message, MessageType.Exception);
            }
            return bolUpdateInfo;
        }

        public bool GetTickDataDownloaderFlag()
        {
            return objConfigSettings.isTickMCXDataDownloader;
        }
        public string GetBidPrice(string TradingSymbol)
        {
            string bidPrice = null;
            try
            {
                if (m_TickData != null)
                {
                    bidPrice = m_TickData.GetBidPrice(TradingSymbol).ToString();
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetBidPrice : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return bidPrice;
        }

        public string GetOpenPrice(string TradingSymbol)
        {
            string openPrice = null;
            try
            {
                if (m_TickData != null)
                {
                    openPrice = m_TickData.GetOpenPrice(TradingSymbol).ToString();
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPrice : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return openPrice;
        }

        public string GetLowPrice(string TradingSymbol)
        {
            string lowPrice = null;
            try
            {
                if (m_TickData != null)
                {
                    lowPrice = m_TickData.GetLowPrice(TradingSymbol).ToString();
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetLowPrice : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return lowPrice;
        }

        public string GetHighPrice(string TradingSymbol)
        {
            string highPrice = null;
            try
            {
                if (m_TickData != null)
                {
                    highPrice = m_TickData.GetHighPrice(TradingSymbol).ToString();
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetHighPrice : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return highPrice;
        }

        public bool GetChangeInPercent(string TradingSymbol, string tickTime, out double highPercent, out double lowPercent)
        {
            highPercent = 0;
            lowPercent = 0;
            try
            {
                composite.GetChangeInPercent(TradingSymbol, tickTime, out highPercent, out lowPercent);
                return true;
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetChangeInPercent : Exception Error Message " + e.Message, MessageType.Exception);
            }
            return false;
        }

        public string GetChangeInPercent(string TradingSymbol)
        {
            string changeInPercent = null;
            try
            {
                if (m_TickData != null)
                {
                    changeInPercent = m_TickData.GetChangeInPercentage(TradingSymbol).ToString();
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetChangeInPercent : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return changeInPercent;
        }


        //sanika::08-oct-2020::Added to get absolute change
        public string GetChangeInAbsolute(string TradingSymbol)
        {
            string changeInAbsolute = null;
            try
            {
                if (m_TickData != null)
                {
                    changeInAbsolute = m_TickData.GetChangeInAbsolute(TradingSymbol).ToString();
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetChangeInAbsolute : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return changeInAbsolute;
        }

        public string GetAskPrice(string TradingSymbol)
        {
            string askPrice = null;
            try
            {
                if (m_TickData != null)
                {
                    askPrice = m_TickData.GetAskPrice(TradingSymbol).ToString();
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetAskPrice : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return askPrice;
        }

        public string GetVolumePrice(string TradingSymbol)
        {
            string volume = null;
            try
            {
                if (m_TickData != null)
                {
                    volume = m_TickData.GetVolume(TradingSymbol).ToString();
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetVolumePrice : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return volume;
        }

        public string GetClosePrice(string TradingSymbol)
        {
            string closePrice = "";
            try
            {
                if (m_TickData != null)
                {
                    closePrice = m_TickData.GetClosePrice(TradingSymbol).ToString();
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetClosePrice : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return closePrice;
        }
        public Dictionary<string, PositionInfoComposite> GetAllPosition()
        {
            //Sanika::11-sep-2020::Remove out variable as per latest design
            //Allorders = new List<Position>();            
            Dictionary<string, PositionInfoComposite> AllPositions = new Dictionary<string, PositionInfoComposite>();
            try
            {
                AllPositions = m_GlobalpositionStore.GetAllPositions();
            }
            catch (Exception e)
            {
                logger.LogMessage("GetAllPosition : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return AllPositions;
        }

        public List<OrderComposite> GetAllOrders()
        {
            //Sanika::11-sep-2020::Remove out variable as per latest design
            List<OrderComposite> Allorders1 = new List<OrderComposite>();
            List<OrderComposite> Allorders = new List<OrderComposite>();
            try
            {
                Allorders = m_GlobalOrderStore.GetAllOrder();
                //Sanika::11-sep-2020::order by orderis instead of timestamp
                Allorders1 = Allorders.OrderByDescending(x => x.OrderId).ToList();
            }
            catch (Exception e)
            {
                logger.LogMessage("GetAllOrders : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return Allorders1;
        }

        public bool GetAllHoldings(out dynamic AllHoldings1)
        {
            Dictionary<string, HoldingSamco> AllHoldings = new Dictionary<string, HoldingSamco>();
            bool res = false;
            if (m_GlobalholdingStore.GetAllHoldings(out AllHoldings))
            {
                res = true;
            }
            AllHoldings1 = AllHoldings;
            return res;
        }
        //sanika::13-July-2021::add counter to konow the count of margin api call
        static long m_ICounterGetMargin = 0;
        public dynamic GetMargin()
        {
            m_ICounterGetMargin++;
            WriteUniquelogs("StartGUI", "GetMargin : In GetMargin function m_ICounterGetMargin = " + m_ICounterGetMargin.ToString(), MessageType.Informational);
            //IRDS::Jyoti::08-09-2020::Samco Integration changes
            var respDict = new Dictionary<string, dynamic>();
            try
            {
                //20-July-2021: sandip:added 1 sec delay for safer side
                Thread.Sleep(1000);
                UserMarginComposite margins = composite.GetMargins(Constants.MARGIN_EQUITY);
                //m_TotalProfitLoss = Math.Round(Convert.ToDouble(margins.MTM), 2);                
                respDict.Add("Utilised", margins.MarginUtilized);
                respDict.Add("Available", margins.NetMarginAvailable);
                respDict.Add("Net", margins.Net);
                WriteUniquelogs("StartGUI", "GetMargin : margins.MarginUtilized " + margins.MarginUtilized + "  margins.NetMarginAvailable " + margins.NetMarginAvailable + " margins.Net " + margins.Net, MessageType.Exception);
            }
            catch (Exception e)
            {
                WriteUniquelogs("StartGUI", "GetMargin : Exception Error Message " + e.Message, MessageType.Exception);
            }

            return respDict;

        }


        //sanika::1-Feb-2021::Added missing functions 
        public bool AddManuallyOrderQuantity(string TradingSymbol, int Quantity)
        {
            bool isSymbolSet = false;
            string symbol = "";
            if (TradingSymbol.Contains("_"))
                symbol = TradingSymbol.Split('_')[1];
            else
                symbol = TradingSymbol;
            try
            {
                if (m_OrderListWithSymbolAndQuantity.ContainsKey(symbol))
                {
                    m_OrderListWithSymbolAndQuantity[symbol] += Quantity;
                }
                else
                {
                    m_OrderListWithSymbolAndQuantity.Add(symbol, Quantity);
                }
                WriteUniquelogs(symbol + " " + m_UserID, "AddManuallyOrderQuantity : Set symbol  " + TradingSymbol + " with quantity " + Quantity, MessageType.Informational);
            }
            catch (Exception e)
            {
                WriteUniquelogs(symbol + " " + m_UserID, "AddManuallyOrderQuantity : Exception Error Message " + e.Message, MessageType.Exception);
            }
            return isSymbolSet;
        }

        public bool RemoveManuallyOrderQuantity(string TradingSymbol)
        {
            bool isSymbolRemove = false;
            string symbol = "";
            if (TradingSymbol.Contains("_"))
                symbol = TradingSymbol.Split('_')[1];
            else
                symbol = TradingSymbol;
            try
            {
                if (m_OrderListWithSymbolAndQuantity.ContainsKey(symbol))
                {
                    m_OrderListWithSymbolAndQuantity.Remove(symbol);
                    WriteUniquelogs(symbol + " " + m_UserID, "RemoveManuallyOrderQuantity : Removed symbol from list " + TradingSymbol, MessageType.Informational);
                }
                else
                {
                    WriteUniquelogs(symbol + " " + m_UserID, "RemoveManuallyOrderQuantity :List not contains symbol " + TradingSymbol, MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(symbol + " " + m_UserID, "RemoveManuallyOrderQuantity : Exception Error Message " + e.Message, MessageType.Exception);
            }
            return isSymbolRemove;
        }

        public int GetManuallyOrderQuantity(string TradingSymbol)
        {
            int lastOrderQuantity = 0;
            string symbol = "";
            if (TradingSymbol.Contains("_"))
                symbol = TradingSymbol.Split('_')[1];
            else
                symbol = TradingSymbol;
            try
            {
                if (m_OrderListWithSymbolAndQuantity.ContainsKey(symbol))
                {
                    lastOrderQuantity = m_OrderListWithSymbolAndQuantity[symbol];
                    WriteUniquelogs(symbol + " " + m_UserID, "GetManuallyOrderQuantity : symbol " + TradingSymbol + " quantity " + lastOrderQuantity, MessageType.Informational);
                }
                else
                {
                    WriteUniquelogs(symbol + " " + m_UserID, "GetManuallyOrderQuantity : List not contains symbol " + TradingSymbol, MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(symbol + " " + m_UserID, "GetManuallyOrderQuantity : Exception Error Message " + e.Message, MessageType.Exception);
            }

            return lastOrderQuantity;
        }
        public bool CheckRiskQuantity(string TradingSymbol, string Direction)
        {
            bool res = false;
            if (m_RiskManagmentFlag)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "CheckRiskQuantity : risk management flag true", MessageType.Informational);
                int brokerquantity = GetOrderQuantityByDirection(TradingSymbol, Direction);
                if (brokerquantity >= m_BrokerQuantity)
                {
                    res = true;
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, "CheckRiskQuantity : broker quantity condtition not macthed m_BrokerQuantity = " + m_BrokerQuantity + " brokerquantity " + brokerquantity, MessageType.Informational);
                    // m_ExceptionDictionary.Add(m_ExceptionCounter, DateTime.Now + " " + "PlaceOrder - " + TradingSymbol + " order not placed!! Trading Stopped!!");
                    WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + TradingSymbol + " Broker quantity exceeds!!");
                    m_ExceptionCounter++;
                }
                int manualQuanity = GetManuallyOrderQuantity(TradingSymbol);
                if (res == false && manualQuanity >= m_ManualQuantity)
                {
                    res = true;
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, "CheckRiskQuantity : manual quantity condtition not macthed m_ManualQuantity = " + m_ManualQuantity + " manualQuanity " + manualQuanity, MessageType.Informational);
                    // m_ExceptionDictionary.Add(m_ExceptionCounter, DateTime.Now + " " + "PlaceOrder - " + TradingSymbol + " order not placed!! Trading Stopped!!");
                    WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + TradingSymbol + " Manual quantity exceeds!!");
                    m_ExceptionCounter++;
                }
            }
            else
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "CheckRiskQuantity : risk management flag false", MessageType.Informational);
                res = false;
            }

            return res;
        }

        public int GetOrderQuantityByDirection(string TradingSymbol, string TransactionType)
        {
            int lastOrderQuantity = 0;
            try
            {
                dynamic AllOrder = GetAllOrders();
                if (AllOrder.Count == 0)
                {
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOrderQuantityByDirection : AllOrder count is 0 TradingSymbol " + TradingSymbol + " TransactionType " + TransactionType, MessageType.Informational);
                }
                for (int i = 0; i <= AllOrder.Count - 1; i++)
                {
                    var order = AllOrder[i];

                    if (order.TransactionType == TransactionType && order.Status == Constants.ORDER_STATUS_COMPLETE && order.Tradingsymbol == TradingSymbol)
                    {
                        lastOrderQuantity += order.Quantity;
                    }
                }
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOrderQuantityByDirection : TradingSymbol " + TradingSymbol + " TransactionType " + TransactionType + " quantity " + lastOrderQuantity, MessageType.Informational);
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOrderQuantityByDirection : Exception Error Message " + e.Message, MessageType.Exception);
            }

            return lastOrderQuantity;
        }

        //sanika::9-Feb-2021::Added to get flag
        public bool IsOverallAllProfitLossHit()
        {
            return m_isSqrOff;
        }
        //IRDSPM::Pratiksha::10-02-2021::Added missing methods


        public void DownloadTokenFromuserManualClick()
        {
            try
            {
                m_StartedinstrumentDownloading = true;
                Thread.Sleep(2000);
                DateTime dt = DateTime.Now;
                IntrumentsCSVToDB();
                DateTime dt1 = DateTime.Now;
                double diff = dt.Subtract(dt1).TotalMinutes;
                File.AppendAllText("time.txt", "Time to download instruments " + diff);
                m_StartedinstrumentDownloading = false;
                WriteDateIntoFile();
            }
            catch (Exception e)
            {
                logger.LogMessage("DownloadTokenFromuserManualClick : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }
        public void ChangeUserID(string userid)
        {
            m_UserID = userid;
            objConfigSettings.readCompositeConfigFile(m_IniFilePath);//sanika::2-Dec-2020::Change methos name to load ini values of samco
            m_UserID = objConfigSettings.username;
        }
        public List<string> UpdateSymbolList(List<string> TradingSymbolList)
        {
            try
            {
                foreach (var symbol in m_RealTimeSymbols)
                {
                    if (!TradingSymbolList.Contains(symbol))
                        TradingSymbolList.Add(symbol);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs("DownloadData", "UpdateSymbolList : Exception Error Message " + e.Message, MessageType.Exception);
            }
            return TradingSymbolList;
        }

        public double GetTrialPeriod()
        {
            int trialPeriod = 7;
            try
            {
                if (m_MySqlConnection == null)
                {
                    m_MySqlConnection = new MySQLConnectZerodha();
                    m_isMysqlconnected = m_MySqlConnection.Connect(logger, m_ServerUrl, m_username, m_password, m_Dbname);
                }
                if (m_isMysqlconnected)
                {
                    string selectCmd = "Select trialdays from trialperiod;";
                    object obj = m_MySqlConnection.ExecuteScalar(selectCmd);
                    if (obj != null)
                    {
                        if (obj.GetType() != typeof(DBNull))
                        {
                            trialPeriod = Convert.ToInt32(obj.ToString());
                        }
                    }
                    else
                    {
                        trialPeriod = 7;
                    }
                    WriteUniquelogs("RegistrationProcess", "GetTrialPeriod :  trialPeriod " + trialPeriod, MessageType.Exception);
                }
            }
            catch (Exception ex)
            {
                trialPeriod = 7;
                WriteUniquelogs("RegistrationProcess", "GetTrialPeriod :  Exception Error Message = " + ex.Message, MessageType.Exception);
            }
            return trialPeriod;
        }
        public string FetchUserdetails(string clientid, string hardwareID)
        {
            string data = "";
            try
            {
                string strQuery = "SELECT * FROM UserDetails ";
                if (m_MySqlConnection == null)
                {
                    if (logger == null)
                    {
                        logger = Logger.Instance;
                    }
                    m_MySqlConnection = new MySQLConnectZerodha();
                    m_isMysqlconnected = m_MySqlConnection.Connect(logger, m_ServerUrl, m_username, m_password, m_Dbname);
                }
                if (m_isMysqlconnected)
                {
                    if (m_MySqlConnection.IsTableExists("UserDetails", m_Dbname))
                    {
                        data = m_MySqlConnection.FetchDataForSubscription(hardwareID);
                    }
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("RegistrationProcess", "FetchUserdetails :  Exception Error Message = " + ex.Message, MessageType.Exception);
            }
            return data;
        }
        public bool FetchUserdetailsfromBlockuser(string hardwareID)
        {
            bool data = false;
            try
            {
                if (m_MySqlConnection == null)
                {
                    if (logger == null)
                    {
                        logger = Logger.Instance;
                    }
                    m_MySqlConnection = new MySQLConnectZerodha();
                    m_isMysqlconnected = m_MySqlConnection.Connect(logger, m_ServerUrl, m_username, m_password, m_Dbname);
                    WriteUniquelogs("RegistrationProcess", "FetchUserdetailsfromBlockuser :  m_isMysqlconnected = " + m_isMysqlconnected, MessageType.Informational);
                }
                if (m_isMysqlconnected)
                {
                    if (!m_MySqlConnection.IsTableExists("UserDetails", m_Dbname))
                    {
                        WriteUniquelogs("RegistrationProcess", "FetchUserdetailsfromBlockuser :  IsTableExists = " + "Table not exist", MessageType.Informational);
                        //create table 
                        m_MySqlConnection.ExecuteNonQueryCommand("Create Table UserDetails(ID int(11),Name varchar(255),Usertype varchar(255),EmailId varchar(255),MobileNo varchar(255) ,UserName varchar(255),Password varchar(255),UserAddress varchar(255),City varchar(255),UserState varchar(255),ClientID varchar(255),ExpiryDate double,Remark varchar(255),Status varchar(255),OnlineInfo varchar(255),AdminId varchar(255),AffiliateID varchar(255),BlockStatus bool, lastlogindate double, PRIMARY KEY(ClientID))");
                    }
                    data = m_MySqlConnection.FetchDataForblock(hardwareID);
                    WriteUniquelogs("RegistrationProcess", "FetchUserdetailsfromBlockuser :  Block user Status = " + data, MessageType.Informational);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("RegistrationProcess", "FetchUserdetailsfromBlockuser :  Exception Error Message = " + ex.Message, MessageType.Exception);
            }
            return data;
        }
        public bool AddUserInformation(int Id, string name, string Usertype, string emailId, string mob, string username, string password, string UserAddress, string city, string Userstate, string clientid, double expiryDate, string remark, string status, string onlineinfo, string Adminid, string AffiliateID, double lastlogindate, bool blockStatus = false)
        {
            bool bolUserInformation = false;
            try
            {
                if (m_MySqlConnection == null)
                {
                    m_MySqlConnection = new MySQLConnectZerodha();
                    m_isMysqlconnected = m_MySqlConnection.Connect(logger, m_ServerUrl, m_username, m_password, m_Dbname);
                    WriteUniquelogs("RegistrationProcess", "m_isMysqlconnected : " + m_isMysqlconnected, MessageType.Informational);
                }
                if (m_isMysqlconnected)
                {
                    if (!m_MySqlConnection.IsTableExists("UserDetails", m_Dbname))
                    {
                        WriteUniquelogs("RegistrationProcess", "IsTableExists : false", MessageType.Informational);
                        //create table 
                        m_MySqlConnection.ExecuteNonQueryCommand("Create Table UserDetails(ID int(11),Name varchar(255),Usertype varchar(255),EmailId varchar(255),MobileNo varchar(255) ,UserName varchar(255),Password varchar(255),UserAddress varchar(255),City varchar(255),UserState varchar(255),ClientID varchar(255),ExpiryDate double,Remark varchar(255),Status varchar(255),OnlineInfo varchar(255),AdminId varchar(255),AffiliateID varchar(255),BlockStatus bool, lastlogindate double, PRIMARY KEY(ClientID))");
                    }
                    m_MySqlConnection.ExecuteNonQueryCommand("insert into UserDetails values(" + Id + ",'" + name + "','" + Usertype + "','" + emailId + "','" + mob + "','" + username + "','" + password + "','" + UserAddress + "','" + city + "','" + Userstate + "','" + clientid + "'," + expiryDate + ",'" + remark + "','" + status + "','" + onlineinfo + "','" + Adminid + "','" + AffiliateID + "'," + blockStatus + "," + lastlogindate + ")");
                    WriteUniquelogs("RegistrationProcess", "inserted successful : " + m_isMysqlconnected, MessageType.Informational);
                    bolUserInformation = true;
                }

            }
            catch (Exception ex)
            {
                WriteUniquelogs("RegistrationProcess", "AddUserInformation: Exception Error Message = " + ex.Message, MessageType.Exception);
            }

            return bolUserInformation;
        }

        //sanika::25-Feb-2021::Added to get individual profit loss 
        public double GetIndividualProfitLoss(string TradingSymbol, string Exchange)
        {
            double profitLoss = 0;
            try
            {
                if (m_IndividualProfitLoss.ContainsKey(TradingSymbol + "_" + Exchange))
                {
                    profitLoss = m_IndividualProfitLoss[TradingSymbol + "_" + Exchange];
                }
            }
            catch (Exception e)
            {
                profitLoss = 0;
            }

            return profitLoss;
        }

        public dynamic GetMarginFromStructure()
        {
            return userMargin;
        }

        //sanika::26-Feb-2021::Added to update margin after 1min
        public void UpdatePNLInFile()
        {
            try
            {
                if (DateTime.Now.ToOADate() > m_TimeToUpdatePNLInFile)
                {
                    m_TimeToUpdatePNLInFile = DateTime.Now.AddMinutes(1).ToOADate();
                    string today = DateTime.Now.ToString("dd-MM-yyyy");
                    string time = DateTime.Now.ToString("HH:mm:ss");
                    decimal marginUsed = 0;
                    decimal marginAvailable = 0;
                    decimal openingBalance = 0;
                    double pnl = m_TotalProfitLoss;
                    //sanika::8-July-2021::commented as need to update margin after every 5 min
                    //userMargin = GetMargin();
                    if (userMargin != null && userMargin.Count != 0)
                    {
                        marginUsed = Math.Round(userMargin["Utilised"], 2);
                        marginAvailable = Math.Round(userMargin["Net"], 2);
                        openingBalance = userMargin["Available"];
                    }
                    //if (!File.Exists(m_PNLFilePath))
                    //{
                    //    string header = String.Join(Environment.NewLine, "Date,Time,Margin Available,Margin Used,Opening Balance,PNL");
                    //    header += Environment.NewLine;
                    //    System.IO.File.AppendAllText(m_PNLFilePath, header);
                    //}
                    //using (StreamWriter streamWriter = File.AppendText(m_PNLFilePath))
                    //{
                    //    String csv = String.Join(Environment.NewLine, today + "," + time + "," + marginAvailable + "," + marginUsed + "," + openingBalance + "," + pnl);
                    //    streamWriter.WriteLine(csv);
                    //}
                }
            }
            catch (Exception e)
            {
                logger.LogMessage("UpdatePNLInFile : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }
        static bool m_IsMarginLoad = false;

        //sanika::8-July-2021::Added to update margin after every 1 min
        public void UpdateMargin()
        {
            try
            {
                //sanika::13-July-2021::flag condition
                if (DateTime.Now.ToOADate() > m_TimeToUpdateMargin || m_IsMarginLoad == true)
                {
                    m_IsMarginLoad = false;
                    //20-July-2021: sandip: added 5 sec refresh rate for margin
                    m_TimeToUpdateMargin = DateTime.Now.AddSeconds(5).ToOADate();
                    userMargin = GetMargin();
                }
            }
            catch (Exception e)
            {
                logger.LogMessage("UpdateMargin : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        public bool GetCloseValuesList(string TradingSymbol, int barCount, out List<double> closeValues, out List<string> dateTimevalues)
        {
            composite.GetCloseValuesList(TradingSymbol, barCount, out closeValues, out dateTimevalues);
            return true;
        }
        //IRDSPM::Pratiksha::15-06-2021::For clear all the symbols from trading symbol list
        public void ClearTradingSymbols()
        {
            composite.TradingSymbol.Clear();
        }
        public double GetLimitPricebyOrderID(string TradingSymbol, string Exchange, string orderId)
        {
            double Price = 0;
            try
            {
                OrderExtComposite orderExt;
                if (m_GlobalOrderStore.GetOpenOrderbyID(TradingSymbol, Exchange, orderId, out orderExt))
                {
                    if (orderExt == null)
                    {
                        WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetLimitPricebyOrderID : order info count is zero", MessageType.Informational);
                        return 0;
                    }
                    //if (orderExt.order.Status == Constants.ORDER_STATUS_PENDING)
                    if (orderExt.order.Status == Constants.ORDER_STATUS_OPEN)
                    {
                        Price = Convert.ToDouble(orderExt.order.Price);
                    }
                    //WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetLimitPricebyOrderID : Price " + Price, MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetLimitPricebyOrderID : Exception Error Message = " + e.Message, MessageType.Informational);
            }
            return Price;
        }
        //sanika::9-Jun-2021::Added to clear master list
        public void ClearMasterList()
        {
            m_DictionaryForOrderId.Clear();
        }

        public string GetMessageforNotification()
        {
            return m_Notification.GetMessageforNotification();
        }
        public void AddNotificationInQueue(string message)
        {
            m_Notification.AddNotificationInQueue(message);
        }

        public int GetOpenPostionQuantityByOrderID(string TradingSymbol, string Exchange, string orderId)
        {
            int quantity = 0;
            try
            {
                TradingSymbol = TradingSymbol.ToUpper();
                OrderExtComposite orderExt;
                if (m_GlobalOrderStore.GetOpenOrderbyID(TradingSymbol, Exchange, orderId, out orderExt))
                {
                    if (orderExt == null)
                    {
                        WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPostionQuantityByOrderID : order info count is zero", MessageType.Informational);
                        return 0;
                    }

                    quantity = Math.Abs(orderExt.order.Quantity);

                    //WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPostionQuantityByOrderId : quantity " + quantity, MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPostionQuantityByOrderID : Exception Error Message = " + e.Message, MessageType.Informational);
            }
            return quantity;
        }
        //IRDSPM::Pratiksha::09-07-2021::For toastNotification
        public void AddNotificationInQueueToast(string message)
        {
            WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + message);
            m_ExceptionCounter++;
        }

        public void CloseOrderWithOrderId(string OrderId, string TradingSymbol, string Exchange, string ProductType)
        {
            try
            {
                CancelOrder(OrderId, ProductType, TradingSymbol);
                OrderComposite latestOrder = new OrderComposite();
                if (!GetLatestOrder(TradingSymbol, Exchange, OrderId, out latestOrder))
                {
                    string errorMsg = string.Format("CloseOrderWithOrderId : Not found current order for symbol : {0}", TradingSymbol);
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, errorMsg, MessageType.Error);

                }
                else
                {
                    string direction = "";
                    string newOrderBuyOrSell = "";
                    int newOrderQty = 0;
                    string logMessage = string.Format("CloseOrderWithOrderId : Current order fetched from server for mapped symbol: {0} Net Filled Quantity: {1}",
                                                  TradingSymbol, latestOrder.FilledQuantity);
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, logMessage, MessageType.Informational);
                    if (latestOrder.TransactionType == Constants.TRANSACTION_TYPE_BUY)
                    {
                        newOrderBuyOrSell = Constants.TRANSACTION_TYPE_SELL;
                        direction = Constants.TRANSACTION_TYPE_BUY;
                    }
                    else if (latestOrder.TransactionType == Constants.TRANSACTION_TYPE_SELL)
                    {
                        newOrderBuyOrSell = Constants.TRANSACTION_TYPE_BUY;
                        direction = Constants.TRANSACTION_TYPE_SELL;
                    }

                    newOrderQty = Math.Abs(latestOrder.FilledQuantity);

                    if (newOrderQty != 0)
                    {
                        WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseOrderWithOrderId :newOrderQty is not zero" + newOrderQty, MessageType.Informational);
                        string orderID = PlaceOrder(Exchange, TradingSymbol, newOrderBuyOrSell, newOrderQty, Constants.ORDER_TYPE_MARKET, Product: ProductType);
                        if (orderID != "NA")
                        {
                            //double openPrice = GetOpenPostionPricebyOrderID(TradingSymbol, Exchange, orderID);
                            //WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseOrder : Order Complete at price = " + openPrice, MessageType.Informational);
                            WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseOrderWithOrderId : placed order successfully", MessageType.Informational);
                            WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + TradingSymbol + " " + direction + " Order Closed successfully!! ");
                            m_ExceptionCounter++;
                            DateTime dateTime = DateTime.Now;
                            m_LocalOrderStructureInfo.AddOrUpdateOrderInformation(TradingSymbol: TradingSymbol, Exchange: Exchange, ClosedTransactionType: newOrderBuyOrSell, Product: ProductType, ClosedQuantity: newOrderQty, ClosedOrderDateTime: dateTime, ClosedOrderId: orderID);
                        }
                        else
                        {
                            WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseOrderWithOrderId : order not closed", MessageType.Informational);
                            WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + TradingSymbol + " " + direction + " Order not Closed");
                            m_ExceptionCounter++;
                        }
                    }
                    else
                    {
                        WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseOrderWithOrderId :newOrderQty is zero", MessageType.Informational);
                        // m_ExceptionDictionary.Add(m_ExceptionCounter, DateTime.Now + " " + "CloseOrder - " + TradingSymbol + " Order closed already!!");
                        WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + TradingSymbol + " Order closed already!!");
                        m_ExceptionCounter++;
                    }
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseOrderWithOrderId: Exception " + ex.Message, MessageType.Error);
            }
        }
    }
}
