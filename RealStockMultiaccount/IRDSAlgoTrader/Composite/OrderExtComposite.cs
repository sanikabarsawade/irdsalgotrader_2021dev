﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static IRDSAlgoOMS.StructuresComposite;

namespace IRDSAlgoOMS
{
    public class OrderExtComposite
    {
        public OrderComposite order { get; set; }
        public DateTime LastUpdateTime { get; set; }
        
        public OrderExtComposite() { }

        public OrderExtComposite(OrderComposite order1)
        {
            order = order1;
            LastUpdateTime = DateTime.Now;
        }
        //20-July-2021: sandip:use existing order structue.

        public void SetOrderExtComposite(OrderComposite order1)
        {
            order = order1;
            LastUpdateTime = DateTime.Now;
        }
    }

    public class OrderStoreComposite
    {
        private Dictionary<string, SymbolWiseOrderStoreComposite> orderStoreBySymbolTT;
        private Dictionary<string, OrderComposite> AllOrder;
        private static readonly object OrdertickLock = new object();
        private Dictionary<string, string> orderIDWithStatus;

        public OrderStoreComposite()
        {
            orderStoreBySymbolTT = new Dictionary<string, SymbolWiseOrderStoreComposite>();
            AllOrder = new Dictionary<string, OrderComposite>();
            orderIDWithStatus = new Dictionary<string, string>();
        }

        public OrderStoreComposite(List<Instrument> instruments)
        {
            foreach (var instrument in instruments)
                orderStoreBySymbolTT.Add(GetKey(instrument.TradingSymbol, instrument.Exchange), new SymbolWiseOrderStoreComposite());
        }

        //private Dictionary<Tkey2, TValue> dic2 = new Dictionary<Tkey2, TValue>();

        public string GetKey(string tradingSymbol, string exchange)
        {
            return tradingSymbol + "|" + exchange;
        }
        //20-July-2021: sandip:add or update with normal structure
        public void AddOrUpdateOrder(OrderComposite newOrder)
        {
            lock (OrdertickLock)
            {
                if (!CheckStatusOfOrder(newOrder.OrderId, newOrder.Status))//Jyoti::16-Sep-2021::Added check if order is already complete then no need to add status
                {
                    var key = GetKey(newOrder.Tradingsymbol, newOrder.Exchange);
                    if (orderStoreBySymbolTT.ContainsKey(key))
                    {
                        SymbolWiseOrderStoreComposite symbolWiseOrderStore;
                        orderStoreBySymbolTT.TryGetValue(key, out symbolWiseOrderStore);
                        if (symbolWiseOrderStore != null)
                        {
                            OrderExtComposite objOrderExt;
                            symbolWiseOrderStore.GetOrder(newOrder.OrderId, out objOrderExt);
                            objOrderExt.SetOrderExtComposite(newOrder);
                            symbolWiseOrderStore.AddOrUpdateOrder(objOrderExt);
                        }
                    }
                    else
                    {
                        orderStoreBySymbolTT.Add(GetKey(newOrder.Tradingsymbol, newOrder.Exchange), new SymbolWiseOrderStoreComposite());
                        if (orderStoreBySymbolTT.ContainsKey(key))
                        {
                            SymbolWiseOrderStoreComposite symbolWiseOrderStore;
                            orderStoreBySymbolTT.TryGetValue(key, out symbolWiseOrderStore);
                            OrderExtComposite objOrderExt = new OrderExtComposite(newOrder);
                            symbolWiseOrderStore.AddOrUpdateOrder(objOrderExt);
                        }
                    }
                    AddStatusOfOrder(newOrder.OrderId, newOrder.Status);
                }
            }
        }
        public void AddOrUpdateOrder(OrderExtComposite newOrder)
        {
            lock (OrdertickLock)
            {
                var key = GetKey(newOrder.order.Tradingsymbol, newOrder.order.Exchange);
                if (orderStoreBySymbolTT.ContainsKey(key))
                {
                    SymbolWiseOrderStoreComposite symbolWiseOrderStore;
                    orderStoreBySymbolTT.TryGetValue(key, out symbolWiseOrderStore);
                    if (symbolWiseOrderStore != null)
                        symbolWiseOrderStore.AddOrUpdateOrder(newOrder);
                }
                else
                {
                    orderStoreBySymbolTT.Add(GetKey(newOrder.order.Tradingsymbol, newOrder.order.Exchange), new SymbolWiseOrderStoreComposite());
                    if (orderStoreBySymbolTT.ContainsKey(key))
                    {
                        SymbolWiseOrderStoreComposite symbolWiseOrderStore;
                        orderStoreBySymbolTT.TryGetValue(key, out symbolWiseOrderStore);

                        symbolWiseOrderStore.AddOrUpdateOrder(newOrder);
                    }
                }
            }

        }
        //20-July-2021: sandip:pass base structure

        public void AddOrder(OrderComposite newOrder)
        {
            //if (!CheckStatusOfOrder(newOrder.OrderId, newOrder.Status))//Jyoti::16-Sep-2021::Added check if order is already complete then no need to add status
            {
                if (!AllOrder.ContainsKey(newOrder.OrderId))
                {
                    AllOrder.Add(newOrder.OrderId, newOrder);
                }
                else
                {
                    AllOrder[newOrder.OrderId] = newOrder;
                }
                //AddStatusOfOrder(newOrder.OrderId, newOrder.Status);
            }
        }
        public bool GetOrderbyID(string tradingSymbol, string exchange, string orderId, out OrderExtComposite existingOrder)
        {
            lock (OrdertickLock)
            {
                var key = GetKey(tradingSymbol, exchange);
                SymbolWiseOrderStoreComposite symbolWiseOrderStore;
                orderStoreBySymbolTT.TryGetValue(key, out symbolWiseOrderStore);
                if (symbolWiseOrderStore != null)
                    return symbolWiseOrderStore.GetOrder(orderId, out existingOrder);
                else
                    existingOrder = new OrderExtComposite();
                return false;
            }
        }

        public List<OrderComposite> GetOrderbySymbol(string tradingSymbol, string exchange)
        {
            lock (OrdertickLock)
            {
                var key = GetKey(tradingSymbol, exchange);
                SymbolWiseOrderStoreComposite symbolWiseOrderStore;
                orderStoreBySymbolTT.TryGetValue(key, out symbolWiseOrderStore);

                List<OrderComposite> orderhistorybysymbol = new List<OrderComposite>();
                List<OrderComposite> previousOrderBySymbol = new List<OrderComposite>();

                //SymbolWiseOrderStore symbolWiseOrderStore;
                //orderStoreBySymbolTT.TryGetValue(key, out symbolWiseOrderStore);
                if (symbolWiseOrderStore != null)
                {
                    orderhistorybysymbol = symbolWiseOrderStore.GetAllOrder();
                }

                return orderhistorybysymbol;
            }
        }

        public List<OrderComposite> GetOpenOrderbySymbol(string tradingSymbol, string exchange)
        {
            lock (OrdertickLock)
            {
                var key = GetKey(tradingSymbol, exchange);
                SymbolWiseOrderStoreComposite symbolWiseOrderStore;
                orderStoreBySymbolTT.TryGetValue(key, out symbolWiseOrderStore);

                List<OrderComposite> orderhistorybysymbol = new List<OrderComposite>();
                List<OrderComposite> previousOrderBySymbol = new List<OrderComposite>();

                if (symbolWiseOrderStore != null)
                {
                    orderhistorybysymbol = symbolWiseOrderStore.GetAllOpenOrder();
                }

                return orderhistorybysymbol;
            }
        }

        public bool GetOpenOrderbyID(string tradingSymbol, string exchange, string orderId, out OrderExtComposite existingOrder)
        {
            lock (OrdertickLock)
            {
                var key = GetKey(tradingSymbol, exchange);
                SymbolWiseOrderStoreComposite symbolWiseOrderStore;
                orderStoreBySymbolTT.TryGetValue(key, out symbolWiseOrderStore);
                if (symbolWiseOrderStore != null)
                    return symbolWiseOrderStore.GetOrder(orderId, out existingOrder);
                else
                    existingOrder = new OrderExtComposite();
                return false;
            }
        }

        public bool GetLatestOrder(string mappedSymbol, string buyOrSell, out OrderExtComposite existingOrder)
        {
            lock (OrdertickLock)
            {
                SymbolWiseOrderStoreComposite symbolWiseOrderStore;
                orderStoreBySymbolTT.TryGetValue(mappedSymbol, out symbolWiseOrderStore);
                return symbolWiseOrderStore.GetLatestOrder(buyOrSell, out existingOrder);
            }
        }

        //Sanika::11-sep-2020::Remove out variable as per latest design
        public List<OrderComposite> GetAllOrder()
        {
            List<OrderComposite> Orders = new List<OrderComposite>();
            foreach (var order in AllOrder)
            {
                Orders.Add(order.Value);
            }
            return Orders;
        }

        //Jyoti::16-Sep-2021::Added check if order is already complete then no need to add status
        public bool CheckStatusOfOrder(string OrderId, string Status)
        {
            bool res = false;
            if (orderIDWithStatus.ContainsKey(OrderId))
            {
                if (orderIDWithStatus[OrderId] == Constants.ORDER_STATUS_COMPLETE)
                {
                    res = true;
                }
            }
            return res;
        }

        //Jyoti::16-Sep-2021::Added check if order is already complete then no need to add status
        public void AddStatusOfOrder(string OrderId, string Status)
        {
            bool res = false;
            if (orderIDWithStatus.ContainsKey(OrderId))
            {
                orderIDWithStatus[OrderId] = Status;
            }
            else
            {
                orderIDWithStatus.Add(OrderId, Status);
            }
        }

    }

    public class SymbolWiseOrderStoreComposite
    {
        public SymbolWiseOrderStoreComposite()
        { }

        private Dictionary<string, OrderExtComposite> orderStoreByOrderId = new Dictionary<string, OrderExtComposite>();

        public void AddOrUpdateOrder(OrderExtComposite newOrder)
        {
            if (orderStoreByOrderId.ContainsKey(newOrder.order.OrderId))
            {
                newOrder.LastUpdateTime = DateTime.Now;
                orderStoreByOrderId[newOrder.order.OrderId] = newOrder;
            }
            else
            {
                orderStoreByOrderId.Add(newOrder.order.OrderId, newOrder);
            }
        }

        public bool GetOrder(string orderId, out OrderExtComposite existingOrder)
        {
            orderStoreByOrderId.TryGetValue(orderId, out existingOrder);
            if (existingOrder != null)
            {
                return true;
            }
            else
            {
                existingOrder = new OrderExtComposite();
                return false;
            }
        }
        public List<OrderComposite> GetAllOrder()
        {
            List<OrderComposite> orderhistorybysymbol = new List<OrderComposite>();
            var orderList = orderStoreByOrderId.Select(o => o.Value).OrderBy(o => o.LastUpdateTime);
            var reverseList = orderList.Reverse();

            foreach (var order1 in reverseList)
            {
                //IRDS::Jyoti::11-Aug-21::Added to cancel PARTIALLYFILLED orders
                if (order1.order.Status == "TRIGGERPENDING" || order1.order.Status == Constants.ORDER_STATUS_OPEN || order1.order.Status == "PARTIALLYFILLED")
                //if (order1.order.Status == "Trigger Pending" || order1.order.Status == "Open")
                {
                    orderhistorybysymbol.Add(order1.order);
                }
            }
            return orderhistorybysymbol;
        }

        public List<OrderComposite> GetAllOpenOrder()
        {
            List<OrderComposite> orderhistorybysymbol = new List<OrderComposite>();
            var orderList = orderStoreByOrderId.Select(o => o.Value).OrderBy(o => o.LastUpdateTime);
            var reverseList = orderList.Reverse();

            foreach (var order1 in reverseList)
            {
                if (order1.order.Status == Constants.ORDER_STATUS_COMPLETE && order1.order.OrderType == Constants.ORDER_TYPE_MARKET)
                {
                    orderhistorybysymbol.Add(order1.order);
                }
            }
            return orderhistorybysymbol;
        }

        public bool GetLatestOrder(string buyOrSell, out OrderExtComposite existingOrder)
        {
            var orderList = orderStoreByOrderId.Select(o => o.Value).OrderBy(o => o.LastUpdateTime);
            var reverseList = orderList.Reverse();

            foreach (var order in reverseList)
            {
                if (order.order.TransactionType == buyOrSell)
                {
                    existingOrder = order;
                    return true;
                }
            }

            existingOrder = new OrderExtComposite();
            return false;
        }
    }

    public class PositionStoreComposite
    {
        private Dictionary<string, PositionInfoComposite> positionStore;
        private Dictionary<string, OpenPositionStore> TimeAndOrderId;
        private static readonly object positiontickLock = new object();
        public List<string> updatedSymbols;
        public string GetKey(string tradingSymbol, string exchange)
        {
            return tradingSymbol + "|" + exchange;
        }
        public int GetCount()
        {
            return positionStore.Count();
        }
        public PositionStoreComposite()
        {
            positionStore = new Dictionary<string, PositionInfoComposite>();
            TimeAndOrderId = new Dictionary<string, OpenPositionStore>();
            updatedSymbols = new List<string>();

            //foreach (var instrument in instruments)
            //{
            //    positionStore.Add(GetKey(instrument.TradingSymbol, instrument.Exchange), new PositionInfo());
            //}
        }

        public bool GetPosition(string mappedSymbol, out PositionInfoComposite positionInfo)
        {
            lock (positiontickLock)
            {
                return positionStore.TryGetValue(mappedSymbol, out positionInfo);
            }
        }

        public bool GetPosition(string tradingSymbol, string exchange, out PositionInfoComposite positionInfo)
        {
            var key = GetKey(tradingSymbol, exchange);
            return positionStore.TryGetValue(key, out positionInfo);
        }

        public void Clear()
        {
            lock (positiontickLock)
            {
                positionStore.Clear();
            }
        }

        public void AddOrUpdatePositionInfo(string tradingSymbol, string exchange, PositionComposite newPosition, bool dayOrNet)
        {
            lock (positiontickLock)
            {
                var key = GetKey(tradingSymbol, exchange);
                if (positionStore.ContainsKey(key))
                {
                    if (dayOrNet)
                        positionStore[key].dayPosition = newPosition;
                    else
                        positionStore[key].netPosition = newPosition;
                    positionStore[key].LastUpdateTime = DateTime.Now;

                    if (!updatedSymbols.Contains(key))
                        updatedSymbols.Add(key);
                }
                else
                {
                    PositionInfoComposite positionInfo = new PositionInfoComposite();
                    if (dayOrNet)
                        positionInfo.dayPosition = newPosition;
                    else
                        positionInfo.netPosition = newPosition;
                    positionInfo.LastUpdateTime = DateTime.Now;

                    positionStore.Add(key, positionInfo);

                    if (!updatedSymbols.Contains(key))
                        updatedSymbols.Add(key);
                }
            }
            return;
        }
        //20-July-2021: sandip:pass base structure
        public void AddOrderIdAndTime(OrderComposite orderExt)
        {
            lock (positiontickLock)
            {
                string TradingSymbol = orderExt.Tradingsymbol + "_" + orderExt.Exchange + "|" + orderExt.Product;

                if (!TimeAndOrderId.ContainsKey(TradingSymbol))
                {
                    OpenPositionStore OpenPositionStore = new OpenPositionStore();
                    OpenPositionStore.OrderId = orderExt.OrderId;
                    OpenPositionStore.TimeStamp = (DateTime)orderExt.OrderTimestamp;
                    TimeAndOrderId.Add(TradingSymbol, OpenPositionStore);
                }
                else
                {
                    OpenPositionStore openPositionStore = TimeAndOrderId[TradingSymbol];
                    if ((DateTime)orderExt.OrderTimestamp > openPositionStore.TimeStamp)
                    {
                        openPositionStore.OrderId = orderExt.OrderId;
                        openPositionStore.TimeStamp = (DateTime)orderExt.OrderTimestamp;
                        TimeAndOrderId[TradingSymbol] = openPositionStore;
                    }
                }
            }

        }

        public void GetOrderIdAndTime(string TradingSymbol, out DateTime Time, out string OrderId)
        {
            lock (positiontickLock)
            {
                if (TimeAndOrderId.ContainsKey(TradingSymbol))
                {
                    OpenPositionStore OpenPositionStore = TimeAndOrderId[TradingSymbol];
                    OrderId = OpenPositionStore.OrderId;
                    Time = OpenPositionStore.TimeStamp;

                }
                else
                {
                    Time = new DateTime();
                    OrderId = "";
                }
            }
        }

        public Dictionary<string, PositionInfoComposite> GetAllPositions()
        {
            Dictionary<string, PositionInfoComposite> allPositions = new Dictionary<string, PositionInfoComposite>();
            lock (positiontickLock)
            {
                foreach (KeyValuePair<string, PositionInfoComposite> entry in positionStore)
                {
                    allPositions.Add(entry.Key, entry.Value);
                }
            }
            return allPositions;
        }

        public void RemoveFromMasterList()
        {
            lock (positiontickLock)
            {
                try
                {
                    foreach (var position in positionStore)
                    {
                        string key = position.Key;
                        if (!updatedSymbols.Contains(key))
                            positionStore.Remove(key);
                    }
                }
                catch (Exception e)
                {

                }
            }
        }
    }
}
