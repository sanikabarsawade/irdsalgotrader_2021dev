﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRDSAlgoOMS
{
    public class StructuresComposite
    {
        //20-July-2021: sandip:remove unwanted variable
        //static SQLiteConnection Sql_con;

        private static readonly object tickdatalock = new object();//sanika::20-Jan-2021::Added for random crash
        public struct InstrumentsComp
        {
            /// <summary>
            /// Gets or sets the exchange
            /// <see cref="ExchangeSegment"/>
            /// </summary>
            public int exchangeSegment { get; set; }

            /// <summary>
            /// Gets or sets the exchange instrument id
            /// </summary>
            public long exchangeInstrumentID { get; set; }
        }

        public struct OrderSource
        {
            /// <summary>
            /// Gets the TWSAPI source
            /// </summary>
            public static readonly string TWSAPI = "TWSAPI";
            /// <summary>
            /// Gets the WebAPI source
            /// </summary>
            public static readonly string WebAPI = "WebAPI";
            /// <summary>
            /// Gets the MobileAndrridAPI
            /// </summary>
            public static readonly string MobileAndroidAPI = "MobileAndroidAPI";
            /// <summary>
            /// Gets the MobileWindowsAPI
            /// </summary>
            public static readonly string MobileWindowsAPI = "MobileWindowsAPI";
            /// <summary>
            /// Gets teh MobileIOSAPI
            /// </summary>
            public static readonly string MobileIOSAPI = "MobileIOSAPI";
        }

        public struct ListQuotesBase
        {
            /// <summary>
            /// Gets or sets the exchange segment
            /// </summary>
            public int ExchangeSegment { get; set; }

            /// <summary>
            /// Gets or sets the exchange instrument id
            /// </summary>
            public long ExchangeInstrumentID { get; set; }

            /// <summary>
            /// Gets or sets the exchange time stamp. 
            /// <see cref="Globals.ToDateTime(long)"/>
            /// </summary>
            public long ExchangeTimeStamp { get; set; }


            public void AssignValue(object data)
            {
                if (data == null)
                    return;

                string[] array = data.ToString().Split(',');

                foreach (var item in array)
                {
                    if (string.IsNullOrEmpty(item))
                        continue;

                    string[] array1 = item.Split(':');
                    if (array1.Length != 2)
                        continue;

                    this.Parse(array1[0], array1[1]);
                }

            }

            void Parse(string field, string value)
            {
                switch (field)
                {
                    case "t":
                        string[] array = value.Split('_');

                        if (int.TryParse(array[0], NumberStyles.Integer, CultureInfo.InvariantCulture, out int exchange))
                        {
                            this.ExchangeSegment = exchange;
                        }

                        if (long.TryParse(array[1], NumberStyles.Integer, CultureInfo.InvariantCulture, out long token))
                        {
                            this.ExchangeInstrumentID = token;
                        }

                        break;
                }
            }

        }
        //28-July-2021: sandip:convert to class for dispose.
        public class OrderComposite
        {
            //20-July-2021: sandip:added new json support. duplicate.
            public OrderComposite()
            {

            }
            public OrderComposite(dynamic data)
            {
                try
                {
                    InstrumentToken = data["ExchangeInstrumentID"].ToString();
                    Exchange = "";
                    if (data["ExchangeSegment"] == "NSECM")
                    {
                        Exchange = "NSE";
                    }
                    if (data["ExchangeSegment"] == "NSEFO")
                    {
                        Exchange = "NFO";
                    }
                    OrderId = data["AppOrderID"].ToString();
                    Tradingsymbol = "";
                    TransactionType = Convert.ToString(data["OrderSide"]).ToUpper();
                    Product = Convert.ToString(data["ProductType"]).ToUpper();
                    OrderType = Convert.ToString(data["OrderType"]).ToUpper();
                    Price = ((data["OrderPrice"] == 0) && (data["OrderAverageTradedPrice"] != "") ? Convert.ToDecimal(data["OrderAverageTradedPrice"]) : Convert.ToDecimal(data["OrderPrice"]));
                    TriggerPrice = Convert.ToDecimal(data["OrderStopPrice"]);
                    orderValidity = data["TimeInForce"];
                    orderStatus = data["OrderStatus"];
                    //orderValue = data["orderValue"];
                    //OrderTimestamp = DateTime.ParseExact((data["OrderGeneratedDateTime"]), "dd-MMM-yyyy HH:mm:ss", null);
                    OrderTimestamp = Convert.ToDateTime(data["OrderGeneratedDateTime"]);
                    userId = data["LoginID"];
                    FilledQuantity = data["CumulativeQuantity"];
                    //fillPrice = data["fillPrice"];
                    AveragePrice = ((data["OrderAverageTradedPrice"] != "") ? Convert.ToDecimal(data["OrderAverageTradedPrice"]) : 0);
                    exchangeConfirmationTime = data["ExchangeTransactTime"];
                    //coverOrderPercentage = data["coverOrderPercentage"];
                    StatusMessage = data["OrderLegStatus"];
                    //if (data.ContainsKey("CancelRejectReason"))
                    //    StatusMessage = data["CancelRejectReason"];
                    exchangeOrderNumber = data["ExchangeOrderID"];
                    //symbol = data["symbol"];
                    //displayStrikePrice = data["displayStrikePrice"];
                    displayNetQuantity = data["OrderDisclosedQuantity"];
                    Status = Convert.ToString(data["OrderStatus"]).ToUpper();
                    if (Status == "NEW" || Status == "REPLACED")
                        Status = "OPEN";
                    if (Status == "FILLED")
                        Status = "COMPLETE";
                    //exchangeStatus = data["exchangeStatus"];
                    expiry = data["OrderExpiryDate"];
                    pendingQuantity = data["LeavesQuantity"];
                    Quantity = Convert.ToInt32(data["OrderQuantity"]);
                    //optionType = data["optionType"];
                    orderPlaceBy = data["GeneratedBy"];
                    ParentOrderId = data["OrderReferenceID"];
                    UniqueId = data["OrderUniqueIdentifier"];
                }
                catch (Exception ex)
                {
                    throw new DataException("Unable to parse data. " + Utils.JsonSerialize(data));
                }
            }

            public OrderComposite(Dictionary<string, dynamic> data)
            {
                try
                {
                    InstrumentToken = data["ExchangeInstrumentID"].ToString();
                    Exchange = "";
                    if (data["ExchangeSegment"] == "NSECM")
                    {
                        Exchange = "NSE";
                    }
                    if (data["ExchangeSegment"] == "NSEFO")
                    {
                        Exchange = "NFO";
                    }
                    OrderId = data["AppOrderID"].ToString();
                    Tradingsymbol = "";
                    TransactionType = data["OrderSide"].ToUpper();
                    Product = data["ProductType"].ToUpper();
                    OrderType = data["OrderType"].ToUpper();
                    Price = ((data["OrderPrice"] == 0) && (data["OrderAverageTradedPrice"] != "") ? Convert.ToDecimal(data["OrderAverageTradedPrice"]) : Convert.ToDecimal(data["OrderPrice"]));
                    TriggerPrice = Convert.ToDecimal(data["OrderStopPrice"]);
                    orderValidity = data["TimeInForce"];
                    orderStatus = data["OrderStatus"];
                    //orderValue = data["orderValue"];
                    //OrderTimestamp = DateTime.ParseExact((data["OrderGeneratedDateTime"]), "dd-MMM-yyyy HH:mm:ss", null);
                    OrderTimestamp = Convert.ToDateTime(data["OrderGeneratedDateTime"]);
                    userId = data["LoginID"];
                    FilledQuantity = data["CumulativeQuantity"];
                    //fillPrice = data["fillPrice"];
                    AveragePrice = ((data["OrderAverageTradedPrice"] != "") ? Convert.ToDecimal(data["OrderAverageTradedPrice"]) : 0);
                    exchangeConfirmationTime = data["ExchangeTransactTime"];
                    //coverOrderPercentage = data["coverOrderPercentage"];
                    StatusMessage = data["OrderLegStatus"];
                    if (data.ContainsKey("CancelRejectReason"))
                        StatusMessage = data["CancelRejectReason"];
                    exchangeOrderNumber = data["ExchangeOrderID"];
                    //symbol = data["symbol"];
                    //displayStrikePrice = data["displayStrikePrice"];
                    displayNetQuantity = data["OrderDisclosedQuantity"];
                    Status = data["OrderStatus"].ToUpper();
                    if (Status == "NEW" || Status == "REPLACED")
                        Status = "OPEN";
                    if (Status == "FILLED")
                        Status = "COMPLETE";
                    //exchangeStatus = data["exchangeStatus"];
                    expiry = data["OrderExpiryDate"];
                    pendingQuantity = data["LeavesQuantity"];
                    Quantity = Convert.ToInt32(data["OrderQuantity"]);
                    //optionType = data["optionType"];
                    orderPlaceBy = data["GeneratedBy"];
                    ParentOrderId = data["OrderReferenceID"];
                    UniqueId = data["OrderUniqueIdentifier"];
                }
                catch (Exception ex)
                {
                    throw new DataException("Unable to parse data. " + Utils.JsonSerialize(data));
                }
            }
            public void SetOrderComposite(Dictionary<string, dynamic> data)
            {
                try
                {
                    InstrumentToken = data["ExchangeInstrumentID"].ToString();
                    Exchange = "";
                    if (data["ExchangeSegment"] == "NSECM")
                    {
                        Exchange = "NSE";
                    }
                    if (data["ExchangeSegment"] == "NSEFO")
                    {
                        Exchange = "NFO";
                    }
                    OrderId = data["AppOrderID"].ToString();
                    Tradingsymbol = "";
                    TransactionType = data["OrderSide"].ToUpper();
                    Product = data["ProductType"].ToUpper();
                    OrderType = data["OrderType"].ToUpper();
                    Price = ((data["OrderPrice"] == 0) && (data["OrderAverageTradedPrice"] != "") ? Convert.ToDecimal(data["OrderAverageTradedPrice"]) : Convert.ToDecimal(data["OrderPrice"]));
                    TriggerPrice = Convert.ToDecimal(data["OrderStopPrice"]);
                    orderValidity = data["TimeInForce"];
                    orderStatus = data["OrderStatus"];
                    //orderValue = data["orderValue"];
                    //OrderTimestamp = DateTime.ParseExact((data["OrderGeneratedDateTime"]), "dd-MMM-yyyy HH:mm:ss", null);
                    OrderTimestamp = Convert.ToDateTime(data["OrderGeneratedDateTime"]);
                    userId = data["LoginID"];
                    FilledQuantity = data["CumulativeQuantity"];
                    //fillPrice = data["fillPrice"];
                    AveragePrice = ((data["OrderAverageTradedPrice"] != "") ? Convert.ToDecimal(data["OrderAverageTradedPrice"]) : 0);
                    exchangeConfirmationTime = data["ExchangeTransactTime"];
                    //coverOrderPercentage = data["coverOrderPercentage"];
                    StatusMessage = data["OrderLegStatus"];
                    if (data.ContainsKey("CancelRejectReason"))
                        StatusMessage = data["CancelRejectReason"];
                    exchangeOrderNumber = data["ExchangeOrderID"];
                    //symbol = data["symbol"];
                    //displayStrikePrice = data["displayStrikePrice"];
                    displayNetQuantity = data["OrderDisclosedQuantity"];
                    Status = data["OrderStatus"].ToUpper();
                    if (Status == "NEW" || Status == "REPLACED")
                        Status = "OPEN";
                    if (Status == "FILLED")
                        Status = "COMPLETE";
                    //exchangeStatus = data["exchangeStatus"];
                    expiry = data["OrderExpiryDate"];
                    pendingQuantity = data["LeavesQuantity"];
                    Quantity = Convert.ToInt32(data["OrderQuantity"]);
                    //optionType = data["optionType"];
                    orderPlaceBy = data["GeneratedBy"];
                    ParentOrderId = data["OrderReferenceID"];
                    UniqueId = data["OrderUniqueIdentifier"];
                }
                catch (Exception ex)
                {
                    throw new DataException("Unable to parse data. " + Utils.JsonSerialize(data));
                }
            }
            public void Clear()
            {
                try
                {
                    InstrumentToken = ""; ;
                    Exchange = "";                    
                    Exchange = ""; ;
                    
                    OrderId = ""; ;
                    Tradingsymbol = "";
                    TransactionType = ""; ;
                    Product = ""; ;
                    OrderType = ""; ;
                    Price = 0;
                    TriggerPrice = 0;
                    orderValidity = "";
                    orderStatus = "";
                    //orderValue = data["orderValue"];
                    //OrderTimestamp = DateTime.ParseExact((data["OrderGeneratedDateTime"]), "dd-MMM-yyyy HH:mm:ss", null);
                    
                    userId = "";
                    FilledQuantity = 0;
                    //fillPrice = data["fillPrice"];
                    AveragePrice = 0;
                    exchangeConfirmationTime = "";
                    //coverOrderPercentage = data["coverOrderPercentage"];
                    StatusMessage = "";
                    
                        StatusMessage = "";
                    exchangeOrderNumber = "";
                    //symbol = data["symbol"];
                    //displayStrikePrice = data["displayStrikePrice"];
                    displayNetQuantity = 0;
                    Status = "";
                    expiry = "";
                    pendingQuantity = 0;
                    Quantity = 0;
                    //optionType = data["optionType"];
                    orderPlaceBy = "";
                    ParentOrderId = "";
                    UniqueId = "";
                }
                catch (Exception ex)
                {
                    throw new DataException("Unable to parse data. ");
                }
            }
            public string InstrumentToken { get; set; }
            public string OrderId { get; set; }
            public string Exchange { get; set; }
            public string Tradingsymbol { get; set; }
            public string TransactionType { get; set; }
            public string Product { get; set; }
            public string OrderType { get; set; }
            public decimal Price { get; set; }
            public decimal AveragePrice { get; set; }
            public decimal TriggerPrice { get; set; }
            public string orderValidity { get; set; }
            public string orderStatus { get; set; }
            public DateTime OrderTimestamp { get; set; }
            public string userId { get; set; }
            public int FilledQuantity { get; set; }
            //public decimal AveragePrice { get; set; }
            public string exchangeConfirmationTime { get; set; }
            public string StatusMessage { get; set; }
            public string exchangeOrderNumber { get; set; }
            public int displayNetQuantity { get; set; }
            public string Status { get; set; }
            //public string exchangeStatus { get; set; }
            public string expiry { get; set; }
            public int pendingQuantity { get; set; }
            public int Quantity { get; set; }
            public string orderPlaceBy { get; set; }
            public string ParentOrderId { get; set; }
            public string UniqueId { get; set; }
        }

        public struct PositionResponseComposite
        {
            
            public PositionResponseComposite(Dictionary<string, dynamic> dataNet, Dictionary<string, dynamic> dataDay,DateTime dateTime, Dictionary<string, int> UniquePositions)
            {
                //04-August-2021: sandip:position memory leak issue
                Day = null;
                Net = null;

                if (dataDay.ContainsKey("result"))
                {
                    foreach (Dictionary<string, dynamic> item in dataDay["result"]["positionList"])
                    {
                        PositionComposite position = new PositionComposite(item, dateTime);
                        string temp = "PositionResponseComposite  Day : symbol " + position.TradingSymbol + "_" + position.Quantity + "_" + position.AveragePrice + " " + position.Exchange + "_" + position.Product + "" + position.BuyPrice + "_" + position.SellPrice + "" + position.buyQuantity + "_" + position.sellQuantity;
                        if (UniquePositions.ContainsKey(temp) == false)
                        {
                            if(Day==null)
                                Day = new List<PositionComposite>();
                            Day.Add(position);
                        }
                        else
                        {
                            position.Clear();
                            position = null;
                        }
                    }
                }
                if (dataNet != null)//sanika::21-Jan-2021::To avoid null exception
                {
                    if (dataNet.ContainsKey("result"))
                    {
                        foreach (Dictionary<string, dynamic> item in dataNet["result"]["positionList"])
                        {
                            PositionComposite position = new PositionComposite(item, dateTime);
                            string temp = "PositionResponseComposite  Net : symbol " + position.TradingSymbol + "_" + position.Quantity + "_" + position.AveragePrice + " " + position.Exchange + "_" + position.Product + "" + position.BuyPrice + "_" + position.SellPrice + "" + position.buyQuantity + "_" + position.sellQuantity;
                            if (UniquePositions.ContainsKey(temp) == false)
                            {
                                if(Net==null)
                                    Net = new List<PositionComposite>();
                                Net.Add(position);
                                UniquePositions.Add(temp,0);
                            }
                            else
                            {
                                position.Clear();
                                position = null;
                            }
                                
                        }
                        
                    }
                }
            }

            public List<PositionComposite> Day { get; }
            public List<PositionComposite> Net { get; }
        }

        //04-August-2021: jyoti/sandip:position memory leak issue
        public class PositionComposite
        {
            public PositionComposite()
            {
            }
            public PositionComposite(Dictionary<string, dynamic> data,DateTime dateTime)
            {
                try
                {
                    InstrumentToken = data["ExchangeInstrumentId"].ToString();
                    Exchange = "";
                    if (data["ExchangeSegment"] == "NSECM")
                    {
                        Exchange = "NSE";
                    }
                    if (data["ExchangeSegment"] == "NSEFO")
                    {
                        Exchange = "NFO";
                    }
                    Product = data["ProductType"];
                    TradingSymbol = data["TradingSymbol"].Split(' ')[0];
                    //sanika::28-Apr-2021::Added condition for option symbols
                    if (data["TradingSymbol"].Contains(" PE ") || data["TradingSymbol"].Contains(" CE "))
                    {
                        string name = data["TradingSymbol"];
                        string currentDate = dateTime.AddDays(-1).Day.ToString();
                        string currentMonth = dateTime.AddDays(-1).ToString("MMM").ToUpper();
                        if (name.Contains(currentDate+currentMonth))
                        {
                            string[] splitted = name.Split(' ');
                            DateTime d = DateTime.Parse(splitted[1]);
                            string year = d.ToString("yy");
                            string month = d.ToString("MMM").ToUpper();
                            string date = d.ToString("dd");
                            TradingSymbol = splitted[0] + year + month + splitted[3] + splitted[2];
                        }
                        else 
                        {
                            string[] splitted = name.Split(' ');
                            DateTime d = DateTime.Parse(splitted[1]);
                            string year = d.ToString("yy");
                            int month = d.Month;
                            string date = d.ToString("dd");
                            TradingSymbol = splitted[0] + year + month.ToString() + date.ToString() + splitted[3] + splitted[2];
                        }
                    }
                    Quantity = (int)Convert.ToDouble(data["Quantity"]);
                    BuyPrice = data["BuyAveragePrice"];
                    SellPrice = data["SellAveragePrice"];
                    if (SellPrice != "NaN")
                    {
                        if (BuyPrice != "NaN")
                        {
                    AveragePrice = (Convert.ToDecimal(BuyPrice) + Convert.ToDecimal(SellPrice)) / 2;
                        }
                        else
                        {
                            AveragePrice = Convert.ToDecimal(SellPrice);
                        }
                    }
                    else if (BuyPrice != "NaN")
                    {
                        AveragePrice = Convert.ToDecimal(BuyPrice);
                    }
                    else
                    {
                        AveragePrice = 0;
                    }
                    buyQuantity = (int)Convert.ToDouble(data["OpenBuyQuantity"]);
                    sellQuantity = (int)Convert.ToDouble(data["OpenSellQuantity"]);
                    multiplier = data["Multiplier"].ToString();
                    if (Quantity != 0)
                    {
                        BuyValue = Convert.ToDecimal(data["BuyAmount"]);
                        SellValue = Convert.ToDecimal(data["SellAmount"]);
                    }
                    else
                    {
                        BuyValue = Convert.ToDecimal(data["BuyAmount"]);
                        SellValue = Convert.ToDecimal(data["SellAmount"]);
                    }
                    //transactionType = data["transactionType"];
                    //PNL = Convert.ToDecimal(data["MTM"]);
                    PNL = Convert.ToDecimal(data["NetAmount"]);
                    unrealizedGainAndLoss = data["UnrealizedMTM"];
                    LastPrice = AveragePrice;
                    //companyName = data["companyName"];
                }
                catch (Exception ex)
                {
                    throw new DataException("Unable to parse data. " + Utils.JsonSerialize(data));
                }

            }

            public decimal AveragePrice { get; set; }
            public decimal LastPrice { get; set; }
            public string InstrumentToken { get; set; }
            public string Exchange { get; set; }
            public string Product { get; set; }
            public string TradingSymbol { get; set; }
            public int Quantity { get; set; }
            public string BuyPrice { get; set; }
            public string SellPrice { get; set; }
            public int buyQuantity { get; set; }
            public int sellQuantity { get; set; }
            public decimal BuyValue { get; set; }
            public string multiplier { get; set; }
            public decimal SellValue { get; set; }
            //public string transactionType { get; set; }
            public decimal PNL { get; set; }
            public string unrealizedGainAndLoss { get; set; }

            public DateTime GetLastThusdayDate()
            {
                DateTime date = DateTime.Now;
                int year = DateTime.Now.Year;
                int month = DateTime.Now.Month;
                try
                {
                    var tmpDate = new DateTime(year, month, 1).AddMonths(1).AddDays(-1);
                    while (tmpDate.DayOfWeek != DayOfWeek.Thursday)
                    {
                        tmpDate = tmpDate.AddDays(-1);
                    }
                    date = tmpDate.AddDays(1);
                }
                catch (Exception e)
                {

                }
                return date;
            }
            //04-August-2021: jyoti:position memory leak issue
            public void Clear()
            {
                AveragePrice = 0;
                LastPrice = 0;
                InstrumentToken = "";
                Exchange = "";
                Product = "";
                TradingSymbol = "";
                Quantity = 0;
                BuyPrice = "";
                SellPrice = "";
                buyQuantity = 0;
                sellQuantity = 0;
                BuyValue = 0;
                multiplier = "";
                SellValue = 0;
                //transactionType = "";
                PNL = 0;
                unrealizedGainAndLoss = "";
            }

        }

        public class PositionInfoComposite
        {
            public PositionComposite dayPosition { get; set; }
            public PositionComposite netPosition { get; set; }
            public DateTime LastUpdateTime { get; set; }
            //public PositionInfoComposite() { }
        }

        public class TickDataComposite
        {
            private Dictionary<string, TickDataComposite> symbolWiseTickInfo;
            public decimal LTP { get; set; }
            decimal Close { get; set; }
            public decimal BidPrice { get; set; }
            public decimal AskPrice { get; set; }
            decimal Volume { get; set; }
            decimal Open { get; set; }
            decimal High { get; set; }
            decimal Low { get; set; }
            decimal ChangeInPercent { get; set; }
            //sanika::8-oct-2020::Added to add change in absolute
            decimal ChangeInAbsolute { get; set; }

            decimal YesterdayClose { get; set; }

            public TickDataComposite()
            {
                symbolWiseTickInfo = new Dictionary<string, TickDataComposite>();
            }

            public void AddOrUpdateTickData(Tick tick, string Symbol)
            {
                //sanika::20-Jan-2021::Added for random crash
                lock (tickdatalock)
                {
                    decimal changeInValues = tick.LastPrice - tick.Close;
                    decimal basePrice;
                    if (tick.LastPrice > tick.Close)
                    {
                        basePrice = tick.LastPrice;
                    }
                    else
                    {
                        basePrice = tick.Close;
                    }
                    decimal changeInPercentage = Math.Round(((changeInValues / basePrice) * 100), 2);

                    if (symbolWiseTickInfo.ContainsKey(Symbol))
                    {
                        symbolWiseTickInfo[Symbol].LTP = tick.LastPrice;
                        symbolWiseTickInfo[Symbol].Close = tick.Close;
                        symbolWiseTickInfo[Symbol].Volume = tick.Volume;
                        symbolWiseTickInfo[Symbol].Open = tick.Open;
                        symbolWiseTickInfo[Symbol].High = tick.High;
                        symbolWiseTickInfo[Symbol].Low = tick.Low;
                        symbolWiseTickInfo[Symbol].ChangeInPercent = changeInPercentage;
                        //sanika::8-oct-2020::Added to add change in absolute
                        symbolWiseTickInfo[Symbol].ChangeInAbsolute = changeInValues;
                        if (tick.Bids != null)
                        {
                            symbolWiseTickInfo[Symbol].BidPrice = tick.Bids[0].Price;
                        }
                        else
                        {
                            symbolWiseTickInfo[Symbol].BidPrice = 0;
                        }
                        if (tick.Offers != null)
                        {
                            symbolWiseTickInfo[Symbol].AskPrice = tick.Offers[0].Price;
                        }
                        else
                        {
                            symbolWiseTickInfo[Symbol].AskPrice = 0;
                        }
                    }
                    else
                    {
                        TickDataComposite tickData = new TickDataComposite();
                        tickData.LTP = tick.LastPrice;
                        tickData.Close = tick.Close;
                        tickData.Volume = tick.Volume;
                        tickData.Open = tick.Open;
                        tickData.High = tick.High;
                        tickData.Low = tick.Low;
                        tickData.ChangeInPercent = changeInPercentage;
                        //sanika::8-oct-2020::Added to add change in absolute
                        tickData.ChangeInAbsolute = changeInValues;
                        if (tick.Bids != null)
                        {
                            tickData.BidPrice = tick.Bids[0].Price;
                        }
                        else
                        {
                            tickData.BidPrice = 0;
                        }
                        if (tick.Offers != null)
                        {
                            tickData.AskPrice = tick.Offers[0].Price;
                        }
                        else
                        {
                            tickData.AskPrice = 0;
                        }

                        symbolWiseTickInfo.Add(Symbol, tickData);
                    }
                }
            }

            public decimal GetLastPrice(string Symbol)
            {
                decimal lastPrice = 0;
                if (symbolWiseTickInfo.ContainsKey(Symbol))
                {
                    lastPrice = symbolWiseTickInfo[Symbol].LTP;
                }
                return lastPrice;
            }

            public decimal GetClosePrice(string Symbol)
            {
                decimal closePrice = 0;
                if (symbolWiseTickInfo.ContainsKey(Symbol))
                {
                    closePrice = symbolWiseTickInfo[Symbol].Close;
                }
                return closePrice;
            }

            public decimal GetBidPrice(string Symbol)
            {
                decimal bidPrice = 0;
                if (symbolWiseTickInfo.ContainsKey(Symbol))
                {
                    bidPrice = symbolWiseTickInfo[Symbol].BidPrice;
                }
                return bidPrice;
            }

            public decimal GetAskPrice(string Symbol)
            {
                decimal askPrice = 0;
                if (symbolWiseTickInfo.ContainsKey(Symbol))
                {
                    askPrice = symbolWiseTickInfo[Symbol].AskPrice;
                }
                return askPrice;
            }

            public decimal GetVolume(string Symbol)
            {
                decimal volume = 0;
                if (symbolWiseTickInfo.ContainsKey(Symbol))
                {
                    volume = symbolWiseTickInfo[Symbol].Volume;
                }
                return volume;
            }

            public decimal GetOpenPrice(string Symbol)
            {
                decimal openPrice = 0;
                if (symbolWiseTickInfo.ContainsKey(Symbol))
                {
                    openPrice = symbolWiseTickInfo[Symbol].Open;
                }
                return openPrice;
            }

            public decimal GetHighPrice(string Symbol)
            {
                decimal highPrice = 0;
                if (symbolWiseTickInfo.ContainsKey(Symbol))
                {
                    highPrice = symbolWiseTickInfo[Symbol].High;
                }
                return highPrice;
            }

            public decimal GetLowPrice(string Symbol)
            {
                decimal lowPrice = 0;
                if (symbolWiseTickInfo.ContainsKey(Symbol))
                {
                    lowPrice = symbolWiseTickInfo[Symbol].Low;
                }
                return lowPrice;
            }

            public decimal GetChangeInPercentage(string Symbol)
            {
                decimal changeInPercentage = 0;
                if (symbolWiseTickInfo.ContainsKey(Symbol))
                {
                    changeInPercentage = symbolWiseTickInfo[Symbol].ChangeInPercent;
                }
                return changeInPercentage;
            }

            //sanika::8-oct-2020::Added to get change in absolute
            public decimal GetChangeInAbsolute(string Symbol)
            {
                decimal changeInAbsolute = 0;
                if (symbolWiseTickInfo.ContainsKey(Symbol))
                {
                    changeInAbsolute = symbolWiseTickInfo[Symbol].ChangeInAbsolute;
                }
                return changeInAbsolute;
            }

            public decimal GetYesterdayClose(string Symbol)
            {
                decimal yesterdayClose = 0;
                if (symbolWiseTickInfo.ContainsKey(Symbol))
                {
                    yesterdayClose = symbolWiseTickInfo[Symbol].YesterdayClose;
                }
                return yesterdayClose;
            }

            public void AddYesterdayClose(double close, string symbol)
            {
                if (symbolWiseTickInfo.ContainsKey(symbol))
                {
                    symbolWiseTickInfo[symbol].YesterdayClose = Convert.ToDecimal(close);
                }
                else
                {
                    TickDataComposite tickData = new TickDataComposite();
                    tickData.YesterdayClose = Convert.ToDecimal(close);
                    symbolWiseTickInfo.Add(symbol, tickData);
                }
            }
        }

        public struct OrdersPlaced
        { 
            public string TradingSymbol { get; set; }
            public string Exchange { get; set; }
            public string TransactionType { get; set; }
            public decimal TriggerPrice { get; set; }
            public decimal Price { get; set; }
            public string Product { get; set; }
            public int Quantity { get; set; }
            public string OrderType { get; set; }
        }

        public struct HoldingComposite
        {
            public HoldingComposite(Dictionary<string, dynamic> data)
            {
                try
                {
                    //Product = data["productCode"];
                    Exchange = data["exchangeSegment"];
                    //Price = (decimal)Convert.ToDouble(data["lastTradedPrice"]);
                    //LastPrice = (decimal)Convert.ToDouble(data["lastTradedPrice"]);
                    CollateralQuantity = data["HoldingQuantity"];
                    //PNL = data["totalGainAndLoss"];
                    //ClosePrice = data["previousClose"];
                    //AveragePrice = data["averagePrice"];
                    //TradingSymbol = data["tradingSymbol"];
                    //ISIN = data["isin"];
                    //Quantity = (int)Convert.ToDouble(data["HoldingQuantity"]);
                    //markToMarketPrice = data["markToMarketPrice"];
                    //symbolDescription = data["symbolDescription"];
                    //holdingsValue = data["holdingsValue"];
                    //holdingsQuantity = data["holdingsQuantity"];
                    //sellableQuantity = data["sellableQuantity"];
                    //totalMarketToMarketPrice = data["totalMarketToMarketPrice"];
                }
                catch (Exception)
                {
                    throw new DataException("Unable to parse data. " + Utils.JsonSerialize(data));
                }
            }

            //public string Product { get; set; }
            public string Exchange { get; set; }
            //public decimal Price { get; set; }
            //public decimal LastPrice { get; set; }
            public int CollateralQuantity { get; set; }
            //public decimal PNL { get; set; }
            //public decimal ClosePrice { get; set; }
            //public decimal AveragePrice { get; set; }
            //public string TradingSymbol { get; set; }
            //public string ISIN { get; set; }
            //public int Quantity { get; set; }
            //public string markToMarketPrice { get; set; }
            //public string symbolDescription { get; set; }
            //public string holdingsValue { get; set; }
            //public string holdingsQuantity { get; set; }
            //public string sellableQuantity { get; set; }
            //public string totalMarketToMarketPrice { get; set; }
        }

        public struct UserMarginComposite
        {           
            public UserMarginComposite(Dictionary<string, dynamic> data)
            {
                try
                {
                    MarginUtilized = Convert.ToDecimal(data["marginUtilized"]);
                    Net = Convert.ToDecimal(data["netMarginAvailable"]);
                    Collateral = Convert.ToDecimal(data["collateral"]);
                    NetMarginAvailable = Convert.ToDecimal(data["cashAvailable"]);
                    MTM = Convert.ToDecimal(data["MTM"]);
                    UnrealizedMTM = Convert.ToDecimal(data["UnrealizedMTM"]);
                    RealizedMTM = Convert.ToDecimal(data["RealizedMTM"]);
                }
                catch (Exception ex)
                {
                    throw new DataException("Unable to parse data. " + Utils.JsonSerialize(data));
                }
            }

            public decimal MarginUtilized { get; set; }
            public decimal Net { get; set; }
            public decimal Collateral { get; set; }
            public decimal NetMarginAvailable { get; set; }
            public decimal MTM { get; set; }
            public decimal UnrealizedMTM { get; set; }
            public decimal RealizedMTM { get; set; }
        }

        

    }
}
