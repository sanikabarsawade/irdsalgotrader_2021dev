﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRDSAlgoOMS
{
    class HoldingStore
    {
        Dictionary<string, Holding> dictionaryForHoldings;
        public HoldingStore()
        {
            dictionaryForHoldings = new Dictionary<string, Holding>();
        }

        public void AddOrUpdateHoldingInfo(Holding holding)
        {
            try
            {
                string key = holding.TradingSymbol + "_" + holding.Product + "|" + holding.Exchange;
                if (dictionaryForHoldings.ContainsKey(key))
                {
                    dictionaryForHoldings[key] = holding;
                }
                else
                {
                    dictionaryForHoldings.Add(key, holding);
                }
            }
            catch(Exception e)
            {

            }
        }

        public Holding GetHoldingBySymbol(string TradingSymbol,string Product,string Exchange)
        {
            Holding holdingRes = new Holding(); ;
            try
            {
                string key = TradingSymbol + "_" + Product + "|" + Exchange;
                if(dictionaryForHoldings.ContainsKey(key))
                {
                    holdingRes = dictionaryForHoldings[key];
                }
            }
            catch (Exception e)
            {

            }
            return holdingRes;
        }

        public bool GetAllHoldings(out Dictionary<string,Holding>AllHoldings)
        {
            AllHoldings = new Dictionary<string, Holding>();
            bool res = false;
            if(dictionaryForHoldings.Count>0)
            {
                AllHoldings = dictionaryForHoldings;
                res = true;
            }
            return res;
        }

    }
}
