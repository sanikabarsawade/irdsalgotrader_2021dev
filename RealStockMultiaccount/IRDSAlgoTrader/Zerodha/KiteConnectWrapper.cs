﻿using IRDSAlgoOMS.Common;
using IRDSAlgoOMS.GUI;
using IRDSAlgoOMS.Zerodha;
using MySql.Data.MySqlClient;
using SendEmailSMS;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;

namespace IRDSAlgoOMS
{
    public class KiteConnectWrapper
    {
        Thread m_CalculateProfit = null;
        static readonly object _ForceFullyRelogin = new object();
        bool m_bneedToForcefullyLogin = false;
        static bool m_LoginStatus = false;
        string m_futureName = "";
        //23-Jully-2021::Pratiksha::for symbol setting form needs this table name
        public string m_futureNameForTable = "";
        List<string> m_RealTimeSymbols = new List<string>();
        private Kite kite;
        Ticker ticker = null;
        bool m_showGUI = false;
        string m_Title = "";
        bool m_StoreDataInMysql = false;
        bool m_StoreDataInSqlite = false;
        bool m_HistoricalDataInMysqlDB = false;
        double m_overallProfit = 0;
        double m_overallLoss = 0;
        public string m_StartTime = "";
        //string m_EndTime = "";
        string m_TimeLimitToPlaceOrder = "";
        //sanika::8-dec-2020::Added for interval
        int m_Interval = 1;
        int m_BrokerQuantity = 0;
        int m_ManualQuantity = 0;
        bool m_RiskManagmentFlag = false;
        //sanika::3-dec-2020::Added for diff exchanges
        public string m_TimeToExitAllNSEOrders = "";
        string m_TimeToExitAllNFOOrders = "";
        string m_TimeToExitAllMCXOrders = "";
        string m_TimeToExitAllCDSOrders = "";

        //sanika::3-dec-2020::Added for diff exchanges
        DateTime m_StartTimeForPlaceOrder;
        DateTime m_EndTimeForPlaceOrder;
        DateTime m_TimeToExitAllNSEOrder;
        DateTime m_TimeToExitAllNFOOrder;
        DateTime m_TimeToExitAllMCXOrder;
        DateTime m_TimeToExitAllCDSOrder;

        Order latestOrder;
        public ConfigSettings objConfigSettings;
        SQLiteConnection sql_con;
        SQLiteConnection sql_conLotSize;
        SQLiteConnection sql_conData;
        SQLiteCommand sql_cmd;
        SQLiteDataReader sql_read;
        SQLiteTransaction tr;
        SQLiteConnection vol_sql_con;
        string DBName = "kite.db";
        string insFileName = "instruments.csv";
        string getCurrentMonth;
        string getCurrentYear;
        string[] value;
        string findElement = "FUT";
        public SpinLock spinLock = new SpinLock(true);
        bool bGetCurrentPositionError = false;
        private static Logger logger = null;
        int getSymbolLotSize;
        decimal price;
        decimal tickSize;
        DateTime dt = DateTime.Now;
        int getFutSymbolLotSize;
        string getFutSymbol;
        decimal getFutSymboltickSize;
        Dictionary<string, int> futureSymbolAndLotSize = new Dictionary<string, int>();
        Dictionary<string, decimal> dictSymbolwithtickSize = new Dictionary<string, decimal>();
        private string m_UserID;
        string m_path = Directory.GetCurrentDirectory();
        OrderStore m_GlobalOrderStore;
        PositionStore m_GlobalpositionStore;
        HoldingStore m_GlobalholdingStore;
        TickData m_TickData;
        Dictionary<string, string> m_DictionaryForOrderId = new Dictionary<string, string>();
        //IRDS::03-Jul-2020::Jyoti::added for historical data download in mysql
        MySQLConnectZerodha m_Mysql;
        string m_IniFilePath = "";
        List<string> m_ListForSymbols = new List<string>();
        int m_PreviousOrderCounter = 0;
        //Dictionary<int, string> m_ExceptionDictionary = new Dictionary<int, string>();
        public int m_ExceptionCounter = 0;
        public bool m_waitingStatus = false;
        public bool m_isCalculateProfitLoss = true;
        List<string> m_ListOfTableExistOrNot = new List<string>();
        Dictionary<string, int> m_ClosedSymbol = new Dictionary<string, int>();
        public static bool m_isSqrOff = false;
        //sanika::3-dec-2020::Change condition for close orders flag according exchange
        public bool m_isSqrOffNSE = false;
        public bool m_isSqrOffNFO = false;
        public bool m_isSqrOffMCX = false;
        public bool m_isSqrOffCDS = false;
        bool m_isCloseAllNSEOrder = false;
        bool m_isCloseAllNFOOrder = false;
        bool m_isCloseAllMCXOrder = false;
        bool m_isCloseAllCDSOrder = false;
        Dictionary<string, Order> m_Allorders = new Dictionary<string, Order>();
        private static readonly object m_calculateProfitLoss = new object();
        private static readonly object m_getAllOrder = new object();
        double m_TotalProfitLoss = 0;
        Thread m_UpdatingStructures = null;
        Thread m_SendResponse = null;
        Thread m_GetAllSymbolsFromDB = null;
        static NamedPipeClientStream m_NamedPipeClient = null;
        // string ConnString = "server=166.62.28.93;user id=pratiksha;password=Irds2020!;persistsecurityinfo=True;database=AlgoTradingUserDetails;SslMode=none";
        //IRDSPM::Pratiksha::19-09-2020::For new hosting
        // string ConnString = "server=148.66.138.114;user id=irdsalgo;password=Irds2020!;persistsecurityinfo=True;database=ServerLicenseSystemTesting;SslMode=none";
        public static bool m_isFromEXEForBridgeConnection = true;
        public static bool m_isFromEXEForshowGUI = false;
        Dictionary<string, int> DatabaseCreated = new Dictionary<string, int>();
        Dictionary<string, int> DatabaseCreatedMysql = new Dictionary<string, int>();
        List<string> m_InsertQueries = new List<string>();
        List<string> finalSymbolList = new List<string>();
        bool isTickMCXDataDownloader = false;
        APISettingWindowsForm m_ApiSettingObj = null;
        bool m_isConnected = false;
        System.Timers.Timer m_Timer = null;
        bool m_StopThread = false;
        static bool m_StopBridgeThread = false;
        Dictionary<string, string> m_OrderListWithSymbolAndDirection = new Dictionary<string, string>();
        Dictionary<string, int> m_OrderListWithSymbolAndQuantity = new Dictionary<string, int>();
        MySQLConnectZerodha m_MySqlConnection = null;
        bool m_isMysqlconnected = false;
        LocalOrderStructureInfo m_LocalOrderStructureInfo = null;
        public bool m_StartedinstrumentDownloading = false;

        public bool IsRegisterSuccess = false;
        //IRDSPM::Pratiksha::20-10-2020::Progressbar counter
        public int m_CounterProgressBar = 0;
        string DownloadTokenWay = "";
        string m_apiKey = "";
        string m_secretKey = "";
        string m_userid = "";
        string m_pwd = "";
        string m_pin = "";
        public bool isDownloadInstrumentsNow = false;
        SqliteConnectZerodha m_sqliteConnectZerodha = null;
        bool isconnectedToSqlite = false;
        //algotrader.caejfnk6d1lx.ap-south-1.rds.amazonaws.com
        string m_ServerUrl = "algotrader.caejfnk6d1lx.ap-south-1.rds.amazonaws.com";

        Dictionary<string, List<OrdersPlaced>> m_DictOrdersPlaced = new Dictionary<string, List<OrdersPlaced>>();
        public int m_OrdersPerSymbolLimit = 0;
        public int m_OverallOrdersLimit = 0;
        public int m_SameOrdersLimit = 0;

        double m_TimeToUpdatePNLInFile = 0;
        string m_PNLFilePath = Directory.GetCurrentDirectory() + "\\PNL.csv";

        Dictionary<string, double> m_IndividualProfitLoss = new Dictionary<string, double>();
        dynamic userMargin = null;
 //Pratiksha::26-04-2021::For fetching symbolname in list
        public List<string> GetSymbolsKite = new List<string>();
//IRDSPM:::Pratiksha::15-06-2021::For options symbol on zerodha
        public List<string> GetOtionsSymbolsKite = new List<string>();
       
        //IRDSPM::PRatiksha::05-07-2021::For instruemnts symbol
        public List<string> GetInstrumentSymbolsKite = new List<string>();
        //IRDSPM::PRatiksha::22-07-2021::For getting future symbols in list
        public List<string> GetFutureSymbolsKite = new List<string>();

        List<string> finalexchangeList = new List<string>();
        public List<string> GetExchangeKite = new List<string>();
        Thread m_GetAllExchangeFromDB = null;
        Notification m_Notification = null;
        string m_serverUrl = "algotrader.caejfnk6d1lx.ap-south-1.rds.amazonaws.com";
        string m_username = "admin";
        string m_password = "IRDS2020!";
        string m_Dbname = "Zerodha";

        //sanika::20-July-2021::Added static variable to download instruments only once if multiple accounts are set
        static bool m_isDownloadedTokensSuccessfully = false;
        //sanika::21-July-2021::Added to update margin after every 5 min
        double m_TimeToUpdateMargin = 0;
        public KiteConnectWrapper(string iniFilePath)
        {
            //sanika::9-dec-2020::Added condition because 2 instance creating on same file
            if (logger == null)
                logger = Logger.Instance;
            try
            {
                var path = Directory.GetCurrentDirectory();
                string iniFile = path + @"\Configuration\" + iniFilePath;
                m_IniFilePath = iniFile;
                objConfigSettings = new ConfigSettings();
                objConfigSettings.readConfigFile(iniFile);
                //IRDSPM::Pratiksha::27-11-2020::Storing values
                m_apiKey = objConfigSettings.apiKey;
                m_secretKey = objConfigSettings.apiSecret;
                m_userid = objConfigSettings.username;
                m_pwd = objConfigSettings.password;
                m_pin = objConfigSettings.Pin;
                objConfigSettings.readTradingINIFile(path + @"\Configuration\TradingSetting.ini");
                DownloadTokenWay = objConfigSettings.DownloadTokenWay;
                objConfigSettings.readDBSettingINIFile(path + @"\Configuration\DatabaseSettings.ini");
                objConfigSettings.readCredentailsForHistoricalData(path + @"\Configuration\HistoricalDataSetting.ini");
                m_UserID = objConfigSettings.username;
                logger.LogMessage("SetSessionExpiryHook successful.", MessageType.Informational);
                m_GlobalpositionStore = new PositionStore();
                m_GlobalOrderStore = new OrderStore();
                m_GlobalholdingStore = new HoldingStore();
                m_TickData = new TickData();
                m_LocalOrderStructureInfo = new LocalOrderStructureInfo();
                m_Notification = new Notification();
                //IRDSP:Pratiksha::07-09-2020::Read flag from settings
                objConfigSettings.ReadSettingConfigFile(path + @"\Configuration\Setting.ini");
                m_showGUI = objConfigSettings.isShowGUI;
                //IRDSPM::Pratiksha::16-02-2021::For manual login title changes
                m_Title = objConfigSettings.m_Title;
                m_StoreDataInMysql = objConfigSettings.storeDataMysqlDB;
                m_StoreDataInSqlite = objConfigSettings.storeDataSqliteDB;
                m_RealTimeSymbols = objConfigSettings.Symbol;
                m_overallLoss = objConfigSettings.m_overallLoss;
                m_overallProfit = objConfigSettings.m_overallProfit;
                m_isCalculateProfitLoss = objConfigSettings.isCalculateProfitLoss;
                m_StartTime = objConfigSettings.StartTimeNSE;
                m_HistoricalDataInMysqlDB = objConfigSettings.historicalDataInMysqlDB;
                //m_EndTime = objConfigSettings.EndTime;
                m_TimeLimitToPlaceOrder = objConfigSettings.TimeLimitToPlaceOrder;
                //sanika::3-dec-2020::Added for different exchanges
                m_TimeToExitAllNSEOrders = objConfigSettings.ExitTimeNSE;
                m_TimeToExitAllNFOOrders = objConfigSettings.ExitTimeNFO;
                m_TimeToExitAllMCXOrders = objConfigSettings.ExitTimeMCX;
                m_TimeToExitAllCDSOrders = objConfigSettings.ExitTimeCDS;

                m_StartTimeForPlaceOrder = DateTime.ParseExact(m_StartTime, "HH:mm:ss", null);
                m_EndTimeForPlaceOrder = DateTime.ParseExact(m_TimeLimitToPlaceOrder, "HH:mm:ss", null);

                //sanika::3-dec-2020::Added for different exchanges
                m_TimeToExitAllNSEOrder = DateTime.ParseExact(m_TimeToExitAllNSEOrders, "HH:mm:ss", null);
                m_TimeToExitAllNFOOrder = DateTime.ParseExact(m_TimeToExitAllNFOOrders, "HH:mm:ss", null);
                m_TimeToExitAllMCXOrder = DateTime.ParseExact(m_TimeToExitAllMCXOrders, "HH:mm:ss", null);
                m_TimeToExitAllCDSOrder = DateTime.ParseExact(m_TimeToExitAllCDSOrders, "HH:mm:ss", null);

                isTickMCXDataDownloader = objConfigSettings.isTickMCXDataDownloader;

                //sanika::8-dec-2020::Added for interval
                m_Interval = objConfigSettings.interval;
                m_BrokerQuantity = Convert.ToInt32(objConfigSettings.brokerQuantity);
                m_ManualQuantity = Convert.ToInt32(objConfigSettings.ManualQuantity);
                m_RiskManagmentFlag = objConfigSettings.isRiskmanagement;
            }
            catch (Exception e)
            {
                logger.LogMessage("KiteConnectWrapper : Exception Error " + e.Message, MessageType.Informational);
            }
        }


        //IRDSPM::Pratiksha::18-09-2020::For Open new api setting form     
        public bool Connect()
        {
            try
            {
                m_waitingStatus = true;
                //IRDSPM::Pratiksha::27-11-2020::For XXXXXX check
                if (m_apiKey.Contains("XXXXXX") || m_userid.Contains("XXXXXX") || m_pwd.Contains("XXXXXX") || m_pin.Contains("XXXXXX") || m_secretKey.Contains("XXXXXX"))
                {
                    //sanika::8-sep-2020::Added check for showing GUI
                    if (m_showGUI == true)
                    {
                        //IRDSPM::PRatiksha::01-09-2020::If userId XXXXXX then open brokr form                        
                        //IRDSPM::Pratiksha::18-09-2020::For Open new api setting form
                        if (m_ApiSettingObj == null)
                        {
                            m_ApiSettingObj = new APISettingWindowsForm(this, m_LoginStatus.ToString(), m_Title, m_userid);
                        }
                        m_ApiSettingObj.LoadKiteConnectObject(this, m_LoginStatus.ToString());

                        m_ApiSettingObj.ShowDialog();
                        //IRDSPM::Pratiksha::27-11-2020::Read values and check values not contain XXXXXX
                        objConfigSettings.readConfigFile(m_IniFilePath);
                        if (objConfigSettings.apiKey != "XXXXXX" || objConfigSettings.apiSecret != "XXXXXX" || objConfigSettings.username != "XXXXXX" || objConfigSettings.password != "XXXXXX" || objConfigSettings.Pin != "XXXXXX")
                        {
                            logger.LogMessage("Connect : Read values properly", MessageType.Informational);
                        }
                        else
                        {
                            logger.LogMessage("Connect : Invalid ini values", MessageType.Informational);
                            return false;
                        }
                    }
                    else if (m_isFromEXEForshowGUI)
                    {//IRDSPM::Pratiksha::06-02-2021::Added change
                        m_ApiSettingObj = null;
                        //IRDSPM::PRatiksha::01-09-2020::If userId XXXXXX then open brokr form                       
                        if (m_ApiSettingObj == null)
                        {
                            m_ApiSettingObj = new APISettingWindowsForm(this, m_LoginStatus.ToString(), m_Title, m_userid);
                        }
                        m_ApiSettingObj.LoadKiteConnectObject(this, m_LoginStatus.ToString());

                        m_ApiSettingObj.ShowDialog();
                        logger.LogMessage("Connect : Invalid ini values", MessageType.Informational);
                    }
                }
                //IRDSPM::Pratiksha::06-02-2021::If XXXXXX then do not open webbrowser
                if (objConfigSettings.apiKey != "XXXXXX" || objConfigSettings.apiSecret != "XXXXXX" || objConfigSettings.username != "XXXXXX" || objConfigSettings.password != "XXXXXX" || objConfigSettings.Pin != "XXXXXX")
                {

                    kite = new Kite(objConfigSettings.apiKey, objConfigSettings, Debug: true);
                    //IRDSPM::PRatiksha::16-02-2021::For set icon to manual login form
                    kite.SetTitle(m_Title);
                    kite.SetSessionExpiryHook(kite.OnTokenExpire);
                    kite.initializeLoggerObject(logger, m_GlobalOrderStore, m_TickData);

                    if (!getCreadentials())
                    {
                        logger.LogMessage("Connect : Going for login", MessageType.Informational);
                        int loginCounter = 0;
                        //sanika::5-Apr-2021::tried login 2 times if request token exeception occur
                        while (loginCounter <= 1)
                        {
                            kite.initSeesion();
                            if (kite.LoginFlag)
                            {
                                m_LoginStatus = true;
                                WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " Connected");
                                m_ExceptionCounter++;
                                logger.LogMessage("Connect : initSeesion successful.", MessageType.Informational);
                                loginCounter = 3;
                            }
                            else
                            {
                                logger.LogMessage("Connect : initSeesion Unsuccessful.", MessageType.Informational);
                                m_LoginStatus = false;
                                if (kite.m_ExceptionError.Contains("10") && kite.m_ExceptionError.Contains("minimum"))
                                {
                                    WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + kite.m_ExceptionError + " Trying to login again");
                                    m_ExceptionCounter++;
                                    if (kite.m_ExceptionError != "")
                                        System.Windows.Forms.MessageBox.Show(new Form { TopMost = true }, kite.m_ExceptionError + " Trying to login again");
                                }
                                else
                                {
                                    WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + kite.m_ExceptionError);
                                    m_ExceptionCounter++;
                                    if (kite.m_ExceptionError != "")
                                        System.Windows.Forms.MessageBox.Show(new Form { TopMost = true }, kite.m_ExceptionError);
                                }
                                loginCounter++;
                            }
                        }

                        //sanika::18-sep-2020::added to download instrument
                        if (m_LoginStatus)
                        {
                            if (DownloadTokenWay.ToLower() == "auto")
                            {
                                //sanika::20-July-2021::Added condition to download instruments only once if multiple accounts are set
                                if (!CheckDownloadInstrumentCSV() && m_isDownloadedTokensSuccessfully == false)
                                {
                                    isDownloadInstrumentsNow = true;
                                    m_StartedinstrumentDownloading = true;
                                    m_isDownloadedTokensSuccessfully = true;
                                    Thread.Sleep(2000);
                                    IntrumentsCSVToDB();
                                    m_StartedinstrumentDownloading = false;
                                    WriteDateIntoFile();
                                    //IRDSPM::Pratiksha::20-10-2020::Progressbar counter
                                    m_CounterProgressBar = 101;

                                }
                            }
                        }
                    }
                    else
                    {
                        logger.LogMessage("Connect : Credential file present", MessageType.Informational);

                        int counter = 0;
                        while (counter < 5)
                        {
                            RequestNUpdatePositioInfo();
                            RequestNUpdateOrderInfo();
                            if (m_bneedToForcefullyLogin == true)
                            {
                                lock (_ForceFullyRelogin)
                                {
                                    logger.LogMessage("Connect : Going for Forcefully login", MessageType.Informational);
                                    ForceFulReLogin();
                                    m_LoginStatus = true;
                                    WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " Connected");
                                    m_ExceptionCounter++;
                                    m_bneedToForcefullyLogin = false;
                                    Thread.Sleep(1000);
                                }
                            }
                            else
                            {
                                m_LoginStatus = true;
                                WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " Connected");
                                m_ExceptionCounter++;
                                break;
                            }
                            counter++;
                        }
                        //20-July-2021: sandip: call after session creates.
                        if (m_LoginStatus)
                        {
                            ValidateTokenFileDatabase();
                            kite.initTicker();

                            logger.LogMessage("initTicker successful.", MessageType.Informational);

                        }

                    }
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine("Exception is " + e.Message);
                logger.LogMessage("Connect : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            //20-July-2021: sandip: call after session initialize
            m_futureName = FetchTableName();
            m_futureNameForTable = m_futureName;
            if (m_LoginStatus)
            {
                kite.SetAccessToken(kite.accessToken);
                logger.LogMessage("Connect : SetAccessToken successful.", MessageType.Informational);
                sql_con = null;
                sql_conData = null;
                if (ticker == null)
                {
                ticker = new Ticker();
                }
                kite.initTicker();
                logger.LogMessage("Connect : initTicker successful.", MessageType.Informational);
                LoadSymbolWithLotandTickInDict();
                RequestNUpdateHoldings();
                if (m_CalculateProfit == null)
                {
                    m_CalculateProfit = new Thread(() => CalculateProfitThread());
                    m_CalculateProfit.Start();
                }

                //sanika::5-sep-2020::Started thread 
                if (m_UpdatingStructures == null)
                {
                    m_UpdatingStructures = new Thread(() => UpdateStructures());
                    m_UpdatingStructures.Start();
                }

                if (m_SendResponse == null)
                {
                    m_SendResponse = new Thread(() => SendResponse());
                    m_SendResponse.Start();
                }

                //IRDSPM::Pratiksha::14-07-2021::Sometimes in thread all symbol not getting
                GetAllSymbolsFromDBdata();

                if (m_Timer == null)
                {
                m_Timer = new System.Timers.Timer();
                }
                m_Timer.Elapsed += _OnTimerExecute;
                m_Timer.Interval = 1000; // checks connection every second
                m_Timer.Enabled = true;

            }
            else
            {
                logger.LogMessage("Connect : Value of m_LoginStatus is false", MessageType.Informational);
            }
            logger.LogMessage("Connect : KiteConnectWrapper finished.", MessageType.Informational);

            return m_LoginStatus;
        }

        //20-July-2021: sandip: added function to check instrument conditoons.
        public void ValidateTokenFileDatabase()
        {
            //sanika::18-sep-2020::added to download instrument                   
            {
                if (DownloadTokenWay.ToLower() == "auto")
        {
                    //sanika::20-July-2021::Added condition to download instruments only once if multiple accounts are set
                    if (!CheckDownloadInstrumentCSV() && m_isDownloadedTokensSuccessfully == false)
            {
                        isDownloadInstrumentsNow = true;
                        m_isDownloadedTokensSuccessfully = true;
                        logger.LogMessage("Connect : Going to download instruments", MessageType.Informational);
                m_StartedinstrumentDownloading = true;
                Thread.Sleep(2000);
                        DateTime dt = DateTime.Now;
                IntrumentsCSVToDB();
                        DateTime dt1 = DateTime.Now;
                        double diff = dt.Subtract(dt1).TotalMilliseconds;
                        File.AppendAllText("time.txt", "Time to download instruments " + diff);
                m_StartedinstrumentDownloading = false;
                WriteDateIntoFile();
                        //IRDSPM::Pratiksha::20-10-2020::Progressbar counter
                        m_CounterProgressBar = 101;
                    }
            }
            }
        }

        public void UpdateStructures()
        {
            //sanika::29-sep-2020::added stop thread condition
            while (true && m_StopThread == false)
            {
                int counter = 0;
                while (counter <= 100 && kite.GetOrderUpdateStatus() == false)
                {
                    Thread.Sleep(10);
                    counter++;
                }
                Thread.Sleep(100);
                try
                {
                    Trace.WriteLine(">>>>>>>>>>>>Start>>>>>>>>>>>>>>>>");
                    RequestNUpdatePositioInfo();
                    Thread.Sleep(100);
                    RequestNUpdateOrderInfo();
                    kite.SetOrderUpdateStatus(false);
                    Trace.WriteLine(">>>>>>>>>>>>Stoppp>>>>>>>>>>>>>>>>");
                }
                catch (Exception e)
                {
                    logger.LogMessage("UpdateStructures : Exception Error Message = " + e.Message, MessageType.Exception);
                }

            }
            logger.LogMessage("UpdateStructures : Stopped thread m_StopThread " + m_StopThread, MessageType.Informational);
        }

        public void ChangeUserID(string userid)
        {
            m_UserID = userid;
            objConfigSettings.readConfigFile(m_IniFilePath);
            m_apiKey = objConfigSettings.apiKey;//sanika::2-dec-2020::Reload the all ini values
            m_secretKey = objConfigSettings.apiSecret;
            m_userid = objConfigSettings.username;
            m_pwd = objConfigSettings.password;
            m_pin = objConfigSettings.Pin;
            m_UserID = objConfigSettings.username;
        }

        public string GetStartTime()
        {
            return m_StartTime;
        }

        public string GetEndTime()
        {
            return "";
            // return m_EndTime;
        }

        public string GetTimeLimitToPlaceOrder()
        {
            return m_TimeLimitToPlaceOrder;
        }

        public string GetTimeLimitToExistOrder()
        {
            return m_TimeToExitAllNSEOrders;
        }

        public double GetOverAllLoss()
        {
            return m_overallLoss;
        }

        public double GetOverAllProfit()
        {
            return m_overallProfit;
        }

        public string GetFilePath()
        {
            return m_IniFilePath;
        }

        public bool GetShowGUIFlag()
        {
            return m_showGUI;
        }

        public bool GetRealTimeSymbols(out List<string> symbolsList)
        {
            symbolsList = m_RealTimeSymbols;
            return true;
        }

        public dynamic GetMargin()
        {
            //IRDS::Jyoti::08-09-2020::Samco Integration changes
            UserMargin margins = kite.GetMargins(Constants.MARGIN_EQUITY);
            var respDict = new Dictionary<string, dynamic>();
            respDict.Add("Utilised", margins.Utilised.Debits);
            respDict.Add("Available", margins.Available.Cash);
            respDict.Add("Net", margins.Net);
            return respDict;

        }

        //sanika::7-sep-2020::Changed calculate profitloss method 
        public void CalculateProfitThread()
        {
            //sanika::29-sep-2020::added stop thread condition
            while (true && m_StopThread == false)
            {
                Thread.Sleep(100);
                try
                {
                    CalculateProfitLossForOpenPosition();
                    //sanika::9-oct-2020::converted profitLoss into absolute
                    double profitLoss = GetTotalMTM();
                    double overallProfit = m_overallProfit;
                    //sanika::9-oct-2020::converted overall loss into absolute
                    double overallLoss = Math.Abs(m_overallLoss);
                    if (m_isCalculateProfitLoss)
                    {
                        if (profitLoss >= overallProfit)
                        {
                            logger.LogMessage("#### Profit Booked calculateProfit : profitLoss " + profitLoss + " overallProfit " + overallProfit + " overallLoss " + overallLoss + "-- Condition true", MessageType.Informational);
                            CloseAllOpenOrders();
                            //sanika::after overall profit/loss hits - pending order remains as it is
                            CancelAllPendingOrders();
                            logger.LogMessage("calculateProfit : closed all position successfully", MessageType.Informational);
                            WriteGUIlogs(m_ExceptionCounter, DateTime.Now + "Total PNL = " + profitLoss + "Overall profit hitted!!");
                            m_ExceptionCounter++;
                            m_isSqrOff = true;
                            logger.LogMessage("calculateProfit : m_isSqrOff set to true overall profit/loss condition matched", MessageType.Informational);

                        }
                        else if (profitLoss < 0 && Math.Abs(profitLoss) > overallLoss)
                        {
                            logger.LogMessage("#### Loss Booked calculateProfit : profitLoss " + profitLoss + " overallProfit " + overallProfit + " overallLoss " + overallLoss + "-- Condition true", MessageType.Informational);
                            CloseAllOpenOrders();
                            //sanika::after overall profit/loss hits - pending order remains as it is
                            CancelAllPendingOrders();
                            logger.LogMessage("calculateProfit : closed all position successfully", MessageType.Informational);
                            WriteGUIlogs(m_ExceptionCounter, DateTime.Now + "Total PNL = " + profitLoss + " Overall loss hitted!!");
                            m_ExceptionCounter++;
                            m_isSqrOff = true;
                            logger.LogMessage("calculateProfit : m_isSqrOff set to true overall profit/loss condition matched", MessageType.Informational);

                        }
                    }
                    CheckExistTimeToCloseAllOrders();
                    Thread.Sleep(300);
                }
                catch (Exception e)
                {
                    logger.LogMessage("CalculateProfit : Exception Error Message : " + e.Message, MessageType.Exception);
                }

                UpdatePNLInFile();
                //sanika::21-July-2021::Added to update margin after every 1 min
                UpdateMargin();
            }
            logger.LogMessage("calculateProfit : Stopped thread m_StopThread " + m_StopThread, MessageType.Informational);
        }
        //sanika::3-dec-2020::Added to close orders according to exchanges
        public void CheckExistTimeToCloseAllOrders()
        {
            //sanika::9-oct-2020::move time code from m_isCalculateProfitLoss flag condition
            //close all orders for particular time
            if (CheckTimeToExistAllNSEOrder() && m_isCloseAllNSEOrder == false) //sanika::15-oct-2020::add flag condition to ignore toast many times 
            {
                logger.LogMessage("CheckToCloseOrders : Closed all orders bacause exit time ", MessageType.Informational);
                //sanika::26-Nov-2020::Call cancel order function to cancel pending order
                CancelAllPendingOrders(Constants.EXCHANGE_NSE);
                logger.LogMessage("CheckToCloseOrders : cancel all pending orders", MessageType.Informational);
                if (CloseAllOpenMISOrders(Constants.EXCHANGE_NSE)) //sanika::12-Nov-2020::Added close mis order seperate function for ajay sir
                {
                    m_isCloseAllNSEOrder = true;
                    m_isSqrOffNSE = true;
                    logger.LogMessage("CheckToCloseOrders : m_isSqrOff set to true exit time condition matched for NSE Symbols", MessageType.Informational);
                    WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " Exit time condition executed for NSE Symbols!!");
                    m_ExceptionCounter++;
                }
                else
                {
                    logger.LogMessage("CheckToCloseOrders : Not able to close all orders!!", MessageType.Informational);
                }
            }

            if (CheckTimeToExistAllNFOOrder() && m_isCloseAllNFOOrder == false) //sanika::15-oct-2020::add flag condition to ignore toast many times 
            {
                logger.LogMessage("CheckToCloseOrders : Closed all orders bacause exit time ", MessageType.Informational);
                //sanika::26-Nov-2020::Call cancel order function to cancel pending order
                CancelAllPendingOrders(Constants.EXCHANGE_NFO);
                logger.LogMessage("CheckToCloseOrders : cancel all pending orders", MessageType.Informational);
                if (CloseAllOpenMISOrders(Constants.EXCHANGE_NFO)) //sanika::12-Nov-2020::Added close mis order seperate function for ajay sir
                {
                    m_isCloseAllNFOOrder = true;
                    m_isSqrOffNFO = true;
                    logger.LogMessage("CheckToCloseOrders : m_isSqrOff set to true exit time condition matched for NFO symbols", MessageType.Informational);
                    WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " Exit time condition executed for NFO symbols!!");
                    m_ExceptionCounter++;
                }
                else
                {
                    logger.LogMessage("CheckToCloseOrders : Not able to close all orders!!", MessageType.Informational);
                }
            }

            if (CheckTimeToExistAllMCXOrder() && m_isCloseAllMCXOrder == false) //sanika::15-oct-2020::add flag condition to ignore toast many times 
            {
                logger.LogMessage("CheckToCloseOrders : Closed all orders bacause exit time ", MessageType.Informational);
                //sanika::26-Nov-2020::Call cancel order function to cancel pending order
                CancelAllPendingOrders(Constants.EXCHANGE_MCX);
                logger.LogMessage("CheckToCloseOrders : cancel all pending orders", MessageType.Informational);
                if (CloseAllOpenMISOrders(Constants.EXCHANGE_MCX)) //sanika::12-Nov-2020::Added close mis order seperate function for ajay sir
                {
                    m_isCloseAllMCXOrder = true;
                    m_isSqrOffMCX = true;
                    logger.LogMessage("CheckToCloseOrders : m_isSqrOff set to true exit time condition matched for MCX Symbols", MessageType.Informational);
                    WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " Exit time condition executed for MCX Symbols!!");
                    m_ExceptionCounter++;
                }
                else
                {
                    logger.LogMessage("CheckToCloseOrders : Not able to close all orders!!", MessageType.Informational);
                }
            }

            if (CheckTimeToExistAllCDSOrder() && m_isCloseAllCDSOrder == false) //sanika::15-oct-2020::add flag condition to ignore toast many times 
            {
                logger.LogMessage("CheckToCloseOrders : Closed all orders bacause exit time ", MessageType.Informational);
                //sanika::26-Nov-2020::Call cancel order function to cancel pending order
                CancelAllPendingOrders(Constants.EXCHANGE_CDS);
                logger.LogMessage("CheckToCloseOrders : cancel all pending orders", MessageType.Informational);
                if (CloseAllOpenMISOrders(Constants.EXCHANGE_CDS)) //sanika::12-Nov-2020::Added close mis order seperate function for ajay sir
                {
                    m_isCloseAllCDSOrder = true;
                    m_isSqrOffCDS = true;
                    logger.LogMessage("CheckToCloseOrders : m_isSqrOff set to true exit time condition matched for CDS symbols", MessageType.Informational);
                    WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " Exit time condition executed for CDS Symbols!!");
                    m_ExceptionCounter++;
                }
                else
                {
                    logger.LogMessage("CheckToCloseOrders : Not able to close all orders!!", MessageType.Informational);
                }
            }
        }

        public bool GetConnectionStatus(bool fromEXE = true)
        {
            if (m_LoginStatus)
            {
                m_isFromEXEForBridgeConnection = fromEXE;
                return true;
            }
            return false;
        }

        public string GetUserID()
        {
            return m_UserID;
        }

        public void RollOver(string TradingSymbol, string Exchange, string Product)
        {
            try
            {
                string symbol = TradingSymbol;
                if (Exchange != Constants.EXCHANGE_NSE)
                {
                    symbol += FetchTableName();
                }
                DateTime LastThusdaydateTime = GetRollOverDate();
                WriteUniquelogs(TradingSymbol + " " + m_UserID, " RollOver : LastThusdaydateTime = " + LastThusdaydateTime, MessageType.Informational);
                DateTime todayDate = DateTime.ParseExact(DateTime.Now.ToString("dd-MM-yyyy"), "dd-MM-yyyy", null);
                if (LastThusdaydateTime == todayDate)
                {
                    double openPrice = GetOpenPostionPrice(symbol, Exchange, Product);
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, " RollOver : openPrice = " + openPrice, MessageType.Informational);
                    if (openPrice != 0)
                    {
                        string direction = GetOpenPostionDirection(symbol, Exchange, Product);
                        int quantity = GetOpenPostionQuantity(symbol, Exchange, Product);
                        WriteUniquelogs(TradingSymbol + " " + m_UserID, " RollOver : direction = " + direction + "quantity = " + quantity, MessageType.Informational);
                        if (CloseOrder(symbol, Exchange, Product))
                        {
                            string name = GetNextMonthSymbolName();
                            if (Exchange != Constants.EXCHANGE_NSE)
                            {
                                int Lotsize = GetMonthLotSize(TradingSymbol, Exchange, name);
                                if (Lotsize > 0)
                                {
                                    quantity = Lotsize;
                                }
                            }

                            string newSymbolName = TradingSymbol;
                            if (Exchange != Constants.EXCHANGE_NSE)
                            {
                                newSymbolName += name;
                            }
                            WriteUniquelogs(TradingSymbol + " " + m_UserID, " RollOver : Closed previous order and new symbol name = " + newSymbolName, MessageType.Informational);
                            WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + "Closed previous order and new symbol name = " + newSymbolName);
                            m_ExceptionCounter++;
                            string orderId = PlaceOrder(Exchange, newSymbolName, direction, quantity, Constants.ORDER_TYPE_MARKET, Product: Product);
                            if (orderId != "NA")
                            {
                                WriteUniquelogs(TradingSymbol + " " + m_UserID, "RollOver : order placed successfully ", MessageType.Informational);
                                WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + TradingSymbol + " Order Placed successfully!! " + orderId);
                                m_ExceptionCounter++;
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, " RollOver : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        public string GetNextMonthSymbolName()
        {
            string name = "";
            try
            {
                string year = DateTime.Now.ToString("yy");
                string currentMonth = DateTime.Now.ToString("MMM");
                if (currentMonth.Equals("Dec"))
                {
                    int yr = Convert.ToInt32(year) + 1;
                    year = yr.ToString();
                }
                name = year + DateTime.Now.AddMonths(1).ToString("MMM").ToUpper() + "FUT";
            }
            catch (Exception e)
            {
                logger.LogMessage("GetNextMonthSymbolName : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            logger.LogMessage(" GetNextMonthSymbolName : name = " + name, MessageType.Informational);
            return name;
        }

        public DateTime GetRollOverDate()
        {
            DateTime date = DateTime.Now;
            int year = DateTime.Now.Year;
            int month = DateTime.Now.Month;
            try
            {
                var tmpDate = new DateTime(year, month, 1).AddMonths(1).AddDays(-1);
                while (tmpDate.DayOfWeek != DayOfWeek.Thursday)
                {
                    tmpDate = tmpDate.AddDays(-1);
                }
                date = tmpDate;
            }
            catch (Exception e)
            {
                logger.LogMessage("GetRollOverDate : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return date;
        }

        public void WriteDateIntoFile()
        {
            try
            {
                string currentDate = DateTime.Now.ToString("M-d-yyyy");
                var path_File = Directory.GetCurrentDirectory();
                string FilePath = path_File + @"\Configuration\" + "instrument.txt";
                string line = currentDate;
                using (StreamWriter streamWriter = new StreamWriter(FilePath, false))
                {
                    streamWriter.WriteLine(line);
                }
                //IRDSPM::Pratiksha::20-10-2020::Progressbar counter
                m_CounterProgressBar = 100;
                Thread.Sleep(2000);
            }
            catch (Exception e)
            {
                logger.LogMessage("WriteDateIntoFile : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        public bool CheckDownloadInstrumentCSV()
        {
            try
            {
                var path_File = Directory.GetCurrentDirectory();
                string FilePath = path_File + @"\Configuration\" + "instrument.txt";
                if (!File.Exists(FilePath))
                {
                    logger.LogMessage("CheckDownloadInstrumentCSV : instrument.txt file not exist", MessageType.Informational);
                    return false;
                }
                else
                {
                    string lastLine = System.IO.File.ReadLines(FilePath).Last();
                    string currentDate = DateTime.Now.ToString("M-d-yyyy");
                    if (lastLine == currentDate)
                    {
                        return true;
                    }
                }
            }
            catch (Exception e)
            {
                logger.LogMessage("CheckDownloadInstrumentCSV : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return false;
        }

        public bool getCreadentials()
        {
            try
            {
                var path_File = Directory.GetCurrentDirectory();
                string FilePath = path_File + @"\Configuration\" + "credentials_" + m_UserID + ".txt";
                if (!File.Exists(FilePath))
                {
                    logger.LogMessage("getCreadentials : credentials.txt file does not exist", MessageType.Informational);
                    return false;
                }
                else
                {
                    string lastLine = System.IO.File.ReadLines(FilePath).Last();
                    string[] splittedString = lastLine.Split(' ');
                    string currentDate = DateTime.Now.ToString("M-d-yyyy");
                    if (splittedString[0] == currentDate && splittedString[1] == m_UserID)
                    {
                        kite.accessToken = splittedString[2];
                        kite.MyPublicToken = splittedString[3];
                        kite.LoginFlag = true;
                        return true;
                    }
                }
            }
            catch (Exception e)
            {
                logger.LogMessage("getCreadentials : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return false;
        }

        public void StopTimer()
        {
            try
            {
                if (m_Timer != null)
                {
                    //sanika::23-Nov-2020::First close then timer then dispose
                    m_Timer.Stop();
                    m_Timer.Close();
                    m_Timer.Dispose();
                    logger.LogMessage("StopTimer : Timer stopped", MessageType.Informational);
                }
                Thread.Sleep(500);
            }
            catch (Exception e)
            {
                logger.LogMessage("StopTimer : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        //sanika::29-sep-2020::changed method to stop thread
        public bool StopThread()
        {
            try
            {
                StopTimer();
                m_StopThread = true;
                m_StopBridgeThread = true;
                Thread.Sleep(500);
                if (kite != null)//sanika::2-Dec-2020::Added condition without login closed then gives exception
                    kite.StopThread();
                return true;
            }
            catch (Exception e)
            {
                logger.LogMessage("StopThread : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return false;
        }

        public KiteConnectWrapper()
        {
        }

        public bool IntrumentsCSVToDB()
        {
            WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " Downloading Instrument Tokens!!");
            m_ExceptionCounter++;
            bool result = false;
            logger.LogMessage("### Started for writing csv = " + DateTime.Now, MessageType.Informational);
            var path_DB = Directory.GetCurrentDirectory();
            string directoryDBName = path_DB + "\\" + "Database";

            if (!Directory.Exists(directoryDBName))
                Directory.CreateDirectory(directoryDBName);

            string DBFilePath = directoryDBName + "\\" + DBName;

            var path = Directory.GetCurrentDirectory();
            string directoryName = path + "\\" + "InstrumentList";

            if (!Directory.Exists(directoryName))
                Directory.CreateDirectory(directoryName);

            string insFilePath = directoryName + "\\" + insFileName;

            if (File.Exists(insFilePath))
            {
                //Delete old instrument file
                File.Delete(insFilePath);
                logger.LogMessage("Previous Instruments.csv file is deleted", MessageType.Informational);
                //create a new instrument file  
                //sanika::20-nov-2020::added to download data
                DownloadInstrumentTokens(insFilePath);
            }
            else
            {
                //create a new file
                //sanika::20-nov-2020::added to download data
                DownloadInstrumentTokens(insFilePath);
            }
            logger.LogMessage("### End for writing csv = " + DateTime.Now, MessageType.Informational);
            result = FetchInstrumentDetailsFromCSV();
            LoadSymbolWithLotandTickInDict();
            //m_ExceptionDictionary.Add(m_ExceptionCounter, DateTime.Now + " Instrument Tokens Downloading Completed!!");
            WriteGUIlogs(m_ExceptionCounter, DateTime.Now + "  Instrument Tokens Downloading Completed!!");
            m_ExceptionCounter++;
            return result;
        }

        //sanika::20-nov-2020::added to download data
        public void DownloadInstrumentTokens(string insFilePath)
        {
            try
            {
                if (objConfigSettings.CheckNSE == true)
                {
                    List<Instrument> instruments = kite.GetInstruments(Constants.EXCHANGE_NSE);
                    String csv = String.Join(Environment.NewLine, instruments.Select(d => d.InstrumentToken + "," + d.ExchangeToken + "," + d.TradingSymbol + "," + d.Name + "," + d.LastPrice + "," + d.Expiry + "," + d.Strike + "," + d.TickSize + "," + d.LotSize + "," + d.InstrumentType + "," + d.Segment + "," + d.Exchange));

                    using (StreamWriter streamWriter = new StreamWriter(insFilePath, true))
                    {
                        streamWriter.WriteLine(csv);
                        streamWriter.Close();
                    }
                }
                if (objConfigSettings.CheckNFO == true)
                {
                    List<Instrument> instruments = kite.GetInstruments(Constants.EXCHANGE_NFO);
                    String csv = String.Join(Environment.NewLine, instruments.Select(d => d.InstrumentToken + "," + d.ExchangeToken + "," + d.TradingSymbol + "," + d.Name + "," + d.LastPrice + "," + d.Expiry + "," + d.Strike + "," + d.TickSize + "," + d.LotSize + "," + d.InstrumentType + "," + d.Segment + "," + d.Exchange));

                    using (StreamWriter streamWriter = new StreamWriter(insFilePath, true))
                    {
                        streamWriter.WriteLine(csv);
                        streamWriter.Close();
                    }
                }
                if (objConfigSettings.CheckOptions == true)
                {
                    List<Instrument> instruments = kite.GetInstruments(Constants.EXCHANGE_NFO);
                    String csv = String.Join(Environment.NewLine, instruments.Select(d => d.InstrumentToken + "," + d.ExchangeToken + "," + d.TradingSymbol + "," + d.Name + "," + d.LastPrice + "," + d.Expiry + "," + d.Strike + "," + d.TickSize + "," + d.LotSize + "," + d.InstrumentType + "," + d.Segment + "," + d.Exchange));

                    using (StreamWriter streamWriter = new StreamWriter(insFilePath, true))
                    {
                        streamWriter.WriteLine(csv);
                        streamWriter.Close();
                    }
                }
                if (objConfigSettings.CheckMCX == true)
                {
                    List<Instrument> instruments = kite.GetInstruments(Constants.EXCHANGE_MCX);
                    String csv = String.Join(Environment.NewLine, instruments.Select(d => d.InstrumentToken + "," + d.ExchangeToken + "," + d.TradingSymbol + "," + d.Name + "," + d.LastPrice + "," + d.Expiry + "," + d.Strike + "," + d.TickSize + "," + d.LotSize + "," + d.InstrumentType + "," + d.Segment + "," + d.Exchange));

                    using (StreamWriter streamWriter = new StreamWriter(insFilePath, true))
                    {
                        streamWriter.WriteLine(csv);
                        streamWriter.Close();
                    }
                }
                //Pratiksha::01-12-2020:: For fetching CDS data
                if (objConfigSettings.CheckCDS == true)
                {
                    List<Instrument> instruments = kite.GetInstruments(Constants.EXCHANGE_CDS);
                    String csv = String.Join(Environment.NewLine, instruments.Select(d => d.InstrumentToken + "," + d.ExchangeToken + "," + d.TradingSymbol + "," + d.Name + "," + d.LastPrice + "," + d.Expiry + "," + d.Strike + "," + d.TickSize + "," + d.LotSize + "," + d.InstrumentType + "," + d.Segment + "," + d.Exchange));

                    using (StreamWriter streamWriter = new StreamWriter(insFilePath, true))
                    {
                        streamWriter.WriteLine(csv);
                        streamWriter.Close();
                    }
                }
            }
            catch (Exception e)
            {
                logger.LogMessage("DownloadInstrumentTokens : Exception Error Message : " + e.Message, MessageType.Exception);
            }
        }

        public bool GetLatestOrderDetails(string TradingSymbol, string Exchange, string TransactionType, string orderStatus, out Order latestOrder)
        {
            Order order = new Order();
            try
            {
                List<Order> orderInfo = m_GlobalOrderStore.GetOrderbySymbol(TradingSymbol, Exchange);
                if (orderInfo.Count == 0)
                {
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, " GetLatestOrderDetails : order info count is zero", MessageType.Informational);
                    latestOrder = order;
                    return false;
                }
                for (int i = 0; i < orderInfo.Count(); i++)
                {
                    order = orderInfo[i];
                    if (order.TransactionType == TransactionType)
                    {
                        if (order.Status == orderStatus)
                        {
                            break;
                        }
                    }
                }
                latestOrder = order;
                return true;
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, " GetLatestOrderDetails : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            latestOrder = order;
            return false;
        }

        public bool ModifyLimitOrder(string TradingSymbol, string TransactionType, decimal Price, string Exchange = Constants.EXCHANGE_NSE, string Product = Constants.PRODUCT_MIS, string Variety = Constants.VARIETY_REGULAR)
        {
            bool isModify = false;
            try
            {
                Order latestOrder;
                string orderQtyInString;
                string orderID;
                string parentOrderID;
                string orderType;
                Dictionary<string, dynamic> response = new Dictionary<string, dynamic>();
                if (GetLatestOrderDetails(TradingSymbol, Exchange, TransactionType, Constants.ORDER_STATUS_OPEN, out latestOrder))
                {
                    orderID = latestOrder.OrderId;
                    orderQtyInString = (latestOrder.Quantity).ToString();
                    orderType = latestOrder.OrderType;
                    parentOrderID = latestOrder.ParentOrderId;

                    if (orderType == Constants.ORDER_TYPE_LIMIT)
                    {
                        if (latestOrder.Status == Constants.ORDER_STATUS_OPEN)
                        {
                            response = kite.ModifyOrder(
                                                    OrderId: orderID,
                                                    ParentOrderId: parentOrderID,
                                                    Exchange: Exchange,
                                                    TradingSymbol: TradingSymbol,
                                                    TransactionType: TransactionType,
                                                    Quantity: orderQtyInString,
                                                    Price: Price,
                                                    OrderType: Constants.ORDER_TYPE_LIMIT,
                                                    Product: Product,
                                                    Variety: Variety);

                            isModify = true;
                            var orderId = response["data"]["order_id"];
                            var logMessage = "Order modified for " + Constants.PRODUCT_MIS + " Successfully. OrderId = " + orderId;
                            WriteUniquelogs(TradingSymbol + " " + m_UserID, logMessage, MessageType.Informational);
                        }
                    }
                }
                else
                {
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, "ModifyLimitOrder : Not able to fetch latest order", MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "ModifyLimitOrder : Exception Error Message" + e.Message, MessageType.Exception);
            }
            return isModify;
        }

        public bool ModifyStopOrder(string TradingSymbol, string TransactionType, decimal TriggerPrice, string Exchange = Constants.EXCHANGE_NSE, string Product = Constants.PRODUCT_MIS)
        {
            bool isModify = false;
            try
            {
                Order latestOrder;
                string orderQtyInString;
                string orderID;
                string orderType;
                Dictionary<string, dynamic> response = new Dictionary<string, dynamic>();
                if (GetLatestOrderDetails(TradingSymbol, Exchange, TransactionType, Constants.ORDER_STATUS_PENDING, out latestOrder))
                {
                    orderID = latestOrder.OrderId;
                    orderQtyInString = (latestOrder.Quantity).ToString();
                    orderType = latestOrder.OrderType;

                    if (orderType == Constants.ORDER_TYPE_SLM)
                    {
                        if (latestOrder.Status == Constants.ORDER_STATUS_PENDING)
                        {
                            response = kite.ModifyOrder(
                                                    OrderId: orderID,
                                                    Exchange: Exchange,
                                                    TradingSymbol: TradingSymbol,
                                                    TransactionType: TransactionType,
                                                    Quantity: orderQtyInString,
                                                    TriggerPrice: TriggerPrice,
                                                    OrderType: Constants.ORDER_TYPE_SLM,
                                                    Product: Product);

                            isModify = true;
                            var orderId = response["data"]["order_id"];
                            var logMessage = "Order modified for " + Constants.PRODUCT_MIS + " Successfully. OrderId = " + orderId;
                            WriteUniquelogs(TradingSymbol + " " + m_UserID, logMessage, MessageType.Informational);
                        }
                    }
                }
                else
                {
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, "ModifyStopOrder : Not able to fetch latest order", MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "ModifyStopOrder :  Exception Error Message" + e.Message, MessageType.Exception);
            }
            return isModify;
        }
        //IRDS::Jyoti::09-Aug-21::Added modify function to convert to MARKET order
        public bool ModifyStopOrderToMarket(string TradingSymbol, string TransactionType, decimal Price, decimal TriggerPrice, string OrderId, int Quantity, string Exchange = Constants.EXCHANGE_NSE, string Product = Constants.PRODUCT_MIS)
        {
            bool isModify = false;
            try
            {
                Order latestOrder;
                string orderQtyInString;
                string orderID;
                string orderType;
                Dictionary<string, dynamic> response = new Dictionary<string, dynamic>();
                //sanika::19-Jam-2021::passed order type because stop order and limit order has same status
                if (GetLatestOrderDetails(TradingSymbol, Exchange, TransactionType, Constants.ORDER_STATUS_PENDING, out latestOrder))
                {
                    orderID = OrderId;
                    orderQtyInString = Quantity.ToString();
                    orderType = latestOrder.OrderType;

                    if (orderType == Constants.ORDER_TYPE_SLM)
                    {
                        if (latestOrder.Status == Constants.ORDER_STATUS_PENDING)
                        {
                            WriteUniquelogs(TradingSymbol + " " + m_UserID, "orderID " + orderID + " Exchange " + Exchange + " TradingSymbol " + TradingSymbol + " TransactionType " + TransactionType + " orderQtyInString " + orderQtyInString + " TriggerPrice " + TriggerPrice + " Product " + Product, MessageType.Informational);
                            response = kite.ModifyOrder(
                                                    OrderId: orderID,
                                                    Exchange: Exchange,
                                                    TradingSymbol: TradingSymbol,
                                                    TransactionType: TransactionType,
                                                    Quantity: orderQtyInString,
                                                    TriggerPrice: TriggerPrice,
                                                    OrderType: "MARKET",
                                                    Product: Product,
                                                    Price: Price,
                                                    DisclosedQuantity: latestOrder.DisclosedQuantity);

                            if (response != null)
                            {
                                isModify = true;
                                long orderId = response["result"]["AppOrderID"];
                                var logMessage = "Order modified for " + Constants.PRODUCT_MIS + " Successfully. OrderId = " + orderId;
                                WriteUniquelogs(TradingSymbol + " " + m_UserID, logMessage, MessageType.Informational);
                            }
                            else
                            {
                                isModify = false;
                                WriteUniquelogs(TradingSymbol + " " + m_UserID, "Not able to modify order", MessageType.Informational);
                            }
                        }
                    }
                }
                else
                {
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, "ModifyStopOrder : Not able to fetch latest order", MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "ModifyStopOrder :  Exception Error Message" + e.Message, MessageType.Exception);
            }
            return isModify;
        }

        public bool ModifyLimitOrderToMarket(string TradingSymbol, string TransactionType, decimal Price, decimal TriggerPrice, string OrderId, int Quantity, string Exchange = Constants.EXCHANGE_NSE, string Product = Constants.PRODUCT_MIS)
        {
            bool isModify = false;
            try
            {
                Order latestOrder;
                string orderQtyInString;
                string orderID;
                string orderType;
                Dictionary<string, dynamic> response = new Dictionary<string, dynamic>();
                //sanika::19-Jam-2021::passed order type because stop order and limit order has same status
                if (GetLatestOrderDetails(TradingSymbol, Exchange, TransactionType, Constants.ORDER_STATUS_PENDING, out latestOrder))
                {
                    orderID = OrderId;
                    orderQtyInString = Quantity.ToString();
                    orderType = latestOrder.OrderType;

                    if (orderType == Constants.ORDER_TYPE_SLM)
                    {
                        if (latestOrder.Status == Constants.ORDER_STATUS_PENDING)
                        {
                            WriteUniquelogs(TradingSymbol + " " + m_UserID, "orderID " + orderID + " Exchange " + Exchange + " TradingSymbol " + TradingSymbol + " TransactionType " + TransactionType + " orderQtyInString " + orderQtyInString + " TriggerPrice " + TriggerPrice + " Product " + Product, MessageType.Informational);
                            response = kite.ModifyOrder(
                                                    OrderId: orderID,
                                                    Exchange: Exchange,
                                                    TradingSymbol: TradingSymbol,
                                                    TransactionType: TransactionType,
                                                    Quantity: orderQtyInString,
                                                    TriggerPrice: TriggerPrice,
                                                    OrderType: "MARKET",
                                                    Product: Product,
                                                    Price: Price,
                                                    DisclosedQuantity: latestOrder.DisclosedQuantity);

                            if (response != null)
                            {
                                isModify = true;
                                long orderId = response["result"]["AppOrderID"];
                                var logMessage = "Order modified for " + Constants.PRODUCT_MIS + " Successfully. OrderId = " + orderId;
                                WriteUniquelogs(TradingSymbol + " " + m_UserID, logMessage, MessageType.Informational);
                            }
                            else
                            {
                                isModify = false;
                                WriteUniquelogs(TradingSymbol + " " + m_UserID, "Not able to modify order", MessageType.Informational);
                            }
                        }
                    }
                }
                else
                {
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, "ModifyStopOrder : Not able to fetch latest order", MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "ModifyStopOrder :  Exception Error Message" + e.Message, MessageType.Exception);
            }
            return isModify;
        }

        public bool ModifyStopOrderWithOrderId(string TradingSymbol, string TransactionType, decimal TriggerPrice, string OrderId, int Quantity, string Exchange = Constants.EXCHANGE_NSE, string Product = Constants.PRODUCT_MIS)
        {
            bool isModify = false;
            try
            {
                Order latestOrder;
                string orderQtyInString;
                string orderID;
                string orderType;
                Dictionary<string, dynamic> response = new Dictionary<string, dynamic>();
                if (GetLatestOrderDetails(TradingSymbol, Exchange, TransactionType, Constants.ORDER_STATUS_PENDING, out latestOrder))
                {
                    orderID = OrderId;
                    orderQtyInString = Quantity.ToString();
                    orderType = latestOrder.OrderType;

                    // if (orderType == Constants.ORDER_TYPE_SLM)
                    {
                        // if (latestOrder.Status == Constants.ORDER_STATUS_PENDING)
                        {
                            WriteUniquelogs(TradingSymbol + " " + m_UserID, "orderID " + orderID + " Exchange " + Exchange + " TradingSymbol " + TradingSymbol + " TransactionType " + TransactionType + " orderQtyInString " + orderQtyInString + " TriggerPrice " + TriggerPrice + " Product " + Product, MessageType.Informational);
                            response = kite.ModifyOrder(
                                                    OrderId: orderID,
                                                    Exchange: Exchange,
                                                    TradingSymbol: TradingSymbol,
                                                    TransactionType: TransactionType,
                                                    Quantity: orderQtyInString,
                                                    TriggerPrice: TriggerPrice,
                                                    OrderType: Constants.ORDER_TYPE_SLM,
                                                    Product: Product);

                            isModify = true;
                            var orderId = response["data"]["order_id"];
                            var logMessage = "Order modified for " + Constants.PRODUCT_MIS + " Successfully. OrderId = " + orderId;
                            WriteUniquelogs(TradingSymbol + " " + m_UserID, logMessage, MessageType.Informational);
                        }
                    }
                }
                else
                {
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, "ModifyStopOrder : Not able to fetch latest order", MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "ModifyStopOrder :  Exception Error Message" + e.Message, MessageType.Exception);
            }
            return isModify;
        }

 //sanika::26-Apr-2021::Added to modify sl order with order id
        public bool ModifySLOrderWithOrderId(string TradingSymbol, string TransactionType, decimal TriggerPrice, string OrderId, int Quantity, string Exchange = Constants.EXCHANGE_NSE, string Product = Constants.PRODUCT_MIS)
        {
            bool isModify = false;
            try
            {
                Order latestOrder;
                string orderQtyInString;
                string orderID;
                string orderType;
                Dictionary<string, dynamic> response = new Dictionary<string, dynamic>();
                if (GetLatestOrderDetails(TradingSymbol, Exchange, TransactionType, Constants.ORDER_STATUS_PENDING, out latestOrder))
                {
                    orderID = OrderId;
                    orderQtyInString = Quantity.ToString();
                    orderType = latestOrder.OrderType;
                            WriteUniquelogs(TradingSymbol + " " + m_UserID, "orderID " + orderID + " Exchange " + Exchange + " TradingSymbol " + TradingSymbol + " TransactionType " + TransactionType + " orderQtyInString " + orderQtyInString + " TriggerPrice " + TriggerPrice + " Product " + Product, MessageType.Informational);
                            response = kite.ModifyOrder(
                                                    OrderId: orderID,
                                                    Exchange: Exchange,
                                                    TradingSymbol: TradingSymbol,
                                                    TransactionType: TransactionType,
                                                    Quantity: orderQtyInString,
                                                    TriggerPrice: TriggerPrice,
                                                    OrderType: Constants.ORDER_TYPE_SL,
                                                    Product: Product);

                            isModify = true;
                            var orderId = response["data"]["order_id"];
                            var logMessage = "Order modified for " + Constants.PRODUCT_MIS + " Successfully. OrderId = " + orderId;
                            WriteUniquelogs(TradingSymbol + " " + m_UserID, logMessage, MessageType.Informational);

                }
                else
                {
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, "ModifyStopOrder : Not able to fetch latest order", MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "ModifyStopOrder :  Exception Error Message" + e.Message, MessageType.Exception);
            }
            return isModify;
        }
        public bool ModifyBOStopOrder(string TradingSymbol, string TransactionType, decimal TriggerPrice, string Exchange, string Product)
        {
            bool isModify = false;
            try
            {
                Order latestOrder;
                string orderQtyInString;
                string orderID;
                string orderType;
                string parentOrderID;
                Dictionary<string, dynamic> response = new Dictionary<string, dynamic>();
                if (GetLatestOrderDetails(TradingSymbol, Exchange, TransactionType, Constants.ORDER_STATUS_PENDING, out latestOrder))
                {
                    orderID = latestOrder.OrderId;
                    parentOrderID = latestOrder.ParentOrderId;
                    orderQtyInString = (latestOrder.Quantity).ToString();
                    orderType = latestOrder.OrderType;

                    if (orderType == Constants.ORDER_TYPE_SL)
                    {
                        if (latestOrder.Status == Constants.ORDER_STATUS_PENDING)
                        {
                            response = kite.ModifyOrder(
                                                    OrderId: orderID,
                                                    ParentOrderId: parentOrderID,
                                                    Exchange: Exchange,
                                                    TradingSymbol: TradingSymbol,
                                                    TransactionType: TransactionType,
                                                    Quantity: orderQtyInString,
                                                    TriggerPrice: TriggerPrice,
                                                    OrderType: Constants.ORDER_TYPE_SL,
                                                    Product: Product);

                            isModify = true;
                            var orderId = response["data"]["order_id"];
                            var logMessage = "Order modified for " + Constants.PRODUCT_MIS + " Successfully. OrderId = " + orderId;
                            WriteUniquelogs(TradingSymbol + " " + m_UserID, logMessage, MessageType.Informational);
                        }
                    }
                }
                else
                {
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, "ModifyBOStopOrder : Not able to fetch latest order", MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "ModifyBOStopOrder : Exception Error Message" + e.Message, MessageType.Exception);
            }
            return isModify;
        }

        public bool CancelAllPendingOrder(string TradingSymbol = "", string Exchange = "")
        {

            try
            {
                List<Order> orderInfo = new List<Order>();
                if (TradingSymbol != "")
                {
                    orderInfo = m_GlobalOrderStore.GetOrderbySymbol(TradingSymbol, Exchange);
                }
                else
                {
                    orderInfo = m_GlobalOrderStore.GetAllOrder();
                }

                if (orderInfo.Count == 0)
                {
                    string errorMsg = "CancelAllPendingOrder: Not Found any previous order - " + orderInfo.Count.ToString();
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, errorMsg, MessageType.Error);

                    latestOrder = new IRDSAlgoOMS.Order();
                    return true;
                }
                for (int iCount = 0; iCount < orderInfo.Count; iCount++)
                {
                    Order latestOrder = orderInfo[iCount];
                    if (latestOrder.Status == Constants.ORDER_STATUS_CANCELLED || latestOrder.Status == Constants.ORDER_STATUS_COMPLETE || latestOrder.Status == Constants.ORDER_STATUS_REJECTED)
                    {
                        //WriteUniquelogs(TradingSymbol + " " + m_UserID, "CancelAllPendingOrder: " + TradingSymbol + " Previous Order Status : " + latestOrder.Status + " Cancel or Complete or Rejected Status", MessageType.Informational);
                    }
                    else
                    {
                        string orderID = latestOrder.OrderId;
                        string ProductType = latestOrder.Product.ToLower();
                        if (ProductType != "bo" && ProductType != "co")
                        {
                            ProductType = Constants.VARIETY_REGULAR;
                        }
                        var getCancelOrder = CancelOrderEX(orderID, ProductType);
                    }
                }
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "CancelAllPendingOrder : Return true", MessageType.Informational);
                WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + TradingSymbol + " Cancel Order successfully!!");
                m_ExceptionCounter++;
                return true;
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "CancelAllPendingOrder :  Exception Error Message " + e.Message, MessageType.Exception);
                WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + TradingSymbol + " " + e.Message);
                m_ExceptionCounter++;
            }

            return false;
        }

        public bool CancelOrder(string orderId, string productType, string symbol)
        {
            bool isCancelOrder = false;
            try
            {
                string ProductType = productType.ToLower();
                if (ProductType != "bo" && ProductType != "co")
                {
                    ProductType = Constants.VARIETY_REGULAR;
                }
                var getCancelOrder = CancelOrderEX(orderId, ProductType);
                isCancelOrder = true;
                //Sanika::3-sep-2020::added for log purpose
                WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + symbol + " Cancelled successfully!");
                m_ExceptionCounter++;
            }
            catch (Exception e)
            {
                WriteUniquelogs(symbol + " " + m_UserID, "CancelOrder :  Exception Error Message " + e.Message, MessageType.Exception);
                WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + e.Message);
                m_ExceptionCounter++;
            }

            return isCancelOrder;
        }

        public void ForceFulReLogin()
        {
            try
            {
                kite.m_ForceFulLoginFlag = false;
                kite.initSeesion();
                //sanika::15-oct-2020::added condition for wrong toast display if relogin fails
                if (kite.m_ForceFulLoginFlag)
                {
                    logger.LogMessage(m_UserID + "Forceful Relogin initSeesion successful.", MessageType.Informational);
                    WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + "Re-connected!!");
                    m_ExceptionCounter++;

                    //IRDS::9-oct-2019::Sanika::Added if login failed then also call this two methods
                    kite.SetAccessToken(kite.accessToken);
                    logger.LogMessage("SetAccessToken successful.", MessageType.Informational);

                    // initialize ticker
                    if (ticker != null)
                    {
                        ticker = new Ticker();
                    }
                    kite.initTicker();

                    logger.LogMessage("initTicker successful.", MessageType.Informational);
                }
                else
                {
                    logger.LogMessage(m_UserID + "Forceful Relogin initSeesion Unsuccessful.", MessageType.Informational);
                    WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + "Not able to Re-connected!!");
                    m_ExceptionCounter++;
                }
            }
            catch (Exception e)
            {
                logger.LogMessage(m_UserID + "ForceFulReLogin : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        public void WriteUniquelogs(string symbol, string message, MessageType msgType)
        {
            try
            {
                SymbolLogs log = new SymbolLogs();
                log.createFile(symbol);
                log.LogMessage(message, MessageType.Informational);
                log = null;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : " + e.Message);
                Trace.WriteLine("Exception : " + e.Message);
            }
        }

        public bool CloseBOOrder(string TradingSymbol, string Exchange)
        {

            bool isClose = false;
            try
            {
                List<Order> orderInfo = m_GlobalOrderStore.GetOrderbySymbol(TradingSymbol, Exchange);

                if (orderInfo.Count == 0)
                {
                    string errorMsg = "CloseBOOrder: Not Found any previous order - " + orderInfo.Count.ToString();
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, errorMsg, MessageType.Informational);

                    latestOrder = new IRDSAlgoOMS.Order();
                    latestOrder.Status = Constants.ORDER_STATUS_CANCELLED;
                    return true;
                }
                for (int iCount = 0; iCount < orderInfo.Count; iCount++)
                {
                    Order latestOrder = orderInfo[iCount];
                    if (latestOrder.Status == Constants.ORDER_STATUS_CANCELLED || latestOrder.Status == Constants.ORDER_STATUS_COMPLETE || latestOrder.Status == Constants.ORDER_STATUS_REJECTED)
                    {
                        //nothing to do
                        isClose = true;
                    }
                    else
                    {
                        string orderID = latestOrder.OrderId;
                        var getCancelOrder = CancelOrderEX(orderID, Constants.VARIETY_BO);
                        List<Order> orderInfoAfterExit = m_GlobalOrderStore.GetOrderbySymbol(TradingSymbol, Exchange);
                        if (orderInfoAfterExit.Count != 0)
                        {
                            for (int iCounter = 0; iCounter < orderInfoAfterExit.Count; iCounter++)
                            {
                                Order latestOrderAfterExit = orderInfoAfterExit[iCounter];
                            }
                        }
                        isClose = true;
                        WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseBOOrder :" + TradingSymbol + "Successfuly exit", MessageType.Informational);
                    }
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseBOOrder : Exception Error Message " + e.Message, MessageType.Informational);
            }
            return isClose;
        }

        public bool CloseAllOpenOrders()
        {
            bool isClose = false;
            try
            {
                Dictionary<string, PositionInfo> allPositions = new Dictionary<string, PositionInfo>();
                allPositions = m_GlobalpositionStore.GetAllPositions();
                if (allPositions.Count > 0)
                {
                    foreach (var position in allPositions)
                    {
                        var positionData = position.Value.netPosition;
                        if (positionData.Quantity != 0)
                        {
                            string newOrderBuyOrSell = "";
                            int newOrderQty = 0;
                            string TradingSymbol = positionData.TradingSymbol;
                            string Exchange = positionData.Exchange;
                            string Product = positionData.Product;
                            WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseAllOpenOrders : Found open position", MessageType.Informational);
                            if ((positionData.Quantity > 0 || positionData.Quantity > 0))
                            {
                                newOrderBuyOrSell = Constants.TRANSACTION_TYPE_SELL;
                            }
                            else if ((positionData.Quantity < 0 || positionData.Quantity < 0))
                            {
                                newOrderBuyOrSell = Constants.TRANSACTION_TYPE_BUY;
                            }

                            //IRDS::17-JUL-2020 :: Add comment because day position quantity got wrong :: sandip/sanika

                            //if (Math.Abs(currentPosition.dayPosition.Quantity) > Math.Abs(currentPosition.netPosition.Quantity))
                            //    newOrderQty = Math.Abs(currentPosition.dayPosition.Quantity);
                            //else

                            newOrderQty = Math.Abs(positionData.Quantity);
                            if (newOrderQty != 0)
                            {
                                WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseAllOpenOrders :newOrderQty is not zero" + newOrderQty, MessageType.Informational);
                                string orderID = PlaceOrder(Exchange, TradingSymbol, newOrderBuyOrSell, newOrderQty, Constants.ORDER_TYPE_MARKET, Product: Product);
                                if (orderID != "NA")
                                {
                                    double openPrice = GetOpenPostionPricebyOrderID(TradingSymbol, Exchange, orderID);
                                    WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseAllOpenOrders : Order Complete at price = " + openPrice, MessageType.Informational);
                                    WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseAllOpenOrders : order closed successfully", MessageType.Informational);
                                    WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + TradingSymbol + " Order Closed successfully!! ");
                                    m_ExceptionCounter++;

                                    //sanika::16-oct-2020::add information after placing order
                                    DateTime dateTime = DateTime.Now;
                                    m_LocalOrderStructureInfo.AddOrUpdateOrderInformation(TradingSymbol: TradingSymbol, Exchange: Exchange, ClosedTransactionType: newOrderBuyOrSell, Product: Product, ClosedQuantity: newOrderQty, ClosedOrderDateTime: dateTime, ClosedOrderId: orderID);
                                    isClose = true;
                                }
                                else
                                {
                                    WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseAllOpenOrders : order not closed", MessageType.Informational);
                                    WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + TradingSymbol + " Order not closed ");
                                }
                            }
                        }
                        else
                        {
                            isClose = true;
                        }
                    }
                }
                else
                {
                    logger.LogMessage("CloseAllOpenOrders : allPositions count found 0", MessageType.Informational);
                    isClose = true;
                }
            }
            catch (Exception e)
            {
                isClose = false;
                logger.LogMessage("CloseAllOpenOrders : Exception Error Message = " + e.Message, MessageType.Informational);
            }
            return isClose;
        }

        //sanika::12-Nov-2020::Added close mis order seperate function for ajay sir
        public bool CloseAllOpenMISOrders(string exchange = "")//sanika::3-dec-2020::Added optional variable as exchange and added condition for that
        {
            bool isClose = false;
            try
            {
                Dictionary<string, PositionInfo> allPositions = new Dictionary<string, PositionInfo>();
                allPositions = m_GlobalpositionStore.GetAllPositions();
                if (allPositions.Count > 0)
                {
                    foreach (var position in allPositions)
                    {
                        var positionData = position.Value.netPosition;//sanika::added check for cnc order 
                        if ((positionData.Quantity != 0 && positionData.Product != Constants.PRODUCT_NRML && positionData.Product != Constants.PRODUCT_CNC) && (exchange == "" || exchange == positionData.Exchange))
                        {
                            string newOrderBuyOrSell = "";
                            int newOrderQty = 0;
                            string TradingSymbol = positionData.TradingSymbol;
                            string Exchange = positionData.Exchange;
                            string Product = positionData.Product;
                            WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseAllOpenOrders : Found open position", MessageType.Informational);
                            if ((positionData.Quantity > 0 || positionData.Quantity > 0))
                            {
                                newOrderBuyOrSell = Constants.TRANSACTION_TYPE_SELL;
                            }
                            else if ((positionData.Quantity < 0 || positionData.Quantity < 0))
                            {
                                newOrderBuyOrSell = Constants.TRANSACTION_TYPE_BUY;
                            }

                            //IRDS::17-JUL-2020 :: Add comment because day position quantity got wrong :: sandip/sanika

                            //if (Math.Abs(currentPosition.dayPosition.Quantity) > Math.Abs(currentPosition.netPosition.Quantity))
                            //    newOrderQty = Math.Abs(currentPosition.dayPosition.Quantity);
                            //else

                            newOrderQty = Math.Abs(positionData.Quantity);
                            if (newOrderQty != 0)
                            {
                                WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseAllOpenOrders :newOrderQty is not zero" + newOrderQty, MessageType.Informational);
                                string orderID = PlaceOrder(Exchange, TradingSymbol, newOrderBuyOrSell, newOrderQty, Constants.ORDER_TYPE_MARKET, Product: Product);
                                if (orderID != "NA")
                                {
                                    double openPrice = GetOpenPostionPricebyOrderID(TradingSymbol, Exchange, orderID);
                                    WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseAllOpenOrders : Order Complete at price = " + openPrice, MessageType.Informational);
                                    WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseAllOpenOrders : order closed successfully", MessageType.Informational);
                                    WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + TradingSymbol + " Order Closed successfully!! ");
                                    m_ExceptionCounter++;

                                    //sanika::16-oct-2020::add information after placing order
                                    DateTime dateTime = DateTime.Now;
                                    m_LocalOrderStructureInfo.AddOrUpdateOrderInformation(TradingSymbol: TradingSymbol, Exchange: Exchange, ClosedTransactionType: newOrderBuyOrSell, Product: Product, ClosedQuantity: newOrderQty, ClosedOrderDateTime: dateTime, ClosedOrderId: orderID);
                                    isClose = true;
                                }
                                else
                                {
                                    WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseAllOpenOrders : order not closed", MessageType.Informational);
                                    WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + TradingSymbol + " Order not closed ");
                                }
                            }
                        }
                        else
                        {
                            isClose = true;
                        }
                    }
                }
                else
                {
                    logger.LogMessage("CloseAllOpenOrders : allPositions count found 0", MessageType.Informational);
                    isClose = true;
                }
            }
            catch (Exception e)
            {
                isClose = false;
                logger.LogMessage("CloseAllOpenOrders : Exception Error Message = " + e.Message, MessageType.Informational);
            }
            return isClose;
        }

        //sanika::3-dec-2020::Added optional variable as exchange and added condition for that
        public bool CancelAllPendingOrders(string Exchange = "")
        {
            bool isCancel = false;

            try
            {
                List<Order> allOrders = new List<Order>();
                allOrders = m_GlobalOrderStore.GetAllOrder();
                if (allOrders.Count > 0)
                {
                    foreach (var order in allOrders)
                    {
                        if ((order.Status == Constants.ORDER_STATUS_PENDING || order.Status == Constants.ORDER_STATUS_OPEN) && (Exchange == "" || Exchange == order.Exchange))
                        {
                            string orderId = order.OrderId;
                            string product = order.Product.ToLower();
                            if (product != "bo" && product != "co")
                            {
                                product = Constants.VARIETY_REGULAR;
                            }
                            var response = CancelOrderEX(orderId, product);
                            if (response != null)
                            {
                                isCancel = true;
                                WriteUniquelogs(order.Tradingsymbol + " " + m_UserID, "CancelAllPendingOrders : Return true", MessageType.Informational);
                                WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + order.Tradingsymbol + " Cancel Order successfully!!");
                                m_ExceptionCounter++;
                            }
                        }
                        else
                        {
                            isCancel = true;
                        }
                    }
                }
                else
                {
                    logger.LogMessage("CancelAllPendingOrders : allOrders count found as 0", MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                isCancel = false;
                logger.LogMessage("CancelAllPendingOrders : Exception Error Message = " + e.Message, MessageType.Exception);
            }

            return isCancel;
        }


        public bool CloseOrder(string TradingSymbol, string Exchange, string Product)
        {
            try
            {
                string newOrderBuyOrSell = "";
                int newOrderQty = 0;
                string mappedSymbol = TradingSymbol + "|" + Exchange;
                if (CancelAllPendingOrder(TradingSymbol, Exchange))
                {
                    //nothing
                }
                PositionInfo currentPosition;
                if (!GetCurrentPosition(mappedSymbol, Product, out currentPosition))
                {
                    string errorMsg = string.Format("CloseOrder : Not found current position for symbol : {0}", mappedSymbol);
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, errorMsg, MessageType.Error);

                    if (bGetCurrentPositionError)
                    {
                        return false;
                    }
                }
                else
                {
                    //sanika::28-sep-2020::added direction in logs/toast
                    string direction = "";
                    string logMessage = string.Format("CloseOrder : Current position fetched from server for mapped symbol: {0} Day Position: {1} , Net Position: {2}",
                                                  mappedSymbol, currentPosition.dayPosition.Quantity, currentPosition.netPosition.Quantity);
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, logMessage, MessageType.Informational);
                    if ((currentPosition.dayPosition.Quantity > 0 || currentPosition.netPosition.Quantity > 0))
                    {
                        newOrderBuyOrSell = Constants.TRANSACTION_TYPE_SELL;
                        //sanika::28-sep-2020::added direction in logs/toast
                        direction = Constants.TRANSACTION_TYPE_BUY; ;
                    }
                    else if ((currentPosition.dayPosition.Quantity < 0 || currentPosition.netPosition.Quantity < 0))
                    {
                        newOrderBuyOrSell = Constants.TRANSACTION_TYPE_BUY;
                        //sanika::28-sep-2020::added direction in logs/toast
                        direction = Constants.TRANSACTION_TYPE_SELL;
                    }

                    //IRDS::17-JUL-2020 :: Add comment because day position quantity got wrong :: sandip/sanika

                    //if (Math.Abs(currentPosition.dayPosition.Quantity) > Math.Abs(currentPosition.netPosition.Quantity))
                    //    newOrderQty = Math.Abs(currentPosition.dayPosition.Quantity);
                    //else

                    newOrderQty = Math.Abs(currentPosition.netPosition.Quantity);

                    if (newOrderQty != 0)
                    {
                        WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseOrder :newOrderQty is not zero" + newOrderQty, MessageType.Informational);
                        string orderID = PlaceOrder(Exchange, TradingSymbol, newOrderBuyOrSell, newOrderQty, Constants.ORDER_TYPE_MARKET, Product: Product);
                        if (orderID != "NA")
                        {
                            double openPrice = GetOpenPostionPricebyOrderID(TradingSymbol, Exchange, orderID);
                            WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseOrder : Order Complete at price = " + openPrice, MessageType.Informational);
                            WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseOrder : order closed successfully", MessageType.Informational);
                            //sanika::28-sep-2020::added direction in logs/toast
                            WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + TradingSymbol + " " + direction + " Order Closed successfully!! ");
                            m_ExceptionCounter++;

                            //sanika::16-oct-2020::add information after placing order
                            DateTime dateTime = DateTime.Now;
                            m_LocalOrderStructureInfo.AddOrUpdateOrderInformation(TradingSymbol: TradingSymbol, Exchange: Exchange, ClosedTransactionType: newOrderBuyOrSell, Product: Product, ClosedQuantity: newOrderQty, ClosedOrderDateTime: dateTime, ClosedOrderId: orderID);
                            return true;
                        }
                        else
                        {
                            WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseOrder : order not closed", MessageType.Informational);
                            WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + TradingSymbol + " " + direction + " Order not Closed");
                        }
                    }
                    else
                    {
                        WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseOrder :newOrderQty is zero", MessageType.Informational);
                        WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + TradingSymbol + " Order closed already!!");
                        m_ExceptionCounter++;
                    }
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseOrder :  Exception Error Message " + e.Message, MessageType.Exception);
            }
            return false;
        }

        public bool CloseOrderWithQuantity(string TradingSymbol, string Exchange, string Product, int Quantity)
        {
            try
            {
                string newOrderBuyOrSell = "";
                int newOrderQty = 0;
                string mappedSymbol = TradingSymbol + "|" + Exchange;
                if (CancelAllPendingOrder(TradingSymbol, Exchange))
                {
                    //nothing
                }
                PositionInfo currentPosition;
                if (!GetCurrentPosition(mappedSymbol, Product, out currentPosition))
                {
                    string errorMsg = string.Format("CloseOrderWithQuantity : Not found current position for symbol : {0}", mappedSymbol);
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, errorMsg, MessageType.Error);

                    if (bGetCurrentPositionError)
                    {
                        return false;
                    }
                }
                else
                {
                    string logMessage = string.Format("CloseOrderWithQuantity : Current position fetched from server for mapped symbol: {0} Day Position: {1} , Net Position: {2}",
                                                  mappedSymbol, currentPosition.dayPosition.Quantity, currentPosition.netPosition.Quantity);
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, logMessage, MessageType.Informational);
                    if ((currentPosition.dayPosition.Quantity > 0 || currentPosition.netPosition.Quantity > 0))
                    {
                        newOrderBuyOrSell = Constants.TRANSACTION_TYPE_SELL;
                    }
                    else if ((currentPosition.dayPosition.Quantity < 0 || currentPosition.netPosition.Quantity < 0))
                    {
                        newOrderBuyOrSell = Constants.TRANSACTION_TYPE_BUY;
                    }

                    //IRDS::17-JUL-2020 :: Add comment because day position quantity got wrong :: sandip/sanika

                    //if (Math.Abs(currentPosition.dayPosition.Quantity) > Math.Abs(currentPosition.netPosition.Quantity))
                    //    newOrderQty = Math.Abs(currentPosition.dayPosition.Quantity);
                    //else
                    if (Quantity == 0)
                    {
                        newOrderQty = Math.Abs(currentPosition.netPosition.Quantity);
                    }
                    else if (Quantity > Math.Abs(currentPosition.netPosition.Quantity))
                    {
                        newOrderQty = Math.Abs(currentPosition.netPosition.Quantity);
                    }
                    else
                    {
                        newOrderQty = Quantity;
                    }

                    if (newOrderQty != 0)
                    {
                        WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseOrderWithQuantity :newOrderQty is not zero" + newOrderQty, MessageType.Informational);
                        string orderID = PlaceOrder(Exchange, TradingSymbol, newOrderBuyOrSell, newOrderQty, Constants.ORDER_TYPE_MARKET, Product: Product);
                        if (orderID != "")
                        {
                            double openPrice = GetOpenPostionPricebyOrderID(TradingSymbol, Exchange, orderID);
                            WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseOrderWithQuantity : Order Complete at price = " + openPrice, MessageType.Informational);
                            WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseOrderWithQuantity : placed order successfully", MessageType.Informational);
                            WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + TradingSymbol + " Order Closed successfully!! ");
                            m_ExceptionCounter++;

                            DateTime dateTime = DateTime.Now;
                            m_LocalOrderStructureInfo.AddOrUpdateOrderInformation(TradingSymbol: TradingSymbol, Exchange: Exchange, ClosedTransactionType: newOrderBuyOrSell, Product: Product, ClosedQuantity: newOrderQty, ClosedOrderDateTime: dateTime, ClosedOrderId: orderID);
                            return true;
                        }
                    }
                    else
                    {
                        WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseOrderWithQuantity :newOrderQty is zero", MessageType.Informational);
                        WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + TradingSymbol + " Order closed already!!");
                        m_ExceptionCounter++;
                    }
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseOrderWithQuantity :  Exception Error Message " + e.Message, MessageType.Exception);
            }
            return false;
        }

        public bool CloseCOOrder(string TradingSymbol, string Exchange, string Product)
        {
            bool isClose = false;
            try
            {
                List<Order> orderInfo = m_GlobalOrderStore.GetOrderbySymbol(TradingSymbol, Exchange);

                if (orderInfo.Count == 0)
                {
                    string errorMsg = "CloseCOOrder: Not Found any previous order - " + orderInfo.Count.ToString();
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, errorMsg, MessageType.Informational);

                    latestOrder = new Order();
                    latestOrder.Status = Constants.ORDER_STATUS_CANCELLED;
                    return true;
                }
                for (int iCount = 0; iCount < orderInfo.Count; iCount++)
                {
                    Order latestOrder = orderInfo[iCount];
                    if (latestOrder.Status == Constants.ORDER_STATUS_CANCELLED || latestOrder.Status == Constants.ORDER_STATUS_COMPLETE || latestOrder.Status == Constants.ORDER_STATUS_REJECTED)
                    {
                        //nothing to do
                        isClose = true;
                    }
                    else
                    {
                        string orderID = latestOrder.OrderId;
                        var getCancelOrder = kite.CancelOrder(orderID, Constants.VARIETY_CO);
                        List<Order> orderInfoAfterExit = m_GlobalOrderStore.GetOrderbySymbol(TradingSymbol, Exchange);
                        if (orderInfoAfterExit.Count != 0)
                        {
                            for (int iCounter = 0; iCounter < orderInfoAfterExit.Count; iCounter++)
                            {
                                Order latestOrderAfterExit = orderInfoAfterExit[iCounter];
                            }
                        }
                        isClose = true;
                        WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseCOOrder :" + TradingSymbol + "Successfuly exit", MessageType.Informational);
                        WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + TradingSymbol + " Order Closed successfully!! ");
                        m_ExceptionCounter++;
                    }
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseCOOrder : Exception Error Message " + e.Message, MessageType.Informational);
                WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + e.Message);
                m_ExceptionCounter++;
            }
            return isClose;
        }


        public bool CloseCOOrderWithOrder(string orderId, string Product)
        {
            bool isClose = false;
            try
            {
                var getCancelOrder = kite.CancelOrder(orderId, Product.ToLower());
                isClose = true;
                WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + orderId + " Order Closed successfully!! ");
                m_ExceptionCounter++;
            }
            catch (Exception e)
            {
                logger.LogMessage("CloseCOOrderWithOrder : Exception Error Message = " + e.Message, MessageType.Exception);
                WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + e.Message);
                m_ExceptionCounter++;
            }
            return isClose;
        }

        //sanika::8-dec-2020::Added to check risk management quantity
        public bool CheckRiskQuantity(string TradingSymbol, string Direction)
        {
            bool res = false;
            if (m_RiskManagmentFlag)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "CheckRiskQuantity : risk management flag true", MessageType.Informational);
                int brokerquantity = GetOrderQuantityByDirection(TradingSymbol, Direction);
                if (brokerquantity >= m_BrokerQuantity)
                {
                    res = true;
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, "CheckRiskQuantity : broker quantity condtition not macthed m_BrokerQuantity = " + m_BrokerQuantity + " brokerquantity " + brokerquantity, MessageType.Informational);
                    WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + TradingSymbol + " Broker quantity exceeds!!");
                    m_ExceptionCounter++;
                }
                int manualQuanity = GetManuallyOrderQuantity(TradingSymbol);
                if (res == false && manualQuanity >= m_ManualQuantity)
                {
                    res = true;
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, "CheckRiskQuantity : manual quantity condtition not macthed m_ManualQuantity = " + m_ManualQuantity + " manualQuanity " + manualQuanity, MessageType.Informational);
                    WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + TradingSymbol + " Manual quantity exceeds!!");
                    m_ExceptionCounter++;
                }
            }
            else
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "CheckRiskQuantity : risk management flag false", MessageType.Informational);
                res = false;
            }

            return res;
        }

        public string PlaceOrder(string Exchange,
                               string TradingSymbol,
                               string TransactionType,
                               int Quantity,
                               string OrderType,
                               decimal? Price = null,
                               string Product = Constants.PRODUCT_MIS,
                               string Validity = Constants.VALIDITY_DAY,
                               int? DisclosedQuantity = null,
                               decimal? TriggerPrice = null,
                               decimal? SquareOffValue = null,
                               decimal? StoplossValue = null,
                               decimal? TrailingStoploss = null,
                               string Variety = Constants.VARIETY_REGULAR,
                               string Tag = "IRDSAlgo")
        {
            string orderId = "NA";
            string errorMessage = "";
            try
            {
                if (m_isSqrOff == false)
                {
                    //sanika::8-dec-2020::added check risk management
                    if (CheckRiskQuantity(TradingSymbol, TransactionType) == false)
                    {
                        WriteUniquelogs(TradingSymbol + " " + m_UserID, "PlaceOrder : Exchange " + Exchange + " TradingSymbol " + TradingSymbol + " TransactionType " + TransactionType + "Quantity " + Quantity + " TriggerPrice " + TriggerPrice + " StoplossValue " + StoplossValue + " Variety " + Variety, MessageType.Informational);
                        WriteUniquelogs(TradingSymbol + " " + m_UserID, "PlaceOrder : OrderType " + OrderType + " Price " + Price + " Product " + Product + "Validity " + Validity + " SquareOffValue " + SquareOffValue + " TrailingStoploss " + TrailingStoploss + " Tag " + Tag, MessageType.Informational);
                        Dictionary<string, dynamic> response = new Dictionary<string, dynamic>();
                        //sanika::15-sep-2020::changed symbol name to uppper                          
                        Trace.WriteLine("&&&&Tuesday Going to placing order kitewrapper " + DateTime.Now + "\n");
                        if (Price == null)
                            Price = 0;
                        if (TriggerPrice == null)
                            TriggerPrice = 0;
                        OrdersPlaced order = new OrdersPlaced();
                        order.TradingSymbol = TradingSymbol;
                        order.Exchange = Exchange;
                        order.TransactionType = TransactionType;
                        order.TriggerPrice = (decimal)TriggerPrice;
                        order.Price = (decimal)Price;
                        order.Product = Product;
                        order.Quantity = Quantity;
                        order.OrderType = OrderType;

                        if (IsOrderLimitsValid(order))
                        {
                            response = kite.PlaceOrder(Exchange: Exchange,
                                    TradingSymbol: TradingSymbol.ToUpper(),
                                    TransactionType: TransactionType,
                                    Quantity: Quantity,
                                    OrderType: OrderType,
                                    Price: Price,
                                    Product: Product,
                                    Validity: Validity,
                                    DisclosedQuantity: DisclosedQuantity,
                                    TriggerPrice: TriggerPrice,
                                    SquareOffValue: SquareOffValue,
                                    StoplossValue: StoplossValue,
                                    TrailingStoploss: TrailingStoploss,
                                    Variety: Variety,
                                    Tag: Tag);
                            orderId = response["data"]["order_id"];
                            Trace.WriteLine("&&&&Tuesday Ends placing order kitewrapper " + DateTime.Now + "\n");
                            //sanika::28-sep-2020::added direction in logs/toast
                            WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + TradingSymbol + " " + TransactionType + " " + orderId + " Order Placed successfully!! ");
                            m_ExceptionCounter++;
                            //sanika::17-sep-2020::added quantity in log file
                            WriteUniquelogs(TradingSymbol + " " + m_UserID, "PlaceOrder : orderId " + orderId + " quantity = " + Quantity, MessageType.Informational);
                            //sanika::5-Jan-2021::Added loop instead of sleep
                            int counter = 0;
                            Order latestOrder = new Order();
                            while (latestOrder.OrderId == null && counter < 5)
                            {
                                GetLatestOrder(TradingSymbol, Exchange, orderId, out latestOrder);
                                counter++;
                            }
                            string orderStatus = latestOrder.Status;
                            if (orderStatus == Constants.ORDER_STATUS_REJECTED)
                            {
                                WriteUniquelogs(TradingSymbol + " " + m_UserID, "PlaceOrder : order status rejected", MessageType.Informational);
                                return orderId;
                            }
                            //sanika::8-dec-2020::Added to add counter in master list
                            if (m_RiskManagmentFlag == true)
                            {
                                AddManuallyOrderQuantity(TradingSymbol, Quantity);
                            }

                            if (orderId != "NA")
                            {
                                m_TickData.ResetLastBarHighLow(TradingSymbol);
                                WriteUniquelogs(TradingSymbol + " " + m_UserID, "PlaceOrder : Reset lastbar high and low values", MessageType.Informational);
                            }
                        }
                        else
                        {
                            WriteUniquelogs(TradingSymbol + " " + m_UserID, "PlaceOrder : IsOrderLimitsValid returns false as order limit exceeds ", MessageType.Informational);
                        }
                    }
                    else
                    {
                        WriteUniquelogs(TradingSymbol + " " + m_UserID, "PlaceOrder : order not placed!! \n Risk managment condition applied ", MessageType.Informational);
                        WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + TradingSymbol + " order not placed!!");
                        m_ExceptionCounter++;
                    }
                }
                else
                {
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, "PlaceOrder : order not placed!! You haved clicked on SqrOff", MessageType.Informational);
                    WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + TradingSymbol + " order not placed!! Trading Stopped!!");
                    m_ExceptionCounter++;
                }
                return orderId;
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "PlaceOrder :  Exception Error Message = " + e.Message, MessageType.Exception);
                errorMessage = e.Message;
                WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + e.Message);
                m_ExceptionCounter++;
                //sanika::6-July-2021::added notification if exception while placing order 
                AddNotificationInQueue("Order not able to place for " + TradingSymbol + " " + m_UserID + " " + DateTime.Now.ToString("HH:mm:ss"));
            }
            finally
            {
                if (orderId == "NA" && errorMessage != "")
                {
                    if (errorMessage.Contains("Trigger price") && errorMessage.Contains("stoploss") && (errorMessage.Contains("lower") || errorMessage.Contains("higher")) && errorMessage.Contains("last traded price") && errorMessage.Contains("Try limit order"))
                    {
                        orderId = "ErrorMessageForStopOrder";
                    }

                    if (errorMessage.Contains("order price") && errorMessage.Contains("limit") && (errorMessage.Contains("lower") || errorMessage.Contains("higher")) && errorMessage.Contains("range"))
                    {
                        orderId = "ErrorMessageForLimitOrder";
                    }

                    if (errorMessage.Contains("MIS orders") && errorMessage.Contains("blocked"))
                    {
                        orderId = "ErrorMessageForBlock";
                    }
                }
            }
            //sanika::21-July-2021::set flag of margin updation after placing order
            m_IsMarginLoad = true;
            return orderId;
        }



        public bool IsOrderLimitsValid(OrdersPlaced orderToPlace)
        {
            bool isValid = false;
            //Sanika::1-Feb-2021::Added try catch
            try
            {
                if (m_DictOrdersPlaced.ContainsKey(orderToPlace.TradingSymbol + "_" + orderToPlace.Exchange))
                {
                    int SameOrdersCount = 0;
                    int TotalOrdersPerSymbol = m_DictOrdersPlaced[orderToPlace.TradingSymbol + "_" + orderToPlace.Exchange].Count;
                    int totalOrders = 0;
                    foreach (var list in m_DictOrdersPlaced.Values)
                    {
                        totalOrders = totalOrders + list.Count;
                    }
                    if (totalOrders < m_OverallOrdersLimit)
                    {
                        if (TotalOrdersPerSymbol < m_OrdersPerSymbolLimit)
                        {
                            for (int i = 0; i < TotalOrdersPerSymbol; i++)
                            {
                                OrdersPlaced order = new OrdersPlaced();
                                order = m_DictOrdersPlaced[orderToPlace.TradingSymbol + "_" + orderToPlace.Exchange][i];
                                if (order.Equals(orderToPlace))
                                {
                                    SameOrdersCount = SameOrdersCount + 1;
                                }
                            }
                            if (SameOrdersCount >= m_SameOrdersLimit)
                            {
                                WriteUniquelogs(orderToPlace.TradingSymbol + " " + m_UserID, "m_SameOrdersLimit = " + m_SameOrdersLimit + " Total SameOrdersCount = " + SameOrdersCount, MessageType.Informational);
                                WriteUniquelogs(orderToPlace.TradingSymbol + " " + m_UserID, "m_OrdersPerSymbolLimit = " + m_OrdersPerSymbolLimit + " TotalOrdersPerSymbol = " + TotalOrdersPerSymbol, MessageType.Informational);
                                isValid = false;
                            }
                            else
                            {
                                isValid = true;
                            }
                        }
                        Trace.WriteLine("Limits ended: " + totalOrders);
                    }
                    else
                    {
                        WriteUniquelogs(orderToPlace.TradingSymbol + " " + m_UserID, "Overall Order Limit = " + m_OverallOrdersLimit + " Total Orders Placed = " + totalOrders, MessageType.Informational);
                        isValid = false;
                    }
                }
                else
                {
                    isValid = true;
                }
            }
            catch (Exception e)
            {
                logger.LogMessage("IsOrderLimitsValid : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            //logger.LogMessage("IsOrderLimitsValid : Returns isValid " + isValid, MessageType.Informational);
            return isValid;
        }

        public bool IsPendingOrder(string TradingSymbol, string Exchange, string TransactionType, string status, string Product, bool isPendingOrderToOpenOrder)
        {
            bool flagToReturn = false;
            try
            {
                List<Order> orderInfo = m_GlobalOrderStore.GetOrderbySymbol(TradingSymbol, Exchange);
                if (orderInfo.Count == 0)
                {
                    //WriteUniquelogs(TradingSymbol + " " + m_UserID, "IsPendingOrder : order info count is zero", MessageType.Informational);
                    return false;
                }
                for (int i = 0; i < orderInfo.Count(); i++)
                {
                    latestOrder = orderInfo[i];
                    if (latestOrder.TransactionType == TransactionType && latestOrder.Product == Product)
                    {
                        if (latestOrder.Status == status)
                        {
                            if ((!isPendingOrderToOpenOrder) && (latestOrder.ParentOrderId == null))
                            {
                                flagToReturn = true;
                                break;
                            }
                            else if ((isPendingOrderToOpenOrder) && (latestOrder.ParentOrderId != null))
                            {
                                flagToReturn = true;
                                break;
                            }
                        }
                    }
                }
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "IsPendingOrder : Return " + flagToReturn + " for " + TransactionType + " order ", MessageType.Informational);
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "IsPendingOrder : Exception Error Message " + e.Message, MessageType.Exception);
            }
            return flagToReturn;
        }

        //sanika::13-oct-2020::Added common function to cancel order
        public Dictionary<string, dynamic> CancelOrderEX(string OrderId, string Variety = Constants.VARIETY_REGULAR, string ParentOrderId = null)
        {
            try
            {
                //sanika::14-oct-2020::checked sqroff flag while cancelling order
                if (m_isSqrOff == false)
                {
                    var getCancelOrder = kite.CancelOrder(OrderId, Variety, ParentOrderId);
                    //sanika::21-July-2021::set flag of margin updation after cancelling order
                    m_IsMarginLoad = true;
                    return getCancelOrder;
                }
                else
                {
                    WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + "Trading Stopped!!");
                    m_ExceptionCounter++;
                }
            }
            catch (Exception e)
            {

            }
            return null;
        }
        //sanika::31-Dec-2020::Added status parameter
        public bool cancelPendingOrderWithTransactionType(string TradingSymbol, string Exchange, string TransactionType, string variety, string Status, bool isCancelToOpenOrder)
        {
            bool flagToReturn = false;

            try
            {
                List<Order> orderInfo = m_GlobalOrderStore.GetOrderbySymbol(TradingSymbol, Exchange);

                if (orderInfo.Count == 0)
                {
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, "cancel_Pending_Order_WithTransactionType : order info count is zero", MessageType.Informational);
                    return false;
                }
                for (int i = 0; i < orderInfo.Count(); i++)
                {
                    latestOrder = orderInfo[i];
                    if (latestOrder.TransactionType == TransactionType)
                    {
                        if (latestOrder.Status == Status)
                        {
                            if ((!isCancelToOpenOrder) && (latestOrder.ParentOrderId == null))
                            {
                                string orderId = latestOrder.OrderId;
                                string ProductType = variety;// latestOrder.Product.ToLower();
                                var getCancelOrder = kite.CancelOrder(orderId, ProductType);
                                flagToReturn = true;
                                //break;
                            }
                            else if ((isCancelToOpenOrder) && (latestOrder.ParentOrderId != null))
                            {
                                string orderId = latestOrder.OrderId;
                                string ProductType = latestOrder.Product.ToLower();
                                var getCancelOrder = CancelOrderEX(orderId, ProductType);
                                flagToReturn = true;
                                //break;
                            }
                        }
                    }
                }
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "cancel_Pending_Order_WithTransactionType : Return " + flagToReturn, MessageType.Informational);
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "cancel_Pending_Order_WithTransactionType : Exception Error Message = " + e.Message, MessageType.Exception);
            }

            return flagToReturn;
        }

        public bool GetLatestOrder(string TradingSymbol, string Exchange, string orderId, out Order latestOrder)
        {
            try
            {
                OrderExt orderExt;
                m_GlobalOrderStore.GetOrderbyID(TradingSymbol, Exchange, orderId, out orderExt);
                if (orderExt != null)
                {
                    latestOrder = orderExt.order;

                    //WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetLatestOrder : Return true ", MessageType.Informational);
                    return true;
                }
                else
                {
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetLatestOrder : orderExt is null", MessageType.Informational);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetLatestOrder : Exception Error Message " + ex.Message, MessageType.Exception);
            }
            latestOrder = new IRDSAlgoOMS.Order();
            return false;
        }

        public bool GetLatestOrderStatus(string TradingSymbol, string Exchange, out Order latestOrder)
        {
            try
            {
                string symbolName = TradingSymbol;
                List<Order> orderInfo = m_GlobalOrderStore.GetOrderbySymbol(TradingSymbol, Exchange);

                if (orderInfo.Count == 0)
                {
                    string errorMsg = "GetLatestOrderStatus: Not Found any previous order - " + orderInfo.Count.ToString();
                    WriteUniquelogs(symbolName + " " + m_UserID, errorMsg, MessageType.Error);

                    latestOrder = new IRDSAlgoOMS.Order();
                    latestOrder.Status = Constants.ORDER_STATUS_REJECTED;
                    return true;
                }

                latestOrder = orderInfo[orderInfo.Count - 1];
                WriteUniquelogs(symbolName + " " + m_UserID, "GetLatestOrderStatus : Return true ", MessageType.Informational);
            }
            catch (Exception ex)
            {
                latestOrder = new IRDSAlgoOMS.Order();
                latestOrder.Status = Constants.ORDER_STATUS_REJECTED;
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetLatestOrderStatus : Exception Error Message " + ex.Message, MessageType.Exception);
            }
            return true;
        }

        private void _OnTimerExecute(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                if (m_UpdatingStructures != null)
                {
                    if (m_UpdatingStructures.IsAlive)
                    {
                        Thread.Sleep(1000);
                    }
                    else
                    {
                        m_UpdatingStructures.Start();
                    }
                }
                else
                {
                    if (m_UpdatingStructures == null)
                    {
                        m_UpdatingStructures = new Thread(() => UpdateStructures());
                        m_UpdatingStructures.Start();
                    }
                }

                if (m_SendResponse != null)
                {
                    if (m_SendResponse.IsAlive)
                    {
                        Thread.Sleep(1000);
                    }
                    else
                    {
                        m_SendResponse.Start();
                    }
                }
                else
                {
                    if (m_SendResponse == null)
                    {
                        m_SendResponse = new Thread(() => SendResponse());
                        m_SendResponse.Start();
                    }
                }

                //check sqroff and closed order flag to abort calculate profitLoss thread
                if (m_isCalculateProfitLoss)
                {
                    //sanika::3-dec-2020::Change condition for close orders flag according exchange
                    if (m_isSqrOff == true || (m_isSqrOffNSE == true && m_isSqrOffNFO == true && m_isSqrOffMCX == true && m_isSqrOffCDS == true))
                    {
                        if (m_CalculateProfit != null)
                        {
                            if (m_CalculateProfit.IsAlive)
                            {
                                logger.LogMessage("_OnTimerExecute : Stopping thread of calculate profit/loss because exit time condition matched or overall profit/loss condition matched", MessageType.Informational);
                                m_CalculateProfit.Abort();
                            }
                        }
                    }//sanika::3-dec-2020::Added condition for all exchanges flags
                    else if (m_isCloseAllNSEOrder == true && m_isCloseAllNFOOrder == true && m_isCloseAllMCXOrder == true && m_isCloseAllCDSOrder == true)
                    {
                        if (m_CalculateProfit != null)
                        {
                            if (m_CalculateProfit.IsAlive)
                            {
                                logger.LogMessage("_OnTimerExecute : Stopping thread of calculate profit/loss because exit time condition matched", MessageType.Informational);
                                m_CalculateProfit.Abort();
                            }
                        }
                    }
                }

            }
            catch (Exception er)
            {
                logger.LogMessage("_OnTimerExecute : Exception Error Message = " + er.Message, MessageType.Exception);
            }
            Thread.Sleep(1000);


        }

        public void GetOrderTimeAndId(string TradingSymbol, string Exchange, string Product, out DateTime time, out string orderId)
        {
            time = new DateTime();
            orderId = "NA";
            try
            {
                string name = TradingSymbol + "_" + Exchange + "|" + Product;
                m_GlobalpositionStore.GetOrderIdAndTime(name, out time, out orderId);
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOrderTimeAndId : Exception Error Message " + e.Message, MessageType.Exception);
            }
        }

        //IRDS::Jyoti::08-09-2020::Samco Integration changes
        public List<Order> GetAllOrders()
        {
            //Sanika::11-sep-2020::Remove out variable as per latest design
            List<Order> Allorders1 = new List<Order>();
            List<Order> Allorders = new List<Order>();
            try
            {
                Allorders = m_GlobalOrderStore.GetAllOrder();
                //Sanika::11-sep-2020::order by orderis instead of timestamp
                Allorders1 = Allorders.OrderByDescending(x => x.OrderId).ToList();
            }
            catch (Exception e)
            {
                logger.LogMessage("GetAllOrders : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return Allorders1;
        }

        //IRDS::Jyoti::08-09-2020::Samco Integration
        public Dictionary<string, PositionInfo> GetAllPosition()
        {
            //Sanika::11-sep-2020::Remove out variable as per latest design
            Dictionary<string, PositionInfo> AllPositions = new Dictionary<string, PositionInfo>();
            try
            {
                AllPositions = m_GlobalpositionStore.GetAllPositions();
            }
            catch (Exception e)
            {
                logger.LogMessage("GetAllPosition : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return AllPositions;
        }
        //sanika::21-July-2021:: declared dictionary to store open positions and orders.
        Dictionary<string, int> UniqueOrderWrite = new Dictionary<string, int>();
        Dictionary<string, int> UniquePositionWrite = new Dictionary<string, int>();
        private void RequestNUpdateOrderInfo()
        {
            m_ListForSymbols.Clear();
            try
            {
                List<Order> orderInfo = new List<Order>();
                orderInfo = kite.GetOrderHistory();
                for (int i = orderInfo.Count - 1; i >= 0; i--)
                {
                    OrderExt objOrderExt = new OrderExt(orderInfo[i]);
                    m_GlobalOrderStore.AddOrUpdateOrder(objOrderExt);
                   
                    
                    //sanika::21-Jul-2021::added logs as per composite
                    string temp = "RequestNUpdateOrderInfo : symbol = " + objOrderExt.order.Tradingsymbol + " quantity " + objOrderExt.order.Quantity + " order type " + objOrderExt.order.OrderType + " order id " + objOrderExt.order.OrderId + " direction " + objOrderExt.order.TransactionType + " trigger price " + objOrderExt.order.TriggerPrice + " price " + objOrderExt.order.Price + " status " + objOrderExt.order.Status;
                    if (UniqueOrderWrite.ContainsKey(temp) == false)
                    {
                        WriteUniquelogs("OrderInfo" + " " + m_UserID, temp, MessageType.Informational);
                        UniqueOrderWrite[temp] = 0;
                    }
                    //store all order
                    m_GlobalOrderStore.AddOrder(objOrderExt);

                    //store time and order id for market order
                    if (objOrderExt.order.Status == Constants.ORDER_STATUS_COMPLETE)
                    {
                        m_GlobalpositionStore.AddOrderIdAndTime(objOrderExt);
                    }
                    if (!isTickMCXDataDownloader)
                    {
                        //sanika::2-Nov-2020::changed condition for mcx symbols
                        if (orderInfo[i].Exchange != Constants.EXCHANGE_NSE)
                        {
                            if (!m_ListForSymbols.Contains(orderInfo[i].Tradingsymbol + "." + orderInfo[i].Exchange)) //sanika::2-Nov-2020::changed condition for mcx symbols 
                            {
                                //IRDS::Jyoti::9-Sept-20::Added for options symbols
                                if (orderInfo[i].Tradingsymbol.EndsWith("CE") || orderInfo[i].Tradingsymbol.EndsWith("PE"))
                                    m_ListForSymbols.Add(orderInfo[i].Tradingsymbol + "." + Constants.EXCHANGE_NFO_OPT);
                                else
                                    m_ListForSymbols.Add(orderInfo[i].Tradingsymbol + "." + orderInfo[i].Exchange); //sanika::2-Nov-2020::changed for mcx symbols
                            }
                        }
                        else
                        {
                            if (!m_ListForSymbols.Contains(orderInfo[i].Tradingsymbol + "." + Constants.EXCHANGE_NSE))
                            {
                                m_ListForSymbols.Add(orderInfo[i].Tradingsymbol + "." + Constants.EXCHANGE_NSE);
                            }
                        }
                    }
                }
                if (orderInfo.Count > m_PreviousOrderCounter)
                {
                    CheckTradingSymbolPresentOrNot(m_ListForSymbols);
                    m_PreviousOrderCounter = orderInfo.Count;
                }

                //WriteUniquelogs("OrderInfo" + " " + m_UserID, "RequestNUpdateOrderInfo : End", MessageType.Informational);
            }
            catch (Exception e)
            {
                //sanika::6-July-2021::added notification 
                AddNotificationInQueue("Not able to get order information " + m_UserID + " " + DateTime.Now.ToString("HH:mm:ss"));
                WriteUniquelogs("OrderInfo" + " " + m_UserID, "RequestNUpdateOrderInfo : Exception Error Message " + e.Message, MessageType.Exception);
            }
        }

        private void RequestNUpdateHoldings()
        {
            var holdingResponse = kite.GetHoldings();
            foreach (var holding in holdingResponse)
            {
                m_GlobalholdingStore.AddOrUpdateHoldingInfo(holding);
            }
        }

        //IRDS::Jyoti::08-09-2020::Samco Integration changes
        public bool GetAllHoldings(out dynamic AllHoldings1)
        {
            Dictionary<string, Holding> AllHoldings = new Dictionary<string, Holding>();
            bool res = false;
            if (m_GlobalholdingStore.GetAllHoldings(out AllHoldings))
            {
                res = true;
            }
            AllHoldings1 = AllHoldings;
            return res;
        }

        private void RequestNUpdatePositioInfo()
        {
            try
            {
                m_GlobalpositionStore.updatedSymbols.Clear();
                var positionResponse = kite.GetPositions();
                foreach (var position in positionResponse.Day)
                {
                    try
                    {
                        m_GlobalpositionStore.AddOrUpdatePositionInfo(position.TradingSymbol + "_" + position.Product, position.Exchange, position, true);
                        Trace.WriteLine("Symbol = " + position.TradingSymbol + " Quanitity = " + position.Quantity);
                        //sanika::21-July-2021::Added logs
                        string temp =  "RequestNUpdatePositioInfo : symbol " + position.TradingSymbol + " Quanitity " + position.Quantity + " price " + position.AveragePrice;
                        if (UniquePositionWrite.ContainsKey(temp) == false)
                        {
                            WriteUniquelogs("Positions" + " " + m_UserID, temp, MessageType.Informational);
                            UniquePositionWrite[temp] = 0;
                        }
                    }
                    catch (Exception e)
                    {
                        //sanika::6-July-2021::added notification for exception in day position
                        AddNotificationInQueue("Not able to get day positions " + m_UserID + " " + DateTime.Now.ToString("HH:mm:ss"));
                        WriteUniquelogs("Positions" + " " + m_UserID, "RequestNUpdatePositioInfo : position day Exception Error Message " + e.Message, MessageType.Exception);
                    }
                }
                foreach (var position in positionResponse.Net)
                {
                    try
                    {
                        m_GlobalpositionStore.AddOrUpdatePositionInfo(position.TradingSymbol + "_" + position.Product, position.Exchange, position, false);
                        Trace.WriteLine("Symbol = " + position.TradingSymbol + " Quanitity = " + position.Quantity);
                        //sanika::21-July-2021::Added logs
                        string temp = "RequestNUpdatePositioInfo : symbol " + position.TradingSymbol + " Quanitity " + position.Quantity + " price " + position.AveragePrice;
                        if (UniquePositionWrite.ContainsKey(temp) == false)
                        {
                            WriteUniquelogs("Positions" + " " + m_UserID, temp, MessageType.Informational);
                            UniquePositionWrite[temp] = 0;
                        }
                    }
                    catch (Exception e)
                    {
                        //sanika::6-July-2021::added notification for exception in net position
                        AddNotificationInQueue("Not able to get net positions " + m_UserID + " " + DateTime.Now.ToString("HH:mm:ss"));
                        WriteUniquelogs("Positions" + " " + m_UserID, "RequestNUpdatePositioInfo : position net Exception Error Message " + e.Message, MessageType.Exception);
                    }
                }
                m_GlobalpositionStore.RemoveFromMasterList();
            }
            catch (Exception e)
            {
                string message = e.Message;
                if (message.Contains("Incorrect") && message.Contains("api_key") && message.Contains("access_token"))
                {
                    m_bneedToForcefullyLogin = true;
                }
                if (m_Icounter < 5)
                {
                    AddNotificationInQueue("position error Please check urgent " + m_userid);
                    m_Icounter++;
                }
                WriteUniquelogs("Positions" + " " + m_UserID, "RequestNUpdatePositioInfo : Exception Error Message" + e.Message, MessageType.Exception);
            }
        }
        static int m_Icounter = 0;
        public bool GetCurrentPosition(string mappedSymbol, string Product, out PositionInfo currentPosition)
        {
            bGetCurrentPositionError = false;
            PositionInfo pos = new PositionInfo();
            var symbol = mappedSymbol.Split('|');
            var newSymbol = "";
            try
            {
                if (symbol.Count() < 1)
                {
                    currentPosition = pos; // Assigning default values
                    bGetCurrentPositionError = true;
                    return false;
                }
                newSymbol = symbol[0] + "_" + Product + "|" + symbol[1];
                if (!m_GlobalpositionStore.GetPosition(newSymbol, out currentPosition))
                {
                    WriteUniquelogs(mappedSymbol, "GetCurrentPosition : Return false", MessageType.Informational);
                    return false;
                }
            }
            catch (DataException e)
            {
                bGetCurrentPositionError = true;
                currentPosition = pos; // Assigning default values
                WriteUniquelogs(symbol[0] + " " + m_UserID, "GetCurrentPosition : Can't get positions from server", MessageType.Exception);
                WriteUniquelogs(symbol[0] + " " + m_UserID, "GetCurrentPosition : Exception Error Message = " + e.Message, MessageType.Exception);
                return false;
            }
            return true;
        }

        public bool isReloadINI(string iniFilePath)
        {
            try
            {
                //Kite kite = new Kite();
                if (objConfigSettings == null)
                {
                    objConfigSettings = new ConfigSettings();
                }
                var path = Directory.GetCurrentDirectory();
                string iniFile = path + @"\Configuration\" + iniFilePath;
                objConfigSettings.readConfigFile(iniFile);
            }
            catch (Exception e)
            {
                logger.LogMessage("isReloadINI : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return true;
        }

        public double GetOpenPostionPrice(string TradingSymbol, string Exchange, string Product)
        {
            //WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPostionPrice :  In function", MessageType.Informational);
            double openprice = 0;
            try
            {
                string newSymbol = TradingSymbol + "_" + Product + "|" + Exchange;
                PositionInfo currentPosition;
                if (m_GlobalpositionStore.GetPosition(newSymbol, out currentPosition))
                {
                    if (currentPosition.dayPosition.TradingSymbol == TradingSymbol && currentPosition.dayPosition.Quantity != 0 && currentPosition.dayPosition.ClosePrice == 0)
                    {
                        if (currentPosition.dayPosition.Quantity > 0)
                        {
                            //buy
                            openprice = Convert.ToDouble(currentPosition.dayPosition.BuyPrice);
                        }
                        else if (currentPosition.dayPosition.Quantity < 0)
                        {
                            //sell
                            openprice = Convert.ToDouble(currentPosition.dayPosition.SellPrice);
                        }
                    }
                    if (currentPosition.netPosition.TradingSymbol == TradingSymbol && currentPosition.netPosition.Quantity != 0 && openprice == 0)
                    {

                        if (currentPosition.netPosition.Quantity > 0)
                        {
                            //buy
                            openprice = Convert.ToDouble(currentPosition.netPosition.BuyPrice);
                        }
                        else if (currentPosition.netPosition.Quantity < 0)
                        {
                            //sell
                            openprice = Convert.ToDouble(currentPosition.netPosition.SellPrice);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPostionPrice :  Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return openprice;
        }


        public string GetOpenPostionDirection(string TradingSymbol, string Exchange, String Product)
        {
            //WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPostionDirection :  In function", MessageType.Informational);
            string direction = "directionnotfound";
            try
            {
                string newSymbol = TradingSymbol + "_" + Product + "|" + Exchange;
                PositionInfo currentPosition;
                if (m_GlobalpositionStore.GetPosition(newSymbol, out currentPosition))
                {
                    if (currentPosition.dayPosition.TradingSymbol == TradingSymbol && currentPosition.dayPosition.Quantity != 0 && currentPosition.dayPosition.ClosePrice == 0)
                    {
                        if (currentPosition.dayPosition.Quantity > 0)
                        {
                            //buy
                            direction = "BUY";
                        }
                        else if (currentPosition.dayPosition.Quantity < 0)
                        {
                            //sell
                            direction = "SELL";
                        }
                    }
                    //sanika::16-sep-2020::changed "" for client issue
                    if (currentPosition.netPosition.TradingSymbol == TradingSymbol && currentPosition.netPosition.Quantity != 0 && direction == "directionnotfound")
                    {

                        if (currentPosition.netPosition.Quantity > 0)
                        {
                            //buy
                            direction = "BUY";
                        }
                        else if (currentPosition.netPosition.Quantity < 0)
                        {
                            //sell
                            direction = "SELL";
                        }
                    }
                }
                else
                {
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPostionDirection :  Not able to get position information", MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPostionDirection :  Exception Error Message = " + e.Message, MessageType.Exception);
            }
            //WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPostionDirection :  direction = " + direction, MessageType.Informational);
            return direction;
        }

        public int GetOpenPostionQuantity(string TradingSymbol, string Exchange, string Product)
        {
            //WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPostionQuantity :  In function", MessageType.Informational);
            int quantity = 0;
            try
            {
                string newSymbol = TradingSymbol + "_" + Product + "|" + Exchange;
                PositionInfo currentPosition;
                if (m_GlobalpositionStore.GetPosition(newSymbol, out currentPosition))
                {
                    if (currentPosition.dayPosition.TradingSymbol == TradingSymbol && currentPosition.dayPosition.Quantity != 0 && currentPosition.dayPosition.ClosePrice == 0)
                    {
                        quantity = Math.Abs(currentPosition.dayPosition.Quantity);
                    }
                    if (currentPosition.netPosition.TradingSymbol == TradingSymbol && currentPosition.netPosition.Quantity != 0 && quantity == 0)
                    {
                        quantity = Math.Abs(currentPosition.netPosition.Quantity);
                    }
                }

            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPostionQuantity :  Exception Error Message = " + e.Message, MessageType.Exception);
            }
            WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPostionQuantity :  quantity = " + quantity, MessageType.Informational);
            return quantity;
        }

        public int GetOpenPostionQuantityByOrderID(string TradingSymbol, string Exchange, string orderId)
        {
            int quantity = 0;
            try
            {
                OrderExt orderExt;
                if (m_GlobalOrderStore.GetOpenOrderbyID(TradingSymbol, Exchange, orderId, out orderExt))
                {
                    if (orderExt == null)
                    {
                        WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPostionQuantityByOrderID : order info count is zero", MessageType.Informational);
                        return 0;
                    }

                    quantity = Math.Abs(orderExt.order.Quantity);

                    //WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPostionQuantityByOrderId : quantity " + quantity, MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPostionQuantityByOrderID : Exception Error Message = " + e.Message, MessageType.Informational);
            }
            return quantity;
        }

        public string FetchTableName()
        {
            return kite.FetchTableName();
        }

        public string getTickCurrentTime()
        {
            return kite.tickCurrentTime;
        }

        public Dictionary<string, List<string>> getFinalDictionary()
        {
            return kite.FinalZerodhaData;
        }

        public string GetLastTradedPrice(string TradingSymbol)
        {
            string ltp = "";
            try
            {
                if (m_TickData != null)
                {
                    decimal lastPrice = m_TickData.GetLastPrice(TradingSymbol);
                    if (lastPrice == 0)
                        ltp = "";
                    else
                    {
                        ltp = lastPrice.ToString();
                    }
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetLastTradedPrice : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return ltp;
        }

        public bool GetHighLow(string TradingSymbol, int barCount, out double dHigh, out double dLow)
        {
            dHigh = 0;
            dLow = 0;
            kite.GetHighLow(TradingSymbol, barCount, out dHigh, out dLow);
            return true;
        }

        public bool GetCloseValuesList(string TradingSymbol, int barCount, out List<double> closeValues, out List<string> dateTimevalues)
        {
            kite.GetCloseValuesList(TradingSymbol, barCount, out closeValues, out dateTimevalues);
            return true;
        }
        public PositionResponse GetPositions()
        {
            return kite.GetPositions();
        }

        public bool OpenConnection()
        {
            try
            {
                //db created at absolute path
                var path_DB = Directory.GetCurrentDirectory();
                string directoryName = path_DB + "\\" + "Database";

                if (!Directory.Exists(directoryName))
                    Directory.CreateDirectory(directoryName);

                string DBFilePath = directoryName + "\\" + DBName;

                if (!File.Exists(DBFilePath))
                {
                    SQLiteConnection.CreateFile(DBFilePath);
                    Thread.Sleep(100);
                }

                string connectionString = String.Format("Data Source={0};Version=3;New=False;Compress=True;", DBFilePath);
                sql_con = new SQLiteConnection(connectionString);
                //open sqlite connection
                sql_con.Open();
            }
            catch (Exception e)
            {
                logger.LogMessage("OpenConnection : Exception Error Message =" + e.Message, MessageType.Exception);
                return false;
            }

            return true;
        }

        bool CloseConnection()
        {
            if (sql_con == null)
            {
                return false;
            }
            sql_con.Close();
            return true;
        }

        //sanika::18-sep-2020::added redesign function
        public bool FetchInstrumentDetailsFromCSV()
        {
            m_InsertQueries.Clear();
            bool isdbConnected = false;
            bool result = false;
            logger.LogMessage("$$$ Started for inserting data = " + DateTime.Now, MessageType.Informational);
            bool isopen = false;
            SqliteConnectZerodha sqliteConnectZerodha = new SqliteConnectZerodha();
            try
            {
                isdbConnected = sqliteConnectZerodha.Connect(DBName);
                {
                    //file read from absolute path
                    var path_File = Directory.GetCurrentDirectory();
                    string directoryName = path_File + "\\" + "InstrumentList";

                    if (!Directory.Exists(directoryName))
                        Directory.CreateDirectory(directoryName);

                    string csvFilePath = directoryName + "\\" + insFileName;

                    if (!File.Exists(csvFilePath))
                    {
                        logger.LogMessage("FetchInstrumentDetailsFromCSV : Instruments.csv file does not exist", MessageType.Error);
                    }
                    else
                    {
                        //check table exist or not if not then create
                        if (!m_ListOfTableExistOrNot.Contains("instruments"))
                        {
                            if (isdbConnected)
                            {
                                if (sqliteConnectZerodha.IsTableExists("instruments") == false)
                                {
                                    string strQuery = "CREATE TABLE instruments (symbol varchar(20) NOT NULL, lotsize int NOT NULL, tickSize real NOT NULL,InstrumentToken read PRIMARY KEY,exchange varchar(10) NOT NULL)";
                                    sqliteConnectZerodha.ExecuteNonQueryCommand(strQuery);
                                }
                                m_ListOfTableExistOrNot.Add("instruments");
                            }
                        }
                        StreamReader sr = new StreamReader(csvFilePath);
                        //sanika::21-Jul-2021::Added changes for inserting like composite
                        int count = 0;
                        while (!sr.EndOfStream)
                        {
                            count++;
                            string line = sr.ReadLine();
                            //handled empty line exception
                            if (line != null)
                            {
                                value = line.Split(',');
                                if (value.Length >= 8)
                                {
                                    if (value[9] == "EQ" && objConfigSettings.CheckNSE == true)//sanika::20-Nov-2020::added condition to check allow insertion or not
                                    {
                                        if (m_ListOfTableExistOrNot.Contains("instruments"))
                                        {
                                            string InstrumentToken = "";
                                            if (value[10] == "NSE")
                                            {
                                                InstrumentToken = value[0];
                                            }
                                            else if (value[10] == "INDICES")
                                            {
                                                InstrumentToken = value[0];
                                            }
                                            //sanika::18-sep-2020::commented because bse symbols also inserted
                                            //else
                                            //{
                                            //    InstrumentToken = value[1];
                                            //}
                                            string txtSQLQuery = "INSERT OR REPLACE into instruments (symbol, lotsize, tickSize,InstrumentToken,exchange) values ('" + value[2] + "','" + value[8] + "','" +  value[7] + "','" + InstrumentToken + "','" + value[10] + "')";
                                            if (!m_InsertQueries.Contains(txtSQLQuery))
                                                m_InsertQueries.Add(txtSQLQuery);
                                        }
                                    }
                                    if (value[9] == "FUT" && value[10] == "NFO-FUT" && objConfigSettings.CheckNFO == true)//sanika::20-Nov-2020::added condition to check allow insertion or not
                                    {
                                        string instrumentToken = "";
                                        string futureSymbolList = value[2];
                                        string futureSymbolsTableName = futureSymbolList.Substring(futureSymbolList.Length - 8);
                                        string futureSymbol = futureSymbolList;


                                        getSymbolLotSize = Convert.ToInt32(value[8]);
                                        //price = Convert.ToDecimal(value[4]);
                                        tickSize = Convert.ToDecimal(value[7]);
                                        instrumentToken = value[0];

                                        FuturetableCreation(futureSymbolsTableName, futureSymbol, getSymbolLotSize, tickSize, instrumentToken, value[11], isdbConnected, sqliteConnectZerodha);
                                    }
                                    //IRDSPM::Pratiksha::For CDS
                                    //if (value[9] == "FUT" && (value[10] == "CDS-FUT" || value[10] == "CDS-OPT") && objConfigSettings.CheckCDS == "true")
                                    if ((value[10] == "CDS-FUT" || value[10] == "CDS-OPT") && objConfigSettings.CheckCDS == true)
                                    {
                                        string instrumentToken = "";
                                        string futureSymbolList = value[2];
                                        string futureSymbolsTableName = "Currency";
                                        string futureSymbol = futureSymbolList;


                                        getSymbolLotSize = Convert.ToInt32(value[8]);
                                        //price = Convert.ToDecimal(value[4]);
                                        tickSize = Convert.ToDecimal(value[7]);
                                        instrumentToken = value[0];

                                        FuturetableCreation(futureSymbolsTableName, futureSymbol, getSymbolLotSize, tickSize, instrumentToken, value[11], isdbConnected, sqliteConnectZerodha);
                                    }
                                    if (value[9] == "FUT" && value[10] == "MCX-FUT" && objConfigSettings.CheckMCX == true)//sanika::20-Nov-2020::added condition to check allow insertion or not
                                    {
                                        string instrumentToken = "";
                                        string futureSymbolList = value[2];
                                        string futureSymbolsTableName = futureSymbolList.Substring(futureSymbolList.Length - 8);
                                        string futureSymbol = futureSymbolList;


                                        getSymbolLotSize = Convert.ToInt32(value[8]);
                                       // price = Convert.ToDecimal(value[4]);
                                        tickSize = Convert.ToDecimal(value[7]);
                                        instrumentToken = value[0];

                                        FuturetableCreation(futureSymbolsTableName, futureSymbol, getSymbolLotSize, tickSize, instrumentToken, value[11], isdbConnected, sqliteConnectZerodha);
                                    }
                                    //IRDS::Jyoti::9-Sept-20::Added for options symbols
                                    if (value[9] != "FUT" && value[10] == "NFO-OPT" && objConfigSettings.CheckOptions == true)//sanika::20-Nov-2020::added condition to check allow insertion or not
                                    {
                                        string instrumentToken = "";
                                        string futureSymbolList = value[3];
                                        string futureSymbolsTableName = "OPTIONS";
                                        string futureSymbol = futureSymbolList;
                                        string otpSymbolcomplete = value[2];

                                        getSymbolLotSize = Convert.ToInt32(value[8]);
                                      //  price = Convert.ToDecimal(value[4]);
                                        tickSize = Convert.ToDecimal(value[7]);
                                        instrumentToken = value[0];
                                        DateTime date = Convert.ToDateTime(value[5]);
                                        string expiry_date = date.ToString("yyyy-MM-dd"); //(DateTime.ParseExact(date.ToString(), "yyyy-MM-ddTHH:mm:ss", CultureInfo.InvariantCulture)).ToString(); //date.ToUniversalTime().ToString();
                                        OptionstableCreation(futureSymbolsTableName, futureSymbol, otpSymbolcomplete, getSymbolLotSize, tickSize, instrumentToken, value[11], expiry_date + "T14:30:00", isdbConnected, sqliteConnectZerodha);
                                    }

                                    //20-July-2021: sandip: insert values after every 2000 count.faster execution
                                    if (isdbConnected && count % 2000 == 0)
                                    {
                                        //Trace.WriteLine("Sandip  InsertDataMultipleValues ");
                                        sqliteConnectZerodha.InsertDataMultipleValues("", m_InsertQueries);
                                        m_InsertQueries.Clear();
                                    }
                                }
                            }
                        }
                        if (isdbConnected)
                            sqliteConnectZerodha.InsertDataMultipleValues("", m_InsertQueries);
                    }
                }
                result = true;
            }
            catch (Exception e)
            {
                result = false;
                logger.LogMessage("FetchInstrumentDetailsFromCSV : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            finally
            {
                if (isdbConnected)
                {
                    sqliteConnectZerodha.CloseConnection();
                }
            }
            logger.LogMessage("$$$ End for inserting data = " + DateTime.Now, MessageType.Informational);
            return result;
        }

        public bool TableExists(string Table_Name)
        {
            using (SQLiteTransaction mytransaction = sql_con.BeginTransaction())
            {
                using (sql_cmd = new SQLiteCommand(sql_con))
                {
                    sql_cmd.CommandText = "SELECT * FROM sqlite_master WHERE type='table' AND name=@name";
                    sql_cmd.Parameters.AddWithValue("@name", Table_Name);

                    using (sql_read = sql_cmd.ExecuteReader())
                    {
                        if (sql_read != null && sql_read.HasRows)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }
        }

        public bool TableDataExists(string Table_Name)
        {
            SQLiteCommand sql_cmd;
            using (sql_cmd = new SQLiteCommand(sql_conData))
            {
                sql_cmd.CommandText = "SELECT * FROM sqlite_master WHERE type='table' AND name=@name";
                sql_cmd.Parameters.AddWithValue("@name", Table_Name);

                using (sql_read = sql_cmd.ExecuteReader())
                {
                    if (sql_read != null && sql_read.HasRows)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }

        //IRDS::Jyoti::12-Sept-20::Added exchange for symbol settings form
        public void FuturetableCreation(string tableName, string symbolValue, int lotsize, decimal tick_Size, string instrumentToken, string exchange, bool isConnected, SqliteConnectZerodha sqliteConnectZerodha)
        {
            try
            {
                if (isConnected)
                {
                    //check if table is already exists or not
                    if (!m_ListOfTableExistOrNot.Contains(tableName))
                    {
                        if (sqliteConnectZerodha.IsTableExists(tableName) == false)
                        {
                            string strQuery = "CREATE TABLE '" + tableName + "' (symbol varchar(20) NOT NULL, lotsize int NOT NULL,  tickSize real NOT NULL,InstrumentToken read PRIMARY KEY,exchange varchar(10) NOT NULL)";
                            sqliteConnectZerodha.ExecuteNonQueryCommand(strQuery);
                        }
                        m_ListOfTableExistOrNot.Add(tableName);
                    }
                    string insertSQLQuery = "INSERT OR REPLACE into '" + tableName + "' (symbol, lotsize, tickSize, InstrumentToken, exchange) values ('" + symbolValue + "','" + lotsize + "','" +  tick_Size + "','" + instrumentToken + "','" + exchange + "')";
                    if (!m_InsertQueries.Contains(insertSQLQuery))
                        m_InsertQueries.Add(insertSQLQuery);
                }
            }
            catch (Exception e)
            {
                logger.LogMessage("FuturetableCreation : Exception Error Message = " + e.Message, MessageType.Informational);
            }
        }
        public void OptionstableCreation(string tableName, string symbolValue, string otpSymbolcomplete, int lotsize,  decimal tick_Size, string instrumentToken, string exchange, string expiry_date, bool isConnected, SqliteConnectZerodha sqliteConnectZerodha)
        {
            try
            {
                if (isConnected)
                {
                    //check if table is already exists or not
                    if (!m_ListOfTableExistOrNot.Contains(tableName))
                    {
                        if (sqliteConnectZerodha.IsTableExists(tableName) == false)
                        {
                            //IRDSPM::Pratiksha::14-06-2021::addded new field expiryDate 
                            string strQuery = "CREATE TABLE '" + tableName + "' (symbol varchar(20) NOT NULL, optionsSymbol varchar(20) NOT NULL, lotsize int NOT NULL, tickSize real NOT NULL,InstrumentToken read PRIMARY KEY,exchange varchar(10) NOT NULL,expiryDate varchar(25) NOT NULL)";
                            sqliteConnectZerodha.ExecuteNonQueryCommand(strQuery);
                        }
                        m_ListOfTableExistOrNot.Add(tableName);
                    }
                    string insertSQLQuery = "INSERT OR REPLACE into '" + tableName + "' (symbol, optionsSymbol, lotsize, tickSize, InstrumentToken, exchange, expiryDate) values ('" + symbolValue + "','" + otpSymbolcomplete + "','" + lotsize + "','" + tick_Size + "','" + instrumentToken + "','" + exchange + "','" + expiry_date + "')";
                    if (!m_InsertQueries.Contains(insertSQLQuery))
                        m_InsertQueries.Add(insertSQLQuery);
                }
            }
            catch (Exception e)
            {
                logger.LogMessage("OptiontableCreation : Exception Error Message = " + e.Message, MessageType.Informational);
            }
        }

        public void LoadSymbolWithLotandTickInDict()
        {
            try
            {
                if (OpenConnection())
                {
                    //IRDS::Sandip::5-July-18::clear the dictionary
                    futureSymbolAndLotSize.Clear();
                    dictSymbolwithtickSize.Clear();
                    getCurrentMonth = dt.ToString("MMM").ToUpper();
                    getCurrentYear = dt.ToString("yy");
                    string getFromFutTableName = getCurrentYear + getCurrentMonth + findElement;
                    string sql_select = "";

                    //IRDS::05-July-2018::Archana::Need to check for first time when DB created with no tables
                    if (TableExists(getFromFutTableName))
                    {
                        sql_select = "select symbol, lotsize, tickSize from '" + getFromFutTableName + "'";
                        sql_cmd = new SQLiteCommand(sql_select, sql_con);
                        sql_read = sql_cmd.ExecuteReader();

                        if (sql_read != null && sql_read.HasRows)
                        {
                            while (sql_read.Read())
                            {
                                getFutSymbol = (string)sql_read["symbol"];
                                getFutSymbolLotSize = (int)sql_read["lotsize"];

                                //IRDS::04-July-2018::Bhagyashri::Getting tickSize from future table
                                getFutSymboltickSize = Convert.ToDecimal((double)sql_read["tickSize"]);

                                if (!futureSymbolAndLotSize.ContainsKey(getFutSymbol))
                                {
                                    futureSymbolAndLotSize.Add(getFutSymbol, getFutSymbolLotSize);
                                }

                                //IRDS::04-July-2018::Bhagyashri::Added tickSize into dictSymbolwithtickSize from future table
                                if (!dictSymbolwithtickSize.ContainsKey(getFutSymbol))
                                {
                                    dictSymbolwithtickSize.Add(getFutSymbol, getFutSymboltickSize);
                                }
                            }
                        }
                    }
                    else
                    {
                        logger.LogMessage(getFromFutTableName + "LoadSymbolWithLotandTickInDict :  table does not exists", MessageType.Error);
                        string sql_new = "CREATE TABLE '" + getFromFutTableName + "'(symbol varchar(20) NOT NULL, lotsize int NOT NULL, price real NOT NULL, tickSize real NOT NULL,InstrumentToken read NOT NULL,exchange varchar(10) NOT NULL)";
                        sql_cmd = new SQLiteCommand(sql_new, sql_con);
                        sql_cmd.ExecuteNonQuery();
                        logger.LogMessage(getFromFutTableName + "LoadSymbolWithLotandTickInDict :  table created", MessageType.Informational);
                    }

                    if (TableExists("instruments"))
                    {
                        sql_select = "select symbol, tickSize from instruments";
                        sql_cmd = new SQLiteCommand(sql_select, sql_con);
                        sql_read = sql_cmd.ExecuteReader();

                        if (sql_read != null && sql_read.HasRows)
                        {
                            while (sql_read.Read())
                            {
                                getFutSymbol = (string)sql_read["symbol"];
                                getFutSymboltickSize = Convert.ToDecimal((double)sql_read["tickSize"]);

                                if (!dictSymbolwithtickSize.ContainsKey(getFutSymbol))
                                {
                                    dictSymbolwithtickSize.Add(getFutSymbol, getFutSymboltickSize);
                                }
                            }
                        }
                    }
                    else
                    {
                        //table created without primary key
                        //IRDS :: Nayana :: 17-Apr-2019 :: Added one more column InstumentToken 
                        string sql = "CREATE TABLE instruments (symbol varchar(20) NOT NULL, lotsize int NOT NULL, price real NOT NULL, tickSize real NOT NULL , InstrumentToken read NOT NULL)";
                        sql_cmd = new SQLiteCommand(sql, sql_con);
                        sql_cmd.ExecuteNonQuery();
                        logger.LogMessage("LoadSymbolWithLotandTickInDict : instruments table created", MessageType.Informational);
                    }

                    if (sql_read != null) //IRDS::Sandip::5-July-18::close after use
                        sql_read.Close();

                    if (sql_con != null)
                        sql_con.Close();
                }
                else
                {
                    logger.LogMessage(m_UserID + "LoadSymbolWithLotandTickInDict :  SQL Connection is not opened", MessageType.Exception);
                }
            }

            catch (Exception e)
            {
                logger.LogMessage("LoadSymbolWithLotandTickInDict : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        public decimal RoundStopLossValueOfAFL(string futSymbol, decimal normalValue, bool flag)
        {
            decimal tickSizeForFutSymbol = 0.05m;
            decimal newRoundedPrice;
            decimal precisionCalcPrice;

            if (dictSymbolwithtickSize.ContainsKey(futSymbol))
            {
                dictSymbolwithtickSize.TryGetValue(futSymbol, out tickSizeForFutSymbol);
            }
            else
            {
                WriteUniquelogs(futSymbol + " " + m_UserID, "RoundStopLossValueOfAFL : Symbol is not present in the dictionary : " + futSymbol, MessageType.Error);
                // return normalValue;
            }

            //IRDS::06-July-2018::Bhagyashri::True for BUY i.e. RoundUp the market value
            if (flag == true)
            {
                if (tickSizeForFutSymbol > 0)
                    precisionCalcPrice = tickSizeForFutSymbol * Math.Round((tickSizeForFutSymbol + normalValue) / tickSizeForFutSymbol);
                else
                    precisionCalcPrice = normalValue;

                newRoundedPrice = Math.Abs(precisionCalcPrice);
            }
            //IRDS::06-July-2018::Bhagyashri::else for SELL i.e. RoundDown the market value
            else
            {
                if (tickSizeForFutSymbol > 0)
                    precisionCalcPrice = tickSizeForFutSymbol * Math.Floor((tickSizeForFutSymbol - normalValue) / tickSizeForFutSymbol);
                else
                    precisionCalcPrice = normalValue;

                newRoundedPrice = Math.Abs(precisionCalcPrice);
            }

            return newRoundedPrice;
        }
        public List<string> marketWatchdeletelistmain = new List<string>();
        public void CheckTradingSymbolPresentOrNot(List<string> list)
        {
            kite.CheckTradingSymbolPresentOrNot(list);
        }

        public List<string> getOHCLValues(string TradingSymbol)
        {
            try
            {
                if (kite.FinalZerodhaData.ContainsKey(TradingSymbol))
                {
                    return kite.FinalZerodhaData[TradingSymbol];
                }
                return null;
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "getOHCLValues : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return null;
        }

        public string GetBidPrice(string TradingSymbol)
        {
            string bidPrice = null;
            try
            {
                if (m_TickData != null)
                {
                    bidPrice = m_TickData.GetBidPrice(TradingSymbol).ToString();
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetBidPrice : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return bidPrice;
        }

        public string GetOpenPrice(string TradingSymbol)
        {
            string openPrice = null;
            try
            {
                if (m_TickData != null)
                {
                    openPrice = m_TickData.GetOpenPrice(TradingSymbol).ToString();
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPrice : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return openPrice;
        }

        public string GetLowPrice(string TradingSymbol)
        {
            string lowPrice = null;
            try
            {
                if (m_TickData != null)
                {
                    lowPrice = m_TickData.GetLowPrice(TradingSymbol).ToString();
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetLowPrice : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return lowPrice;
        }

        public string GetHighPrice(string TradingSymbol)
        {
            string highPrice = null;
            try
            {
                if (m_TickData != null)
                {
                    highPrice = m_TickData.GetHighPrice(TradingSymbol).ToString();
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetHighPrice : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return highPrice;
        }

        public string GetChangeInPercent(string TradingSymbol)
        {
            string changeInPercent = null;
            try
            {
                if (m_TickData != null)
                {
                    changeInPercent = m_TickData.GetChangeInPercentage(TradingSymbol).ToString();
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetChangeInPercent : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return changeInPercent;
        }

        //sanika::08-oct-2020::Added to get absolute change
        public string GetChangeInAbsolute(string TradingSymbol)
        {
            string changeInAbsolute = null;
            try
            {
                if (m_TickData != null)
                {
                    changeInAbsolute = m_TickData.GetChangeInAbsolute(TradingSymbol).ToString();
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetChangeInAbsolute : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return changeInAbsolute;
        }

        public string GetAskPrice(string TradingSymbol)
        {
            string askPrice = null;
            try
            {
                if (m_TickData != null)
                {
                    askPrice = m_TickData.GetAskPrice(TradingSymbol).ToString();
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetAskPrice : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return askPrice;
        }

        public string GetVolumePrice(string TradingSymbol)
        {
            string volume = null;
            try
            {
                if (m_TickData != null)
                {
                    volume = m_TickData.GetVolume(TradingSymbol).ToString();
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetVolumePrice : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return volume;
        }

        public string GetClosePrice(string TradingSymbol)
        {
            string closePrice = "";
            try
            {
                if (m_TickData != null)
                {
                    closePrice = m_TickData.GetClosePrice(TradingSymbol).ToString();
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetClosePrice : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return closePrice;
        }

        public string getVariety(string TradingSymbol, string Exchange)
        {
            List<Order> orderInfo = m_GlobalOrderStore.GetOrderbySymbol(TradingSymbol, Exchange);
            if (orderInfo.Count == 0)
            {
                string errorMsg = "getVariety : Failed to get order history for TradingSymbol - " + TradingSymbol;
                WriteUniquelogs(TradingSymbol + " " + m_UserID, errorMsg, MessageType.Error);

                latestOrder = new IRDSAlgoOMS.Order();
                return null;
            }

            latestOrder = orderInfo[orderInfo.Count - 1];
            WriteUniquelogs(TradingSymbol + " " + m_UserID, "getVariety : " + latestOrder.Variety, MessageType.Informational);
            return latestOrder.Variety;
        }

        public Dictionary<string, List<string>> getOHCLDictionary()
        {
            if (kite.FinalZerodhaData.Count > 0)
            {
                return kite.FinalZerodhaData;
            }
            else
            {
                return null;
            }
        }

        public double GetOpenPostionPricebyOrderID(string TradingSymbol, string Exchange, string orderId)
        {
            double Price = 0;
            try
            {
                OrderExt orderExt;
                if (m_GlobalOrderStore.GetOpenOrderbyID(TradingSymbol, Exchange, orderId, out orderExt))
                {
                    if (orderExt == null)
                    {
                        WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPostionPricebyOrderID : order info count is zero", MessageType.Informational);
                        return 0;
                    }
                    if (orderExt.order.Status == Constants.ORDER_STATUS_COMPLETE)
                    {
                        Price = Convert.ToDouble(orderExt.order.AveragePrice);
                    }
                    // WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPostionPricebyOrderID : Price " + Price, MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPostionPricebyOrderID : Exception Error Message = " + e.Message, MessageType.Informational);
            }
            return Price;
        }

        public double GetStopPricebyOrderID(string TradingSymbol, string Exchange, string orderId)
        {
            double Price = 0;
            try
            {
                OrderExt orderExt;
                if (m_GlobalOrderStore.GetOpenOrderbyID(TradingSymbol, Exchange, orderId, out orderExt))
                {
                    if (orderExt == null)
                    {
                        WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPostionPricebyOrderID : order info count is zero", MessageType.Informational);
                        return 0;
                    }
                    if (orderExt.order.Status == Constants.ORDER_STATUS_PENDING)
                    {
                        Price = Convert.ToDouble(orderExt.order.TriggerPrice);
                    }
                    //WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPostionPricebyOrderID : Price " + Price, MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPostionPricebyOrderID : Exception Error Message = " + e.Message, MessageType.Informational);
            }
            return Price;
        }

        public string GetOpenPostionDirectionByOrderId(string TradingSymbol, string Exchange, string orderId)
        {
            string direction = "directionnotfound";
            try
            {
                OrderExt orderExt;
                if (m_GlobalOrderStore.GetOpenOrderbyID(TradingSymbol, Exchange, orderId, out orderExt))
                {
                    if (orderExt == null)
                    {
                        WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPostionDirectionByOrderId : order info count is zero", MessageType.Informational);
                        return direction;
                    }
                    //if (orderExt.order.Status == Constants.ORDER_STATUS_COMPLETE) //sanika::18-Feb-2021::Not getting direction for sl and limit order
                    {
                        direction = orderExt.order.TransactionType;
                    }
                    //WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPostionDirectionByOrderId : direction " + direction, MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPostionDirectionByOrderId : Exception Error Message = " + e.Message, MessageType.Informational);
            }
            return direction;
        }

        public string GetOrderStatusByOrderID(string TradingSymbol, string Exchange, string orderId)
        {
            string status = "ordernotupdated";
            string orderType = "";
            try
            {
                int counter = 0;
                OrderExt orderExt;
                while (counter <= 1)
                {
                    if (orderId == null)
                        orderId = "";
                    if (m_GlobalOrderStore.GetOpenOrderbyID(TradingSymbol, Exchange, orderId, out orderExt))
                    {
                        if (orderExt == null)
                        {
                            WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOrderStatusByOrderID : order info count is zero", MessageType.Informational);
                            return status;
                        }

                        if (orderExt.order.Status != "" && orderExt.order.Status != null)
                        {
                            status = orderExt.order.Status;
                            orderType = orderExt.order.OrderType;
                        }

                        WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOrderStatusByOrderID : orderType " + orderType + " status " + status, MessageType.Informational);
                    }
                    if (status != "ordernotupdated")
                    {
                        break;
                    }
                    Thread.Sleep(10);
                    counter++;
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOrderStatusByOrderID : Exception Error Message = " + e.Message, MessageType.Informational);
            }
            return status;
        }

        //sanika::21-Jul-2021::Changed function as it is commented 
        public double GetOpenPostionPricebySymbol(string TradingSymbol, string Exchange)
        {
            double Price = 0;
            try
            {
                OrderExt orderExt;
                List<Order> orderStore = m_GlobalOrderStore.GetOpenOrderbySymbol(TradingSymbol, Exchange);

                if (orderStore == null && orderStore.Count == 0)
                {
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPostionPricebyOrderID : order info count is zero", MessageType.Informational);
                    return 0;
                }
                foreach (var order in orderStore)
                {
                    if (order.Status == Constants.ORDER_STATUS_COMPLETE)
                    {
                        Price = Convert.ToDouble(order.AveragePrice);
                    }
                }
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPostionPricebyOrderID : Price " + Price, MessageType.Informational);

            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPostionPricebyOrderID : Exception Error Message = " + e.Message, MessageType.Informational);
            }
            return Price;
        }

        public double GetStopPrice(string TradingSymbol, string Exchange, string TransactionType)
        {
            double stopPrice = 0;
            List<Order> orderInfo = m_GlobalOrderStore.GetOrderbySymbol(TradingSymbol, Exchange);

            if (orderInfo.Count == 0)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetStopPrice : order info count is zero", MessageType.Informational);
                return 0;
            }
            for (int i = 0; i < orderInfo.Count(); i++)
            {
                latestOrder = orderInfo[i];
                if (latestOrder.TransactionType == TransactionType)
                {
                    if (latestOrder.Status == Constants.ORDER_STATUS_PENDING)
                    {
                        stopPrice = Convert.ToDouble(latestOrder.TriggerPrice);
                        break;
                    }
                }
            }
            WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetStopPrice : stopPrice " + stopPrice, MessageType.Informational);
            return stopPrice;
        }

        public double GetLimitPrice(string TradingSymbol, string Exchange, string TransactionType)
        {
            double limitPrice = 0;
            List<Order> orderInfo = m_GlobalOrderStore.GetOrderbySymbol(TradingSymbol, Exchange);

            if (orderInfo.Count == 0)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetLimitPrice :  order info count is zero", MessageType.Informational);
                return 0;
            }
            for (int i = 0; i < orderInfo.Count(); i++)
            {
                latestOrder = orderInfo[i];
                if (latestOrder.TransactionType == TransactionType)
                {
                    if (latestOrder.Status == Constants.ORDER_STATUS_OPEN)
                    {
                        limitPrice = Convert.ToDouble(latestOrder.Price);
                        break;
                    }
                }
            }
            WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetLimitPrice : limitPrice " + limitPrice, MessageType.Informational);
            return limitPrice;
        }

        public string GetTransactionType(string TradingSymbol, string Exchange)
        {
            string BuyOrSell = null; ;
            try
            {
                string newSymbol = TradingSymbol + "_" + Constants.PRODUCT_MIS + "|" + Exchange;
                PositionInfo currentPosition;
                if (m_GlobalpositionStore.GetPosition(newSymbol, out currentPosition))
                {
                    if (currentPosition.dayPosition.TradingSymbol == TradingSymbol && currentPosition.dayPosition.Quantity != 0)
                    {
                        if (currentPosition.dayPosition.Quantity > 0)
                        {
                            //buy
                            BuyOrSell = Constants.TRANSACTION_TYPE_BUY;
                        }
                        else if (currentPosition.dayPosition.Quantity < 0)
                        {
                            //sell
                            BuyOrSell = Constants.TRANSACTION_TYPE_SELL;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetTransactionType :  Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return BuyOrSell;
        }

        public bool OpenConnectionForLOtSize()
        {
            try
            {
                //db created at absolute path
                var path_DB = Directory.GetCurrentDirectory();
                string directoryName = path_DB + "\\" + "Database";

                if (!Directory.Exists(directoryName))
                    Directory.CreateDirectory(directoryName);

                string DBFilePath = directoryName + "\\" + DBName;

                if (!File.Exists(DBFilePath))
                {
                    SQLiteConnection.CreateFile(DBFilePath);
                    Thread.Sleep(100);
                }

                string connectionString = String.Format("Data Source={0};Version=3;New=False;Compress=True;", DBFilePath);
                sql_conLotSize = new SQLiteConnection(connectionString);
                //open sqlite connection
                sql_conLotSize.Open();
            }
            catch (Exception e)
            {
                logger.LogMessage("OpenConnectionForLOtSize : Exception Error Message = " + e.Message, MessageType.Exception);
                return false;
            }

            return true;
        }

        public int GetLotSize(string TradingSymbol, string Exchange)
        {
            int lotsize = 0;
            try
            {
                if (sql_conLotSize == null)
                {
                    OpenConnectionForLOtSize();
                }

                string tableName = FetchTableName();

                //sanika::19-oct-2020::added for nfo symbols
                string symbol = "";
                if (Exchange == Constants.EXCHANGE_NFO)
                    symbol = TradingSymbol;
                else
                    symbol = TradingSymbol + tableName;

                //TradingSymbol;// + tableName; 

                string sql = "SELECT lotsize from '" + tableName + "' WHERE symbol = '" + symbol + "'";
                sql_cmd = new SQLiteCommand(sql, sql_conLotSize);
                sql_read = sql_cmd.ExecuteReader();

                if (sql_read != null && sql_read.HasRows)
                {
                    if (sql_read.Read())
                    {
                        lotsize = (int)sql_read["lotsize"];
                    }
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetLotSize :  Exception " + e.Message, MessageType.Exception);
            }
            WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetLotSize :  lotsize " + lotsize, MessageType.Informational);
            return lotsize;
        }

        public int GetMonthLotSize(string TradingSymbol, string Exchange, string MonthFutureName)
        {
            int lotsize = 0;
            try
            {
                if (sql_conLotSize == null)
                {
                    OpenConnectionForLOtSize();
                }

                string symbol = TradingSymbol + MonthFutureName;

                string sql = "SELECT lotsize from '" + MonthFutureName + "' WHERE symbol = '" + symbol + "'";
                sql_cmd = new SQLiteCommand(sql, sql_conLotSize);
                sql_read = sql_cmd.ExecuteReader();

                if (sql_read != null && sql_read.HasRows)
                {
                    if (sql_read.Read())
                    {
                        lotsize = (int)sql_read["lotsize"];
                    }
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetMonthLotSize :  Exception Error Message =" + e.Message, MessageType.Exception);
            }
            WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetMonthLotSize :  lotsize " + lotsize, MessageType.Informational);
            return lotsize;
        }


        private static readonly object databaselock = new object();
        public void WriteDataIntoFile(string TradingSymbol, string Datetime, double Open, double High, double Low, double Close, double Volume, bool bWriteinDB, bool bCommit)
        {
            try
            {
                int firstSpaceIndex = Datetime.IndexOf(" ");
                string d = Datetime.Substring(0, firstSpaceIndex); // INAGX4
                string ti = Datetime.Substring(firstSpaceIndex + 1); // Agatti Island
                string[] spliitedDateTime = Datetime.Split(' ');
                string date = Convert.ToDateTime(d).ToString("yyyy-MM-dd");
                DateTime datetime = DateTime.Parse(ti, System.Globalization.CultureInfo.CurrentCulture);
                string t = datetime.ToString("HH:mm:00");
                string path = m_path + "//Data//" + TradingSymbol + ".txt";
                Open = Convert.ToDouble(RoundStopLossValueOfAFL(TradingSymbol, Convert.ToDecimal(Open), true));
                High = Convert.ToDouble(RoundStopLossValueOfAFL(TradingSymbol, Convert.ToDecimal(High), false));
                Low = Convert.ToDouble(RoundStopLossValueOfAFL(TradingSymbol, Convert.ToDecimal(Low), true));
                Close = Convert.ToDouble(RoundStopLossValueOfAFL(TradingSymbol, Convert.ToDecimal(Close), false));

                string values = TradingSymbol + "," + date + "," + t + "," + Open + "," + High + "," + Low + "," + Close + "," + Volume;
                using (StreamWriter streamWriter = File.AppendText(path))
                {
                    streamWriter.WriteLine(values);
                }
                if (bWriteinDB)
                    WriteDataIntoDB(TradingSymbol, date, t, Open, High, Low, Close, Volume, bCommit);
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "WriteDataIntoFile :  Exception Error Message " + e.Message, MessageType.Informational);
            }
        }

        public void WriteDataIntoDB(string TradingSymbol, string Date, string Time, double Open, double High, double Low, double Close, double Volume, bool bCommit)
        {
            List<String> DBInsertQueries = new List<string>();
            SQLiteCommand sql_cmdlocal;
            try
            {

                string dateTime = Date + " " + Time;
                if (sql_conData == null)
                {
                    lock (databaselock)
                    {
                        OpenConnectionForZerdhaData();
                    }
                }

                if (DatabaseCreated.ContainsKey(TradingSymbol) == false)
                {
                    lock (databaselock)
                    {
                        if (TableDataExists(TradingSymbol) == false)
                        {
                            string sql_new = "CREATE TABLE '" + TradingSymbol + "'(DateTime varchar(20) PRIMARY KEY, Open real NOT NULL, High real NOT NULL,Low real NOT NULL,Close real NOT NULL,Volume real NOT NULL)";
                            sql_cmdlocal = new SQLiteCommand(sql_new, sql_conData);
                            sql_cmdlocal.ExecuteNonQuery();
                        }
                    }

                }
                else
                {
                    DatabaseCreated[TradingSymbol] = 1;//
                }

                string sql_select = "SELECT * FROM '" + TradingSymbol + "' WHERE DateTime = '" + dateTime + "'";
                sql_cmdlocal = new SQLiteCommand(sql_select, sql_conData);
                sql_read = sql_cmdlocal.ExecuteReader();

                if (!(sql_read != null && sql_read.HasRows))
                {
                    lock (databaselock)
                    {
                        string insertSQLQuery = "insert into '" + TradingSymbol + "' (DateTime, Open, High, Low, Close, Volume) values ('" + dateTime + "','" + Open + "','" + High + "','" + Low + "','" + Close + "','" + Volume + "')";
                        DBInsertQueries.Add(insertSQLQuery);
                        if (bCommit)
                        {
                            using (SQLiteTransaction mytransaction = sql_conData.BeginTransaction())
                            {
                                using (SQLiteCommand mycommand = new SQLiteCommand(sql_conData))
                                {
                                    SQLiteParameter myparam = new SQLiteParameter();
                                    int n;
                                    for (n = 0; n < DBInsertQueries.Count(); n++)
                                    {
                                        //myparam.Value = n + 1;
                                        mycommand.CommandText = DBInsertQueries[n];// "INSERT INTO [MyTable] ([MyId]) VALUES(?)";
                                        mycommand.ExecuteNonQuery();
                                    }
                                }
                                mytransaction.Commit();
                                DBInsertQueries.Clear();
                            }
                        }
                    }

                }

            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "WriteDataIntoDB :  Exception Error Message " + e.Message, MessageType.Exception);
            }
        }

        public bool OpenConnectionForZerdhaData()
        {
            try
            {
                //db created at absolute path
                var path_DB = Directory.GetCurrentDirectory();
                string directoryName = path_DB + "\\" + "Database";

                if (!Directory.Exists(directoryName))
                    Directory.CreateDirectory(directoryName);

                string DBFilePath = directoryName + "\\" + "zerodhadata.db";

                if (!File.Exists(DBFilePath))
                {
                    SQLiteConnection.CreateFile(DBFilePath);
                    Thread.Sleep(100);
                }

                string connectionString = String.Format("Data Source={0};Version=3;New=False;Compress=True;", DBFilePath);
                sql_conData = new SQLiteConnection(connectionString);
                //open sqlite connection
                sql_conData.Open();
            }
            catch (Exception e)
            {
                logger.LogMessage("OpenConnectionForZerdhaData : Exception Error Message = " + e.Message, MessageType.Exception);
                return false;
            }

            return true;
        }

        //sanika::17-sep-2020::for downloading historical data for symbol from symbollist.ini
        public List<string> UpdateSymbolList(List<string> TradingSymbolList)
        {
            try
            {
                foreach (var symbol in m_RealTimeSymbols)
                {
                    if (!TradingSymbolList.Contains(symbol))
                        TradingSymbolList.Add(symbol);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs("DownloadData", "UpdateSymbolList : Exception Error Message " + e.Message, MessageType.Exception);
            }
            return TradingSymbolList;
        }

        private static readonly object sync = new Object();

        //sanika::9-Dec-2020::Added for historical data downloading 
        public void DownloadHistoricalData(List<string> TradingSymbolList, int daysForHistroricalData, bool isUseDifferentAccount)
        {
            //sanika::16-Dec-2020::Added sqlite and mysql changes
            if (m_StoreDataInMysql || m_HistoricalDataInMysqlDB)
            {
                HistoricalDataDownloading historicalDataDownloading = new HistoricalDataDownloading();
                WriteUniquelogs("DownloadData", "Started to download " + DateTime.Now, MessageType.Informational);
                Trace.WriteLine("@@@Download Started download");
                historicalDataDownloading.DownloadData(TradingSymbolList, m_Interval.ToString(), daysForHistroricalData, objConfigSettings.dbServer, objConfigSettings.dbUserid, objConfigSettings.dbPassword, objConfigSettings.databaseName, kite);
                Trace.WriteLine("@@@Download Completed download");
                WriteUniquelogs("DownloadData", "End to download " + DateTime.Now, MessageType.Informational);
            }
            if (m_StoreDataInSqlite)
            {
                string message = "";
                if (DownloadDataEx(TradingSymbolList, daysForHistroricalData, isUseDifferentAccount))
                {
                    message = "Client ID = " + m_UserID + " Historical data downloaded sucessfully";
                }
                else
                {
                    message = "Client ID = " + m_UserID + "Not able to download historical data";
                }
                SendNotification(message);
            }
        }

        public void SendNotification(string message)
        {
            string iniFileSetting = Directory.GetCurrentDirectory() + @"\Configuration\" + "Setting.ini";

        }
        //sanika::28-Oct-2020::Added for historical data
        public bool DownloadDataEx(List<string> TradingSymbolList, int daysForHistroricalData, bool isUseDifferentAccount)
        {
            bool isDownloaded = false;
            TradingSymbolList = UpdateSymbolList(TradingSymbolList);

            if (daysForHistroricalData > 0)
            {
                SqliteConnectZerodha sqliteConnectZerodha = new SqliteConnectZerodha();
                bool bCommit = false;
                try
                {
                    bool isconnected = sqliteConnectZerodha.Connect("zerodhadata.db");
                    string dateTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm") + ":00";
                    DateTime todateTime = DateTime.ParseExact(dateTime, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture).AddMinutes(1);
                    DateTime startdateTime = todateTime.AddDays(-daysForHistroricalData);
                    todateTime = todateTime.AddDays(1);
                    int Interval = 60;
                    WriteUniquelogs("DownloadData", "Started Time = " + DateTime.Now, MessageType.Informational);
                    WriteUniquelogs("DownloadData", "startdateTime = " + startdateTime + " todateTime = " + todateTime, MessageType.Informational);
                    //sanika::8-dec-2020::added for dynamic interval
                    string interval = GetInterval(m_Interval);
                    foreach (var name in TradingSymbolList)
                    {
                        string[] strAarray = name.Split('.');
                        string TradingSymbol = strAarray[0];
                        if (strAarray[1] == Constants.EXCHANGE_NFO || strAarray[1] == Constants.EXCHANGE_CDS)
                        {
                            //IRDS::05-Sep-2020::Jyoti::Commented for saving original symbol
                            if (!TradingSymbol.Contains("-I") && (!TradingSymbol.Contains(m_futureName)))
                                TradingSymbol += kite.FetchTableName();
                        }
                        string token = kite.GetToken(name);
                        CheckSqliteTableIsCreatedOrNot(TradingSymbol, sqliteConnectZerodha);
                        if (GetHistoricalData(TradingSymbol, token, daysForHistroricalData, interval, startdateTime.ToString(), todateTime.ToString(), sqliteConnectZerodha))
                        {
                            isDownloaded = true;
                            WriteUniquelogs("DownloadData", "Data Downloaded for " + TradingSymbol, MessageType.Informational);
                        }
                    }

                    WriteUniquelogs("DownloadData", "Data Downloaded = " + DateTime.Now, MessageType.Informational);
                }
                catch (Exception er)
                {
                    isDownloaded = false;
                    WriteUniquelogs("DownloadData", "DownloadData : Exception Error Message " + er.Message, MessageType.Exception);
                }
            }
            return isDownloaded;
        }

        //sanika::8-Dec-2020::Added interval dynamically
        public string GetInterval(int period)//sanika::2-Jan-2021::Added parameter for getting interval
        {
            //minute, 3minute, 5minute, 10minute, 15minute, 30minute, 60minute
            string interval = "minute";
            if (period == 1)
                interval = "minute";
            else if (period == 3)
                interval = "3minute";
            else if (period == 5)
                interval = "5minute";
            else if (period == 10)
                interval = "10minute";
            else if (period == 15)
                interval = "15minute";
            else if (period == 30)
                interval = "30minute";
            else if (period == 60)
                interval = "60minute";
            return interval;
        }

        //sanika::28-Oct-2020::Added for historical data
        private bool GetHistoricalData(string strSymbolName, string token, int intBarIntervalQuantity, string strBarIntervalUnit, string strStartDate, string strEndDate, SqliteConnectZerodha sqliteConnectZerodha)
        {
            Boolean bolGetHistoricalBarBackData = false;
            try
            {
                WriteUniquelogs("DownloadData", "GetHistoricalData : SymbolName: " + strSymbolName + " , BarIntervalQuantity: " + intBarIntervalQuantity.ToString() + " , BarIntervalUnit: " + strBarIntervalUnit + " , StartDate: " + strStartDate + " , EndDate: " + strEndDate, MessageType.Informational);

                bool ContinueExecution = false;
                string Interval = "";
                string EndDate = Convert.ToDateTime(strEndDate).ToString();
                Interval = "60";

                System.Globalization.CultureInfo tempCultureInfo = System.Globalization.CultureInfo.InvariantCulture;
                string CurrentDate = DateTime.Now.AddMinutes(1).ToString("dd-MM-yyyy", tempCultureInfo);
                DateTime current = DateTime.ParseExact(CurrentDate, "dd-MM-yyyy", null);
                while (!ContinueExecution)
                {
                    lock (sync)
                    {
                        strEndDate = Convert.ToDateTime(strStartDate).AddDays(Convert.ToInt32(Interval)).ToString();
                        string NewEndDate = strEndDate;
                        string NewStartDate = strStartDate;

                        if (Convert.ToDateTime(NewStartDate) > current)
                        {
                            break;
                        }
                        else
                        {
                            if (Convert.ToDateTime(NewEndDate) > Convert.ToDateTime(EndDate))
                            {
                                strEndDate = EndDate;
                                ContinueExecution = true;
                            }
                        }
                        List<Historical> historicalList = new List<Historical>();
                        if (m_userid != objConfigSettings.usernameHistorical)
                        {
                            historicalList = kite.GetHistoricalDataWithOtherAccount(token, Convert.ToDateTime(strStartDate), Convert.ToDateTime(strEndDate), strBarIntervalUnit, false);
                        }
                        else
                        {
                            historicalList = kite.GetHistoricalData(token, Convert.ToDateTime(strStartDate), Convert.ToDateTime(strEndDate), strBarIntervalUnit, false);
                        }
                        //sanika::4-Apr-2021::As per sir's suggestion future table as e.g. NIFTY-I
                        if (strSymbolName.Contains(m_futureName))
                            strSymbolName = strSymbolName.Replace(m_futureName, "-I");
                        InsertHistoricalDataIntoSqlite(historicalList, strSymbolName, sqliteConnectZerodha);
                        if (strBarIntervalUnit == "minute")
                        {
                            strStartDate = Convert.ToDateTime(strEndDate).AddMinutes(intBarIntervalQuantity).ToString();
                        }
                        else
                        {
                            strStartDate = strEndDate;
                        }
                    }
                }
                WriteUniquelogs("DownloadData", "GetHistoricalData : Data Downloaded", MessageType.Informational);
            }
            catch (Exception ex)
            {
                bolGetHistoricalBarBackData = false;
                WriteUniquelogs("DownloadData", "GetHistoricalData : Exception Error Message = " + ex.Message, MessageType.Informational);
            }
            return bolGetHistoricalBarBackData;
        }

        //sanika::28-Oct-2020::Added for historical data
        public void InsertHistoricalDataIntoSqlite(List<Historical> historicalList, string TradingSymbol, SqliteConnectZerodha sqliteConnectZerodha)
        {
            try
            {
                int counter = 0;
                List<String> DBInsertQueries = new List<string>();
                DateTime dateTimeForTimeCondition = DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd") + " 15:29:00", "yyyy-MM-dd HH:mm:ss", null);
                foreach (var historical in historicalList)
                {
                    counter++;
                    string timestamp = historical.TimeStamp.ToString("yyyy-MM-dd HH:mm:ss").TrimStart();
                    DateTime timeStamp = DateTime.ParseExact(timestamp, "yyyy-MM-dd HH:mm:ss", null);

                    lock (databaselock)
                    {
                        if (timeStamp <= dateTimeForTimeCondition)
                        {
                            string insertSQLQuery = "INSERT OR IGNORE into '" + TradingSymbol + "' (TickDateTime, Open, High, Low, Close, Volume, DoubleDate) values ('" + historical.TimeStamp + "','" + historical.Open + "','" + historical.High + "','" + historical.Low + "','" + historical.Close + "','" + historical.Volume + "','" + historical.TimeStamp.ToOADate() + "')";
                            DBInsertQueries.Add(insertSQLQuery);
                            //WriteUniquelogs("DownloadData", "Trading symbol = " + TradingSymbol + " " + insertSQLQuery, MessageType.Informational);
                        }
                        if (counter == historicalList.Count - 1 && DBInsertQueries.Count > 0)
                        {
                            WriteUniquelogs("DownloadData", "InsertHistoricalDataIntoSqlite : Data Downloaded for " + TradingSymbol + " Count = " + historicalList.Count, MessageType.Informational);
                            sqliteConnectZerodha.InsertDataMultipleValues(TradingSymbol, DBInsertQueries);
                            DBInsertQueries.Clear();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs("DownloadData", "InsertHistoricalDataIntoSqlite :  Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        //sanika::28-Oct-2020::Added for historical data
        public void CheckSqliteTableIsCreatedOrNot(string TradingSymbol, SqliteConnectZerodha sqliteConnectZerodha)
        {
            try
            {
                //sanika::4-Apr-2021::As per sir's suggestion future table as e.g. NIFTY-I
                if (TradingSymbol.Contains(m_futureName))
                {
                    TradingSymbol = TradingSymbol.Replace(m_futureName, "-I");
                }
                if (DatabaseCreated.ContainsKey(TradingSymbol) == false)
                {
                    lock (databaselock)
                    {
                        if (sqliteConnectZerodha.IsTableExists(TradingSymbol) == false)
                        {
                            WriteUniquelogs("DownloadData", "CheckSqliteTableIsCreatedOrNot : Table is created for " + TradingSymbol, MessageType.Informational);
                            sqliteConnectZerodha.ExecuteNonQueryCommand("CREATE TABLE '" + TradingSymbol + "'(TickDateTime DateTime PRIMARY KEY, Open real NOT NULL, High real NOT NULL,Low real NOT NULL,Close real NOT NULL,Volume real NOT NULL,DoubleDate real NOT NULL)");
                        }
                    }
                }
                else
                {
                    DatabaseCreated[TradingSymbol] = 1;
                }
            }
            catch (Exception er)
            {
                WriteUniquelogs("DownloadData", "CheckSqliteTableIsCreatedOrNot : Exception Error Message " + er.Message, MessageType.Exception);
            }
        }

        public bool IsPositionOpen(string TradingSymbol, string Exchange)
        {
            bool isOpen = false;
            try
            {
                string newSymbol = TradingSymbol + "_" + Constants.PRODUCT_MIS + "|" + Exchange;
                PositionInfo currentPosition;
                if (m_GlobalpositionStore.GetPosition(newSymbol, out currentPosition))
                {
                    if (currentPosition.dayPosition.TradingSymbol == TradingSymbol && currentPosition.dayPosition.Quantity != 0)
                    {
                        if (currentPosition.dayPosition.Quantity > 0)
                        {
                            //buy
                            isOpen = true;
                        }
                        else if (currentPosition.dayPosition.Quantity < 0)
                        {
                            //sell
                            isOpen = true;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                isOpen = false;
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "IsPositionOpen :  Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return isOpen;
        }

        public void AddOrderIdInMasterList(string TradingSymbol, string OrderId)
        {
            string symbol = "";
            if (TradingSymbol.Contains("_"))
                symbol = TradingSymbol.Split('_')[1];
            else
                symbol = TradingSymbol;
            try
            {
                if (m_DictionaryForOrderId.ContainsKey(symbol))
                {
                    m_DictionaryForOrderId[symbol] = OrderId;
                    WriteUniquelogs(symbol + " " + m_UserID, "AddOrderIdInMasterList :  Updated order id in dictionary " + OrderId, MessageType.Informational);
                }
                else
                {
                    m_DictionaryForOrderId.Add(symbol, OrderId);
                    WriteUniquelogs(symbol + " " + m_UserID, "AddOrderIdInMasterList :  Added order id in dictionary " + OrderId, MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(symbol + " " + m_UserID, "AddOrderIdInMasterList :  Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        public void RemoveOrderIdFromMasterList(string TradingSymbol)
        {
            string symbol = "";
            if (TradingSymbol.Contains("_"))
                symbol = TradingSymbol.Split('_')[1];
            else
                symbol = TradingSymbol;
            try
            {
                if (m_DictionaryForOrderId.ContainsKey(symbol))
                {
                    m_DictionaryForOrderId.Remove(symbol);
                    WriteUniquelogs(symbol + " " + m_UserID, "RemoveOrderIdFromMasterList :  Remove orderId from dictionary", MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(symbol + " " + m_UserID, "RemoveOrderIdFromMasterList :  Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        public string GetOrderIdFromMasterList(string TradingSymbol)
        {
            string orderId = "NA";
            string symbol = "";
            if (TradingSymbol.Contains("_"))
                symbol = TradingSymbol.Split('_')[1];
            else
                symbol = TradingSymbol;
            try
            {
                if (m_DictionaryForOrderId.ContainsKey(symbol))
                {
                    orderId = m_DictionaryForOrderId[symbol];
                    //sanika::28-sep-2020::temp commented as per sandip sir's suggestion
                    //if (!CheckOrderStatus(TradingSymbol, orderId))
                    //{
                    //    orderId = "NA";
                    //    RemoveOrderIdFromMasterList(symbol);
                    //    WriteUniquelogs(symbol + " " + m_UserID, "GetOrderIdFromMasterList :  order is closed " + orderId, MessageType.Informational);
                    //}
                    WriteUniquelogs(symbol + " " + m_UserID, "GetOrderIdFromMasterList :  Retrived order from dictionary " + orderId, MessageType.Informational);
                }
                else
                {
                    WriteUniquelogs(symbol + " " + m_UserID, "GetOrderIdFromMasterList :  Dictionary not contains the symbol", MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(symbol + " " + m_UserID, "GetOrderIdFromMasterList :  Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return orderId;
        }

        //function for amibroker
        public bool CheckWithinTime(string strDateTime, int interval, string key)
        {
            try
            {
                WriteUniquelogs(key, "CheckWithinTime : strDateTime = " + strDateTime + " interval= " + interval + " key = " + key, MessageType.Informational);
                DateTime oDate = DateTime.Parse(strDateTime);
                WriteUniquelogs(key, "CheckWithinTime : oDate = " + oDate, MessageType.Informational);
                WriteUniquelogs(key, "CheckWithinTime : Passed Date : " + oDate, MessageType.Informational);
                DateTime currDate = DateTime.Now;
                WriteUniquelogs(key, "CheckWithinTime : current Date : " + currDate, MessageType.Informational);
                int diff = Math.Abs(Convert.ToInt32((oDate - currDate).TotalMinutes));
                WriteUniquelogs(key, "CheckWithinTime : Difference : " + diff, MessageType.Informational);
                if (diff <= interval)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(key, "CheckWithinTime : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return false;
        }

        //function for amibroker
        public void InsertMessageDetails(string datestr, int signal, string name, string key)
        {
            try
            {
                WriteUniquelogs(key, "InsertMessageDetails : datestr :" + datestr + " signal = " + signal + " name = " + name + " key " + key, MessageType.Informational);
                DateTime Fdate = DateTime.Parse(datestr);
                WriteUniquelogs(key, "InsertMessageDetails : Fdate :" + Fdate, MessageType.Informational);

                if (OpenConnection())
                {
                    if (TableExistsforKeyTable(key) == false)
                    {
                        string sql_new = "CREATE TABLE '" + key + "'(DateTimeFormat DATETIME NOT NULL PRIMARY KEY, Signal int NOT NULL,  Name varchar(20) NOT NULL, Key varchar(20) NOT NULL)";
                        sql_cmd = new SQLiteCommand(sql_new, sql_con);
                        sql_cmd.ExecuteNonQuery();
                    }
                    try
                    {
                        string insertSQLQuery = "insert into " + key + " (DateTimeFormat, Signal, Name, Key) values ('" + Fdate + "'," + signal + ",'" + name + "','" + key + "');";
                        sql_cmd = new SQLiteCommand(insertSQLQuery, sql_con);
                        sql_cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        WriteUniquelogs(key, "InsertMessageDetails : Exception Error Message :" + ex, MessageType.Exception);
                    }
                }
                else
                {
                    WriteUniquelogs(key, "InsertMessageDetails : Failed to establish connection", MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(key, "InsertMessageDetails : Exception Error Message " + e.Message, MessageType.Exception);
            }
        }

        //function for amibroker
        public bool TableExistsforKeyTable(string Table_Name)
        {
            try
            {
                using (sql_cmd = new SQLiteCommand(sql_con))
                {
                    sql_cmd.CommandText = "SELECT * FROM sqlite_master WHERE type='table' AND name=@name";
                    sql_cmd.Parameters.AddWithValue("@name", Table_Name);

                    using (sql_read = sql_cmd.ExecuteReader())
                    {
                        if (sql_read != null && sql_read.HasRows)
                        {
                            WriteUniquelogs(Table_Name, "TableExistsforKeyTable : table exist", MessageType.Informational);
                            return true;
                        }
                        else
                        {
                            WriteUniquelogs(Table_Name, "TableExistsforKeyTable : table not exist", MessageType.Informational);
                            return false;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                logger.LogMessage("TableExistsforKeyTable : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return false;
        }

        //function for amibroker
        public void GetMessagePosition(string tablename)
        {
            SQLiteConnection Sql_con;
            SQLiteCommand Sql_cmd;
            SQLiteDataReader Sql_read;
            SQLiteTransaction Tr;
            var path_DB = Directory.GetCurrentDirectory();
            string directoryName = path_DB + "\\" + "Database";
            string DBFilePath = directoryName + "\\" + "kite.db";
            try
            {
                WriteUniquelogs(tablename, "GetMessagePosition : tablename : " + tablename, MessageType.Informational);
                string connectionString = String.Format("Data Source={0};Version=3;New=False;Compress=True;", DBFilePath);
                if (TableExistsforKeyTable(tablename) == false)
                {
                    WriteUniquelogs(tablename, "GetMessagePosition : tablename not exist: " + tablename, MessageType.Informational);
                    return;
                }
                using (Sql_con = new SQLiteConnection(connectionString))
                {
                    Sql_con.Open();

                    using (Tr = Sql_con.BeginTransaction())
                    {
                        string query = "SELECT * FROM " + tablename + " order by DateTimeFormat desc  LIMIT 1;";
                        Sql_cmd = new SQLiteCommand(query, Sql_con);
                        Sql_read = Sql_cmd.ExecuteReader();
                        if (Sql_read != null && Sql_read.HasRows)
                        {
                            while (Sql_read.Read())
                            {
                                string name = (string)Sql_read["Name"];
                                string key = (string)Sql_read["Key"];
                                WriteUniquelogs(tablename, "key : " + key + "Name : " + name + "Signal : " + (int)Sql_read["Signal"], MessageType.Informational);
                            }
                        }
                        Tr.Commit();
                    }
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(tablename, "GetMessagePosition : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        //function for amibroker
        //IRDS::03-Jul-2020::Jyoti::added for historical data download in mysql
        public void DownloadDataMysql(List<string> TradingSymbolList, int daysForHistroricalData, bool isUseDifferentAccount)
        {
            if (daysForHistroricalData > 0)
            {
                bool bCommit = false;
                SQLiteCommand sql_cmdlocal;
                try
                {
                    if (m_Mysql == null)
                    {
                        m_Mysql = new MySQLConnectZerodha();
                        if (m_Mysql.Connect(logger, kite.dbServer, kite.dbUserid, kite.dbPassword, kite.databaseName))
                        {
                            m_isConnected = true;
                        }
                    }
                    DateTime dateTimeForTimeCondition = DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd") + " 15:29:00", "yyyy-MM-dd HH:mm:ss", null);
                    string dateTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm") + ":00";
                    //string dateTime = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
                    DateTime todateTime = DateTime.ParseExact(dateTime, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture).AddMinutes(1);
                    DateTime startdateTime = todateTime.AddDays(-daysForHistroricalData);
                    todateTime = todateTime.AddDays(1);
                    WriteUniquelogs("DownloadData", "Satrted Time = " + DateTime.Now, MessageType.Informational);
                    WriteUniquelogs("DownloadData", "startdateTime = " + startdateTime + " todateTime = " + todateTime, MessageType.Informational);
                    foreach (var name in TradingSymbolList)
                    {
                        string[] strAarray = name.Split('.');
                        string TradingSymbol = strAarray[0];
                        if (strAarray[1] == Constants.EXCHANGE_NFO)
                        {
                            //IRDS::05-Sep-2020::Jyoti::Commented for saving original symbol
                            if (!TradingSymbol.Contains("-I"))
                                TradingSymbol += kite.FetchTableName();
                        }
                        if (DatabaseCreated.ContainsKey(TradingSymbol) == false)
                        {
                            if (m_Mysql.IsTableExists(TradingSymbol, objConfigSettings.databaseName))
                            {
                                //m_Mysql.CleanDatabase(TradingSymbol);
                            }
                            else
                            {
                                m_Mysql.CreateTable(TradingSymbol, "Symbol varchar(50) NOT NULL, LastTradeTime datetime NOT NULL, Open double NOT NULL, High double NOT NULL,Low double NOT NULL,Close double NOT NULL,Volume BIGINT NOT NULL", "LastTradeTime");
                            }
                        }
                        else
                        {
                            DatabaseCreated[TradingSymbol] = 1;
                        }
                        try
                        {
                            string token = kite.GetToken(name);
                            List<Historical> historicalList = new List<Historical>();
                            if (isUseDifferentAccount)
                            {
                                historicalList = kite.GetHistoricalDataWithOtherAccount(token, startdateTime, todateTime, "minute", false);
                            }
                            else
                            {
                                historicalList = kite.GetHistoricalData(token, startdateTime, todateTime, "minute", false);
                            }

                            int counter = 0;
                            string insertData = "";
                            WriteUniquelogs("DownloadData", "Msql Data Downloaded for " + TradingSymbol, MessageType.Informational);
                            foreach (var historical in historicalList)
                            {
                                counter++;
                                string timestamp = historical.TimeStamp.ToString("yyyy-MM-dd HH:mm:ss").TrimStart();
                                DateTime timeStamp = DateTime.ParseExact(timestamp, "yyyy-MM-dd HH:mm:ss", null);

                                {
                                    string strQuery = "SELECT * FROM `" + TradingSymbol + "` WHERE LastTradeTime = '" + timestamp + "'";
                                    MySqlCommand cmd = new MySqlCommand(strQuery, m_Mysql.mysqlconnDBManipulation);//create command and assign the query and connection from the constructor
                                    MySqlDataReader dataReader = cmd.ExecuteReader(); //Create a data reader and Execute the command

                                    if (!(dataReader != null && dataReader.HasRows))
                                    {
                                        //lock (databaselock)
                                        {
                                            if (timeStamp <= dateTimeForTimeCondition)
                                            {
                                                insertData = insertData + "('" + TradingSymbol + "', STR_TO_DATE('" + timestamp + "', '%Y-%m-%d %H:%i:%s'),'" + historical.Open + "', '" + historical.High + "', '" + historical.Low + "', '" + historical.Close + "', '" + historical.Volume + "'),";
                                            }
                                        }
                                    }
                                    dataReader.Close();
                                }
                            }
                            if (insertData.Length > 1)
                            {
                                WriteUniquelogs("DownloadData", "Msql Data Downloaded for " + TradingSymbol, MessageType.Informational);
                                insertData = insertData.Remove(insertData.Length - 1, 1);
                                m_Mysql.InsertHistDataMultipleValues(TradingSymbol, insertData);
                            }
                        }
                        catch (Exception er)
                        {
                            WriteUniquelogs("DownloadData", "DownloadDataMysql : Exception Error Message =  " + TradingSymbol + " :: " + er.Message, MessageType.Exception);
                        }
                    }
                    WriteUniquelogs("DownloadData", "Msql Data Downloaded = " + DateTime.Now, MessageType.Informational);
                }
                catch (Exception er)
                {
                    WriteUniquelogs("DownloadData", "DownloadDataMysql : Exception Error Message = " + er.Message, MessageType.Exception);
                }
            }
        }
        //IRDSPM::Pratiksha::-01-07-2020:: Fetching tablename and symbolname method call --start
        public List<string> FetchDetails(string exchange)
        {
            if (kite != null)
            {
                return kite.GetDataforTradingsymbol(exchange);
            }
            else
            {
                return null;
            }
        }

        public bool CheckTimeToPlaceOrder()
        {
            try
            {
                if (m_StartTimeForPlaceOrder != null && m_EndTimeForPlaceOrder != null)
                {
                    DateTime currentDateTime = DateTime.Now;
                    if ((currentDateTime.ToOADate() >= m_StartTimeForPlaceOrder.ToOADate()) && (currentDateTime.ToOADate() < m_EndTimeForPlaceOrder.ToOADate()))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception e)
            {
                logger.LogMessage("CheckTimeToPlaceOrder: Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return false;
        }

        public bool CheckTimeToExistAllCDSOrder()
        {
            try
            {
                if (m_TimeToExitAllCDSOrder != null)
                {
                    DateTime currentDateTime = DateTime.Now;
                    if ((currentDateTime.ToOADate() >= m_TimeToExitAllCDSOrder.ToOADate()))
                    {
                        logger.LogMessage("CheckTimeToExistAllCDSOrder: m_TimeToExitAllCDSOrder : " + m_TimeToExitAllCDSOrder.ToString() + " currentDateTime " + currentDateTime.ToString(), MessageType.Informational);
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception e)
            {
                logger.LogMessage("CheckTimeToExistAllCDSOrder: Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return false;
        }
        public bool CheckTimeToExistAllMCXOrder()
        {
            try
            {
                if (m_TimeToExitAllMCXOrder != null)
                {
                    DateTime currentDateTime = DateTime.Now;
                    if ((currentDateTime.ToOADate() >= m_TimeToExitAllMCXOrder.ToOADate()))
                    {
                        logger.LogMessage("CheckTimeToExistAllMCXOrder: m_TimeToExitAllMCXOrder : " + m_TimeToExitAllMCXOrder.ToString() + " currentDateTime " + currentDateTime.ToString(), MessageType.Informational);
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception e)
            {
                logger.LogMessage("CheckTimeToExistAllMCXOrder: Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return false;
        }
        public bool CheckTimeToExistAllNSEOrder()
        {
            try
            {
                if (m_TimeToExitAllNSEOrder != null)
                {
                    DateTime currentDateTime = DateTime.Now;
                    if ((currentDateTime.ToOADate() >= m_TimeToExitAllNSEOrder.ToOADate()))
                    {
                        logger.LogMessage("CheckTimeToExistAllNSEOrder: m_TimeToExitAllNSEOrder : " + m_TimeToExitAllNSEOrder.ToString() + " currentDateTime " + currentDateTime.ToString(), MessageType.Informational);
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception e)
            {
                logger.LogMessage("CheckTimeToExistAllNSEOrder: Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return false;
        }
        public bool CheckTimeToExistAllNFOOrder()
        {
            try
            {
                if (m_TimeToExitAllNFOOrder != null)
                {
                    DateTime currentDateTime = DateTime.Now;
                    if ((currentDateTime.ToOADate() >= m_TimeToExitAllNFOOrder.ToOADate()))
                    {
                        logger.LogMessage("CheckTimeToExistAllNFOOrder: m_TimeToExitAllNFOOrder : " + m_TimeToExitAllNFOOrder.ToString() + " currentDateTime " + currentDateTime.ToString(), MessageType.Informational);
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception e)
            {
                logger.LogMessage("CheckTimeToExistAllNFOOrder: Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return false;
        }




        public string GetOrderId(string TradingSymbol, string Exchange, string TransactionType, string Product)
        {
            string orderId = "ordernotfound";
            try
            {
                List<Order> orderInfo = m_GlobalOrderStore.GetOrderbySymbol(TradingSymbol, Exchange);
                if (orderInfo.Count == 0)
                {
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOrderId : order info count is zero", MessageType.Informational);
                    return orderId;
                }
                for (int i = orderInfo.Count - 1; i >= 0; i--)
                {
                    if (orderInfo[i].TransactionType == TransactionType && orderInfo[i].Product == Product)
                    {
                        orderId = orderInfo[i].OrderId;
                        WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + "Order id retrived as " + orderId);
                        m_ExceptionCounter++;
                    }
                }
            }
            catch (Exception e)
            {
                WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + e.Message);
                m_ExceptionCounter++;
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOrderId :Error Exception Message = " + e.Message, MessageType.Informational);
            }
            return orderId;
        }

        public string GetParentOrderId(string TradingSymbol, string Exchange, string OrderId)
        {
            string orderId = "ordernotfound";
            try
            {
                OrderExt orderExt;
                m_GlobalOrderStore.GetOrderbyID(TradingSymbol, Exchange, OrderId, out orderExt);
                if (orderExt != null)
                {
                    if (orderExt.order.ParentOrderId != null)
                    {
                        orderId = orderExt.order.ParentOrderId;
                        WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetParentOrderId : OrderId retrive as " + orderId, MessageType.Informational);
                        //m_ExceptionDictionary.Add(m_ExceptionCounter, DateTime.Now + " " + "GetParentOrderId - OrderId retrive as " + orderId);
                        WriteGUIlogs(m_ExceptionCounter, DateTime.Now + "OrderId retrive as " + orderId);
                        m_ExceptionCounter++;
                    }
                    return orderId;
                }
                else
                {
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetParentOrderId : orderExt is null", MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                //m_ExceptionDictionary.Add(m_ExceptionCounter, DateTime.Now + " " + "GetParentOrderId - " + e.Message);
                WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " GetParentOrderId - " + e.Message);
                m_ExceptionCounter++;
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetParentOrderId :Error Exception Message = " + e.Message, MessageType.Exception);
            }
            return orderId;
        }

        public static void WriteGUIlogs(int Counter, string message)
        {
            try
            {
                SymbolLogs log = new SymbolLogs();
                log.createFile("GUILogs");
                log.LogMessage(Counter + "|" + message, MessageType.Informational);
                log = null;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : " + e.Message);
                Trace.WriteLine("Exception : " + e.Message);
            }
        }

        public void SetSqroffFlag(bool sqrOffFlag)
        {
            m_isSqrOff = sqrOffFlag;
        }

        public void isReloadTradingSettingINI()
        {
            var path = Directory.GetCurrentDirectory();
            objConfigSettings.readTradingINIFile(path + @"\Configuration\TradingSetting.ini");
            m_overallLoss = objConfigSettings.m_overallLoss;
            m_overallProfit = objConfigSettings.m_overallProfit;
            m_isCalculateProfitLoss = objConfigSettings.isCalculateProfitLoss;
            m_StartTime = objConfigSettings.StartTimeNSE;
            m_TimeLimitToPlaceOrder = objConfigSettings.TimeLimitToPlaceOrder;
            //sanika::3-dec-2020::Added for diff exchanges
            m_TimeToExitAllNSEOrders = objConfigSettings.ExitTimeNSE;
            m_TimeToExitAllNFOOrders = objConfigSettings.ExitTimeNFO;
            m_TimeToExitAllMCXOrders = objConfigSettings.ExitTimeMCX;
            m_TimeToExitAllCDSOrders = objConfigSettings.ExitTimeCDS;
            m_StartTimeForPlaceOrder = DateTime.ParseExact(m_StartTime, "HH:mm:ss", null);
            m_EndTimeForPlaceOrder = DateTime.ParseExact(m_TimeLimitToPlaceOrder, "HH:mm:ss", null);

            //sanika::3-dec-2020::Added for diff exchanges
            m_TimeToExitAllNSEOrder = DateTime.ParseExact(m_TimeToExitAllNSEOrders, "HH:mm:ss", null);
            m_TimeToExitAllNFOOrder = DateTime.ParseExact(m_TimeToExitAllNFOOrders, "HH:mm:ss", null);
            m_TimeToExitAllMCXOrder = DateTime.ParseExact(m_TimeToExitAllMCXOrders, "HH:mm:ss", null);
            m_TimeToExitAllCDSOrder = DateTime.ParseExact(m_TimeToExitAllCDSOrders, "HH:mm:ss", null);

            //sanika::8-dec-2020::added for risk managment 
            m_BrokerQuantity = Convert.ToInt32(objConfigSettings.brokerQuantity);
            m_ManualQuantity = Convert.ToInt32(objConfigSettings.ManualQuantity);
            m_RiskManagmentFlag = objConfigSettings.isRiskmanagement;
        }

        public bool GetTickDataDownloaderFlag()
        {
            return objConfigSettings.isTickMCXDataDownloader;
        }


        //Sanika::27-Aug-2020::To check order is open or not 
        public bool CheckOrderStatus(string TradingSymbol, string orderId)
        {
            Trace.WriteLine("***********Started time " + DateTime.Now);
            bool isOpen = false;
            string Symbol = "";
            if (TradingSymbol.Contains("_"))
                Symbol = TradingSymbol.Split('_')[1];
            else
                Symbol = TradingSymbol;
            try
            {
                OrderExt orderExt;
                int counter = 0;
                while (counter <= 1)
                {
                    if (m_GlobalOrderStore.GetOrderbyID(Symbol, Constants.EXCHANGE_NSE, orderId, out orderExt))
                    {
                        if (orderExt != null)
                        {
                            //sanika::28-sep-2020::checked 1 min time after placing order
                            DateTime orderTime = (DateTime)orderExt.order.OrderTimestamp;
                            if (DateTime.Now.Subtract(orderTime).TotalMinutes < 1)
                            {
                                WriteUniquelogs(Symbol + " " + m_UserID, "CheckOrderStatus : order time is less than 1 min", MessageType.Informational);
                                isOpen = true;
                            }
                            string mappedSymbol = orderExt.order.Tradingsymbol + "|" + orderExt.order.Exchange;
                            PositionInfo currentPosition;
                            if (GetCurrentPosition(mappedSymbol, orderExt.order.Product, out currentPosition))
                            {
                                if (currentPosition.netPosition.Quantity != 0)
                                {
                                    WriteUniquelogs(Symbol + " " + m_UserID, "CheckOrderStatus : order is open", MessageType.Informational);
                                    isOpen = true;
                                    break;
                                }
                                else
                                {
                                    WriteUniquelogs(Symbol + " " + m_UserID, "CheckOrderStatus : order is not open", MessageType.Informational);
                                    isOpen = false;
                                    break;
                                }
                            }
                            else
                            {
                                WriteUniquelogs(Symbol + " " + m_UserID, "CheckOrderStatus : Not get any current position", MessageType.Informational);
                            }
                        }
                        else
                        {
                            WriteUniquelogs(Symbol + " " + m_UserID, "CheckOrderStatus : order count is zero", MessageType.Informational);
                        }
                    }
                    else if (m_GlobalOrderStore.GetOrderbyID(Symbol, Constants.EXCHANGE_NFO, orderId, out orderExt))
                    {
                        if (orderExt != null)
                        {
                            //sanika::28-sep-2020::checked 1 min time after placing order
                            DateTime orderTime = (DateTime)orderExt.order.OrderTimestamp;
                            if (DateTime.Now.Subtract(orderTime).TotalMinutes < 1)
                            {
                                WriteUniquelogs(Symbol + " " + m_UserID, "CheckOrderStatus : order time is less than 1 min", MessageType.Informational);
                                isOpen = true;
                            }

                            string mappedSymbol = orderExt.order.Tradingsymbol + "|" + orderExt.order.Exchange;
                            PositionInfo currentPosition;
                            if (GetCurrentPosition(mappedSymbol, orderExt.order.Product, out currentPosition))
                            {
                                if (currentPosition.netPosition.Quantity != 0)
                                {
                                    WriteUniquelogs(Symbol + " " + m_UserID, "CheckOrderStatus : order is open", MessageType.Informational);
                                    isOpen = true;
                                    break;
                                }
                                else
                                {
                                    WriteUniquelogs(Symbol + " " + m_UserID, "CheckOrderStatus : order is not open", MessageType.Informational);
                                    isOpen = false;
                                    break;
                                }
                            }
                            else
                            {
                                WriteUniquelogs(Symbol + " " + m_UserID, "CheckOrderStatus : Not get any current position", MessageType.Informational);
                            }
                        }
                        else
                        {
                            WriteUniquelogs(Symbol + " " + m_UserID, "CheckOrderStatus : order count is zero", MessageType.Informational);
                        }
                    }
                    else
                    {
                        WriteUniquelogs(Symbol + " " + m_UserID, "CheckOrderStatus : order not updated yet or not present ", MessageType.Informational);
                    }
                    counter++;
                    Thread.Sleep(1000);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(Symbol + " " + m_UserID, "CheckOrderStatus :Error Exception Message = " + e.Message, MessageType.Exception);
            }
            Trace.WriteLine("***********End time " + DateTime.Now);
            return isOpen;
        }

        //Sanika::27-Aug-2020::To connect other account for historical data
        public bool ConnectAccount()
        {
            bool isConnect = false;
            if (!getHistoricalCreadentials())
            {
                if (kite.ConnectAccount())
                {
                    isConnect = true;
                }
                //sanika::4-Apr-2021::Added for exception while logged in for account
                else
                {
                    WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + kite.m_ExceptionError);
                    m_ExceptionCounter++;
                    if (kite.m_ExceptionError != "")
                        System.Windows.Forms.MessageBox.Show(new Form { TopMost = true }, kite.m_ExceptionError);
                }
            }
            else
            {
                isConnect = false;
            }
            return isConnect;
        }

        //Sanika::27-Aug-2020::Added method to write logs from named pipe
        public static void SendResponse()
        {
            //Sanika::2-oct-2020::add seperate class for pipe
            PipeConnection pipeConnection = null;
            while (!m_StopBridgeThread)
            {

                if (m_isFromEXEForBridgeConnection == false)
                {
                    Trace.WriteLine("SendResponse : m_isFromEXEForBridgeConnection =" + m_isFromEXEForBridgeConnection);
                    if (m_LoginStatus)
                    {
                        Trace.WriteLine("SendResponse : Going to create client obj");
                        if (pipeConnection == null)
                        {
                            pipeConnection = new PipeConnection();
                            pipeConnection.StartClientMethod();
                        }
                    }
                    else
                    {
                        Trace.WriteLine("SendResponse :Login not sucessfull");
                        WPFMainControl.m_BridgeConnection = "Not Connected";
                    }
                }
                //sanika::8-oct-2020::Added sleep to reduced cpu usage 
                Thread.Sleep(1000);
            }
        }

        public bool getHistoricalCreadentials()
        {
            var path_File = Directory.GetCurrentDirectory();
            string FilePath = path_File + @"\Configuration\" + "credentialsHistoricalData.txt";
            if (!File.Exists(FilePath))
            {
                logger.LogMessage("credentialsHistoricalData.txt file does not exist", MessageType.Informational);
                return false;
            }
            else
            {
                string lastLine = System.IO.File.ReadLines(FilePath).Last();
                string[] splittedString = lastLine.Split(' ');
                string currentDate = DateTime.Now.ToString("M-d-yyyy");
                if (splittedString[0] == currentDate && splittedString[1] == "AY1598")
                {
                    kite.m_accessTokenDownloadHistoricalData = splittedString[2];
                    kite.m_MyPublicTokenDownloadHistoricalData = splittedString[3];
                    return true;
                }
            }
            return false;
        }

        //Sanika::2-Sep-2020::Added method to set fromexe flag
        public void loadIsFromEXEValue(bool isFromEXE)
        {
            Trace.WriteLine("m_isFromEXEForBridgeConnection " + m_isFromEXEForBridgeConnection);
            m_isFromEXEForshowGUI = isFromEXE;
            m_isFromEXEForBridgeConnection = isFromEXE;
            Trace.WriteLine("m_isFromEXEForBridgeConnection " + m_isFromEXEForBridgeConnection);
        }

        //Sanika::2-Sep-2020::Added method to return fromexe flag
        public bool GetFromEXEValue()//sanika::28-Jul-2021::remove static
        {
            return m_isFromEXEForBridgeConnection;
        }

        //Sanika::3-Sep-2020::Added method to return GetTotalMTM
        public double GetTotalMTM()
        {
            return m_TotalProfitLoss;
        }

        public void DownloadTokenFromuserManualClick()
        {
            try
            {
                m_StartedinstrumentDownloading = true;
                Thread.Sleep(2000);
                DateTime dt = DateTime.Now;
                IntrumentsCSVToDB();
                DateTime dt1 = DateTime.Now;
                double diff = dt.Subtract(dt1).TotalMinutes;
                File.AppendAllText("time.txt", "Time to download instruments " + diff);
                m_StartedinstrumentDownloading = false;
                WriteDateIntoFile();
            }
            catch (Exception e)
            {
                logger.LogMessage("DownloadTokenFromuserManualClick : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }
        //IRDSPM::Pratiksha::11-09-2020::For getting list from db
        public List<string> GetAllSymbolsFromDBdata()
        {
            List<string> SymbolList = new List<string>();

            try
            {
                if (OpenConnection() == true)
                {
                    SymbolList = kite.fetchTablenamesForsearchboxList();
                    foreach (var item in SymbolList)
                    {
                        Console.WriteLine(item.ToString());
                        string tablenameforFetching = item.ToString();
                        try
                        {
                            string sql_select = "select DISTINCT symbol from '" + tablenameforFetching + "'";
                            sql_cmd = new SQLiteCommand(sql_select, sql_con);
                            sql_read = sql_cmd.ExecuteReader();

                            if (sql_read != null && sql_read.HasRows)
                            {
                                while (sql_read.Read())
                                {
                                    getFutSymbol = (string)sql_read["symbol"];

                                    if (!finalSymbolList.Contains(getFutSymbol))
                                    {
                                        finalSymbolList.Add(getFutSymbol);
                                    }
                                    if (tablenameforFetching == "OPTIONS")
                                    {
                                        if (!GetOtionsSymbolsKite.Contains(getFutSymbol))
                                        {
                                            GetOtionsSymbolsKite.Add(getFutSymbol);
                                        }
                                    }
                                    //IRDSPM::PRatiksha::05-07-2021::For getting instruemnts symbol
                                    if (tablenameforFetching == "instruments")
                                    {
                                        if (!GetInstrumentSymbolsKite.Contains(getFutSymbol))
                                        {
                                            GetInstrumentSymbolsKite.Add(getFutSymbol);
                                        }
                                    }
                                    if (tablenameforFetching == m_futureName)
                                    {
                                        if (!GetFutureSymbolsKite.Contains(getFutSymbol))
                                        {
                                            GetFutureSymbolsKite.Add(getFutSymbol);
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            logger.LogMessage("GetAllSymbolsFromDBdata : Exception Error Message = " + e.Message, MessageType.Exception);
                        }
                    }
                }
                else
                {
                    logger.LogMessage("GetAllSymbolsFromDBdata : Table not exist", MessageType.Informational);
                }
            }
            catch (Exception ex)
            {
                logger.LogMessage("GetAllSymbolsFromDBdata : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
            //IRDSPM::Pratiksha::26-04-2021::For sending list to symbol setting form
            GetSymbolsKite = finalSymbolList;
            return finalSymbolList;
        }
//Pratiksha::26-04-2021::get exchange from db 
        public List<string> GetAllExchangeFromDBdata()
        {
            List<string> ExchangeList = new List<string>();

            try
            {
                if (OpenConnection() == true)
                {
                    ExchangeList = kite.fetchTablenamesForsearchboxList();
                    foreach (var item in ExchangeList)
                    {
                        Console.WriteLine(item.ToString());
                        string tablenameforFetching = item.ToString();
                        try
                        {
                            string sql_select = "select exchange from '" + tablenameforFetching + "'";
                            sql_cmd = new SQLiteCommand(sql_select, sql_con);
                            sql_read = sql_cmd.ExecuteReader();

                            if (sql_read != null && sql_read.HasRows)
                            {
                                while (sql_read.Read())
                                {
                                    getFutSymbol = (string)sql_read["exchange"];

                                    if (!finalexchangeList.Contains(getFutSymbol))
                                    {
                                        finalexchangeList.Add(getFutSymbol);
                                    }
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            logger.LogMessage("GetAllSymbolsFromDBdata : Exception Error Message = " + e.Message, MessageType.Exception);
                        }
                    }
                }
                else
                {
                    logger.LogMessage("GetAllSymbolsFromDBdata : Table not exist", MessageType.Informational);
                }
            }
            catch (Exception ex)
            {
                logger.LogMessage("GetAllSymbolsFromDBdata : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
            //IRDSPM::Pratiksha::26-04-2021::For sending list to symbol setting form
            GetExchangeKite = finalexchangeList;
            return finalexchangeList;
        }
        //IRDSPM::Pratiksha::14-09-2020::for returning list to buyForm
        public List<string> ReturnAllSymbolsFromDBdata()
        {
            //sanika::24-sep-2020::As sandip sir's suggestion 
            //GetAllSymbolsFromDBdata();
            return finalSymbolList;
        }


        //sanika::15-sep-2020::Added function for open position
        public void CalculateProfitLossForOpenPosition()
        {
            double pnl = 0;
            try
            {
                Trace.WriteLine("*****Calculate profit/loss started");
                Dictionary<string, PositionInfo> allPositions = new Dictionary<string, PositionInfo>();
                allPositions = m_GlobalpositionStore.GetAllPositions();
                if (allPositions.Count > 0)
                {
                    foreach (var p in allPositions)
                    {
                        var net = p.Value.netPosition;
                        string ltp = GetLastTradedPrice(net.TradingSymbol);
                        double sellValue = Convert.ToDouble(net.SellValue);
                        double buyValue = Convert.ToDouble(net.BuyValue);
                        if (ltp == "")
                        {
                            ltp = net.LastPrice.ToString();
                        }
                        double openValue = Math.Abs(Convert.ToDouble(ltp) * net.Quantity);
                        //WriteUniquelogs(net.TradingSymbol + " " + m_UserID, "ltp = " + ltp + " sellValue = " + sellValue + " buyValue = " + buyValue + " openValue = " + openValue,MessageType.Informational);
                        if (net.Quantity > 0)
                        {
                            sellValue += openValue;
                            double mtm = sellValue - buyValue;
                            //WriteUniquelogs(net.TradingSymbol + " " + m_UserID, "mtm = " + mtm, MessageType.Informational);
                            //Trace.WriteLine(net.TradingSymbol + " " + mtm);
                            pnl += mtm;
                            pnl = Math.Round(pnl, 2);
                            if (m_IndividualProfitLoss.ContainsKey(net.TradingSymbol + "_" + net.Exchange))
                            {
                                m_IndividualProfitLoss[net.TradingSymbol + "_" + net.Exchange] = mtm;
                            }
                            else
                            {
                                m_IndividualProfitLoss.Add(net.TradingSymbol + "_" + net.Exchange, mtm);
                            }
                        }
                        else if (net.Quantity < 0)
                        {
                            buyValue += openValue;
                            double mtm = sellValue - buyValue;
                            //WriteUniquelogs(net.TradingSymbol + " " + m_UserID, "mtm = " + mtm, MessageType.Informational);
                            // Trace.WriteLine(net.TradingSymbol + " " + mtm);
                            pnl += mtm;
                            pnl = Math.Round(pnl, 2);

                            if (m_IndividualProfitLoss.ContainsKey(net.TradingSymbol + "_" + net.Exchange))
                            {
                                m_IndividualProfitLoss[net.TradingSymbol + "_" + net.Exchange] = mtm;
                            }
                            else
                            {
                                m_IndividualProfitLoss.Add(net.TradingSymbol + "_" + net.Exchange, mtm);
                            }
                        }
                        else
                        {
                            double mtm = Convert.ToDouble(net.PNL);
                            double mtmTest = sellValue - buyValue;
                            //WriteUniquelogs(net.TradingSymbol + " " + m_UserID, "mtm = " + mtm + " mtmTest = "+ mtmTest, MessageType.Informational);
                            //Trace.WriteLine(net.TradingSymbol + " " + mtm);
                            pnl += mtm;
                            pnl = Math.Round(pnl, 2);
                            if (m_IndividualProfitLoss.ContainsKey(net.TradingSymbol + "_" + net.Exchange))
                            {
                                m_IndividualProfitLoss[net.TradingSymbol + "_" + net.Exchange] = mtm;
                            }
                            else
                            {
                                m_IndividualProfitLoss.Add(net.TradingSymbol + "_" + net.Exchange, mtm);
                            }
                        }
                    }
                }
                m_TotalProfitLoss = pnl;
                Trace.WriteLine("*****Calculate profit/loss Stopped");
                //logger.LogMessage("CalculateProfitLossForOpenPosition : m_TotalProfitLoss = " + m_TotalProfitLoss, MessageType.Informational);
            }
            catch (Exception e)
            {
                logger.LogMessage("CalculateProfitLossForOpenPosition : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        public double GetProfitFromDB(string tblName, string symbol, string datetime)
        {
            double profitValue = 0;
            string res = "";
            try
            {
                if (m_Mysql == null)
                {
                    m_Mysql = new MySQLConnectZerodha();
                    if (m_Mysql.Connect(logger, kite.dbServer, kite.dbUserid, kite.dbPassword, kite.databaseName))
                    {
                        m_isConnected = true;
                    }
                }
                if (m_Mysql.IsTableExists(tblName, kite.databaseName))
                {
                    profitValue = Convert.ToDouble(m_Mysql.FetchProfitData(datetime, tblName, symbol));
                }
            }
            catch (Exception er)
            {
                Console.WriteLine(er.Message);
                WriteUniquelogs(symbol, "GetProfitFromDB : Exception Error Message " + er.Message, MessageType.Exception);

            }
            return profitValue;

        }

        public void WriteProfitIntoDB(string tblName, string TradingSymbol, string Datetime, double Profit)
        {
            int firstSpaceIndex = Datetime.IndexOf(" ");
            string d = Datetime.Substring(0, firstSpaceIndex); // INAGX4
            string ti = Datetime.Substring(firstSpaceIndex + 1); // Agatti Island
            string[] spliitedDateTime = Datetime.Split(' ');
            string date = Convert.ToDateTime(d).ToString("yyyy-MM-dd");
            DateTime datetime = DateTime.Parse(ti, System.Globalization.CultureInfo.CurrentCulture);
            string t = datetime.ToString("HH:mm:00");
            Profit = Convert.ToDouble(RoundStopLossValueOfAFL(TradingSymbol, Convert.ToDecimal(Profit), true));
            Datetime = date + " " + t;
            try
            {
                if (m_Mysql == null)
                {
                    m_Mysql = new MySQLConnectZerodha();
                    if (m_Mysql.Connect(logger, kite.dbServer, kite.dbUserid, kite.dbPassword, kite.databaseName))
                    {
                        m_isConnected = true;
                    }
                }
                if (m_Mysql.IsTableExists(tblName, kite.databaseName))
                {
                }
                else
                {
                    //m_Mysql.CreateTable(tblName, "Symbol varchar(50) NOT NULL, Datetime varchar(50) NOT NULL, Profit double NOT NULL", "Datetime");
                    m_Mysql.ExecuteNonQueryCommand("CREATE TABLE `" + tblName + "` (" + "Symbol varchar(50) NOT NULL, Datetime varchar(50) NOT NULL, Profit double NOT NULL" + " );");
                }
                string insertData = " '" + TradingSymbol + "' ,'" + Datetime + "', '" + Profit + "'";

                m_Mysql.InsertProfitDataMultipleValues(tblName, insertData);
                //WriteUniquelogs(TradingSymbol, "Profit inserted into db", MessageType.Informational);
            }
            catch (Exception er)
            {
                Console.WriteLine(er.Message);
                WriteUniquelogs(TradingSymbol, "WriteProfitIntoDB : Exception Error Message " + er.Message, MessageType.Exception);
            }

        }

        public void DeleteAllProfitRecords(string tblName)
        {
            m_Mysql.DeleteAll(tblName);
        }

        public bool SetSymbolLastOrder(string TradingSymbol, string TransactionType)
        {
            bool isSymbolSet = false;
            string symbol = "";
            if (TradingSymbol.Contains("_"))
                symbol = TradingSymbol.Split('_')[1];
            else
                symbol = TradingSymbol;
            try
            {
                if (m_OrderListWithSymbolAndDirection.ContainsKey(symbol))
                {
                    m_OrderListWithSymbolAndDirection[symbol] = TransactionType;
                }
                else
                {
                    m_OrderListWithSymbolAndDirection.Add(symbol, TransactionType);
                }
                WriteUniquelogs(symbol + " " + m_UserID, "SetSymbolLastOrder : Set symbol  " + TradingSymbol + " with transactiontype " + TransactionType, MessageType.Informational);
            }
            catch (Exception e)
            {
                WriteUniquelogs(symbol + " " + m_UserID, "SetSymbolLastOrder : Exception Error Message " + e.Message, MessageType.Exception);
            }
            return isSymbolSet;
        }

        public bool RemoveSymbolLastOrder(string TradingSymbol, string TransactionType)
        {
            bool isSymbolRemove = false;
            bool isSymbolSet = false;
            string symbol = "";
            if (TradingSymbol.Contains("_"))
                symbol = TradingSymbol.Split('_')[1];
            else
                symbol = TradingSymbol;
            try
            {
                if (m_OrderListWithSymbolAndDirection.ContainsKey(symbol))
                {
                    m_OrderListWithSymbolAndDirection.Remove(symbol);
                    WriteUniquelogs(symbol + " " + m_UserID, "RemoveSymbolLastOrder : Removed symbol from list " + TradingSymbol, MessageType.Informational);
                }
                else
                {
                    WriteUniquelogs(symbol + " " + m_UserID, "RemoveSymbolLastOrder :List not contains symbol " + TradingSymbol, MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(symbol + " " + m_UserID, "RemoveSymbolLastOrder : Exception Error Message " + e.Message, MessageType.Exception);
            }
            return isSymbolRemove;
        }

        public string GetSymbolLastOrder(string TradingSymbol)
        {
            string lastOrder = "NA";
            string symbol = "";
            if (TradingSymbol.Contains("_"))
                symbol = TradingSymbol.Split('_')[1];
            else
                symbol = TradingSymbol;
            try
            {
                if (m_OrderListWithSymbolAndDirection.ContainsKey(symbol))
                {
                    lastOrder = m_OrderListWithSymbolAndDirection[symbol];
                    WriteUniquelogs(symbol + " " + m_UserID, "GetSymbolLastOrder : symbol " + TradingSymbol + " transaction type " + lastOrder, MessageType.Informational);
                }
                else
                {
                    WriteUniquelogs(symbol + " " + m_UserID, "GetSymbolLastOrder : List not contains symbol " + TradingSymbol, MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(symbol + " " + m_UserID, "GetSymbolLastOrder : Exception Error Message " + e.Message, MessageType.Exception);
            }

            return lastOrder;
        }

        public bool AddManuallyOrderQuantity(string TradingSymbol, int Quantity)
        {
            bool isSymbolSet = false;
            string symbol = "";
            if (TradingSymbol.Contains("_"))
                symbol = TradingSymbol.Split('_')[1];
            else
                symbol = TradingSymbol;
            try
            {
                if (m_OrderListWithSymbolAndQuantity.ContainsKey(symbol))
                {
                    m_OrderListWithSymbolAndQuantity[symbol] += Quantity;
                }
                else
                {
                    m_OrderListWithSymbolAndQuantity.Add(symbol, Quantity);
                }
                WriteUniquelogs(symbol + " " + m_UserID, "AddManuallyOrderQuantity : Set symbol  " + TradingSymbol + " with quantity " + Quantity, MessageType.Informational);
            }
            catch (Exception e)
            {
                WriteUniquelogs(symbol + " " + m_UserID, "AddManuallyOrderQuantity : Exception Error Message " + e.Message, MessageType.Exception);
            }
            return isSymbolSet;
        }

        public bool RemoveManuallyOrderQuantity(string TradingSymbol)
        {
            bool isSymbolRemove = false;
            string symbol = "";
            if (TradingSymbol.Contains("_"))
                symbol = TradingSymbol.Split('_')[1];
            else
                symbol = TradingSymbol;
            try
            {
                if (m_OrderListWithSymbolAndQuantity.ContainsKey(symbol))
                {
                    m_OrderListWithSymbolAndQuantity.Remove(symbol);
                    WriteUniquelogs(symbol + " " + m_UserID, "RemoveManuallyOrderQuantity : Removed symbol from list " + TradingSymbol, MessageType.Informational);
                }
                else
                {
                    WriteUniquelogs(symbol + " " + m_UserID, "RemoveManuallyOrderQuantity :List not contains symbol " + TradingSymbol, MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(symbol + " " + m_UserID, "RemoveManuallyOrderQuantity : Exception Error Message " + e.Message, MessageType.Exception);
            }
            return isSymbolRemove;
        }

        public int GetManuallyOrderQuantity(string TradingSymbol)
        {
            int lastOrderQuantity = 0;
            string symbol = "";
            if (TradingSymbol.Contains("_"))
                symbol = TradingSymbol.Split('_')[1];
            else
                symbol = TradingSymbol;
            try
            {
                if (m_OrderListWithSymbolAndQuantity.ContainsKey(symbol))
                {
                    lastOrderQuantity = m_OrderListWithSymbolAndQuantity[symbol];
                    WriteUniquelogs(symbol + " " + m_UserID, "GetManuallyOrderQuantity : symbol " + TradingSymbol + " quantity " + lastOrderQuantity, MessageType.Informational);
                }
                else
                {
                    WriteUniquelogs(symbol + " " + m_UserID, "GetManuallyOrderQuantity : List not contains symbol " + TradingSymbol, MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(symbol + " " + m_UserID, "GetManuallyOrderQuantity : Exception Error Message " + e.Message, MessageType.Exception);
            }

            return lastOrderQuantity;
        }

        public int GetOrderQuantityByDirection(string TradingSymbol, string TransactionType)
        {
            int lastOrderQuantity = 0;
            try
            {
                dynamic AllOrder = GetAllOrders();
                if (AllOrder.Count == 0)
                {
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOrderQuantityByDirection : AllOrder count is 0 TradingSymbol " + TradingSymbol + " TransactionType " + TransactionType, MessageType.Informational);
                }
                for (int i = 0; i <= AllOrder.Count - 1; i++)
                {
                    var order = AllOrder[i];

                    if (order.TransactionType == TransactionType && order.Status == Constants.ORDER_STATUS_COMPLETE && order.Tradingsymbol == TradingSymbol)
                    {
                        lastOrderQuantity += order.Quantity;
                    }
                }
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOrderQuantityByDirection : TradingSymbol " + TradingSymbol + " TransactionType " + TransactionType + " quantity " + lastOrderQuantity, MessageType.Informational);
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOrderQuantityByDirection : Exception Error Message " + e.Message, MessageType.Exception);
            }

            return lastOrderQuantity;
        }

        public bool GetChangeInPercent(string TradingSymbol, string tickTime, out double highPercent, out double lowPercent)
        {
            highPercent = 0;
            lowPercent = 0;
            try
            {
                kite.GetChangeInPercent(TradingSymbol, tickTime, out highPercent, out lowPercent);
                return true;
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetChangeInPercent : Exception Error Message " + e.Message, MessageType.Exception);
            }
            return false;
        }

        //sanika::21-oct-2020::added new function of close order which return order id
        public string CloseOrderEx(string TradingSymbol, string Exchange, string Product)
        {
            string orderId = "NA";
            try
            {
                string newOrderBuyOrSell = "";
                int newOrderQty = 0;
                string mappedSymbol = TradingSymbol + "|" + Exchange;
                if (CancelAllPendingOrder(TradingSymbol, Exchange))
                {
                    //nothing
                }
                PositionInfo currentPosition;
                if (!GetCurrentPosition(mappedSymbol, Product, out currentPosition))
                {
                    string errorMsg = string.Format("CloseOrderEx : Not found current position for symbol : {0}", mappedSymbol);
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, errorMsg, MessageType.Error);

                    if (bGetCurrentPositionError)
                    {
                        return orderId;
                    }
                }
                else
                {
                    //sanika::28-sep-2020::added direction in logs/toast
                    string direction = "";
                    string logMessage = string.Format("CloseOrderEx : Current position fetched from server for mapped symbol: {0} Day Position: {1} , Net Position: {2}",
                                                  mappedSymbol, currentPosition.dayPosition.Quantity, currentPosition.netPosition.Quantity);
                    WriteUniquelogs(TradingSymbol + " " + m_UserID, logMessage, MessageType.Informational);
                    if ((currentPosition.dayPosition.Quantity > 0 || currentPosition.netPosition.Quantity > 0))
                    {
                        newOrderBuyOrSell = Constants.TRANSACTION_TYPE_SELL;
                        //sanika::28-sep-2020::added direction in logs/toast
                        direction = Constants.TRANSACTION_TYPE_BUY; ;
                    }
                    else if ((currentPosition.dayPosition.Quantity < 0 || currentPosition.netPosition.Quantity < 0))
                    {
                        newOrderBuyOrSell = Constants.TRANSACTION_TYPE_BUY;
                        //sanika::28-sep-2020::added direction in logs/toast
                        direction = Constants.TRANSACTION_TYPE_SELL;
                    }

                    //IRDS::17-JUL-2020 :: Add comment because day position quantity got wrong :: sandip/sanika

                    //if (Math.Abs(currentPosition.dayPosition.Quantity) > Math.Abs(currentPosition.netPosition.Quantity))
                    //    newOrderQty = Math.Abs(currentPosition.dayPosition.Quantity);
                    //else

                    newOrderQty = Math.Abs(currentPosition.netPosition.Quantity);

                    if (newOrderQty != 0)
                    {
                        WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseOrderEx :newOrderQty is not zero" + newOrderQty, MessageType.Informational);
                        orderId = PlaceOrder(Exchange, TradingSymbol, newOrderBuyOrSell, newOrderQty, Constants.ORDER_TYPE_MARKET, Product: Product);
                        if (orderId != "NA")
                        {
                            double openPrice = GetOpenPostionPricebyOrderID(TradingSymbol, Exchange, orderId);
                            WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseOrderEx : Order Complete at price = " + openPrice, MessageType.Informational);
                            WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseOrderEx : order closed successfully", MessageType.Informational);
                            //sanika::28-sep-2020::added direction in logs/toast
                            WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + TradingSymbol + " " + direction + " Order Closed successfully!! ");
                            m_ExceptionCounter++;

                            //sanika::16-oct-2020::add information after placing order
                            DateTime dateTime = DateTime.Now;
                            m_LocalOrderStructureInfo.AddOrUpdateOrderInformation(TradingSymbol: TradingSymbol, Exchange: Exchange, ClosedTransactionType: newOrderBuyOrSell, Product: Product, ClosedQuantity: newOrderQty, ClosedOrderDateTime: dateTime, ClosedOrderId: orderId);
                            return orderId;
                        }
                        else
                        {
                            WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseOrderEx : order not closed", MessageType.Informational);
                            //sanika::28-sep-2020::added direction in logs/toast
                            WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + TradingSymbol + " " + direction + " Order not Closed");
                        }
                    }
                    else
                    {
                        WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseOrderEx :newOrderQty is zero", MessageType.Informational);
                        WriteGUIlogs(m_ExceptionCounter, DateTime.Now + " " + TradingSymbol + " Order closed already!!");
                        m_ExceptionCounter++;
                    }
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "CloseOrderEx :  Exception Error Message " + e.Message, MessageType.Exception);
            }
            return orderId;
        }

        //IRDSPM::PRatiksha::17-10-2020::Get all details for subsription details
        public string FetchUserdetails(string clientid, string hardwareID)
        {
            string data = "";
            try
            {
                string strQuery = "SELECT * FROM UserDetails ";
                if (m_MySqlConnection == null)
                {
                    if (logger == null)
                    {
                        logger = Logger.Instance;
                    }
                    m_MySqlConnection = new MySQLConnectZerodha();
                    m_isMysqlconnected = m_MySqlConnection.Connect(logger, m_serverUrl, m_username, m_password, m_Dbname);
                }
                if (m_isMysqlconnected)
                {
                    if (m_MySqlConnection.IsTableExists("UserDetails", m_Dbname))
                    {
                        data = m_MySqlConnection.FetchDataForSubscription(hardwareID);
                    }
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("RegistrationProcess", "FetchUserdetails :  Exception Error Message = " + ex.Message, MessageType.Exception);
            }
            return data;
        }

        //sanika::5-nov-2020::added for OpenBreakoutStrategy
        public bool GetHighLowFromDB(string TradingSymbol, string StartTime, string EndTime, out double dHigh, out double dLow)
        {
            dHigh = 0;
            dLow = 0;
            kite.GetHighLowFromDB(TradingSymbol, StartTime, EndTime, out dHigh, out dLow);
            return true;
        }

        //sanika::12-Nov-2020::Added to get product of open position
        public string GetOpenPostionProductByOrderID(string TradingSymbol, string Exchange, string orderId)
        {
            string product = "productnotfound";
            try
            {
                OrderExt orderExt;
                if (m_GlobalOrderStore.GetOpenOrderbyID(TradingSymbol, Exchange, orderId, out orderExt))
                {
                    if (orderExt == null)
                    {
                        WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPostionProductByOrderId : order info count is zero", MessageType.Informational);
                        return product;
                    }
                    if (orderExt.order.Status == Constants.ORDER_STATUS_COMPLETE)
                    {
                        product = orderExt.order.Product;
                    }
                    //WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPostionProductByOrderId : direction " + direction, MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetOpenPostionProductByOrderId : Exception Error Message = " + e.Message, MessageType.Informational);
            }
            return product;
        }

        //Pratiksha::04-12-2020::fetching status
        public bool FetchUserdetailsfromBlockuser(string hardwareID)
        {
            bool data = false;
            try
            {
                if (m_MySqlConnection == null)
                {
                    if (logger == null)
                    {
                        logger = Logger.Instance;
                    }
                    m_MySqlConnection = new MySQLConnectZerodha();
                    m_isMysqlconnected = m_MySqlConnection.Connect(logger, m_serverUrl, m_username, m_password, m_Dbname);
                    WriteUniquelogs("RegistrationProcess", "FetchUserdetailsfromBlockuser :  m_isMysqlconnected = " + m_isMysqlconnected, MessageType.Informational);
                }
                if (m_isMysqlconnected)
                {
                    if (!m_MySqlConnection.IsTableExists("UserDetails", m_Dbname))
                    {
                        WriteUniquelogs("RegistrationProcess", "FetchUserdetailsfromBlockuser :  IsTableExists = " + "Table not exist", MessageType.Informational);
                        //create table 
                        m_MySqlConnection.ExecuteNonQueryCommand("Create Table UserDetails(ID int(11),Name varchar(255),Usertype varchar(255),EmailId varchar(255),MobileNo varchar(255) ,UserName varchar(255),Password varchar(255),UserAddress varchar(255),City varchar(255),UserState varchar(255),ClientID varchar(255),ExpiryDate double,Remark varchar(255),Status varchar(255),OnlineInfo varchar(255),AdminId varchar(255),AffiliateID varchar(255),BlockStatus bool, lastlogindate double, PRIMARY KEY(ClientID))");
                    }
                    data = m_MySqlConnection.FetchDataForblock(hardwareID);
                    WriteUniquelogs("RegistrationProcess", "FetchUserdetailsfromBlockuser :  Block user Status = " + data, MessageType.Informational);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("RegistrationProcess", "FetchUserdetailsfromBlockuser :  Exception Error Message = " + ex.Message, MessageType.Exception);
            }
            return data;
        }

        public bool RiskManagement(string SymbolName, string Exchange, double riskPercent)
        {
            double close = 0;
            double lastPrice = 0;
            string price = GetLastTradedPrice(SymbolName);
            if (price != "")
            {
                lastPrice = Convert.ToDouble(price);
            }
            else
            {
                WriteUniquelogs(SymbolName + " " + m_UserID, "RiskManagement : Not get lastPrice. Returning false", MessageType.Informational);
                return false;
            }
            if (m_sqliteConnectZerodha == null)
            {
                m_sqliteConnectZerodha = new SqliteConnectZerodha();
                isconnectedToSqlite = m_sqliteConnectZerodha.Connect("zerodhadata.db");
            }
            if (isconnectedToSqlite == false)
            {
                isconnectedToSqlite = m_sqliteConnectZerodha.Connect("zerodhadata.db");
            }
            if (isconnectedToSqlite)
            {
                close = m_sqliteConnectZerodha.GetYesterdayClose(SymbolName);
                if (close == 0)
                {
                    close = Convert.ToDouble(GetClosePrice(SymbolName));
                    WriteUniquelogs(SymbolName + " " + m_UserID, "RiskManagement : get close as 0 from db", MessageType.Informational);
                }
            }
            else
            {
                WriteUniquelogs(SymbolName + " " + m_UserID, "RiskManagement : Not able to connect to db", MessageType.Informational);
                close = Convert.ToDouble(GetClosePrice(SymbolName));
            }

            double CalculatedPercent = (lastPrice * riskPercent) / 100;
            double priceDiff = Math.Abs(lastPrice - close);
            WriteUniquelogs(SymbolName + " " + m_UserID, "RiskManagement : lastPrice " + lastPrice + " riskPercent " + riskPercent + " close " + close + " CalculatedPercent " + CalculatedPercent + " priceDiff " + priceDiff, MessageType.Informational);
            if (priceDiff <= CalculatedPercent)
            {
                WriteUniquelogs(SymbolName + " " + m_UserID, "RiskManagement : Returning true", MessageType.Informational);
                return true;
            }
            else
            {
                WriteUniquelogs(SymbolName + " " + m_UserID, "RiskManagement : Returning false", MessageType.Informational);
                return false;
            }
        }
        //IRDSPM:::Pratiksha::22-12-2020::For updating last login date in db
        public bool UpdateDateinDB(string ClientId)
        {
            bool bolUpdateInfo = false;
            double dCurrentDate;
            DateTime dtCurrentDate;
            try
            {
                if (m_MySqlConnection == null)
                {
                    m_MySqlConnection = new MySQLConnectZerodha();
                    m_isMysqlconnected = m_MySqlConnection.Connect(logger, m_serverUrl, m_username, m_password, m_Dbname);
                }

                if (m_isMysqlconnected)
                {
                    dtCurrentDate = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));
                    dCurrentDate = dtCurrentDate.ToOADate();
                    string updatecmd = "update UserDetails set lastlogindate = " + dCurrentDate + " where ClientID = '" + ClientId + "'";
                    m_MySqlConnection.ExecuteNonQueryCommand(updatecmd);
                    bolUpdateInfo = true;
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("RegistrationProcess", "UpdateDateinDB :  Exception Error Message = " + ex.Message, MessageType.Exception);
            }
            return bolUpdateInfo;
        }

        public void GetFirstTwoTick(string TradingSymbol, string FirstTickTime, string SecondTickTime, out double firstCandleHigh, out double firstCandleLow, out double secondCandleHigh, out double secondCandleLow)
        {
            firstCandleHigh = firstCandleLow = secondCandleHigh = secondCandleLow = 0;
            kite.GetFirstTwoTick(TradingSymbol, FirstTickTime, SecondTickTime, out firstCandleHigh, out firstCandleLow, out secondCandleHigh, out secondCandleLow);
        }

        public void GetCurrentHighLow(string TradingSymbol, string startTime, string endTime, int interval, out double dHigh, out double dLow)
        {
            string symbol = "";
            if (TradingSymbol.Contains('.'))
            {
                symbol = TradingSymbol.Split('.')[0];
            }
            else
            {
                symbol = TradingSymbol;
            }
            WriteUniquelogs(symbol + " " + m_UserID, "GetCurrentHighLow : startTime " + startTime + " endTime " + endTime + " interval " + interval, MessageType.Informational);
            dHigh = dLow = 0;
            string token = kite.GetToken(TradingSymbol);
            string CurrentDate = DateTime.Now.ToString("yyyy-MM-dd");
            DateTime startDateTime = DateTime.ParseExact(CurrentDate + " " + startTime + ":00", "yyyy-MM-dd HH:mm:ss", null);
            DateTime endDateTime = DateTime.ParseExact(CurrentDate + " " + endTime + ":00", "yyyy-MM-dd HH:mm:ss", null);
            string period = GetInterval(interval);
            List<Historical> historicals = kite.GetHistoricalData(token, startDateTime, endDateTime, period, false);
            if (historicals.Count() > 0)
            {
                dHigh = Convert.ToDouble(historicals[0].High);
                dLow = Convert.ToDouble(historicals[0].Low);
                WriteUniquelogs(symbol + " " + m_UserID, "GetCurrentHighLow : dHigh " + dHigh + " dLow " + dLow, MessageType.Informational);
            }
            else
            {
                WriteUniquelogs(symbol + " " + m_UserID, "GetCurrentHighLow : historicals data length fetch as 0", MessageType.Informational);
            }
        }

        public void GetTwoTickFromHistoricalData(string TradingSymbol, string FirstTickTime, string SecondTickTime, int interval, out double firstCandleHigh, out double firstCandleLow, out double secondCandleHigh, out double secondCandleLow)
        {
            firstCandleHigh = firstCandleLow = secondCandleHigh = secondCandleLow = 0;
            string token = kite.GetToken(TradingSymbol);
            string CurrentDate = DateTime.Now.ToString("yyyy-MM-dd");
            DateTime startDateTime = DateTime.ParseExact(CurrentDate + " " + FirstTickTime + ":00", "yyyy-MM-dd HH:mm:ss", null);
            DateTime endDateTime = DateTime.ParseExact(CurrentDate + " " + SecondTickTime + ":00", "yyyy-MM-dd HH:mm:ss", null);
            string period = GetInterval(interval);
            List<Historical> historicals = kite.GetHistoricalData(token, startDateTime, endDateTime, period, false);
            if (historicals.Count() > 0)
            {
                firstCandleHigh = Convert.ToDouble(historicals[0].High);
                firstCandleLow = Convert.ToDouble(historicals[0].Low);
                secondCandleHigh = Convert.ToDouble(historicals[1].High);
                secondCandleLow = Convert.ToDouble(historicals[1].Low);
            }
        }

        public List<string> GetHighLowClose(string TradingSymbol, int interval, int period, string startTime, string endTime)
        {
            string symbol = TradingSymbol;
            if (TradingSymbol.Contains("."))
                symbol = TradingSymbol.Split('.')[0];
            List<string> list = new List<string>();
            try
            {
                string dateTime = DateTime.Now.ToString("yyyy-MM-dd");
                DateTime todateTime = DateTime.ParseExact(dateTime + " " + endTime + ":00", "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                DateTime startdateTime = DateTime.ParseExact(startTime + ":00", "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                todateTime = todateTime.AddDays(1);
                string intervalPeriod = GetInterval(interval);
                string token = kite.GetToken(TradingSymbol);
                List<Historical> historicals = new List<Historical>();
                historicals = kite.GetHistoricalData(token, startdateTime, todateTime, intervalPeriod, false);
                if (historicals.Count > 0)
                {
                    DateTime marketCloseTime = DateTime.ParseExact(dateTime + " " + "15:30:00", "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                    foreach (var data in historicals)
                    {
                        if (data.TimeStamp < marketCloseTime)
                        {
                            string d = data.High + "," + data.Low + "," + data.Close + "," + data.TimeStamp.ToString();
                            list.Add(d);
                        }
                    }
                }
                else
                {
                    WriteUniquelogs(symbol + " " + m_UserID, "GetHighLowClose : Historical Data Not fetched ", MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(symbol + " " + m_UserID, "GetHighLowClose :Exception Error Message " + e.Message, MessageType.Exception);
            }
            return list;
        }

        public bool IsOverallAllProfitLossHit()
        {
            return m_isSqrOff;
        }

        public double GetIndividualProfitLoss(string TradingSymbol, string Exchange)
        {
            double profitLoss = 0;
            try
            {
                if (m_IndividualProfitLoss.ContainsKey(TradingSymbol + "_" + Exchange))
                {
                    profitLoss = m_IndividualProfitLoss[TradingSymbol + "_" + Exchange];
                }
            }
            catch (Exception e)
            {
                profitLoss = 0;
            }

            return profitLoss;
        }

        public void UpdatePNLInFile()
        {
            try
            {
                if (DateTime.Now.ToOADate() > m_TimeToUpdatePNLInFile)
                {
                    m_TimeToUpdatePNLInFile = DateTime.Now.AddMinutes(1).ToOADate();
                    string today = DateTime.Now.ToString("dd-MM-yyyy");
                    string time = DateTime.Now.ToString("HH:mm:ss");
                    decimal marginUsed = 0;
                    decimal marginAvailable = 0;
                    decimal openingBalance = 0;
                    double pnl = m_TotalProfitLoss;

                    if (userMargin != null && userMargin.Count != 0)
                    {
                        marginUsed = Math.Round(userMargin["Utilised"], 2);
                        marginAvailable = Math.Round(userMargin["Net"], 2);
                        openingBalance = userMargin["Available"];
                    }
                    if (!File.Exists(m_PNLFilePath))
                    {
                        string header = String.Join(Environment.NewLine, "Date,Time,Margin Available,Margin Used,Opening Balance,PNL");
                        header += Environment.NewLine;
                        System.IO.File.AppendAllText(m_PNLFilePath, header);
                    }
                    using (StreamWriter streamWriter = File.AppendText(m_PNLFilePath))
                    {
                        String csv = String.Join(Environment.NewLine, today + "," + time + "," + marginAvailable + "," + marginUsed + "," + openingBalance + "," + pnl);
                        streamWriter.WriteLine(csv);
                    }
                }
            }
            catch (Exception e)
            {
                logger.LogMessage("UpdatePNLInFile : Exception Error Message : " + e.Message, MessageType.Exception);
            }
        }
        //sanika::21-July-2021::Added to update margin after every 1 min

        static bool m_IsMarginLoad = false;
        public void UpdateMargin()
        {
            try
            {//sanika::13-July-2021::flag condition
                if (DateTime.Now.ToOADate() > m_TimeToUpdateMargin || m_IsMarginLoad == true)
                {
                    m_IsMarginLoad = false;
                    m_TimeToUpdateMargin = DateTime.Now.AddMinutes(1).ToOADate();
                    userMargin = GetMargin();
                }
            }
            catch (Exception e)
            {
                logger.LogMessage("UpdateMargin : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        public dynamic GetMarginFromStructure()
        {
            return userMargin;
        }

        public double GetLimitPricebyOrderID(string TradingSymbol, string Exchange, string orderId)
        {
            double Price = 0;
            try
            {
                OrderExt orderExt;
                if (m_GlobalOrderStore.GetOpenOrderbyID(TradingSymbol, Exchange, orderId, out orderExt))
                {
                    if (orderExt == null)
                    {
                        WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetLimitPricebyOrderID : order info count is zero", MessageType.Informational);
                        return 0;
                    }
                    if (orderExt.order.Status == Constants.ORDER_STATUS_OPEN)
                    {
                        Price = Convert.ToDouble(orderExt.order.Price);
                    }
                    //WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetLimitPricebyOrderID : Price " + Price, MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetLimitPricebyOrderID : Exception Error Message = " + e.Message, MessageType.Informational);
            }
            return Price;
        }
        public string GetLastBarHigh(string TradingSymbol)
        {
            string highPrice = null;
            try
            {
                if (m_TickData != null)
                {
                    highPrice = m_TickData.GetLastBarHigh(TradingSymbol).ToString();
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetLastBarHigh : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return highPrice;
        }

        public string GetLastBarLow(string TradingSymbol)
        {
            string lowPrice = null;
            try
            {
                if (m_TickData != null)
                {
                    lowPrice = m_TickData.GetLastBarLow(TradingSymbol).ToString();
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + m_UserID, "GetLastBarLow : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return lowPrice;
        }

        //IRDSPM::Pratiksha::15-06-2021::For clear all the symbols from trading symbol list
        public void ClearTradingSymbols()
        {
            kite.TradingSymbol.Clear();
        }
        public string GetMessageforNotification()
        {
            return m_Notification.GetMessageforNotification();
        }
        public void AddNotificationInQueue(string message)
        {
            m_Notification.AddNotificationInQueue(message);
        }
        //sanika::9-Jun-2021::Added to clear master list
        public void ClearMasterList()
        {
            m_DictionaryForOrderId.Clear();
        }
        
        public string GetLotsizeexpiryDateFromDBdata(string symbol, string table, string expiryDt)
        {
            string quantity = "0";
            try
            {
                if (OpenConnection() == true)
                {
                    try
                    {
                        //IRDSPM::PRatiksha::04-06-2021::Change the query
                        string sql_select = "select DISTINCT lotsize from '" + table + "' where symbol = '" + symbol + "' AND expiryDate = '" + expiryDt + "';";
                        sql_cmd = new SQLiteCommand(sql_select, sql_con);
                        sql_read = sql_cmd.ExecuteReader();

                        if (sql_read != null && sql_read.HasRows)
                        {
                            while (sql_read.Read())
                            {
                                quantity = Convert.ToString(sql_read["lotsize"]);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        logger.LogMessage("GetLotsizeexpiryDateFromDBdata : Exception Error Message = " + e.Message, MessageType.Exception);
                    }
                }
                else
                {
                    logger.LogMessage("GetLotsizeexpiryDateFromDBdata : Table not exist", MessageType.Informational);
                }
            }
            catch (Exception ex)
            {
                logger.LogMessage("GetLotsizeexpiryDateFromDBdata : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
            return quantity;
        }
        //Pratiksha::26-04-2021::get exchange from db 
        public List<string> GetSymbolexpiryDateFromDBdata(string symbol, string table)
        {
            List<string> expirydateList = new List<string>();
            string expirydate;
            try
            {
                if (OpenConnection() == true)
                {
                    try
                    {
                        //IRDSPM::PRatiksha::04-06-2021::Change the query
                        string sql_select = "select DISTINCT expiryDate from '" + table + "' where symbol = '" + symbol + "' AND expiryDate >= date()" + "  ORDER BY expiryDate ASC;";
                        sql_cmd = new SQLiteCommand(sql_select, sql_con);
                        sql_read = sql_cmd.ExecuteReader();

                        if (sql_read != null && sql_read.HasRows)
                        {
                            while (sql_read.Read())
                            {
                                expirydate = (string)sql_read["expiryDate"];
                                string Date = expirydate.Replace("T", " ");
                                string FullDate = Convert.ToDateTime(Date).ToString("dd-MMM-yy").Replace("-", string.Empty).ToUpper();
                                if (!expirydateList.Contains(FullDate))
                                {
                                    expirydateList.Add(FullDate);
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        logger.LogMessage("GetSymbolExchangeFromDBdata : Exception Error Message = " + e.Message, MessageType.Exception);
                    }
                }
                else
                {
                    logger.LogMessage("GetSymbolExchangeFromDBdata : Table not exist", MessageType.Informational);
                }
            }
            catch (Exception ex)
            {
                logger.LogMessage("GetAllSymbolsFromDBdata : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
            return expirydateList;
        }
        public string getOptionsInstrumentToken(string symbolName, string tableName)
        {
            string token = "";
            try
            {
                if (sql_conLotSize == null)
                {
                    OpenConnectionForLOtSize();
                }
                SQLiteTransaction TrToken;
                SQLiteCommand Sql_cmdToken;
                SQLiteDataReader Sql_readToken;
                //IRDSPM::Pratiksha::17-06-2021::remove hardcoded tableName
                string query = "select InstrumentToken from " + tableName + " where optionsSymbol = '" + symbolName + "'";
                Sql_cmdToken = new SQLiteCommand(query, sql_conLotSize);
                Sql_readToken = Sql_cmdToken.ExecuteReader();
                if (Sql_readToken != null && Sql_readToken.HasRows)
                {
                    while (Sql_readToken.Read())
                    {
                        token = (Sql_readToken["InstrumentToken"]).ToString();
                    }
                }

    }
            catch (Exception e)
            {
                logger.LogMessage("getOptionsInstrumentToken : Exception Error Message = " + e.Message, MessageType.Exception);
                Console.WriteLine("Exception : " + e.Message);
            }
            return token;
        }
        public string GetToken(string symbol)
        {
            string token = kite.GetToken(symbol);
            return token;
        }
        public int GetOptionsLotSize(string OptionsSymbol, string Exchange)
        {
            int lotsize = 0;
            try
            {
                if (sql_conLotSize == null)
                {
                    OpenConnectionForLOtSize();
                }

                string tableName = Exchange;

                string sql = "SELECT lotSize from '" + tableName + "' WHERE optionsSymbol = '" + OptionsSymbol + "'";
                sql_cmd = new SQLiteCommand(sql, sql_conLotSize);
                sql_read = sql_cmd.ExecuteReader();

                if (sql_read != null && sql_read.HasRows)
                {
                    if (sql_read.Read())
                    {
                        lotsize = (int)sql_read["lotsize"];
                    }
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(OptionsSymbol + " " + m_UserID, "GetLotSize :  Exception " + e.Message, MessageType.Exception);
            }
            WriteUniquelogs(OptionsSymbol + " " + m_UserID, "GetLotSize :  lotsize " + lotsize, MessageType.Informational);
            return lotsize;
        }

        public string GetSymbolName(string token, string exchange)
        {
            if(kite!= null)
            {
                return kite.GetSymbol(token, exchange);
            }
            return "";
        }
        
       //IRDSPM::Pratiksha::09-07-2021::For toastNotification
        public void AddNotificationInQueueToast(string message)
        {
            WriteGUIlogs(m_ExceptionCounter, message);
            m_ExceptionCounter++;
        }
    }
    }

