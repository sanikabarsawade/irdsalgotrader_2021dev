﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRDSAlgoOMS
{
    class InstrumentStore
    {
        Kite k;
        private Dictionary<string, string> ABtoZerodhaSymbolMapping;
        private Dictionary<string, Instrument> orderStoreBySymbolandExchange;
        private Dictionary<string, string> symbolWiseLotSize;        

        public InstrumentStore()
        {
            orderStoreBySymbolandExchange = new Dictionary<string, Instrument>();
            ABtoZerodhaSymbolMapping = new Dictionary<string, string>();
            symbolWiseLotSize = new Dictionary<string, string>();
        }

        public string GetKey(string tradingSymbol, string exchnage)
        {
            return tradingSymbol + "|" + exchnage;
        }
        public void AddInstrument(Instrument newInstrument)
        {
            var key = GetKey(newInstrument.TradingSymbol, newInstrument.Exchange);
            if (!orderStoreBySymbolandExchange.ContainsKey(key))
                orderStoreBySymbolandExchange[key] = newInstrument;
            //else
            //    orderStoreBySymbolandExchange.Add(key, newInstrument);
        }

        public bool GetInstrument(string tradingSymbol, string exchnage, out Instrument existingInstrument)
        {
            var key = GetKey(tradingSymbol, exchnage);
            return orderStoreBySymbolandExchange.TryGetValue(key, out existingInstrument);
        }

        public bool GetInstrument(string mappedSymbol, out Instrument existingInstrument)
        {
            return orderStoreBySymbolandExchange.TryGetValue(mappedSymbol, out existingInstrument);
        }

        public void AddMapping(string symbol, string mappedSymbol)
        {
            if (!ABtoZerodhaSymbolMapping.ContainsKey(symbol))
                ABtoZerodhaSymbolMapping.Add(symbol, mappedSymbol);
        }

        public bool GetMappedZerodhaSymbol(string symbol, out string mappedSymbol)
        {
            return ABtoZerodhaSymbolMapping.TryGetValue(symbol, out mappedSymbol);
        }

        public void AddLotSize(string symbol, string lotSize)
        {
            if (!symbolWiseLotSize.ContainsKey(symbol))
                symbolWiseLotSize.Add(symbol, lotSize);
        }
        public bool GetLotSize(string symbol, out string lotSize)
        {
            return symbolWiseLotSize.TryGetValue(symbol, out lotSize);
        }

    }
}
