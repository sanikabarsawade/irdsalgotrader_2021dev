﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRDSAlgoOMS
{
    public class PositionStore
    {
        private Dictionary<string, PositionInfo> positionStore;
        private Dictionary<string, OpenPositionStore> TimeAndOrderId;
        private static readonly object positiontickLock = new object();
        public List<string> updatedSymbols;
        public string GetKey(string tradingSymbol, string exchange)
        {
            return tradingSymbol + "|" + exchange;
        }
        public int GetCount()
        {
            return positionStore.Count();
        }
        public PositionStore()
        {
            positionStore = new Dictionary<string, PositionInfo>();
            TimeAndOrderId = new Dictionary<string, OpenPositionStore>();
            updatedSymbols = new List<string>();

            //foreach (var instrument in instruments)
            //{
            //    positionStore.Add(GetKey(instrument.TradingSymbol, instrument.Exchange), new PositionInfo());
            //}
        }

        public bool GetPosition(string mappedSymbol, out PositionInfo positionInfo)
        {
            lock (positiontickLock)
            {
                return positionStore.TryGetValue(mappedSymbol, out positionInfo);
            }
        }

        public bool GetPosition(string tradingSymbol, string exchange, out PositionInfo positionInfo)
        {
            var key = GetKey(tradingSymbol, exchange);
            return positionStore.TryGetValue(key, out positionInfo);
        }

        public void Clear()
        {
            lock (positiontickLock)
            {
                positionStore.Clear();
            }
        }

        public void AddOrUpdatePositionInfo(string tradingSymbol, string exchange, Position newPosition, bool dayOrNet)
        {
            lock (positiontickLock)
            {
                var key = GetKey(tradingSymbol, exchange);
                if (positionStore.ContainsKey(key))
                {                    
                    if (dayOrNet)
                        positionStore[key].dayPosition = newPosition;
                    else
                        positionStore[key].netPosition = newPosition;
                    positionStore[key].LastUpdateTime = DateTime.Now;

                    if (!updatedSymbols.Contains(key))
                        updatedSymbols.Add(key);
                }
                else
                {
                    PositionInfo positionInfo = new PositionInfo();
                    if (dayOrNet)
                        positionInfo.dayPosition = newPosition;
                    else
                        positionInfo.netPosition = newPosition;
                    positionInfo.LastUpdateTime = DateTime.Now;

                    positionStore.Add(key, positionInfo);

                    if (!updatedSymbols.Contains(key))
                        updatedSymbols.Add(key);
                }

            }
            return;
        }

        public void AddOrderIdAndTime(OrderExt orderExt)
        {
            lock (positiontickLock)
            {
                string TradingSymbol = orderExt.order.Tradingsymbol + "_" + orderExt.order.Exchange + "|" + orderExt.order.Product;

                if (!TimeAndOrderId.ContainsKey(TradingSymbol))
                {
                    OpenPositionStore OpenPositionStore = new OpenPositionStore();
                    OpenPositionStore.OrderId = orderExt.order.OrderId;
                    OpenPositionStore.TimeStamp = (DateTime)orderExt.order.OrderTimestamp;
                    TimeAndOrderId.Add(TradingSymbol, OpenPositionStore);
                }
                else
                {
                    OpenPositionStore openPositionStore = TimeAndOrderId[TradingSymbol];
                    if ((DateTime)orderExt.order.OrderTimestamp > openPositionStore.TimeStamp)
                    {
                        openPositionStore.OrderId = orderExt.order.OrderId;
                        openPositionStore.TimeStamp = (DateTime)orderExt.order.OrderTimestamp;
                        TimeAndOrderId[TradingSymbol] = openPositionStore;
                    }
                }
            }

        }

        public void GetOrderIdAndTime(string TradingSymbol, out DateTime Time, out string OrderId)
        {
            lock (positiontickLock)
            {
                if (TimeAndOrderId.ContainsKey(TradingSymbol))
                {
                    OpenPositionStore OpenPositionStore = TimeAndOrderId[TradingSymbol];
                    OrderId = OpenPositionStore.OrderId;
                    Time = OpenPositionStore.TimeStamp;

                }
                else
                {
                    Time = new DateTime();
                    OrderId = "";
                }
            }
        }

        public Dictionary<string, PositionInfo> GetAllPositions()
        {
            Dictionary<string, PositionInfo> allPositions = new Dictionary<string, PositionInfo>();
            lock (positiontickLock)
            {
                foreach (KeyValuePair<string, PositionInfo> entry in positionStore)
                {
                    allPositions.Add(entry.Key, entry.Value);
                }                
            }            
            return allPositions;
        }

        public void RemoveFromMasterList()
        {
            lock (positiontickLock)
            {
                try
                {
                    foreach (var position in positionStore)
                    {
                        string key = position.Key;
                        if (!updatedSymbols.Contains(key))
                            positionStore.Remove(key);
                    }
                }
                catch (Exception e)
                {

                }
            }
        }
    }
}
