﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRDSAlgoOMS.Zerodha
{
    class LocalOrderStructure
    {
        //public LocalPositionStructure localPosition { get; set; }
        public string Product { get; set; }
        public string Exchange { get; set; }
        public string TradingSymbol { get; set; }
        public string TransactionType { get; set; }
        public int Quantity { get; set; }
        public string OrderID { get; set; }
        public DateTime PlacedOrderDateTime { get; set; }
        public string ClosedTransactionType { get; set; }
        public int ClosedQuantity { get; set; }
        public string ClosedOrderID { get; set; }
        public DateTime ClosedOrderDateTime { get; set; }
        public LocalOrderStructure () {}
    }

    class LocalOrderStructureInfo
    {
        Dictionary<string, LocalOrderStructure> m_OrderInfo = null;
        public LocalOrderStructureInfo ()
        {
            m_OrderInfo = new Dictionary<string, LocalOrderStructure>();
        }

        public void AddOrUpdateOrderInformation (string TradingSymbol = "",string Exchange= "" ,string TransactionType= "",string Product= "",int Quantity = 0,string OrderId = "",DateTime dateTime = new DateTime(), string ClosedOrderId = "",string ClosedTransactionType = "",int ClosedQuantity = 0,DateTime ClosedOrderDateTime = new DateTime())
        {
            try
            {
                string Key = TradingSymbol + "|" + Exchange + "_" + Product;
                if (!m_OrderInfo.ContainsKey(Key))
                {
                    LocalOrderStructure localPosition = new LocalOrderStructure();
                    localPosition.TradingSymbol = TradingSymbol;
                    localPosition.Exchange = Exchange;
                    localPosition.TransactionType = TransactionType;
                    localPosition.Quantity = Quantity;
                    localPosition.OrderID = OrderId;
                    localPosition.PlacedOrderDateTime = dateTime;
                    localPosition.Product = Product;
                    localPosition.ClosedOrderID = ClosedOrderId;
                    localPosition.ClosedTransactionType = ClosedTransactionType;
                    localPosition.ClosedQuantity = ClosedQuantity;
                    localPosition.ClosedOrderDateTime = ClosedOrderDateTime;
                    m_OrderInfo.Add(Key, localPosition);
                }
                else
                {
                    if (Quantity == 0)
                    {
                        m_OrderInfo[Key].ClosedOrderID = ClosedOrderId;
                        m_OrderInfo[Key].ClosedTransactionType = ClosedTransactionType;
                        m_OrderInfo[Key].ClosedOrderDateTime = ClosedOrderDateTime;
                        m_OrderInfo[Key].ClosedQuantity = ClosedQuantity;
                    }
                    else
                    {
                        m_OrderInfo[Key].TradingSymbol = TradingSymbol;
                        m_OrderInfo[Key].Exchange = Exchange;
                        m_OrderInfo[Key].TransactionType = TransactionType;
                        m_OrderInfo[Key].Quantity = Quantity;
                        m_OrderInfo[Key].OrderID = OrderId;
                        m_OrderInfo[Key].PlacedOrderDateTime = dateTime;
                        m_OrderInfo[Key].Product = Product;
                    }
                }

            }
            catch(Exception e)
            {

            }
        }

        public void RemoveOrderInformation (string TradingSymbol,string Exchange,string Product)
        {
            try
            {
                string Key = TradingSymbol + "|" + Exchange + "_" + Product;
                if(m_OrderInfo.ContainsKey(Key))
                {
                    m_OrderInfo.Remove(Key);
                }
            }
            catch(Exception e)
            {

            }
        }
    }
}
