﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRDSAlgoOMS
{
    public class OrderExt
    {
        public Order order { get; set; }
        public DateTime LastUpdateTime { get; set; }

        public OrderExt() { }

        public OrderExt(Order order)
        {
            this.order = order;
            LastUpdateTime = DateTime.Now;
        }
    }
}
