﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using System.Diagnostics;
using System.Threading;
using System.Collections;
using System.Globalization;
using System.IO;
namespace IRDSAlgoOMS
{
    public class MySQLConnectZerodha
    {
        public MySqlConnection mysqlconnDBManipulation;
        private string mServer;
        private string mFilePath;
        private string mUserid;
        private string mPassword;
        private string mDatabase;
        private string mFilepath;
        public bool mTblIsEmpty = false;//rohita ,flag for empty tbl
        private UInt64 TotalCount = 0;
        //    private csLogger ObjCsLogger=null;
        public static string TblString = "";
        public double lastRealTime_timestamp = 0.0;
        public static bool IsInvalidIBContract = false;
        private static readonly object mySqldatabaselock = new object();

        Boolean mConnectSuccess;
        Logger m_logger;

        public Boolean Connect(Logger logger, string strServer, string strUserID, string strPassword, string strDatabase)
        {
            mConnectSuccess = false;
            string strMySQLConnectionString = "";
            mServer = strServer;
            mUserid = strUserID;
            mPassword = strPassword;
            mDatabase = strDatabase;
            mFilePath = "";
            string FilePath = "";
            m_logger = logger;
            int sleep_time = 1000;
            //sanika::9-Dec-2020::Added flag into string for dump file
            strMySQLConnectionString = "server=" + mServer + ";" + "uid=" + mUserid + ";" + "pwd=" + mPassword + ";" + "database=" + mDatabase + ";AllowLoadLocalInfile=true;";
            try
            {
                mysqlconnDBManipulation = new MySqlConnection(strMySQLConnectionString);
                mysqlconnDBManipulation.Open();
                mConnectSuccess = true;
                // m_logger.LogMessage("Mysql connection successfull", MessageType.Informational);
                WriteUniquelogs("MysqlLogs", "Mysql connection successfull", MessageType.Informational);
            }
            catch (MySqlException ex)
            {
                mConnectSuccess = false;
                try {
                    //m_logger.LogMessage(" Error: Connect: " + ex.Message, MessageType.Informational); 
                    WriteUniquelogs("MysqlLogs", "Connect: " + ex.Message, MessageType.Informational);
                }
                catch { };
                switch (ex.Number)
                {
                    case 0: //m_logger.LogMessage("Cannot connect to PDB!", MessageType.Informational); 
                        WriteUniquelogs("MysqlLogs", "Cannot connect to PDB!", MessageType.Informational); break;
                    case 1045: //m_logger.LogMessage("Invalid PDB Username/Password!", MessageType.Informational); 
                        WriteUniquelogs("MysqlLogs", "Invalid PDB Username/Password!", MessageType.Informational); break;
                }
            }
            return mConnectSuccess;
        }

        public void ExecuteNonQueryCommand(string strQuery) // function inside MYSQL lock
        {
            if (mConnectSuccess) //check open connection
            {
                try
                {
                    MySqlCommand cmd = new MySqlCommand(strQuery, mysqlconnDBManipulation); //create command and assign the query and connection from the constructor
                    cmd.ExecuteNonQuery();//Execute command
                }
                catch (Exception ex)
                {                   
                    // m_logger.LogMessage("Error: ExecuteNonQueryCommand: " + ex, MessageType.Informational);
                    WriteUniquelogs("MysqlLogs", "Error: ExecuteNonQueryCommand : " + ex.Message, MessageType.Informational);
                }
            }
        }

        public Boolean CreateDatabase(string db)
        {
            if (mConnectSuccess) //check open connection
            {
                try
                {
                    ExecuteNonQueryCommand("CREATE SCHEMA " + db);
                    CloseConnection();
                    Connect(m_logger,mServer, mUserid, mPassword, db);
                    // m_logger.LogMessage("Created database: " + db + " and connected to it successfully!", MessageType.Informational);
                    WriteUniquelogs("MysqlLogs", "Created database: " + db + " and connected to it successfully!", MessageType.Informational);
                    return true;
                }
                catch (MySqlException ex)
                {                   
                    //m_logger.LogMessage("Error: CreateDatabase: " + ex, MessageType.Informational);
                    WriteUniquelogs("MysqlLogs", "Error: CreateDatabase : " + ex.Message, MessageType.Informational);
                }
            }
            return false;
        }

        public Boolean CreateTable(string tblName, string fieldsList, string key)
        {
            if (mConnectSuccess) //check open connection
            {
                try
                {
                    ExecuteNonQueryCommand("CREATE TABLE `" + tblName + "` (" + fieldsList + ", PRIMARY KEY(" + key + "));");
                    // m_logger.LogMessage("Created table " + tblName + " successfully!", MessageType.Informational);
                    WriteUniquelogs("MysqlLogs", "Created table " + tblName + " successfully!", MessageType.Informational);
                    return true;
                }
                catch (MySqlException ex)
                {                   
                    // m_logger.LogMessage("Error: CreateTable: " + ex, MessageType.Informational);
                    WriteUniquelogs("MysqlLogs", "Error: CreateTable : " + ex.Message, MessageType.Informational);
                }
            }
            return false;
        }

        public Boolean FetchData(string tablename) // function inside MYSQL lock
        {
            string strQuery = "SELECT * FROM " + tablename + ";";
            if (mConnectSuccess)//check open connection
            {
                try
                {
                    MySqlCommand cmd = new MySqlCommand(strQuery, mysqlconnDBManipulation);//create command and assign the query and connection from the constructor
                    MySqlDataReader dataReader = cmd.ExecuteReader(); //Create a data reader and Execute the command
                    
                    string data = "";
                    while (dataReader.Read())
                    {
                        for (int i = 0; i < dataReader.FieldCount; i++)
                        {
                            
                            data = data + " " + dataReader[i].ToString();
                        }
                        data = data + "\n";
                    }
                    // m_logger.LogMessage("Fetched data: \n" + data, MessageType.Informational);
                    WriteUniquelogs("MysqlLogs", "Fetched data: \n" + data, MessageType.Informational);
                    dataReader.Close(); //close Data Reader
                    return true;
                }
                catch (MySqlException ex)
                {
                    
                    //m_logger.LogMessage("Error: FetchData: " + ex.Message, MessageType.Informational);
                    WriteUniquelogs("MysqlLogs", "Error: FetchData : " + ex.Message, MessageType.Informational);
                }
            }
            return false;
        }

        public Boolean UpdateRow(string tablename, string LastTradeTime, string Symbol, decimal Bid, decimal Ask, decimal LTP, decimal ChangeInValues, decimal ChangeInPercentage, decimal Open, decimal High, decimal Low, decimal Close, decimal Average, uint Volume, uint OpenInterest, uint Buyers, uint Sellers, string Expiry, int BidChange, int AskChange, int LTPChange, decimal yOpen, decimal yHigh, decimal yLow, decimal yClose) // function inside MYSQL lock
        {
            //string strQuery = "UPDATE " + tablename + " SET " + "LastTradeTime = STR_TO_DATE('" + LastTradeTime + "', '%d-%m-%Y %H:%i:%s'), Symbol = '" + Symbol+ "', Bid = "+Bid+ ", Ask = " + Ask + ", LTP = " + LTP + ", ChangeInValues = " + ChangeInValues + ", ChangeInPercentage = " + ChangeInPercentage + ", Open = " + Open + ", High = " + High + ", Low = " + Low + ", Close = " + Close + ", Average = " + Average + ", Volume = " + Volume + ", OpenInterest = " + OpenInterest + ", Buyers = " + Buyers + ", Sellers = " + Sellers + ", Expiry = '" + Expiry + "'  WHERE LastTradeTime = STR_TO_DATE('" + LastTradeTime + "', '%d-%m-%Y %H:%i:%s') LIMIT 1;";
            //string strQuery = "UPDATE " + tablename + " SET " + "LastTradeTime = STR_TO_DATE('" + LastTradeTime + "', '%d-%m-%Y %H:%i:%s'), Symbol = '" + Symbol + "', Bid = " + Bid + ", Ask = " + Ask + ", LTP = " + LTP + ", ChangeInValues = " + ChangeInValues + ", ChangeInPercentage = " + ChangeInPercentage + ", Open = " + Open + ", High = " + High + ", Low = " + Low + ", Close = " + Close + ", Average = " + Average + ", Volume = " + Volume + ", OpenInterest = " + OpenInterest + ", Buyers = " + Buyers + ", Sellers = " + Sellers + ", Expiry = '" + Expiry + "' order by LastTradeTime desc LIMIT 1;";
            string strQuery = "UPDATE " + tablename + " SET " + "LastTradeTime = STR_TO_DATE('" + LastTradeTime + "', '%d-%m-%Y %H:%i:%s'), Symbol = '" + Symbol + "', Bid = " + Bid + ", Ask = " + Ask + ", LTP = " + LTP + ", ChangeInValues = " + ChangeInValues + ", ChangeInPercentage = " + ChangeInPercentage + ", Open = " + Open + ", High = " + High + ", Low = " + Low + ", Close = " + Close + ", Average = " + Average + ", Volume = " + Volume + ", OpenInterest = " + OpenInterest + ", Buyers = " + Buyers + ", Sellers = " + Sellers + ", Expiry = '" + Expiry + "', BidChange = " + BidChange + ", AskChange = " + AskChange + ", LTPChange = " + LTPChange;
            if (yOpen > 0 && yHigh > 0 && yLow > 0 && yClose > 0)
            {
                strQuery = strQuery + ", yOpen = " + yOpen + ", yHigh = " + yHigh + ", yLow = " + yLow + ", yClose = " + yClose;
            }
            strQuery = strQuery + " order by LastTradeTime desc LIMIT 1;";
            if (mConnectSuccess)//check open connection
            {
                try
                {
                    //if (mysqlconnDBManipulation.State != System.Data.ConnectionState.Open) mysqlconnDBManipulation.Open();
                    MySqlCommand cmd = new MySqlCommand(strQuery, mysqlconnDBManipulation);
                    int iReturn = cmd.ExecuteNonQuery();
                    if (iReturn == 1)
                    {
                        Console.WriteLine("Updated Table " + tablename + " Successfully :: " + LastTradeTime);
                        //m_logger.LogMessage("Updated Table " + tablename + " Successfully :: " + LastTradeTime, MessageType.Informational);
                        return true;
                    }
                    else
                    {
                        Console.WriteLine("Updated Table " + tablename + " Successfully :: " + LastTradeTime);
                        //m_logger.LogMessage("Updated Table " + tablename + " Successfully :: " + LastTradeTime, MessageType.Informational);
                    }
                }
                catch (MySqlException ex)
                {
                    Console.WriteLine("UPDATE error : " + ex.Message);
                    Console.WriteLine("Error for " + tablename + " :: " + LastTradeTime);
                    // m_logger.LogMessage("Error: UpdateRow: " + ex.Message, MessageType.Informational);
                    WriteUniquelogs("MysqlLogs", "Error: UpdateRow : " + ex.Message, MessageType.Informational);
                }
            }
            return false;
        }

        public Boolean IsTableExists(string tablename, string dbname) // function inside MYSQL lock
        {
            
            string strQuery = @"SELECT count(*) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='"+tablename+"' and table_schema = '"+dbname+"';";
            if (mConnectSuccess)//check open connection
            {
                lock (mySqldatabaselock)
                {
                    try
                    {
                        MySqlCommand cmd = new MySqlCommand(strQuery, mysqlconnDBManipulation);//create command and assign the query and connection from the constructor
                        int x = Convert.ToInt32(cmd.ExecuteScalar());
                        if (x == 1)
                            return true;
                        else
                            return false;
                    }
                    catch (MySqlException ex)
                    {
                        Console.WriteLine("IsTableExists error : " + ex.Message);
                        // m_logger.LogMessage("Error: IsTableExists: " + ex.Message, MessageType.Informational);
                        WriteUniquelogs("MysqlLogs", "Error: IsTableExists : " + ex.Message, MessageType.Informational);
                    }
                }
            }
            return false;
        }

        public void CloseConnection() // function inside MYSQL lock
        {
            if (mConnectSuccess)
            {

                mysqlconnDBManipulation.Close();
            }
        }

        public bool InsertDataMultipleValues(string TblName, string strValues)
        {
            Boolean bolInsertBasicFileData = false;           
            string fields = "";

            //IRDS::03-Jul-2020::Jyoti::changed for Ajay Sir Changes
            //fields = "(LastTradeTime, Symbol, Bid, Ask, LTP, ChangeInValues, ChangeInPercentage, Open, High, Low, Close, Average, Volume, OpenInterest, Buyers, Sellers)";
            //fields = "(LastTradeTime, Bid, Ask, LTP, ChangeInValues, ChangeInPercentage, Open, High, Low, Close, Average, Volume, OpenInterest, Buyers, Sellers, PrevClose)";
            fields = "(LastTradeTime, Symbol, Bid, Ask, LTP, ChangeInValues, ChangeInPercentage, Open, High, Low, Close, Average, Volume, OpenInterest, Buyers, Sellers,Expiry,BidChange,AskChange,LTPChange,yOpen,yHigh,yLow,yClose)";

            if (mConnectSuccess)
                {
                    lock (mySqldatabaselock)
                    {
                        try
                        {
                            if (mysqlconnDBManipulation.State != System.Data.ConnectionState.Open) mysqlconnDBManipulation.Open();
                            string qurInsertData = "INSERT INTO " + TblName + " " + fields + " VALUES " + strValues;
                            MySqlCommand cmd = new MySqlCommand(qurInsertData, mysqlconnDBManipulation);
                                    int iReturn = Convert.ToInt32(cmd.ExecuteScalar());
                            //m_logger.LogMessage("Successfully inserted "+strValues, MessageType.Informational);
                            bolInsertBasicFileData = true;
                            cmd = null;
                        }
                        catch (MySqlException ex)
                        {

                            bolInsertBasicFileData = false;
                        // m_logger.LogMessage("ERROR: InsertDataMultipleValues: Failed insert " + TblName + " : " + ex, MessageType.Informational);
                        WriteUniquelogs("MysqlLogs", "ERROR: InsertDataMultipleValues: Failed insert " + TblName + " : "+ ex.Message, MessageType.Informational);
                            //Console.WriteLine("ERROR: InsertDataMultipleValues: Failed insert " + TblName + " : " + ex.Message);
                            //Console.WriteLine("Can't continue!");
                        }
                    }
                }
                else { //m_logger.LogMessage("ERROR: InsertDataMultipleValues: No connection to the PDB. Can't continue!", MessageType.Informational); 
                WriteUniquelogs("MysqlLogs", "ERROR: InsertDataMultipleValues: No connection to the PDB. Can't continue!", MessageType.Informational);
            }
            
            //objPDB.CloseConnection();
            return bolInsertBasicFileData;
        }

        public bool DeleteRows(string tbl, string col, string val)
        {
            if (mConnectSuccess)
            {
                try
                {
                    string qurInsertData = "DELETE FROM " + tbl + " WHERE " + col + " = '" + val + "'";
                    if (mysqlconnDBManipulation.State != System.Data.ConnectionState.Open) mysqlconnDBManipulation.Open();
                    MySqlCommand cmd = new MySqlCommand(qurInsertData, mysqlconnDBManipulation);
                    int iReturn = cmd.ExecuteNonQuery();
                    if (iReturn == 1)
                    {
                        Console.WriteLine("Deleted Successfully :: " + col + ": " + val);
                      //  m_logger.LogMessage("Deleted Successfully :: " + col + ": " + val, MessageType.Informational);
                        WriteUniquelogs("MysqlLogs", "Deleted Successfully :: " + col + ": " + val, MessageType.Informational);
                        return true;
                    }
                    else
                    {
                        Console.WriteLine("Delete Failed :: " + col + ": " + val);
                       // m_logger.LogMessage("Delete Failed :: " + col + ": " + val, MessageType.Informational);
                        WriteUniquelogs("MysqlLogs", "Delete Failed :: " + col + ": " + val, MessageType.Informational);
                    }
                }
                catch (MySqlException ex)
                {
                    Console.WriteLine("Failed to delete " + ex.Message);
                    // m_logger.LogMessage("Error: Delete: " + ex.Message, MessageType.Informational);
                    WriteUniquelogs("MysqlLogs", "Error: Delete : " + ex.Message, MessageType.Informational);
                }
            }
            return false;
        }

        public bool CleanDatabase(string oldTbl, string dbname)
        {
            string newTbl = oldTbl + "_old";
            if (mConnectSuccess)
            {
                if (mysqlconnDBManipulation.State != System.Data.ConnectionState.Open) mysqlconnDBManipulation.Open();
                if (IsTableExists(newTbl, dbname)) {
                    string qurdropTable = "DROP TABLE " + newTbl + ";";
                    DateTime d = DateTime.Now;
                    //string qurdropTable = "ALTER TABLE " + newTbl + " RENAME TO " + newTbl + "_"+d.ToString().Replace(" ","_")+";";
                try
                {
                    MySqlCommand cmd1 = new MySqlCommand(qurdropTable, mysqlconnDBManipulation);
                    cmd1.ExecuteNonQuery();
                        //  m_logger.LogMessage("Dropped table: " + newTbl, MessageType.Informational);
                        WriteUniquelogs("MysqlLogs", "Dropped table: " + newTbl, MessageType.Informational);
                }
                catch (MySqlException ex)
                {
                        Console.WriteLine("Failed to drop " + ex.Message);
                        // m_logger.LogMessage("Error: Drop: " + ex.Message, MessageType.Informational);
                        WriteUniquelogs("MysqlLogs", "Error: Drop : " + ex.Message, MessageType.Informational);
                    }
                }
                string qurRenameTable = "ALTER TABLE " + oldTbl + " RENAME TO " + newTbl + ";";
                try
                {
                    MySqlCommand cmd = new MySqlCommand(qurRenameTable, mysqlconnDBManipulation);
                    cmd.ExecuteNonQuery();
                    Console.WriteLine("Renamed Successfully :: " + oldTbl + " to " + newTbl);
                 //   m_logger.LogMessage("Renamed Successfully :: " + oldTbl + " to " + newTbl, MessageType.Informational);
                    WriteUniquelogs("MysqlLogs", "Renamed Successfully :: " + oldTbl + " to " + newTbl, MessageType.Informational);
                    return true;
                }
                catch (MySqlException ex)
                {
                    Console.WriteLine("Failed to rename " + ex.Message);
                  //  m_logger.LogMessage("Error: Rename: " + ex.Message, MessageType.Informational);
                    WriteUniquelogs("MysqlLogs", "Error: Rename : " + ex.Message, MessageType.Informational);
                }
            }
            return false;
        }

        public bool CreateZerodhaTable(string tblName, string dbName)
        {
            if (IsTableExists(tblName, dbName))
            {
                CleanDatabase(tblName, dbName);
            }
            string fields = "id INT NOT NULL AUTO_INCREMENT, LastTradeTime DATETIME NOT NULL, Symbol VARCHAR(50) NOT NULL, Bid DOUBLE NOT NULL,Ask DOUBLE NOT NULL,LTP DOUBLE NOT NULL,ChangeInValues DOUBLE NOT NULL,ChangeInPercentage DOUBLE NOT NULL,Open DOUBLE NOT NULL,High DOUBLE NOT NULL,Low DOUBLE NOT NULL,Close DOUBLE NOT NULL,Average DOUBLE NOT NULL,Volume BIGINT NOT NULL,OpenInterest BIGINT NOT NULL,Buyers BIGINT NOT NULL,Sellers BIGINT NOT NULL";
            //string fields = "id INT NOT NULL AUTO_INCREMENT,LastTradeTime DATETIME NOT NULL,Bid DOUBLE NOT NULL,Ask DOUBLE NOT NULL,LTP DOUBLE NOT NULL,ChangeInValues DOUBLE NOT NULL,ChangeInPercentage DOUBLE NOT NULL,Open DOUBLE NOT NULL,High DOUBLE NOT NULL,Low DOUBLE NOT NULL,Close DOUBLE NOT NULL,Average DOUBLE NOT NULL,Volume BIGINT NOT NULL,OpenInterest BIGINT NOT NULL,Buyers BIGINT NOT NULL,Sellers BIGINT NOT NULL,PrevClose DOUBLE NOT NULL";
            CreateTable(tblName, fields, "id");
            return true;
        }

        public double GetPrevClose(string tblName, string dbname)
        {
            double prevClose = 0;
            if (IsTableExists(tblName, dbname))
            {
                string query = "SELECT * FROM " + tblName + " order by LastTradeTime desc LIMIT 1;";
                try
                {
                    MySqlCommand cmd = new MySqlCommand(query, mysqlconnDBManipulation);//create command and assign the query and connection from the constructor
                    MySqlDataReader dataReader = cmd.ExecuteReader(); //Create a data reader and Execute the command
                                                                      //Console.WriteLine(dataReader.FieldCount);
                    if (dataReader.Read())
                    {
                        //m_logger.LogMessage("Previous Close for " + tblName+ ": " + dataReader["id"] + " " + dataReader["LastTradeTime"] + " " + dataReader["Bid"] + " " + dataReader["Ask"] +
                        //" " + dataReader["LTP"] + " " + dataReader["ChangeInValues"] + " " + dataReader["ChangeInPercentage"] + " " + dataReader["Open"] + " " +
                        //dataReader["High"] + " " + dataReader["Low"] + " " + dataReader["Close"] + " " + dataReader["Average"] + " " + dataReader["Volume"] + " " +
                        //dataReader["OpenInterest"] + " " + dataReader["Buyers"] + " " + dataReader["Sellers"], MessageType.Informational);
                        

                        WriteUniquelogs("MysqlLogs", "Previous Close for " + tblName + ": " + dataReader["id"] + " " + dataReader["LastTradeTime"] + " " + dataReader["Bid"] + " " + dataReader["Ask"] +
                        " " + dataReader["LTP"] + " " + dataReader["ChangeInValues"] + " " + dataReader["ChangeInPercentage"] + " " + dataReader["Open"] + " " +
                        dataReader["High"] + " " + dataReader["Low"] + " " + dataReader["Close"] + " " + dataReader["Average"] + " " + dataReader["Volume"] + " " +
                        dataReader["OpenInterest"] + " " + dataReader["Buyers"] + " " + dataReader["Sellers"], MessageType.Informational);
                        prevClose = (double)dataReader["Close"];
                    }
                    dataReader.Close(); //close Data Reader
                }
                catch (MySqlException ex)
                {
                    Console.WriteLine("FetchData error : " + ex.Message);
                   // m_logger.LogMessage("Error: FetchData: " + ex.Message, MessageType.Informational);
                    WriteUniquelogs("MysqlLogs", "Error: GetPrevClose : " + ex.Message, MessageType.Informational);
                }
            }
            return prevClose;
        }

        public bool InsertHistDataMultipleValues(string TblName, string strValues)
        {
            Boolean bolInsertBasicFileData = false;
            string fields = "";
            //(DateTime, Open, High, Low, Close, Volume)
            fields = "(Symbol, LastTradeTime, Open, High, Low, Close, Volume)";
            //fields = "(LastTradeTime, Bid, Ask, LTP, ChangeInValues, ChangeInPercentage, Open, High, Low, Close, Average, Volume, OpenInterest, Buyers, Sellers, PrevClose)";

            try
            {
                if (mConnectSuccess)
                {
                    string qurInsertData = "INSERT INTO `" + TblName + "` " + fields + " VALUES " + strValues;
                    MySqlCommand cmd = new MySqlCommand(qurInsertData, mysqlconnDBManipulation);
                    int iReturn = cmd.ExecuteNonQuery();
                    bolInsertBasicFileData = true;
                    cmd = null;
                }
                else
                { //m_logger.LogMessage("ERROR: InsertDataMultipleValues: No connection to the PDB. Can't continue!", MessageType.Informational); //while (true) { }; //sanika::30-sep-2020::commented loop 
                    WriteUniquelogs("MysqlLogs", "ERROR: InsertDataMultipleValues: No connection to the PDB. Can't continue!", MessageType.Informational);
                }
            }
            catch (MySqlException ex)
            {
                bolInsertBasicFileData = false;
               // m_logger.LogMessage("ERROR: InsertDataMultipleValues: Failed insert " + TblName + " : " + ex.Message, MessageType.Informational);
                WriteUniquelogs("MysqlLogs", "ERROR: InsertHistDataMultipleValues: Failed insert " + TblName + " : " + ex.Message, MessageType.Informational);
                Console.WriteLine("ERROR: InsertDataMultipleValues: Failed insert " + TblName + " : " + ex.Message);
                Console.WriteLine("Can't continue!"); //while (true) { }; //sanika::30-sep-2020::commented loop 
            }
            //objPDB.CloseConnection();
            return bolInsertBasicFileData;
        }
        MySqlTransaction myTrans;
        public void beginTran()
        {
            // Start a local transaction
            if (mysqlconnDBManipulation.State != System.Data.ConnectionState.Open) mysqlconnDBManipulation.Open();
            myTrans = mysqlconnDBManipulation.BeginTransaction();

        }

        public void CommitTran()
        {
            if(myTrans != null)
            // Start a local transaction
                myTrans.Commit();

        }

        public bool InsertorUpdate(string TblName, string datetime, string open, string high, string low, string close, string volume)
        {
            
            Boolean bolInsertBasicFileData = false;
            if (mConnectSuccess)
            {
                lock (mySqldatabaselock)
                {
                    try
                    {
                        string qurSelectData = "SELECT count(*) FROM `" + TblName + "` WHERE LastTradeTime = '" + datetime + "';";
                        MySqlCommand cmd = new MySqlCommand(qurSelectData, mysqlconnDBManipulation);
                        int iReturn = Convert.ToInt32(cmd.ExecuteScalar());
                       
                        if (iReturn > 0)
                        {
                            string qUpdate = "UPDATE `" + TblName + "` SET Open = " + open + ", High = " + high + ", Low = " + low + ", Close = " + close + ", Volume = " + volume + " WHERE LastTradeTime = '" + datetime + "' LIMIT 1;";
                          
                            MySqlCommand cmd1 = new MySqlCommand(qUpdate, mysqlconnDBManipulation, myTrans);
                            //cmd1.Transaction = myTrans;
                            iReturn = cmd1.ExecuteNonQuery();
                           
                            if (iReturn == 1)
                            {
                                
                                //m_logger.LogMessage("Updated Successfully :: " + open + " " + high + " " + low + " " + close, MessageType.Informational);
                                return true;
                            }
                            else
                            {
                               
                               // m_logger.LogMessage("Update Failed :: " + open + " " + high + " " + low + " " + close, MessageType.Informational);
                                WriteUniquelogs("MysqlLogs", "Update Failed :: " + open + " " + high + " " + low + " " + close, MessageType.Informational);
                                return false;
                            }
                        }
                        else
                        {
                            string qurInsertData = "INSERT INTO `" + TblName + "` (Symbol, LastTradeTime, Open, High, Low, Close, Volume) VALUES ('" + TblName + "',STR_TO_DATE('" + datetime + "', '%Y-%m-%d %H:%i:%s')," + open + "," + high + "," + low + "," + close + "," + volume + ");";
                            cmd = new MySqlCommand(qurInsertData, mysqlconnDBManipulation);
                            iReturn = cmd.ExecuteNonQuery();
                            cmd = null;
                            return true;
                        }
                    }
                    catch (MySqlException ex)
                    {
                        bolInsertBasicFileData = false;
                      //  m_logger.LogMessage("ERROR: InsertorUpdate: Failed insert " + TblName + " : " + ex.Message, MessageType.Informational);
                        WriteUniquelogs("MysqlLogs", "ERROR: InsertorUpdate: Failed insert " + TblName + " : " + ex.Message, MessageType.Informational);
                        //Console.WriteLine("ERROR: InsertorUpdate: Failed insert " + TblName + " : " + ex.Message);
                        //Console.WriteLine("Can't continue!");
                    }
                }
            }
            else
            {
               // m_logger.LogMessage("ERROR: InsertorUpdate: No connection to the PDB. Can't continue!", MessageType.Informational);
                WriteUniquelogs("MysqlLogs", "ERROR: InsertorUpdate: No connection to the PDB. Can't continue!", MessageType.Informational);
                return false;
            }            
            
            //objPDB.CloseConnection();
            return bolInsertBasicFileData;
        }

        //IRDS::03-Jul-2020::Jyoti::added InsertorUpdateSymbol, DeleteOldRows, DeleteAll, GetRowsCount, GetYestValues for Ajay Sir Changes
        //IRDS::13-Jul-2020::Jyoti::added for SymbolDetails table
        public bool InsertorUpdateSymbol(string TblName, string symbolName, string tableName, string exchange, int sortCounter)
        {
            Boolean bolInsertBasicFileData = false;
            try
            {
                if (mConnectSuccess)
                {
                    /*string qurSelectData = "SELECT count(*) FROM " + TblName + " WHERE Symbol_name = '" + symbolName + "';";
                    MySqlCommand cmd = new MySqlCommand(qurSelectData, mysqlconnDBManipulation);
                    int iReturn = Convert.ToInt32(cmd.ExecuteScalar());
                    if (iReturn > 0)
                    {
                        string qUpdate = "";
                        if (tableName == "")
                        {
                            qUpdate = "UPDATE " + TblName + " SET Exchange = '" + exchange + "' WHERE Symbol_name = '" + symbolName + "';";
                        }
                        else if (exchange == "")
                        {
                            qUpdate = "UPDATE " + TblName + " SET Table_name = '" + tableName + "' WHERE Symbol_name = '" + symbolName + "';";
                        }
                        else
                        {
                            qUpdate = "UPDATE " + TblName + " SET Table_name = '" + tableName + "', Exchange = '" + exchange + "' WHERE Symbol_name = '" + symbolName + "';";
                        }
                        cmd = new MySqlCommand(qUpdate, mysqlconnDBManipulation);
                        iReturn = cmd.ExecuteNonQuery();
                        Console.WriteLine("InsertorUpdateSymbol:: Updated Successfully");
                        //m_logger.LogMessage("Updated Successfully :: " + open + " " + high + " " + low + " " + close, MessageType.Informational);
                        return true;
                    }
                    else
                    {*/
                    string qurInsertData = "INSERT INTO " + TblName + " (Symbol_name, Table_name, Exchange,SortIndex) VALUES ('" + symbolName + "','" + tableName + "', '" + exchange + "', " + sortCounter + ");";
                    MySqlCommand cmd = new MySqlCommand(qurInsertData, mysqlconnDBManipulation);
                    int iReturn = cmd.ExecuteNonQuery();
                    cmd = null;
                    return true;
                    //}
                }
                else
                {
                  //  m_logger.LogMessage("ERROR: InsertorUpdateSymbol: No connection to the PDB. Can't continue!", MessageType.Informational);
                    WriteUniquelogs("MysqlLogs", "ERROR: InsertorUpdateSymbol: No connection to the PDB. Can't continue!", MessageType.Informational);
                    return false;
                }
            }
            catch (MySqlException ex)
            {
                bolInsertBasicFileData = false;
               // m_logger.LogMessage("ERROR: InsertorUpdateSymbol: Failed insert " + TblName + " : " + ex.Message, MessageType.Informational);
                WriteUniquelogs("MysqlLogs", "ERROR: InsertorUpdateSymbol: Failed insert " + TblName + " : " + ex.Message, MessageType.Informational);
                Console.WriteLine("ERROR: InsertorUpdateSymbol: Failed insert " + TblName + " : " + ex.Message);
                Console.WriteLine("Can't continue!");
            }
            //objPDB.CloseConnection();
            return bolInsertBasicFileData;
        }

        public bool DeleteOldRows(string tbl)
        {
            if (mConnectSuccess)
            {
                try
                {
                    string qurInsertData = "DELETE FROM " + tbl + " WHERE id NOT IN ( SELECT id FROM(SELECT id FROM " + tbl + " ORDER BY id DESC LIMIT 1) x);";
                    if (mysqlconnDBManipulation.State != System.Data.ConnectionState.Open) mysqlconnDBManipulation.Open();
                    MySqlCommand cmd = new MySqlCommand(qurInsertData, mysqlconnDBManipulation);
                    int iReturn = cmd.ExecuteNonQuery();
                   // m_logger.LogMessage("Deleted Successfully " + iReturn + " records From Table :: " + tbl, MessageType.Informational);
                    WriteUniquelogs("MysqlLogs", "Deleted Successfully " + iReturn + " records From Table :: " + tbl, MessageType.Informational);
                    Trace.WriteLine("Deleted Successfully " + iReturn + " records From Table :: " + tbl);
                    return true;
                }
                catch (MySqlException ex)
                {
                    Console.WriteLine("Failed to delete " + ex.Message);
                  //  m_logger.LogMessage("Error: Delete: " + ex.Message, MessageType.Informational);
                    WriteUniquelogs("MysqlLogs", "Error: DeleteOldRows : " + ex.Message, MessageType.Informational);
                }
            }
            return false;
        }

        public bool DeleteAll(string tbl)
        {
            if (mConnectSuccess)
            {
                try
                {
                    string qurInsertData = "DELETE FROM `" + tbl + "`;";
                    if (mysqlconnDBManipulation.State != System.Data.ConnectionState.Open) mysqlconnDBManipulation.Open();
                    MySqlCommand cmd = new MySqlCommand(qurInsertData, mysqlconnDBManipulation);
                    int iReturn = cmd.ExecuteNonQuery();
                   // m_logger.LogMessage("Deleted Successfully " + iReturn + " records From Table :: " + tbl, MessageType.Informational);
                    WriteUniquelogs("MysqlLogs", "Deleted Successfully " + iReturn + " records From Table :: " + tbl, MessageType.Informational);
                    Trace.WriteLine("Deleted Successfully " + iReturn + " records From Table :: " + tbl);
                    return true;
                }
                catch (MySqlException ex)
                {
                    Console.WriteLine("Failed to delete " + ex.Message);
                   // m_logger.LogMessage("Error: Delete: " + ex.Message, MessageType.Informational);
                    WriteUniquelogs("MysqlLogs", "Error: DeleteAll : " + ex.Message, MessageType.Informational);
                }
            }
            return false;
        }
        public int GetRowsCount(string tbl)
        {
            int iReturn = 0;
            if (mConnectSuccess)
            {
                try
                {
                    //string qurInsertData = "SELECT COUNT(*) FROM " + tbl + " where LastTradeTime > '" + DateTime.Now.ToString("yyyy-MM-dd") + " 00:00:00" + "';";
                    string qurInsertData = "SELECT COUNT(*) FROM " + tbl + ";";
                    if (mysqlconnDBManipulation.State != System.Data.ConnectionState.Open) mysqlconnDBManipulation.Open();
                    MySqlCommand cmd = new MySqlCommand(qurInsertData, mysqlconnDBManipulation);
                    //iReturn = cmd.ExecuteNonQuery();
                    MySqlDataReader dataReader = cmd.ExecuteReader(); //Create a data reader and Execute the command
                    if (dataReader.Read())
                    {
                        iReturn = Convert.ToInt32(dataReader["COUNT(*)"]);
                        dataReader.Close();//close Data Reader
                    }
                    //m_logger.LogMessage("Found " + iReturn + " records From Table :: " + tbl, MessageType.Informational);
                    Trace.WriteLine("Found " + iReturn + " records From Table :: " + tbl);
                    return iReturn;
                }
                catch (MySqlException ex)
                {
                    Console.WriteLine("Failed to get count " + ex.Message);
                   // m_logger.LogMessage("Error: get count: " + ex.Message, MessageType.Informational);
                    WriteUniquelogs("MysqlLogs", "Error: GetRowsCount : " + ex.Message, MessageType.Informational);
                }
            }
            return iReturn;
        }

        public string GetYestValues(string tbl)
        {
            int iReturn = 0;
            string data = "0,0,0,0";
            if (mConnectSuccess)
            {
                try
                {
                    //string qurInsertData = "SELECT COUNT(*) FROM " + tbl + " where LastTradeTime > '" + DateTime.Now.ToString("yyyy-MM-dd") + " 00:00:00" + "';";
                    string qurInsertData = "SELECT * FROM " + tbl + " where LastTradeTime < '" + DateTime.Now.ToString("yyyy-MM-dd") + " 00:00:00" + "';";
                    //and LastTradeTime > '" + DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd") + " 00:00:00" + "'
                    if (mysqlconnDBManipulation.State != System.Data.ConnectionState.Open) mysqlconnDBManipulation.Open();
                    MySqlCommand cmd = new MySqlCommand(qurInsertData, mysqlconnDBManipulation);
                    //iReturn = cmd.ExecuteNonQuery();
                    MySqlDataReader dataReader = cmd.ExecuteReader(); //Create a data reader and Execute the command
                    if (dataReader.Read())
                    {
                        data = dataReader["Open"] + "," + dataReader["High"] + "," + dataReader["Low"] + "," + dataReader["Close"];
                      //  m_logger.LogMessage("Updating Table " + tbl + " " + dataReader["LastTradeTime"] + " yOpen = " + dataReader["Open"] + ", yHigh = " + dataReader["High"] + ", yLow = " + dataReader["Low"] + ", yClose = " + dataReader["Close"], MessageType.Informational);
                        WriteUniquelogs("MysqlLogs", "Updating Table " + tbl + " " + dataReader["LastTradeTime"] + " yOpen = " + dataReader["Open"] + ", yHigh = " + dataReader["High"] + ", yLow = " + dataReader["Low"] + ", yClose = " + dataReader["Close"], MessageType.Informational);
                    }
                    dataReader.Close();//close Data Reader
                    //m_logger.LogMessage("Found " + data + " records From Table :: " + tbl, MessageType.Informational);
                    Trace.WriteLine("Found " + data + " records From Table :: " + tbl);
                    return data;
                }
                catch (MySqlException ex)
                {
                    //dataReader.Close();//close Data Reader
                    Console.WriteLine("Failed to get count " + ex.Message);
                  //  m_logger.LogMessage("Error: get count: " + ex.Message, MessageType.Informational);
                    WriteUniquelogs("MysqlLogs", "Error: GetYestValues : " + ex.Message, MessageType.Informational);
                }
            }
            return data;
        }

        public bool InsertProfitDataMultipleValues(string TblName, string strValues)
        {
            Boolean bolInsertBasicFileData = false;
            string fields = "";
            //(DateTime, Open, High, Low, Close, Volume)
            fields = "(Symbol, Datetime, Profit, Date, Time)";
            //fields = "(LastTradeTime, Bid, Ask, LTP, ChangeInValues, ChangeInPercentage, Open, High, Low, Close, Average, Volume, OpenInterest, Buyers, Sellers, PrevClose)";

            try
            {
                if (mConnectSuccess)
                {
                    string qurInsertData = "INSERT IGNORE INTO `" + TblName + "` " + fields + " VALUES " + strValues + "";
                    MySqlCommand cmd = new MySqlCommand(qurInsertData, mysqlconnDBManipulation);
                    int iReturn = cmd.ExecuteNonQuery();
                    bolInsertBasicFileData = true;
                    cmd = null;
                }
                else
                {
                   // m_logger.LogMessage("ERROR: InsertDataMultipleValues: No connection to the PDB. Can't continue!", MessageType.Informational); //while (true) { }; //sanika::30-sep-2020::commented loop 
                    WriteUniquelogs("MysqlLogs", "Error: InsertProfitDataMultipleValues :: No connection to the PDB. Can't continue! ", MessageType.Informational);
                }
            }
            catch (MySqlException ex)
            {
                bolInsertBasicFileData = false;
               // m_logger.LogMessage("ERROR: InsertDataMultipleValues: Failed insert " + TblName + " : " + ex.Message, MessageType.Informational);
                WriteUniquelogs("MysqlLogs", "Error: InsertProfitDataMultipleValues :  Failed insert " + TblName + " : " + ex.Message, MessageType.Informational);
                Console.WriteLine("ERROR: InsertDataMultipleValues: Failed insert " + TblName + " : " + ex.Message);
                Console.WriteLine("Can't continue!"); //while (true) { };//sanika::30-sep-2020::commented loop 
            }
            //objPDB.CloseConnection();
            return bolInsertBasicFileData;
        }

        public string FetchProfitData(string date, string tablename, string symbol) // function inside MYSQL lock
        {
            string data = "0";
            string strQuery = "SELECT SUM(Profit) FROM `"+tablename+"` where Symbol = '" + symbol + "' AND Datetime <= '"+date+"' group by Symbol;";
            if (mConnectSuccess)//check open connection
            {
                try
                {
                    MySqlCommand cmd = new MySqlCommand(strQuery, mysqlconnDBManipulation);//create command and assign the query and connection from the constructor
                    MySqlDataReader dataReader = cmd.ExecuteReader(); //Create a data reader and Execute the command
                                                                      //Console.WriteLine(dataReader.FieldCount);

                    if (dataReader.Read())
                    {
                        data = dataReader[0].ToString();
                    }
                    //m_logger.LogMessage("Fetched data: \n" + data, MessageType.Informational);
                    dataReader.Close(); //close Data Reader
                    return data;
                }
                catch (MySqlException ex)
                {
                    Console.WriteLine("FetchData error : " + ex.Message);
                  //  m_logger.LogMessage("Error: FetchData: " + ex.Message, MessageType.Informational);
                    WriteUniquelogs("MysqlLogs", "Error: FetchProfitData : " + ex.Message, MessageType.Informational);
                }
            }
            return data;
        }

        public string FetchTotalProfitData(string date, string tablename) // function inside MYSQL lock
        {
            string data = "0";
            string strQuery = "SELECT SUM(Profit) FROM `" + tablename + "` where Datetime = '" + date + "' group by Datetime;";
            if (mConnectSuccess)//check open connection
            {
                try
                {
                    MySqlCommand cmd = new MySqlCommand(strQuery, mysqlconnDBManipulation);//create command and assign the query and connection from the constructor
                    MySqlDataReader dataReader = cmd.ExecuteReader(); //Create a data reader and Execute the command
                                                                      //Console.WriteLine(dataReader.FieldCount);

                    if (dataReader.Read())
                    {
                        data = dataReader[0].ToString();
                    }
                    //m_logger.LogMessage("Fetched data: \n" + data, MessageType.Informational);
                    dataReader.Close(); //close Data Reader
                    return data;
                }
                catch (MySqlException ex)
                {
                    Console.WriteLine("FetchData error : " + ex.Message);
                  //  m_logger.LogMessage("Error: FetchData: " + ex.Message, MessageType.Informational);
                    WriteUniquelogs("MysqlLogs", "Error: FetchTotalProfitData : " + ex.Message, MessageType.Informational);
                }
            }
            return data;
        }

        public object ExecuteScalar(string strQuery) // function inside MYSQL lock
        {
            object obj = null;
            if (mConnectSuccess) //check open connection
            {
                try
                {
                    MySqlCommand cmd = new MySqlCommand(strQuery, mysqlconnDBManipulation); //create command and assign the query and connection from the constructor
                    obj = cmd.ExecuteScalar();//Execute command                   
                }
                catch (Exception ex)
                {
                  //  m_logger.LogMessage("Error: ExecuteNonQueryCommand: " + ex, MessageType.Informational);
                    WriteUniquelogs("MysqlLogs", "Error: ExecuteScalar : " + ex.Message, MessageType.Informational);
                }
            }
            return obj;
        }
        //IRDSPM::Pratiksha::19-10-2020::For getting details for subscription form
        public string FetchDataForSubscription(string hardwareID) // function inside MYSQL lock
        {
            string data = "";
            string strQuery = "SELECT Name as name,Usertype as usertype,EmailId as emailid,MobileNo as mobileno,ExpiryDate as expirydate,AdminId as adminid,AffiliateID as affiliateid, ClientID as clientid FROM UserDetails WHERE ClientID = '" + hardwareID + "';";
            if (mConnectSuccess)
            {
                try
                {
                    MySqlCommand cmd = new MySqlCommand(strQuery, mysqlconnDBManipulation);//create command and assign the query and connection from the constructor
                    MySqlDataReader dataReader = cmd.ExecuteReader(); //Create a data reader and Execute the command
                    //sanika::22-oct-2020::changed fetching way
                    if (dataReader.Read())
                    {
                        data += dataReader["name"];
                        data += ","+ dataReader["usertype"];
                        data += "," + dataReader["emailid"];
                        data += "," + dataReader["mobileno"];
                        data += "," + dataReader["expirydate"];
                        data += "," + dataReader["adminid"];
                        data += "," + dataReader["affiliateid"];//clientid
                        data += "," + dataReader["clientid"];
                    }
                   // WriteUniquelogs("MysqlLogs", "Error: FetchData : " + ex.Message, MessageType.Informational);
                   // m_logger.LogMessage("Fetched data: \n" + data, MessageType.Informational);
                    WriteUniquelogs("MysqlLogs", "Fetched data: \n" + data, MessageType.Informational);
                    dataReader.Close(); //close Data Reader
                }
                catch (MySqlException ex)
                {
                    WriteUniquelogs("MysqlLogs", "FetchDataForSubscription : Exception Error Message =" + ex.Message, MessageType.Exception);
                    // m_logger.LogMessage("Error: FetchData: " + ex.Message, MessageType.Informational);
                }
            }
            return data;
        }
        public void WriteUniquelogs(string symbol, string message, MessageType msgType)
        {
            try
            {
                SymbolLogs log = new SymbolLogs();
                log.createFile(symbol);
                log.LogMessage(message, MessageType.Informational);
                log = null;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : " + e.Message);
                Trace.WriteLine("Exception : " + e.Message);
            }
        }

        //IRDSPM::Pratiksha::04-12-2020::fetch data from block user
        public bool FetchDataForblock(string hardwareID) // function inside MYSQL lock
        {           
            bool value = false;
            string strQuery = "SELECT BlockStatus as blockstatus FROM UserDetails WHERE ClientID = '" + hardwareID + "'; ";
            if (mConnectSuccess)
            {
                try
                {
                    MySqlCommand cmd = new MySqlCommand(strQuery, mysqlconnDBManipulation);//create command and assign the query and connection from the constructor
                    MySqlDataReader dataReader = cmd.ExecuteReader(); //Create a data reader and Execute the command
                    if (dataReader.HasRows && dataReader.Read())
                    {                        
                        value = Convert.ToBoolean(dataReader["blockstatus"]);
                    }
                    else
                    {
                        value = false;
                    }
                    WriteUniquelogs("MysqlLogs", "FetchDataForblock : Fetched data: \n" + value, MessageType.Informational);
                    dataReader.Close(); //close Data Reader
                }
                catch (MySqlException ex)
                {
                    WriteUniquelogs("MysqlLogs", "FetchDataForblock : Exception Error Message = " + ex.Message, MessageType.Exception);
                }                
            }
            return value;
        }

        public string GetLastRecordFromTable(string tableName)
        {
            string lastDate = "";
            try
            {
                string strQuery = "SELECT * FROM `"+ tableName+"` order by LastTradeTime desc limit 1";
                if(mConnectSuccess)
                {
                    MySqlCommand cmd = new MySqlCommand(strQuery, mysqlconnDBManipulation);//create command and assign the query and connection from the constructor
                    MySqlDataReader dataReader = cmd.ExecuteReader(); //Create a data reader and Execute the command
                    if (dataReader.HasRows && dataReader.Read())
                    {
                        lastDate = Convert.ToDateTime(dataReader["LastTradeTime"]).ToString("yyyy-MM-dd HH:mm:ss");
                    }
                }
                WriteUniquelogs("MysqlLogs", "GetLastRecordFromTable : lastDate = " + lastDate, MessageType.Informational);
            }
            catch (Exception e)
            {
                WriteUniquelogs("MysqlLogs", "GetLastRecordFromTable : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return lastDate;
        }

        /// <summary>
        /// Bulk Data Import
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="tableName"></param>
        public void BulkDataImport(string fileName, string tableName)
        {
            WriteUniquelogs("MysqlLogs", "BulkDataImport : Start for " + tableName,MessageType.Informational);
            
            MySqlBulkLoader mySqlBulkLoader = new MySqlBulkLoader(mysqlconnDBManipulation)
            {
                Local = true,
                TableName = "`" + tableName + "`",
                FieldTerminator = ",",
                LineTerminator = "\n",
                FileName = fileName,
                NumberOfLinesToSkip = 1
            };
            try
            {
                int count = 0;
                if (mConnectSuccess)
                { 
                    // Upload data from file
                    count = mySqlBulkLoader.Load();
                 }
                WriteUniquelogs("MysqlLogs", "BulkDataImport : In " + tableName + " added " + count.ToString() + " lines.",MessageType.Informational);
            }
            catch (Exception ex)
            {
                WriteUniquelogs("MysqlLogs", "BulkDataImport :Exception Error Messagge " + tableName + " Error Msg  " + ex.StackTrace,MessageType.Exception);
            }
            WriteUniquelogs("MysqlLogs", "BulkDataImport : Ends for " + tableName,MessageType.Informational);
        }
    }
}
