﻿using KiteConnect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRDSAlgoOMS
{
    /// Order structure

    public class OrderStore
    {
        private Dictionary<string, SymbolWiseOrderStore> orderStoreBySymbolTT;
        private Dictionary<string, string> orderIDWithStatus;
        private Dictionary<string, Order> AllOrder;
        private static readonly object OrdertickLock = new object();       
        public OrderStore()
        {
            orderStoreBySymbolTT = new Dictionary<string, SymbolWiseOrderStore>();
            orderIDWithStatus = new Dictionary<string, string>();
            AllOrder = new Dictionary<string, Order>();
        }

        public OrderStore(List<Instrument> instruments)
        {
            foreach (var instrument in instruments)
                orderStoreBySymbolTT.Add(GetKey(instrument.TradingSymbol, instrument.Exchange), new SymbolWiseOrderStore());
        }

        //private Dictionary<Tkey2, TValue> dic2 = new Dictionary<Tkey2, TValue>();

        public string GetKey(string tradingSymbol, string exchange)
        {
            return tradingSymbol + "|" + exchange;
        }

        public void AddOrUpdateOrder(OrderExt newOrder)
        {
            lock (OrdertickLock)
            {
                if (!CheckStatusOfOrder(newOrder.order.OrderId, newOrder.order.Status))//sanika::5-Mar-2021::Added check if order is already complete then no need to add status
                {                   
                    var key = GetKey(newOrder.order.Tradingsymbol, newOrder.order.Exchange);
                    if (orderStoreBySymbolTT.ContainsKey(key))
                    {
                        SymbolWiseOrderStore symbolWiseOrderStore;
                        orderStoreBySymbolTT.TryGetValue(key, out symbolWiseOrderStore);
                        if (symbolWiseOrderStore != null)
                            symbolWiseOrderStore.AddOrUpdateOrder(newOrder);
                    }
                    else
                    {
                        orderStoreBySymbolTT.Add(GetKey(newOrder.order.Tradingsymbol, newOrder.order.Exchange), new SymbolWiseOrderStore());
                        if (orderStoreBySymbolTT.ContainsKey(key))
                        {
                            SymbolWiseOrderStore symbolWiseOrderStore;
                            orderStoreBySymbolTT.TryGetValue(key, out symbolWiseOrderStore);

                            symbolWiseOrderStore.AddOrUpdateOrder(newOrder);
                        }
                    }
                    AddStatusOfOrder(newOrder.order.OrderId, newOrder.order.Status);
                }

            }

        }

        public void AddOrder(OrderExt newOrder)
        {
            lock (OrdertickLock)
            {
               //. if (!CheckStatusOfOrder(newOrder.order.OrderId, newOrder.order.Status))//sanika::5-Mar-2021::Added check if order is already complete then no need to add status
                {
                    if (!AllOrder.ContainsKey(newOrder.order.OrderId))
                    {
                        AllOrder.Add(newOrder.order.OrderId, newOrder.order);
                    }
                    else
                    {
                        AllOrder[newOrder.order.OrderId] = newOrder.order;
                    }
                }
            }
        }

        public bool GetOrderbyID(string tradingSymbol, string exchange, string orderId, out OrderExt existingOrder)
        {
            lock (OrdertickLock)
            {
                var key = GetKey(tradingSymbol, exchange);
                SymbolWiseOrderStore symbolWiseOrderStore;
                orderStoreBySymbolTT.TryGetValue(key, out symbolWiseOrderStore);
                if (symbolWiseOrderStore != null)
                    return symbolWiseOrderStore.GetOrder(orderId, out existingOrder);
                else
                    existingOrder = new OrderExt();
                return false;
            }
        }

        public List<Order> GetOrderbySymbol(string tradingSymbol, string exchange)
        {
            lock (OrdertickLock)
            {
                var key = GetKey(tradingSymbol, exchange);
                SymbolWiseOrderStore symbolWiseOrderStore;
                orderStoreBySymbolTT.TryGetValue(key, out symbolWiseOrderStore);

                List<Order> orderhistorybysymbol = new List<Order>();
                List<Order> previousOrderBySymbol = new List<Order>();

                //SymbolWiseOrderStore symbolWiseOrderStore;
                //orderStoreBySymbolTT.TryGetValue(key, out symbolWiseOrderStore);
                if (symbolWiseOrderStore != null)
                {
                    orderhistorybysymbol = symbolWiseOrderStore.GetAllOrder();
                }

                return orderhistorybysymbol;
            }
        }

        public List<Order> GetOpenOrderbySymbol(string tradingSymbol, string exchange)
        {
            lock (OrdertickLock)
            {
                var key = GetKey(tradingSymbol, exchange);
                SymbolWiseOrderStore symbolWiseOrderStore;
                orderStoreBySymbolTT.TryGetValue(key, out symbolWiseOrderStore);

                List<Order> orderhistorybysymbol = new List<Order>();
                List<Order> previousOrderBySymbol = new List<Order>();

                if (symbolWiseOrderStore != null)
                {
                    orderhistorybysymbol = symbolWiseOrderStore.GetAllOpenOrder();
                }

                return orderhistorybysymbol;
            }
        }

        public bool GetOpenOrderbyID(string tradingSymbol, string exchange, string orderId, out OrderExt existingOrder)
        {
            lock (OrdertickLock)
            {
                var key = GetKey(tradingSymbol, exchange);
                SymbolWiseOrderStore symbolWiseOrderStore;
                orderStoreBySymbolTT.TryGetValue(key, out symbolWiseOrderStore);
                if (symbolWiseOrderStore != null)
                {
                    symbolWiseOrderStore.GetOrder(orderId, out existingOrder);
                    return true;
                }
                else
                    existingOrder = new OrderExt();
                return false;
            }
        }

        public bool GetLatestOrder(string mappedSymbol, string buyOrSell, out OrderExt existingOrder)
        {
            lock (OrdertickLock)
            {
                SymbolWiseOrderStore symbolWiseOrderStore;
                orderStoreBySymbolTT.TryGetValue(mappedSymbol, out symbolWiseOrderStore);
                return symbolWiseOrderStore.GetLatestOrder(buyOrSell, out existingOrder);
            }
        }

        public List<Order> GetAllOrder()
        {
            List<Order> Orders = new List<Order>();
            lock (OrdertickLock)
            {
                foreach (var order in AllOrder)
                {
                    Orders.Add(order.Value);
                }
            }
            return Orders;
        }

        //sanika::5-Mar-2021::Added check if order is already complete then no need to add status
        public bool CheckStatusOfOrder(string OrderId, string Status)
        {
            bool res = false;
            if (orderIDWithStatus.ContainsKey(OrderId))
            {
                if (orderIDWithStatus[OrderId] == Constants.ORDER_STATUS_COMPLETE)
                {
                    res = true;
                }
            }
            return res;
        }

        //sanika::5-Mar-2021::Added check if order is already complete then no need to add status
        public void AddStatusOfOrder(string OrderId, string Status)
        {
            bool res = false;
            if (orderIDWithStatus.ContainsKey(OrderId))
            {
                orderIDWithStatus[OrderId] = Status;
            }
            else
            {
                orderIDWithStatus.Add(OrderId, Status);
            }
        }
    }
}



