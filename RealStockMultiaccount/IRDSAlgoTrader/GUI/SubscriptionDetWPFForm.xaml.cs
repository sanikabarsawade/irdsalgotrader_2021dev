﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace IRDSAlgoOMS.GUI
{
    /// <summary>
    /// Interaction logic for SubscriptionDetWPFForm.xaml
    /// </summary>
    public partial class SubscriptionDetWPFForm : UserControl
    {
        dynamic m_ObjkiteConnectWrapper = null;
        SubscriptionDetWindowsForm m_objSubDet = null;
        string m_UserID = "";
        string hardwareId = "";
        string[] Details;
        public SubscriptionDetWPFForm(SubscriptionDetWindowsForm obj, dynamic ObjkiteConnectWrapper)
        {
            try
            {
                //IRDSPM::Pratiksha::26-10-2020::Added new GUI, remove unwanted methods
                InitializeComponent();
                m_objSubDet = obj;
                m_ObjkiteConnectWrapper = ObjkiteConnectWrapper;
                //Get hardware id
                hardwareId = License.Status.HardwareID;
                if (m_ObjkiteConnectWrapper != null)
                {
                    string data = "";
                    m_UserID = m_ObjkiteConnectWrapper.GetUserID();
                    data = m_ObjkiteConnectWrapper.FetchUserdetails(m_UserID, hardwareId);
                    if (data != "")//sanika::22-oct-2020::changed condition
                    {
                        //sanika::22-oct-2020::changed splitting type and indexes
                        string[] splittedData = data.Split(',');
                        Details = splittedData;
                        //IRDSPM::Pratiksha::24-12-2020::Split if long name
                        if (Details[0].Split(' ').Length > 1)
                        {
                            lblName.Content = Details[0].Split(' ')[0].ToUpper();
                        }
                        else
                        {
                        lblName.Content = Details[0].ToUpper();
                        }
                        lblNameValue.Content = Details[0];
                        //IRDSPM::PRatiksha::02-11-2020::Email text waas cropping
                        lblemailValue.Text = Details[2];
                        lbleMobileValue.Content = Details[3];
                        //license
                        lblclientIDValue.Content = Details[7];
                        lblAffiliateIDValue.Content = Details[6];
                        lbleExpiryDateValue.Content = (DateTime.FromOADate(Convert.ToDouble(Details[4])).ToString());
                    }
                    else
                    {
                        MessageBox.Show("Unable to load data");
                    }
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "Subscription Details Setting | Exception : " + ex.Message, MessageType.Informational);
            }
        }
        private void btnCancel_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            btnCancel.Foreground = Brushes.Black;
        }

        private void btnCancel_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            btnCancel.Foreground = Brushes.White;
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            m_objSubDet.Close();
        }
        
        public void WriteUniquelogs(string symbol, string message, MessageType msgType)
        {
            try
            {
                SymbolLogs log = new SymbolLogs();
                log.createFile(symbol);
                log.LogMessage(message, MessageType.Informational);
                log = null;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : " + e.Message);
            }
        }

        private void btnRenew_MouseEnter(object sender, MouseEventArgs e)
        {
            btnRenew.Foreground = Brushes.Black;
        }

        private void btnRenew_MouseLeave(object sender, MouseEventArgs e)
            {
            btnRenew.Foreground = Brushes.White;
        }

        private void btnRenew_Click(object sender, RoutedEventArgs e)
                {
            try
                    {
                Process.Start("https://rzp.io/i/dxIZMgn");
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "Subscription Details Setting |btnRenew_Click Exception : " + ex.Message, MessageType.Informational);
            }
        }
    }
}
