﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IRDSAlgoOMS.GUI
{
    public partial class AboutUsWindowsForm : Form
    {
		//IRDSPM::Pratiksha::08-02-2021::Added parameter
        public AboutUsWindowsForm(dynamic ObjkiteConnectWrapper, string title)
        {
            this.TopMost = true;
            InitializeComponent(this, ObjkiteConnectWrapper, title);
            this.Load += AboutUsWindowsForm_Load;
        }

        private void AboutUsWindowsForm_Load(object sender, EventArgs e)
        {
            this.TopMost = false;
        }
    }
}
