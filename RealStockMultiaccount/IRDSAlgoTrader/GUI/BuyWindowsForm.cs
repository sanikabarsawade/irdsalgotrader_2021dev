﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IRDSAlgoOMS.GUI
{
    public partial class BuyWindowsForm : Form
    {
        //sanika::11-sep-2020::changed to dynamic for samco and zerodha 
        public BuyWindowsForm(dynamic objKiteConnectWrapper, string title)
        {
            InitializeComponent(objKiteConnectWrapper, this, title);
        }

        public void orderType(string typeofOrder)
        {
            this.buyForm1.orderType(typeofOrder);
        }
    }
}
