﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace IRDSAlgoOMS.GUI
{
    public partial class TradingSymbolWPFForm : System.Windows.Controls.UserControl
    {
        TradingSymbolWindowForm m_obj = null;
        //NSE
        string startTimeNSE = "";
        string EndTimeNSE = "";
        string ExitTimeNSE = "";
        //NFO
        string startTimeNFO = "";
        string EndTimeNFO = "";
        string ExitTimeNFO = "";
        //NSE
        string startTimeMCX = "";
        string EndTimeMCX = "";
        string ExitTimeMCX = "";
        //MCX
        string startTimeCDS = "";
        string EndTimeCDS = "";
        string ExitTimeCDS = "";

        //string TimeLimitToPlaceOrder4 = " ";
       // string TimeForExitAllOpenPosition5 = " ";
        //string endTime6 = " ";
        bool calculateProfitLoss = false;
        string profit = "";
        string loss = "";
        ConfigSettings Settings;
        string iniFileTradingSymbol;
        string path = Directory.GetCurrentDirectory();
        string fontDet = "";
        string fontStyle = "";
        bool currentcalculateProfitLoss;
        bool calculateProfitLossVal;
        dynamic m_objKiteConnectWrapper = null;
        string message = "";
        //IRDSPM::Pratiksha::08-02-2021::Dynamic name
        string title = "";
        MessageBoxButtons buttons;
        DialogResult dialog;
        INIFile inifileObj = null;

        //IRDSPM::Pratiksha::20-10-2020::For Risk management on trader setting
        bool isRiskmanagement = false;
        string brokerQuantity = "";
        string ManualQuantity = "";

        //IRDSPM::Pratiksha::26-10-2020::Added for Trading status
        bool isTradingStatus = false;

        //IRDSPM::Pratiksha::10-11-2020::For New fields

        bool CheckNSE = false;
        bool CheckNFO = false;
        bool CheckMCX = false;
        bool CheckOptions = false;
        bool CheckCDS = false;
        string m_Userid = "";
        //IRDSPM::Pratiksha::23-11-2020::For Auto/manual Status for downalod token
        string DownloadTokenWay = "";
        public TradingSymbolWPFForm(dynamic kiteConnectWrapper, TradingSymbolWindowForm obj, string m_title, string Userid)
        {
            InitializeComponent();
            m_obj = obj;
            m_Userid = Userid;
            m_objKiteConnectWrapper = kiteConnectWrapper;
            iniFileTradingSymbol = path + @"\Configuration\" + "TradingSetting.ini";
            try
            {
                if (File.Exists(iniFileTradingSymbol))
                {
                    Settings = new ConfigSettings();
                    Settings.readTradingINIFile(iniFileTradingSymbol);
                    //Passing values from config file to local variables
                    //Risk Details
                    isTradingStatus = Settings.isTradingStatus;
                    isRiskmanagement = Settings.isRiskmanagement;
                    brokerQuantity = Settings.brokerQuantity.ToString();
                    ManualQuantity = Settings.ManualQuantity.ToString();

                    //Trading Details
                    //NSE
                    startTimeNSE = Settings.StartTimeNSE;
                    EndTimeNSE = Settings.EndTimeNSE;
                    ExitTimeNSE = Settings.ExitTimeNSE;
                    //NFO
                    startTimeNFO = Settings.StartTimeNFO;
                    EndTimeNFO = Settings.EndTimeNFO;
                    ExitTimeNFO = Settings.ExitTimeNFO;
                    //MCX
                    startTimeMCX = Settings.StartTimeMCX;
                    EndTimeMCX = Settings.EndTimeMCX;
                    ExitTimeMCX = Settings.ExitTimeMCX;
                    //CDS
                    startTimeCDS = Settings.StartTimeCDS;
                    EndTimeCDS = Settings.EndTimeCDS;
                    ExitTimeCDS = Settings.ExitTimeCDS;

                    //Profit Details
                    calculateProfitLoss = Settings.isCalculateProfitLoss;
                    profit = Settings.m_overallProfit.ToString();
                    loss = Settings.m_overallLoss.ToString();

                    //Download Details
                    DownloadTokenWay = Settings.DownloadTokenWay.ToLower();
                    CheckNSE = Settings.CheckNSE;//sanika::22-Dec-2020::changed to tostring()
                    CheckNFO = Settings.CheckNFO;
                    CheckMCX = Settings.CheckMCX;
                    CheckOptions = Settings.CheckOptions;
                    CheckCDS = Settings.CheckCDS;

                    //Passing local values to UI
                    //Risk Details
                    if (isTradingStatus == true)
                    { RadioBtnTradingStatusTrue.IsChecked = true; }
                    else
                    {  RadioBtnTradingStatusFalse.IsChecked = true; }

                    if (isRiskmanagement == true) {
                        RadioBtnRiskMgmtTrue.IsChecked = true;
                        TextBrokerQuantity.IsEnabled = true;
                        TextManualQuantity.IsEnabled = true;
                    }
                    else { 
                        RadioBtnRiskMgmtFalse.IsChecked = true;
                        TextBrokerQuantity.IsEnabled = false;
                        TextManualQuantity.IsEnabled = false;
                    }
                    TextBrokerQuantity.Text = brokerQuantity;
                    TextManualQuantity.Text = ManualQuantity;

                    //Trading details 
                    //NSE
                    TextNSEStartTime.Text = startTimeNSE;
                    TextNSEEndTime.Text = EndTimeNSE;
                    TextNSEExit.Text = ExitTimeNSE;
                    //NFO
                    TextNFOStartTime.Text = startTimeNFO;
                    TextNFOEndTime.Text = EndTimeNFO;
                    TextNFOExit.Text = ExitTimeNFO;
                    //MCX
                    TextMCXStartTime.Text = startTimeMCX;
                    TextMCXEndTime.Text = EndTimeMCX;
                    TextMCXExit.Text = ExitTimeMCX;
                    //CDS
                    TextCDSStartTime.Text = startTimeCDS;
                    TextCDSEndTime.Text = EndTimeCDS;
                    TextCDSExit.Text = ExitTimeCDS;

                    //Profit Details
                    if (calculateProfitLoss == true){
                        RadioBtnProfitLossTrue.IsChecked = true;
                        TextOverallProfit.IsEnabled = true;
                        TextOverallLoss.IsEnabled = true;
                    }
                    else {
                        RadioBtnProfitLossFalse.IsChecked = true;
                        TextOverallProfit.IsEnabled = false;
                        TextOverallLoss.IsEnabled = false;
                    }
                    TextOverallProfit.Text = profit;
                    TextOverallLoss.Text = loss;

                    //Download Details

                    if (DownloadTokenWay == "auto") {  RadioBtnAutoTrue.IsChecked = true; }
                    else { RadioBtnManualTrue.IsChecked = true; }
                    //sanika::22-dec-2020::added to lower
                    if ( CheckNSE == true) { checkBox1NSE.IsChecked = true; }
                    else { checkBox1NSE.IsChecked = false; }

                    if (CheckNFO == true) { checkBox2NFO.IsChecked = true; }
                    else { checkBox2NFO.IsChecked = false; }

                    if (CheckMCX == true) { checkBox3MCX.IsChecked = true; }
                    else { checkBox3MCX.IsChecked = false; }

                    if (CheckOptions == true) { checkBox4Options.IsChecked = true; }
                    else { checkBox4Options.IsChecked = false; }

                    if(CheckCDS == true) { checkBox5Forex.IsChecked = true;}
                    else { checkBox5Forex.IsChecked = false; }
                    WriteUniquelogs("settingLogs" + " ", "Trading Setting | Data loaded on form properly." , MessageType.Informational);
                    //Pratiksha::08-02-2021::For title
                    title = m_title;
                }
                else
                {
                    buttons = MessageBoxButtons.OK;
                    message = iniFileTradingSymbol + " INI file does not exit!!";
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "Trading Setting | Exception : " + ex, MessageType.Informational);
            }
        }
        //private void btnClear_Click(object sender, RoutedEventArgs e)
        //{
        //    TextNSEStartTime.Text = "";
        //    TextNFOStartTime.Text = "";
        //    TextMCXStartTime.Text = "";
        //    textBox4TimetoPlaceOrder.Text = "";
        //    textBox5timeforExitAllposition.Text = "";
        //    textBox6EndTime.Text = "";
        //    RadioBtnProfitLossFalse.IsChecked = true;
        //    TextOverallProfit.Text = "";
        //    TextOverallLoss.Text = "";
        //    //IRDSPM::Pratiksha::20-10-2020::Set default to null
        //    RadioBtnRiskMgmtFalse.IsChecked = true;
        //    Text3BrokerQuantity.Text = "";
        //    Text4ManualQuantity.Text = "";
        //    //IRDSPM::Pratiksha::20-10-2020::For Risk management on trader setting 
        //    RadioBtnTradingStatusFalse.IsChecked = true;
        //    TextNSEEndTime.Text = "";
        //    TextNFOEndTime.Text = "";
        //    TextMCXEndTime.Text = "";
        //    checkBox1NSE.IsChecked = false;
        //    checkBox2NFO.IsChecked = false;
        //    checkBox3MCX.IsChecked = false;
        //    checkBox4Options.IsChecked = false;
        //}

        private void btnApply_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                message = "Do you want to save the changes?";
                buttons = MessageBoxButtons.YesNo;
                dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                if (dialog == DialogResult.Yes)
                {
                inifileObj = new INIFile(iniFileTradingSymbol);
                string key_val;
                string value_value;
                    string[] key = new string[25];
                    string[] value = new string[25];
                    //Trading Details
                key[0] = "startTimeNSE";
                    key[1] = "endTimeNSE";
                    key[2] = "exitTimeNSE";
                    //NFO
                    key[3] = "startTimeNFO";
                    key[4] = "endTimeNFO";
                    key[5] = "exitTimeNFO";
                    //MCX
                    key[6] = "startTimeMCX";
                    key[7] = "endTimeMCX";
                    key[8] = "exitTimeMCX";
                    //CDS
                    key[9] = "startTimeCDS";
                    key[10] = "endTimeCDS";
                    key[11] = "exitTimeCDS";
                    //Risk management
                    key[12] = "TradingStatus";
                    key[13] = "riskManagement";
                    key[14] = "brokerQuantity";
                    key[15] = "ManualQuantity";
                    //Profit Details
                    key[16] = "calculateProfitLoss";
                    key[17] = "overallProfit";
                    key[18] = "overallLoss";
                    //Download Details
                    key[19] = "downloadTokenWay";
                    key[20] = "CheckNSE";
                    key[21] = "CheckNFO";
                    key[22] = "CheckMCX";
                    key[23] = "CheckOptions";
                    key[24] = "CheckCDS";

                    //Risk Management
                    value[0] = TextNSEStartTime.Text;
                    value[1] = TextNSEEndTime.Text;
                    value[2] = TextNSEExit.Text;
                    //NFO
                    value[3] = TextNFOStartTime.Text;
                    value[4] = TextNFOEndTime.Text;
                    value[5] = TextNFOExit.Text;
                    //MCX
                    value[6] = TextMCXStartTime.Text;
                    value[7] = TextMCXEndTime.Text;
                    value[8] = TextMCXExit.Text;
                    //CDS
                    value[9] = TextCDSStartTime.Text;
                    value[10] = TextCDSEndTime.Text;
                    value[11] = TextCDSExit.Text;
                    //Risk management
                    if (RadioBtnTradingStatusTrue.IsChecked == true)
                    { value[12] = "true";}
                    else { value[12] = "false";  }

                    if (RadioBtnRiskMgmtTrue.IsChecked == true)
                    { value[13] = "true"; }
                    else { value[13] = "false"; }
                    value[14] = TextBrokerQuantity.Text;
                    value[15] = TextManualQuantity.Text;

                    //Profit Details
                    if (RadioBtnProfitLossTrue.IsChecked == true)
                    { value[16] = "true"; }
                    else { value[16] = "false"; }
                    value[17] = TextOverallProfit.Text;
                    value[18] = TextOverallLoss.Text;
                    //Download Details
                    if (RadioBtnAutoTrue.IsChecked == true)
                    { value[19] = "Auto"; }
                    else if (RadioBtnManualTrue.IsChecked == true)
                    { value[19] = "Manual"; }

                    if (checkBox1NSE.IsChecked == true)
                    {  value[20] = "true"; }
                    else  { value[20] = "false"; }

                    if (checkBox2NFO.IsChecked == true)
                    {  value[21] = "true"; }
                else
                    { value[21] = "false"; }

                    if (checkBox3MCX.IsChecked == true)
                    { value[22] = "true"; }
                    else
                    {  value[22] = "false"; }

                    if (checkBox4Options.IsChecked == true)
                    { value[23] = "true"; }
                    else {  value[23] = "false"; }

                    if (checkBox5Forex.IsChecked == true)
                    { value[24] = "true"; }
                    else { value[24] = "false"; }

                    for (int i = 0; i < 25; i++)
                    {
                        inifileObj.TradingIniWriteValue("TRADINGSETTING", key[i], value[i], iniFileTradingSymbol);
                    }
                    m_objKiteConnectWrapper.isReloadTradingSettingINI();

                    //IRDSPM::Pratiksha::16-02-2021::To add into guilogs for toast
                    WriteGUIlogs(m_objKiteConnectWrapper.m_ExceptionCounter, DateTime.Now + " Trading setting's changes saved successfully!!!");
                    m_objKiteConnectWrapper.m_ExceptionCounter++;

                   // m_objKiteConnectWrapper.IntrumentsCSVToDB();//sanika::27-Nov-2020::commented as option are given auto and manual
                    //message = "Saved changes successfully!!!";
                    //buttons = MessageBoxButtons.OK;
                    //dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                    //if (dialog == DialogResult.OK)
                    //{
                        m_obj.Close();
                    //    WriteUniquelogs("settingLogs" + " ", "Trading Setting | Data updated in tradingSetting.ini file.", MessageType.Informational);
                    //}
                }
                else
                {
                    m_obj.Close();
                    WriteUniquelogs("settingLogs" + " ", "Trading Setting | You have clicked No, no data updated in tradingSetting.ini file.", MessageType.Informational);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "Trading Setting | Exception : " + ex.Message, MessageType.Exception);
            }
        }
        //IRDSPM::Pratiksha::16-02-2021::For toast notification
        public void WriteGUIlogs(int Counter, string message)
        {
            try
            {
                SymbolLogs log = new SymbolLogs();
                log.createFile("GUILogs_"+ m_Userid);
                log.LogMessage(Counter + "|" + message, MessageType.Informational);
                log = null;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : " + e.Message);
                Trace.WriteLine("Exception : " + e.Message);
            }
        }
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            //IRDSPM::Pratiksha::18-11-2020::For On cancel ask for want to save changes
            bool currentCalculateProfitVal = false;
            bool currentisRiskmanagement = false;
            bool currentisTradingStatus = false;
            string currentAutoValue = "";
            bool currentNSE = false;
            bool currentNFO = false;
            bool currentMCX = false;
            bool currentOption = false;
            bool currentCDS = false;
            if(RadioBtnAutoTrue.IsChecked == true)
            { currentAutoValue = "auto"; }
            else { currentAutoValue = "manual"; }

            if (RadioBtnProfitLossTrue.IsChecked == true)
            { currentCalculateProfitVal = true; }
            else { currentCalculateProfitVal = false; }

            if (RadioBtnRiskMgmtTrue.IsChecked == true)
            { currentisRiskmanagement = true; }
            else { currentisRiskmanagement = false; }

            if (RadioBtnTradingStatusTrue.IsChecked == true)
            { currentisTradingStatus = true; }
            else { currentisTradingStatus = false; }
            //checkboxes
            if (checkBox1NSE.IsChecked == true)
            { currentNSE = true; }
            else { currentNSE = false; }

            if (checkBox2NFO.IsChecked == true)
            { currentNFO = true; }
            else { currentNFO = false; }

            if (checkBox3MCX.IsChecked == true)
            { currentMCX = true; }
            else { currentMCX = false; }

            if (checkBox4Options.IsChecked == true)
            { currentOption = true; }
            else { currentOption = false; }
            
            if (checkBox5Forex.IsChecked == true)
            { currentCDS = true; }
            else { currentCDS = false; }

            if (startTimeNSE != TextNSEStartTime.Text || startTimeNFO != TextNFOStartTime.Text || startTimeMCX != TextMCXStartTime.Text || startTimeCDS != TextCDSStartTime.Text ||
            EndTimeNSE != TextNSEEndTime.Text || EndTimeNFO != TextNFOEndTime.Text || EndTimeMCX != TextMCXEndTime.Text || EndTimeCDS != TextCDSEndTime.Text||
            ExitTimeNSE != TextNSEExit.Text || ExitTimeNFO != TextNFOExit.Text || ExitTimeMCX != TextMCXExit.Text || ExitTimeCDS != TextCDSExit.Text||
            profit != TextOverallProfit.Text || loss != TextOverallLoss.Text ||
            brokerQuantity != TextBrokerQuantity.Text || ManualQuantity != TextManualQuantity.Text || calculateProfitLoss != currentCalculateProfitVal ||
            isRiskmanagement != currentisRiskmanagement || isTradingStatus != currentisTradingStatus ||  currentAutoValue.ToLower() != DownloadTokenWay.ToLower() ||
            CheckNSE != currentNSE || CheckNFO != currentNFO || CheckMCX != currentMCX || CheckOptions != currentOption || CheckCDS != currentCDS)
            {
                btnApply_Click(sender, e);
            }
            else
            {
            m_obj.Close();
        }
        }

        //private void btnClear_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        //{
        //    btnClear.Foreground = Brushes.Black;
        //}

        private void btnApply_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            btnApply.Foreground = Brushes.Black;
        }

        private void btnCancel_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            btnCancel.Foreground = Brushes.Black;
        }

        //private void btnClear_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        //{
        //    btnClear.Foreground = Brushes.White;
        //}

        private void btnApply_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            btnApply.Foreground = Brushes.White;
        }

        private void btnCancel_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            btnCancel.Foreground = Brushes.White;
        }
        public void LoadKiteConnectObject(dynamic kiteConnectWrapper)
        {
            m_objKiteConnectWrapper = kiteConnectWrapper;
        }

        private void RadioBtnProfitLossTrue_Click(object sender, RoutedEventArgs e)
        {
            TextOverallProfit.IsEnabled = true;
            TextOverallLoss.IsEnabled = true;
        }

        private void RadioBtnProfitLossFalse_Click(object sender, RoutedEventArgs e)
        {
            TextOverallProfit.IsEnabled = false;
            TextOverallLoss.IsEnabled = false;
        }
        public void WriteUniquelogs(string symbol, string message, MessageType msgType)
        {
            try
            {
                SymbolLogs log = new SymbolLogs();
                log.createFile(symbol);
                log.LogMessage(message, MessageType.Informational);
                log = null;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : " + e.Message);
                Trace.WriteLine("Exception : " + e.Message);
            }
        }

        private void RadioBtnRiskMgmtTrue_Click(object sender, RoutedEventArgs e)
        {
            TextBrokerQuantity.IsEnabled = true;
            TextManualQuantity.IsEnabled = true;
        }

        private void RadioBtnRiskMgmtFalse_Click(object sender, RoutedEventArgs e)
        {
            TextBrokerQuantity.IsEnabled = false;
            TextManualQuantity.IsEnabled = false;
        }
    }
}
