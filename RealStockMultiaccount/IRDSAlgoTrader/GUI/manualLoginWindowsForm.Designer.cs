﻿using System.IO;
using System.Reflection;

namespace IRDSAlgoOMS.GUI
{
    partial class manualLoginWindowsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /// 
        //IRDSPM::Pratiksha::12-02-2021::dynamically set icon
        Assembly myAssembly = Assembly.GetExecutingAssembly();
        Stream IconStream = null;
        private void InitializeComponent(Kite kite, string url, manualLoginWindowsForm objManualLoginWindowsForm, string Title)
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.elementHost2 = new System.Windows.Forms.Integration.ElementHost();
            this.wpfManualLogin2 = new IRDSAlgoOMS.GUI.WPFManualLogin(kite, url, objManualLoginWindowsForm);
            this.SuspendLayout();
            // 
            // elementHost2
            // 
            this.elementHost2.Location = new System.Drawing.Point(0, 0);
            this.elementHost2.Name = "elementHost2";
            this.elementHost2.Size = new System.Drawing.Size(668, 210);
            this.elementHost2.TabIndex = 0;
            this.elementHost2.Text = "elementHost2";
            this.elementHost2.Child = this.wpfManualLogin2;
            // 
            // manualLoginWindowsForm
            // 
            this.ClientSize = new System.Drawing.Size(667, 212);
            this.Controls.Add(this.elementHost2);
            IconStream = myAssembly.GetManifestResourceStream("IRDSAlgoOMS.Resources." + Title + ".ico");
            this.Icon = new System.Drawing.Icon(IconStream);

            this.Name = "manualLoginWindowsForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Manual Login";
            this.TopMost = true;
            this.MaximizeBox = false;
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Integration.ElementHost elementHost1;
        private WPFManualLogin wpfManualLogin1;
        private System.Windows.Forms.Integration.ElementHost elementHost2;
        private WPFManualLogin wpfManualLogin2;
    }
}