﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Data;
using System.Drawing;
namespace IRDSAlgoOMS.GUI
{
    /// <summary>
    /// Interaction logic for WPFManualLogin.xaml
    /// </summary>
    public partial class WPFManualLogin : UserControl
    {
        Kite m_Kite = null;
        public string m_token = "";
        string m_url = "";
        manualLoginWindowsForm m_ManualLoginWindowsForm  = null;
        public WPFManualLogin()
        {
            InitializeComponent();
        }
        public WPFManualLogin(Kite kite, string url, manualLoginWindowsForm objManualLoginWindowsForm)
        {
            InitializeComponent();
            m_Kite = kite;
            m_url = url;
            lblUrl.Content = url;
            m_ManualLoginWindowsForm = objManualLoginWindowsForm;
        }

        private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            string token = txtToken.Text;
            m_ManualLoginWindowsForm.Close();
        }

        public string Token
        {
            get
            {
                return txtToken.Text;
            }
        }
        private void lblUrl_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start(m_url);
        }
    }
}
