﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IRDSAlgoOMS.GUI
{
    public partial class TradingSymbolWindowForm : Form
    {
        public dynamic m_ObjkiteConnectWrapper = null;
        public TradingSymbolWindowForm(dynamic kiteConnectWrapper, string m_title, string m_Userid)
        {
            this.TopMost = true;
            InitializeComponent(kiteConnectWrapper , this, m_title, m_Userid);
            this.Load += TradingSymbolWindowForm_Load;
        }

        private void TradingSymbolWindowForm_Load(object sender, EventArgs e)
        {
            this.TopMost = false;
        }

        public void LoadKiteConnectObject(dynamic kiteConnectWrapper)
        {
            tradingSymbolWPFForm2.LoadKiteConnectObject(kiteConnectWrapper);
        }
    }
}
