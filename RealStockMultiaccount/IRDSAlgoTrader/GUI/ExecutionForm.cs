﻿using IRDSAlgoOMS.GUI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IRDSAlgoOMS
{
    public partial class ExecutionForm : Form
    {        
        Color bColor;
        Color fColor;       
        Logger logger = Logger.Instance;

        public KiteConnectWrapper m_ObjkiteConnectWrapper = null;
        List<string> ListForSymbolName = new List<string>();
        List<string> ListForOpenPositionGridView = new List<string>();
        List<string> ListForMarketWtchGridView = new List<string>();
        List<string> ListForOrderBookGridView = new List<string>();
        public DataTable mydtOpenPosition;
        public DataTable mydtMarketWatch;
        public DataTable mydtOrderBook;
        public bool loginStatusValue = false;
        string m_UserID = "";
        ExecutionForm objExecutionForm = null;
        Dictionary<string, string> m_RealTimeSymbols = new Dictionary<string, string>();
        string m_futName = "";
        List<string> SymbolList = null;
        List<double> ProfitOrLoss = new List<double>();
        public ExecutionForm(string userID, List<string> symbols)
        {
            InitializeComponent();
            //IRDSPM::Pratiksha::03-07-2020::For reading and assigning color to background --start
            string path = Directory.GetCurrentDirectory();
            string iniFileSetting = path + @"\Configuration\" + "Setting.ini";
            //IRDSPM::Pratiksha::03-07-2020::For setting ini
            string bgcolor = "";
            string fontcolor = "";
            string fontDet = "";
            string fontStyle = "";
            if (File.Exists(iniFileSetting))
            {
                ConfigSettings Settings1 = new ConfigSettings(logger);
                Settings1.ReadSettingConfigFile(iniFileSetting);
                bgcolor = Settings1.bgcolorname.ToString();
                fontcolor = Settings1.fontcolorname.ToString();
                fontDet = Settings1.fontdet.ToString();
                fontStyle = Settings1.fontstyle.ToString();
            }
            string[] colorDetfromINI = { bgcolor, fontcolor };
            for (int i = 0; i < colorDetfromINI.Length; i++)
            {
                Match m = Regex.Match(colorDetfromINI[i], @"A=(?<Alpha>\d+),\s*R=(?<Red>\d+),\s*G=(?<Green>\d+),\s*B=(?<Blue>\d+)");
                if (m.Success)
                {
                    int alpha = int.Parse(m.Groups["Alpha"].Value);
                    int red = int.Parse(m.Groups["Red"].Value);
                    int green = int.Parse(m.Groups["Green"].Value);
                    int blue = int.Parse(m.Groups["Blue"].Value);
                    if (i == 0)
                    {
                        bColor = Color.FromArgb(alpha, red, green, blue);
                    }
                    else
                    {
                        fColor = Color.FromArgb(alpha, red, green, blue);
                    }
                }
                else
                {
                    if (i == 0)
                    {
                        bColor = Color.FromName(colorDetfromINI[i]);
                    }
                    else
                    {
                        fColor = Color.FromName(colorDetfromINI[i]);
                    }
                }
            }

            label1.ForeColor = fColor;
            label2.ForeColor = fColor;
            label3.ForeColor = fColor;
            label4.ForeColor = fColor;
            label5.ForeColor = fColor;
            label6.ForeColor = fColor;
            label7.ForeColor = fColor;
            label8.ForeColor = fColor;
            label9.ForeColor = fColor;
            label10.ForeColor = fColor;
            loginstatusval.ForeColor = fColor;

            lblUpdatedTime.ForeColor = Color.Black;
            lblOpenPosition.ForeColor = Color.Black;
            label15.ForeColor = Color.Black;
            label16.ForeColor = Color.Black;
            label17.ForeColor = Color.Black;
            lblUpdatedOrder.ForeColor = Color.Black;
            lblOrders.ForeColor = Color.Black;
            label20.ForeColor = Color.Black;
            label21.ForeColor = Color.Black;
            label22.ForeColor = Color.Black;
            login.ForeColor = Color.White;
            button1.ForeColor = Color.White;
            btnRollOver.ForeColor = Color.White;
            refresh.ForeColor = Color.White;

            groupBox1.BackColor = bColor;
            BackColor = bColor;
            groupBox1.ForeColor = fColor;
            groupBox3.ForeColor = fColor;
            dataGridView1.ForeColor = Color.Black;
            dataGridView.ForeColor = Color.Black;
            OrderBook.ForeColor = Color.Black;
            //IRDSPM::Pratiksha::09-07-2020::Accessing font family and font size
            try
            {
                string fontfamilyAndSize = fontDet.Substring(fontDet.IndexOf("=") + 1, fontDet.IndexOf(",") - 1);
                string fontname = fontfamilyAndSize.Substring(0, fontfamilyAndSize.IndexOf(","));
                int pFrom = fontfamilyAndSize.IndexOf("=") + "=".Length;
                int pTo = fontfamilyAndSize.LastIndexOf(",");
                string result = fontfamilyAndSize.Substring(pFrom, fontfamilyAndSize.Length - pFrom);
                float fontSize = float.Parse(result);

                file.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                exit.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                readCsvToolStripMenuItem.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                reconnectToolStripMenuItem.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                oROMSToolStripMenuItem.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                symbolSelectionToolStripMenuItem.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                generalSettingToolStripMenuItem.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                settingsToolStripMenuItem.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                button1.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                btnRollOver.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                groupBox1.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                groupBox3.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                //IRDSPM::Pratiksha::17-07-2020::Make the menu fontsize constant, and background color black
                aboutUsToolStripMenuItem.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                registerToolStripMenuItem.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                helpToolStripMenuItem.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                listBox.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                listBoxuserView.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                label1.Font = new Font(fontname, fontSize + 1, FontStyle.Bold, GraphicsUnit.Point);
                label2.Font = new Font(fontname, fontSize + 1, FontStyle.Bold, GraphicsUnit.Point);
                label22.Font = new Font(fontname, fontSize + 1, FontStyle.Bold, GraphicsUnit.Point);
                label21.Font = new Font(fontname, fontSize + 1, FontStyle.Bold, GraphicsUnit.Point);
                label16.Font = new Font(fontname, fontSize + 1, FontStyle.Bold, GraphicsUnit.Point);
                label15.Font = new Font(fontname, fontSize + 1, FontStyle.Bold, GraphicsUnit.Point);
                label20.Font = new Font(fontname, fontSize + 1, FontStyle.Bold, GraphicsUnit.Point);
                button2.Font = new Font(fontname, fontSize + 1, FontStyle.Bold, GraphicsUnit.Point);
                cancel.Font = new Font(fontname, fontSize + 1, FontStyle.Bold, GraphicsUnit.Point);
                login.Font = new Font(fontname, fontSize + 1, FontStyle.Bold, GraphicsUnit.Point);
                button1.Font = new Font(fontname, fontSize + 1, FontStyle.Bold, GraphicsUnit.Point);
                refresh.Font = new Font(fontname, fontSize + 1, FontStyle.Bold, GraphicsUnit.Point);
                btnRollOver.Font = new Font(fontname, fontSize + 1, FontStyle.Bold, GraphicsUnit.Point);
                close.Font = new Font(fontname, fontSize + 1, FontStyle.Bold, GraphicsUnit.Point);
                loginstatusval.Font = new Font(fontname, fontSize + 3, FontStyle.Bold, GraphicsUnit.Point);
                lblStatus.Font = new Font(fontname, fontSize + 7, FontStyle.Bold, GraphicsUnit.Point);
                label3.Font = new Font(fontname, fontSize + 1, FontStyle.Bold, GraphicsUnit.Point);
                label4.Font = new Font(fontname, fontSize + 1, FontStyle.Bold, GraphicsUnit.Point);
                label5.Font = new Font(fontname, fontSize + 1, FontStyle.Bold, GraphicsUnit.Point);
                label6.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                label7.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                label8.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                label9.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                label10.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                lblOpenPosition.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                lblUpdatedTime.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                label17.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                lblOrders.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                lblUpdatedOrder.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);

            }
            catch (Exception ex)
            {
                logger.LogMessage("ExecutionForm : Exception Error Message = " + ex.Message, MessageType.Exception);
            }

            //load 
            m_UserID = userID;
            SymbolList = symbols;
            lblStatus.Text = "Waiting";

            //to add columns in dataTable
            AddColumns();

            //start Timer
            StartTimer();

            
        }

        public void StartTimer()
        {
            System.Timers.Timer _timer = new System.Timers.Timer();
            _timer.Elapsed += _OnTimerExecute;
            //04-August-2021: sandip:memory issue
            _timer.Interval = 60000; // checks connection every second
            _timer.Enabled = true;
            objExecutionForm = this;
            listBoxuserView.Items.Add(DateTime.Now + "  " + "Initially " + lblStatus.Text);
            listBox.Items.Add(DateTime.Now + "  " + "Initially " + lblStatus.Text);
            logger.LogMessage("Start timer : started new System.Timers.Timer()", MessageType.Informational);
        }

        public void AddColumns()
        {
            //open position
            mydtOpenPosition = new DataTable();
            mydtOpenPosition.Columns.Add("Exchange");
            mydtOpenPosition.Columns.Add("Symbol");
            mydtOpenPosition.Columns.Add("Time");
            mydtOpenPosition.Columns.Add("OrderId");
            mydtOpenPosition.Columns.Add("Product");
            mydtOpenPosition.Columns.Add("Quantity");
            mydtOpenPosition.Columns.Add("Price");
            mydtOpenPosition.Columns.Add("MTM");
            mydtOpenPosition.Columns.Add("SqrOff");
            //mydtOpenPosition.Columns.Add("P&L");
            dataGridView.DataSource = mydtOpenPosition;
            //IRDSPM::Pratiksha::17-07-2020::to make all columns of same width               

            dataGridView.Columns[0].Width = 135;
            dataGridView.Columns[1].Width = 130;
            dataGridView.Columns[2].Width = 130;
            dataGridView.Columns[3].Width = 130;
            dataGridView.Columns[4].Width = 130;
            dataGridView.Columns[5].Width = 130;
            dataGridView.Columns[6].Width = 130;
            dataGridView.Columns[7].Width = 130;
            dataGridView.Columns[8].Width = 130;

            //Market watch
            mydtMarketWatch = new DataTable();
            mydtMarketWatch.Columns.Add("Symbol");
            mydtMarketWatch.Columns.Add("LTP");
            dataGridView1.DataSource = mydtMarketWatch;
            dataGridView1.Columns[0].Width = 595;
            dataGridView1.Columns[1].Width = 595;


            //order book
            mydtOrderBook = new DataTable();
            mydtOrderBook.Columns.Add("Exchange");
            mydtOrderBook.Columns.Add("Symbol");
            mydtOrderBook.Columns.Add("OrderId");
            mydtOrderBook.Columns.Add("Time");
            mydtOrderBook.Columns.Add("Transaction Type");
            mydtOrderBook.Columns.Add("Order Type");
            mydtOrderBook.Columns.Add("Product");
            mydtOrderBook.Columns.Add("Order Price");
            mydtOrderBook.Columns.Add("Trigger Price");
            mydtOrderBook.Columns.Add("Order Quantity");
            mydtOrderBook.Columns.Add("Status");
            mydtOrderBook.Columns.Add("Filled Qty");
            mydtOrderBook.Columns.Add("Filled Price");
            mydtOrderBook.Columns.Add("Cancel Order");
            mydtOrderBook.Columns.Add("Rejection Order Remark");

            OrderBook.DataSource = mydtOrderBook;
            OrderBook.Columns[0].Width = 80;
            OrderBook.Columns[1].Width = 80;
            OrderBook.Columns[2].Width = 80;
            OrderBook.Columns[3].Width = 80;
            OrderBook.Columns[4].Width = 80;
            OrderBook.Columns[5].Width = 80;
            OrderBook.Columns[6].Width = 80;
            OrderBook.Columns[7].Width = 80;
            OrderBook.Columns[8].Width = 80;
            OrderBook.Columns[9].Width = 80;
            OrderBook.Columns[10].Width = 80;
            OrderBook.Columns[11].Width = 75;
            OrderBook.Columns[12].Width = 75;
            OrderBook.Columns[13].Width = 75;
            OrderBook.Columns[14].Width = 75;
        }


        private static readonly object orderbookLock = new object();

        private void _OnTimerExecute(object sender, System.Timers.ElapsedEventArgs e)
        {
            bool m_LoginStatus = m_ObjkiteConnectWrapper.GetConnectionStatus();
            objExecutionForm.ChangeFlag(m_LoginStatus);
            if (m_LoginStatus)
            {
                if (m_futName == "")
                {
                    InsertSymbolsIntoList();
                }
                lock (orderbookLock)
                {
                    LoadWatchList();
                    LoadOpenPositions();
                    LoadOrderBook();
                    LoadLogs();
                    LoadInformationStatus();
                }
            }
        }

        public void LoadInformationStatus()
        {
            string connection = "Not-Connected";
            bool connectionStatus = m_ObjkiteConnectWrapper.GetConnectionStatus();
            if (connectionStatus)
                connection = "Connected";
            UserMargin userMargin = m_ObjkiteConnectWrapper.GetMargin();
            decimal usedMargin = Math.Round(userMargin.Utilised.Debits, 2);
            decimal marginAvailable = Math.Round(userMargin.Net, 2);
            decimal openingBalance = userMargin.Available.Cash;

            
             label7.Invoke((Action)(() => label7.Text = connection));
             label10.Invoke((Action)(() => label10.Text = marginAvailable.ToString()));
             label9.Invoke((Action)(() => label9.Text = usedMargin.ToString()));
             label8.Invoke((Action)(() => label8.Text = openingBalance.ToString()));
        }


        public void LoadLogs()
        {
            try
            {
                Dictionary<int, string> dictionary = new Dictionary<int, string>();
                //m_ObjkiteConnectWrapper.GetExceptionDictionary(out dictionary);
                if (dictionary.Count() > 0)
                {                    
                    foreach (var data in dictionary)
                    {
                        if (!listBox.Items.Contains(data.Value))
                            listBox.Items.Add(data.Value);
                    }
                }
                 
            }
            catch(Exception e)
            {

            }
        }

        public void InsertSymbolsIntoList()
        {
            m_futName = m_ObjkiteConnectWrapper.FetchTableName();
            foreach (var sym in SymbolList)
            {
                if (!m_RealTimeSymbols.ContainsKey(sym.Split('.')[0]))
                    m_RealTimeSymbols.Add(sym.Split('.')[0], sym.Split('.')[1]);
                if (sym.Split('.')[1] == Constants.EXCHANGE_NFO)
                {
                    ListForSymbolName.Add(sym.Split('.')[0] + m_futName);
                }
                else
                {
                    ListForSymbolName.Add(sym.Split('.')[0]);
                }
            }
            label6.Invoke((Action)(() => label6.Text = m_UserID));
            listBox.Items.Add(DateTime.Now + " Login Successful!!");
        }

        public void LoadWatchList()
        {
            try
            {
                foreach (var symbol in m_RealTimeSymbols)
                {
                    string tradingSymbol = symbol.Key;
                    string exchange = symbol.Value;
                    if (exchange == Constants.EXCHANGE_NFO)
                    {
                        tradingSymbol = tradingSymbol + m_futName;
                    }
                    string ltp = m_ObjkiteConnectWrapper.GetLastTradedPrice(tradingSymbol);
                    if (ListForMarketWtchGridView.Contains(tradingSymbol))
                    {
                        foreach (DataRow dr in mydtMarketWatch.Rows)
                        {
                            if ((dr["Symbol"]).ToString() == tradingSymbol)
                            {
                                //WriteUniquelogs(TradingSymbol + " " + userID, "updateIntoDataGridView : Found symbol " + TradingSymbol, MessageType.Informational);
                                dr["LTP"] =  ltp;                               
                               break;
                            }
                        }
                    }
                    else
                    {
                        mydtMarketWatch.Rows.Add(new object[] { tradingSymbol, ltp });
                        ListForMarketWtchGridView.Add(tradingSymbol);
                    }
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "LoadWatchList : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }
        
        public void LoadOrderBook()
        {           
            //try
            {
                //IRDS::Jyoti::08-09-2020::Samco Integration changes
                dynamic Orders = m_ObjkiteConnectWrapper.GetAllOrders();
                //m_ObjkiteConnectWrapper.GetAllOrders(out Orders);
                if(Orders.Count() > 0)
                {
                    foreach(var order in Orders)
                    {
                        double openPrice = m_ObjkiteConnectWrapper.GetOpenPostionPricebyOrderID(order.Tradingsymbol, order.Exchange, order.OrderId);
                        if (!ListForOrderBookGridView.Contains(order.OrderId))
                        {                            
                            mydtOrderBook.Rows.Add(new object[] { order.Exchange, order.Tradingsymbol, order.OrderId, order.OrderTimestamp.ToString(), order.TransactionType, order.OrderType, order.Product, order.AveragePrice.ToString(), order.TriggerPrice.ToString(), order.Quantity.ToString(), order.Status, order.FilledQuantity.ToString(), order.AveragePrice.ToString(), order.CancelledQuantity.ToString(), "" });
                            ListForOrderBookGridView.Add(order.OrderId);
                        }
                        else
                        {                            
                            foreach (DataRow dr in mydtOrderBook.Rows)
                            {
                                if ((dr["OrderId"]).ToString() == order.OrderId)
                                {                                    
                                    //WriteUniquelogs(TradingSymbol + " " + userID, "updateIntoDataGridView : Found symbol " + TradingSymbol, MessageType.Informational);
                                    dr["Exchange"] = order.Exchange;
                                    dr["Time"] = order.OrderTimestamp.ToString();
                                    dr["Symbol"] = order.Tradingsymbol;
                                    dr["Transaction Type"] = order.TransactionType;
                                    dr["Order Type"] = order.OrderType;
                                    dr["Product"] = order.Product;
                                    dr["Order Quantity"] = order.Quantity.ToString();
                                    dr["Order Price"] = order.AveragePrice.ToString();
                                    dr["Trigger Price"] = order.TriggerPrice.ToString();
                                    dr["Status"] = order.Status;
                                    dr["Filled Qty"] = order.FilledQuantity.ToString();
                                    dr["Cancel Order"] = order.CancelledQuantity.ToString();
                                    dr["Filled Price"] =openPrice.ToString();
                                    dr["Rejection Order Remark"] = order.StatusMessage;
                                    break;
                                }
                            }
                        }
                    }
                }

                lblOrders.Invoke((Action)(() => lblOrders.Text = Orders.Count().ToString()));
                lblUpdatedOrder.Invoke((Action)(() => lblUpdatedOrder.Text = DateTime.Now.ToString()));
              

            }
            //catch (Exception e)
            //{
            //    WriteUniquelogs("ExecutionForm" + " " + m_UserID, "LoadOrderBook : Exception Error Message = " + e.Message, MessageType.Exception);
            //}
        }

        private void button1_Click(object sender, EventArgs e)
        {
           // listBoxuserView.Items.Add(DateTime.Now + "  " + "Forcefull login Click");            
            try
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "button1_Click : clicked on forcefully login", MessageType.Informational);
                m_ObjkiteConnectWrapper.ForceFulReLogin();
                listBox.Items.Add(DateTime.Now + "  " + "Forcefully login Successful");
            }
            catch (Exception er)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "button1_Click : Exception Error Message = " + er.Message, MessageType.Exception);
            }
        }

        public void ChangeFlag(bool loginStatus)
        {
            try
            {
                //WriteUniquelogs("ExecutionForm" + " " + m_UserID, "ChangeFlag : To change login status", MessageType.Informational);
                if (loginStatus)
                {                    
                    login.Enabled = false;
                    //listBoxuserView.Items.Add(DateTime.Now + "  " + "Login Status :  Successful");
                   // listBox.Items.Add(DateTime.Now + "  " + "Login Status :  Successful");
                    //this.lblStatus.Text = "Login Status :  Successful";
                    lblStatus.ForeColor = Color.LightGreen;
                    lblStatus.Invoke((Action)(() => lblStatus.Text = "Successful"));
                }
                else if(loginStatus == false)
                {
                    //listBoxuserView.Items.Add(DateTime.Now + "  " + "Login Status : Failure");
                    //listBox.Items.Add(DateTime.Now + "  " + "Login Status : Failure");
                    //this.lblStatus.Text = "Login Status : Failure";
                    lblStatus.ForeColor = Color.Crimson;
                    lblStatus.Invoke((Action)(() => lblStatus.Text = "Failure"));
                }
            }
            catch(Exception e)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "ChangeFlag : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        public void LoadOpenPositions()
        {
            int counter = 0;
            ProfitOrLoss.Clear();
            try
            {
                //WriteUniquelogs("ExecutionForm" + " " + m_UserID, "loadOpenPositions : To load position in data grid view", MessageType.Informational);
                PositionResponse positionResponseForOverall = m_ObjkiteConnectWrapper.GetPositions();
                if (positionResponseForOverall.Net.Count != 0)
                {
                    foreach (var Position in positionResponseForOverall.Net)
                    {
                        //if (ListForSymbolName.Contains(Position.TradingSymbol))
                        {                          
                            string transactionType = "";
                            string price = "";
                            string lastPrice = m_ObjkiteConnectWrapper.GetLastTradedPrice(Position.TradingSymbol);
                            if(lastPrice == "")
                            {
                                lastPrice = Position.LastPrice.ToString();
                            }
                            double p_l = 0;
                            if (Position.Quantity > 0)
                            {
                                transactionType = Constants.TRANSACTION_TYPE_BUY;
                                price = Position.BuyPrice.ToString();
                                double pl = Math.Round((Convert.ToDouble(lastPrice) - Convert.ToDouble(price)) * Position.Quantity, 2);
                                p_l = (pl);//.ToString();
                                ProfitOrLoss.Add(p_l);
                                counter++;
                            }
                            else if (Position.Quantity < 0)
                            {
                                transactionType = Constants.TRANSACTION_TYPE_SELL;
                                price = Position.SellPrice.ToString();
                                double pl = Math.Round((Convert.ToDouble(price) - Convert.ToDouble(lastPrice)) * Math.Abs(Position.Quantity), 2);
                                p_l = (pl);//.ToString();
                                ProfitOrLoss.Add(p_l);
                                //IRDSPM::Pratiksha::16-07-2020::Logs added
                                //listBox.Items.Add(DateTime.Now + "  "+ price);
                                //listBoxuserView.Items.Add(DateTime.Now + "  " + price);
                                counter++;
                            }
                            else
                            {
                                p_l = Math.Round(Convert.ToDouble(Position.PNL),2);
                                ProfitOrLoss.Add(p_l);
                                price = Position.AveragePrice.ToString();
                            }
                            DateTime time;
                            string orderId = "";
                            m_ObjkiteConnectWrapper.GetOrderTimeAndId(Position.TradingSymbol, Position.Exchange,Position.Product, out time, out orderId);
                            double MTM = p_l;// m_ObjkiteConnectWrapper.GetProfitOrLoss(Position.TradingSymbol);
                            string sqrOff = "";
                            //  var filtered = mydt.AsEnumerable().Where(r => r.Field<String>("Symbol").Contains(Position.TradingSymbol));
                            if (!ListForOpenPositionGridView.Contains(Position.TradingSymbol))
                            {
                                addIntoDataGridView(Position.Exchange,Position.TradingSymbol,time.ToString(),orderId,Position.Product,Position.Quantity.ToString(), price,MTM,sqrOff);
                            }
                            else
                            {
                                updateIntoDataGridView(Position.Exchange, Position.TradingSymbol, time.ToString(), orderId, Position.Product, Position.Quantity.ToString(), price, MTM, sqrOff);
                                //listBox.Items.Add(DateTime.Now + "  " + "updateIntoDataGridView");
                                //listBox.Items.Add(DateTime.Now + "  " + "updateIntoDataGridView");
                            }

                        }
                    }
                    int totalOPenPosition = counter;
                    lblOpenPosition.Invoke((Action)(() => lblOpenPosition.Text = totalOPenPosition.ToString()));
                    lblUpdatedTime.Invoke((Action)(() => lblUpdatedTime.Text = DateTime.Now.ToString()));
                    double totalMTM = 0;
                    if (ProfitOrLoss.Count > 0)
                    {
                        totalMTM = ProfitOrLoss.Sum(x => x);
                    }
                    label17.Invoke((Action)(() => label17.Text = totalMTM.ToString()));
                    //lblMTM.Invoke((Action)(() => lblMTM.Text = totalMTM.ToString()));

                }
            }
            catch (Exception er)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "loadOpenPositions : Exception Error Message = " + er.Message, MessageType.Exception);
                //IRDSPM::Pratiksha::16-07-2020::Logs added
                listBox.Items.Add(DateTime.Now + "  loadOpenPositions - " + er.Message);
            }
        }

        public void addIntoDataGridView(string Exchange,string TradingSymbol, string Time,string OrderId,string Product, string Quantity, string Price,double MTM,string SqrOff)
        {            
            //try
            {
                mydtOpenPosition.Rows.Add(new object[] { Exchange, TradingSymbol, Time, OrderId, Product , Quantity , Price , MTM , SqrOff });
                ListForOpenPositionGridView.Add(TradingSymbol); 
            }
            //catch (Exception e)
            //{
            //    WriteUniquelogs("ExecutionForm" + " " + m_UserID, "addIntoDataGridView : Exception Error Message = " + e.Message, MessageType.Exception);
            //    //IRDSPM::Pratiksha::16-07-2020::Logs added
            //    listBox.Items.Add(DateTime.Now + "  " + e.Message);
            //}
        }

        public void updateIntoDataGridView(string Exchange,string TradingSymbol, string Time,string OrderId, string Product, string Quantity, string Price,double MTM,string SqrOff)
        {           
            //try
            {
                foreach (DataRow dr in mydtOpenPosition.Rows)
                {
                    if ((dr["Symbol"]).ToString() == TradingSymbol)
                    {
                        //WriteUniquelogs(TradingSymbol + " " + userID, "updateIntoDataGridView : Found symbol " + TradingSymbol, MessageType.Informational);
                        dr["Exchange"] = Exchange;
                        dr["Time"] = Time;
                        dr["OrderId"] = OrderId;
                        dr["Product"] = Product;
                        dr["Quantity"] = Quantity;
                        dr["Price"] = Price;
                        dr["MTM"] = MTM;
                        dr["SqrOff"] = SqrOff;
                        break;
                    }
                }
            }
            //catch (Exception e)
            //{
            //    WriteUniquelogs("ExecutionForm" + " " + m_UserID, "updateIntoDataGridView : Exception Error Message = " + e.Message, MessageType.Exception);
            //}
        }
        

        private void btnDownloadInstrument_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                m_ObjkiteConnectWrapper.IntrumentsCSVToDB();
                MessageBox.Show("Downloaded successfully");
            }
            catch(Exception er)
            {
                MessageBox.Show("Not able to Downloaded");
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "btnDownloadInstrument_Click : Exception Error Message = " + er.Message, MessageType.Exception);
            }
            Cursor.Current = Cursors.Default;
        }

        private void btnRollOver_Click(object sender, EventArgs e)
        {
            btnRollOver.Enabled = false;
            try
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "btnRollOver_Click : clicked on Roll over button", MessageType.Informational);
                DialogResult dialogResult = MessageBox.Show("Are you sure to Roll over Banknifty? ", "Roll over", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    m_ObjkiteConnectWrapper.RollOver("BANKNIFTY", "NFO", "NRML");
                    MessageBox.Show("Roll Over process finish!");
                    listBox.Items.Add(DateTime.Now + "  " + "Roll Over process finish");
                }
                else if (dialogResult == DialogResult.No)
                {
                    //do something else
                }
            }
            catch(Exception er)
            {
                WriteUniquelogs("ExecutionForm" + " " + m_UserID, "btnRollOver_Click : Exception Error Message = " + er.Message, MessageType.Exception);
                //IRDSPM::Pratiksha::16-07-2020::Logs added
                listBox.Items.Add(DateTime.Now + "  RollOver " + er.Message);
            }
            finally
            {
                btnRollOver.Enabled = true;
            }
        }

        public void WriteUniquelogs(string fileName, string message, MessageType msgType)
        {
            try
            {
                SymbolLogs log = new SymbolLogs();
                //log = SymbolLogs.InstanceSymbolLogs;
                log.createFile(fileName);
                log.LogMessage(message, MessageType.Informational);
                log = null;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : " + e.Message);               
            }
        }

        private void oROMSToolStripMenuItem_Click_1(object sender, EventArgs e)
        {            
            IROMS ironsForms = new IROMS();
            ironsForms.Show();
        }

        private void symbolSelectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AlgoOMS signalProcessor = new AlgoOMS();
            SymbolSetting symbolForm = new SymbolSetting(signalProcessor);
            symbolForm.Show();
        }
        private void generalSettingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            generalSetting genralForm = new generalSetting();
            genralForm.Show();
        }

        //IRDSPM::Pratiksha::17-07-2020::Added registration form
        private void registerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //IRDSPM::Pratiksha::18-09-2020::Added new form
            //RegistrationForm RegForm = new RegistrationForm();
            //RegForm.Show();
            //RegistrationWindowForm regformObj = new RegistrationWindowForm(m_ObjkiteConnectWrapper, title);
            //regformObj.ShowDialog();
        }
        
        public void CancelAllOrders()
        {
            m_ObjkiteConnectWrapper.CancelAllPendingOrder();
        }

        public void CloseAllOrder()
        {
            m_ObjkiteConnectWrapper.CloseAllOpenOrders();
        }
        
         private void login_Click(object sender, EventArgs e)
        {
            listBoxuserView.Items.Add(DateTime.Now + "  Login Clicked");
        }

        private void login_Click_1(object sender, EventArgs e)
        {
            m_ObjkiteConnectWrapper.Connect();
        }

        private void cancel_Click(object sender, EventArgs e)
        {
            //CancelAllOrders();
        }

        private void close_Click(object sender, EventArgs e)
        {
            //CloseAllOrder();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //CancelAllOrders();
        }

        private void ExecutionForm_Load(object sender, EventArgs e)
        {
            //MainWindow mainForm = new MainWindow();
            //mainForm.loadValues(m_ObjkiteConnectWrapper, m_UserID, SymbolList);
            //mainForm.Show();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            m_ObjkiteConnectWrapper.ForceFulReLogin();
        }

        private void btnRollOver_Click_1(object sender, EventArgs e)
        {

        }
    }
}
