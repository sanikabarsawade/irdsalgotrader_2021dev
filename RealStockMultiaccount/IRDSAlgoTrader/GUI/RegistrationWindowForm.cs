﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IRDSAlgoOMS.GUI
{
    public partial class RegistrationWindowForm : Form
    {
        //dynamic m_WrapperObject = null;
        public RegistrationWindowForm(dynamic wrapperObject, string title, double trialperiod1, string token)
        {
            this.TopMost = true;
            //double trialPeriod = Convert.ToDouble(wrapperObject.GetTrialPeriod());
            double trialPeriod = Convert.ToDouble(trialperiod1);
            InitializeComponent(this, trialPeriod, title, token);
            //sanika::1-oct-2020::Set value of object
            //this.registrationWPFForm1.m_WrapperObject = wrapperObject;
            //this.registrationWPFForm1.trialPeriod = Convert.ToDouble(wrapperObject.GetTrialPeriod());
            this.Load += RegistrationWindowForm_Load;
        }

        private void RegistrationWindowForm_Load(object sender, EventArgs e)
        {
            this.TopMost = false;
        }

        //sanika::1-oct-2020::add method to write value in ini file
        public void writeInINIFile(string userid,double getExpiryDate)
        {
            this.registrationWPFForm1.WritingintoINI(userid, getExpiryDate);
        }
    }
}
