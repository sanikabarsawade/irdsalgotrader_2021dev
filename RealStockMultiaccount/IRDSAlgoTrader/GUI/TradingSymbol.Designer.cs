﻿namespace IRDSAlgoOMS
{
    partial class TradingSymbol
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TradingSymbol));
            this.button1 = new System.Windows.Forms.Button();
            this.set = new System.Windows.Forms.Button();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.lblCpl = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.endTimePicker = new System.Windows.Forms.DateTimePicker();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.endTime = new System.Windows.Forms.Label();
            this.startTimeMCXTimePicker = new System.Windows.Forms.DateTimePicker();
            this.startTimeNFOTimePicker = new System.Windows.Forms.DateTimePicker();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.startTimeNFO = new System.Windows.Forms.Label();
            this.startTimeMCX = new System.Windows.Forms.Label();
            this.TOEOTimePicker = new System.Windows.Forms.DateTimePicker();
            this.TTPOTimePicker = new System.Windows.Forms.DateTimePicker();
            this.startTimePicker = new System.Windows.Forms.DateTimePicker();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.txtProfit = new System.Windows.Forms.TextBox();
            this.overallLoss = new System.Windows.Forms.Label();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.txtLoss = new System.Windows.Forms.TextBox();
            this.TimeLimitToPlaceOrder = new System.Windows.Forms.Label();
            this.overallProfit = new System.Windows.Forms.Label();
            this.TimeForExitAllOpenPosition = new System.Windows.Forms.Label();
            this.startTime = new System.Windows.Forms.Label();
            this.hdimdTrue = new System.Windows.Forms.RadioButton();
            this.hdimdFalse = new System.Windows.Forms.RadioButton();
            this.sdcTrue = new System.Windows.Forms.RadioButton();
            this.sdcFalse = new System.Windows.Forms.RadioButton();
            this.sdisdTrue = new System.Windows.Forms.RadioButton();
            this.sdisdFalse = new System.Windows.Forms.RadioButton();
            this.sdimdTrue = new System.Windows.Forms.RadioButton();
            this.sdimdFalse = new System.Windows.Forms.RadioButton();
            this.cplTrue = new System.Windows.Forms.RadioButton();
            this.cplFalse = new System.Windows.Forms.RadioButton();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(190)))), ((int)(((byte)(230)))), ((int)(((byte)(253)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.Black;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(459, 440);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(120, 33);
            this.button1.TabIndex = 91;
            this.button1.Text = "Cancel";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // set
            // 
            this.set.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.set.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(190)))), ((int)(((byte)(230)))), ((int)(((byte)(253)))));
            this.set.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.set.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.set.ForeColor = System.Drawing.Color.Black;
            this.set.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.set.Location = new System.Drawing.Point(241, 440);
            this.set.Name = "set";
            this.set.Size = new System.Drawing.Size(120, 33);
            this.set.TabIndex = 89;
            this.set.Text = "Apply";
            this.set.UseVisualStyleBackColor = false;
            this.set.Click += new System.EventHandler(this.set_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(156)))), ((int)(((byte)(209)))));
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.lblCpl);
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.endTimePicker);
            this.groupBox1.Controls.Add(this.pictureBox7);
            this.groupBox1.Controls.Add(this.endTime);
            this.groupBox1.Controls.Add(this.startTimeMCXTimePicker);
            this.groupBox1.Controls.Add(this.startTimeNFOTimePicker);
            this.groupBox1.Controls.Add(this.pictureBox4);
            this.groupBox1.Controls.Add(this.pictureBox6);
            this.groupBox1.Controls.Add(this.startTimeNFO);
            this.groupBox1.Controls.Add(this.startTimeMCX);
            this.groupBox1.Controls.Add(this.TOEOTimePicker);
            this.groupBox1.Controls.Add(this.TTPOTimePicker);
            this.groupBox1.Controls.Add(this.startTimePicker);
            this.groupBox1.Controls.Add(this.pictureBox5);
            this.groupBox1.Controls.Add(this.pictureBox10);
            this.groupBox1.Controls.Add(this.pictureBox3);
            this.groupBox1.Controls.Add(this.pictureBox11);
            this.groupBox1.Controls.Add(this.txtProfit);
            this.groupBox1.Controls.Add(this.overallLoss);
            this.groupBox1.Controls.Add(this.pictureBox9);
            this.groupBox1.Controls.Add(this.txtLoss);
            this.groupBox1.Controls.Add(this.TimeLimitToPlaceOrder);
            this.groupBox1.Controls.Add(this.overallProfit);
            this.groupBox1.Controls.Add(this.TimeForExitAllOpenPosition);
            this.groupBox1.Controls.Add(this.startTime);
            this.groupBox1.Font = new System.Drawing.Font("Arial", 10F);
            this.groupBox1.Location = new System.Drawing.Point(4, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(591, 422);
            this.groupBox1.TabIndex = 88;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Trading Details";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.radioButton2);
            this.groupBox3.Controls.Add(this.radioButton1);
            this.groupBox3.Location = new System.Drawing.Point(298, 275);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(276, 46);
            this.groupBox3.TabIndex = 144;
            this.groupBox3.TabStop = false;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(132, 18);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(56, 20);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "false";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(18, 18);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(51, 20);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "true";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // lblCpl
            // 
            this.lblCpl.AutoSize = true;
            this.lblCpl.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Bold);
            this.lblCpl.Location = new System.Drawing.Point(51, 295);
            this.lblCpl.Name = "lblCpl";
            this.lblCpl.Size = new System.Drawing.Size(155, 18);
            this.lblCpl.TabIndex = 143;
            this.lblCpl.Text = "Calculate Profit Loss";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pictureBox1.Location = new System.Drawing.Point(16, 289);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(29, 27);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 142;
            this.pictureBox1.TabStop = false;
            // 
            // endTimePicker
            // 
            this.endTimePicker.CalendarFont = new System.Drawing.Font("Arial", 11.25F);
            this.endTimePicker.CustomFormat = "HH:mm:ss";
            this.endTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.endTimePicker.Location = new System.Drawing.Point(299, 250);
            this.endTimePicker.Name = "endTimePicker";
            this.endTimePicker.ShowUpDown = true;
            this.endTimePicker.Size = new System.Drawing.Size(277, 23);
            this.endTimePicker.TabIndex = 141;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox7.Image")));
            this.pictureBox7.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pictureBox7.Location = new System.Drawing.Point(16, 247);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(29, 27);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox7.TabIndex = 140;
            this.pictureBox7.TabStop = false;
            // 
            // endTime
            // 
            this.endTime.AutoSize = true;
            this.endTime.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Bold);
            this.endTime.ForeColor = System.Drawing.Color.Black;
            this.endTime.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.endTime.Location = new System.Drawing.Point(50, 252);
            this.endTime.Name = "endTime";
            this.endTime.Size = new System.Drawing.Size(75, 18);
            this.endTime.TabIndex = 139;
            this.endTime.Text = "End Time";
            // 
            // startTimeMCXTimePicker
            // 
            this.startTimeMCXTimePicker.CalendarFont = new System.Drawing.Font("Arial", 11.25F);
            this.startTimeMCXTimePicker.CustomFormat = "HH:mm:ss";
            this.startTimeMCXTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.startTimeMCXTimePicker.Location = new System.Drawing.Point(297, 127);
            this.startTimeMCXTimePicker.Name = "startTimeMCXTimePicker";
            this.startTimeMCXTimePicker.ShowUpDown = true;
            this.startTimeMCXTimePicker.Size = new System.Drawing.Size(277, 23);
            this.startTimeMCXTimePicker.TabIndex = 138;
            // 
            // startTimeNFOTimePicker
            // 
            this.startTimeNFOTimePicker.CalendarFont = new System.Drawing.Font("Arial", 11.25F);
            this.startTimeNFOTimePicker.CustomFormat = "HH:mm:ss";
            this.startTimeNFOTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.startTimeNFOTimePicker.Location = new System.Drawing.Point(297, 86);
            this.startTimeNFOTimePicker.Name = "startTimeNFOTimePicker";
            this.startTimeNFOTimePicker.ShowUpDown = true;
            this.startTimeNFOTimePicker.Size = new System.Drawing.Size(277, 23);
            this.startTimeNFOTimePicker.TabIndex = 137;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pictureBox4.Location = new System.Drawing.Point(14, 124);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(29, 27);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 136;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pictureBox6.Location = new System.Drawing.Point(14, 82);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(29, 27);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox6.TabIndex = 135;
            this.pictureBox6.TabStop = false;
            // 
            // startTimeNFO
            // 
            this.startTimeNFO.AutoSize = true;
            this.startTimeNFO.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Bold);
            this.startTimeNFO.ForeColor = System.Drawing.Color.Black;
            this.startTimeNFO.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.startTimeNFO.Location = new System.Drawing.Point(48, 87);
            this.startTimeNFO.Name = "startTimeNFO";
            this.startTimeNFO.Size = new System.Drawing.Size(117, 18);
            this.startTimeNFO.TabIndex = 134;
            this.startTimeNFO.Text = "Start Time NFO";
            // 
            // startTimeMCX
            // 
            this.startTimeMCX.AutoSize = true;
            this.startTimeMCX.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Bold);
            this.startTimeMCX.ForeColor = System.Drawing.Color.Black;
            this.startTimeMCX.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.startTimeMCX.Location = new System.Drawing.Point(48, 129);
            this.startTimeMCX.Name = "startTimeMCX";
            this.startTimeMCX.Size = new System.Drawing.Size(119, 18);
            this.startTimeMCX.TabIndex = 133;
            this.startTimeMCX.Text = "Start Time MCX";
            // 
            // TOEOTimePicker
            // 
            this.TOEOTimePicker.CalendarFont = new System.Drawing.Font("Arial", 11.25F);
            this.TOEOTimePicker.CustomFormat = "HH:mm:ss";
            this.TOEOTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.TOEOTimePicker.Location = new System.Drawing.Point(298, 208);
            this.TOEOTimePicker.Name = "TOEOTimePicker";
            this.TOEOTimePicker.ShowUpDown = true;
            this.TOEOTimePicker.Size = new System.Drawing.Size(277, 23);
            this.TOEOTimePicker.TabIndex = 116;
            // 
            // TTPOTimePicker
            // 
            this.TTPOTimePicker.CalendarFont = new System.Drawing.Font("Arial", 11.25F);
            this.TTPOTimePicker.CustomFormat = "HH:mm:ss";
            this.TTPOTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.TTPOTimePicker.Location = new System.Drawing.Point(298, 167);
            this.TTPOTimePicker.Name = "TTPOTimePicker";
            this.TTPOTimePicker.ShowUpDown = true;
            this.TTPOTimePicker.Size = new System.Drawing.Size(277, 23);
            this.TTPOTimePicker.TabIndex = 115;
            // 
            // startTimePicker
            // 
            this.startTimePicker.CalendarFont = new System.Drawing.Font("Arial", 11.25F);
            this.startTimePicker.CustomFormat = "HH:mm:ss";
            this.startTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.startTimePicker.Location = new System.Drawing.Point(298, 40);
            this.startTimePicker.Name = "startTimePicker";
            this.startTimePicker.ShowUpDown = true;
            this.startTimePicker.Size = new System.Drawing.Size(277, 23);
            this.startTimePicker.TabIndex = 114;
            this.startTimePicker.Value = new System.DateTime(2020, 7, 30, 11, 51, 0, 0);
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pictureBox5.Location = new System.Drawing.Point(14, 373);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(29, 27);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox5.TabIndex = 113;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox10.Image")));
            this.pictureBox10.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pictureBox10.Location = new System.Drawing.Point(15, 205);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(29, 27);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox10.TabIndex = 111;
            this.pictureBox10.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pictureBox3.Location = new System.Drawing.Point(14, 332);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(29, 27);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 112;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox11.Image")));
            this.pictureBox11.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pictureBox11.Location = new System.Drawing.Point(15, 163);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(29, 27);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox11.TabIndex = 110;
            this.pictureBox11.TabStop = false;
            // 
            // txtProfit
            // 
            this.txtProfit.BackColor = System.Drawing.Color.White;
            this.txtProfit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtProfit.Font = new System.Drawing.Font("Arial", 11.25F);
            this.txtProfit.Location = new System.Drawing.Point(297, 331);
            this.txtProfit.Name = "txtProfit";
            this.txtProfit.Size = new System.Drawing.Size(278, 25);
            this.txtProfit.TabIndex = 111;
            // 
            // overallLoss
            // 
            this.overallLoss.AutoSize = true;
            this.overallLoss.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold);
            this.overallLoss.ForeColor = System.Drawing.Color.Black;
            this.overallLoss.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.overallLoss.Location = new System.Drawing.Point(48, 378);
            this.overallLoss.Name = "overallLoss";
            this.overallLoss.Size = new System.Drawing.Size(98, 18);
            this.overallLoss.TabIndex = 110;
            this.overallLoss.Text = "Overall Loss";
            // 
            // pictureBox9
            // 
            this.pictureBox9.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox9.Image")));
            this.pictureBox9.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pictureBox9.Location = new System.Drawing.Point(14, 40);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(29, 27);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox9.TabIndex = 108;
            this.pictureBox9.TabStop = false;
            // 
            // txtLoss
            // 
            this.txtLoss.BackColor = System.Drawing.Color.White;
            this.txtLoss.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLoss.Font = new System.Drawing.Font("Arial", 11.25F);
            this.txtLoss.Location = new System.Drawing.Point(297, 372);
            this.txtLoss.Name = "txtLoss";
            this.txtLoss.Size = new System.Drawing.Size(278, 25);
            this.txtLoss.TabIndex = 109;
            // 
            // TimeLimitToPlaceOrder
            // 
            this.TimeLimitToPlaceOrder.AutoSize = true;
            this.TimeLimitToPlaceOrder.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Bold);
            this.TimeLimitToPlaceOrder.ForeColor = System.Drawing.Color.Black;
            this.TimeLimitToPlaceOrder.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.TimeLimitToPlaceOrder.Location = new System.Drawing.Point(49, 168);
            this.TimeLimitToPlaceOrder.Name = "TimeLimitToPlaceOrder";
            this.TimeLimitToPlaceOrder.Size = new System.Drawing.Size(192, 18);
            this.TimeLimitToPlaceOrder.TabIndex = 105;
            this.TimeLimitToPlaceOrder.Text = "Time Limit To Place Order";
            // 
            // overallProfit
            // 
            this.overallProfit.AutoSize = true;
            this.overallProfit.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold);
            this.overallProfit.ForeColor = System.Drawing.Color.Black;
            this.overallProfit.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.overallProfit.Location = new System.Drawing.Point(48, 334);
            this.overallProfit.Name = "overallProfit";
            this.overallProfit.Size = new System.Drawing.Size(103, 18);
            this.overallProfit.TabIndex = 108;
            this.overallProfit.Text = "Overall Profit";
            // 
            // TimeForExitAllOpenPosition
            // 
            this.TimeForExitAllOpenPosition.AutoSize = true;
            this.TimeForExitAllOpenPosition.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Bold);
            this.TimeForExitAllOpenPosition.ForeColor = System.Drawing.Color.Black;
            this.TimeForExitAllOpenPosition.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.TimeForExitAllOpenPosition.Location = new System.Drawing.Point(49, 210);
            this.TimeForExitAllOpenPosition.Name = "TimeForExitAllOpenPosition";
            this.TimeForExitAllOpenPosition.Size = new System.Drawing.Size(227, 18);
            this.TimeForExitAllOpenPosition.TabIndex = 103;
            this.TimeForExitAllOpenPosition.Text = "Time For Exit All Open Position";
            // 
            // startTime
            // 
            this.startTime.AutoSize = true;
            this.startTime.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold);
            this.startTime.ForeColor = System.Drawing.Color.Black;
            this.startTime.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.startTime.Location = new System.Drawing.Point(48, 45);
            this.startTime.Name = "startTime";
            this.startTime.Size = new System.Drawing.Size(81, 18);
            this.startTime.TabIndex = 98;
            this.startTime.Text = "Start Time";
            // 
            // hdimdTrue
            // 
            this.hdimdTrue.AutoSize = true;
            this.hdimdTrue.Font = new System.Drawing.Font("Arial", 11F);
            this.hdimdTrue.ForeColor = System.Drawing.Color.Black;
            this.hdimdTrue.Location = new System.Drawing.Point(41, 13);
            this.hdimdTrue.Name = "hdimdTrue";
            this.hdimdTrue.Size = new System.Drawing.Size(51, 21);
            this.hdimdTrue.TabIndex = 71;
            this.hdimdTrue.TabStop = true;
            this.hdimdTrue.Text = "true";
            this.hdimdTrue.UseVisualStyleBackColor = true;
            // 
            // hdimdFalse
            // 
            this.hdimdFalse.AutoSize = true;
            this.hdimdFalse.Font = new System.Drawing.Font("Arial", 11F);
            this.hdimdFalse.ForeColor = System.Drawing.Color.Black;
            this.hdimdFalse.Location = new System.Drawing.Point(189, 13);
            this.hdimdFalse.Name = "hdimdFalse";
            this.hdimdFalse.Size = new System.Drawing.Size(57, 21);
            this.hdimdFalse.TabIndex = 72;
            this.hdimdFalse.TabStop = true;
            this.hdimdFalse.Text = "false";
            this.hdimdFalse.UseVisualStyleBackColor = true;
            // 
            // sdcTrue
            // 
            this.sdcTrue.AutoSize = true;
            this.sdcTrue.Font = new System.Drawing.Font("Arial", 11F);
            this.sdcTrue.ForeColor = System.Drawing.Color.Black;
            this.sdcTrue.Location = new System.Drawing.Point(41, 13);
            this.sdcTrue.Name = "sdcTrue";
            this.sdcTrue.Size = new System.Drawing.Size(51, 21);
            this.sdcTrue.TabIndex = 71;
            this.sdcTrue.TabStop = true;
            this.sdcTrue.Text = "true";
            this.sdcTrue.UseVisualStyleBackColor = true;
            // 
            // sdcFalse
            // 
            this.sdcFalse.AutoSize = true;
            this.sdcFalse.Font = new System.Drawing.Font("Arial", 11F);
            this.sdcFalse.ForeColor = System.Drawing.Color.Black;
            this.sdcFalse.Location = new System.Drawing.Point(189, 13);
            this.sdcFalse.Name = "sdcFalse";
            this.sdcFalse.Size = new System.Drawing.Size(57, 21);
            this.sdcFalse.TabIndex = 72;
            this.sdcFalse.TabStop = true;
            this.sdcFalse.Text = "false";
            this.sdcFalse.UseVisualStyleBackColor = true;
            // 
            // sdisdTrue
            // 
            this.sdisdTrue.AutoSize = true;
            this.sdisdTrue.Font = new System.Drawing.Font("Arial", 11F);
            this.sdisdTrue.ForeColor = System.Drawing.Color.Black;
            this.sdisdTrue.Location = new System.Drawing.Point(41, 10);
            this.sdisdTrue.Name = "sdisdTrue";
            this.sdisdTrue.Size = new System.Drawing.Size(51, 21);
            this.sdisdTrue.TabIndex = 69;
            this.sdisdTrue.TabStop = true;
            this.sdisdTrue.Text = "true";
            this.sdisdTrue.UseVisualStyleBackColor = true;
            // 
            // sdisdFalse
            // 
            this.sdisdFalse.AutoSize = true;
            this.sdisdFalse.Font = new System.Drawing.Font("Arial", 11F);
            this.sdisdFalse.ForeColor = System.Drawing.Color.Black;
            this.sdisdFalse.Location = new System.Drawing.Point(189, 10);
            this.sdisdFalse.Name = "sdisdFalse";
            this.sdisdFalse.Size = new System.Drawing.Size(57, 21);
            this.sdisdFalse.TabIndex = 70;
            this.sdisdFalse.TabStop = true;
            this.sdisdFalse.Text = "false";
            this.sdisdFalse.UseVisualStyleBackColor = true;
            // 
            // sdimdTrue
            // 
            this.sdimdTrue.AutoSize = true;
            this.sdimdTrue.Font = new System.Drawing.Font("Arial", 11F);
            this.sdimdTrue.ForeColor = System.Drawing.Color.Black;
            this.sdimdTrue.Location = new System.Drawing.Point(41, 13);
            this.sdimdTrue.Name = "sdimdTrue";
            this.sdimdTrue.Size = new System.Drawing.Size(51, 21);
            this.sdimdTrue.TabIndex = 71;
            this.sdimdTrue.TabStop = true;
            this.sdimdTrue.Text = "true";
            this.sdimdTrue.UseVisualStyleBackColor = true;
            // 
            // sdimdFalse
            // 
            this.sdimdFalse.AutoSize = true;
            this.sdimdFalse.Font = new System.Drawing.Font("Arial", 11F);
            this.sdimdFalse.ForeColor = System.Drawing.Color.Black;
            this.sdimdFalse.Location = new System.Drawing.Point(189, 13);
            this.sdimdFalse.Name = "sdimdFalse";
            this.sdimdFalse.Size = new System.Drawing.Size(57, 21);
            this.sdimdFalse.TabIndex = 72;
            this.sdimdFalse.TabStop = true;
            this.sdimdFalse.Text = "false";
            this.sdimdFalse.UseVisualStyleBackColor = true;
            // 
            // cplTrue
            // 
            this.cplTrue.AutoSize = true;
            this.cplTrue.Font = new System.Drawing.Font("Arial", 11F);
            this.cplTrue.ForeColor = System.Drawing.Color.Black;
            this.cplTrue.Location = new System.Drawing.Point(41, 10);
            this.cplTrue.Name = "cplTrue";
            this.cplTrue.Size = new System.Drawing.Size(51, 21);
            this.cplTrue.TabIndex = 69;
            this.cplTrue.TabStop = true;
            this.cplTrue.Text = "true";
            this.cplTrue.UseVisualStyleBackColor = true;
            // 
            // cplFalse
            // 
            this.cplFalse.AutoSize = true;
            this.cplFalse.Font = new System.Drawing.Font("Arial", 11F);
            this.cplFalse.ForeColor = System.Drawing.Color.Black;
            this.cplFalse.Location = new System.Drawing.Point(189, 10);
            this.cplFalse.Name = "cplFalse";
            this.cplFalse.Size = new System.Drawing.Size(57, 21);
            this.cplFalse.TabIndex = 70;
            this.cplFalse.TabStop = true;
            this.cplFalse.Text = "false";
            this.cplFalse.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(190)))), ((int)(((byte)(230)))), ((int)(((byte)(253)))));
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.Black;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(18, 440);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(120, 33);
            this.button2.TabIndex = 92;
            this.button2.Text = "Clear";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // TradingSymbol
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(156)))), ((int)(((byte)(209)))));
            this.ClientSize = new System.Drawing.Size(606, 480);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.set);
            this.Controls.Add(this.groupBox1);
            this.MaximizeBox = false;
            this.Name = "TradingSymbol";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Trading Setting";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button set;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.Label TimeLimitToPlaceOrder;
        private System.Windows.Forms.Label TimeForExitAllOpenPosition;
        private System.Windows.Forms.Label startTime;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.TextBox txtProfit;
        private System.Windows.Forms.Label overallLoss;
        private System.Windows.Forms.TextBox txtLoss;
        private System.Windows.Forms.Label overallProfit;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DateTimePicker startTimePicker;
        private System.Windows.Forms.DateTimePicker TOEOTimePicker;
        private System.Windows.Forms.DateTimePicker TTPOTimePicker;
      //  private System.Windows.Forms.PictureBox pictureBox14;
       // private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.RadioButton sdcTrue;
        private System.Windows.Forms.RadioButton sdcFalse;
        //private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.RadioButton sdisdTrue;
        private System.Windows.Forms.RadioButton sdisdFalse;
        //private System.Windows.Forms.PictureBox pictureBox13;
       // private System.Windows.Forms.Label storeDataCSV;
       // private System.Windows.Forms.Label storeDataSqliteDB;
       // private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RadioButton sdimdTrue;
        private System.Windows.Forms.RadioButton sdimdFalse;
       // private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.RadioButton cplTrue;
        private System.Windows.Forms.RadioButton cplFalse;
       // private System.Windows.Forms.PictureBox pictureBox1;
       // private System.Windows.Forms.Label storeDataMysqlDB;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.Label calculateProfitLoss;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton hdimdTrue;
        private System.Windows.Forms.RadioButton hdimdFalse;
       // private System.Windows.Forms.PictureBox pictureBox2;
        //private System.Windows.Forms.Label historicalDataInMysqlDB;
        private System.Windows.Forms.DateTimePicker endTimePicker;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.Label endTime;
        private System.Windows.Forms.DateTimePicker startTimeMCXTimePicker;
        private System.Windows.Forms.DateTimePicker startTimeNFOTimePicker;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Label startTimeNFO;
        private System.Windows.Forms.Label startTimeMCX;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblCpl;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
    }
}