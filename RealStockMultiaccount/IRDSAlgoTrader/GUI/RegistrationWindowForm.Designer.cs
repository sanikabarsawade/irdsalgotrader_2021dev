﻿using System;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

namespace IRDSAlgoOMS.GUI
{
    partial class RegistrationWindowForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /// 
        string m_title = "";
        //IRDSPM::Pratiksha::12-02-2021::dynamically set icon
        Assembly myAssembly = Assembly.GetExecutingAssembly();
        Stream IconStream = null;
        private void InitializeComponent(RegistrationWindowForm obj,double trialDays, string title, string token)
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.elementHost1 = new System.Windows.Forms.Integration.ElementHost();
            m_title = title;
            this.registrationWPFForm1 = new IRDSAlgoOMS.GUI.RegistrationWPFForm(obj, trialDays, title, token);
            this.SuspendLayout();
            // 
            // elementHost1
            // 
            this.elementHost1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.elementHost1.Location = new System.Drawing.Point(0, 0);
            this.elementHost1.Name = "elementHost1";
            this.elementHost1.Size = new System.Drawing.Size(525, 360);
            this.elementHost1.TabIndex = 0;
            this.elementHost1.Text = "elementHost1";
            this.elementHost1.Child = this.registrationWPFForm1;
            // 
            // RegistrationWindowForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(525, 360);
            this.Controls.Add(this.elementHost1);
            IconStream = myAssembly.GetManifestResourceStream("IRDSAlgoOMS.Resources." + title + ".ico");
            this.Icon = new System.Drawing.Icon(IconStream);
            this.MaximizeBox = false;
            this.Name = "RegistrationWindowForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Registration Form";
            this.FormClosing += RegistrationWindowForm_FormClosing;
            this.ResumeLayout(false);

        }

        private void RegistrationWindowForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.registrationWPFForm1.IsRegisterSuccess)
            { }
            else
            {
                //IRDSPM::Pratiksha::08-02-2021::Dynamic name
                string title = m_title;
                string message = "Are you sure you want to exit from "+ title+"?";
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                DialogResult result = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {
                    Environment.Exit(0);
                }
                else if (result == DialogResult.No)
                {
                    e.Cancel = true;
                }
            }
        }

        #endregion

        private System.Windows.Forms.Integration.ElementHost elementHost1;
        private RegistrationWPFForm registrationWPFForm1;
    }
}