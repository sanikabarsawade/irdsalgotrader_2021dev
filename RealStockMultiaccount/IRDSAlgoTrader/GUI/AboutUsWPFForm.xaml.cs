﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace IRDSAlgoOMS.GUI
{

    public partial class AboutUsWPFForm : System.Windows.Controls.UserControl
    {
        AboutUsWindowsForm m_obj = null;
        dynamic m_ObjkiteConnectWrapper = null;
        SubscriptionDetWindowsForm m_objSubDet = null;
        string m_UserID = "";
        string hardwareId = "";
        string[] Details;
        public AboutUsWPFForm(AboutUsWindowsForm obj, dynamic ObjkiteConnectWrapper, string title)
        {
            InitializeComponent();
            m_obj = obj;
            try
            {
                if(title.ToLower() == "irds algo trader")
                {
                    lblname.Content = title + " 1.0.1(Indian)";
                }
                else
                {
                    lblname.Content = title;
                }

                image.Source = new BitmapImage(new Uri("pack://application:,,,/IRDSAlgoTrader;component/Resources/" + title + ".png"));
                m_ObjkiteConnectWrapper = ObjkiteConnectWrapper;
                //Get hardware id
                hardwareId = License.Status.HardwareID;
                if (m_ObjkiteConnectWrapper != null)
                {
                    string data = "";
                    m_UserID = m_ObjkiteConnectWrapper.GetUserID();
                    data = m_ObjkiteConnectWrapper.FetchUserdetails(m_UserID, hardwareId);
                    if (data != "")//sanika::22-oct-2020::changed condition
                    {
                        //sanika::22-oct-2020::changed splitting type and indexes
                        string[] splittedData = data.Split(',');
                        Details = splittedData;
                        lblUsernameValue.Content = Details[0];
                        lblhwidValue.Content = hardwareId;
                    }
                    else
                    {
                        MessageBox.Show("Unable to load data");
                    }
                }
            }
            catch (Exception ex)
            {
             ///   WriteUniquelogs("settingLogs" + " ", "Subscription Details Setting | Exception : " + ex.Message, MessageType.Informational);
            }
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            m_obj.Close();
        }
        public void WriteUniquelogs(string fileName, string message, MessageType msgType)
        {
            try
            {
                SymbolLogs log = new SymbolLogs();
                log.createFile(fileName);
                log.LogMessage(message, MessageType.Informational);
                log = null;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : " + e.Message);
            }
        }
    }
}
