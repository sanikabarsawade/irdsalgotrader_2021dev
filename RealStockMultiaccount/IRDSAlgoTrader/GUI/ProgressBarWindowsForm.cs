﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IRDSAlgoOMS.GUI
{
    public partial class ProgressBarWindowsForm : Form
    {
        public ProgressBarWindowsForm(string title)
        {
            this.TopMost = true;
            InitializeComponent(this, title);
            this.Load += ProgressBarWindowsForm_Load;
        }

        private void ProgressBarWindowsForm_Load(object sender, EventArgs e)
        {
            this.TopMost = false;
        }

        public void setWorkstate(int counter)
        {
            this.progressBarForm1.setWorkstate(counter);
        }
    }
}
