﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IRDSAlgoOMS.GUI
{
    public partial class SymbolSettingWindowForm : Form
    {
        public SymbolSettingWindowForm(string title)
        {
            this.TopMost = true;
            InitializeComponent(title);
            this.Load += SymbolSettingWindowForm_Load;
        }

        private void SymbolSettingWindowForm_Load(object sender, EventArgs e)
        {
            this.TopMost = false;
        }
    }
}
