﻿namespace IRDSAlgoOMS
{
    partial class generalSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.chooseBtn = new System.Windows.Forms.Button();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.button1 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.fontDialog1 = new System.Windows.Forms.FontDialog();
            this.button2 = new System.Windows.Forms.Button();
            this.colorDialog2 = new System.Windows.Forms.ColorDialog();
            this.button3 = new System.Windows.Forms.Button();
            this.toastNotification = new System.Windows.Forms.Label();
            this.toastAudio = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.TNfalse = new System.Windows.Forms.RadioButton();
            this.TNtrue = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.TAfalse = new System.Windows.Forms.RadioButton();
            this.TAtrue = new System.Windows.Forms.RadioButton();
            this.btnApply = new System.Windows.Forms.Button();
            this.showGUIForOpenPositions = new System.Windows.Forms.Label();
            this.showGUIpibtureBox = new System.Windows.Forms.PictureBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.showGuiFalse = new System.Windows.Forms.RadioButton();
            this.showGuiTrue = new System.Windows.Forms.RadioButton();
            this.ToastAudioPicturebox = new System.Windows.Forms.PictureBox();
            this.ToastnotificationPicturebox = new System.Windows.Forms.PictureBox();
            this.FontsettingPicturebox = new System.Windows.Forms.PictureBox();
            this.FontcolorPicturebox = new System.Windows.Forms.PictureBox();
            this.bgcolorPicturebox = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.showGUIpibtureBox)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ToastAudioPicturebox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ToastnotificationPicturebox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FontsettingPicturebox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FontcolorPicturebox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bgcolorPicturebox)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Location = new System.Drawing.Point(1, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(133, 471);
            this.panel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(40, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 23);
            this.label1.TabIndex = 1;
            this.label1.Text = "General";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(11, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(25, 27);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(154, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 21);
            this.label2.TabIndex = 2;
            this.label2.Text = "Appearance";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(178, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(131, 21);
            this.label3.TabIndex = 3;
            this.label3.Text = "Background color";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(179, 171);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 21);
            this.label4.TabIndex = 4;
            this.label4.Text = "Font setting";
            // 
            // chooseBtn
            // 
            this.chooseBtn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.chooseBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(190)))), ((int)(((byte)(230)))), ((int)(((byte)(253)))));
            this.chooseBtn.Enabled = false;
            this.chooseBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.chooseBtn.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chooseBtn.ForeColor = System.Drawing.Color.Black;
            this.chooseBtn.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.chooseBtn.Location = new System.Drawing.Point(436, 72);
            this.chooseBtn.Name = "chooseBtn";
            this.chooseBtn.Size = new System.Drawing.Size(120, 30);
            this.chooseBtn.TabIndex = 57;
            this.chooseBtn.Text = "Select Color";
            this.chooseBtn.UseVisualStyleBackColor = false;
            this.chooseBtn.Click += new System.EventHandler(this.chooseBtn_Click);
            // 
            // button1
            // 
            this.button1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(190)))), ((int)(((byte)(230)))), ((int)(((byte)(253)))));
            this.button1.Enabled = false;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.Black;
            this.button1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button1.Location = new System.Drawing.Point(436, 121);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(120, 30);
            this.button1.TabIndex = 60;
            this.button1.Text = "Select Color";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(178, 121);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 21);
            this.label5.TabIndex = 59;
            this.label5.Text = "Font color";
            // 
            // button2
            // 
            this.button2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(190)))), ((int)(((byte)(230)))), ((int)(((byte)(253)))));
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.Black;
            this.button2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button2.Location = new System.Drawing.Point(436, 171);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(120, 30);
            this.button2.TabIndex = 61;
            this.button2.Text = "Select font";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.button3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(190)))), ((int)(((byte)(230)))), ((int)(((byte)(253)))));
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold);
            this.button3.Location = new System.Drawing.Point(436, 383);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(120, 30);
            this.button3.TabIndex = 63;
            this.button3.Text = "Cancel";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // toastNotification
            // 
            this.toastNotification.AutoSize = true;
            this.toastNotification.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toastNotification.Location = new System.Drawing.Point(179, 220);
            this.toastNotification.Name = "toastNotification";
            this.toastNotification.Size = new System.Drawing.Size(130, 21);
            this.toastNotification.TabIndex = 64;
            this.toastNotification.Text = "Toast Notification";
            // 
            // toastAudio
            // 
            this.toastAudio.AutoSize = true;
            this.toastAudio.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toastAudio.Location = new System.Drawing.Point(179, 272);
            this.toastAudio.Name = "toastAudio";
            this.toastAudio.Size = new System.Drawing.Size(91, 21);
            this.toastAudio.TabIndex = 65;
            this.toastAudio.Text = "Toast Audio";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.TNfalse);
            this.groupBox1.Controls.Add(this.TNtrue);
            this.groupBox1.Location = new System.Drawing.Point(356, 209);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 44);
            this.groupBox1.TabIndex = 66;
            this.groupBox1.TabStop = false;
            // 
            // TNfalse
            // 
            this.TNfalse.AutoSize = true;
            this.TNfalse.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TNfalse.Location = new System.Drawing.Point(109, 14);
            this.TNfalse.Name = "TNfalse";
            this.TNfalse.Size = new System.Drawing.Size(50, 22);
            this.TNfalse.TabIndex = 1;
            this.TNfalse.TabStop = true;
            this.TNfalse.Text = "OFF";
            this.TNfalse.UseVisualStyleBackColor = true;
            this.TNfalse.Click += new System.EventHandler(this.TNfalse_Click);
            // 
            // TNtrue
            // 
            this.TNtrue.AutoSize = true;
            this.TNtrue.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TNtrue.Location = new System.Drawing.Point(7, 14);
            this.TNtrue.Name = "TNtrue";
            this.TNtrue.Size = new System.Drawing.Size(46, 22);
            this.TNtrue.TabIndex = 0;
            this.TNtrue.TabStop = true;
            this.TNtrue.Text = "ON";
            this.TNtrue.UseVisualStyleBackColor = true;
            this.TNtrue.CheckedChanged += new System.EventHandler(this.TNtrue_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.TAfalse);
            this.groupBox2.Controls.Add(this.TAtrue);
            this.groupBox2.Location = new System.Drawing.Point(356, 261);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 44);
            this.groupBox2.TabIndex = 67;
            this.groupBox2.TabStop = false;
            // 
            // TAfalse
            // 
            this.TAfalse.AutoSize = true;
            this.TAfalse.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TAfalse.Location = new System.Drawing.Point(110, 16);
            this.TAfalse.Name = "TAfalse";
            this.TAfalse.Size = new System.Drawing.Size(50, 22);
            this.TAfalse.TabIndex = 1;
            this.TAfalse.TabStop = true;
            this.TAfalse.Text = "OFF";
            this.TAfalse.UseVisualStyleBackColor = true;
            // 
            // TAtrue
            // 
            this.TAtrue.AutoSize = true;
            this.TAtrue.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TAtrue.Location = new System.Drawing.Point(8, 15);
            this.TAtrue.Name = "TAtrue";
            this.TAtrue.Size = new System.Drawing.Size(46, 22);
            this.TAtrue.TabIndex = 0;
            this.TAtrue.TabStop = true;
            this.TAtrue.Text = "ON";
            this.TAtrue.UseVisualStyleBackColor = true;
            // 
            // btnApply
            // 
            this.btnApply.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnApply.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(190)))), ((int)(((byte)(230)))), ((int)(((byte)(253)))));
            this.btnApply.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnApply.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold);
            this.btnApply.Location = new System.Drawing.Point(287, 383);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(120, 30);
            this.btnApply.TabIndex = 68;
            this.btnApply.Text = "Apply";
            this.btnApply.UseVisualStyleBackColor = false;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // showGUIForOpenPositions
            // 
            this.showGUIForOpenPositions.AutoSize = true;
            this.showGUIForOpenPositions.Font = new System.Drawing.Font("Calibri", 12.75F);
            this.showGUIForOpenPositions.ForeColor = System.Drawing.Color.Black;
            this.showGUIForOpenPositions.Location = new System.Drawing.Point(186, 322);
            this.showGUIForOpenPositions.Name = "showGUIForOpenPositions";
            this.showGUIForOpenPositions.Size = new System.Drawing.Size(78, 21);
            this.showGUIForOpenPositions.TabIndex = 102;
            this.showGUIForOpenPositions.Text = "Show GUI";
            // 
            // showGUIpibtureBox
            // 
            this.showGUIpibtureBox.Image = ((System.Drawing.Image)(resources.GetObject("showGUIpibtureBox.Image")));
            this.showGUIpibtureBox.Location = new System.Drawing.Point(142, 319);
            this.showGUIpibtureBox.Name = "showGUIpibtureBox";
            this.showGUIpibtureBox.Size = new System.Drawing.Size(28, 24);
            this.showGUIpibtureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.showGUIpibtureBox.TabIndex = 103;
            this.showGUIpibtureBox.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.showGuiFalse);
            this.groupBox3.Controls.Add(this.showGuiTrue);
            this.groupBox3.Location = new System.Drawing.Point(356, 311);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(200, 44);
            this.groupBox3.TabIndex = 68;
            this.groupBox3.TabStop = false;
            // 
            // showGuiFalse
            // 
            this.showGuiFalse.AutoSize = true;
            this.showGuiFalse.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.showGuiFalse.Location = new System.Drawing.Point(110, 16);
            this.showGuiFalse.Name = "showGuiFalse";
            this.showGuiFalse.Size = new System.Drawing.Size(50, 22);
            this.showGuiFalse.TabIndex = 1;
            this.showGuiFalse.TabStop = true;
            this.showGuiFalse.Text = "OFF";
            this.showGuiFalse.UseVisualStyleBackColor = true;
            // 
            // showGuiTrue
            // 
            this.showGuiTrue.AutoSize = true;
            this.showGuiTrue.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.showGuiTrue.Location = new System.Drawing.Point(8, 15);
            this.showGuiTrue.Name = "showGuiTrue";
            this.showGuiTrue.Size = new System.Drawing.Size(46, 22);
            this.showGuiTrue.TabIndex = 0;
            this.showGuiTrue.TabStop = true;
            this.showGuiTrue.Text = "ON";
            this.showGuiTrue.UseVisualStyleBackColor = true;
            // 
            // ToastAudioPicturebox
            // 
            this.ToastAudioPicturebox.Image = ((System.Drawing.Image)(resources.GetObject("ToastAudioPicturebox.Image")));
            this.ToastAudioPicturebox.Location = new System.Drawing.Point(140, 269);
            this.ToastAudioPicturebox.Name = "ToastAudioPicturebox";
            this.ToastAudioPicturebox.Size = new System.Drawing.Size(28, 24);
            this.ToastAudioPicturebox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ToastAudioPicturebox.TabIndex = 104;
            this.ToastAudioPicturebox.TabStop = false;
            // 
            // ToastnotificationPicturebox
            // 
            this.ToastnotificationPicturebox.Image = ((System.Drawing.Image)(resources.GetObject("ToastnotificationPicturebox.Image")));
            this.ToastnotificationPicturebox.Location = new System.Drawing.Point(140, 217);
            this.ToastnotificationPicturebox.Name = "ToastnotificationPicturebox";
            this.ToastnotificationPicturebox.Size = new System.Drawing.Size(28, 24);
            this.ToastnotificationPicturebox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ToastnotificationPicturebox.TabIndex = 105;
            this.ToastnotificationPicturebox.TabStop = false;
            // 
            // FontsettingPicturebox
            // 
            this.FontsettingPicturebox.Image = ((System.Drawing.Image)(resources.GetObject("FontsettingPicturebox.Image")));
            this.FontsettingPicturebox.Location = new System.Drawing.Point(140, 168);
            this.FontsettingPicturebox.Name = "FontsettingPicturebox";
            this.FontsettingPicturebox.Size = new System.Drawing.Size(28, 24);
            this.FontsettingPicturebox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.FontsettingPicturebox.TabIndex = 106;
            this.FontsettingPicturebox.TabStop = false;
            // 
            // FontcolorPicturebox
            // 
            this.FontcolorPicturebox.Image = ((System.Drawing.Image)(resources.GetObject("FontcolorPicturebox.Image")));
            this.FontcolorPicturebox.Location = new System.Drawing.Point(140, 118);
            this.FontcolorPicturebox.Name = "FontcolorPicturebox";
            this.FontcolorPicturebox.Size = new System.Drawing.Size(28, 24);
            this.FontcolorPicturebox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.FontcolorPicturebox.TabIndex = 107;
            this.FontcolorPicturebox.TabStop = false;
            // 
            // bgcolorPicturebox
            // 
            this.bgcolorPicturebox.Image = ((System.Drawing.Image)(resources.GetObject("bgcolorPicturebox.Image")));
            this.bgcolorPicturebox.Location = new System.Drawing.Point(140, 69);
            this.bgcolorPicturebox.Name = "bgcolorPicturebox";
            this.bgcolorPicturebox.Size = new System.Drawing.Size(28, 24);
            this.bgcolorPicturebox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bgcolorPicturebox.TabIndex = 108;
            this.bgcolorPicturebox.TabStop = false;
            // 
            // generalSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(156)))), ((int)(((byte)(209)))));
            this.ClientSize = new System.Drawing.Size(629, 438);
            this.Controls.Add(this.bgcolorPicturebox);
            this.Controls.Add(this.FontcolorPicturebox);
            this.Controls.Add(this.FontsettingPicturebox);
            this.Controls.Add(this.ToastnotificationPicturebox);
            this.Controls.Add(this.ToastAudioPicturebox);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.showGUIpibtureBox);
            this.Controls.Add(this.showGUIForOpenPositions);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.toastAudio);
            this.Controls.Add(this.toastNotification);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.chooseBtn);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "generalSetting";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "General Setting";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.showGUIpibtureBox)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ToastAudioPicturebox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ToastnotificationPicturebox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FontsettingPicturebox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FontcolorPicturebox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bgcolorPicturebox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button chooseBtn;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.FontDialog fontDialog1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ColorDialog colorDialog2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label toastNotification;
        private System.Windows.Forms.Label toastAudio;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton TNfalse;
        private System.Windows.Forms.RadioButton TNtrue;
        private System.Windows.Forms.RadioButton TAfalse;
        private System.Windows.Forms.RadioButton TAtrue;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.Label showGUIForOpenPositions;
        private System.Windows.Forms.PictureBox showGUIpibtureBox;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton showGuiFalse;
        private System.Windows.Forms.RadioButton showGuiTrue;
        private System.Windows.Forms.PictureBox ToastAudioPicturebox;
        private System.Windows.Forms.PictureBox ToastnotificationPicturebox;
        private System.Windows.Forms.PictureBox FontsettingPicturebox;
        private System.Windows.Forms.PictureBox FontcolorPicturebox;
        private System.Windows.Forms.PictureBox bgcolorPicturebox;
    }
}