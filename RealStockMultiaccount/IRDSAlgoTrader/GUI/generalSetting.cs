﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IRDSAlgoOMS
{
    public partial class generalSetting : Form
    {
        IROMS iromsObj = new IROMS();
        INIFileForSetting updateSetting = new INIFileForSetting();
        Logger logger = Logger.Instance;
        string iniFileSetting;
        //IRDSPM::Pratiksha::07-07-2020::For setting ini
        string bgcolor = "";
        string fontcolor = "";
        string fontDet = "";
        string fontStyle = "";
        bool toastNotificationVal = false;
        bool toastAudioVal = false;
        bool showGUIVal = false;
        Color bColor;
        Color fColor;
        string title = "";
        ConfigSettings Settings;
        public generalSetting()
        {
            InitializeComponent();
            var path = Directory.GetCurrentDirectory();
            iniFileSetting = path + @"\Configuration\" + "Setting.ini";
            if (File.Exists(iniFileSetting))
            {
                Settings = new ConfigSettings(logger);
                Settings.ReadSettingConfigFile(iniFileSetting);
                bgcolor = Settings.bgcolorname.ToString();
                fontcolor = Settings.fontcolorname.ToString();
                fontDet = Settings.fontdet.ToString();
                fontStyle = Settings.fontstyle.ToString();
                toastNotificationVal = Settings.toastNotification;
                toastAudioVal = Settings.toastAudio;
                showGUIVal = Settings.isShowGUI;
                title = Settings.m_Title;
            }
            string[] colorDetfromINI = { bgcolor, fontcolor };
            for (int i = 0; i < colorDetfromINI.Length; i++)
            {
                Match m = Regex.Match(colorDetfromINI[i], @"A=(?<Alpha>\d+),\s*R=(?<Red>\d+),\s*G=(?<Green>\d+),\s*B=(?<Blue>\d+)");
                if (m.Success)
                {
                    int alpha = int.Parse(m.Groups["Alpha"].Value);
                    int red = int.Parse(m.Groups["Red"].Value);
                    int green = int.Parse(m.Groups["Green"].Value);
                    int blue = int.Parse(m.Groups["Blue"].Value);
                    if (i == 0)
                    {
                        bColor = Color.FromArgb(alpha, red, green, blue);
                    }
                    else
                    {
                        fColor = Color.FromArgb(alpha, red, green, blue);
                    }
                }
                else
                {
                    if (i == 0)
                    {
                        bColor = Color.FromName(colorDetfromINI[i]);
                    }
                    else
                    {
                        fColor = Color.FromName(colorDetfromINI[i]);
                    }
                }
            }
            //IRDSPM::Pratiksha::09-07-2020::Accessing font family and font size
            try
            {
                string fontfamilyAndSize = fontDet.Substring(fontDet.IndexOf("=") + 1, fontDet.IndexOf(",") - 1);
                string fontname = fontfamilyAndSize.Substring(0, fontfamilyAndSize.IndexOf(","));
                int pFrom = fontfamilyAndSize.IndexOf("=") + "=".Length;
                int pTo = fontfamilyAndSize.LastIndexOf(",");
                string result = fontfamilyAndSize.Substring(pFrom, fontfamilyAndSize.Length - pFrom);
                float fontSize = float.Parse(result);
                label2.Font = new Font(fontname, fontSize, FontStyle.Bold, GraphicsUnit.Point);
                label3.Font = new Font(fontname, fontSize, FontStyle.Bold, GraphicsUnit.Point);
                label4.Font = new Font(fontname, fontSize, FontStyle.Bold, GraphicsUnit.Point);
                label5.Font = new Font(fontname, fontSize, FontStyle.Bold, GraphicsUnit.Point);
                showGUIForOpenPositions.Font = new Font(fontname, fontSize, FontStyle.Bold, GraphicsUnit.Point);
                toastAudio.Font = new Font(fontname, fontSize, FontStyle.Bold, GraphicsUnit.Point);
                toastNotification.Font = new Font(fontname, fontSize, FontStyle.Bold, GraphicsUnit.Point);
                TAfalse.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                TAtrue.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                TNfalse.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                TNtrue.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                showGuiFalse.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                showGuiTrue.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
            }
            catch (Exception ex)
            {
                iromsObj.WriteUniquelogs("settingLogs" + " ", "  Exception Error Message = " + ex.Message, MessageType.Exception);
            }

            if (toastAudioVal == true)
            {
                TAtrue.Checked = true;
            }
            else
            {
                TAfalse.Checked = true;
            }
            if (toastNotificationVal == true)
            {
                TNtrue.Checked = true;
                TAtrue.Enabled = true;
                TAfalse.Enabled = true;
            }
            else
            {
                TNfalse.Checked = true;
                TAtrue.Enabled = false;
                TAfalse.Enabled = false;
            }

            if (showGUIVal == true)
            {
                showGuiTrue.Checked = true;
            }
            else
            {
                showGuiFalse.Checked = true;
            }
            iromsObj.WriteUniquelogs("settingLogs" + " ", " Changes from settings.ini file updated on dgeneral settings.", MessageType.Informational);
        }

        private void chooseBtn_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {

                Match m = Regex.Match(colorDialog1.Color.ToString(), @"A=(?<Alpha>\d+),\s*R=(?<Red>\d+),\s*G=(?<Green>\d+),\s*B=(?<Blue>\d+)");
                if (!m.Success)
                {
                    string selectedColor = colorDialog1.Color.ToString();
                    string newString = selectedColor.Substring(selectedColor.IndexOf('[') + 1);
                    string Selectedcolorname = newString.Substring(0, newString.IndexOf(']'));

                    updateSetting.IniWriteValueupdateColor("GENERALSETTING", "bgcolor", Selectedcolorname, iniFileSetting);
                    if (Selectedcolorname == "White")
                    {
                        MessageBox.Show("Please change the Font Color, otherwise you are not able to  see the content!!!");
                    }
                }
                else
                {
                    updateSetting.IniWriteValueupdateColor("GENERALSETTING", "bgcolor", colorDialog1.Color.ToString(), iniFileSetting);
                }
                BackColor = colorDialog1.Color;
                iromsObj.WriteUniquelogs("settingLogs" + " ", " Background color updated on setting.ini files.", MessageType.Informational);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (fontDialog1.ShowDialog() == DialogResult.OK)
            {
                label2.Font = fontDialog1.Font;
                label3.Font = fontDialog1.Font;
                label4.Font = fontDialog1.Font;
                label5.Font = fontDialog1.Font;

                // MessageBox.Show(fontDialog1.Font.Style.ToString());
                updateSetting.IniWriteValueupdateColor("GENERALSETTING", "fontDet", fontDialog1.Font.ToString(), iniFileSetting);
                updateSetting.IniWriteValueupdateColor("GENERALSETTING", "fontStyle", fontDialog1.Font.Style.ToString(), iniFileSetting);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (colorDialog2.ShowDialog() == DialogResult.OK)
            {
                Match m = Regex.Match(colorDialog2.Color.ToString(), @"A=(?<Alpha>\d+),\s*R=(?<Red>\d+),\s*G=(?<Green>\d+),\s*B=(?<Blue>\d+)");
                if (!m.Success)
                {
                    string selectedColor = colorDialog2.Color.ToString();
                    string newString = selectedColor.Substring(selectedColor.IndexOf('[') + 1);
                    string Selectedcolorname = newString.Substring(0, newString.IndexOf(']'));
                    updateSetting.IniWriteValueupdateColor("GENERALSETTING", "fontcolor", Selectedcolorname, iniFileSetting);
                    //  cr.WriteUniquelogs("settingLogs" + " ", "Selected color from OHL = " + Selectedcolorname, MessageType.Informational);
                }
                else
                {
                    updateSetting.IniWriteValueupdateColor("GENERALSETTING", "fontcolor", colorDialog2.Color.ToString(), iniFileSetting);
                    //  cr.WriteUniquelogs("settingLogs" + " ", "Selected color from OHL = " + colorDialog1.Color.ToString(), MessageType.Informational);
                }
                label2.ForeColor = colorDialog2.Color;
                label3.ForeColor = colorDialog2.Color;
                label4.ForeColor = colorDialog2.Color;
                label5.ForeColor = colorDialog2.Color;
                iromsObj.WriteUniquelogs("settingLogs" + " ", " Font color applied and values are loaded to open-high setting.", MessageType.Informational);
            }
        }
        //IRDSPM::Pratiksha::29-07-2020::Added for cancel button
        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            string[] key = new string[3];
            string[] value = new string[3];
            string key_val;
            string value_value;
            key[0] = toastNotification.Name;
            key[1] = toastAudio.Name;
            key[2] = showGUIForOpenPositions.Name;
            if (TNtrue.Checked == true)
            {
                value[0] = "true";
            }
            else
            {
                value[0] = "false";
                value[1] = "false";
            }
            if (TAtrue.Enabled == true)
            {
                if (TAtrue.Checked == true)
                {
                    value[1] = "true";
                }
                else
                {
                    value[1] = "false";
                }
            }

            if (showGuiTrue.Checked == true)
            {
                value[2] = "true";
            }
            else
            {
                value[2] = "false";
            }

            for (int i = 0; i < 3; i++)
            {
                if (i < 2)
                {
                    key_val = key[i];
                    value_value = value[i];
                    Settings.writeINIFile("TOASTSETTING", key_val, value_value);
                }
                else
                {
                    key_val = key[i];
                    value_value = value[i];
                    Settings.writeINIFile("GUI", key_val, value_value);
                }
            }
            string message = "Saved changes successfully!!!";
            MessageBoxButtons buttons = MessageBoxButtons.OK;
            DialogResult dialog = MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
            if (dialog == DialogResult.OK)
            {
                this.Close();
            }
        }

        private void TNfalse_Click(object sender, EventArgs e)
        {
            TAfalse.Enabled = false;
            TAtrue.Enabled = false;
        }

        private void TNtrue_CheckedChanged(object sender, EventArgs e)
        {
            TAfalse.Enabled = true;
            TAtrue.Enabled = true;
        }
    }
}
