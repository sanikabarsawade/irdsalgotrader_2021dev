﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using IRDSAlgoOMS;

namespace IRDSAlgoOMS
{
    public partial class SymbolSetting : Form
    {
        //IRDSPM::Pratiksha::18-07-2020::new Symbolini file for symbol reading
        string inisymbolList;
        dynamic signalProcessor;
        IROMS iromsObj = new IROMS();
        ConfigSettings Settings;
        ReadSettings objReadSetting;
        Logger logger = Logger.Instance;
        public List<string> iromsRealtimeSymbol;
        public List<string> RealtimeSymbolfromsymbolIni;
        List<string> customsymbollist = new List<string>();
        //IRDSPM::Pratiksha::03-07-2020::For setting ini
        string bgcolor = "";
        string fontcolor = "";
        string fontDet;
        string fontStyle = "";
        Color bColor;
        Color fColor;
        string iniFileSetting;
        //Jyoti
        List<string> NFOSymbolsList;
        List<string> NSESymbolsList;
        List<string> MCXSymbolsList;
        List<string> EquitySymbolsList;
        List<string> OptionSymbolsList;
        string title = "";
        public SymbolSetting(dynamic sample)
        {
            signalProcessor = sample;
            InitializeComponent();
            //IRDSPM::PRatiksha::01-07-2020::Added to read color and realtimeSymbols from ini --start
            var path = Directory.GetCurrentDirectory();
            //IRDSPM::Pratiksha::03-07-2020::For reading and assigning color to background --start
            iniFileSetting = path + @"\Configuration\" + "Setting.ini";
            if (File.Exists(iniFileSetting))
            {
                Settings = new ConfigSettings(logger);
                Settings.ReadSettingConfigFile(iniFileSetting);
                bgcolor = Settings.bgcolorname.ToString();
                fontcolor = Settings.fontcolorname.ToString();
                fontDet = Settings.fontdet.ToString();
                fontStyle = Settings.fontstyle.ToString();
                title = Settings.m_Title;
            }
            string[] colorDetfromINI = { bgcolor, fontcolor };
            for (int i = 0; i < colorDetfromINI.Length; i++)
            {
                Match m = Regex.Match(colorDetfromINI[i], @"A=(?<Alpha>\d+),\s*R=(?<Red>\d+),\s*G=(?<Green>\d+),\s*B=(?<Blue>\d+)");
                if (m.Success)
                {
                    int alpha = int.Parse(m.Groups["Alpha"].Value);
                    int red = int.Parse(m.Groups["Red"].Value);
                    int green = int.Parse(m.Groups["Green"].Value);
                    int blue = int.Parse(m.Groups["Blue"].Value);
                    if (i == 0)
                    {
                        bColor = Color.FromArgb(alpha, red, green, blue);
                    }
                    else
                    {
                        fColor = Color.FromArgb(alpha, red, green, blue);
                    }
                }
                else
                {
                    if (i == 0)
                    {
                        bColor = Color.FromName(colorDetfromINI[i]);
                    }
                    else
                    {
                        fColor = Color.FromName(colorDetfromINI[i]);
                    }
                }
                /*    groupBox1.BackColor = bColor;
                groupBox2.BackColor = bColor;
                BackColor = bColor;
                label2.ForeColor = fColor;
                groupBox3.ForeColor = fColor;
                //IRDSPM::Pratiksha::18-07-2020::Added font color radio buttons and group box
                radioButton1.ForeColor = fColor;
                radioButton2.ForeColor = fColor;
                    radioButton3.ForeColor = fColor;*/
            }
            iromsObj.WriteUniquelogs("settingLogs" + " ", " Settings applied on symbol setting.", MessageType.Informational);
            //IRDSPM::Pratiksha::03-07-2020::For reading and assigning color to background --end

            //IRDSPM::Pratiksha::09-07-2020::Accessing font family and font size --start
            try
            {
                string fontfamilyAndSize = fontDet.Substring(fontDet.IndexOf("=") + 1, fontDet.IndexOf(",")-1);
                string fontname = fontfamilyAndSize.Substring(0, fontfamilyAndSize.IndexOf(","));
                int pFrom = fontfamilyAndSize.IndexOf("=") + "=".Length;
                //IRDSPM::Pratiksha::18-07-2020::For crash issue
                 string result = fontfamilyAndSize.Substring(pFrom , (fontfamilyAndSize.Length-pFrom));
                float fontSize = float.Parse(result);

                label2.Font = new Font(fontname, fontSize, FontStyle.Bold, GraphicsUnit.Point);
                checkedListBox1.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                listBox1.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
               // customSymbol.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);

                //IRDSPM::Pratiksha::18-07-2020::Added font size and family to radio buttons and group box
                groupBox2.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                groupBox3.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                radioButton1.Font = new Font(fontname, fontSize, FontStyle.Bold, GraphicsUnit.Point);
                radioButton2.Font = new Font(fontname, fontSize, FontStyle.Bold, GraphicsUnit.Point);
                radioButton3.Font = new Font(fontname, fontSize, FontStyle.Bold, GraphicsUnit.Point);

                add.Font = new Font(fontname, fontSize, FontStyle.Bold, GraphicsUnit.Point);
                addall.Font = new Font(fontname, fontSize, FontStyle.Bold, GraphicsUnit.Point);
                delete.Font = new Font(fontname, fontSize, FontStyle.Bold, GraphicsUnit.Point);
                deleteall.Font = new Font(fontname, fontSize, FontStyle.Bold, GraphicsUnit.Point);
                search.Font = new Font(fontname, fontSize, FontStyle.Bold, GraphicsUnit.Point);
                button1.Font = new Font(fontname, fontSize, FontStyle.Bold, GraphicsUnit.Point);
            }
            catch(Exception ex)
            {
                Console.WriteLine("Exception : " + ex);
            }
            //IRDSPM::Pratiksha::09-07-2020::Accessing font family and font size --end

            //IRDSPM::Pratiksha::18-07-2020::request to read Symbolini for symbol list
            RealtimeSymbolfromsymbolIni = new List<string>();
            inisymbolList = path + @"\Configuration\" + "symbollist.ini";
            if (File.Exists(inisymbolList))
            {
                Settings = new ConfigSettings(logger);
                Settings.ReadSymbolList();
                //RealtimeSymbolfromsymbolIni = Settings.SymbolListFromsymbolini;
                foreach (string sym in Settings.Symbol)
                {
                    string[] symParts = sym.Split('.');
                    if (symParts[1] != "NSE")
                    {
                        string newSym = symParts[0] + DateTime.Now.ToString("yy") + DateTime.Now.ToString("MMM").ToUpper() + "FUT";
                        RealtimeSymbolfromsymbolIni.Add(newSym);
                    }
                    else
                    {
                        RealtimeSymbolfromsymbolIni.Add(symParts[0]);
                    }
                }
            }

            //IRDSPM::PRatiksha::01-07-2020::Added to read color and realtimeSymbols from ini --end
            List<string> list;
            //signalProcessor = new AlgoOMS();

            NFOSymbolsList = signalProcessor.FetchDetails("NFO");
            MCXSymbolsList = signalProcessor.FetchDetails("MCX");
            EquitySymbolsList = signalProcessor.FetchDetails("NSE");
            OptionSymbolsList = signalProcessor.FetchDetails("OPTIONS");
            NSESymbolsList = new List<string>();

            foreach (string sym in NFOSymbolsList)
            {
                string newSym = sym.Substring(0, sym.Length - 8);
                NSESymbolsList.Add(newSym);
            }

            foreach (var item in RealtimeSymbolfromsymbolIni)
            {
                checkedListBox1.Items.Add(item);
            }
            iromsObj.WriteUniquelogs("settingLogs" + " ", "Read Symbols names from ini in symbol setting. ", MessageType.Informational);
            //Adding symbols from db, id present in ini file then should not duplicate
            for (int i = 0; i < NSESymbolsList.Count; i++)
            {
                if (!checkedListBox1.Items.Contains(NSESymbolsList[i]))
                {
                    checkedListBox1.Items.Add(NSESymbolsList[i]);
                }
            }
            for (int i = 0; i < NFOSymbolsList.Count; i++)
            {
                if (!checkedListBox1.Items.Contains(NFOSymbolsList[i]))
                {
                    checkedListBox1.Items.Add(NFOSymbolsList[i]);
                }
            }
            for (int i = 0; i < MCXSymbolsList.Count; i++)
            {
                if (!checkedListBox1.Items.Contains(MCXSymbolsList[i]))
                {
                    checkedListBox1.Items.Add(MCXSymbolsList[i]);
                }
            }
            for (int i = 0; i < OptionSymbolsList.Count && i <= 100; i++)
            {
                if (!checkedListBox1.Items.Contains(OptionSymbolsList[i]))
                {
                    checkedListBox1.Items.Add(OptionSymbolsList[i]);
                }
            }
            iromsObj.WriteUniquelogs("settingLogs" + " ", "Read symbol names from db.", MessageType.Informational);
            // from ini file should be checked and added in listbox
            for (int k = 0; k < checkedListBox1.Items.Count; k++)
            {
                if (k < RealtimeSymbolfromsymbolIni.Count)
                {
                    checkedListBox1.SetItemChecked(k, true);
                    listBox1.Items.Add(checkedListBox1.Items[k]);
                }
            }
           // iromsObj.WriteUniquelogs("settingLogs" + " ", "Symbols from ini are checked by default.", MessageType.Informational);
        }

        //IRDSPM::Pratiksha::07-07-2020::For search box
        private void search_Click_1(object sender, EventArgs e)
        {
            bool found = false;
            string search = textBox1.Text.ToUpper();
            if (search.Length > 0)
            {
            for (int i = 0; i <= checkedListBox1.Items.Count - 1; i++)
            {
                    if (checkedListBox1.Items[i].ToString().ToUpper().Contains(search))
                {
                    checkedListBox1.SetSelected(i, true);
                    checkedListBox1.SetItemChecked(i, true);
                        found = true;
                    if (!listBox1.Items.Contains(checkedListBox1.Items[i].ToString()))
                    {
                        listBox1.Items.Add(checkedListBox1.Items[i].ToString());
                    }
                }
            }
                if (found == false)
                {
                    for (int i = 0; i <= OptionSymbolsList.Count - 1; i++)
                    {
                        if (OptionSymbolsList[i].ToString().ToUpper().Contains(search))
                        {
                            if (!listBox1.Items.Contains(OptionSymbolsList[i].ToString()))
                            {
                                listBox1.Items.Add(OptionSymbolsList[i].ToString());
                                found = true;
                            }
                        }
                    }
                }
            }
        }

        //IRDSPM::Pratiksha::30-06-2020:: add the symbol name in list, should not duplicate
        private void add_Click_1(object sender, EventArgs e)
        {
            //listBox1.Items.Clear();
            for (int i = 0; i < checkedListBox1.CheckedItems.Count; i++)
            {
                //IRDSPM::Pratiksha::18-07-2020::Removed condition because facing issue while inserting
                if (!RealtimeSymbolfromsymbolIni.Contains(checkedListBox1.CheckedItems[i].ToString()))
                {
                    listBox1.Items.Add(checkedListBox1.CheckedItems[i]);
                    RealtimeSymbolfromsymbolIni.Add(checkedListBox1.CheckedItems[i].ToString());
                }
                }
            /*if (customsymbollist.Count > 0)
            {
                foreach (var item in customsymbollist)
                {
                    if (!listBox1.Items.Contains(item))
                    {
                        listBox1.Items.Add(item);
                        RealtimeSymbolfromsymbolIni.Add(item);
                    }
            }
                // listBox1.Items.Add(customSymbol.Text);
            }*/
            iromsObj.WriteUniquelogs("settingLogs" + " ", "Added in listbox by add button: " + listBox1.Items.Count, MessageType.Informational);
            /*for (int i = 0; i < listBox1.Items.Count; i++)
            {
                iromsObj.WriteUniquelogs("settingLogs" + " ", "Added in listbox by add button: " + checkedListBox1.CheckedItems[i], MessageType.Informational);
            }*/
        }

        //IRDSPM::Pratiksha::30-06-2020:: add all the symbol name in list
        private void addall_Click_1(object sender, EventArgs e)
        {
            for (int i = 0; i < checkedListBox1.Items.Count; i++)
            {
                checkedListBox1.SetItemChecked(i, true);
            }
            for (int i = 0; i < checkedListBox1.CheckedItems.Count; i++)
            {
                if (!RealtimeSymbolfromsymbolIni.Contains(checkedListBox1.CheckedItems[i].ToString()))
                {
                    listBox1.Items.Add(checkedListBox1.CheckedItems[i]);
                    RealtimeSymbolfromsymbolIni.Add(checkedListBox1.CheckedItems[i].ToString());
                }
            }
            iromsObj.WriteUniquelogs("settingLogs" + " ", "Added all names in listbox ", MessageType.Informational);
        }

        private void delete_Click_1(object sender, EventArgs e)
        {
            INIFileForSetting iniObj = new INIFileForSetting();
            for (int v = 0; v < listBox1.SelectedItems.Count; v++)
            {
                try
                {
                    string temp = listBox1.SelectedItems[v].ToString();
                    listBox1.Items.Remove(temp);
                    RealtimeSymbolfromsymbolIni.Remove(temp);
                    if (checkedListBox1.Items.Contains(temp))
                    {
                        int indexPos = checkedListBox1.Items.IndexOf(temp);
                        checkedListBox1.SetItemChecked(indexPos, false);
                        iromsObj.WriteUniquelogs("settingLogs" + " ", "Removed item from listbox by delete button ", MessageType.Informational);
                    }
                    /*else
                    {
                        foreach (var item in customsymbollist)
                        {
                            if (customsymbollist.Contains(temp))
                            {
                                listBox1.Items.Remove(temp);
                                customsymbollist.Remove(temp);
                            }
                        }
                    }*/
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Exception : " + ex);
                }
            }
        }

        //IRDSPM::Pratiksha::01-07-2020:: delete all the symbol from list
        private void deleteall_Click_1(object sender, EventArgs e)
        {
            iromsObj.WriteUniquelogs("settingLogs" + " ", "Deleted all names from listbox ", MessageType.Informational);
            for (int i = 0; i <= checkedListBox1.Items.Count - 1; i++)
            {
                checkedListBox1.SetItemChecked(i, false);
                listBox1.Items.Clear();
            }
                customsymbollist.Clear();
                RealtimeSymbolfromsymbolIni.Clear();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            try
            {
                string item = customSymbol.Text.ToString();
                if ((item.Length > 0) && (NFOSymbolsList.Contains(item) || MCXSymbolsList.Contains(item) || EquitySymbolsList.Contains(item) || OptionSymbolsList.Contains(item)))
                {
                    customsymbollist.Add(customSymbol.Text);
                    if (!listBox1.Items.Contains(customSymbol.Text))
                    {
                        listBox1.Items.Add(customSymbol.Text);
                        RealtimeSymbolfromsymbolIni.Add(customSymbol.Text);
                    }
                }
                else
                {
                    MessageBox.Show("Please enter valid symbol name");
                }
                /*if (customsymbollist.Count > 0)
                {
                    foreach (var item in customsymbollist)
                    {
                        if (!listBox1.Items.Contains(item))
                {
                            listBox1.Items.Add(item);
                            RealtimeSymbolfromsymbolIni.Add(item);
                        }
                    }
                    // listBox1.Items.Add(customSymbol.Text);
                }*/
            }
            catch (Exception ex)
            {
                iromsObj.WriteUniquelogs("settingLogs" + " ", "In Exception : " + ex, MessageType.Informational);
            }
        }

        //IRDSPM::Pratiksha::01-07-2020:: clear all the symbol from list and checkbxlist
        private void reset_Click_1(object sender, EventArgs e)
        {
            INIFileForSetting iniObj = new INIFileForSetting();

            string message = "Do you want to clear symbols from INI file?";

            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result = MessageBox.Show(message, title, buttons);
            if (result == DialogResult.Yes)
            {
                //for (int i = 0; i < RealtimeSymbolfromsymbolIni.Count; i++)
                //{
                    iniObj.clearTestingSymbol("RealTimeSymbols", null, null, inisymbolList);
                //}
                for (int i = 0; i <= checkedListBox1.Items.Count - 1; i++)
                {
                    checkedListBox1.SetItemChecked(i, false);
                    listBox1.Items.Clear();
                }
            }
            iromsObj.WriteUniquelogs("settingLogs" + " ", "Reset all Data from symbol setting to ini file", MessageType.Informational);
        }

        //IRDSPM::Pratiksha::01-07-2020:: add the symbols name in ini file
        private void set_Click_1(object sender, EventArgs e)
        {
            try
            {
                INIFileForSetting iniObj = new INIFileForSetting();
                //for (int i = 0; i < RealtimeSymbolfromsymbolIni.Count; i++)
                //{
                    iniObj.clearTestingSymbol("RealTimeSymbols", null, null, inisymbolList);
                //}
                List<string> FinalSymbolsList = new List<string>();
                for (int k = 0; k < RealtimeSymbolfromsymbolIni.Count; k++)
                {
                    string sym = RealtimeSymbolfromsymbolIni[k];
                    string newSym = sym + ".NSE";
                    if (sym.EndsWith("FUT"))
                    {
                        newSym = sym.Substring(0, sym.Length - 8);
                        if (NFOSymbolsList.Contains(sym))
                        {
                            newSym = newSym + ".NFO";
                        }
                        if (MCXSymbolsList.Contains(sym))
                        {
                            newSym = newSym + ".MCX";
                        }
                        if (!newSym.Contains("."))
                {
                            foreach (string symIni in Settings.Symbol)
                            {
                                if (newSym == symIni.Split('.')[0])
                                {
                                    newSym = symIni;
                                    break;
                                }
                            }
                        }
                }
                    FinalSymbolsList.Add(newSym);
                    Settings.WriteSymbolList("RealTimeSymbols", k.ToString(), newSym);
                    iromsObj.WriteUniquelogs("settingLogs" + " ", "Add in ini by apply click\t" + RealtimeSymbolfromsymbolIni[k].ToString()+" as "+newSym, MessageType.Informational);
                }
                signalProcessor.CheckTradingSymbolPresentOrNot(FinalSymbolsList);
                string message = "Saved changes successfully!!!";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                DialogResult dialog = MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                if (dialog == DialogResult.OK)
                {
                    this.Close();
                }
            }
            catch(Exception ex)
            {
                iromsObj.WriteUniquelogs("settingLogs" + " ", ex.ToString(), MessageType.Informational);
            }
        }

        //IRDSPM::Pratiksha::28-07-2020::for same like all pages
        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void button2_Click_1(object sender, EventArgs e)
        {
            INIFileForSetting iniObj = new INIFileForSetting();

            string message = "Do you want to clear symbols from INI file?";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result = MessageBox.Show(message, title, buttons);
            if (result == DialogResult.Yes)
            {
                //for (int i = 0; i < RealtimeSymbolfromsymbolIni.Count; i++)
                //{
                    iniObj.clearTestingSymbol("RealTimeSymbols", null, null, inisymbolList);
                //}
                for (int i = 0; i <= checkedListBox1.Items.Count - 1; i++)
                {
                    checkedListBox1.SetItemChecked(i, false);
                    listBox1.Items.Clear();
                }
            }
            iromsObj.WriteUniquelogs("settingLogs" + " ", "Reset all Data from symbol setting to ini file", MessageType.Informational);
        }

        //Jyoti
        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked == true)
            {
                checkedListBox1.Items.Clear();
                listBox1.Items.Clear();
                int count = 0;
                foreach (var item in RealtimeSymbolfromsymbolIni)
                {
                    if (!item.EndsWith("FUT"))
                    {
                        checkedListBox1.Items.Add(item);
                        count = count + 1;
                    }
                }
                for (int i = 0; i < NSESymbolsList.Count; i++)
                {
                    if (!checkedListBox1.Items.Contains(NSESymbolsList[i]))
                    {
                        checkedListBox1.Items.Add(NSESymbolsList[i]);
                    }
                }
                for (int k = 0; k < checkedListBox1.Items.Count; k++)
                {
                    if (k < count)
                    {
                        checkedListBox1.SetItemChecked(k, true);
                        listBox1.Items.Add(checkedListBox1.Items[k]);
                    }
                }
            }
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton2.Checked == true)
            {
                checkedListBox1.Items.Clear();
                listBox1.Items.Clear();
                int count = 0;
                for (int i = 0; i < RealtimeSymbolfromsymbolIni.Count; i++)
                {
                    string item = RealtimeSymbolfromsymbolIni[i];
                    if (NFOSymbolsList.Contains(item))
                    {
                        checkedListBox1.Items.Add(item);
                        checkedListBox1.SetItemChecked(count, true);
                        listBox1.Items.Add(item);
                        count++;
                    }
                }
                for (int i = 0; i < NFOSymbolsList.Count; i++)
                {
                    if (!checkedListBox1.Items.Contains(NFOSymbolsList[i]))
                    {
                        checkedListBox1.Items.Add(NFOSymbolsList[i]);
                    }
                }
            }
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton3.Checked == true)
            {
                checkedListBox1.Items.Clear();
                listBox1.Items.Clear();
                int count = 0;
                for (int i = 0; i < RealtimeSymbolfromsymbolIni.Count; i++)
                {
                    string item = RealtimeSymbolfromsymbolIni[i];
                    if (MCXSymbolsList.Contains(item))
                    {
                        checkedListBox1.Items.Add(item);
                        checkedListBox1.SetItemChecked(count, true);
                        listBox1.Items.Add(item);
                        count++;
                    }
                }
                for (int i = 0; i < MCXSymbolsList.Count; i++)
                {
                    if (!checkedListBox1.Items.Contains(MCXSymbolsList[i]))
                    {
                        checkedListBox1.Items.Add(MCXSymbolsList[i]);
                    }
                }
            }
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton4.Checked == true)
            {
                //List<string> list;
                checkedListBox1.Items.Clear();
                listBox1.Items.Clear();
                int count = 0;
                for (int i = 0; i < RealtimeSymbolfromsymbolIni.Count; i++)
                {
                    string item = RealtimeSymbolfromsymbolIni[i];
                    if (OptionSymbolsList.Contains(item))
                    {
                        checkedListBox1.Items.Add(item);
                        checkedListBox1.SetItemChecked(count, true);
                        listBox1.Items.Add(item);
                        count++;
                    }
                }
                for (int i = 0; i < OptionSymbolsList.Count && i <= 100; i++)
                {
                    if (!checkedListBox1.Items.Contains(OptionSymbolsList[i]))
                    {
                        checkedListBox1.Items.Add(OptionSymbolsList[i]);
                    }
                }
            }
        }
    }
}
