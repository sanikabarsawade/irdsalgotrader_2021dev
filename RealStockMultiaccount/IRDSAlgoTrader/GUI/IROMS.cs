﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IRDSAlgoOMS
{ 
    public partial class IROMS : Form
    {
        Logger logger = Logger.Instance;
        INIFileForSetting updateColor = new INIFileForSetting();
        string userID = " ";
        string rootVal = " ";
        string loginVal = " ";
        string MyAPIKeyVal = " ";
        string MySecretVal = " ";
        string MyUserIdVal = " ";
        string MyPasswordVal = " ";
        string PinVal = " ";
        bool manualLoginVal;
        ReadSettings Settings1 = null;
        ConfigSettings objconfig = null;
        string iniFileIROMS;
        string iniFileSamco;
       // bool showGUIForOpenPositionsVal;
        bool currentValManualLogin;
        //IRDSPM::14-08-2020::For temporary storing value
        bool currentStoreDataMysqlDBVal;
        bool currentstoreDataSqliteDB;
        bool currentstoreDataCSV;
        bool currentValShowGUI;
        bool currentstoreDataMysqlDB;
        bool currentcalculateProfitLoss;
        //IRDSPM::PRatiksha::14-08-2020:: newly added
        bool calculateProfitLossVal;
        bool storeDataMysqlDBVal;
        bool storeDataSqliteDBVal;
        bool storeDataCSVVal;
        //IRDSPM::Pratiksha::03-07-2020::For setting ini
        string bgcolor = "";
        string fontcolor = "";
        string fontDet = "";
        string fontStyle = "";
        Color bColor;
        Color fColor;
        string iniFileSetting;
        string path = Directory.GetCurrentDirectory();
        string m_IniFilePath = "";
        dynamic m_objKiteConnectWrapper = null;       
        string m_loginStatus = "";
        string m_BrokerName = "";

        public IROMS()
        {
            InitializeComponent();
            //IRDSPM::Pratiksha::03-07-2020::For reading and assigning color to background --start
            iniFileSetting = path + @"\Configuration\" + "Setting.ini";
            if (File.Exists(iniFileSetting))
            {
                //sanika::14-sep-2020::Added condition
                if (objconfig == null)
                {
                    objconfig = new ConfigSettings(logger);
                }
                objconfig.ReadSettingConfigFile(iniFileSetting);
                bgcolor = objconfig.bgcolorname.ToString();
                fontcolor = objconfig.fontcolorname.ToString();
                fontDet = objconfig.fontdet.ToString();
                fontStyle = objconfig.fontstyle.ToString();
                //sanika::14-sep-2020::read broker name from ini file
                m_BrokerName = objconfig.m_brokerName;
            }
            string[] colorDetfromINI = { bgcolor, fontcolor };
            for (int i = 0; i < colorDetfromINI.Length; i++)
            {
                Match m = Regex.Match(colorDetfromINI[i], @"A=(?<Alpha>\d+),\s*R=(?<Red>\d+),\s*G=(?<Green>\d+),\s*B=(?<Blue>\d+)");
                if (m.Success)
                {
                    int alpha = int.Parse(m.Groups["Alpha"].Value);
                    int red = int.Parse(m.Groups["Red"].Value);
                    int green = int.Parse(m.Groups["Green"].Value);
                    int blue = int.Parse(m.Groups["Blue"].Value);
                    if (i == 0)
                    {
                        bColor = Color.FromArgb(alpha, red, green, blue);
                    }
                    else
                    {
                        fColor = Color.FromArgb(alpha, red, green, blue);
                    }
                }
                else
                {
                    if (i == 0)
                    {
                        bColor = Color.FromName(colorDetfromINI[i]);
                    }
                    else
                    {
                        fColor = Color.FromName(colorDetfromINI[i]);
                    }
                }
                //IRDSPM::Pratiksha::03-07-2020::For reading and assigning color to background --end
            }            
            groupBox4.ForeColor = Color.Black;
            //IRDSPM::Pratiksha::09-07-2020::Accessing font family and font size
            try
            {

                string fontfamilyAndSize = fontDet.Substring(fontDet.IndexOf("=") + 1, fontDet.IndexOf(",") - 1);
                string fontname = fontfamilyAndSize.Substring(0, fontfamilyAndSize.IndexOf(","));
                int pFrom = fontfamilyAndSize.IndexOf("=") + "=".Length;
                int pTo = fontfamilyAndSize.LastIndexOf(",");
                string result = fontfamilyAndSize.Substring(pFrom, fontfamilyAndSize.Length - pFrom);
                float fontSize = float.Parse(result);

                root.Font = new Font(fontname, fontSize, FontStyle.Bold, GraphicsUnit.Point);
                login.Font = new Font(fontname, fontSize, FontStyle.Bold, GraphicsUnit.Point);
                MyAPIKey.Font = new Font(fontname, fontSize, FontStyle.Bold, GraphicsUnit.Point);
                MySecret.Font = new Font(fontname, fontSize, FontStyle.Bold, GraphicsUnit.Point);
                MyUserId.Font = new Font(fontname, fontSize, FontStyle.Bold, GraphicsUnit.Point);
                MyPassword.Font = new Font(fontname, fontSize, FontStyle.Bold, GraphicsUnit.Point);
                Pin.Font = new Font(fontname, fontSize, FontStyle.Bold, GraphicsUnit.Point);
                manualLogin.Font = new Font(fontname, fontSize, FontStyle.Bold, GraphicsUnit.Point);                
                textBox1.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                textBox2.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                textBox3.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                textBox4.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                textBox5.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                textBox6.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                textBox7.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                radioButton1.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                radioButton2.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                radioButton3.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);
                radioButton4.Font = new Font(fontname, fontSize, FontStyle.Regular, GraphicsUnit.Point);                
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", " Exception Error Message =", MessageType.Exception);
            }

            WriteUniquelogs("settingLogs" + " ", " color applied and values are loaded to broker setting.", MessageType.Informational);

            comboBoxBrokerSelect.Items.Add("Samco");
            comboBoxBrokerSelect.Items.Add("Zerodha");
            //sanika::14-sep-2020::check broker from ini file and load data according to that
            if (m_BrokerName == "Zerodha")
            {
                RefreshDataFromINIfile();
            }
            else if(m_BrokerName == "Samco")
            {
                RefreshDataFromSamcoINIfile();
            } 
            else
            {
                m_BrokerName = "Zerodha";
                RefreshDataFromINIfile();
            }
            comboBoxBrokerSelect.SelectedItem = m_BrokerName;
            comboBoxBrokerSelect.SelectedIndexChanged += new EventHandler(Selected_Index);
        }

        protected void Selected_Index(Object sender, EventArgs e)
        {

            if (comboBoxBrokerSelect.SelectedItem.ToString() == "Samco")
            {               
                RefreshDataFromSamcoINIfile();
            }
            else if (comboBoxBrokerSelect.SelectedItem.ToString() == "Zerodha")
            {               
                RefreshDataFromINIfile();
            }
        }


        public void RefreshDataFromINIfile()
        {
            //sanika::14-sep-2020::Added try-catch
            try
            {
                iniFileIROMS = path + @"\Configuration\" + "APISetting.ini";
                if (File.Exists(iniFileIROMS))
                {
                    //sanika::14-sep-2020::Added condition
                    if (Settings1 == null)
                    {
                        Settings1 = new ReadSettings(logger);
                    }
                    MyAPIKey.Text = "API Key";
                    MyAPIKey.Name = "MyAPIKey";
                    MySecret.Text = "Secret";
                    MySecret.Name = "MySecret";
                    login.Text = "Login";
                    login.Name = "login";
                    userID = Settings1.readUserId(iniFileIROMS);
                    Settings1.readConfigFileOROMS(iniFileIROMS);
                    rootVal = Settings1.root;
                    loginVal = Settings1.login;
                    MyAPIKeyVal = Settings1.MyAPIKey;
                    MySecretVal = Settings1.MySecret;
                    MyUserIdVal = Settings1.MyUserId;
                    MyPasswordVal = Settings1.MyPassword;
                    PinVal = Settings1.Pin;
                    //IRDSPM::Pratiksha::10-07-2020::conversion Change done on config file
                    manualLoginVal = Settings1.autoLogin;
                }
                else
                {
                    MessageBox.Show(iniFileIROMS + " INI file does not exit!!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                textBox7.Enabled = true;
                textBox3.Enabled = true;
                textBox4.Enabled = true;
                textBox1.Text = rootVal;
                textBox2.Text = loginVal;
                textBox3.Text = MyAPIKeyVal;
                textBox4.Text = MySecretVal;
                textBox5.Text = MyUserIdVal;
                textBox6.Text = MyPasswordVal;
                textBox7.Text = PinVal;
                //IRDSPM::Pratiksha::17-08-2020::For act like password
                textBox7.UseSystemPasswordChar = true;
                if (manualLoginVal == true)
                {
                    radioButton1.Checked = true;
                }
                else
                {
                    radioButton2.Checked = true;
                }

                WriteUniquelogs("settingLogs" + " ", " refresh data in ini file from broker setting.", MessageType.Informational);
                //IRDSPM::Pratiksha::get details in local variable to display in form --end
            }
            catch(Exception e)
            {
                WriteUniquelogs("settingLogs" + " ", "RefreshDataFromINIfile : Exception Error Message = ", MessageType.Exception);
            }
        }

        private void IROMS_FormClosing(object sender, FormClosingEventArgs e)
        {
            //WriteUniquelogs("settingLogs" + " ", " In form close of broker setting.", MessageType.Informational);
            //if (radioButton1.Checked)
            //{
            //    currentValManualLogin = true;
            //}
            //else
            //{
            //    currentValManualLogin = false;
            //}
            //if (radioButton3.Checked)
            //{
            //    currentValShowGUI = true;
            //}
            //else 
            //{
            //    currentValShowGUI = false;
            //}
            //if ((textBox1.Text != rootVal) || (textBox2.Text != loginVal) || (textBox3.Text != MyAPIKeyVal) || (textBox4.Text != MySecretVal) || (textBox5.Text != MyUserIdVal) || (textBox6.Text != MyPasswordVal) || (textBox7.Text != PinVal) || (currentValShowGUI != showGUIForOpenPositionsVal) || (currentValManualLogin != manualLoginVal))
            //{
            //    string message = "Do you want to save the changes?";
            //    string title = "AlgoTrader";
            //    MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            //    DialogResult result = MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
            //    if (result == DialogResult.Yes)
            //    {
            //        refreshDataFromINIfile();
            //        m_objKiteConnectWrapper.ChangeUserID(MyUserIdVal);
            //        WriteUniquelogs("settingLogs" + " ", " Data updated from broker setting to IROMS.ini file.", MessageType.Informational);
            //}
            //    else
            //{
            //        WriteUniquelogs("settingLogs" + " ", " No data updated from broker setting.", MessageType.Informational);
            //    }
            //}
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void WriteUniquelogs(string symbol, string message, MessageType msgType)
        {
            try
            {
                SymbolLogs log = new SymbolLogs();
                //log = SymbolLogs.InstanceSymbolLogs;
                log.createFile(symbol);
                log.LogMessage(message, MessageType.Informational);
                log = null;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : " + e.Message);
                Trace.WriteLine("Exception : " + e.Message);
            }
        }

        private void set_Click(object sender, EventArgs e)
        {
            //sanika::14-sep-2020::Added try-catch
            try
            {
                if (m_loginStatus != "Connected")
                {
                    if (comboBoxBrokerSelect.SelectedItem.ToString() == "Samco")
                    {
                        string key_val;
                        string value_value;
                        string[] key = new string[6];
                        string[] value = new string[6];
                        key[0] = root.Name;
                        key[1] = MyUserId.Name;
                        key[2] = MyPassword.Name;
                        key[3] = "YOB";
                        key[4] = manualLogin.Name;
                       

                        value[0] = textBox1.Text;
                        value[1] = textBox5.Text;
                        value[2] = textBox6.Text;
                        value[3] = textBox2.Text;
                        if (radioButton1.Checked)
                        {
                            value[4] = "true";
                        }
                        else
                        {
                            value[4] = "false";
                        }

                        
                        //cheking same values or not
                        if ((textBox1.Text != rootVal) || (textBox2.Text != Settings1.YOB) || (textBox5.Text != MyUserIdVal) || (textBox6.Text != MyPasswordVal) || (currentValManualLogin != manualLoginVal))
                        {
                            string messageCon = "Do you want to save the changes?";
                            string titleCon = "AlgoTrader";
                            MessageBoxButtons buttonsCon = MessageBoxButtons.YesNo;
                            DialogResult result = MessageBox.Show(messageCon, titleCon, buttonsCon, MessageBoxIcon.Warning);
                            if (result == DialogResult.Yes)
                            {
                                //sanika::14-sep-2020::move above for loop in this condition because after click to no also it saving values in ini file
                                for (int i = 0; i < 5; i++)
                                {
                                    key_val = key[i];
                                    value_value = value[i];
                                    Settings1.writeINIFile("credentials", key_val, value_value);
                                }
                                string userid = textBox5.Text;
                                RefreshDataFromSamcoINIfile();
                                //sanika::14-sep-2020::call method to set changed userid
                                m_objKiteConnectWrapper.ChangeUserID(userid);
                                SetBrokerName("Samco");
                                WriteUniquelogs("settingLogs" + " ", " Data updated ", MessageType.Informational);

                                //sanika::14-sep-2020::move below message box in this condition because after clicking No it shows saved successfully message box
                                string message = "Saved changes successfully!!!";
                                string title = "AlgoTrader";
                                MessageBoxButtons buttons = MessageBoxButtons.OK;
                                DialogResult dialog = MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                                if (dialog == DialogResult.OK)
                                {
                                    this.Close();
                                }
                            }
                            else
                            {
                                WriteUniquelogs("settingLogs" + " ", " Data not updated clicked on No", MessageType.Informational);
                                //sanika::14-sep-2020::close form after click on NO
                                this.Close();
                            }
                        }
                        
                    }
                    else if (comboBoxBrokerSelect.SelectedItem.ToString() == "Zerodha")
                    {
                        string key_val;
                        string value_value;
                        string[] key = new string[13];
                        string[] value = new string[13];
                        key[0] = root.Name;
                        key[1] = login.Name;
                        key[2] = MyAPIKey.Name;
                        key[3] = MySecret.Name;
                        key[4] = MyUserId.Name;
                        key[5] = MyPassword.Name;
                        key[6] = Pin.Name;
                        key[7] = manualLogin.Name;

                        value[0] = textBox1.Text;
                        value[1] = textBox2.Text;
                        value[2] = textBox3.Text;
                        value[3] = textBox4.Text;
                        value[4] = textBox5.Text;
                        value[5] = textBox6.Text;
                        value[6] = textBox7.Text;
                        if (radioButton1.Checked)
                        {
                            value[7] = "true";
                        }
                        else
                        {
                            value[7] = "false";
                        }

                        //cheking same values or not                       
                        if (radioButton1.Checked)
                        {
                            currentValManualLogin = true;
                        }
                        else
                        {
                            currentValManualLogin = false;
                        }

                        //IRDSPM::Pratiksha::14-08-2020:: to check the original value and current value

                        if ((textBox1.Text != rootVal) || (textBox2.Text != loginVal) || (textBox3.Text != MyAPIKeyVal) || (textBox4.Text != MySecretVal) || (textBox5.Text != MyUserIdVal) || (textBox6.Text != MyPasswordVal) || (textBox7.Text != PinVal) || (currentValManualLogin != manualLoginVal) || (currentcalculateProfitLoss != calculateProfitLossVal) || (currentstoreDataMysqlDB != storeDataMysqlDBVal) || (currentstoreDataSqliteDB != storeDataSqliteDBVal) || (currentstoreDataCSV != storeDataCSVVal))
                        {
                            string messageCon = "Do you want to save the changes?";
                            string titleCon = "AlgoTrader";
                            MessageBoxButtons buttonsCon = MessageBoxButtons.YesNo;
                            DialogResult result = MessageBox.Show(messageCon, titleCon, buttonsCon, MessageBoxIcon.Warning);
                            if (result == DialogResult.Yes)
                            {
                                //sanika::14-sep-2020::move above for loop in this condition because after click to no also it saving values in ini file
                                for (int i = 0; i < 8; i++)
                                {
                                    key_val = key[i];
                                    value_value = value[i];
                                    Settings1.writeINIFile("credentials", key_val, value_value);
                                }
                                string userid = textBox5.Text;
                                RefreshDataFromINIfile();
                                SetBrokerName("Zerodha");
                                m_objKiteConnectWrapper.ChangeUserID(userid);
                                WriteUniquelogs("settingLogs" + " ", " Data Updated", MessageType.Informational);

                                //sanika::14-sep-2020::move below message box in this condition because after clicking No it shows saved successfully message box
                                string message = "Saved changes successfully!!!";
                                string title = "AlgoTrader";
                                MessageBoxButtons buttons = MessageBoxButtons.OK;
                                DialogResult dialog = MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                                if (dialog == DialogResult.OK)
                                {
                                    this.Close();
                                }
                            }
                            else
                            {
                                WriteUniquelogs("settingLogs" + " ", " Data not updated clicked on No", MessageType.Informational);
                                //sanika::14-sep-2020::close form after click on NO
                                this.Close();
                            }
                        }
                    }
                    //sanika::14-sep-2020::code commented condition invalid
                    //else
                    //{
                    //    string message = "Not Allowed - You are logged-In already";
                    //    string title = "AlgoTrader";
                    //    MessageBoxButtons buttons = MessageBoxButtons.OK;
                    //    DialogResult dialog = MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);

                    //}
                }
                else
                {
                    string message = "You are logged in user, you are not allowed to change the setting.";
                    string title = "AlgoTrader";
                    MessageBoxButtons buttons = MessageBoxButtons.OK;
                    DialogResult dialog = MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                }
            }
            catch(Exception er)
            {
                WriteUniquelogs("settingLogs" + " ", "set_Click : Exception Error Message = " + er.Message, MessageType.Exception);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox6.Text = "";
            textBox7.Text = "";
            
            WriteUniquelogs("settingLogs" + " ", "Textboxes Fields are cleared on broker setting.", MessageType.Informational);
        }
        //IRDSPM::Pratiksha::On set buttton click all data should write in ini file --end

        //sanika::14-sep-2020::changed to dynamic 
        public void LoadKiteConnectObject(dynamic kiteConnectWrapper, string loginStatus)
        {
            m_objKiteConnectWrapper = kiteConnectWrapper;
            m_loginStatus = loginStatus;
        }

        public void RefreshDataFromSamcoINIfile()
        {
            //sanika::14-sep-2020::Added try-catch
            try
            {
                iniFileSamco = path + @"\Configuration\" + "SamcoSettings.ini";
                if (File.Exists(iniFileSamco))
                {
                    Settings1 = new ReadSettings(logger);

                    userID = Settings1.readUserId(iniFileSamco);
                    Settings1.readConfigFileSAMCO(iniFileSamco);
                    rootVal = Settings1.root;
                    login.Text = "YOB";
                    login.Name = "YOB";
                    MyAPIKeyVal = Settings1.MyAPIKey;
                    MyAPIKey.Text = "API Key";

                    MySecretVal = Settings1.MySecret;
                    MySecret.Text = "Secret";
                    MyUserIdVal = Settings1.MyUserId;
                    MyPasswordVal = Settings1.MyPassword;
                    PinVal = Settings1.Pin;
                    //IRDSPM::Pratiksha::10-07-2020::conversion Change done on config file
                    manualLoginVal = Settings1.manualLoginsamco;
                    calculateProfitLossVal = Settings1.calculateProfitLosssamco;
                    storeDataMysqlDBVal = Settings1.storeDataMysqlDBsamco;
                    storeDataSqliteDBVal = Settings1.storeDataSqliteDBsamco;
                    storeDataCSVVal = Settings1.storeDataCSVsamco;
                }
                else
                {
                    MessageBox.Show(iniFileSamco + " INI file does not exit!!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                textBox1.Text = rootVal;
                textBox2.Text = Settings1.YOB;
                textBox3.Text = "NA";
                textBox4.Text = "NA";
                textBox3.Enabled = false;
                textBox4.Enabled = false;
                textBox5.Text = MyUserIdVal;
                textBox6.Text = MyPasswordVal;
                //IRDSPM::Pratiksha::17-08-2020::For act like password
                textBox7.UseSystemPasswordChar = false;
                textBox7.Text = "NA";
                textBox7.Enabled = false;

                WriteUniquelogs("settingLogs" + " ", " refresh data in ini file from broker setting.", MessageType.Informational);
                //IRDSPM::Pratiksha::get details in local variable to display in form --end
            }
            catch(Exception e)
            {
                WriteUniquelogs("settingLogs" + " ", " RefreshDataFromSamcoINIfile : Exception Error Message = "+e.Message, MessageType.Exception);
            }
        }

        private void IROMS_Load(object sender, EventArgs e)
        {

        }


        //sanika::14-sep-2020::Added method to write changed broker name in ini file
        public void SetBrokerName(string brokerName)
        {
            try
            {
                if(objconfig == null)
                {
                    objconfig = new ConfigSettings(logger);                    
                }
                objconfig.writeINIFile("BROKER", "brokername", brokerName);
            }
            catch(Exception e)
            {
                WriteUniquelogs("settingLogs" + " ", " SetBrokerName : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }
    }
}
