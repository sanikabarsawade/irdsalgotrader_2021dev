﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace IRDSAlgoOMS.GUI
{
    public partial class symbolListForm : System.Windows.Controls.UserControl
    {
        public DataView DataViewSymbolList { get; set; }
        public DataTable mydtSymbolList;
        string inisymbolList;
        ReadSettings Settings;
        Logger logger = Logger.Instance;
        public List<string> iromsRealtimeSymbol;
        public List<string> RealtimeSymbolfromsymbolIni;
        bool addval = false;
        string title = "";
        string message = "";
        MessageBoxButtons buttons = MessageBoxButtons.OK;
        DialogResult dialog;
        public symbolListForm(string m_title)
        {
            InitializeComponent();
            title = m_title;
            var path = Directory.GetCurrentDirectory();
            mydtSymbolList = new DataTable();
            //mydtSymbolList.Columns.Add("Rank");
            mydtSymbolList.Columns.Add("Symbol Name");
            mydtSymbolList.Columns.Add("Expiry Date");
            DataViewSymbolList = mydtSymbolList.DefaultView;
            DataContext = this;
            inisymbolList = path + @"\Configuration\" + "symbollist.ini";
            int rank = 0;
            if (File.Exists(inisymbolList))
            {
                try
                {
                Settings = new ReadSettings(logger);
                Settings.readConfigFilesymbollist(inisymbolList);
                RealtimeSymbolfromsymbolIni = Settings.SymbolListFromsymbolini;
                foreach (var item in RealtimeSymbolfromsymbolIni)
                {
                    try
                    {
                        string[] values = item.Split(',');
                        this.Dispatcher.Invoke(() =>
                        {
                            mydtSymbolList.Rows.Add(new object[] { values[0], values[1] });
                            DataContext = this;
                        });
                        rank++;
                    }
                    catch (Exception ex)
                    {
                        System.Windows.MessageBox.Show("Exception : " + ex);
                    }
                }
                WriteUniquelogs("Mcx Symbol Setting ", "All details loaded successfully.", MessageType.Informational);
            }
                catch (Exception ex)
                {
                    WriteUniquelogs("settingLogs" + " ", "SymbolList Setting | Exception : " + ex, MessageType.Informational);
                }
            }
            else
            {
                WriteUniquelogs("Mcx Symbol Setting ", "INI file not present", MessageType.Error);
            }
            //mydtSymbolList.Columns[0].ReadOnly = true;
        }


        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            int j = 0;
            int counter = 0;
            int k = mydtSymbolList.Rows.Count;
            bool flagSymbolname = false;
            var re = @"^[0-9]{2}[a-zA-Z]{3}[0-9]{2}$";
            INIFileForSetting iniObj = new INIFileForSetting();
            DialogResult dialog;
            try
            {
            foreach (DataRow dr in mydtSymbolList.Rows)
            {
                flagSymbolname = false;
                if (((dr["Symbol Name"]).ToString().Length == 0) || ((dr["Expiry Date"]).ToString().Length == 0))
                {
                    message = "Not able to add this because null value is present.";
                    dialog = System.Windows.Forms.MessageBox.Show(new Form { TopMost = true }, message, title, buttons, MessageBoxIcon.Warning);
                    WriteUniquelogs("Mcx Symbol Setting ", "Null value present at :" + dr["Symbol Name"] + dr["Expiry Date"] + ". Not able to add this value.", MessageType.Error);

                }
                else
                {
                    if ((dr["Symbol Name"]).ToString().Contains(".") && (((dr["Symbol Name"]).ToString().Length > 0)))
                    {
                        // flagSymbolname = true;
                        Match m = Regex.Match((dr["Expiry Date"]).ToString(), re);

                        if (m.Success)
                        {
                            //Settings.writeINIFile("RealTimeSymbols", (j.ToString()), (dr["Symbol Name"]).ToString().ToUpper() + "," + (dr["Expiry Date"]).ToString().ToUpper());
                            j++;
                        }
                        else
                        {
                            message = "Not able to add " + dr["Symbol Name"].ToString() + " because expiry date is not proper.";
                            dialog = System.Windows.Forms.MessageBox.Show(new Form { TopMost = true }, message, title, buttons, MessageBoxIcon.Warning);
                            WriteUniquelogs("Mcx Symbol Setting ", message, MessageType.Error);
                        }
                    }
                    else
                    {
                        flagSymbolname = false;
                        message = "Not able to add " + dr["Symbol Name"].ToString() + " because symbol name is not proper.";
                        dialog = System.Windows.Forms.MessageBox.Show(new Form { TopMost = true }, message, title, buttons, MessageBoxIcon.Warning);
                        WriteUniquelogs("Mcx Symbol Setting ", message, MessageType.Error);
                    }
                    //Match m = Regex.Match((dr["Expiry Date"]).ToString(), re);
                    //if (m.Success && flagSymbolname == true)
                    //{
                    //    Settings.writeINIFile("RealTimeSymbols", (j.ToString()), (dr["Symbol Name"]).ToString().ToUpper() + "," + (dr["Expiry Date"]).ToString().ToUpper());
                    //    j++;
                    //}
                    //else
                    //{
                    //    if (flagSymbolname != false)
                    //    {
                    //        message = "Not able to add " + dr["Symbol Name"].ToString() + " because expiry date is not proper.";
                    //        dialog = System.Windows.Forms.MessageBox.Show(new Form { TopMost = true }, message, title, buttons, MessageBoxIcon.Warning);
                    //        WriteUniquelogs("Mcx Symbol Setting ", message, MessageType.Error);
                    //    }
                    //}
                }
            }

            if (j == k)
            {
                    //for (int i = 0; i < RealtimeSymbolfromsymbolIni.Count; i++)
                    //{
                    iniObj.clearTestingSymbol("RealTimeSymbols", null, null, inisymbolList);
                    //}
                foreach (DataRow dr in mydtSymbolList.Rows)
                {
                    Settings.writeINIFile("RealTimeSymbols", counter.ToString(), (dr["Symbol Name"]).ToString().ToUpper() + "," + (dr["Expiry Date"]).ToString().ToUpper());
                    counter++;
                }

                message = "Changes updated successfully!!!";
                dialog = System.Windows.Forms.MessageBox.Show(new Form { TopMost = true }, message, title, buttons, MessageBoxIcon.Information);

                if (dialog == DialogResult.OK)
                {
                   // Close();
                }
            }
                WriteUniquelogs("settingLogs" + " ", "SymbolList Setting | Details updated Properly. ", MessageType.Informational);
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "SymbolList Setting | Exception : " + ex, MessageType.Informational);
            }
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            if (dataGriddetails.SelectedIndex != -1)
            {
                DataRow row = mydtSymbolList.NewRow();
                int rank = dataGriddetails.SelectedIndex;
                mydtSymbolList.Rows.InsertAt(row, rank + 1);
                WriteUniquelogs("Mcx Symbol Setting ", "New row inserted at " + (rank + 1) + " position.", MessageType.Informational);
            }
            else
            {
                int lastcount = mydtSymbolList.Rows.Count;
                mydtSymbolList.Rows.Add("", "");
                WriteUniquelogs("Mcx Symbol Setting ", "New row inserted at bottom.", MessageType.Informational);
            }
            WriteUniquelogs("settingLogs" + " ", "SymbolList Setting | New row inserted.", MessageType.Informational);
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            int rank = dataGriddetails.SelectedIndex;
            mydtSymbolList.Rows.RemoveAt(dataGriddetails.SelectedIndex);
            WriteUniquelogs("Mcx Symbol Setting ", "Row deleted at :" + rank + " position.", MessageType.Informational);
        }
        public void WriteUniquelogs(string fileName, string message, MessageType msgType)
        {
            try
            {
                SymbolLogs log = new SymbolLogs();
                log.createFile(fileName);
                log.LogMessage(message, MessageType.Informational);
                log = null;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : " + e.Message);
            }
        }
    }
}
