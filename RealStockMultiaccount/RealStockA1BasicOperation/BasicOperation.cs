﻿using IRDSAlgoOMS;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace RealStockA1Strategy
{
    public class BasicOperation
    {
        OpenOrderInfo m_OpenOrderInfo = null;
        public BasicOperation()
        {
            m_OpenOrderInfo = new OpenOrderInfo();
        }

        public bool AddEntryPrice(string TradingSymbol, double OpenPrice)
        {
            return m_OpenOrderInfo.AddOpenPrice(TradingSymbol, OpenPrice);
        }
       
        public string GetOpenOrderId(string TradingSymbol)
        {
            return m_OpenOrderInfo.GetOpenOrderId(TradingSymbol);
        }
        public DateTime GetTime(string TradingSymbol)
        {
            return m_OpenOrderInfo.GetTime(TradingSymbol);
        }
        public void AddOrUpdateTime(string TradingSymbol,DateTime time)
        {
            m_OpenOrderInfo.AddOrUpdateTime(TradingSymbol,time);
        }
        public string GetStopLossOrderId(string TradingSymbol)
        {
            return m_OpenOrderInfo.GetStopLossOrderId(TradingSymbol);
        }
        public string GetOpenOrderIdWithOptionType(string TradingSymbol,string optionType)
        {
            return m_OpenOrderInfo.GetOpenOrderIdWithOptionType(TradingSymbol, optionType);
        }

        public double GetEntryOrderPriceWithOptionType(string TradingSymbol, string optionType)
        {
            return m_OpenOrderInfo.GetEntryOrderPriceWithOptionType(TradingSymbol, optionType);
        }

        public double GetEntryOrderPrice(string TradingSymbol)
        {
            return m_OpenOrderInfo.GetEntryOrderPrice(TradingSymbol);
        }

        public void AddOrUpdateOrderId(string TradingSymbol, string OrderId, bool isReplace = false)
        {
            m_OpenOrderInfo.AddOrUpdateOrderId(TradingSymbol, OrderId, isReplace);
        }
        public void ReplaceOrderId(string TradingSymbol, string oldOrderId, string newOrderId)
        {
            m_OpenOrderInfo.ReplaceOrderId(TradingSymbol, oldOrderId, newOrderId);
        }
        public void ResetOrderId(string TradingSymbol)
        {
            m_OpenOrderInfo.ResetOrderId(TradingSymbol);
        }

        public void AddInformationIntoLog()
        {
            m_OpenOrderInfo.AddInformationIntoLog();
        }

        public void SaveBinFile(string m_BinFileName)
        {
            Stream SaveFileStream = File.Create(m_BinFileName);
            BinaryFormatter serializer = new BinaryFormatter();
            serializer.Serialize(SaveFileStream, m_OpenOrderInfo);
            SaveFileStream.Close();
        }

        public void LoadStructure(string m_BinFileName)
        {
            Console.WriteLine("Reading saved file");
            Stream openFileStream = File.OpenRead(m_BinFileName);
            if (openFileStream.Length != 0)
            {
                BinaryFormatter deserializer = new BinaryFormatter();
                m_OpenOrderInfo = (OpenOrderInfo)deserializer.Deserialize(openFileStream);
                if (m_OpenOrderInfo != null)
                {
                    m_OpenOrderInfo.AddInformationIntoLog();
                }
                m_OpenOrderInfo.WriteUniquelogs("RestoreStructure", "LoadStructure : Successfully restored structure", MessageType.Informational);
            }
            else
            {
                m_OpenOrderInfo.WriteUniquelogs("RestoreStructure", "LoadStructure : bin file is empty", MessageType.Informational);
            }
            openFileStream.Close();
        }

        public void AddOrUpdateSymbolName(string TradingSymbol, string SymbolName)
        {
            m_OpenOrderInfo.AddOrUpdateSymbolName(TradingSymbol,SymbolName);
        }

        public string GetSymbolName(string TradingSymbol,string optionType)
        {
            return m_OpenOrderInfo.GetSymbolName(TradingSymbol,optionType);
        }

        public string GetSymbolNameWithOptionType(string optionType)
        {
            return m_OpenOrderInfo.GetSymbolNameWithOptionType( optionType);
        }

        public bool GetIsStopLossOrderPlaced(string TradingSymbol)
        {
            return m_OpenOrderInfo.GetIsStopLossOrderPlaced(TradingSymbol);
        }

        public void AddOrUpdateIsStopLossOrderPlaced(string TradingSymbol, bool value)
        {
            m_OpenOrderInfo.AddOrUpdateIsStopLossOrderPlaced(TradingSymbol, value);
        }

        public void AddOrUpdateStopLossOrderId(string TradingSymbol, string orderId)
        {
            m_OpenOrderInfo.AddOrUpdateStopLossOrderId(TradingSymbol, orderId);
        }

        public void AddOrUpdateQuantity(string TradingSymbol, int Quantity)
        {
            m_OpenOrderInfo.AddOrUpdateQuantity(TradingSymbol, Quantity);
        }

        public int GetQuantity(string TradingSymbol)
        {
            return m_OpenOrderInfo.GetQuantity(TradingSymbol);
        }
        public int GetOrderCounter(string TradingSymbol)
        {
            return m_OpenOrderInfo.GetOrderCounter(TradingSymbol);
        }
        public void AddOrUpdateOrderCounter(string TradingSymbol)
        {
            m_OpenOrderInfo.AddOrUpdateOrderCounter(TradingSymbol);
        }

        public Dictionary<string,string> GetOptionSymbols()
        {
            return m_OpenOrderInfo.GetOptionSymbols();
        }

        public void AddOrUpdateOptionSymbols(string TradingSymbol,string Exchange)
        {
            m_OpenOrderInfo.AddOrUpdateOptionSymbols(TradingSymbol, Exchange);
        }

        public string GetCESymbolName(string TradingSymbol)
        {
            return m_OpenOrderInfo.GetCESymbolName(TradingSymbol);
        }
        public string GetPESymbolName(string TradingSymbol)
        {
            return m_OpenOrderInfo.GetPESymbolName(TradingSymbol);
        }

        public bool GetIsOrderModified(string TradingSymbol)
        {
            return m_OpenOrderInfo.GetOrderModified(TradingSymbol);
        }
        public void AddOrUpdateIsOrderModified(string TradingSymbol, bool value)
        {
            m_OpenOrderInfo.AddOrUpdateIsOrderModified(TradingSymbol, value);
        }

        public void AddOrUpdateCompletionList(string TradingSymbol, string symbol,bool value)
        {
            m_OpenOrderInfo.AddOrUpdateCompletionList(TradingSymbol, symbol, value);
        }

        public bool GetCycleCompletionFlag(string TradingSymbol)
        {
            return m_OpenOrderInfo.GetCycleCompletionFlag(TradingSymbol);
        }
        public string GetOrderType(string TradingSymbol)
        {
            return m_OpenOrderInfo.GetOrderType(TradingSymbol);
        }
        public void AddOrUpdateOrderType(string TradingSymbol, string orderType)
        {
            m_OpenOrderInfo.AddOrUpdateOrderType(TradingSymbol, orderType);
        }
        public double GetMarginLimit(string TradingSymbol)
        {
            return m_OpenOrderInfo.GetMarginLimit(TradingSymbol);
        }
        public void AddOrUpdateMarginLimit(string TradingSymbol, double marginLimit)
        {
            m_OpenOrderInfo.AddOrUpdateMarginLimit(TradingSymbol, marginLimit);
        }
        public void AddOrUpdateEntryOrderTime(string TradingSymbol, DateTime Time)
        {
            m_OpenOrderInfo.AddOrUpdateEntryOrderTime(TradingSymbol, Time);
        }
        public DateTime GetEntryOrderTime(string TradingSymbol)
        {
            return m_OpenOrderInfo.GetEntryOrderTime(TradingSymbol);
        }
        public DateTime GetSLOrderTime(string TradingSymbol)
        {
            return m_OpenOrderInfo.GetSLOrderTime(TradingSymbol);
        }
        public void AddOrUpdateSLOrderTime(string TradingSymbol, DateTime Time)
        {
            m_OpenOrderInfo.AddOrUpdateSLOrderTime(TradingSymbol, Time);
        }
        public bool IsMarketOrderPlaced(string TradingSymbol)
        {
            return m_OpenOrderInfo.IsMarketOrderPlaced(TradingSymbol);
        }
        public void AddOrUpdateMarketOrderPlaced(string TradingSymbol, bool value)
        {
            m_OpenOrderInfo.AddOrUpdateMarketOrderPlaced(TradingSymbol, value);
        }

        public void AddOrUpdateNoOfLotSize(string TradingSymbol, int noOfLotSize)
        {
            m_OpenOrderInfo.AddOrUpdateNoOfLotSize(TradingSymbol, noOfLotSize);
        }

        public void AddOrUpdateQuantityMultiplier(string TradingSymbol, double quantity)
        {
            m_OpenOrderInfo.AddOrUpdateQuantityMultiplier(TradingSymbol, quantity);
        }

        public double GetQuantityMultiplier(string TradingSymbol)
        {
            return m_OpenOrderInfo.GetQuantityMultiplier(TradingSymbol);
        }

        public int GetNoOfLotSize(string TradingSymbol)
        {
            return m_OpenOrderInfo.GetNoOfLotSize(TradingSymbol);
        }

        public void ClearOptionSymbols()
        {
            m_OpenOrderInfo.ClearOptionSymbols();
        }
        public void ResetStorage()
        {
            m_OpenOrderInfo.ResetStorage();
        }
        public Dictionary<string, string> GetOriginalSymbols()
        {
            return m_OpenOrderInfo.GetOriginalSymbols();
        }
        public void AddOrUpdateOriginalSymbols(string TradingSymbol, string Exchange)
        {
            m_OpenOrderInfo.AddOrUpdateOriginalSymbols(TradingSymbol, Exchange);
        }
        public void AddOrUpdateStopLoss(string TradingSymbol, double StopLoss)
        {
            m_OpenOrderInfo.AddOrUpdateStopLoss(TradingSymbol, StopLoss);
        }
        public double GetStopLoss(string TradingSymbol)
        {
            return m_OpenOrderInfo.GetStopLoss(TradingSymbol);
        }
        public void AddOrUpdateTrasactionType(string TradingSymbol, string type)
        {
            m_OpenOrderInfo.AddOrUpdateTrasactionType(TradingSymbol, type);
        }

        public string GetTrasactionType(string TradingSymbol)
        {
            return m_OpenOrderInfo.GetTrasactionType(TradingSymbol);
        }

        public void AddOrUpdateSymbolsList(string TradingSymbol, string CESymbol, string PESymbol)
        {
            m_OpenOrderInfo.AddOrUpdateSymbolsList(TradingSymbol, CESymbol, PESymbol);
        }

        public List<string> GetSymbolsList(string TradingSymbol)
        {
            return m_OpenOrderInfo.GetSymbolsList(TradingSymbol);
        }
    }
}
