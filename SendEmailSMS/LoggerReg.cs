﻿using System;
using System.IO;

namespace SendEmailSMS
{
    public sealed class LoggerReg
    {
        public static string symbol = "";
        public string logFilePath = "";
        public string directoryName = "";
        public StreamWriter swDefault;

        private static LoggerReg instance = null;
        private static readonly object padlock = new object();

        public enum MessageType
        {
            Informational,
            Error,
            Exception
        }
        public const string LogFileSeperator = "|";
        public LoggerReg()
        {
            string todaysDate = string.Format("{0:dd-MM-yyyy}", DateTime.Now);
            string fileName = string.Format("{0:hh-mm-ss-tt}.txt", DateTime.Now);

            var path = Directory.GetCurrentDirectory();
            directoryName = path + "\\" + "Logs" + "\\" + todaysDate;

            if (!Directory.Exists(directoryName))
                Directory.CreateDirectory(directoryName);

            logFilePath = directoryName + "\\" + fileName;
            swDefault = new StreamWriter(logFilePath, true);
        }

        public static LoggerReg Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (padlock)
                    {
                        if (instance == null)
                        {
                            instance = new LoggerReg();
                        }
                    }
                }
                return instance;
            }
        }

        public string GetFormattedString(string message, MessageType msgType)
        {
            string msgTypeString = "";
            switch (msgType)
            {
                case MessageType.Informational:
                    {
                        msgTypeString = "Informational";
                    }
                    break;

                case MessageType.Error:
                    {
                        msgTypeString = "Error";
                    }
                    break;

                case MessageType.Exception:
                    {
                        msgTypeString = "Exception";
                    }
                    break;
            }

            var formattetString = DateTime.Now.ToString() + LogFileSeperator + msgTypeString + LogFileSeperator + message;
            return formattetString;
        }
    }
}
