﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Amazon;
using Amazon.Pinpoint;
using MailKit.Net.Smtp;
using MimeKit;
using Amazon.Pinpoint.Model;
using MailKit.Security;

namespace SendEmailSMS
{
    public class SendInfo
    {
        ConfigSettings m_ConfigObj = null;

        public bool SendEmailViaMailkit(string emailid, string EmailSubject, string EmailBody, string attach1 = "", string attach2 = "")
        {
            string attachment1 = attach1;
            string attachment2 = attach2;
            try
            {
                //var mailMessage = new MimeMessage();
                //mailMessage.From.Add(new MailboxAddress("Intellirich Devsoft Team", "pratiksha.aware@intellirichdevsoft.com"));
                //mailMessage.To.Add(new MailboxAddress("", emailid));
                //mailMessage.Subject = EmailSubject;
                //mailMessage.Body = new TextPart("plain")
                //{
                //    Text = EmailBody
                //};

                //using (var smtpClient = new MailKit.Net.Smtp.SmtpClient())
                //{
                //    smtpClient.Connect("smtpout.asia.secureserver.net", 465, true);
                //    smtpClient.Authenticate("pratiksha.aware@intellirichdevsoft.com", "test123456");
                //    smtpClient.Send(mailMessage);
                //    smtpClient.Disconnect(true);
                //    WriteUniquelogs("SendEmailSms", "SendEmailViaMailkit : Sms send successfully on " + emailid + ".", MessageType.TRANSACTIONAL);
                //    return true;
                //}
                if (m_ConfigObj == null)
                {
                    m_ConfigObj = new ConfigSettings();
                }
                var mailMessage = new MimeMessage();
                mailMessage.From.Add(new MailboxAddress(m_ConfigObj.m_senderName, m_ConfigObj.m_senderemail));
                mailMessage.To.Add(new MailboxAddress("", emailid));
                mailMessage.Subject = EmailSubject;
                var body = new TextPart("plain")
                {
                    Text = EmailBody
                };

                var multipart = new Multipart("mixed");
                if (attachment1.Length > 1)
                {
                    var attachment3 = new MimePart("csv", "csv")
                    {
                        Content = new MimeContent(File.OpenRead(attachment1)),
                        ContentDisposition = new ContentDisposition(ContentDisposition.Attachment),
                        ContentTransferEncoding = ContentEncoding.Base64,
                        FileName = Path.GetFileName(attachment1)
                    };
                    var attachment4 = new MimePart("csv", "csv")
                    {
                        Content = new MimeContent(File.OpenRead(attachment2)),
                        ContentDisposition = new ContentDisposition(ContentDisposition.Attachment),
                        ContentTransferEncoding = ContentEncoding.Base64,
                        FileName = Path.GetFileName(attachment2)
                    };
                    multipart.Add(body);
                    multipart.Add(attachment3);
                    multipart.Add(attachment4);
                    mailMessage.Body = multipart;
                }
                else
                {
                    multipart.Add(body);
                    mailMessage.Body = multipart;
                }
                string host = m_ConfigObj.m_senderhost;
                int port = m_ConfigObj.m_senderPort;
                using (var smtpClient = new MailKit.Net.Smtp.SmtpClient())
                {
                    smtpClient.Connect(host, port, SecureSocketOptions.StartTls);
                    smtpClient.Authenticate(m_ConfigObj.m_senderemail, m_ConfigObj.m_senderpassword);
                    smtpClient.Send(mailMessage);
                    smtpClient.Disconnect(true);
                    WriteUniquelogs("SendEmailSms", "SendEmailViaMailkit : Sms send successfully on " + emailid + ".", MessageType.TRANSACTIONAL);
                    return true;
                }

            }
            catch (Exception ex)
            {
                WriteUniquelogs("SendEmailSms", "SendEmail : Exception Error Message = " + ex.Message, MessageType.TRANSACTIONAL);
                return false;
            }
            return false;
        }
      
        public bool SendSMS(string Number, string content)
        {
            try
            {
                using (AmazonPinpointClient client = new AmazonPinpointClient("AKIAJFQCAHE2TMI6FOOA", "wTx5CbBKwUGhtG5luL6waBKeSNVxm0k21hAQlBi6", RegionEndpoint.GetBySystemName("us-east-1")))
                {
                    SendMessagesRequest sendRequest = new SendMessagesRequest
                    {
                        ApplicationId = "212bd461e6bb402c87ed7bb42ed22841",
                        MessageRequest = new MessageRequest
                        {
                            Addresses = new Dictionary<string, AddressConfiguration>
                        {
                            {
                                Number,
                                new AddressConfiguration
                                {
                                    ChannelType = "SMS"
                                }
                            }
                        },
                            MessageConfiguration = new DirectMessageConfiguration
                            {
                                SMSMessage = new SMSMessage
                                {
                                    Body = content,
                                    MessageType = "TRANSACTIONAL",
                                    SenderId = "ISFT",
                                    Keyword = "IRDSIS"
                                }
                            }
                        }
                    };
                    try
                    {
                        SendMessagesResponse response = client.SendMessages(sendRequest);
                        WriteUniquelogs("SendEmailSms", "SendSMS : Sms send successfully on " + Number + ".", MessageType.TRANSACTIONAL);
                    }
                    catch (Exception ex)
                    {
                        WriteUniquelogs("SendEmailSms", "SendSMS : Exception Error Message = " + ex.Message, MessageType.TRANSACTIONAL);
                    }
                }

            }
            catch (Exception ex)
            {
                WriteUniquelogs("SendEmailSms", "SendSMS : Exception Error Message = " + ex.Message, MessageType.TRANSACTIONAL);
            }
            return false;
        }
        public void WriteUniquelogs(string symbol, string message, MessageType msgType)
        {
            try
            {
                if (m_ConfigObj == null)
                {
                    m_ConfigObj = new ConfigSettings();
                }
                m_ConfigObj.createFile(symbol);
                m_ConfigObj.LogMessagestring(message, "Informational");
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : " + e.Message);
                Trace.WriteLine("Exception : " + e.Message);
            }
        }
    }
}
