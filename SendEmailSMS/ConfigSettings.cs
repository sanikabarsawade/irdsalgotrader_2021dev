﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using static SendEmailSMS.LoggerReg;
namespace SendEmailSMS
{
    public class ConfigSettings
    {
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);

        string directoryName = "";
        string logFilePath = "";
        //added for read date from registration.info
        public string strExpiryDate = "";
        public string strName = "";
        public string strDate = "";

        //title for form and messagebox
        public string m_Title = "";
        public const string LogFileSeperator = "|";
        static string path = Directory.GetCurrentDirectory();
        string iniFileSetting = path + @"\Configuration\" + "Setting.ini";
        ConfigSettings Settings = null;
        public string m_senderName = "";
        public string m_senderemail = "";
        public string m_senderpassword = "";
        public string m_senderhost = "";
        public int m_senderPort = 0;
        //IRDSPM::Pratiksha::15-07-2021::For smtp encrypt and decrypt
        List<string> SMTPDecryptDetList;
        List<string> SMTPEncDetList;
        public ConfigSettings()
        {
            //IRDSPM::Pratiksha::19-07-2021::For email deatils
            if (File.Exists(iniFileSetting))
            {
                ReadSettingConfigFile(iniFileSetting);
            }
        }

        public bool ReadSettingConfigFile(string filename)
        {
            SMTPEncDetList = new List<string>();
            bool isload = true;
            try
            {
                string name = IniReadValue("SMTP", "senderName", filename);
                string email = IniReadValue("SMTP", "senderemail", filename);
                string pwd = IniReadValue("SMTP", "senderpassword", filename);
                string host = IniReadValue("SMTP", "senderhost", filename);
                string port = IniReadValue("SMTP", "senderPort", filename);

                if (name != "XXXXXX" && email != "XXXXXX" && pwd != "XXXXXX" && host != "XXXXXX" && port != "XXXXXX")
                {
                    SMTPEncDetList.Add(name); SMTPEncDetList.Add(email); SMTPEncDetList.Add(pwd); SMTPEncDetList.Add(host); SMTPEncDetList.Add(port);
                    decryptDetSMTP(SMTPEncDetList);
                    m_senderName = SMTPDecryptDetList[0];
                    m_senderemail = SMTPDecryptDetList[1];
                    m_senderpassword = SMTPDecryptDetList[2];
                    m_senderhost = SMTPDecryptDetList[3];
                    m_senderPort = Convert.ToInt32(SMTPDecryptDetList[4]);
                }
                else
                {
                    m_senderName = name;
                    m_senderemail = email;
                    m_senderpassword = pwd;
                    m_senderhost = host;
                    m_senderPort = Convert.ToInt32(port);
                }

            }
            catch (Exception ex)
            {
                WriteUniquelogs("ReadSettingConfigFile", "read Setting ConfigFile - exception" + ex.Message, MessageType.Informational);
            }
            return isload;
        }
        public string IniReadValue(string Section, string Key, string filename)
        {
            StringBuilder temp = new StringBuilder(255);
            int i = GetPrivateProfileString(Section, Key, "", temp, 255, filename);
            return temp.ToString();
        }
        //IRDSPM::Pratiksha::24-11-2020::to decypt details
        public void decryptDetSMTP(List<string> EncDetList)
        {
            try
            {
                SMTPDecryptDetList = new List<string>();
                for (int i = 0; i < EncDetList.Count; i++)
                {
                    string plaintext = DecryptData(EncDetList[i], "IrdsalgoTrader");
                    SMTPDecryptDetList.Add(plaintext);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs("ReadConfigSettings", "decryptDet : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }
        public static string DecryptData(string strCryptTxt, string strKey)
        {
            strCryptTxt = strCryptTxt.Replace(" ", "+");

            byte[] bytesBuff = Convert.FromBase64String(strCryptTxt);

            using (Aes aes__1 = Aes.Create())
            {
                Rfc2898DeriveBytes crypto = new Rfc2898DeriveBytes(strKey, new byte[] {
                0x49,
                0x76,
                0x61,
                0x6e,
                0x20,
                0x4d,
                0x65,
                0x64,
                0x76,
                0x65,
                0x64,
                0x65,
                0x76
            });
                aes__1.Key = crypto.GetBytes(32);
                aes__1.IV = crypto.GetBytes(16);

                using (MemoryStream mStream = new MemoryStream())
                {
                    using (CryptoStream cStream = new CryptoStream(mStream, aes__1.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cStream.Write(bytesBuff, 0, bytesBuff.Length);
                        cStream.Close();
                    }

                    strCryptTxt = Encoding.Unicode.GetString(mStream.ToArray());
                }
            }
            return strCryptTxt;
        }
        public void createFile(string symbol)
        {
            string todaysDate = string.Format("{0:dd-MM-yyyy}", DateTime.Now);
            string todaysDateTime = string.Format("{0:dd-MM-yyyy:hh-mm-ss-tt}", DateTime.Now);
            string fileName = symbol + ".txt";

            var path = Directory.GetCurrentDirectory();
            directoryName = path + "\\" + "Logs" + "\\" + todaysDate;

            if (!Directory.Exists(directoryName))
                Directory.CreateDirectory(directoryName);

            logFilePath = directoryName + "\\" + fileName;
        }
        public void WriteUniquelogs(string symbol, string message, MessageType msgType)
        {
            try
            {
                createFile(symbol);
                LogMessage(message, MessageType.Informational);
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : " + e.Message);
                Trace.WriteLine("Exception : " + e.Message);
            }
        }
        public void LogMessage(string message, MessageType msgType)
        {
            using (StreamWriter streamWriter = new StreamWriter(logFilePath, true))
            {
                streamWriter.WriteLine(GetFormattedString(message, msgType));
                streamWriter.Close();
            }
        }

        public void LogMessagestring(string message, string msgType)
        {
            using (StreamWriter streamWriter = new StreamWriter(logFilePath, true))
            {
                streamWriter.WriteLine(GetFormattedStringstr(message, msgType));
                streamWriter.Close();
            }
        }
        public string GetFormattedString(string message, MessageType msgType)
        {
            string msgTypeString = "";
            switch (msgType)
            {
                case MessageType.Informational:
                    {
                        msgTypeString = "Informational";
                    }
                    break;

                case MessageType.Error:
                    {
                        msgTypeString = "Error";
                    }
                    break;

                case MessageType.Exception:
                    {
                        msgTypeString = "Exception";
                    }
                    break;
            }

            var formattetString = DateTime.Now.ToString() + LogFileSeperator + msgTypeString + LogFileSeperator + message;
            return formattetString;
        }
        public string GetFormattedStringstr(string message, string msgType)
        {
            string msgTypeString = "";
            switch (msgType)
            {
                case "Informational":
                    {
                        msgTypeString = "Informational";
                    }
                    break;

                case "Error":
                    {
                        msgTypeString = "Error";
                    }
                    break;

                case "Exception":
                    {
                        msgTypeString = "Exception";
                    }
                    break;
            }

            var formattetString = DateTime.Now.ToString() + LogFileSeperator + msgTypeString + LogFileSeperator + message;
            return formattetString;
        }
    }
}
