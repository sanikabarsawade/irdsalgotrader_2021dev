﻿namespace RealStockA1Strategy
{
    partial class AutoTradingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AutoTradingForm));
            this.btnLogin = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.btnHistorical = new System.Windows.Forms.Button();
            this.btnStopCloseOrder = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.ChanBreakStrategy = new System.Windows.Forms.CheckBox();
            this.btnRealStockStrategy1 = new System.Windows.Forms.Button();
            this.btnRealStockStrategy2 = new System.Windows.Forms.Button();
            this.btnRealStockStrategy3 = new System.Windows.Forms.Button();
            this.btnRealStockStrategy4 = new System.Windows.Forms.Button();
            this.btnRealStockStrategy5 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbtnZerodha = new System.Windows.Forms.RadioButton();
            this.rbtnComposite = new System.Windows.Forms.RadioButton();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.settingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.accountSettingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TabRealStockStrategy = new System.Windows.Forms.TabControl();
            this.Tab1 = new System.Windows.Forms.TabPage();
            this.Tab2 = new System.Windows.Forms.TabPage();
            this.Tab3 = new System.Windows.Forms.TabPage();
            this.Tab4 = new System.Windows.Forms.TabPage();
            this.Tab5 = new System.Windows.Forms.TabPage();
            this.btnClose = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.TabRealStockStrategy.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnLogin
            // 
            this.btnLogin.Location = new System.Drawing.Point(0, 0);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(75, 23);
            this.btnLogin.TabIndex = 2;
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(0, 0);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 0;
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(0, 0);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(75, 23);
            this.btnStop.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.btnHistorical);
            this.groupBox2.Controls.Add(this.btnLogin);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(430, 87);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(0, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            // 
            // btnHistorical
            // 
            this.btnHistorical.Location = new System.Drawing.Point(0, 0);
            this.btnHistorical.Name = "btnHistorical";
            this.btnHistorical.Size = new System.Drawing.Size(75, 23);
            this.btnHistorical.TabIndex = 1;
            // 
            // btnStopCloseOrder
            // 
            this.btnStopCloseOrder.Location = new System.Drawing.Point(0, 0);
            this.btnStopCloseOrder.Name = "btnStopCloseOrder";
            this.btnStopCloseOrder.Size = new System.Drawing.Size(75, 23);
            this.btnStopCloseOrder.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.ChanBreakStrategy);
            this.groupBox3.Location = new System.Drawing.Point(18, 162);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(430, 93);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Strategy Selection";
            // 
            // ChanBreakStrategy
            // 
            this.ChanBreakStrategy.Location = new System.Drawing.Point(0, 0);
            this.ChanBreakStrategy.Name = "ChanBreakStrategy";
            this.ChanBreakStrategy.Size = new System.Drawing.Size(104, 24);
            this.ChanBreakStrategy.TabIndex = 0;
            // 
            // btnRealStockStrategy1
            // 
            this.btnRealStockStrategy1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRealStockStrategy1.Location = new System.Drawing.Point(32, 127);
            this.btnRealStockStrategy1.Name = "btnRealStockStrategy1";
            this.btnRealStockStrategy1.Size = new System.Drawing.Size(190, 36);
            this.btnRealStockStrategy1.TabIndex = 3;
            this.btnRealStockStrategy1.Text = "RealStock Strategy 1";
            this.btnRealStockStrategy1.UseVisualStyleBackColor = true;
            this.btnRealStockStrategy1.Visible = false;
            this.btnRealStockStrategy1.Click += new System.EventHandler(this.btnRealStockStrategy1_Click);
            // 
            // btnRealStockStrategy2
            // 
            this.btnRealStockStrategy2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRealStockStrategy2.Location = new System.Drawing.Point(296, 127);
            this.btnRealStockStrategy2.Name = "btnRealStockStrategy2";
            this.btnRealStockStrategy2.Size = new System.Drawing.Size(190, 36);
            this.btnRealStockStrategy2.TabIndex = 4;
            this.btnRealStockStrategy2.Text = "RealStock Strategy 2";
            this.btnRealStockStrategy2.UseVisualStyleBackColor = true;
            this.btnRealStockStrategy2.Visible = false;
            this.btnRealStockStrategy2.Click += new System.EventHandler(this.btnRealStockStrategy2_Click);
            // 
            // btnRealStockStrategy3
            // 
            this.btnRealStockStrategy3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRealStockStrategy3.Location = new System.Drawing.Point(560, 127);
            this.btnRealStockStrategy3.Name = "btnRealStockStrategy3";
            this.btnRealStockStrategy3.Size = new System.Drawing.Size(190, 36);
            this.btnRealStockStrategy3.TabIndex = 5;
            this.btnRealStockStrategy3.Text = "RealStock Strategy 3";
            this.btnRealStockStrategy3.UseVisualStyleBackColor = true;
            this.btnRealStockStrategy3.Visible = false;
            this.btnRealStockStrategy3.Click += new System.EventHandler(this.btnRealStockStrategy3_Click);
            // 
            // btnRealStockStrategy4
            // 
            this.btnRealStockStrategy4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRealStockStrategy4.Location = new System.Drawing.Point(815, 127);
            this.btnRealStockStrategy4.Name = "btnRealStockStrategy4";
            this.btnRealStockStrategy4.Size = new System.Drawing.Size(190, 36);
            this.btnRealStockStrategy4.TabIndex = 6;
            this.btnRealStockStrategy4.Text = "RealStock Strategy 4";
            this.btnRealStockStrategy4.UseVisualStyleBackColor = true;
            this.btnRealStockStrategy4.Visible = false;
            this.btnRealStockStrategy4.Click += new System.EventHandler(this.btnRealStockStrategy4_Click);
            // 
            // btnRealStockStrategy5
            // 
            this.btnRealStockStrategy5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRealStockStrategy5.Location = new System.Drawing.Point(1082, 127);
            this.btnRealStockStrategy5.Name = "btnRealStockStrategy5";
            this.btnRealStockStrategy5.Size = new System.Drawing.Size(190, 36);
            this.btnRealStockStrategy5.TabIndex = 7;
            this.btnRealStockStrategy5.Text = "RealStock Strategy 5";
            this.btnRealStockStrategy5.UseVisualStyleBackColor = true;
            this.btnRealStockStrategy5.Visible = false;
            this.btnRealStockStrategy5.Click += new System.EventHandler(this.btnRealStockStrategy5_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbtnZerodha);
            this.groupBox1.Controls.Add(this.rbtnComposite);
            this.groupBox1.Enabled = false;
            this.groupBox1.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(23, 44);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1773, 91);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Set Broker";
            // 
            // rbtnZerodha
            // 
            this.rbtnZerodha.AutoSize = true;
            this.rbtnZerodha.Enabled = false;
            this.rbtnZerodha.Font = new System.Drawing.Font("Arial", 12F);
            this.rbtnZerodha.Location = new System.Drawing.Point(474, 35);
            this.rbtnZerodha.Name = "rbtnZerodha";
            this.rbtnZerodha.Size = new System.Drawing.Size(104, 27);
            this.rbtnZerodha.TabIndex = 1;
            this.rbtnZerodha.Text = "Zerodha";
            this.rbtnZerodha.UseVisualStyleBackColor = true;
            this.rbtnZerodha.CheckedChanged += new System.EventHandler(this.rbtnZerodha_CheckedChanged);
            // 
            // rbtnComposite
            // 
            this.rbtnComposite.AutoSize = true;
            this.rbtnComposite.Checked = true;
            this.rbtnComposite.Font = new System.Drawing.Font("Arial", 12F);
            this.rbtnComposite.Location = new System.Drawing.Point(1125, 35);
            this.rbtnComposite.Name = "rbtnComposite";
            this.rbtnComposite.Size = new System.Drawing.Size(125, 27);
            this.rbtnComposite.TabIndex = 2;
            this.rbtnComposite.TabStop = true;
            this.rbtnComposite.Text = "Composite";
            this.rbtnComposite.UseVisualStyleBackColor = true;
            this.rbtnComposite.CheckedChanged += new System.EventHandler(this.rbtnComposite_CheckedChanged);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("Arial", 12F);
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1819, 31);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.TabStop = true;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // settingToolStripMenuItem
            // 
            this.settingToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.accountSettingToolStripMenuItem});
            this.settingToolStripMenuItem.Name = "settingToolStripMenuItem";
            this.settingToolStripMenuItem.ShortcutKeyDisplayString = "";
            this.settingToolStripMenuItem.Size = new System.Drawing.Size(85, 27);
            this.settingToolStripMenuItem.Text = "Setting";
            // 
            // accountSettingToolStripMenuItem
            // 
            this.accountSettingToolStripMenuItem.Name = "accountSettingToolStripMenuItem";
            this.accountSettingToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+F1";
            this.accountSettingToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F1)));
            this.accountSettingToolStripMenuItem.Size = new System.Drawing.Size(307, 28);
            this.accountSettingToolStripMenuItem.Text = "Account Setting";
            this.accountSettingToolStripMenuItem.Click += new System.EventHandler(this.accountSettingToolStripMenuItem_Click);
            // 
            // TabRealStockStrategy
            // 
            this.TabRealStockStrategy.Controls.Add(this.Tab1);
            this.TabRealStockStrategy.Controls.Add(this.Tab2);
            this.TabRealStockStrategy.Controls.Add(this.Tab3);
            this.TabRealStockStrategy.Controls.Add(this.Tab4);
            this.TabRealStockStrategy.Controls.Add(this.Tab5);
            this.TabRealStockStrategy.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed;
            this.TabRealStockStrategy.Font = new System.Drawing.Font("Arial", 11F);
            this.TabRealStockStrategy.ItemSize = new System.Drawing.Size(352, 37);
            this.TabRealStockStrategy.Location = new System.Drawing.Point(22, 44);
            this.TabRealStockStrategy.Name = "TabRealStockStrategy";
            this.TabRealStockStrategy.SelectedIndex = 0;
            this.TabRealStockStrategy.Size = new System.Drawing.Size(1774, 922);
            this.TabRealStockStrategy.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.TabRealStockStrategy.TabIndex = 8;
            this.TabRealStockStrategy.DrawItem += tabControl1_DrawItem;
            this.TabRealStockStrategy.SelectedIndexChanged += new System.EventHandler(this.TabRealStockStrategy_SelectedIndexChanged);
            // 
            // Tab1
            // 
            this.Tab1.Location = new System.Drawing.Point(4, 41);
            this.Tab1.Name = "Tab1";
            this.Tab1.Padding = new System.Windows.Forms.Padding(3);
            this.Tab1.Size = new System.Drawing.Size(1766, 877);
            this.Tab1.TabIndex = 0;
            this.Tab1.Text = "RealStock Strategy 1";
            this.Tab1.UseVisualStyleBackColor = true;
            // 
            // Tab2
            // 
            this.Tab2.Location = new System.Drawing.Point(4, 41);
            this.Tab2.Name = "Tab2";
            this.Tab2.Padding = new System.Windows.Forms.Padding(3);
            this.Tab2.Size = new System.Drawing.Size(1766, 877);
            this.Tab2.TabIndex = 1;
            this.Tab2.Text = "RealStock Strategy 2";
            this.Tab2.UseVisualStyleBackColor = true;
            // 
            // Tab3
            // 
            this.Tab3.Location = new System.Drawing.Point(4, 41);
            this.Tab3.Name = "Tab3";
            this.Tab3.Size = new System.Drawing.Size(1766, 877);
            this.Tab3.TabIndex = 2;
            this.Tab3.Text = "RealStock Strategy 3";
            this.Tab3.UseVisualStyleBackColor = true;
            // 
            // Tab4
            // 
            this.Tab4.Location = new System.Drawing.Point(4, 41);
            this.Tab4.Name = "Tab4";
            this.Tab4.Size = new System.Drawing.Size(1766, 877);
            this.Tab4.TabIndex = 3;
            this.Tab4.Text = "RealStock Strategy 4";
            this.Tab4.UseVisualStyleBackColor = true;
            // 
            // Tab5
            // 
            this.Tab5.Location = new System.Drawing.Point(4, 41);
            this.Tab5.Name = "Tab5";
            this.Tab5.Size = new System.Drawing.Size(1766, 877);
            this.Tab5.TabIndex = 4;
            this.Tab5.Text = "RealStock Strategy 5";
            this.Tab5.UseVisualStyleBackColor = true;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Arial", 11F);
            this.btnClose.Location = new System.Drawing.Point(1600, 992);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(157, 35);
            this.btnClose.TabIndex = 9;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Highlight;
            this.panel1.Location = new System.Drawing.Point(22, 44);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(437, 100);
            this.panel1.TabIndex = 3;
            // 
            // AutoTradingForm
            // 
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1819, 1055);
            this.Controls.Add(this.TabRealStockStrategy);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnRealStockStrategy1);
            this.Controls.Add(this.btnRealStockStrategy2);
            this.Controls.Add(this.btnRealStockStrategy3);
            this.Controls.Add(this.btnRealStockStrategy4);
            this.Controls.Add(this.btnRealStockStrategy5);
            this.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "AutoTradingForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Algo Trading";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AutoTradingForm_FormClosing);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.TabRealStockStrategy.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        //private void TabRealStockStrategy_DrawItem1(object sender, System.Windows.Forms.DrawItemEventArgs e)
        //{
        //    throw new System.NotImplementedException();
        //}

        #endregion

        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox ChanBreakStrategy;
        private System.Windows.Forms.Button btnStopCloseOrder;
        private System.Windows.Forms.Button btnHistorical;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnRealStockStrategy1;
        private System.Windows.Forms.Button btnRealStockStrategy2;
        private System.Windows.Forms.Button btnRealStockStrategy3;
        private System.Windows.Forms.Button btnRealStockStrategy4;
        private System.Windows.Forms.Button btnRealStockStrategy5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbtnComposite;
        private System.Windows.Forms.RadioButton rbtnZerodha;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem settingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem accountSettingToolStripMenuItem;
        private System.Windows.Forms.TabControl TabRealStockStrategy;
        private System.Windows.Forms.TabPage Tab1;
        private System.Windows.Forms.TabPage Tab2;
        private System.Windows.Forms.TabPage Tab3;
        private System.Windows.Forms.TabPage Tab4;
        private System.Windows.Forms.TabPage Tab5;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Panel panel1;
    }
}

