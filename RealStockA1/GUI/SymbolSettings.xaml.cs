﻿using IRDSAlgoOMS;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace RealStockA1Strategy
{
    public partial class SymbolSettings :   System.Windows.Controls.UserControl
    {
        string inisymbolList;
        dynamic m_kiteWrapperObj;
        ReadSettings objReadSetting;
        Logger logger = Logger.Instance;
        public List<string> SymbolListwithExchangeOnly;
        public List<string> RealtimeSymbolfromsymbolIni;
        //List<string> customsymbollist = new List<string>();

        List<string> NFOSymbolsList;
        List<string> NSESymbolsList;
        List<string> MCXSymbolsList;
        List<string> EquitySymbolsList;
        List<string> OptionSymbolsList;
        List<string> CDSSymbolsList;
        //List<string> ConstantSymbolsList = new List<string>();
        SymbolSettingWindows m_symbolsettingwindowObj = null;
        string message = "";
        string title = "IRDS Algo Trader";
        MessageBoxButtons buttons;
        string m_userID = "";
        DialogResult result;
        string tblName = "";
        string path = "";
        int count = 0;
        INIFile iniObj;
        string m_iniChoose;
        public SymbolSettings(dynamic sample, string userID, SymbolSettingWindows obj, string iniChoose)
        {
            InitializeComponent();
            m_kiteWrapperObj = sample;
            m_symbolsettingwindowObj = obj;
            m_userID = userID;
            m_iniChoose = iniChoose;
            path = Directory.GetCurrentDirectory();
        }
        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //pratiksha::16-oct-2020::call to close search box
                ForClosingSearchlist();
                if (listBoxleft.SelectedItem != null)
                {
                    if (!RealtimeSymbolfromsymbolIni.Contains(listBoxleft.SelectedItem))
                    {
                        if (!listBoxright.Items.Contains(listBoxleft.SelectedItem))
                        {
                            listBoxright.Items.Add(listBoxleft.SelectedItem);
                        }
                        RealtimeSymbolfromsymbolIni.Add(listBoxleft.SelectedItem.ToString());
                    }
                    else
                    {
                        message = listBoxleft.SelectedItem + " symbol already added!!!";
                        buttons = MessageBoxButtons.OK;
                        result = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                    }
                }
                //IRDSPM::PRatiksha::02-10-2020::Added messagebox if user does not select any symbol
                else
                {
                    message = "Please select symbol.";
                    buttons = MessageBoxButtons.OK;
                    result = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "btnAdd_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void btnAddAll_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //pratiksha::16-oct-2020::call to close search box
                ForClosingSearchlist();
                //listBoxright.Items.Clear();

                for (int i = 0; i < listBoxleft.Items.Count; i++)
                {
                    if (!RealtimeSymbolfromsymbolIni.Contains(listBoxleft.Items[i].ToString()))
                    {
                        listBoxright.Items.Add(listBoxleft.Items[i].ToString());
                        RealtimeSymbolfromsymbolIni.Add(listBoxleft.Items[i].ToString());
                    }
                }
                WriteUniquelogs("settingLogs" + " ", "btnAddAll_Click : Added all names in final listbox. ", MessageType.Informational);
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "btnAddAll_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }
        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
            //pratiksha::16-oct-2020::call to close search box
            ForClosingSearchlist();
            if (listBoxright.SelectedItem != null)
            {                
                try
                {
                    string temp = listBoxright.SelectedItem.ToString();
                    listBoxright.Items.Remove(temp);
                    RealtimeSymbolfromsymbolIni.Remove(temp);
                }
                catch (Exception ex)
                {
                        WriteUniquelogs("SymbolSettingLogs" + " ", "btnDelete_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
                }
            }
            //IRDSPM::PRatiksha::02-10-2020::Added messagebox if user does not select any symbol
            else
            {
                message = "Please select symbol.";
                buttons = MessageBoxButtons.OK;
                result = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
            }
        }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs" + " ", "btnDelete_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }
        private void btnDeleteAll_Click(object sender, RoutedEventArgs e)
        {
            try
            {
            //pratiksha::16-oct-2020::call to close search box
            ForClosingSearchlist();
                WriteUniquelogs("SymbolSettingLogs" + " ", "btnDeleteAll_Click : Deleted all names from final listbox.", MessageType.Informational);
            listBoxright.Items.Clear();
            //customsymbollist.Clear();
            RealtimeSymbolfromsymbolIni.Clear();
        }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs" + " ", "btnDeleteAll_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }
        private void NSEradioButton_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                if (searchlistBoxsuggestion.Visibility == Visibility.Visible)
                {
                    searchforSymboltext.Text = "";
                    searchlistBoxsuggestion.Visibility = Visibility.Hidden;
                }

                if (NSEradioButton.IsChecked == true)
                {
                    listBoxleft.Items.Clear();
                    int count = 0;
                    //IRDSPM::Pratiksha::11-11-2020::For adding particular symbol in listboxleft
                    foreach (var item in SymbolListwithExchangeOnly)
                        {
                        string[] symname = item.Split('.');
                        if(symname[1] == Constants.EXCHANGE_NSE)
                            {
                            listBoxleft.Items.Add(symname[0]);
                            count = count + 1;
                        }
                    }
                    for (int i = 0; i < NSESymbolsList.Count && i<=100; i++)
                    {
                        if (!listBoxleft.Items.Contains(NSESymbolsList[i]))
                        {
                            listBoxleft.Items.Add(NSESymbolsList[i]);
                        }
                        else
                        {
                            if (!listBoxleft.Items.Contains(NSESymbolsList[i]))
                            {
                                listBoxleft.Items.Add(NSESymbolsList[i]);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs" + " ", "NSEradioButton_Checked : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }
        private void NFOradioButton_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                if (searchlistBoxsuggestion.Visibility == Visibility.Visible)
                {
                    searchforSymboltext.Text = "";
                    searchlistBoxsuggestion.Visibility = Visibility.Hidden;
                }

                if (NFOradioButton.IsChecked == true)
                {
                    listBoxleft.Items.Clear();
                    int count = 0;
                    //IRDSPM::Pratiksha::11-11-2020::For adding particular symbol in listboxleft
                    foreach (var item in SymbolListwithExchangeOnly)
                    {
                        string[] symname = item.Split('.');
                        if (symname[1] == Constants.EXCHANGE_NFO)
                        {
                            listBoxleft.Items.Add(symname[0]+ tblName);
                            count = count + 1;
                        }
                    }
                    for (int i = 0; i < NFOSymbolsList.Count; i++)
                    {
                        if (!listBoxleft.Items.Contains(NFOSymbolsList[i]))
                        {
                            listBoxleft.Items.Add(NFOSymbolsList[i]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs" + " ", "NFOradioButton_Checked : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }
        private void MCXradioButton_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                if (searchlistBoxsuggestion.Visibility == Visibility.Visible)
                {
                    searchforSymboltext.Text = "";
                    searchlistBoxsuggestion.Visibility = Visibility.Hidden;
                }

                if (MCXradioButton.IsChecked == true)
                {
                    listBoxleft.Items.Clear();
                    int count = 0;
                    //IRDSPM::Pratiksha::11-11-2020::For adding particular symbol in listboxleft
                    foreach (var item in SymbolListwithExchangeOnly)
                    {
                        string[] symname = item.Split('.');
                        if (symname[1] == Constants.EXCHANGE_MCX)
                        {
                            listBoxleft.Items.Add(symname[0]+ tblName);
                            count = count + 1;
                        }
                    }
                    for (int i = 0; i < MCXSymbolsList.Count; i++)
                    {
                        if (!listBoxleft.Items.Contains(MCXSymbolsList[i]))
                        {
                            listBoxleft.Items.Add(MCXSymbolsList[i]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs" + " ", "MCXradioButton_Checked :  Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }
        private void optionsradioButton_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                if (searchlistBoxsuggestion.Visibility == Visibility.Visible)
                {
                    searchforSymboltext.Text = "";
                    searchlistBoxsuggestion.Visibility = Visibility.Hidden;
                }

                if (optionsradioButton.IsChecked == true)
                {
                    listBoxleft.Items.Clear();
                    int count = 0;
                    //IRDSPM::Pratiksha::11-11-2020::For adding particular symbol in listboxleft
                    foreach (var item in SymbolListwithExchangeOnly)
                    {
                        string[] symname = item.Split('.');
                        if (symname[1] == Constants.EXCHANGE_NFO_OPT)
                        {
                            listBoxleft.Items.Add(symname[0]);
                            count = count + 1;
                        }
                    }
                    for (int i = 0; i < OptionSymbolsList.Count && i <= 100; i++)
                    {
                        if (!listBoxleft.Items.Contains(OptionSymbolsList[i]))
                        {
                            listBoxleft.Items.Add(OptionSymbolsList[i]);
                        }
                        else
                        {
                            if (OptionSymbolsList[i].Contains(','))
                            {
                                string[] parts = OptionSymbolsList[i].Split(',');
                                listBoxleft.Items.Add(parts[0]);
                            }
                            else
                            {
                                listBoxleft.Items.Add(OptionSymbolsList[i]);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs" + " ", "optionsradioButton_Checked :  Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }
        private void btncustomAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ForClosingSearchlist();
                string item = customSymbol.Text.ToString().ToUpper();
                //Pratiksha::01-12-2020::For currency
                if ((item.Length > 0) && (NFOSymbolsList.Contains(item) || MCXSymbolsList.Contains(item) || EquitySymbolsList.Contains(item) || OptionSymbolsList.Contains(item)) || CDSSymbolsList.Contains(item))
                {
                    //customsymbollist.Add(customSymbol.Text.ToUpper());
                    if (!listBoxright.Items.Contains(customSymbol.Text.ToUpper()))
                    {
                        listBoxright.Items.Add(customSymbol.Text.ToUpper());
                        RealtimeSymbolfromsymbolIni.Add(customSymbol.Text.ToUpper());
                    }
                    //IRDSPM::PRatiksha::23-12-2020::Added for alert
                    else
                    {
                        message = customSymbol.Text.ToUpper() + " symbol already added!!!";
                        buttons = MessageBoxButtons.OK;
                        result = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    message = "Please enter valid symbol name.";
                    buttons = MessageBoxButtons.OK;
                    result = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "btncustomAdd_Click :  Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        //private void btnClear_Click(object sender, RoutedEventArgs e)
        //        {
        //    try
        //            {
        //        INIFile iniObj = new INIFile(inisymbolList);
        //        message = "Do you want to clear symbols from INI file?";
        //        buttons = MessageBoxButtons.YesNo;
        //        result = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
        //        if (result == DialogResult.Yes)
        //        {
        //            //    for (int i = 0; i < RealtimeSymbolfromsymbolIni.Count; i++)
        //            //{
        //            iniObj.clearTestingSymbol("TRADINGSYMBOL", null, null, inisymbolList);
        //            //        }
        //            listBoxright.Items.Clear();
        //            RealtimeSymbolfromsymbolIni.Clear();
        //            }
        //        WriteUniquelogs("SymbolSettingLogs" + " ", "btnClear_Click :  Reset all Data from symbol setting to ini file", MessageType.Informational);
        //    }
        //    catch (Exception ex)
        //    {
        //        WriteUniquelogs("SymbolSettingLogs" + " ", "btnClear_Click :  Exception Error Message = " + ex.Message, MessageType.Exception);
        //}
        //}
        private void btnApply_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int m = 0;
                message = "Are you sure you want to save the changes?";
                buttons = MessageBoxButtons.YesNo;
                result = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {
                    if (listBoxright.Items.Count > 0)
                    {
                        if (iniObj == null)
                        {
                            iniObj = new INIFile(path);
                        }
                        iniObj.clearTestingSymbol("RealTimeSymbols", null, null, inisymbolList);
                        List<string> FinalSymbolsList = new List<string>();
                        //File.AppendAllText("AllSymbol.txt", "1: " + DateTime.Now + "\n");
                        //for (int i = 0; i < ConstantSymbolsList.Count; i++)
                        //{
                        //    objReadSetting.WriteSymbolList("RealTimeSymbols", m.ToString(), ConstantSymbolsList[i], m_iniChoose);
                        //    m++;
                        //    WriteUniquelogs("settingLogs" + " ", "Symbol setting | Add in ini by apply click\t" + ConstantSymbolsList[i].ToString() + " as " + ConstantSymbolsList[i], MessageType.Informational);
                        //}
                        //File.AppendAllText("AllSymbol.txt", "2: " + DateTime.Now + "\n");
                        for (int k = 0; k < RealtimeSymbolfromsymbolIni.Count; k++)
                        {
                            string sym = RealtimeSymbolfromsymbolIni[k];
                            string newSym = sym + "." + Constants.EXCHANGE_NSE;
                            if (sym.EndsWith("FUT"))
                            {
                                newSym = sym.Substring(0, sym.Length - 8);
                                if (NFOSymbolsList.Contains(sym))
                                {
                                    //sanika::15-oct-2020::changed string to constant variable
                                    newSym = newSym + "." + Constants.EXCHANGE_NFO;
                                }
                                if (MCXSymbolsList.Contains(sym))
                                {
                                    //sanika::15-oct-2020::changed string to constant variable
                                    newSym = newSym + "." + Constants.EXCHANGE_MCX;
                                }
                                //Pratiksha::01-12-2020::For currency symbol add
                                else if (CDSSymbolsList.Contains(sym))
                                {
                                    newSym = sym + "." + Constants.EXCHANGE_CDS;
                                }
                                if (!newSym.Contains("."))
                                {
                                   // foreach (string symIni in objReadSetting.Symbol)
                                    {
                                       // if (newSym == symIni.Split('.')[0])
                                        {
                                        //    newSym = symIni;
                                          //  break;
                                        }
                                    }
                                }
                            }
                            //sanika::15-oct-2020::Added condition for option symbols
                            else if (sym.EndsWith("PE") || sym.EndsWith("CE"))
                            {
                                if (OptionSymbolsList.Contains(sym))
                                {
                                    newSym = sym + "." + Constants.EXCHANGE_NFO_OPT;
                                }
                                //Pratiksha::01-12-2020::For currency symbol add
                                else if (CDSSymbolsList.Contains(sym))
                                {
                                    newSym = sym + "." + Constants.EXCHANGE_CDS;
                                }
                            }
                            else if (CDSSymbolsList.Contains(sym))
                            {
                                newSym = sym + "." + Constants.EXCHANGE_CDS;
                            }
                            FinalSymbolsList.Add(newSym);
                           // objReadSetting.WriteSymbolList("RealTimeSymbols", m.ToString(), newSym, m_iniChoose);
                            m++;
                            WriteUniquelogs("settingLogs" + " ", "Symbol setting | Add in ini by apply click\t" + RealtimeSymbolfromsymbolIni[k].ToString() + " as " + newSym, MessageType.Informational);
                        }
                        //File.AppendAllText("AllSymbol.txt", "3: " + DateTime.Now + "\n");
                        //IRDSPM::Pratiksha::08-12-2020::For storing deleted symbol in list
                       // if (objReadSetting.Symbol.Count > 0)
                        {
                            //IRDSPM::Pratiksha::14-12-2020::for adding symbol in list for delete from market watch
                          //  foreach (string symbol in objReadSetting.Symbol)
                            {
                                //string[] symParts = symbol.Split('.');
                                //string symCheck = symParts[0] + tblName;
                            }
                        }
                        //File.AppendAllText("AllSymbol.txt", "4: " + DateTime.Now + "\n");
                       // m_kiteWrapperObj.CheckTradingSymbolPresentOrNot(FinalSymbolsList);
                        //File.AppendAllText("AllSymbol.txt", "5: " + DateTime.Now + "\n");
                        message = "Saved changes successfully!!!";
                        buttons = MessageBoxButtons.OK;
                        result = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                        if (result == DialogResult.OK)
                        {
                            m_symbolsettingwindowObj.Close();
                        }
                        //File.AppendAllText("AllSymbol.txt", "6: " + DateTime.Now + "\n");
                    }
                    else
                    {
                        //IRDSPM::Pratiksha::07-12-2020::Change spelling of their
                        message = "Sorry, we are unable to save as symbols are not there.";
                        buttons = MessageBoxButtons.OK;
                        result = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                    }
                    //File.AppendAllText("AllSymbol.txt", "7: " + DateTime.Now + "\n");
                }
                else
                {
                    m_symbolsettingwindowObj.Close();
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "btnApply_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
            //File.AppendAllText("AllSymbol.txt", "8: " + DateTime.Now + "\n");
        }
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            if (listBoxright.Items.Count != SymbolListwithExchangeOnly.Count)
            {
                btnApply_Click(sender,e);
            }
            else
            {
                //for(int i = 0; i < listBoxright.Items.Count; i++) { }
                m_symbolsettingwindowObj.Close();
            }
        }

    //private void btnClear_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
    //{
    //    btnClear.Foreground = Brushes.Black;
    //}

    //private void btnClear_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
    //{
    //    btnClear.Foreground = Brushes.White;
    //}

    private void btnApply_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            btnApply.Foreground = Brushes.Black;
        }

        private void btnApply_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            btnApply.Foreground = Brushes.White;
        }

        private void btnCancel_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            btnCancel.Foreground = Brushes.Black;
        }

        private void btnCancel_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            btnCancel.Foreground = Brushes.White;
        }

        private void searchforSymboltext_TextChanged(object sender, TextChangedEventArgs e)
        {
            searchlistBoxsuggestion.Items.Clear();
            listBoxleft.SelectedIndex = -1;
            
            try
            {
                searchlistBoxsuggestion.Visibility = Visibility.Visible;
                if (optionsradioButton.IsChecked == true)
                {
                    for (int i = 0; i <= OptionSymbolsList.Count - 1; i++)
                    {
                        //IRDSPM::Pratiksha::16-10-2020::Because of toower it not sarching for searchbix list.
                        if (OptionSymbolsList[i].ToString().ToUpper().Contains(searchforSymboltext.Text.ToUpper()))
                        {
                            if (!searchlistBoxsuggestion.Items.Contains(OptionSymbolsList[i].ToString()))
                            {
                                searchlistBoxsuggestion.Items.Add(OptionSymbolsList[i].ToString());
                            }
                        }
                    }
                }
                else if (ForexradioButton.IsChecked == true)
                {
                    for (int i = 0; i <= CDSSymbolsList.Count - 1; i++)
                    {
                        if (CDSSymbolsList[i].ToString().ToUpper().Contains(searchforSymboltext.Text.ToUpper()))
                        {
                            if (!searchlistBoxsuggestion.Items.Contains(CDSSymbolsList[i].ToString()))
                            {
                                searchlistBoxsuggestion.Items.Add(CDSSymbolsList[i].ToString());
                            }
                        }
                    }
                }
                else if(NSEradioButton.IsChecked == true)
                {
                    for (int i = 0; i <= NSESymbolsList.Count - 1; i++)
                    {
                        //IRDSPM::Pratiksha::16-10-2020::Because of toower it not sarching for searchbix list.
                        if (NSESymbolsList[i].ToString().ToUpper().Contains(searchforSymboltext.Text.ToUpper()))
                        {
                            if (!searchlistBoxsuggestion.Items.Contains(NSESymbolsList[i].ToString()))
                            {
                                searchlistBoxsuggestion.Items.Add(NSESymbolsList[i].ToString());
                            }
                        }
                    }
                }
                else
                {
                    foreach (var symbolname in listBoxleft.Items)
                    {
                        if (symbolname.ToString().ToLower().Contains(searchforSymboltext.Text.ToLower()))
                        {
                            searchlistBoxsuggestion.Items.Add(symbolname);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs" + " ", "searchforSymboltext_TextChanged : Exception Error Message =" + ex.Message, MessageType.Exception);
            }
            if (searchforSymboltext.Text.Length == 0)
            {
                searchlistBoxsuggestion.Visibility = Visibility.Hidden;
            }
        }

        private void searchlistBoxsuggestion_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (searchlistBoxsuggestion.SelectedItem != null)
                {
                    if (!RealtimeSymbolfromsymbolIni.Contains(searchlistBoxsuggestion.SelectedItem))
                    {
                        listBoxright.Items.Add(searchlistBoxsuggestion.SelectedItem);
                        RealtimeSymbolfromsymbolIni.Add(searchlistBoxsuggestion.SelectedItem.ToString());
                    }
                    //IRDSPM::PRatiksha::23-12-2020::Added for alert
                    else
                    {
                        message = searchlistBoxsuggestion.SelectedItem + " symbol already added!!!";
                        buttons = MessageBoxButtons.OK;
                        result = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                    }
                }
                searchlistBoxsuggestion.Visibility = Visibility.Hidden;
                searchforSymboltext.Text = "";
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "searchlistBoxsuggestion_SelectionChanged : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        public void WriteUniquelogs(string symbol, string message, MessageType msgType)
        {
            try
            {
                SymbolLogs log = new SymbolLogs();
                log.createFile(symbol);
                log.LogMessage(message, MessageType.Informational);
                log = null;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : " + e.Message);
                Trace.WriteLine("Exception : " + e.Message);
            }
        }

        private void Grid_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                m_symbolsettingwindowObj.Close();
            }
        }

        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (searchlistBoxsuggestion.Visibility == Visibility.Visible)
            {
                if (searchlistBoxsuggestion.SelectedItem == null)
                {
                    searchlistBoxsuggestion.Visibility = Visibility.Hidden;
                    searchforSymboltext.Text = "";
                }
            }
        }
        private void listBoxright_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ForClosingSearchlist();
        }
        private void listBoxleft_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ForClosingSearchlist();
        }
        public void ForClosingSearchlist()
        {
            if (searchlistBoxsuggestion.Visibility == Visibility.Visible)
            {
                searchforSymboltext.Text = "";
                searchlistBoxsuggestion.Visibility = Visibility.Hidden;
            }
        }

        private void customSymbol_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ForClosingSearchlist();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            listBoxright.Items.Clear();
            RealtimeSymbolfromsymbolIni = new List<string>();
            SymbolListwithExchangeOnly = new List<string>();
            inisymbolList = path + @"\Configuration\" + "symbollistRealStockA1" + m_iniChoose + ".ini";
            try
            {
                customSymbol.Text = "";
                if (File.Exists(inisymbolList))
                {
                    objReadSetting = new ReadSettings(logger);
                   // objReadSetting.ReadSymbolList(inisymbolList);
                    tblName = m_kiteWrapperObj.FetchTableName(m_userID);
                   // count = objReadSetting.Symbol.Count;
                   // foreach (string sym in objReadSetting.Symbol)
                    {
                        string sym = "";
                        string[] symParts = sym.Split('.');
                        string newSym = "";
                        if (symParts[1] != Constants.EXCHANGE_NSE && symParts.Length < 3)
                        {
                            //string newSym = symParts[0] + DateTime.Now.ToString("yy") + DateTime.Now.ToString("MMM").ToUpper() + "FUT";
                            //IRDSPM::Pratiksha::01-12-2020::To avoid CDS symbol concatanation
                            if (symParts[0].EndsWith("FUT"))
                            {
                                newSym = symParts[0];
                            }
                            else
                            {
                                //IRDSPM::Pratiksha::03-12-2020::for not adding fut at the end for NFO-OPT
                                if (symParts[1] == Constants.EXCHANGE_NFO_OPT || symParts[1] == Constants.EXCHANGE_CDS)
                                {
                                    newSym = symParts[0];
                                }
                                else
                                    newSym = symParts[0] + tblName;
                            }
                            RealtimeSymbolfromsymbolIni.Add(newSym);
                        }
                        else
                        {
                            if (symParts.Length == 3)
                            {
                                newSym = symParts[0] + "." + symParts[1];
                                RealtimeSymbolfromsymbolIni.Add(newSym);
                            }
                            //else if (!c.Contains(symParts[0] + ".NSE"))
                            //{
                            //    ConstantSymbolsList.Add(symParts[0] + ".NSE");
                            //}
                            //IRDSPM::Pratiksha::14-12-2020::For adding symbols in list
                            else
                            {
                                RealtimeSymbolfromsymbolIni.Add(symParts[0]);
                            }
                        }

                        //IRDSPM::Pratiksha::11-11-2020::For adding with exchange, without transation type
                        string[] SymExg = sym.Split(',');
                        SymbolListwithExchangeOnly.Add(SymExg[0]);
                    }
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "AllSymbolSettingWPFForm :  Exception Error Message = " + ex.Message, MessageType.Exception);
            }

            List<string> list;
            NFOSymbolsList = m_kiteWrapperObj.FetchDetails(Constants.EXCHANGE_NFO, m_userID);
            MCXSymbolsList = m_kiteWrapperObj.FetchDetails(Constants.EXCHANGE_MCX, m_userID);
            EquitySymbolsList = m_kiteWrapperObj.FetchDetails(Constants.EXCHANGE_NSE, m_userID);
            OptionSymbolsList = m_kiteWrapperObj.FetchDetails("OPTIONS", m_userID);
            //Pratiksha::01-12-2020::For currency
            CDSSymbolsList = m_kiteWrapperObj.FetchDetails(Constants.EXCHANGE_CDS, m_userID);
            NSESymbolsList = new List<string>();

            try
            {
                // foreach (string sym in NFOSymbolsList)
                foreach (string sym in EquitySymbolsList)
                {
                    //string newSym = sym.Substring(0, sym.Length - 8);
                    NSESymbolsList.Add(sym);
                }

                foreach (var item in RealtimeSymbolfromsymbolIni)
                {
                    listBoxleft.Items.Add(item);
                }
                WriteUniquelogs("settingLogs" + " ", "AllSymbolSettingWPFForm : Read Symbols names from ini in symbol setting. ", MessageType.Informational);

                WriteUniquelogs("settingLogs" + " ", "AllSymbolSettingWPFForm : Read symbol names from db.", MessageType.Informational);

                for (int k = 0; k < RealtimeSymbolfromsymbolIni.Count; k++)
                {
                    //IRDSPM::Pratiksha::11-12-2020::For not include nifty 50 and bank nifty in list
                    if (!listBoxright.Items.Contains(RealtimeSymbolfromsymbolIni[k]))
                    {
                        listBoxright.Items.Add(RealtimeSymbolfromsymbolIni[k]);
                    }
                }
                NSEradioButton.IsChecked = true;
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "UserControl_Loaded : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }
        private void ForexradioButton_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                int count = 0;
                if (searchlistBoxsuggestion.Visibility == Visibility.Visible)
                {
                    searchforSymboltext.Text = "";
                    searchlistBoxsuggestion.Visibility = Visibility.Hidden;
                }

                if (ForexradioButton.IsChecked == true)
                {
                    listBoxleft.Items.Clear();
                    foreach (var item in SymbolListwithExchangeOnly)
                    {
                        string[] symname = item.Split('.');
                        if (symname[1] == Constants.EXCHANGE_CDS)
                        {
                            listBoxleft.Items.Add(symname[0]);
                            count = count + 1;
                        }
                    }
                    for (int i = 0; i < CDSSymbolsList.Count && i <= 100; i++)
                    {
                        if (!listBoxleft.Items.Contains(CDSSymbolsList[i]))
                        {
                            listBoxleft.Items.Add(CDSSymbolsList[i]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("settingLogs" + " ", "ForexradioButton_Checked :  Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }
    }
}
