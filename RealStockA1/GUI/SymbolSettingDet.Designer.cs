﻿
namespace RealStockA1Strategy
{
    partial class SymbolSettingDet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SymbolSettingDet));
            this.btnDeleteselectedrow = new System.Windows.Forms.Button();
            this.btnApply = new System.Windows.Forms.Button();
            this.GPSymbolDet = new System.Windows.Forms.GroupBox();
            this.GPAccount = new System.Windows.Forms.GroupBox();
            this.lblAccount = new System.Windows.Forms.Label();
            this.AccountList = new System.Windows.Forms.ComboBox();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.SymbolName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Exchange = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ExpiryDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ExpiryPeriod = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OrderType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MarketLotSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalNumberofLotSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RoundOff = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CallStrikePrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PutStrikePrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MarginPerLot = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Stoploss = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GPAddDet = new System.Windows.Forms.GroupBox();
            this.pnlMarginLimit = new System.Windows.Forms.Panel();
            this.txtMarginLimit = new System.Windows.Forms.TextBox();
            this.pnlStoploss = new System.Windows.Forms.Panel();
            this.txtStoploss = new System.Windows.Forms.TextBox();
            this.lblStoploss = new System.Windows.Forms.Label();
            this.btnClearAll = new System.Windows.Forms.Button();
            this.lblMargin = new System.Windows.Forms.Label();
            this.pnlTotalNumberofLotSize = new System.Windows.Forms.Panel();
            this.txtTotalNumberofLotSize = new System.Windows.Forms.TextBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.lblstrikepriceput = new System.Windows.Forms.Label();
            this.lblTotalNumberofLotSize = new System.Windows.Forms.Label();
            this.strikepriceputSubList = new System.Windows.Forms.ComboBox();
            this.lblRoundOff = new System.Windows.Forms.Label();
            this.strikepriceputList = new System.Windows.Forms.ComboBox();
            this.lblstrikepriceCall = new System.Windows.Forms.Label();
            this.lblInstruments = new System.Windows.Forms.Label();
            this.ExchangeList = new System.Windows.Forms.ComboBox();
            this.lblSymbol = new System.Windows.Forms.Label();
            this.SymbolList = new System.Windows.Forms.ComboBox();
            this.lblExpiryDate = new System.Windows.Forms.Label();
            this.ExpirydtList = new System.Windows.Forms.ComboBox();
            this.lblExpiryPeriod = new System.Windows.Forms.Label();
            this.ExpiryPeriodList = new System.Windows.Forms.ComboBox();
            this.lblOrdertype = new System.Windows.Forms.Label();
            this.OrderTypeList = new System.Windows.Forms.ComboBox();
            this.lblMarketLotSize = new System.Windows.Forms.Label();
            this.pnlLotSize = new System.Windows.Forms.Panel();
            this.txtMarketLotSize = new System.Windows.Forms.TextBox();
            this.pnlRoundoff = new System.Windows.Forms.Panel();
            this.txtroundoff = new System.Windows.Forms.TextBox();
            this.strikepriceCallList = new System.Windows.Forms.ComboBox();
            this.strikepricecallSubList = new System.Windows.Forms.ComboBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.GPSymbolDet.SuspendLayout();
            this.GPAccount.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.GPAddDet.SuspendLayout();
            this.pnlMarginLimit.SuspendLayout();
            this.pnlStoploss.SuspendLayout();
            this.pnlTotalNumberofLotSize.SuspendLayout();
            this.pnlLotSize.SuspendLayout();
            this.pnlRoundoff.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnDeleteselectedrow
            // 
            this.btnDeleteselectedrow.Font = new System.Drawing.Font("Arial", 10.5F);
            this.btnDeleteselectedrow.Location = new System.Drawing.Point(38, 554);
            this.btnDeleteselectedrow.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnDeleteselectedrow.Name = "btnDeleteselectedrow";
            this.btnDeleteselectedrow.Size = new System.Drawing.Size(160, 32);
            this.btnDeleteselectedrow.TabIndex = 21;
            this.btnDeleteselectedrow.Text = "Delete symbol";
            this.btnDeleteselectedrow.UseVisualStyleBackColor = true;
            this.btnDeleteselectedrow.Click += new System.EventHandler(this.btnDeleteselectedrow_Click);
            // 
            // btnApply
            // 
            this.btnApply.Font = new System.Drawing.Font("Arial", 10.5F);
            this.btnApply.Location = new System.Drawing.Point(617, 554);
            this.btnApply.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(100, 32);
            this.btnApply.TabIndex = 22;
            this.btnApply.Text = "Apply";
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // GPSymbolDet
            // 
            this.GPSymbolDet.Controls.Add(this.GPAccount);
            this.GPSymbolDet.Controls.Add(this.dataGridView);
            this.GPSymbolDet.Controls.Add(this.GPAddDet);
            this.GPSymbolDet.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GPSymbolDet.Location = new System.Drawing.Point(9, 11);
            this.GPSymbolDet.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.GPSymbolDet.Name = "GPSymbolDet";
            this.GPSymbolDet.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.GPSymbolDet.Size = new System.Drawing.Size(838, 539);
            this.GPSymbolDet.TabIndex = 1;
            this.GPSymbolDet.TabStop = false;
            this.GPSymbolDet.Text = "Symbol Details";
            // 
            // GPAccount
            // 
            this.GPAccount.Controls.Add(this.lblAccount);
            this.GPAccount.Controls.Add(this.AccountList);
            this.GPAccount.Enabled = false;
            this.GPAccount.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GPAccount.Location = new System.Drawing.Point(10, 18);
            this.GPAccount.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.GPAccount.Name = "GPAccount";
            this.GPAccount.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.GPAccount.Size = new System.Drawing.Size(824, 54);
            this.GPAccount.TabIndex = 0;
            this.GPAccount.TabStop = false;
            this.GPAccount.Text = "Account";
            // 
            // lblAccount
            // 
            this.lblAccount.AutoSize = true;
            this.lblAccount.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblAccount.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblAccount.Location = new System.Drawing.Point(16, 20);
            this.lblAccount.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Size = new System.Drawing.Size(111, 16);
            this.lblAccount.TabIndex = 1;
            this.lblAccount.Text = "Choose Account";
            // 
            // AccountList
            // 
            this.AccountList.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.AccountList.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.AccountList.Font = new System.Drawing.Font("Arial", 12F);
            this.AccountList.FormattingEnabled = true;
            this.AccountList.Location = new System.Drawing.Point(160, 17);
            this.AccountList.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.AccountList.Name = "AccountList";
            this.AccountList.Size = new System.Drawing.Size(180, 26);
            this.AccountList.TabIndex = 1;
            this.AccountList.SelectedIndexChanged += new System.EventHandler(this.AccountList_SelectedIndexChanged);
            // 
            // dataGridView
            // 
            this.dataGridView.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SymbolName,
            this.Exchange,
            this.ExpiryDate,
            this.ExpiryPeriod,
            this.OrderType,
            this.MarketLotSize,
            this.TotalNumberofLotSize,
            this.RoundOff,
            this.CallStrikePrice,
            this.PutStrikePrice,
            this.MarginPerLot,
            this.Stoploss});
            this.dataGridView.Location = new System.Drawing.Point(10, 294);
            this.dataGridView.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.ReadOnly = true;
            this.dataGridView.RowHeadersWidth = 51;
            this.dataGridView.RowTemplate.Height = 24;
            this.dataGridView.Size = new System.Drawing.Size(824, 228);
            this.dataGridView.TabIndex = 1;
            this.dataGridView.TabStop = false;
            this.dataGridView.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView_CellMouseClick);
            // 
            // SymbolName
            // 
            this.SymbolName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.SymbolName.HeaderText = "Symbol Name";
            this.SymbolName.MinimumWidth = 6;
            this.SymbolName.Name = "SymbolName";
            this.SymbolName.ReadOnly = true;
            this.SymbolName.Width = 89;
            // 
            // Exchange
            // 
            this.Exchange.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Exchange.FillWeight = 90F;
            this.Exchange.HeaderText = "Exchange";
            this.Exchange.MinimumWidth = 6;
            this.Exchange.Name = "Exchange";
            this.Exchange.ReadOnly = true;
            // 
            // ExpiryDate
            // 
            this.ExpiryDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ExpiryDate.HeaderText = "Expiry Date";
            this.ExpiryDate.MinimumWidth = 10;
            this.ExpiryDate.Name = "ExpiryDate";
            this.ExpiryDate.ReadOnly = true;
            // 
            // ExpiryPeriod
            // 
            this.ExpiryPeriod.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ExpiryPeriod.FillWeight = 76.19048F;
            this.ExpiryPeriod.HeaderText = "Expiry Period";
            this.ExpiryPeriod.MinimumWidth = 6;
            this.ExpiryPeriod.Name = "ExpiryPeriod";
            this.ExpiryPeriod.ReadOnly = true;
            // 
            // OrderType
            // 
            this.OrderType.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.OrderType.FillWeight = 76.19048F;
            this.OrderType.HeaderText = "Order Type";
            this.OrderType.MinimumWidth = 6;
            this.OrderType.Name = "OrderType";
            this.OrderType.ReadOnly = true;
            // 
            // MarketLotSize
            // 
            this.MarketLotSize.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.MarketLotSize.FillWeight = 76.19048F;
            this.MarketLotSize.HeaderText = "Market Lot Size";
            this.MarketLotSize.MinimumWidth = 6;
            this.MarketLotSize.Name = "MarketLotSize";
            this.MarketLotSize.ReadOnly = true;
            // 
            // TotalNumberofLotSize
            // 
            this.TotalNumberofLotSize.FillWeight = 80F;
            this.TotalNumberofLotSize.HeaderText = "Total Number of Lot Size";
            this.TotalNumberofLotSize.MinimumWidth = 6;
            this.TotalNumberofLotSize.Name = "TotalNumberofLotSize";
            this.TotalNumberofLotSize.ReadOnly = true;
            this.TotalNumberofLotSize.Width = 80;
            // 
            // RoundOff
            // 
            this.RoundOff.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.RoundOff.FillWeight = 76.19048F;
            this.RoundOff.HeaderText = "Round Off";
            this.RoundOff.MinimumWidth = 6;
            this.RoundOff.Name = "RoundOff";
            this.RoundOff.ReadOnly = true;
            // 
            // CallStrikePrice
            // 
            this.CallStrikePrice.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CallStrikePrice.FillWeight = 76.19048F;
            this.CallStrikePrice.HeaderText = "Call Strike Multiplier";
            this.CallStrikePrice.MinimumWidth = 6;
            this.CallStrikePrice.Name = "CallStrikePrice";
            this.CallStrikePrice.ReadOnly = true;
            // 
            // PutStrikePrice
            // 
            this.PutStrikePrice.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.PutStrikePrice.FillWeight = 76.19048F;
            this.PutStrikePrice.HeaderText = "Put Strike Multiplier";
            this.PutStrikePrice.MinimumWidth = 6;
            this.PutStrikePrice.Name = "PutStrikePrice";
            this.PutStrikePrice.ReadOnly = true;
            // 
            // MarginPerLot
            // 
            this.MarginPerLot.HeaderText = "Margin Per Lot";
            this.MarginPerLot.MinimumWidth = 6;
            this.MarginPerLot.Name = "MarginPerLot";
            this.MarginPerLot.ReadOnly = true;
            this.MarginPerLot.Width = 70;
            // 
            // Stoploss
            // 
            this.Stoploss.HeaderText = "Stoploss";
            this.Stoploss.MinimumWidth = 6;
            this.Stoploss.Name = "Stoploss";
            this.Stoploss.ReadOnly = true;
            this.Stoploss.Width = 70;
            // 
            // GPAddDet
            // 
            this.GPAddDet.Controls.Add(this.pnlMarginLimit);
            this.GPAddDet.Controls.Add(this.pnlStoploss);
            this.GPAddDet.Controls.Add(this.lblStoploss);
            this.GPAddDet.Controls.Add(this.btnClearAll);
            this.GPAddDet.Controls.Add(this.lblMargin);
            this.GPAddDet.Controls.Add(this.pnlTotalNumberofLotSize);
            this.GPAddDet.Controls.Add(this.btnAdd);
            this.GPAddDet.Controls.Add(this.btnUpdate);
            this.GPAddDet.Controls.Add(this.lblstrikepriceput);
            this.GPAddDet.Controls.Add(this.lblTotalNumberofLotSize);
            this.GPAddDet.Controls.Add(this.strikepriceputSubList);
            this.GPAddDet.Controls.Add(this.lblRoundOff);
            this.GPAddDet.Controls.Add(this.strikepriceputList);
            this.GPAddDet.Controls.Add(this.lblstrikepriceCall);
            this.GPAddDet.Controls.Add(this.lblInstruments);
            this.GPAddDet.Controls.Add(this.ExchangeList);
            this.GPAddDet.Controls.Add(this.lblSymbol);
            this.GPAddDet.Controls.Add(this.SymbolList);
            this.GPAddDet.Controls.Add(this.lblExpiryDate);
            this.GPAddDet.Controls.Add(this.ExpirydtList);
            this.GPAddDet.Controls.Add(this.lblExpiryPeriod);
            this.GPAddDet.Controls.Add(this.ExpiryPeriodList);
            this.GPAddDet.Controls.Add(this.lblOrdertype);
            this.GPAddDet.Controls.Add(this.OrderTypeList);
            this.GPAddDet.Controls.Add(this.lblMarketLotSize);
            this.GPAddDet.Controls.Add(this.pnlLotSize);
            this.GPAddDet.Controls.Add(this.pnlRoundoff);
            this.GPAddDet.Controls.Add(this.strikepriceCallList);
            this.GPAddDet.Controls.Add(this.strikepricecallSubList);
            this.GPAddDet.Font = new System.Drawing.Font("Arial", 10F);
            this.GPAddDet.Location = new System.Drawing.Point(10, 76);
            this.GPAddDet.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.GPAddDet.Name = "GPAddDet";
            this.GPAddDet.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.GPAddDet.Size = new System.Drawing.Size(824, 213);
            this.GPAddDet.TabIndex = 2;
            this.GPAddDet.TabStop = false;
            this.GPAddDet.Text = "Add details";
            // 
            // pnlMarginLimit
            // 
            this.pnlMarginLimit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlMarginLimit.Controls.Add(this.txtMarginLimit);
            this.pnlMarginLimit.Location = new System.Drawing.Point(539, 141);
            this.pnlMarginLimit.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pnlMarginLimit.Name = "pnlMarginLimit";
            this.pnlMarginLimit.Size = new System.Drawing.Size(179, 26);
            this.pnlMarginLimit.TabIndex = 13;
            this.pnlMarginLimit.TabStop = true;
            // 
            // txtMarginLimit
            // 
            this.txtMarginLimit.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtMarginLimit.Font = new System.Drawing.Font("Arial", 12F);
            this.txtMarginLimit.Location = new System.Drawing.Point(4, 2);
            this.txtMarginLimit.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtMarginLimit.Name = "txtMarginLimit";
            this.txtMarginLimit.Size = new System.Drawing.Size(172, 19);
            this.txtMarginLimit.TabIndex = 11;
            // 
            // pnlStoploss
            // 
            this.pnlStoploss.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlStoploss.Controls.Add(this.txtStoploss);
            this.pnlStoploss.Location = new System.Drawing.Point(539, 172);
            this.pnlStoploss.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pnlStoploss.Name = "pnlStoploss";
            this.pnlStoploss.Size = new System.Drawing.Size(179, 26);
            this.pnlStoploss.TabIndex = 14;
            this.pnlStoploss.TabStop = true;
            // 
            // txtStoploss
            // 
            this.txtStoploss.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtStoploss.Font = new System.Drawing.Font("Arial", 12F);
            this.txtStoploss.Location = new System.Drawing.Point(3, 2);
            this.txtStoploss.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtStoploss.Name = "txtStoploss";
            this.txtStoploss.Size = new System.Drawing.Size(172, 19);
            this.txtStoploss.TabIndex = 13;
            // 
            // lblStoploss
            // 
            this.lblStoploss.AutoSize = true;
            this.lblStoploss.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblStoploss.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblStoploss.Location = new System.Drawing.Point(346, 176);
            this.lblStoploss.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblStoploss.Name = "lblStoploss";
            this.lblStoploss.Size = new System.Drawing.Size(62, 16);
            this.lblStoploss.TabIndex = 25;
            this.lblStoploss.Text = "Stoploss";
            // 
            // btnClearAll
            // 
            this.btnClearAll.Font = new System.Drawing.Font("Arial", 10F);
            this.btnClearAll.Location = new System.Drawing.Point(725, 110);
            this.btnClearAll.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnClearAll.Name = "btnClearAll";
            this.btnClearAll.Size = new System.Drawing.Size(95, 25);
            this.btnClearAll.TabIndex = 23;
            this.btnClearAll.Text = "Clear All";
            this.btnClearAll.UseVisualStyleBackColor = true;
            this.btnClearAll.Click += new System.EventHandler(this.btnClearAll_Click);
            // 
            // lblMargin
            // 
            this.lblMargin.AutoSize = true;
            this.lblMargin.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblMargin.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblMargin.Location = new System.Drawing.Point(345, 146);
            this.lblMargin.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblMargin.Name = "lblMargin";
            this.lblMargin.Size = new System.Drawing.Size(101, 16);
            this.lblMargin.TabIndex = 20;
            this.lblMargin.Text = "Margin Per Lot";
            // 
            // pnlTotalNumberofLotSize
            // 
            this.pnlTotalNumberofLotSize.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlTotalNumberofLotSize.Controls.Add(this.txtTotalNumberofLotSize);
            this.pnlTotalNumberofLotSize.Location = new System.Drawing.Point(539, 17);
            this.pnlTotalNumberofLotSize.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pnlTotalNumberofLotSize.Name = "pnlTotalNumberofLotSize";
            this.pnlTotalNumberofLotSize.Size = new System.Drawing.Size(179, 26);
            this.pnlTotalNumberofLotSize.TabIndex = 10;
            this.pnlTotalNumberofLotSize.TabStop = true;
            // 
            // txtTotalNumberofLotSize
            // 
            this.txtTotalNumberofLotSize.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTotalNumberofLotSize.Font = new System.Drawing.Font("Arial", 12F);
            this.txtTotalNumberofLotSize.Location = new System.Drawing.Point(4, 2);
            this.txtTotalNumberofLotSize.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtTotalNumberofLotSize.Name = "txtTotalNumberofLotSize";
            this.txtTotalNumberofLotSize.Size = new System.Drawing.Size(172, 19);
            this.txtTotalNumberofLotSize.TabIndex = 11;
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("Arial", 10F);
            this.btnAdd.Location = new System.Drawing.Point(725, 175);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(95, 25);
            this.btnAdd.TabIndex = 20;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Font = new System.Drawing.Font("Arial", 10F);
            this.btnUpdate.Location = new System.Drawing.Point(725, 142);
            this.btnUpdate.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(95, 25);
            this.btnUpdate.TabIndex = 19;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // lblstrikepriceput
            // 
            this.lblstrikepriceput.AutoSize = true;
            this.lblstrikepriceput.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblstrikepriceput.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblstrikepriceput.Location = new System.Drawing.Point(343, 117);
            this.lblstrikepriceput.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblstrikepriceput.Name = "lblstrikepriceput";
            this.lblstrikepriceput.Size = new System.Drawing.Size(129, 16);
            this.lblstrikepriceput.TabIndex = 4;
            this.lblstrikepriceput.Text = "Put Strike Multiplier";
            // 
            // lblTotalNumberofLotSize
            // 
            this.lblTotalNumberofLotSize.AutoSize = true;
            this.lblTotalNumberofLotSize.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblTotalNumberofLotSize.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblTotalNumberofLotSize.Location = new System.Drawing.Point(344, 24);
            this.lblTotalNumberofLotSize.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTotalNumberofLotSize.Name = "lblTotalNumberofLotSize";
            this.lblTotalNumberofLotSize.Size = new System.Drawing.Size(161, 16);
            this.lblTotalNumberofLotSize.TabIndex = 19;
            this.lblTotalNumberofLotSize.Text = "Total Number of Lot Size";
            // 
            // strikepriceputSubList
            // 
            this.strikepriceputSubList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.strikepriceputSubList.Font = new System.Drawing.Font("Arial", 12F);
            this.strikepriceputSubList.FormattingEnabled = true;
            this.strikepriceputSubList.Location = new System.Drawing.Point(539, 112);
            this.strikepriceputSubList.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.strikepriceputSubList.Name = "strikepriceputSubList";
            this.strikepriceputSubList.Size = new System.Drawing.Size(179, 26);
            this.strikepriceputSubList.TabIndex = 17;
            // 
            // lblRoundOff
            // 
            this.lblRoundOff.AutoSize = true;
            this.lblRoundOff.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblRoundOff.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblRoundOff.Location = new System.Drawing.Point(344, 54);
            this.lblRoundOff.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblRoundOff.Name = "lblRoundOff";
            this.lblRoundOff.Size = new System.Drawing.Size(73, 16);
            this.lblRoundOff.TabIndex = 12;
            this.lblRoundOff.Text = "Round Off";
            // 
            // strikepriceputList
            // 
            this.strikepriceputList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.strikepriceputList.Enabled = false;
            this.strikepriceputList.Font = new System.Drawing.Font("Arial", 12F);
            this.strikepriceputList.FormattingEnabled = true;
            this.strikepriceputList.Location = new System.Drawing.Point(539, 111);
            this.strikepriceputList.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.strikepriceputList.Name = "strikepriceputList";
            this.strikepriceputList.Size = new System.Drawing.Size(74, 26);
            this.strikepriceputList.TabIndex = 16;
            this.strikepriceputList.Visible = false;
            this.strikepriceputList.SelectedValueChanged += new System.EventHandler(this.strikepriceputList_SelectedValueChanged);
            // 
            // lblstrikepriceCall
            // 
            this.lblstrikepriceCall.AutoSize = true;
            this.lblstrikepriceCall.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblstrikepriceCall.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblstrikepriceCall.Location = new System.Drawing.Point(344, 85);
            this.lblstrikepriceCall.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblstrikepriceCall.Name = "lblstrikepriceCall";
            this.lblstrikepriceCall.Size = new System.Drawing.Size(132, 16);
            this.lblstrikepriceCall.TabIndex = 5;
            this.lblstrikepriceCall.Text = "Call Strike Multiplier";
            // 
            // lblInstruments
            // 
            this.lblInstruments.AutoSize = true;
            this.lblInstruments.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblInstruments.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblInstruments.Location = new System.Drawing.Point(19, 24);
            this.lblInstruments.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblInstruments.Name = "lblInstruments";
            this.lblInstruments.Size = new System.Drawing.Size(81, 16);
            this.lblInstruments.TabIndex = 18;
            this.lblInstruments.Text = "Instruments";
            // 
            // ExchangeList
            // 
            this.ExchangeList.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ExchangeList.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.ExchangeList.Font = new System.Drawing.Font("Arial", 12F);
            this.ExchangeList.FormattingEnabled = true;
            this.ExchangeList.Location = new System.Drawing.Point(159, 16);
            this.ExchangeList.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ExchangeList.Name = "ExchangeList";
            this.ExchangeList.Size = new System.Drawing.Size(180, 26);
            this.ExchangeList.TabIndex = 3;
            this.ExchangeList.SelectedValueChanged += new System.EventHandler(this.ExchangeList_SelectedValueChanged);
            // 
            // lblSymbol
            // 
            this.lblSymbol.AutoSize = true;
            this.lblSymbol.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblSymbol.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblSymbol.Location = new System.Drawing.Point(19, 54);
            this.lblSymbol.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSymbol.Name = "lblSymbol";
            this.lblSymbol.Size = new System.Drawing.Size(54, 16);
            this.lblSymbol.TabIndex = 17;
            this.lblSymbol.Text = "Symbol";
            // 
            // SymbolList
            // 
            this.SymbolList.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.SymbolList.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.SymbolList.Font = new System.Drawing.Font("Arial", 12F);
            this.SymbolList.FormattingEnabled = true;
            this.SymbolList.Location = new System.Drawing.Point(159, 47);
            this.SymbolList.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.SymbolList.Name = "SymbolList";
            this.SymbolList.Size = new System.Drawing.Size(180, 26);
            this.SymbolList.TabIndex = 4;
            this.SymbolList.SelectedValueChanged += new System.EventHandler(this.SymbolList_SelectedValueChanged);
            // 
            // lblExpiryDate
            // 
            this.lblExpiryDate.AutoSize = true;
            this.lblExpiryDate.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblExpiryDate.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblExpiryDate.Location = new System.Drawing.Point(19, 85);
            this.lblExpiryDate.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblExpiryDate.Name = "lblExpiryDate";
            this.lblExpiryDate.Size = new System.Drawing.Size(80, 16);
            this.lblExpiryDate.TabIndex = 16;
            this.lblExpiryDate.Text = "Expiry Date";
            // 
            // ExpirydtList
            // 
            this.ExpirydtList.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ExpirydtList.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.ExpirydtList.Font = new System.Drawing.Font("Arial", 12F);
            this.ExpirydtList.FormattingEnabled = true;
            this.ExpirydtList.Location = new System.Drawing.Point(159, 78);
            this.ExpirydtList.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ExpirydtList.Name = "ExpirydtList";
            this.ExpirydtList.Size = new System.Drawing.Size(180, 26);
            this.ExpirydtList.TabIndex = 5;
            this.ExpirydtList.SelectedValueChanged += new System.EventHandler(this.ExpirydtList_SelectedValueChanged);
            // 
            // lblExpiryPeriod
            // 
            this.lblExpiryPeriod.AutoSize = true;
            this.lblExpiryPeriod.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblExpiryPeriod.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblExpiryPeriod.Location = new System.Drawing.Point(19, 116);
            this.lblExpiryPeriod.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblExpiryPeriod.Name = "lblExpiryPeriod";
            this.lblExpiryPeriod.Size = new System.Drawing.Size(91, 16);
            this.lblExpiryPeriod.TabIndex = 13;
            this.lblExpiryPeriod.Text = "Expiry Period";
            // 
            // ExpiryPeriodList
            // 
            this.ExpiryPeriodList.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ExpiryPeriodList.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.ExpiryPeriodList.Font = new System.Drawing.Font("Arial", 12F);
            this.ExpiryPeriodList.FormattingEnabled = true;
            this.ExpiryPeriodList.Location = new System.Drawing.Point(159, 109);
            this.ExpiryPeriodList.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ExpiryPeriodList.Name = "ExpiryPeriodList";
            this.ExpiryPeriodList.Size = new System.Drawing.Size(180, 26);
            this.ExpiryPeriodList.TabIndex = 6;
            // 
            // lblOrdertype
            // 
            this.lblOrdertype.AutoSize = true;
            this.lblOrdertype.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblOrdertype.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblOrdertype.Location = new System.Drawing.Point(19, 147);
            this.lblOrdertype.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblOrdertype.Name = "lblOrdertype";
            this.lblOrdertype.Size = new System.Drawing.Size(80, 16);
            this.lblOrdertype.TabIndex = 9;
            this.lblOrdertype.Text = "Order Type";
            // 
            // OrderTypeList
            // 
            this.OrderTypeList.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.OrderTypeList.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.OrderTypeList.Font = new System.Drawing.Font("Arial", 12F);
            this.OrderTypeList.FormattingEnabled = true;
            this.OrderTypeList.Location = new System.Drawing.Point(159, 140);
            this.OrderTypeList.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.OrderTypeList.Name = "OrderTypeList";
            this.OrderTypeList.Size = new System.Drawing.Size(180, 26);
            this.OrderTypeList.TabIndex = 7;
            // 
            // lblMarketLotSize
            // 
            this.lblMarketLotSize.AutoSize = true;
            this.lblMarketLotSize.Font = new System.Drawing.Font("Arial", 10.2F);
            this.lblMarketLotSize.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblMarketLotSize.Location = new System.Drawing.Point(19, 178);
            this.lblMarketLotSize.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblMarketLotSize.Name = "lblMarketLotSize";
            this.lblMarketLotSize.Size = new System.Drawing.Size(105, 16);
            this.lblMarketLotSize.TabIndex = 8;
            this.lblMarketLotSize.Text = "Market Lot Size";
            // 
            // pnlLotSize
            // 
            this.pnlLotSize.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlLotSize.Controls.Add(this.txtMarketLotSize);
            this.pnlLotSize.Location = new System.Drawing.Point(159, 171);
            this.pnlLotSize.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pnlLotSize.Name = "pnlLotSize";
            this.pnlLotSize.Size = new System.Drawing.Size(179, 26);
            this.pnlLotSize.TabIndex = 8;
            this.pnlLotSize.TabStop = true;
            // 
            // txtMarketLotSize
            // 
            this.txtMarketLotSize.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtMarketLotSize.Font = new System.Drawing.Font("Arial", 12F);
            this.txtMarketLotSize.Location = new System.Drawing.Point(4, 2);
            this.txtMarketLotSize.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtMarketLotSize.Name = "txtMarketLotSize";
            this.txtMarketLotSize.Size = new System.Drawing.Size(172, 19);
            this.txtMarketLotSize.TabIndex = 9;
            // 
            // pnlRoundoff
            // 
            this.pnlRoundoff.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlRoundoff.Controls.Add(this.txtroundoff);
            this.pnlRoundoff.Location = new System.Drawing.Point(539, 48);
            this.pnlRoundoff.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pnlRoundoff.Name = "pnlRoundoff";
            this.pnlRoundoff.Size = new System.Drawing.Size(179, 26);
            this.pnlRoundoff.TabIndex = 12;
            this.pnlRoundoff.TabStop = true;
            // 
            // txtroundoff
            // 
            this.txtroundoff.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtroundoff.Font = new System.Drawing.Font("Arial", 12F);
            this.txtroundoff.Location = new System.Drawing.Point(3, 2);
            this.txtroundoff.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtroundoff.Name = "txtroundoff";
            this.txtroundoff.Size = new System.Drawing.Size(172, 19);
            this.txtroundoff.TabIndex = 13;
            // 
            // strikepriceCallList
            // 
            this.strikepriceCallList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.strikepriceCallList.Enabled = false;
            this.strikepriceCallList.Font = new System.Drawing.Font("Arial", 12F);
            this.strikepriceCallList.FormattingEnabled = true;
            this.strikepriceCallList.Location = new System.Drawing.Point(539, 80);
            this.strikepriceCallList.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.strikepriceCallList.Name = "strikepriceCallList";
            this.strikepriceCallList.Size = new System.Drawing.Size(75, 26);
            this.strikepriceCallList.TabIndex = 14;
            this.strikepriceCallList.Visible = false;
            this.strikepriceCallList.SelectedValueChanged += new System.EventHandler(this.strikepriceCallList_SelectedValueChanged);
            // 
            // strikepricecallSubList
            // 
            this.strikepricecallSubList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.strikepricecallSubList.Font = new System.Drawing.Font("Arial", 12F);
            this.strikepricecallSubList.FormattingEnabled = true;
            this.strikepricecallSubList.Location = new System.Drawing.Point(539, 80);
            this.strikepricecallSubList.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.strikepricecallSubList.Name = "strikepricecallSubList";
            this.strikepricecallSubList.Size = new System.Drawing.Size(180, 26);
            this.strikepricecallSubList.TabIndex = 15;
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("Arial", 10.5F);
            this.btnCancel.Location = new System.Drawing.Point(736, 554);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(100, 32);
            this.btnCancel.TabIndex = 23;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // SymbolSettingDet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(856, 592);
            this.Controls.Add(this.GPSymbolDet);
            this.Controls.Add(this.btnDeleteselectedrow);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.btnCancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.MaximizeBox = false;
            this.Name = "SymbolSettingDet";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Symbol Setting";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SymbolSettingDet_FormClosing);
            this.GPSymbolDet.ResumeLayout(false);
            this.GPAccount.ResumeLayout(false);
            this.GPAccount.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.GPAddDet.ResumeLayout(false);
            this.GPAddDet.PerformLayout();
            this.pnlMarginLimit.ResumeLayout(false);
            this.pnlMarginLimit.PerformLayout();
            this.pnlStoploss.ResumeLayout(false);
            this.pnlStoploss.PerformLayout();
            this.pnlTotalNumberofLotSize.ResumeLayout(false);
            this.pnlTotalNumberofLotSize.PerformLayout();
            this.pnlLotSize.ResumeLayout(false);
            this.pnlLotSize.PerformLayout();
            this.pnlRoundoff.ResumeLayout(false);
            this.pnlRoundoff.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        //private System.Windows.Forms.Button btnAddRow;
        private System.Windows.Forms.ComboBox AccountList;
        private System.Windows.Forms.ComboBox ExchangeList;
        private System.Windows.Forms.ComboBox SymbolList;
        private System.Windows.Forms.ComboBox ExpirydtList;
        private System.Windows.Forms.ComboBox ExpiryPeriodList;
        private System.Windows.Forms.ComboBox OrderTypeList;
        private System.Windows.Forms.TextBox txtMarketLotSize;
        private System.Windows.Forms.TextBox txtroundoff;
        private System.Windows.Forms.ComboBox strikepriceCallList;
        private System.Windows.Forms.ComboBox strikepriceputSubList;
        private System.Windows.Forms.ComboBox strikepriceputList;
        private System.Windows.Forms.ComboBox strikepricecallSubList;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnDeleteselectedrow;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.Button btnCancel;


        private System.Windows.Forms.GroupBox GPSymbolDet;
        private System.Windows.Forms.GroupBox GPAddDet;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.GroupBox GPAccount;
        private System.Windows.Forms.Panel pnlRoundoff;
        private System.Windows.Forms.Panel pnlLotSize;
        private System.Windows.Forms.Label lblRoundOff;
        private System.Windows.Forms.Label lblOrdertype;
        private System.Windows.Forms.Label lblstrikepriceput;
        private System.Windows.Forms.Label lblstrikepriceCall;
        private System.Windows.Forms.Label lblMarketLotSize;
        private System.Windows.Forms.Label lblExpiryPeriod;
        private System.Windows.Forms.Label lblAccount;
        private System.Windows.Forms.Label lblExpiryDate;
        private System.Windows.Forms.Label lblInstruments;
        private System.Windows.Forms.Label lblSymbol;
        private System.Windows.Forms.Label lblTotalNumberofLotSize;
        private System.Windows.Forms.Panel pnlTotalNumberofLotSize;
        private System.Windows.Forms.TextBox txtTotalNumberofLotSize;
        private System.Windows.Forms.Label lblMargin;
        private System.Windows.Forms.Button btnClearAll;
        private System.Windows.Forms.Label lblStoploss;
        private System.Windows.Forms.DataGridViewTextBoxColumn SymbolName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Exchange;
        private System.Windows.Forms.DataGridViewTextBoxColumn ExpiryDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ExpiryPeriod;
        private System.Windows.Forms.DataGridViewTextBoxColumn OrderType;
        private System.Windows.Forms.DataGridViewTextBoxColumn MarketLotSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalNumberofLotSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn RoundOff;
        private System.Windows.Forms.DataGridViewTextBoxColumn CallStrikePrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn PutStrikePrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn MarginPerLot;
        private System.Windows.Forms.DataGridViewTextBoxColumn Stoploss;
        private System.Windows.Forms.Panel pnlMarginLimit;
        private System.Windows.Forms.TextBox txtMarginLimit;
        private System.Windows.Forms.Panel pnlStoploss;
        private System.Windows.Forms.TextBox txtStoploss;
    }
}