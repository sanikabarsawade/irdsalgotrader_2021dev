﻿using IRDSAlgoOMS;
using RealStockA1Strategy;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RealStockA1Strategy
{
    public partial class SymbolSettingDet : Form
    {
        string inisymbolList;
        RealStockA1Strategy.ReadSettings Settings;
        IRDSAlgoOMS.Logger logger = IRDSAlgoOMS.Logger.Instance;
        string title = "RealStockA1 Strategy";
        string message = "";
        MessageBoxButtons buttons = MessageBoxButtons.OK;
        DialogResult dialog;
        List<string> finalExchangeList = new List<string>();
        dynamic ConnectWrapper = null;
        string path = System.AppDomain.CurrentDomain.BaseDirectory + "Configuration";
        bool isexit = false;
        public SymbolSettingDet(dynamic kiteConnectWrapper, string userID, ReadSettings objSettings)
        {
            InitializeComponent();
            try
            {
                if (Settings == null)
                    Settings = objSettings;
                ConnectWrapper = kiteConnectWrapper;
                ExchangeList.Items.Add("OPTIONS");
                //IRDSPM::Pratiksha::03-05-2021::To have default value in exchange setting
                //if(ExchangeList.Items.Count == 1)
                //{
                //    ExchangeList.SelectedIndex = 0;
                //}
                string[] files = Directory.GetFiles(path, "StrategySetting_*");
                foreach (string fileName in files)
                {
                    string file = Path.GetFileNameWithoutExtension(fileName);
                    AccountList.Items.Add(file.Split('_')[1]);
                }
                //IRDSPM::Pratiksha::28-05-2021::for selected account need to be default selected
                if (AccountList.Items.Contains(userID))
                {
                    AccountList.SelectedItem = userID;
                }
                else
                {
                    AccountList.SelectedIndex = 0;
                }
                OrderTypeList.Items.Add("MIS");
                OrderTypeList.Items.Add("NRML");
                ExpiryPeriodList.Items.Add("Weekly");
                ExpiryPeriodList.Items.Add("Monthly");
                for (int i = 1; i < 11; i++)
                {
                    strikepriceCallList.Items.Add(i + "0");
                    strikepriceputList.Items.Add(i + "0");
                }
                //IRDSPM::Pratiksha::20-05-2021::List default selectd and disabled
                strikepriceCallList.SelectedIndex = 0;
                strikepriceputList.SelectedIndex = 0;
                ExchangeList.SelectedIndex = 0;
                WriteUniquelogs("SymbolSettingLogs", "InitializeComponent : loaded symbols and fetched details properly.", MessageType.Informational);
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs", "InitializeComponent : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            try
            {
                if (SymbolList.SelectedItem == null && ExpirydtList.Text == null && OrderTypeList.SelectedItem == null && ExpiryPeriodList.SelectedItem == null && txtroundoff.Text.ToString().Length == 0 && txtMarketLotSize.Text.ToString().Length == 0 && txtTotalNumberofLotSize.Text.ToString().Length == 0 && txtMarginLimit.Text.ToString().Length == 0 && txtStoploss.Text.ToString().Length == 0)
                {
                    message = "Do you want to save the changes?";
                    buttons = MessageBoxButtons.YesNo;
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                    if (dialog == DialogResult.Yes)
                    {
                        AddIntoINIFile();
                        this.Close();
                    }
                }
                else
                {
                    if (ExchangeList.SelectedItem != null && SymbolList.SelectedItem != null && ExpirydtList.Text != null && OrderTypeList.SelectedItem != null && ExpiryPeriodList.SelectedItem != null && txtroundoff.Text.ToString().Length > 0 && txtMarketLotSize.Text.ToString().Length > 0 && txtTotalNumberofLotSize.Text.ToString().Length > 0 && txtMarginLimit.Text.ToString().Length > 0 && txtStoploss.Text.ToString().Length > 0)
                    {
                        Match rountOff1, MarketLotSize1, OneLotSize1, MarginPerLot1, Stoploss1;
                        Validations(out rountOff1, out MarketLotSize1, out OneLotSize1, out MarginPerLot1, out Stoploss1);
                        if (rountOff1.Success && MarketLotSize1.Success && OneLotSize1.Success && MarginPerLot1.Success && Stoploss1.Success)
                        {
                            message = "Do you want to save the changes?";
                            buttons = MessageBoxButtons.YesNo;
                            dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                            if (dialog == DialogResult.Yes)
                            {
                                AddIntoINIFile();
                            }
                        }
                        else
                        {
                            string Logmessage = "";
                            string parameter = "";
                            if (!MarketLotSize1.Success) { parameter += "\nMarket Lot Size"; Logmessage += "Market Lot Size,"; }
                            if (!OneLotSize1.Success) { parameter += "\nTotal Number of Lot Size"; Logmessage += "Total Number of Lot Size,"; }
                            if (!rountOff1.Success) { parameter += "\nRound Off"; Logmessage += "Round Off"; }
                            if (!MarginPerLot1.Success) { parameter += "\nMargin Per Lot"; Logmessage += "Margin Per Lot"; }
                            if (!Stoploss1.Success) { parameter += "\nStoploss"; Logmessage += "Stoploss"; }
                            message = "Please add valid values for " + parameter + ".";
                            buttons = MessageBoxButtons.OK;
                            dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                            WriteUniquelogs("SymbolSettingLogs", "btnApply_Click : " + Logmessage, MessageType.Informational);
                        }
                    }
                    else
                    {
                        CheckNullValuesAndShowPopup();
                    }
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs", "btnApply_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void AddIntoINIFile(bool IsDisplay)
        {
            bool issaved = false;
            bool isAdded = false;
            int j = 0;
            int counter = 0;
            int k = dataGridView.Rows.Count;
            bool flagSymbolname = false;
            var re = @"^[0-9]{2}[a-zA-Z]{3}[0-9]{2}$";
            INIFile iniObj = new INIFile(inisymbolList);
            DialogResult dialog;
            try
            {
                foreach (DataGridViewRow row in dataGridView.Rows)
                {
                    flagSymbolname = false;
                    if ((row.Cells["SymbolName"].Value.ToString() == null) && (row.Cells["Exchange"].Value.ToString() == null) && (row.Cells["ExpiryDate"].Value.ToString() == null) && (row.Cells["ExpiryPeriod"].Value.ToString() == null) && (row.Cells["OrderType"].Value.ToString() == null) && (row.Cells["MarketLotSize"].Value.ToString() == null) && (row.Cells["TotalNumberofLotSize"].Value.ToString() == null) && (row.Cells["RoundOff"].Value.ToString() == null) && (row.Cells["MarginPerLot"].Value.ToString() == null) && (row.Cells["Stoploss"].Value.ToString() == null))
                    {
                    }
                    else
                    {
                        if (ExchangeList.SelectedItem != null && SymbolList.SelectedItem != null && ExpirydtList.Text != null && OrderTypeList.SelectedItem != null && ExpiryPeriodList.SelectedItem != null && txtroundoff.Text.ToString().Length > 0 && txtMarketLotSize.Text.ToString().Length > 0 && txtTotalNumberofLotSize.Text.ToString().Length > 0 && txtMarginLimit.Text.ToString().Length > 0 && txtStoploss.Text.ToString().Length > 0)
                        {
                            if (SymbolList.SelectedItem.ToString() == row.Cells["SymbolName"].Value.ToString())
                            {
                                if ((row.Cells[0].Value.Equals(SymbolList.SelectedItem.ToString())) && (row.Cells[1].Value.Equals("NFO")) && (row.Cells[2].Value.Equals(ExpirydtList.Text.ToString())) && (row.Cells[3].Value.Equals(ExpiryPeriodList.SelectedItem.ToString())) && (row.Cells[4].Value.Equals(OrderTypeList.SelectedItem.ToString())) && (row.Cells[5].Value.Equals(txtMarketLotSize.Text.ToString())) && (row.Cells[6].Value.Equals(txtTotalNumberofLotSize.Text.ToString())) && (row.Cells[7].Value.Equals(txtroundoff.Text.ToString())) && (row.Cells[8].Value.Equals(strikepricecallSubList.SelectedItem.ToString())) && (row.Cells[9].Value.Equals(strikepriceputSubList.SelectedItem.ToString())) && (row.Cells[10].Value.Equals(txtMarginLimit.Text.ToString())) && (row.Cells[11].Value.Equals(txtStoploss.Text.ToString())))
                                {
                                    if (AddUpdateRowinDataGrid(0, false) == true)
                                    {
                                        k += 1;
                                        SymbolList.SelectedIndex = -1;
                                        ClearFields();
                                        issaved = true;
                                        isAdded = true;
                                    }
                                    else
                                    {
                                        issaved = false;
                                    }
                                }
                                else
                                {
                                    foreach (DataGridViewRow r in dataGridView.SelectedRows)
                                    {
                                        r.Cells["SymbolName"].Value = SymbolList.Text;
                                        if (ExchangeList.Text == "OPTIONS")
                                        {
                                            r.Cells["Exchange"].Value = "NFO";
                                        }
                                        else
                                        {
                                            r.Cells["Exchange"].Value = ExchangeList.Text;
                                        }
                                        r.Cells["ExpiryDate"].Value = ExpirydtList.Text;
                                        r.Cells["ExpiryPeriod"].Value = ExpiryPeriodList.Text;
                                        r.Cells["OrderType"].Value = OrderTypeList.Text;
                                        r.Cells["MarketLotSize"].Value = txtMarketLotSize.Text;
                                        r.Cells["TotalNumberofLotSize"].Value = txtTotalNumberofLotSize.Text;
                                        r.Cells["RoundOff"].Value = txtroundoff.Text;
                                        r.Cells["CallStrikePrice"].Value = strikepricecallSubList.SelectedItem;
                                        r.Cells["PutStrikePrice"].Value = strikepriceputSubList.SelectedItem;
                                        r.Cells["MarginPerLot"].Value = txtMarginLimit.Text;
                                        r.Cells["Stoploss"].Value = txtStoploss.Text;
                                        SymbolList.SelectedIndex = -1;
                                        ClearFields();
                                        issaved = true;
                                    }
                                }
                            }
                            else
                            {
                                if (isAdded == false)
                                {
                                    bool isFound = false;
                                    foreach (DataGridViewRow r in dataGridView.Rows)
                                    {
                                        if (SymbolList.SelectedItem.ToString() == r.Cells["SymbolName"].Value.ToString())
                                        {
                                            isFound = true;
                                            break;
                                        }
                                        else
                                        {
                                            isFound = false;
                                        }
                                    }
                                    if (isFound == false)
                                    {
                                        if (AddUpdateRowinDataGrid(0, false) == true)
                                        {
                                            k += 1;
                                            SymbolList.SelectedIndex = -1;
                                            ClearFields();
                                            issaved = true;
                                            isAdded = true;
                                        }
                                        else
                                        {
                                            issaved = false;
                                        }
                                    }
                                }
                            }
                        }
                        if ((row.Cells["SymbolName"].Value.ToString().Length == 0) || (row.Cells["Exchange"].Value.ToString().Length == 0) || (row.Cells["ExpiryDate"].Value.ToString().Length == 0) || (row.Cells["ExpiryPeriod"].Value.ToString().Length == 0) || (row.Cells["OrderType"].Value.ToString().Length == 0) || (row.Cells["MarketLotSize"].Value.ToString().Length == 0) || (row.Cells["TotalNumberofLotSize"].Value.ToString().Length == 0) || (row.Cells["RoundOff"].Value.ToString().Length == 0) || (row.Cells["MarginPerLot"].Value.ToString().Length == 0) || (row.Cells["Stoploss"].Value.ToString().Length == 0))
                        {
                            message = "Not able to add this because null value is present.";
                            buttons = MessageBoxButtons.OK;
                            dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                            WriteUniquelogs("SymbolSettingLogs", "AddIntoINIFile : Null value present at :" + row.Cells["SymbolName"].Value.ToString() + row.Cells["Exchange"].Value.ToString() + (row.Cells["ExpiryDate"].Value.ToString().Length == 0) + (row.Cells["ExpiryPeriod"].Value.ToString().Length == 0) + (row.Cells["OrderType"].Value.ToString().Length == 0) + (row.Cells["MarketLotSize"].Value.ToString().Length == 0) + (row.Cells["TotalNumberofLotSize"].Value.ToString().Length == 0) + (row.Cells["RoundOff"].Value.ToString().Length == 0) + (row.Cells["MarginPerLot"].Value.ToString().Length == 0) + (row.Cells["Stoploss"].Value.ToString().Length == 0) + ". Not able to add this value.", MessageType.Informational);
                        }
                        else
                        {
                            if (row.Cells["SymbolName"].Value.ToString().Length > 0)
                            {
                                Match m = Regex.Match(row.Cells["ExpiryDate"].Value.ToString(), re);

                                if (m.Success)
                                {
                                    j++;
                                }
                                else
                                {
                                    message = "Not able to add " + row.Cells["SymbolName"].Value.ToString() + " because expiry date is not proper.";
                                    buttons = MessageBoxButtons.OK;
                                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                                    WriteUniquelogs("SymbolSettingLogs", "AddIntoINIFile : " + message, MessageType.Informational);
                                }
                            }
                            else
                            {
                                flagSymbolname = false;
                                message = "Not able to add " + row.Cells["SymbolName"].Value.ToString() + " because symbol name is not proper.";
                                buttons = MessageBoxButtons.OK;
                                dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                                WriteUniquelogs("SymbolSettingLogs", "AddIntoINIFile : " + message, MessageType.Informational);
                            }
                        }
                    }
                }

                if ((j == k) && (issaved = true))
                {
                    if (Settings == null)
                    {
                        Settings = new RealStockA1Strategy.ReadSettings(logger);
                    }
                    iniObj.clearTestingSymbol("TRADINGSYMBOL", null, null, inisymbolList);
                    foreach (DataGridViewRow row in dataGridView.Rows)
                    {
                        Settings.writeINIFileSymbolSetting("TRADINGSYMBOL", row.Cells["SymbolName"].Value.ToString().ToUpper() + "," + (row.Cells["Exchange"].Value.ToString().ToUpper() + "," + (row.Cells["ExpiryDate"].Value.ToString().ToUpper()) + "," + (row.Cells["ExpiryPeriod"].Value.ToString()) + "," + (row.Cells["OrderType"].Value.ToString()) + "," + (row.Cells["MarketLotSize"].Value.ToString()) + "," + (row.Cells["TotalNumberofLotSize"].Value.ToString()) + "," + (row.Cells["RoundOff"].Value.ToString()) + "," + (row.Cells["CallStrikePrice"].Value.ToString()) + "," + (row.Cells["PutStrikePrice"].Value.ToString()) + "," + (row.Cells["MarginPerLot"].Value.ToString()) + "," + (row.Cells["Stoploss"].Value.ToString())), inisymbolList);
                        counter++;
                        string rowOutout = row.Cells["SymbolName"].Value.ToString().ToUpper() + "," + (row.Cells["Exchange"].Value.ToString().ToUpper()) + "," + (row.Cells["ExpiryDate"].Value.ToString().ToUpper()) + "," + (row.Cells["ExpiryPeriod"].Value.ToString()) + "," + (row.Cells["OrderType"].Value.ToString()) + "," + (row.Cells["MarketLotSize"].Value.ToString()) + "," + (row.Cells["TotalNumberofLotSize"].Value.ToString()) + "," + (row.Cells["RoundOff"].Value.ToString()) + "," + (row.Cells["CallStrikePrice"].Value.ToString()) + "," + (row.Cells["PutStrikePrice"].Value.ToString()) + "," + (row.Cells["MarginPerLot"].Value.ToString()) + "," + (row.Cells["Stoploss"].Value.ToString());
                        WriteUniquelogs("SymbolSettingLogs", "AddIntoINIFile :  Details saved " + rowOutout, MessageType.Informational);
                    }

                    message = "Changes updated successfully!!!";
                    buttons = MessageBoxButtons.OK;
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);

                    WriteUniquelogs("SymbolSettingLogs", "AddIntoINIFile : " + message, MessageType.Informational);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs", "AddIntoINIFile : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void btnDeleteselectedrow_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridView.Rows.Count > 0)
                {
                    if (dataGridView.SelectedRows.Count == 1)
                    {
                        foreach (DataGridViewRow row in dataGridView.SelectedRows)
                        {
                            //IRDSPM::Pratiksha::04-06-2021::To delete perticular row
                            message = "Are you sure you want to delete the " + row.Cells[0].Value + " ?";
                            buttons = MessageBoxButtons.YesNo;
                            dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);

                            if (dialog == DialogResult.Yes)
                            {
                                dataGridView.Rows.RemoveAt(row.Index);
                                WriteUniquelogs("SymbolSettingLogs", "btnDeleteselectedrow_Click : Row deleted at :" + row.Cells[0].Value + " position.", MessageType.Informational);
                                //IRDSPM::Pratiksha::04-06-2021::For clearing details for deleted symbol
                                SymbolList.Text = null;
                                ClearFields();
                                strikepriceCallList.SelectedIndex = 0;
                                strikepriceputList.SelectedIndex = 0;
                                strikepricecallSubList.SelectedIndex = -1;
                                strikepriceputSubList.SelectedIndex = -1;
                                AddIntoINIFile(false);
                            }
                        }
                    }
                    else
                    {
                        message = "Please select the row first.";
                        buttons = MessageBoxButtons.OK;
                        dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                    }
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs", "btnDeleteselectedrow_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }
        public void WriteUniquelogs(string fileName, string message, MessageType msgType)
        {
            try
            {
                SymbolLogs log = new SymbolLogs();
                log.createFile(fileName);
                log.LogMessage(message, MessageType.Informational);
                log = null;
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs", "WriteUniquelogs : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                bool found = false;
                if (ExchangeList.SelectedItem != null && SymbolList.SelectedItem != null && ExpirydtList.Text != null && OrderTypeList.SelectedItem != null && ExpiryPeriodList.SelectedItem != null && txtroundoff.Text.ToString().Length > 0 && txtMarketLotSize.Text.ToString().Length > 0 && txtTotalNumberofLotSize.Text.ToString().Length > 0 && txtMarginLimit.Text.ToString().Length > 0 && txtStoploss.Text.ToString().Length > 0)
                {
                    Match rountOff1, MarketLotSize1, OneLotSize1, MarginPerLot1, Stoploss1;
                    Validations(out rountOff1, out MarketLotSize1, out OneLotSize1, out MarginPerLot1, out Stoploss1);
                    if (rountOff1.Success && MarketLotSize1.Success && OneLotSize1.Success && MarginPerLot1.Success && Stoploss1.Success)
                    {
                        foreach (DataGridViewRow row in this.dataGridView.Rows)
                        {
                            if (row.Cells[0].Value.Equals(SymbolList.SelectedItem.ToString()))
                            {
                                found = true;
                                break;
                            }
                        }
                        if (found == false)
                        {
                            AddUpdateRowinDataGrid(0, true);
                        }
                        else
                        {
                            message = "This entry is already exist.";
                            buttons = MessageBoxButtons.OK;
                            dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                            WriteUniquelogs("SymbolSettingLogs", "btnAdd_Click : " + message, MessageType.Informational);
                            ClearFields();
                            SymbolList.SelectedIndex = -1;
                        }
                    }
                    else
                    {
                        string Logmessage = "";
                        string parameter = "";
                        if (!MarketLotSize1.Success) { parameter += "\nMarket Lot Size"; Logmessage += "Market Lot Size,"; }
                        if (!OneLotSize1.Success) { parameter += "\nTotal Number of Lot Size"; Logmessage += "Total Number of Lot Size,"; }
                        if (!rountOff1.Success) { parameter += "\nRound Off"; Logmessage += "Round Off"; }
                        if (!MarginPerLot1.Success) { parameter += "\nMargin Per Lot"; Logmessage += "Margin Per Lot"; }
                        if (!Stoploss1.Success) { parameter += "\nStoploss"; Logmessage += "Stoploss"; }
                        message = "Please add valid values for " + parameter + ".";
                        buttons = MessageBoxButtons.OK;
                        dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                        WriteUniquelogs("SymbolSettingLogs", "btnAdd_Click : " + Logmessage, MessageType.Informational);
                    }
                }
                else
                {
                    CheckNullValuesAndShowPopup();
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs", "btnAdd_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private bool AddUpdateRowinDataGrid(int choice, bool SavedIniFile)
        {
            bool IsAdded = false;
            try
            {
                bool found = false;
                string exchange = "";
                if (ExchangeList.SelectedItem.ToString() == "OPTIONS")
                {
                    exchange = "NFO";
                }
                foreach (DataGridViewRow row in this.dataGridView.Rows)
                {
                    if ((row.Cells[0].Value.Equals(SymbolList.SelectedItem.ToString())) && (row.Cells[1].Value.Equals(exchange)) && (row.Cells[2].Value.Equals(ExpirydtList.Text.ToString())) && (row.Cells[3].Value.Equals(ExpiryPeriodList.SelectedItem.ToString())) && (row.Cells[4].Value.Equals(OrderTypeList.SelectedItem.ToString())) && (row.Cells[5].Value.Equals(txtMarketLotSize.Text.ToString())) && (row.Cells[6].Value.Equals(txtTotalNumberofLotSize.Text.ToString())) && (row.Cells[7].Value.Equals(txtroundoff.Text.ToString())) && (row.Cells[8].Value.Equals(strikepricecallSubList.SelectedItem.ToString())) && (row.Cells[9].Value.Equals(strikepriceputSubList.SelectedItem.ToString())) && (row.Cells[10].Value.Equals(txtMarginLimit.Text.ToString())) && (row.Cells[11].Value.Equals(txtStoploss.Text.ToString())))
                    {
                        found = true;
                        break;
                    }
                }

                if (!found)
                {
                    if (choice == 0)
                    {
                        string OutputRow = SymbolList.SelectedItem.ToString() + "," + exchange + "," + ExpirydtList.SelectedItem.ToString() + "," + ExpiryPeriodList.SelectedItem.ToString() + "," + OrderTypeList.SelectedItem.ToString() + "," + txtMarketLotSize.Text + "," + txtTotalNumberofLotSize.Text + "," + txtroundoff.Text + "," + strikepricecallSubList.SelectedItem + "," + strikepriceputSubList.SelectedItem + "," + txtMarginLimit.Text + "," + txtStoploss.Text;
                        WriteUniquelogs("SymbolSettingLogs", "AddUpdateRowinDataGrid : Row added in datagrid : " + OutputRow, MessageType.Informational);
                        dataGridView.Rows.Add(SymbolList.SelectedItem.ToString(), exchange, ExpirydtList.SelectedItem.ToString(), ExpiryPeriodList.SelectedItem.ToString(), OrderTypeList.SelectedItem.ToString(), txtMarketLotSize.Text, txtTotalNumberofLotSize.Text, txtroundoff.Text, strikepricecallSubList.SelectedItem, strikepriceputSubList.SelectedItem, txtMarginLimit.Text, txtStoploss.Text);
                        IsAdded = true;
                        ClearFields();
                        SymbolList.SelectedIndex = -1;
                    }
                    else
                    {
                        foreach (DataGridViewRow r in dataGridView.SelectedRows)
                        {
                            r.Cells["SymbolName"].Value = SymbolList.Text;
                            if (ExchangeList.Text == "OPTIONS")
                            {
                                r.Cells["Exchange"].Value = "NFO";
                            }
                            else
                            {
                                r.Cells["Exchange"].Value = ExchangeList.Text;
                            }
                            r.Cells["ExpiryDate"].Value = ExpirydtList.Text;
                            r.Cells["ExpiryPeriod"].Value = ExpiryPeriodList.Text;
                            r.Cells["OrderType"].Value = OrderTypeList.Text;
                            r.Cells["MarketLotSize"].Value = txtMarketLotSize.Text;
                            r.Cells["TotalNumberofLotSize"].Value = txtTotalNumberofLotSize.Text;
                            r.Cells["RoundOff"].Value = txtroundoff.Text;
                            r.Cells["CallStrikePrice"].Value = strikepricecallSubList.SelectedItem;
                            r.Cells["PutStrikePrice"].Value = strikepriceputSubList.SelectedItem;
                            r.Cells["MarginPerLot"].Value = txtMarginLimit.Text;
                            r.Cells["Stoploss"].Value = txtStoploss.Text;
                        }
                    }
                    if (SavedIniFile == true)
                    {
                        AddIntoINIFile();
                        IsAdded = true;
                        WriteUniquelogs("SymbolSettingLogs", "AddUpdateRowinDataGrid : Row added.", MessageType.Informational);
                    }
                    SymbolList.SelectedItem = null;
                    ExpirydtList.Text = null;
                    ExpiryPeriodList.Text = null;
                    OrderTypeList.Text = null;
                    txtroundoff.Text = null;
                    txtMarketLotSize.Text = null;
                    txtTotalNumberofLotSize.Text = null;
                    strikepricecallSubList.SelectedIndex = -1;
                    strikepriceputSubList.SelectedIndex = -1;
                    txtMarginLimit.Text = null;
                    txtStoploss.Text = null;
                }
                else
                {
                    message = "This entry is already exist.";
                    buttons = MessageBoxButtons.OK;
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                    WriteUniquelogs("SymbolSettingLogs", "AddUpdateRowinDataGrid : " + message, MessageType.Informational);
                    IsAdded = false;
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs", "AddUpdateRowinDataGrid : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
            return IsAdded;
        }

        private void SymbolList_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                List<string> finalOptionsList = new List<string>();
                ExpirydtList.Items.Clear();
                if (SymbolList.SelectedItem != null)
                {
                    string symbolname = SymbolList.SelectedItem.ToString();
                    string tablename = ExchangeList.SelectedItem.ToString();
                    finalOptionsList = ConnectWrapper.fetchExpiryDtForOptionSymbol(symbolname, tablename);
                    foreach (string listItem in finalOptionsList)
                    {
                        ExpirydtList.Items.Add(listItem);
                    }
                    strikepriceCallList.SelectedIndex = 0;
                    strikepriceputList.SelectedIndex = 0;
                    strikepricecallSubList.SelectedIndex = 10;
                    strikepriceputSubList.SelectedIndex = 10;
                    ClearFields();
                }
                WriteUniquelogs("SymbolSettingLogs", "SymbolList_SelectedValueChanged : Added expiry dates into list.", MessageType.Informational);
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs", "SymbolList_SelectedValueChanged : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void ExchangeList_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                finalExchangeList = null;
                SymbolList.SelectedItem = null;
                SymbolList.Items.Clear();
                if (ExchangeList.SelectedItem != null)
                {
                    if (ExchangeList.SelectedItem.ToString() == "OPTIONS")
                    {
                        finalExchangeList = ConnectWrapper.GetOptionsSymbols();
                        foreach (string listItem in finalExchangeList)
                        {
                            Trace.WriteLine("Symbol : Combo" + listItem);
                            SymbolList.Items.Add(listItem);
                        }
                    }
                    WriteUniquelogs("SymbolSettingLogs", "ExchangeList_SelectedValueChanged : SymbolList added.", MessageType.Informational);
                }
                else
                {
                    WriteUniquelogs("SymbolSettingLogs", "ExchangeList_SelectedValueChanged : ExpiryPeriodList got clear.", MessageType.Informational);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs", "ExchangeList_SelectedValueChanged : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void AccountList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (AccountList.SelectedItem.ToString() != "Choose Account")
                {
                    SymbolList.Text = null;
                    ClearFields();
                    dataGridView.Rows.Clear();
                    FetchDetailsFromINI(AccountList.SelectedItem.ToString());
                    WriteUniquelogs("SymbolSettingLogs", "AccountList_SelectedIndexChanged : Fetched details properly.", MessageType.Informational);
                }
                else
                {
                    GPAddDet.Enabled = false;
                    dataGridView.Rows.Clear();
                    WriteUniquelogs("SymbolSettingLogs", "AccountList_SelectedIndexChanged : dataGridView got clear.", MessageType.Informational);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs", "AccountList_SelectedIndexChanged : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void ClearFields()
        {
            try
            {
                ExpirydtList.Text = null;
                ExpiryPeriodList.Text = null;
                OrderTypeList.Text = null;
                txtroundoff.Text = null;
                txtMarketLotSize.Text = null;
                txtTotalNumberofLotSize.Text = null;
                GPAddDet.Enabled = true;
                txtMarginLimit.Text = null;
                txtStoploss.Text = null;
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs", "ClearFields : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void FetchDetailsFromINI(string Path)
        {
            try
            {
                string filePath = path + @"\StrategySetting_" + Path + ".ini";
                inisymbolList = filePath;
                int rank = 0;
                if (File.Exists(inisymbolList))
                {
                    try
                    {
                        if (Settings == null)
                        {
                            Settings = new RealStockA1Strategy.ReadSettings(logger);
                        }
                        //IRDSPM::Pratiksha::29-04-2021::Used existing method change for reading values
                        Settings.ReadTradingSymbols(inisymbolList);
                        foreach (var item in Settings.TradingSymbolsInfoList)
                        {
                            try
                            {
                                string TradingSymbol = item.TradingSymbol;
                                string Exchange = item.Exchange;
                                string ExpiryDate = item.ExpiryDate;
                                string ExpiryPeriod = item.ExpiryPeriod;
                                string OrderType = item.OrderType;
                                double lotsize = item.lotsize;
                                //IRDS::Pratiksha::08-06-2021::Add TotalNoOfLots 
                                double TotalNoOfLots = item.TotalNoOfLots;
                                double Roundoff = item.Roundoff;
                                //IRDSPM::Pratiksha::19-05-2021::For strike price
                                double StrikePricePositive = item.StrikePriceCall;
                                double StrikePriceNegative = item.StrikePricePut;
                                double MarginLimit = item.MarginLimit;
                                double stoploss = item.Stoploss;
                                dataGridView.Rows.Add(new object[] { TradingSymbol, Exchange, ExpiryDate, ExpiryPeriod, OrderType, lotsize, TotalNoOfLots, Roundoff, StrikePricePositive, StrikePriceNegative, MarginLimit, stoploss });
                                rank++;
                            }
                            catch (Exception ex)
                            {
                                WriteUniquelogs("SymbolSettingLogs", "FetchDetailsFromINI : Exception Error Message = " + ex.Message, MessageType.Exception);
                            }
                        }
                        dataGridView.AllowUserToAddRows = false;
                    }
                    catch (Exception ex)
                    {
                        WriteUniquelogs("SymbolSettingLogs", "FetchDetailsFromINI : Exception Error Message = " + ex.Message, MessageType.Exception);
                    }
                }
                else
                {
                    WriteUniquelogs("SymbolSettingLogs", "FetchDetailsFromINI : INI file not present.", MessageType.Informational);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs", "FetchDetailsFromINI : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                OnCancelClickSaveChanges();
                //IRDS::Jyoti::22-07-2021::Added for restore automation changes
                WriteUniquelogs("SymbolSettingLogs", "btnCancel_Click : Settings.TradingSymbolsInfoList.Count = " + Settings.TradingSymbolsInfoList.Count + " dataGridView.Rows.Count = " + dataGridView.Rows.Count, MessageType.Exception);
                if (Settings.TradingSymbolsInfoList.Count < dataGridView.Rows.Count)
                {
                    WriteUniquelogs("SymbolSettingLogs", "btnCancel_Click : Setting m_IsNewSymbolAdded = true", MessageType.Exception);
                    Settings.m_IsNewSymbolAdded = true;
                }

            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs", "btnCancel_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void dataGridView_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0)
                {
                    dataGridView.Rows[e.RowIndex].Selected = true;
                    DataGridViewRow row = dataGridView.Rows[e.RowIndex];
                    SymbolList.Text = row.Cells[0].Value.ToString();
                    if (row.Cells[1].Value.ToString() == "NFO")
                    {
                        ExchangeList.Text = "OPTIONS";
                    }
                    else
                    {
                        ExchangeList.Text = ExchangeList.Text;
                    }
                    ExpirydtList.Text = row.Cells[2].Value.ToString();
                    ExpiryPeriodList.Text = row.Cells[3].Value.ToString();
                    OrderTypeList.Text = row.Cells[4].Value.ToString();
                    txtMarketLotSize.Text = row.Cells[5].Value.ToString();
                    txtTotalNumberofLotSize.Text = row.Cells[6].Value.ToString();
                    txtroundoff.Text = row.Cells[7].Value.ToString();
                    strikepricecallSubList.ResetText();
                    strikepriceputSubList.ResetText();
                    strikepricecallSubList.Text = row.Cells[8].Value.ToString();
                    strikepriceputSubList.Text = row.Cells[9].Value.ToString();
                    txtMarginLimit.Text = row.Cells[10].Value.ToString();
                    txtStoploss.Text = row.Cells[11].Value.ToString();
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs", "dataGridView_CellMouseClick : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridView.SelectedRows.Count == 1)
                {
                    if (ExchangeList.SelectedItem != null && SymbolList.SelectedItem != null && ExpirydtList.Text != null && OrderTypeList.SelectedItem != null && ExpiryPeriodList.SelectedItem != null && txtroundoff.Text.ToString().Length > 0 && txtMarketLotSize.Text.ToString().Length > 0 && txtTotalNumberofLotSize.Text.ToString().Length > 0 && txtMarginLimit.Text.ToString().Length > 0 && txtStoploss.Text.ToString().Length > 0)
                    {
                        Match rountOff1, MarketLotSize1, OneLotSize1, MarginPerLot1, Stoploss1;
                        Validations(out rountOff1, out MarketLotSize1, out OneLotSize1, out MarginPerLot1, out Stoploss1);
                        if (rountOff1.Success && MarketLotSize1.Success && OneLotSize1.Success && MarginPerLot1.Success && Stoploss1.Success)
                        {
                            AddIntoINIFile();
                        }
                        else
                        {
                            string Logmessage = "";
                            string parameter = "";
                            if (!MarketLotSize1.Success) { parameter += "\nMarket Lot Size"; Logmessage += "Market Lot Size,"; }
                            if (!OneLotSize1.Success) { parameter += "\nTotal Number of Lot Size"; Logmessage += "Total Number of Lot Size,"; }
                            if (!rountOff1.Success) { parameter += "\nRound Off"; Logmessage += "Round Off"; }
                            if (!MarginPerLot1.Success) { parameter += "\nMargin Per Lot"; Logmessage += "Margin Per Lot"; }
                            if (!Stoploss1.Success) { parameter += "\nStoploss"; Logmessage += "Stoploss"; }
                            message = "Please add valid values for " + parameter + ".";
                            buttons = MessageBoxButtons.OK;
                            dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                            WriteUniquelogs("SymbolSettingLogs", "btnUpdate_Click : " + Logmessage, MessageType.Informational);
                        }
                    }
                    else
                    {
                        CheckNullValuesAndShowPopup();
                    }
                }
                else
                {
                    message = "Please select the row first.";
                    buttons = MessageBoxButtons.OK;
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs", "btnUpdate_Click : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void strikepriceCallList_SelectedValueChanged(object sender, EventArgs e)
        {
            strikepricecallSubList.Items.Clear();
            int count = Convert.ToInt32(strikepriceCallList.SelectedItem);
            for (int i = -count; i < 0; i++)
            {
                strikepricecallSubList.Items.Add(i);
            }
            for (int i = 1; i < count + 1; i++)
            {
                strikepricecallSubList.Items.Add(i);
            }
        }

        private void strikepriceputList_SelectedValueChanged(object sender, EventArgs e)
        {
            strikepriceputSubList.Items.Clear();
            int count = Convert.ToInt32(strikepriceputList.SelectedItem);
            for (int i = -count; i < 0; i++)
            {
                strikepriceputSubList.Items.Add(i);
            }
            for (int i = 1; i < count + 1; i++)
            {
                strikepriceputSubList.Items.Add(i);
            }
        }

        //IRDS::Pratiksha::07-06-2021::For fetching lotsize
        private void ExpirydtList_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (SymbolList.SelectedItem != null)
                {
                    string expirydt = null;
                    string symbolname = null;
                    string tablename = null;
                    try
                    {
                        symbolname = SymbolList.SelectedItem.ToString();
                        tablename = ExchangeList.SelectedItem.ToString();
                        expirydt = ExpirydtList.SelectedItem.ToString();
                    }
                    catch (Exception ex)
                    {
                        WriteUniquelogs("SymbolSettingLogs", "ExpirydtList_SelectedValueChanged : Exception Error Message = " + ex.Message, MessageType.Exception);
                    }
                    if (expirydt != null && symbolname != null && tablename != null)
                    {
                        DateTime dt = Convert.ToDateTime(expirydt);
                        DateTime date1 = new DateTime(dt.Year, dt.Month, dt.Day + 1);
                        string expiryDate = date1.ToUniversalTime().ToString("yyyy'-'MM'-'dd");
                        string UTCDate = expiryDate.Split(' ')[0] + "T14:30:00";
                        string quantity = ConnectWrapper.fetchLotsizeForOptionSymbol(symbolname, tablename, UTCDate);
                        txtMarketLotSize.Text = null;
                        txtMarketLotSize.Text = quantity;
                    }
                }
                WriteUniquelogs("SymbolSettingLogs", "ExpirydtList_SelectedValueChanged : Added expiry dates into list.", MessageType.Informational);
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs", "ExpirydtList_SelectedValueChanged : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }

        private void btnClearAll_Click(object sender, EventArgs e)
        {
            SymbolList.SelectedIndex = -1;
            ClearFields();
        }

        private bool OnCancelClickSaveChanges()
        {
            bool formCloseStatus = false;
            bool changeFound = false;
            try
            {
                if (ExchangeList.SelectedItem != null || SymbolList.SelectedItem != null || ExpirydtList.Text != null || OrderTypeList.SelectedItem != null || ExpiryPeriodList.SelectedItem != null || txtroundoff.Text.ToString().Length > 0 || txtMarketLotSize.Text.ToString().Length > 0 || txtTotalNumberofLotSize.Text.ToString().Length > 0 || txtMarginLimit.Text.ToString().Length > 0 || txtStoploss.Text.ToString().Length > 0)
                {
                    foreach (DataGridViewRow row in dataGridView.Rows)
                    {
                        if (SymbolList.SelectedItem != null)
                        {
                            if (row.Cells[0].Value.Equals(SymbolList.SelectedItem.ToString()) && (row.Cells[1].Value.Equals("NFO")))
                            {
                                if ((row.Cells[2].Value.ToString().Equals(ExpirydtList.Text.ToString())) && (row.Cells[3].Value.ToString().Equals(ExpiryPeriodList.SelectedItem.ToString())) && (row.Cells[4].Value.ToString().Equals(OrderTypeList.SelectedItem.ToString())) && (row.Cells[5].Value.ToString().Equals(txtMarketLotSize.Text.ToString())) && (row.Cells[6].Value.ToString().Equals(txtTotalNumberofLotSize.Text.ToString())) && (row.Cells[7].Value.ToString().Equals(txtroundoff.Text.ToString())) && (row.Cells[8].Value.ToString().Equals(strikepricecallSubList.Text.ToString())) && (row.Cells[9].Value.ToString().Equals(strikepriceputSubList.Text.ToString())) && (row.Cells[10].Value.ToString().Equals(txtMarginLimit.Text.ToString())) && (row.Cells[11].Value.ToString().Equals(txtStoploss.Text.ToString())))
                                {
                                    changeFound = false;
                                    isexit = true;
                                    break;
                                }
                                else
                                {
                                    changeFound = true;
                                }
                            }
                            else
                            {
                                changeFound = true;
                            }
                        }
                    }
                    if (changeFound == true)
                    {
                        message = "You have some unsaved changes, do you want to save those changes?";
                        buttons = MessageBoxButtons.YesNo;
                        dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                        if (DialogResult.Yes == dialog)
                        {
                            if (ExchangeList.SelectedItem != null && SymbolList.SelectedItem != null && ExpirydtList.SelectedItem != null && OrderTypeList.SelectedItem != null && ExpiryPeriodList.SelectedItem != null && txtroundoff.Text.ToString().Length > 0 && txtMarketLotSize.Text.ToString().Length > 0 && txtTotalNumberofLotSize.Text.ToString().Length > 0 && strikepricecallSubList.SelectedItem != null && strikepriceputSubList.SelectedItem != null && txtMarginLimit.Text.ToString().Length > 0 && txtStoploss.Text.ToString().Length > 0)
                            {
                                Match rountOff1, MarketLotSize1, OneLotSize1, MarginPerLot1, Stoploss1;
                                Validations(out rountOff1, out MarketLotSize1, out OneLotSize1, out MarginPerLot1, out Stoploss1);
                                if (rountOff1.Success && MarketLotSize1.Success && OneLotSize1.Success && MarginPerLot1.Success && Stoploss1.Success)
                                {
                                    AddIntoINIFile();
                                    isexit = true;
                                    this.Close();
                                }
                                else
                                {
                                    string Logmessage = "";
                                    string parameter = "";
                                    if (!MarketLotSize1.Success) { parameter += "\nMarket Lot Size"; Logmessage += "Market Lot Size,"; }
                                    if (!OneLotSize1.Success) { parameter += "\nTotal Number of Lot Size"; Logmessage += "Total Number of Lot Size,"; }
                                    if (!rountOff1.Success) { parameter += "\nRound Off"; Logmessage += "Round Off"; }
                                    if (!MarginPerLot1.Success) { parameter += "\nMargin Per Lot"; Logmessage += "Margin Per Lot"; }
                                    if (!Stoploss1.Success) { parameter += "\nStoploss"; Logmessage += "Stoploss"; }
                                    message = "Please add valid values for " + parameter + ".";
                                    buttons = MessageBoxButtons.OK;
                                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                                    WriteUniquelogs("SymbolSettingLogs", "OnCancelClickSaveChanges : " + Logmessage, MessageType.Informational);
                                }
                            }
                            else
                            {
                                CheckNullValuesAndShowPopup();
                                formCloseStatus = false;
                            }
                        }
                        else
                        {
                            isexit = true;
                            this.Close();
                            WriteUniquelogs("SymbolSettingLogs", "OnCancelClickSaveChanges : You clicked on No.", MessageType.Informational);
                        }
                    }
                    else
                    {
                        isexit = true;
                        this.Close();
                        WriteUniquelogs("SymbolSettingLogs", "OnCancelClickSaveChanges : No change found so close the form.", MessageType.Informational);
                    }
                }
                else
                {
                    this.Close();
                    WriteUniquelogs("SymbolSettingLogs", "OnCancelClickSaveChanges : No change found so close the form.", MessageType.Informational);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs", "OnCancelClickSaveChanges : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
            return formCloseStatus;
        }
        private void SymbolSettingDet_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (isexit != true)
            {
                message = "Are you sure you want to close the form?";
                buttons = MessageBoxButtons.YesNo;
                dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                if (DialogResult.Yes == dialog)
                {
                    //IRDS::Jyoti::22-07-2021::Added for restore automation changes
                    WriteUniquelogs("SymbolSettingLogs", "SymbolSettingDet_FormClosing : Settings.TradingSymbolsInfoList.Count = " + Settings.TradingSymbolsInfoList.Count + " dataGridView.Rows.Count = " + dataGridView.Rows.Count, MessageType.Exception);
                    if (Settings.TradingSymbolsInfoList.Count < dataGridView.Rows.Count)
                    {
                        WriteUniquelogs("SymbolSettingLogs", "SymbolSettingDet_FormClosing : Setting m_IsNewSymbolAdded = true", MessageType.Exception);
                        Settings.m_IsNewSymbolAdded = true;
                    }
                    e.Cancel = false;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }
        private void CheckNullValuesAndShowPopup()
        {
            string Logmessage = "Please add Proper values for ";
            string parameter = "";
            if (ExchangeList.SelectedItem == null) { parameter += "Instruments"; Logmessage += "Instruments,"; }
            if (SymbolList.SelectedItem == null) { parameter += "\nSymbol"; Logmessage += "Symbol,"; }
            if (ExpirydtList.SelectedItem == null) { parameter += "\nExpiry Date"; Logmessage += "Expiry Date,"; }
            if (ExpiryPeriodList.SelectedItem == null) { parameter += "\nExpiry Period"; Logmessage += "Expiry Period,"; }
            if (OrderTypeList.SelectedItem == null) { parameter += "\nOrder Type"; Logmessage += "Order Type,"; }
            if (txtMarketLotSize.Text.ToString().Length == 0) { parameter += "\nMarket Lot Size"; Logmessage += "Market Lot Size,"; }
            if (txtTotalNumberofLotSize.Text.ToString().Length == 0) { parameter += "\nTotal Number of Lot Size"; Logmessage += "Total Number of Lot Size,"; }
            if (txtroundoff.Text.ToString().Length == 0) { parameter += "\nRound Off"; Logmessage += "Round Off,"; }
            if (strikepricecallSubList.SelectedItem == null) { parameter += "\nCall Strike Multiplier"; Logmessage += "Call Strike Multiplier,"; }
            if (strikepriceputList.SelectedItem == null) { parameter += "\nPut Strike Multiplier"; Logmessage += "Put Strike Multiplier,"; }
            if (txtMarginLimit.Text.ToString().Length == 0) { parameter += "\nMargin Per Lot"; Logmessage += "Margin Per Lot,"; }
            if (txtStoploss.Text.ToString().Length == 0) { parameter += "\nStoploss"; Logmessage += "Stoploss"; }
            message = "Please add Proper values for " + parameter + ".";
            buttons = MessageBoxButtons.OK;
            dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
            WriteUniquelogs("SymbolSettingLogs", "CheckNullValuesAndShowPopup : " + Logmessage, MessageType.Informational);
        }
        private void Validations(out Match rountOff1, out Match MarketLotSize1, out Match OneLotSize1, out Match MarginPerLot1, out Match Stoploss1)
        {
            //string Decpattern = "^[-+]?[.]?([0-9]+[.]?[0-9]+)$";
            // string Intpattern = "^[0-9]*$";
            //string Decpattern = @" -?\d+(?:\.\d+)?";
            //string Decpattern = @"^(?:\d{ 1,2})?(?:\.\d{ 1,2})?$";

            string Decpattern = "^[0-9]+(.|,)?[0-9]*?$";
            //For roundoff
            Regex r1 = new Regex(Decpattern);
            rountOff1 = r1.Match(txtroundoff.Text.ToString());
            MarginPerLot1 = r1.Match(txtMarginLimit.Text.ToString());
            MarketLotSize1 = r1.Match(txtMarketLotSize.Text.ToString());
            OneLotSize1 = r1.Match(txtTotalNumberofLotSize.Text.ToString());
            Stoploss1 = r1.Match(txtStoploss.Text.ToString());
        }
        private void AddIntoINIFile()
        {
            bool issaved = false;
            bool isAdded = false;
            int j = 0;
            int counter = 0;
            int k = dataGridView.Rows.Count;
            bool flagSymbolname = false;
            var re = @"^[0-9]{2}[a-zA-Z]{3}[0-9]{2}$";
            INIFile iniObj = new INIFile(inisymbolList);
            DialogResult dialog;
            try
            {
                foreach (DataGridViewRow row in dataGridView.Rows)
                {
                    flagSymbolname = false;
                    if ((row.Cells["SymbolName"].Value.ToString().Length == 0) || (row.Cells["Exchange"].Value.ToString().Length == 0) || (row.Cells["ExpiryDate"].Value.ToString().Length == 0) || (row.Cells["ExpiryPeriod"].Value.ToString().Length == 0) || (row.Cells["OrderType"].Value.ToString().Length == 0) || (row.Cells["MarketLotSize"].Value.ToString().Length == 0) || (row.Cells["TotalNumberofLotSize"].Value.ToString().Length == 0) || (row.Cells["RoundOff"].Value.ToString().Length == 0) || (row.Cells["CallStrikePrice"].Value.ToString().Length == 0) || (row.Cells["PutStrikePrice"].Value.ToString().Length == 0) || (row.Cells["MarginPerLot"].Value.ToString().Length == 0) || (row.Cells["Stoploss"].Value.ToString().Length == 0))
                    {
                    }
                    else
                    {
                        if (ExchangeList.SelectedItem != null && SymbolList.SelectedItem != null && ExpirydtList.Text != null && OrderTypeList.SelectedItem != null && ExpiryPeriodList.SelectedItem != null && txtroundoff.Text.ToString().Length > 0 && txtMarketLotSize.Text.ToString().Length > 0 && txtTotalNumberofLotSize.Text.ToString().Length > 0 && strikepricecallSubList.SelectedItem != null && strikepriceputSubList.SelectedItem != null)
                        {
                            if (SymbolList.SelectedItem.ToString() == row.Cells["SymbolName"].Value.ToString())
                            {

                                if ((row.Cells[0].Value.Equals(SymbolList.SelectedItem.ToString())) && (row.Cells[1].Value.Equals("NFO")) && (row.Cells[2].Value.Equals(ExpirydtList.Text.ToString())) && (row.Cells[3].Value.Equals(ExpiryPeriodList.SelectedItem.ToString())) && (row.Cells[4].Value.Equals(OrderTypeList.SelectedItem.ToString())) && (row.Cells[5].Value.Equals(txtMarketLotSize.Text.ToString())) && (row.Cells[6].Value.Equals(txtTotalNumberofLotSize.Text.ToString())) && (row.Cells[7].Value.Equals(txtroundoff.Text.ToString())) && (row.Cells[8].Value.Equals(strikepricecallSubList.SelectedItem.ToString())) && (row.Cells[9].Value.Equals(strikepriceputSubList.SelectedItem.ToString())) && (row.Cells[10].Value.Equals(txtMarginLimit.Text.ToString())) && (row.Cells[10].Value.Equals(txtStoploss.Text.ToString())))
                                {
                                    if (AddUpdateRowinDataGrid(0, false) == true)
                                    {
                                        k += 1;
                                        SymbolList.SelectedIndex = -1;
                                        ClearFields();
                                        issaved = true;
                                        isAdded = true;
                                    }
                                    else
                                    {
                                        issaved = false;
                                    }
                                }
                                else
                                {
                                    foreach (DataGridViewRow r in dataGridView.SelectedRows)
                                    {
                                        r.Cells["SymbolName"].Value = SymbolList.Text;
                                        if (ExchangeList.Text == "OPTIONS")
                                        {
                                            r.Cells["Exchange"].Value = "NFO";
                                        }
                                        else
                                        {
                                            r.Cells["Exchange"].Value = ExchangeList.Text;
                                        }
                                        r.Cells["ExpiryDate"].Value = ExpirydtList.Text;
                                        r.Cells["ExpiryPeriod"].Value = ExpiryPeriodList.Text;
                                        r.Cells["OrderType"].Value = OrderTypeList.Text;
                                        r.Cells["MarketLotSize"].Value = txtMarketLotSize.Text;
                                        r.Cells["TotalNumberofLotSize"].Value = txtTotalNumberofLotSize.Text;
                                        r.Cells["RoundOff"].Value = txtroundoff.Text;
                                        r.Cells["CallStrikePrice"].Value = strikepricecallSubList.SelectedItem;
                                        r.Cells["PutStrikePrice"].Value = strikepriceputSubList.SelectedItem;
                                        r.Cells["MarginPerLot"].Value = txtMarginLimit.Text;
                                        r.Cells["Stoploss"].Value = txtStoploss.Text;
                                        SymbolList.SelectedIndex = -1;
                                        ClearFields();
                                        issaved = true;
                                    }
                                }
                            }
                            else
                            {
                                if (isAdded == false)
                                {
                                    bool isFound = false;
                                    foreach (DataGridViewRow r in dataGridView.Rows)
                                    {
                                        if (SymbolList.SelectedItem.ToString() == r.Cells["SymbolName"].Value.ToString())
                                        {
                                            isFound = true;
                                            break;
                                        }
                                        else
                                        {
                                            isFound = false;
                                        }
                                    }
                                    if (isFound == false)
                                    {
                                        if (AddUpdateRowinDataGrid(0, false) == true)
                                        {
                                            k += 1;
                                            SymbolList.SelectedIndex = -1;
                                            ClearFields();
                                            issaved = true;
                                            isAdded = true;
                                        }
                                        else
                                        {
                                            issaved = false;
                                        }
                                    }
                                }
                            }
                        }
                        if ((row.Cells["SymbolName"].Value.ToString().Length == 0) || (row.Cells["Exchange"].Value.ToString().Length == 0) || (row.Cells["ExpiryDate"].Value.ToString().Length == 0) || (row.Cells["ExpiryPeriod"].Value.ToString().Length == 0) || (row.Cells["OrderType"].Value.ToString().Length == 0) || (row.Cells["MarketLotSize"].Value.ToString().Length == 0) || (row.Cells["TotalNumberofLotSize"].Value.ToString().Length == 0) || (row.Cells["RoundOff"].Value.ToString().Length == 0) || (row.Cells["CallStrikePrice"].Value.ToString().Length == 0) || (row.Cells["PutStrikePrice"].Value.ToString().Length == 0) || (row.Cells["MarginPerLot"].Value.ToString().Length == 0) || (row.Cells["Stoploss"].Value.ToString().Length == 0))
                        {
                            message = "Not able to add this because null value is present.";
                            buttons = MessageBoxButtons.OK;
                            dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                            WriteUniquelogs("SymbolSettingLogs", "AddIntoINIFile : Null value present at :" + row.Cells["SymbolName"].Value.ToString() + row.Cells["Exchange"].Value.ToString() + (row.Cells["ExpiryDate"].Value.ToString().Length == 0) + (row.Cells["ExpiryPeriod"].Value.ToString().Length == 0) + (row.Cells["OrderType"].Value.ToString().Length == 0) + (row.Cells["MarketLotSize"].Value.ToString().Length == 0) + (row.Cells["TotalNumberofLotSize"].Value.ToString().Length == 0) + (row.Cells["RoundOff"].Value.ToString().Length == 0) + (row.Cells["MarginPerLot"].Value.ToString().Length == 0) + (row.Cells["Stoploss"].Value.ToString().Length == 0) + ". Not able to add this value.", MessageType.Informational);
                        }
                        else
                        {
                            if (row.Cells["SymbolName"].Value.ToString().Length > 0)
                            {
                                Match m = Regex.Match(row.Cells["ExpiryDate"].Value.ToString(), re);

                                if (m.Success)
                                {
                                    j++;
                                }
                                else
                                {
                                    message = "Not able to add " + row.Cells["SymbolName"].Value.ToString() + " because expiry date is not proper.";
                                    buttons = MessageBoxButtons.OK;
                                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                                    WriteUniquelogs("SymbolSettingLogs", "AddIntoINIFile : " + message, MessageType.Informational);
                                }
                            }
                            else
                            {
                                flagSymbolname = false;
                                message = "Not able to add " + row.Cells["SymbolName"].Value.ToString() + " because symbol name is not proper.";
                                buttons = MessageBoxButtons.OK;
                                dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                                WriteUniquelogs("SymbolSettingLogs", "AddIntoINIFile : " + message, MessageType.Informational);
                            }
                        }
                    }
                }

                if ((j == k) && (issaved = true))
                {
                    if (Settings == null)
                    {
                        Settings = new RealStockA1Strategy.ReadSettings(logger);
                    }
                    iniObj.clearTestingSymbol("TRADINGSYMBOL", null, null, inisymbolList);
                    foreach (DataGridViewRow row in dataGridView.Rows)
                    {
                        Settings.writeINIFileSymbolSetting("TRADINGSYMBOL", row.Cells["SymbolName"].Value.ToString().ToUpper() + "," + (row.Cells["Exchange"].Value.ToString().ToUpper()) + "," + (row.Cells["ExpiryDate"].Value.ToString().ToUpper()) + /*"," + Strikevalue + */"," + (row.Cells["ExpiryPeriod"].Value.ToString()) + "," + (row.Cells["OrderType"].Value.ToString()) + "," + (row.Cells["MarketLotSize"].Value.ToString()) + "," + (row.Cells["TotalNumberofLotSize"].Value.ToString()) + "," + (row.Cells["RoundOff"].Value.ToString()) + "," + (row.Cells["CallStrikePrice"].Value.ToString()) + "," + (row.Cells["PutStrikePrice"].Value.ToString()) + "," + (row.Cells["MarginPerLot"].Value.ToString()) + "," + (row.Cells["Stoploss"].Value.ToString()), inisymbolList);
                        counter++;
                        string rowOutout = row.Cells["SymbolName"].Value.ToString().ToUpper() + "," + (row.Cells["Exchange"].Value.ToString().ToUpper()) + "," + (row.Cells["ExpiryDate"].Value.ToString().ToUpper()) +/* "," + Strikevalue +*/ "," + (row.Cells["ExpiryPeriod"].Value.ToString()) + "," + (row.Cells["OrderType"].Value.ToString()) + "," + (row.Cells["MarketLotSize"].Value.ToString()) + "," + (row.Cells["TotalNumberofLotSize"].Value.ToString()) + "," + (row.Cells["RoundOff"].Value.ToString()) + "," + (row.Cells["CallStrikePrice"].Value.ToString()) + "," + (row.Cells["PutStrikePrice"].Value.ToString()) + "," + (row.Cells["MarginPerLot"].Value.ToString()) + "," + (row.Cells["Stoploss"].Value.ToString());
                        WriteUniquelogs("SymbolSettingLogs", "AddIntoINIFile :  Details saved " + rowOutout, MessageType.Informational);
                    }

                    message = "Changes updated successfully!!!";
                    buttons = MessageBoxButtons.OK;
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);

                    WriteUniquelogs("SymbolSettingLogs", "AddIntoINIFile : " + message, MessageType.Informational);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("SymbolSettingLogs", "AddIntoINIFile : Exception Error Message = " + ex.Message, MessageType.Exception);
            }
        }
    }
}
