﻿using IRDSAlgoOMS;
using RealStockA1Strategy;
using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace RealStockA1Strategy
{
    public partial class TradingSetting : Form
    {
        [field: NonSerialized()]
        AlgoOMS AlgoOMSObj;
        //sanika::25-sep-2020::added for resume 
        [field: NonSerialized()]
        RealStockA1Strategy.ReadSettings readSettings;
        //sanika::25-sep-2020::added for resume 
        [field: NonSerialized()]
        Logger logger;
        string path = System.IO.Directory.GetCurrentDirectory();

        string message = "";
        string title = "RealStockA1 Strategy";
        MessageBoxButtons buttons;
        string m_iniChoose = "";
        DialogResult dialog;
        INIFile iniObj = null;
        string userID = null;
        static string ConfigFilePath = System.AppDomain.CurrentDomain.BaseDirectory + "Configuration";
        string accountNumber = "";
        string StartEntryTime = "";
        string EndEntryTime = "";
        string EndExitTime = "";
        //string StopLossPercent = "";
        string BreakEvenStop = "";
        string StoplossRejectTimeInterval = "";
        string AdditionalAccount = "";
        public TradingSetting(Logger logger, AlgoOMS obj, string userid)
        {
            InitializeComponent();
            try
            {
            this.AlgoOMSObj = obj;
            this.logger = logger;
                userID = userid;
            string[] files = Directory.GetFiles(ConfigFilePath, "StrategySetting_*");
            foreach (string fileName in files)
            {
                string file = Path.GetFileNameWithoutExtension(fileName);
                AccountDetList.Items.Add(file.Split('_')[1]);
                    AdditionalList.Items.Add(file.Split('_')[1]);
            }

                //IRDSPM::Pratiksha::28-05-2021::for selected account need to be default selected
                if (AccountDetList.Items.Contains(userid))
                {
                    AccountDetList.SelectedItem = userid;
                    AdditionalList.SelectedItem = userid;
                }
                else
                {
            AccountDetList.SelectedIndex = 0;
                AdditionalList.SelectedIndex = 0;
                }
            txtAccountNumber.Enabled = false;
                if (readSettings == null)
                {
                    readSettings = new RealStockA1Strategy.ReadSettings(logger);
                }
            readSettings = new RealStockA1Strategy.ReadSettings(this.logger, this.AlgoOMSObj);           
        }
            catch (Exception ex)
            {
                WriteUniquelogs("TradingSettingLogs", "InitializeComponent : Exception Error Message = " + ex.Message, MessageType.Informational);
            }
        }
        private void FetchDetailsFromINI(string Path)
        {
            try
            {
                
                string filePath = path + @"\Configuration\StrategySetting_" + Path + ".ini";
                if (File.Exists(filePath))
                {
                    if(readSettings == null)
                    {
                        readSettings = new RealStockA1Strategy.ReadSettings(logger);
                    }
                    readSettings.ReadINIFile(filePath);
                    accountNumber = readSettings.m_AccountNumber.ToString();
                    StartEntryTime = readSettings.m_StartEntryTime.ToString();
                    EndEntryTime = readSettings.m_EndEntryTime.ToString();
                    EndExitTime = readSettings.m_EndExitTime.ToString();
                   // StopLossPercent = readSettings.m_StopLossPercent.ToString();
                    BreakEvenStop = readSettings.m_BreakEvenStop.ToString();
                    StoplossRejectTimeInterval = readSettings.m_StoplossRejectTimeInterval.ToString();
                    AdditionalAccount = readSettings.m_AdditionalAccount.ToString();

                    txtAccountNumber.Text = accountNumber;
                    txtStartEntryTime.Value = Convert.ToDateTime(StartEntryTime);
                    txtEndEntryTime.Value = Convert.ToDateTime(EndEntryTime);
                    txtEndExitTime.Value = Convert.ToDateTime(EndExitTime);
                    //txtStoplossPercent.Text = StopLossPercent;

                    if (BreakEvenStop.ToLower() == "true")
                    {
                        TrueBreakEvenStop.Checked = true;
                    }
                    else if (BreakEvenStop.ToString().ToLower() == "false")
                    {
                        FalseBreakEvenStop.Checked = true;
                    }
                    txtStoplossRejectTimeInterval.Text = StoplossRejectTimeInterval;
                    txtAdditionalAccount.Text = AdditionalAccount;
                       // txtStartExitTime.Value = Convert.ToDateTime(readSettings.m_StartExitTime);
                    //IRDS::Pratiksha::08-06-2021::Comment 2 fields
                    //txtInterval.Text = readSettings.m_Interval.ToString();
                    //txtNoOfLots.Text = readSettings.m_NoOfLots.ToString();
                   // txtCallStrikeValue.Text = readSettings.m_CallStrikeValue.ToString();
                   // txtPutStrikeValue.Text = readSettings.m_PutStrikeValue.ToString();
                    //txtReducePercentInLot.Text = readSettings.m_ReducePercentInLot.ToString();
                    //IRDSPM::Pratiksha::14-05-2021::For aditional account details
                    WriteUniquelogs("TradingSettingLogs", "FetchDetailsFromINI : Reading details from INI file.", MessageType.Informational);
                }
                else
                {
                    Clearfields();
                    WriteUniquelogs("TradingSettingLogs", "FetchDetailsFromINI : INI file not exist.", MessageType.Informational);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("TradingSettingLogs", "FetchDetailsFromINI : Exception Error Message = " + ex.Message, MessageType.Informational);
            }
        }
        private void btnApply_Click(object sender, EventArgs e)
        {
            try
            {
                //string[] totalAccount = null;
                //if (txtAdditionalAccount.Text.Length > 0)
                //{
                //    totalAccount = txtAdditionalAccount.Text.Split(',');
                //}
                if (txtAccountNumber.Text != "" && txtStartEntryTime.Text != "" && txtEndEntryTime.Text != "" && txtEndExitTime.Text != ""  && txtStoplossRejectTimeInterval.Text != "")//&& txtStoplossPercent.Text != ""
                    {
                    message = "Do you want to save the changes for " + txtAdditionalAccount.Text.ToString() + "?";
                        buttons = MessageBoxButtons.YesNo;
                        dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                        if (dialog == DialogResult.Yes)
                        {
                        SavedInIniFile(); // (totalAccount);

                        WriteUniquelogs("TradingSettingLogs", "btnApply_Click :  Details saved in ini file.", MessageType.Informational);
                    }
                    else
                    {
                        this.Close();
                        WriteUniquelogs("TradingSettingLogs", "btnApply_Click : You have clicked No, no data updated in trade setting.ini file.", MessageType.Informational);
                    }
                }
                else
                {
                    string parameter = "";
                    if (txtAccountNumber.Text == "") { parameter += "\n Account Number"; }
                    if (txtStartEntryTime.Text == "") { parameter += "\nStart Entry Time"; }
                    if (txtEndEntryTime.Text == "") { parameter += "\nEnd Entry Time"; }
                    if (txtEndExitTime.Text == "") { parameter += "\nEnd Exit Time"; }
                    //if (txtStoplossPercent.Text == "") { parameter += "\nStoploss Percent"; }
                    if (txtStoplossRejectTimeInterval.Text == "") { parameter += "\nStoploss Reject Time Interval"; }
                    message = "Please enter proper values for following field: " + parameter;
                    buttons = MessageBoxButtons.OK;
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                    WriteUniquelogs("TradingSettingLogs", "btnApply_Click : Please enter proper values.", MessageType.Informational);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("TradingSettingLogs", "btnApply_Click : Exception Error Message = " + ex.Message, MessageType.Informational);
            }
        }

        private void SavedInIniFile()//(string[] totalAccount)
                        {
            //for (int ii = 0; ii < totalAccount.Length; ii++)
            //{
            string filePath = ConfigFilePath + "\\StrategySetting_" + txtAccountNumber.Text + ".ini";
                                iniObj = new INIFile(filePath);
                            string[] key = new string[8];
                            string[] value = new string[8];
                            key[0] = "AccountNumber";
                            key[1] = "StartEntryTime";
                            key[2] = "EndEntryTime";
                            key[3] = "EndExitTime";
            //key[4] = "StoplossPercent";
            key[4] = "BreakEvenStop";
            key[5] = "StoplossRejectTimeInterval";
            key[6] = "AdditionalAccount";

                            value[0] = txtAccountNumber.Text;
                            value[1] = txtStartEntryTime.Text;
                            value[2] = txtEndEntryTime.Text;
                            value[3] = txtEndExitTime.Text;
            //value[4] = txtStoplossPercent.Text;
                            if (TrueBreakEvenStop.Checked == true)
                            {
                value[4] = "true";
                            }
                            else if (FalseBreakEvenStop.Checked == true)
                            {
                value[4] = "false";
                            }
            value[5] = txtStoplossRejectTimeInterval.Text;
            value[6] = txtAdditionalAccount.Text;
            for (int i = 0; i < 7; i++)
                            {
                                iniObj.clearTestingSymbol("SDATA", key[i], value[i], filePath);
                                WriteUniquelogs("TradingSettingLogs", "btnApply_Click :  Details saved " + key[i]+ " = "+ value[i], MessageType.Informational);
                            }
            //}
                            message = "Saved changes successfully!!!";
                            buttons = MessageBoxButtons.OK;
                            dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                            if (dialog == DialogResult.OK)
                            {
                                this.Close();
                            }
        }

        public void WriteUniquelogs(string symbol, string message, MessageType msgType)
        {
            try
            {
                SymbolLogs log = new SymbolLogs();
                log.createFile(symbol);
                log.LogMessage(message, MessageType.Informational);
                log = null;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : " + e.Message);
                Trace.WriteLine("Exception : " + e.Message);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                bool currentBreakEvenStop = false;
                if (TrueBreakEvenStop.Checked == true)
                {
                    currentBreakEvenStop = true;
                }
                else if (FalseBreakEvenStop.Checked == true)
                {
                    currentBreakEvenStop = false;
                }
                if ((StartEntryTime != txtStartEntryTime.Text) || (EndEntryTime != txtEndEntryTime.Text) ||
                    (EndExitTime != txtEndExitTime.Text) || //(StopLossPercent != txtStoplossPercent.Text) ||
                 (BreakEvenStop != currentBreakEvenStop.ToString()) ||
                (StoplossRejectTimeInterval != txtStoplossRejectTimeInterval.Text))// || (readSettings.m_Stoploss.ToString() != txtstoploss.Text))
                {
                    message = "You have some unsaved changes, do you want to save those changes?";
                    buttons = MessageBoxButtons.YesNo;
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                    if (DialogResult.Yes == dialog)
                    {
                        SavedInIniFile();
                    }
                    else
                    {
                        this.Close();
                    }
                }
                else
                {
                    this.Close();
                }
                WriteUniquelogs("TradingSettingLogs", "btnCancel_Click : Cancel click.", MessageType.Informational);
            }
            catch (Exception ex)
            {
                WriteUniquelogs("TradingSettingLogs", "btnCancel_Click : Exception Error Message = " + ex.Message, MessageType.Informational);
            }
        }

        private void AccountDetList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
            FetchDetailsFromINI(AccountDetList.SelectedItem.ToString());
                if(AccountDetList.SelectedItem.ToString() != "Choose Account")
                {
                    groupBox1.Enabled = true;
                }
                else
                {
                    groupBox1.Enabled = false;
                }
        }
            catch (Exception ex)
            {
                WriteUniquelogs("TradingSettingLogs", "AccountDetList_SelectedIndexChanged : Exception Error Message = " + ex.Message, MessageType.Informational);
            }
        }
        private void Clearfields()
        {
            try
            {
            txtAccountNumber.Text = null;
            txtStartEntryTime.Text = null;
            txtEndEntryTime.Text = null;
            txtEndExitTime.Text = null;
                //txtStoplossPercent.Text = null;
                txtAdditionalAccount.Text = null;
            txtStoplossRejectTimeInterval.Text = null;
                WriteUniquelogs("TradingSettingLogs", "Clearfields : All fields cleared.", MessageType.Informational);
            }
            catch (Exception ex)
            {
                WriteUniquelogs("TradingSettingLogs", "Clearfields : Exception Error Message = " + ex.Message, MessageType.Informational);
            }
        }

        private void btnOpenSymbolSetting_Click(object sender, EventArgs e)
        {
            try
            {
                SymbolSettingDet symbolObj = new SymbolSettingDet(this.AlgoOMSObj, txtAccountNumber.Text, null);
                symbolObj.ShowDialog();
            }
            catch (Exception ex)
            {
                WriteUniquelogs("TradingSettingLogs", "btnOpenSymbolSetting_Click : Exception Error Message = " + ex.Message, MessageType.Informational);
            }
        }


        private void AdditionalList_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                string[] totalAccount = null;
                string selectedvalue = AdditionalList.SelectedItem.ToString();
                if (txtAdditionalAccount.Text.Length > 0)
                {
                    totalAccount = txtAdditionalAccount.Text.Split(',');
                }
                if (txtAdditionalAccount.Text.Length > 0)
                {
                    int pos = Array.IndexOf(totalAccount, selectedvalue);
                    if (pos > -1)
                    {
                    }
                    else
                    {
                        txtAdditionalAccount.Text = txtAdditionalAccount.Text + "," + selectedvalue;
                    }
                }
                else
                {
                    txtAdditionalAccount.Text = AdditionalList.SelectedItem.ToString();
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("TradingSettingLogs", "AdditionalList_SelectedValueChanged : Exception Error Message = " + ex.Message, MessageType.Informational);
            }
        }
    }
}
