﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using IRDSAlgoOMS;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;


namespace RealStockA1Strategy
{
    //sanika::25-sep-2020::added for resume 
    [Serializable()]
    public class RealStockA1StrategyClass
    {
        static string path = Directory.GetCurrentDirectory();
        string m_TradingIniFilePath = path + @"\Configuration\";
        string m_SymbolListIniFilePath = path + @"\Configuration\";
        string FilePath = path + "\\Data\\ZerodhaData\\";
        //sanika::25-sep-2020::added for resume 
        [field: NonSerialized()]
        AlgoOMS AlgoOMS;
        //sanika::25-sep-2020::added for resume 
        [field: NonSerialized()]
        ReadSettings m_ReadSettings;
        //sanika::25-sep-2020::added for resume 
        [field: NonSerialized()]
        Logger logger;
        string userID;
        string EndTime = "";
        string FirstTickEnd = "";
        string StrategyStartTime = "";
        string End = "";
        //IRDS::Jyoti::16-07-2021::Added for restore changes
        List<TradingSymbolInfo> ListOfTradingSymbolsInfo = new List<TradingSymbolInfo>();
        //sanika::25-sep-2020::added for resume 
        [field: NonSerialized()]
        Thread automation = null;
        //sanika::25-sep-2020::added for resume 
        [field: NonSerialized()]
        StrategyForm form_Auto;
        public BasicOperation m_BasicOperation;
        string m_BinFileName = path + @"\RealStockA1";
        public bool m_isCloseAllOrder = false;
        public string m_AccountNumber = "";
        public string m_StartEntryTime = "";
        public string m_EndEntryTime = "";
        public string m_StartExitTime = "";
        public string m_EndExitTime = "";
        public int m_Interval = 1;
        public int m_NoOfLots = 0;
        public double m_CallStrikeValue = 0;
        public double m_PutStrikeValue = 0;
        public double m_StopLossPercent = 0;
        public bool m_BreakEvenStop = false;
        public double m_ReducePercentInLot = 0;
        public int m_StoplossRejectTimeInterval = 0;
        Dictionary<string, bool> m_OrderClosed = new Dictionary<string, bool>();
        string m_SymbolListINIFile = "";
        string m_TradingINIFile = "";
        bool m_IsOptionSymbolNameCreated = false;
        string m_brokerName = "";
        int LimitPriceIncrement = 6;
        int TriggerPriceIncrement = 1;
        int ModifyPriceIncrement = 2;


        public RealStockA1StrategyClass()
        {

        }
        public RealStockA1StrategyClass(AlgoOMS AlgoOMS, Logger logger, string userID, string tradingFilename, string brokerName)
        {

            this.AlgoOMS = AlgoOMS;
            this.userID = userID;
            this.logger = logger;
            m_TradingINIFile = tradingFilename;
            m_brokerName = brokerName;
            m_TradingIniFilePath += m_TradingINIFile;
            m_BasicOperation = new BasicOperation();
            LoadStructure(m_BasicOperation);
        }

        public void ResetStorage()
        {
            m_IsOptionSymbolNameCreated = false;
            m_BasicOperation.ClearOptionSymbols();
            AlgoOMS.ClearMasterList(userID);
            m_BasicOperation.ResetStorage();
            DeleteBinFile();
        }

        //To start Thread
        public void ExecuteSyncLogic()
        {
            bool isStopThread = false;
            //if (m_IsOptionSymbolNameCreated)
            loadINIValues();
            while (true)
            {
                try
                {
                    Thread.Sleep(100);
                    string TickCurrentTime = AlgoOMS.getTickCurrentTime();
                    //condition for TickCurrentTime 
                    if (TickCurrentTime == "")
                    {
                        logger.LogMessage("TickCurrentTime not recieved" + " " + userID, MessageType.Error);
                        continue;
                    }

                    //condition for stop automation day close condition i.e.3:30
                    if (TimeSpan.Parse(TickCurrentTime) >= TimeSpan.Parse(EndTime))
                    {
                        logger.LogMessage("ExecuteSyncLogic :Stopped because of endtime is " + EndTime + " " + userID, MessageType.Informational);
                        isStopThread = true;
                        form_Auto.enableButton();
                        StopThread();
                        break;
                    }

                    //condition for exit all orders after 3:29
                    if (TimeSpan.Parse(TickCurrentTime) >= TimeSpan.Parse(m_EndExitTime) || TimeSpan.Parse(DateTime.Now.ToString("HH:mm")) >= TimeSpan.Parse(m_EndExitTime))
                    {
                        logger.LogMessage("@@@@ In exit time condition TickCurrentTime " + TickCurrentTime + " m_EndExitTime " + m_EndExitTime, MessageType.Informational);
                        if (m_isCloseAllOrder == false)
                        {
                            logger.LogMessage("ExecuteSyncLogic : condition true for close all position" + " " + userID, MessageType.Informational);
                            CloseAllOrder();
                            AlgoOMS.AddNotificationInQueue(userID, "UserId : " + userID + " All orders closed due to exit time " + DateTime.Now.ToString("HH:mm:ss"));
                            m_isCloseAllOrder = true;
                            //IRDS::Jyoti::26-07-2021::Added for restore structure issue
                            //DeleteBinFile();
                            ResetStorage();
                        }
                    }
                    else
                    {
                        Trace.WriteLine("@@@@ In else TickCurrentTime " + TickCurrentTime + " m_EndExitTime " + m_EndExitTime);
                        RunSystemLoop(TickCurrentTime);
                    }
                }
                catch (Exception e)
                {
                    logger.LogMessage("ExecuteSyncLogic :Exception Error Message =  " + e.Message + " " + userID, MessageType.Exception);
                }
                finally
                {
                    if (isStopThread)
                    {
                        form_Auto.enableButton();
                    }
                }
            }
        }

        public bool CheckForOpenPosition(string TradingSymbol, string Exchange)
        {
            //checking position open or not                     
            if (!AlgoOMS.IsPositionOpen(userID, TradingSymbol, Exchange))
            {
                Thread.Sleep(1000);
                //wait for updating order from server to local array
                if (!AlgoOMS.IsPositionOpen(userID, TradingSymbol, Exchange))
                {
                    if (AlgoOMS.IsPendingStopOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_BUY) && (AlgoOMS.GetOpenPostionPricebyOrderID(userID, TradingSymbol, Exchange, m_BasicOperation.GetOpenOrderId(TradingSymbol)) == 0))
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "CheckForOpenPosition : buy pending order and sell is open", MessageType.Informational);
                    }
                    else
                    {
                        if (!AlgoOMS.IsPositionOpen(userID, TradingSymbol, Exchange) && AlgoOMS.GetOrderStatusByOrderID(userID, TradingSymbol, Exchange, m_BasicOperation.GetStopLossOrderId(TradingSymbol)) != Constants.ORDER_STATUS_PENDING)
                        {
                            WriteUniquelogs(TradingSymbol + " " + userID, "CheckForOpenPosition : StopLoss Hit", MessageType.Informational);
                        }
                    }
                    return false;
                }
                else
                {
                    WriteUniquelogs(TradingSymbol + " " + userID, "CheckForOpenPosition : Open position found", MessageType.Informational);
                }
            }
            return true;
        }

        void RunSystemLoop(string TickCurrentTime)
        {
            double quantity = 0;
            double openPrice = 0;
            string lOrderId = "";
            double LTP = 0;
            if (m_ReadSettings.m_IsNewSymbolAdded)
            {
                logger.LogMessage("RunSystemLoop : Get Setting.m_IsNewSymbolAdded = true so reading ini file", MessageType.Exception);
                readTradingSymbol(m_ReadSettings);
            }
            //time condition for placing order 9:20
            if ((TimeSpan.Parse(TickCurrentTime) >= TimeSpan.Parse(StrategyStartTime)))
            {
                if (!m_IsOptionSymbolNameCreated || m_ReadSettings.m_IsNewSymbolAdded)
                {
                    double margin = AlgoOMS.GetMargin(userID);
                    //loop for the trading symbol from ini file
                    foreach (var symbolInfo in ListOfTradingSymbolsInfo)
                    {
                        //Get Symbol Name and expiry date of symbol  and quantity  
                        string TradingSymbol = symbolInfo.getSymbol();
                        //IRDS::Jyoti::16-07-2021::Added for restore changes
                        Dictionary<string, string> originalSymbolNames = m_BasicOperation.GetOriginalSymbols();
                        if (originalSymbolNames.ContainsKey(TradingSymbol))
                        {
                            continue;
                        }
                        string Exchange = symbolInfo.getExchange();
                        string ExpiryDate = symbolInfo.getExpiryDate();
                        string ExpiryPeriod = symbolInfo.getExpiryPeriod();
                        int roundOffValue = Convert.ToInt32(symbolInfo.getRoundoff());
                        string orderType = symbolInfo.getOrderType();
                        //double marginLimit = symbolInfo.getLotSize();
                        double marginLimit = symbolInfo.getMarginLimit();
                        int putStrikePrice = Convert.ToInt32(symbolInfo.getStrikePricePut());
                        int callStrikePrice = Convert.ToInt32(symbolInfo.getStrikePriceCall());
                        quantity = symbolInfo.getLotSize();
                        double Stoploss = symbolInfo.getStoploss();
                        int noOfLotSize = Convert.ToInt32(symbolInfo.getTotalNoOfLots());
                        if (margin == 0 || ((marginLimit * noOfLotSize) * 2) < margin)
                        //if (((marginLimit * noOfLotSize) * 2) < margin)
                        {
                            logger.LogMessage("@@@@@ placeEntryOrder : Margin condition matched margin " + margin + " marginLimit " + marginLimit * noOfLotSize * 2, MessageType.Informational);
                        }
                        else
                        {
                            logger.LogMessage("@@@@@ placeEntryOrder : Margin condition not matched margin " + margin + " marginLimit " + marginLimit * noOfLotSize * 2, MessageType.Informational);
                            AlgoOMS.AddNotificationInQueue(userID, "UserId : " + userID + " Insufficient Margin to place " + TradingSymbol + " entry orders " + DateTime.Now.ToString("HH:mm:ss"));
                            AlgoOMS.AddNotificationInQueueForToast(userID, "Insufficient Margin to place " + TradingSymbol + " entry orders ");
                            continue;
                        }
                        logger.LogMessage("RunSystemLoop : Going to create option symbol name for " + TradingSymbol + " " + userID, MessageType.Informational);
                        if (!GetSymbolName(TradingSymbol, Exchange, ExpiryDate, ExpiryPeriod, roundOffValue, orderType, marginLimit, putStrikePrice, callStrikePrice, quantity, noOfLotSize, Stoploss))
                        {
                            continue;
                        }
                    }
                    m_IsOptionSymbolNameCreated = true;
                    m_ReadSettings.m_IsNewSymbolAdded = false;
                }

                Dictionary<string, string> optionSymbolNames = m_BasicOperation.GetOptionSymbols();
                if (optionSymbolNames != null && optionSymbolNames.Count > 0)
                {
                    foreach (var sym in optionSymbolNames)
                    {
                        //IRDS::Jyoti::16-07-2021::Added for restore changes
                        openPrice = 0;
                        string TradingSymbol = sym.Key;
                        string Exchange = sym.Value;
                        //get openprice from order id
                        lOrderId = m_BasicOperation.GetOpenOrderId(TradingSymbol);
                        if (lOrderId != "NA" && lOrderId != null)
                        {
                            openPrice = AlgoOMS.GetOpenPostionPricebyOrderID(userID, TradingSymbol, Exchange, lOrderId);
                        }
                        LTP = AlgoOMS.GetLastPrice(userID, TradingSymbol);
                        //time limit to place order i.e. 02:30
                        if ((TimeSpan.Parse(TickCurrentTime) <= TimeSpan.Parse(m_EndEntryTime)))
                        // if ((TimeSpan.Parse(TickCurrentTime) >= TimeSpan.Parse(FirstTickEnd)) && (TimeSpan.Parse(TickCurrentTime) < TimeSpan.Parse(End)) && (TimeSpan.Parse(TickCurrentTime) <= TimeSpan.Parse(m_EndEntryTime)))
                        {
                            //check open price =0 and no order with symbol name  
                            if (openPrice == 0 && (AlgoOMS.IsPendingLimitOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_SELL, m_BasicOperation.GetOrderType(TradingSymbol)) == false) && m_BasicOperation.GetCycleCompletionFlag(TradingSymbol) == false)
                            {
                                //20-July-2021: sandip: avoid multple times
                                WriteUniquelogs(TradingSymbol + " " + userID, "TickTime :" + TickCurrentTime, MessageType.Informational);
                                WriteUniquelogs(TradingSymbol + " " + userID, "Open price is 0 and no pending order", MessageType.Informational);
                                if (AlgoOMS.GetLimitPrice(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_SELL) == 0 && AlgoOMS.GetOrderIdFromMasterList(userID, TradingSymbol) == "NA")
                                {
                                    WriteUniquelogs(TradingSymbol + " " + userID, "Limit price fetched as 0", MessageType.Informational);
                                    //get trigger price and limit price
                                    double entryPrice = GetPrice(TradingSymbol);
                                    //get quantity
                                    //Quantity = m_NoOfLots;
                                    int iquantity = GetQuantity(TradingSymbol, Exchange);
                                    WriteUniquelogs(TradingSymbol + " " + userID, "entryPrice " + entryPrice + " Quantity " + iquantity + " quantity " + quantity, MessageType.Informational);
                                    //place 2 sell orders for pe and ce symbols
                                    placeEntryOrder(Exchange, TradingSymbol, iquantity, entryPrice);
                                }
                            }
                        }
                        if (openPrice != 0)
                        {
                            PlaceBuyOrder(TradingSymbol, Exchange, openPrice);
                            //check break even flag
                            if (m_BreakEvenStop)
                            {
                                //if true
                                //check ce or pe executed if ce executed modify pe price 
                                ModifyPairOrder(TradingSymbol, Exchange, openPrice);
                            }
                            CheckStopLossOrder(TradingSymbol, Exchange, LTP, openPrice);
                        }
                        CheckRejectedOrders(TradingSymbol, Exchange);
                        CheckPendingOrder(TradingSymbol, Exchange, m_BasicOperation.GetOrderType(TradingSymbol));
                        IsBothOrdersExecuted(TradingSymbol, Exchange, m_BasicOperation.GetOrderType(TradingSymbol));
                        //CheckForOpenPosition(TradingSymbol, Exchange);
                    }
                    changeTime(TickCurrentTime);

                }
            }
            SaveBinFile();
        }

        public void IsBothOrdersExecuted(string TradingSymbol, string Exchange, string product)
        {
            try
            {
                string PESymbol = m_BasicOperation.GetPESymbolName(TradingSymbol);
                string CESymbol = m_BasicOperation.GetCESymbolName(TradingSymbol);
                DateTime PESLOrderTime = m_BasicOperation.GetSLOrderTime(PESymbol);
                DateTime CESLOrderTime = m_BasicOperation.GetSLOrderTime(CESymbol);
                //WriteUniquelogs(TradingSymbol + " " + userID, "CheckPendingOrder : PESLOrderTime "+ PESLOrderTime + " CESLOrderTime "+ CESLOrderTime, MessageType.Informational);
                if ((!m_BasicOperation.IsMarketOrderPlaced(CESymbol)) && CESLOrderTime.Date != DateTime.Now.Date && PESLOrderTime.Date == DateTime.Now.Date && (!m_BasicOperation.GetIsStopLossOrderPlaced(CESymbol)) && Math.Abs(PESLOrderTime.Subtract(DateTime.Now).TotalMinutes) >= 2)
                {
                    WriteUniquelogs(TradingSymbol + " " + userID, "IsBothOrdersExecuted : ce order not executed yet placing market order for that symbol PESLOrderTime " + PESLOrderTime, MessageType.Informational);
                    if ((AlgoOMS.GetOrderStatusByOrderID(userID, CESymbol, Exchange, m_BasicOperation.GetOpenOrderId(CESymbol)) != "PARTIALLYFILLED") && AlgoOMS.GetOrderStatusByOrderID(userID, CESymbol, Exchange, m_BasicOperation.GetOpenOrderId(CESymbol)) != "COMPLETE")
                    {
                        if (AlgoOMS.CancelPendingLimitOrder(userID, CESymbol, Exchange, Constants.TRANSACTION_TYPE_SELL) || AlgoOMS.IsPendingLimitOrder(userID, CESymbol, Exchange, Constants.TRANSACTION_TYPE_SELL, product) == false)
                        {
                            if (m_BasicOperation.GetOrderCounter(TradingSymbol) < 4)
                            {
                                WriteUniquelogs(TradingSymbol + " " + userID, "IsBothOrdersExecuted : Counter check succeed Counter = " + m_BasicOperation.GetOrderCounter(TradingSymbol), MessageType.Informational);
                                string orderID = AlgoOMS.PlaceMarketOrder(userID, Exchange, CESymbol, Constants.TRANSACTION_TYPE_SELL, m_BasicOperation.GetQuantity(CESymbol), product);
                                if (orderID != "NA")
                                {
                                    m_BasicOperation.AddOrUpdateOrderId(CESymbol, orderID);
                                    m_BasicOperation.AddEntryPrice(CESymbol, AlgoOMS.GetOpenPostionPricebyOrderID(userID, CESymbol, Exchange, orderID));
                                    m_BasicOperation.AddOrUpdateOrderCounter(CESymbol);
                                    AlgoOMS.AddOrderIdInMasterList(userID, CESymbol, orderID);
                                    m_BasicOperation.AddOrUpdateEntryOrderTime(CESymbol, DateTime.Now);
                                    m_BasicOperation.AddOrUpdateMarketOrderPlaced(CESymbol, true);
                                }
                                m_BasicOperation.AddOrUpdateOrderCounter(TradingSymbol);
                            }
                            else
                            {
                                WriteUniquelogs(TradingSymbol + " " + userID, "IsBothOrdersExecuted : Counter check failed Counter = " + m_BasicOperation.GetOrderCounter(TradingSymbol), MessageType.Informational);
                            }
                        }//sanika::14-Jun-2021::added logs
                        else
                        {
                            WriteUniquelogs(TradingSymbol + " " + userID, "IsBothOrdersExecuted : ce order not able to cancel or found pending order", MessageType.Informational);
                        }
                    }//sanika::14-Jun-2021::added logs
                    else
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "IsBothOrdersExecuted : ce order status partially filled or complete", MessageType.Informational);
                    }
                }
                if ((!m_BasicOperation.IsMarketOrderPlaced(PESymbol)) && PESLOrderTime.Date != DateTime.Now.Date && CESLOrderTime.Date == DateTime.Now.Date && (!m_BasicOperation.GetIsStopLossOrderPlaced(PESymbol)) && Math.Abs(CESLOrderTime.Subtract(DateTime.Now).TotalMinutes) >= 2)
                {
                    WriteUniquelogs(TradingSymbol + " " + userID, "IsBothOrdersExecuted : pe order not executed yet placing market order for that symbol CESLOrderTime " + CESLOrderTime, MessageType.Informational);
                    if ((AlgoOMS.GetOrderStatusByOrderID(userID, PESymbol, Exchange, m_BasicOperation.GetOpenOrderId(PESymbol)) != "PARTIALLYFILLED") && (AlgoOMS.GetOrderStatusByOrderID(userID, PESymbol, Exchange, m_BasicOperation.GetOpenOrderId(PESymbol)) != "COMPLETE"))
                    {                                                                                                       //sanika::27-Jul-2021::changed transaction type from buy to sell because limit pending order is only sell
                        if (AlgoOMS.CancelPendingLimitOrder(userID, PESymbol, Exchange, Constants.TRANSACTION_TYPE_SELL) || AlgoOMS.IsPendingLimitOrder(userID, PESymbol, Exchange, Constants.TRANSACTION_TYPE_SELL, product) == false)
                        {                                                                                                       //sanika::29-July-2021::changed symbol name
                            if (m_BasicOperation.GetOrderCounter(TradingSymbol) < 4)
                            {
                                WriteUniquelogs(TradingSymbol + " " + userID, "IsBothOrdersExecuted : Counter check succeed Counter = " + m_BasicOperation.GetOrderCounter(TradingSymbol), MessageType.Informational);
                                string orderID = AlgoOMS.PlaceMarketOrder(userID, Exchange, PESymbol, Constants.TRANSACTION_TYPE_SELL, m_BasicOperation.GetQuantity(PESymbol), product);
                                if (orderID != "NA")
                                {
                                    m_BasicOperation.AddOrUpdateOrderId(PESymbol, orderID);
                                    m_BasicOperation.AddEntryPrice(PESymbol, AlgoOMS.GetOpenPostionPricebyOrderID(userID, PESymbol, Exchange, orderID));
                                    m_BasicOperation.AddOrUpdateOrderCounter(PESymbol);
                                    AlgoOMS.AddOrderIdInMasterList(userID, PESymbol, orderID);
                                    m_BasicOperation.AddOrUpdateEntryOrderTime(PESymbol, DateTime.Now);
                                    m_BasicOperation.AddOrUpdateMarketOrderPlaced(PESymbol, true);
                                }
                                m_BasicOperation.AddOrUpdateOrderCounter(TradingSymbol);
                            }
                            else
                            {
                                WriteUniquelogs(TradingSymbol + " " + userID, "IsBothOrdersExecuted : Counter check failed Counter = " + m_BasicOperation.GetOrderCounter(TradingSymbol), MessageType.Informational);
                            }
                        }//sanika::14-Jun-2021::added logs
                        else
                        {
                            WriteUniquelogs(TradingSymbol + " " + userID, "IsBothOrdersExecuted : pe order not able to cancel or found pending order", MessageType.Informational);
                        }
                    }//sanika::14-Jun-2021::added logs
                    else
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "IsBothOrdersExecuted : pe order status partially filled or complete", MessageType.Informational);
                    }
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + userID, "IsBothOrdersExecuted :Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }
        public void CheckPendingOrder(string TradingSymbol, string Exchange, string product)
        {
            try
            {
                DateTime placedTime = m_BasicOperation.GetEntryOrderTime(TradingSymbol);
                string PESymbol = m_BasicOperation.GetPESymbolName(TradingSymbol);
                string CESymbol = m_BasicOperation.GetCESymbolName(TradingSymbol);
                if (Math.Abs(placedTime.Subtract(DateTime.Now).TotalMinutes) >= 2 && (!m_BasicOperation.GetIsStopLossOrderPlaced(PESymbol) && !m_BasicOperation.GetIsStopLossOrderPlaced(CESymbol)))
                {
                    WriteUniquelogs(TradingSymbol + " " + userID, "CheckPendingOrder : 2 min exceeds for placing limit order placedTime " + placedTime, MessageType.Informational);

                    if (AlgoOMS.IsPendingLimitOrder(userID, PESymbol, Exchange, Constants.TRANSACTION_TYPE_SELL, product) == true)
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "CheckPendingOrder : order not executed = going to cancel " + PESymbol, MessageType.Informational);
                        AlgoOMS.CancelPendingLimitOrder(userID, PESymbol, Exchange, Constants.TRANSACTION_TYPE_SELL);
                    }
                    if (AlgoOMS.IsPendingLimitOrder(userID, CESymbol, Exchange, Constants.TRANSACTION_TYPE_SELL, product) == true)
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "CheckPendingOrder : order not executed = going to cancel " + CESymbol, MessageType.Informational);
                        AlgoOMS.CancelPendingLimitOrder(userID, CESymbol, Exchange, Constants.TRANSACTION_TYPE_SELL);
                    }
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + userID, "CheckPendingOrder : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        //get spot price according to symbol (if symbol is nifty then take nifty 50 price and if symbol is banknifty then take nifty bank price) 
        //round off spot price to 50
        //atm to spot price
        //create symbol name and fetch ask and bid price of that symbol
        //get ask and bid price and calculate entry price
        public double GetPrice(string TradingSymbol)
        {
            double entryPrice = 0;
            try
            {
                double ask = AlgoOMS.GetAskPrice(userID, TradingSymbol);
                if (ask == 0)
                {
                    double tmp = 0;
                    string Token = AlgoOMS.GetOptionsInstrumentToken(userID, TradingSymbol, "OPTIONS");
                    AlgoOMS.GetBidAndAskPrice(userID, long.Parse(Token), "NSEFO", out tmp, out ask);

                }
                double bid = AlgoOMS.GetBidPrice(userID, TradingSymbol);
                if (bid == 0)
                {
                    double tmp = 0;
                    string Token = AlgoOMS.GetOptionsInstrumentToken(userID, TradingSymbol, "OPTIONS");
                    AlgoOMS.GetBidAndAskPrice(userID, long.Parse(Token), "NSEFO", out bid, out tmp);
                }
                entryPrice = (ask + bid) / 2;
                WriteUniquelogs(TradingSymbol + " " + userID, "GetPrice : entryPrice " + entryPrice + " ask " + ask + " bid " + bid, MessageType.Informational);
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + userID, "GetPrice : Exception Error Message = " + e.Message, MessageType.Exception);
            }

            return entryPrice;
        }

        //check lot size symbol wise - if nifty then it should be multiplier of 75 and for banknifty 25
        public bool VerifyQuantity(string TradingSymbol, int Quantity)
        {
            try
            {
                if (TradingSymbol.Length > 5)
                {
                    if (Quantity % 25 == 0)
                        return true;
                }
                else
                {
                    if (Quantity % 75 == 0)
                        return true;
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + userID, "VerifyQuantity : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return false;
        }

        //place 2 order with entry order for ce and pe
        //if order rejected - get reduce percent and reduced from lot size and try to place again 
        //again rejected - try till lot size 0
        //store orderid
        public void placeEntryOrder(string Exchange, string TradingSymbol, int Quantity, double EntryPrice)
        {
            try
            {
                if (m_BasicOperation.GetOrderCounter(TradingSymbol) < 1)
                {
                    //double marginLimit = m_BasicOperation.GetMarginLimit(TradingSymbol);
                    //double margin = AlgoOMS.GetMargin(userID);
                    //if (((marginLimit * Quantity)* 2) < margin)
                    //{
                    //WriteUniquelogs(TradingSymbol + " " + userID, "@@@@@ placeEntryOrder : Margin condition matched margin " + margin + " marginLimit " + marginLimit * Quantity * 2, MessageType.Informational);
                    WriteUniquelogs(TradingSymbol + " " + userID, "placeEntryOrder : Counter check succeed Counter = " + m_BasicOperation.GetOrderCounter(TradingSymbol), MessageType.Informational);
                    string orderId = "NA";
                    double price = AlgoOMS.RoundOff(userID, TradingSymbol, Convert.ToDecimal(EntryPrice), false);
                    EntryPrice = price;
                    WriteUniquelogs(TradingSymbol + " " + userID, "@@@@@ placeEntryOrder : TradingSymbol " + TradingSymbol + " Exchange " + Exchange + " Quantity " + Quantity + " EntryPrice " + EntryPrice, MessageType.Informational);
                    orderId = AlgoOMS.PlaceLimitOrder(userID, Exchange, TradingSymbol, Constants.TRANSACTION_TYPE_SELL, Quantity, Convert.ToDecimal(EntryPrice), m_BasicOperation.GetOrderType(TradingSymbol));
                    string status = AlgoOMS.GetOrderStatusByOrderID(userID, TradingSymbol, Exchange, orderId);
                    WriteUniquelogs(TradingSymbol + " " + userID, "@@@@@ placeEntryOrder : orderId " + orderId + " status " + status, MessageType.Informational);
                    if (status == Constants.ORDER_STATUS_REJECTED)
                    {
                        CheckForClosingOrder(TradingSymbol, Exchange);
                    }
                    if (orderId != "NA")
                    {
                        AlgoOMS.AddNotificationInQueue(userID, "UserId : " + userID + " Placed Entry Order " + DateTime.Now.ToString("HH:mm:ss"));
                        WriteUniquelogs(TradingSymbol + " " + userID, "@@@@@ placeEntryOrder : Going to store order id in storage orderId " + orderId, MessageType.Informational);
                        m_BasicOperation.AddOrUpdateOrderId(TradingSymbol, orderId);
                        m_BasicOperation.AddOrUpdateQuantity(TradingSymbol, Quantity);
                        m_BasicOperation.AddEntryPrice(TradingSymbol, EntryPrice);
                        AlgoOMS.AddOrderIdInMasterList(userID, TradingSymbol, orderId);
                        m_BasicOperation.AddOrUpdateEntryOrderTime(TradingSymbol, DateTime.Now);
                    }
                    //20-July-2021: sandip://to avoid any duplicate order
                    m_BasicOperation.AddOrUpdateOrderCounter(TradingSymbol);
                    //}
                    //else
                    //{
                    //    WriteUniquelogs(TradingSymbol + " " + userID, "@@@@@ placeEntryOrder : Margin condition not matched margin "+ margin + " marginLimit "+marginLimit*Quantity*2, MessageType.Informational);
                    //}
                }
                else
                {
                    WriteUniquelogs(TradingSymbol + " " + userID, "@@@@@ placeEntryOrder : Counter condition not matched", MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + userID, "@@@@@ placeEntryOrder : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }


        //check order executed and check buy order for same symbol should not be present 
        //calculate trigger price and limit price on open price of executed order by stoploss percent 
        //for trigger price +1 and limit price +3
        //then place buy order for same symbol 

        public void PlaceBuyOrder(string TradingSymbol, string Exchange, double openPrice)
        {
            try
            {
                if (openPrice != 0 && (!m_BasicOperation.GetIsStopLossOrderPlaced(TradingSymbol)))
                {
                    if (m_BasicOperation.GetOrderCounter(TradingSymbol) < 4)
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "PlaceBuyOrder : Counter check succeed Counter = " + m_BasicOperation.GetOrderCounter(TradingSymbol), MessageType.Informational);
                        double entryPrice = m_BasicOperation.GetEntryOrderPrice(TradingSymbol);
                        if (entryPrice == 0)
                        {
                            entryPrice = openPrice;
                            WriteUniquelogs(TradingSymbol + " " + userID, "##### PlaceBuyOrder : entry price fetched as 0 so open price intialized to entry price " + entryPrice + " open price " + openPrice, MessageType.Informational);
                        }
                        m_StopLossPercent = m_BasicOperation.GetStopLoss(TradingSymbol);
                        double PercentValue = m_StopLossPercent * entryPrice / 100;
                        double CalPrice = openPrice + PercentValue;
                        double tprice = AlgoOMS.RoundOff(userID, TradingSymbol, Convert.ToDecimal(CalPrice), false);
                        double triggerPrice = tprice + TriggerPriceIncrement;
                        double limitPrice = tprice + LimitPriceIncrement;
                        int Quantity = m_BasicOperation.GetQuantity(TradingSymbol);
                        WriteUniquelogs(TradingSymbol + " " + userID, "##### PlaceBuyOrder : openPrice = " + openPrice + " entryPrice = " + entryPrice + " PercentValue " + PercentValue + " CalPrice " + CalPrice + " tprice " + tprice + " triggerPrice " + triggerPrice + " limitPrice " + limitPrice + " Quantity " + Quantity, MessageType.Informational);
                        string orderId = AlgoOMS.PlaceOrder(userID, Exchange, TradingSymbol, Constants.TRANSACTION_TYPE_BUY, Quantity, "STOPLIMIT", Convert.ToDecimal(limitPrice), m_BasicOperation.GetOrderType(TradingSymbol), TriggerPrice: Convert.ToDecimal(triggerPrice));
                        if (orderId != "NA")
                        {
                            AlgoOMS.AddNotificationInQueue(userID, "UserId : " + userID + " Entry order executed. placed sl order " + DateTime.Now.ToString("HH:mm:ss"));
                            m_BasicOperation.AddOrUpdateStopLossOrderId(TradingSymbol, orderId);
                            m_BasicOperation.AddOrUpdateIsStopLossOrderPlaced(TradingSymbol, true);
                            m_BasicOperation.AddOrUpdateSLOrderTime(TradingSymbol, DateTime.Now);
                            m_BasicOperation.AddOrUpdateTime(TradingSymbol, DateTime.Now);
                            WriteUniquelogs(TradingSymbol + " " + userID, "##### PlaceBuyOrder : Placed successfully orderid " + orderId, MessageType.Informational);
                        }
                        m_BasicOperation.AddOrUpdateOrderCounter(TradingSymbol);
                    }
                    else
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "PlaceBuyOrder : Counter check failed Counter = " + m_BasicOperation.GetOrderCounter(TradingSymbol), MessageType.Informational);
                    }
                }

            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + userID, "##### PlaceBuyOrder : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        //check ce or pe executed if ce executed modify pe price 
        //if ce executed
        //modify trigger and limit price of pe
        //if pe executed
        //modify trigger and limit price of ce
        //price ???
        public void ModifyPairOrder(string TradingSymbol, string Exchange, double openPrice)
        {
            try
            {
                if (!m_BasicOperation.GetIsOrderModified(TradingSymbol))
                {
                    if (TradingSymbol.EndsWith("CE"))
                    {
                        string symbolName = m_BasicOperation.GetPESymbolName(TradingSymbol);
                        //sanika::22-Jul-2021::assigned open price of pe symbol
                        openPrice = AlgoOMS.GetOpenPostionPricebyOrderID(userID, symbolName, Exchange, m_BasicOperation.GetOpenOrderId(symbolName));
                        //modify PE order
                        if (AlgoOMS.GetOpenPostionPricebyOrderID(userID, TradingSymbol, Exchange, m_BasicOperation.GetOpenOrderId(TradingSymbol)) != 0 && AlgoOMS.GetOpenPostionPricebyOrderID(userID, TradingSymbol, Exchange, m_BasicOperation.GetStopLossOrderId(TradingSymbol)) != 0)
                        {
                            if (AlgoOMS.GetOpenPostionPricebyOrderID(userID, symbolName, Exchange, m_BasicOperation.GetOpenOrderId(symbolName)) != 0 && AlgoOMS.GetOpenPostionPricebyOrderID(userID, symbolName, Exchange, m_BasicOperation.GetStopLossOrderId(symbolName)) == 0)
                            {
                                double entryPrice = m_BasicOperation.GetEntryOrderPrice(symbolName);
                                if (entryPrice == 0)
                                {
                                    entryPrice = openPrice;
                                    WriteUniquelogs(TradingSymbol + " " + userID, "***** ModifyPairOrder :entry price fetched as 0 so assigned open price " + openPrice, MessageType.Informational);
                                }
                                string orderId = m_BasicOperation.GetStopLossOrderId(symbolName);
                                int quantity = m_BasicOperation.GetQuantity(symbolName);
                                double limitPrice = entryPrice + ModifyPriceIncrement;
                                WriteUniquelogs(TradingSymbol + " " + userID, "***** ModifyPairOrder : symbolName " + symbolName + " entryPrice " + entryPrice + " orderId " + orderId + " quantity " + quantity, MessageType.Informational);
                                if (AlgoOMS.ModifySLOrderWithOrderId(userID, symbolName, Constants.TRANSACTION_TYPE_BUY, Convert.ToDecimal(limitPrice), Convert.ToDecimal(entryPrice), orderId, quantity, Exchange, m_BasicOperation.GetOrderType(TradingSymbol)))
                                {
                                    WriteUniquelogs(TradingSymbol + " " + userID, "***** ModifyPairOrder : Order modified successfully", MessageType.Informational);
                                    m_BasicOperation.AddOrUpdateIsOrderModified(TradingSymbol, true);
                                }
                                else
                                {
                                    //sanika::29-Jul-2021::added check for cancel order
                                    if (AlgoOMS.CancelOrder(userID, orderId, symbolName, m_BasicOperation.GetOrderType(TradingSymbol)))
                                    {
                                        if (m_BasicOperation.GetOrderCounter(TradingSymbol) < 5)
                                        {
                                            WriteUniquelogs(TradingSymbol + " " + userID, "ModifyPairOrder : Counter check succeed Counter = " + m_BasicOperation.GetOrderCounter(TradingSymbol), MessageType.Informational);
                                            WriteUniquelogs(TradingSymbol + " " + userID, "***** ModifyPairOrder : Placing market order as not able to modify", MessageType.Informational);
                                            orderId = AlgoOMS.PlaceMarketOrder(userID, Exchange, symbolName, Constants.TRANSACTION_TYPE_BUY, quantity, m_BasicOperation.GetOrderType(TradingSymbol));
                                            if (orderId != "NA")
                                            {
                                                m_BasicOperation.AddOrUpdateIsOrderModified(TradingSymbol, true);
                                                WriteUniquelogs(TradingSymbol + " " + userID, "***** ModifyPairOrder : Market order placed successfully orderId " + orderId, MessageType.Informational);
                                            }
                                            m_BasicOperation.AddOrUpdateOrderCounter(TradingSymbol);
                                        }
                                        else
                                        {
                                            WriteUniquelogs(TradingSymbol + " " + userID, "ModifyPairOrder : Counter check failed Counter = " + m_BasicOperation.GetOrderCounter(TradingSymbol), MessageType.Informational);
                                        }
                                    }
                                    else
                                    {
                                        WriteUniquelogs(TradingSymbol + " " + userID, "***** ModifyPairOrder : Market order not placed because order not able to cancel " + orderId, MessageType.Informational);
                                    }
                                }

                            }
                            else
                            {
                                if (AlgoOMS.IsPendingLimitOrder(userID, symbolName, Exchange, Constants.TRANSACTION_TYPE_SELL, m_BasicOperation.GetOrderType(TradingSymbol)))
                                {
                                    WriteUniquelogs(TradingSymbol + " " + userID, "***** ModifyPairOrder : Open order not found for symbol Going to cancel pending order " + symbolName, MessageType.Informational);
                                    AlgoOMS.CancelOrder(userID, m_BasicOperation.GetOpenOrderId(symbolName), symbolName, m_BasicOperation.GetOrderType(TradingSymbol));
                                }

                            }
                        }
                        //else
                        //{
                        //    WriteUniquelogs(TradingSymbol + " " + userID, "***** ModifyPairOrder : Stoploss not hit yet for " + TradingSymbol, MessageType.Informational);
                        //}
                    }
                    else if (TradingSymbol.EndsWith("PE"))
                    {
                        string symbolName = m_BasicOperation.GetCESymbolName(TradingSymbol);
                        //sanika::22-Jul-2021::assigned open price of ce symbol
                        openPrice = AlgoOMS.GetOpenPostionPricebyOrderID(userID, symbolName, Exchange, m_BasicOperation.GetOpenOrderId(symbolName));
                        //modify CE order
                        if (AlgoOMS.GetOpenPostionPricebyOrderID(userID, TradingSymbol, Exchange, m_BasicOperation.GetOpenOrderId(TradingSymbol)) != 0 && AlgoOMS.GetOpenPostionPricebyOrderID(userID, TradingSymbol, Exchange, m_BasicOperation.GetStopLossOrderId(TradingSymbol)) != 0)
                        {
                            if (AlgoOMS.GetOpenPostionPricebyOrderID(userID, symbolName, Exchange, m_BasicOperation.GetOpenOrderId(symbolName)) != 0 && AlgoOMS.GetOpenPostionPricebyOrderID(userID, symbolName, Exchange, m_BasicOperation.GetStopLossOrderId(symbolName)) == 0)
                            {
                                double entryPrice = m_BasicOperation.GetEntryOrderPrice(symbolName);
                                if (entryPrice == 0)
                                {
                                    entryPrice = openPrice;
                                    WriteUniquelogs(TradingSymbol + " " + userID, "***** ModifyPairOrder :entry price fetched as 0 so assigned open price " + openPrice, MessageType.Informational);
                                }
                                string orderId = m_BasicOperation.GetStopLossOrderId(symbolName);
                                int quantity = m_BasicOperation.GetQuantity(symbolName);
                                double limitPrice = entryPrice + ModifyPriceIncrement;
                                WriteUniquelogs(TradingSymbol + " " + userID, "***** ModifyPairOrder : symbolName " + symbolName + " entryPrice " + entryPrice + " orderId " + orderId + " quantity " + quantity, MessageType.Informational);
                                if (AlgoOMS.ModifySLOrderWithOrderId(userID, symbolName, Constants.TRANSACTION_TYPE_BUY, Convert.ToDecimal(limitPrice), Convert.ToDecimal(entryPrice), orderId, quantity, Exchange, m_BasicOperation.GetOrderType(symbolName)))
                                {
                                    m_BasicOperation.AddOrUpdateIsOrderModified(TradingSymbol, true);
                                    WriteUniquelogs(TradingSymbol + " " + userID, "***** ModifyPairOrder : Order modified successfully", MessageType.Informational);
                                }
                                else
                                {
                                    //sanika::29-Jul-2021::added check for cancel order
                                    if (AlgoOMS.CancelOrder(userID, orderId, symbolName, m_BasicOperation.GetOrderType(TradingSymbol)))
                                    {
                                        if (m_BasicOperation.GetOrderCounter(TradingSymbol) < 5)
                                        {
                                            WriteUniquelogs(TradingSymbol + " " + userID, "ModifyPairOrder : Counter check succeed Counter = " + m_BasicOperation.GetOrderCounter(TradingSymbol), MessageType.Informational);
                                            WriteUniquelogs(TradingSymbol + " " + userID, "***** ModifyPairOrder : Placing market order as not able to modify", MessageType.Informational);
                                            orderId = AlgoOMS.PlaceMarketOrder(userID, Exchange, symbolName, Constants.TRANSACTION_TYPE_BUY, quantity, m_BasicOperation.GetOrderType(TradingSymbol));
                                            if (orderId != "NA")
                                            {
                                                m_BasicOperation.AddOrUpdateIsOrderModified(TradingSymbol, true);
                                                WriteUniquelogs(TradingSymbol + " " + userID, "***** ModifyPairOrder : Market order placed successfully orderId " + orderId, MessageType.Informational);
                                            }
                                            m_BasicOperation.AddOrUpdateOrderCounter(TradingSymbol);
                                        }
                                        else
                                        {
                                            WriteUniquelogs(TradingSymbol + " " + userID, "ModifyPairOrder : Counter check failed Counter = " + m_BasicOperation.GetOrderCounter(TradingSymbol), MessageType.Informational);
                                        }
                                    }
                                    else
                                    {
                                        WriteUniquelogs(TradingSymbol + " " + userID, "***** ModifyPairOrder : Market order not placed because order not able to cancel " + orderId, MessageType.Informational);
                                    }
                                }
                            }
                            else
                            {
                                if (AlgoOMS.IsPendingLimitOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_SELL, m_BasicOperation.GetOrderType(TradingSymbol)))
                                {
                                    WriteUniquelogs(TradingSymbol + " " + userID, "***** ModifyPairOrder : Open order not found for symbol Going to cancel pending order " + symbolName, MessageType.Informational);
                                    AlgoOMS.CancelOrder(userID, m_BasicOperation.GetOpenOrderId(symbolName), symbolName, m_BasicOperation.GetOrderType(TradingSymbol));
                                }
                            }
                        }
                        //else
                        //{
                        //    WriteUniquelogs(TradingSymbol + " " + userID, "***** ModifyPairOrder : Stoploss not hit yet for " + TradingSymbol, MessageType.Informational);
                        //}
                    }
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + userID, "***** ModifyPairOrder : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        //check ltp > stop loss price
        //check time interval e.g. 2 min
        //check order still open then close order 

        public void CheckStopLossOrder(string TradingSymbol, string Exchange, double LTP, double openPrice)
        {
            try
            {
                string orderId = m_BasicOperation.GetStopLossOrderId(TradingSymbol);
                double price = AlgoOMS.GetStopPricebyOrderID(userID, TradingSymbol, Exchange, orderId);
                if (price != 0)
                {
                    if (LTP > price)
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, ">>>>> CheckStopLossOrder : orderId " + orderId + " price " + price + " LTP " + LTP, MessageType.Informational);
                        DateTime lastCheckedTime = m_BasicOperation.GetTime(TradingSymbol);
                        if (!(lastCheckedTime.Date < DateTime.Now.Date))
                        {
                            WriteUniquelogs(TradingSymbol + " " + userID, ">>>>> CheckStopLossOrder : Date condition satisfied m_StoplossRejectTimeInterval = " + m_StoplossRejectTimeInterval + " lastCheckedTime = " + lastCheckedTime, MessageType.Informational);
                            //IRDS::Jyoti::27-Jul-21::Added changes to avoid duplicate order
                            if (Math.Abs(lastCheckedTime.Subtract(DateTime.Now).TotalMinutes) >= m_StoplossRejectTimeInterval)
                            {
                                if (AlgoOMS.IsPositionOpen(userID, TradingSymbol, Exchange))
                                {
                                    Thread.Sleep(200);
                                    if (AlgoOMS.GetOpenPostionPricebyOrderID(userID, TradingSymbol, Exchange, m_BasicOperation.GetOpenOrderId(TradingSymbol)) != 0)
                                    {
                                        WriteUniquelogs(TradingSymbol + " " + userID, ">>>>> CheckStopLossOrder : Going to close order", MessageType.Informational);
                                        if (m_BasicOperation.GetOrderCounter(TradingSymbol) < 6)
                                        {
                                            WriteUniquelogs(TradingSymbol + " " + userID, "CheckStopLossOrder : Counter check succeed Counter = " + m_BasicOperation.GetOrderCounter(TradingSymbol), MessageType.Informational);
                                            AlgoOMS.CloseOrder(userID, TradingSymbol, Exchange);
                                            m_BasicOperation.AddOrUpdateOrderCounter(TradingSymbol);
                                        }
                                        else
                                        {
                                            WriteUniquelogs(TradingSymbol + " " + userID, "CheckStopLossOrder : Counter check failed Counter = " + m_BasicOperation.GetOrderCounter(TradingSymbol), MessageType.Informational);
                                        }
                                    }
                                    else
                                    {
                                        WriteUniquelogs(TradingSymbol + " " + userID, ">>>>> CheckStopLossOrder : Position already closed", MessageType.Informational);
                                    }
                                }
                                else
                                {
                                    WriteUniquelogs(TradingSymbol + " " + userID, ">>>>> CheckStopLossOrder : Position already closed", MessageType.Informational);
                                }
                                m_BasicOperation.AddOrUpdateTime(TradingSymbol, DateTime.Now);
                            }
                        }
                        else
                        {
                            WriteUniquelogs(TradingSymbol + " " + userID, ">>>>> CheckStopLossOrder : Date condition not satisfied", MessageType.Informational);
                        }
                    }
                }
                else
                {
                    //IRDS::Jyoti::22-Jul-21::Added for handling Rejected sl order
                    if (openPrice != 0 && LTP > price && (AlgoOMS.GetOrderStatusByOrderID(userID, TradingSymbol, Exchange, orderId) == Constants.ORDER_STATUS_REJECTED))
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, ">>>>> CheckStopLossOrder : orderId " + orderId + " price " + price + " LTP " + LTP, MessageType.Informational);
                        DateTime lastCheckedTime = m_BasicOperation.GetTime(TradingSymbol);
                        if (!(lastCheckedTime.Date < DateTime.Now.Date))
                        {
                            WriteUniquelogs(TradingSymbol + " " + userID, ">>>>> CheckStopLossOrder : Date condition satisfied", MessageType.Informational);
                            //IRDS::Jyoti::27-Jul-21::Added changes to avoid duplicate order
                            if (Math.Abs(lastCheckedTime.Subtract(DateTime.Now).TotalMinutes) >= m_StoplossRejectTimeInterval)
                            {
                                if (AlgoOMS.IsPositionOpen(userID, TradingSymbol, Exchange))
                                {
                                    Thread.Sleep(200);
                                    if (AlgoOMS.GetOpenPostionPricebyOrderID(userID, TradingSymbol, Exchange, m_BasicOperation.GetOpenOrderId(TradingSymbol)) != 0)
                                    {
                                        WriteUniquelogs(TradingSymbol + " " + userID, ">>>>> CheckStopLossOrder : Going to close order", MessageType.Informational);
                                        if (m_BasicOperation.GetOrderCounter(TradingSymbol) < 6)
                                        {
                                            WriteUniquelogs(TradingSymbol + " " + userID, "CheckStopLossOrder : Counter check succeed Counter = " + m_BasicOperation.GetOrderCounter(TradingSymbol), MessageType.Informational);
                                            AlgoOMS.CloseOrder(userID, TradingSymbol, Exchange);
                                            m_BasicOperation.AddOrUpdateOrderCounter(TradingSymbol);
                                        }
                                        else
                                        {
                                            WriteUniquelogs(TradingSymbol + " " + userID, "CheckStopLossOrder : Counter check failed Counter = " + m_BasicOperation.GetOrderCounter(TradingSymbol), MessageType.Informational);
                                        }
                                    }
                                    else
                                    {
                                        WriteUniquelogs(TradingSymbol + " " + userID, ">>>>> CheckStopLossOrder : Position already closed", MessageType.Informational);
                                    }
                                }
                                else
                                {
                                    WriteUniquelogs(TradingSymbol + " " + userID, ">>>>> CheckStopLossOrder : Position already closed", MessageType.Informational);
                                }
                                m_BasicOperation.AddOrUpdateTime(TradingSymbol, DateTime.Now);
                            }
                        }
                        else
                        {
                            WriteUniquelogs(TradingSymbol + " " + userID, ">>>>> CheckStopLossOrder : Date condition not satisfied", MessageType.Informational);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + userID, ">>>>> CheckStopLossOrder : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        public bool GetSymbolName(string TradingSymbol, string Exchange, string ExpiryDate, string ExpiryPeriod, int RoundOffValue, string orderType, double marginLimit, int putStrikePrice, int callStrikePrice, double quantity, int noOfLotsize, double stoploss)
        {
            logger.LogMessage("GetSymbolName : TradingSymbol " + TradingSymbol + " Exchange " + Exchange + " ExpiryDate " + ExpiryDate + " " + userID, MessageType.Informational);
            bool isSymbolFound = false;
            try
            {
                double LTP = 0;
                if (TradingSymbol.Length > 5)
                {
                    logger.LogMessage("GetSymbolName : Getting LTP of NIFTY BANK" + " " + userID, MessageType.Informational);
                    LTP = AlgoOMS.GetLastPrice(userID, "NIFTY BANK");
                    if (ExpiryPeriod == "Weekly")
                    {
                        if (CheckWeeklyExpiry(TradingSymbol, Exchange, ExpiryDate, LTP, RoundOffValue, orderType, marginLimit, putStrikePrice, callStrikePrice, quantity, noOfLotsize, stoploss))
                        {
                            logger.LogMessage("GetSymbolName : Fetched weekly expiry symbol" + " " + userID, MessageType.Informational);
                            isSymbolFound = true;
                        }
                        else
                        {
                            AlgoOMS.AddGUINotification(userID, "Not able to create weekly expiration symbol name");
                        }
                        //else if (CheckMonthlyExpiry(TradingSymbol, Exchange, ExpiryDate, LTP,orderType, marginLimit, RoundOffValue,putStrikePrice,callStrikePrice,quantity,noOfLotsize, stoploss))
                        //{
                        //    logger.LogMessage("GetSymbolName : Fetched monthly expiry symbol" + " " + userID, MessageType.Informational);
                        //    isSymbolFound = true;
                        //}
                    }
                    else
                    {
                        if (CheckMonthlyExpiry(TradingSymbol, Exchange, ExpiryDate, LTP, orderType, marginLimit, RoundOffValue, putStrikePrice, callStrikePrice, quantity, noOfLotsize, stoploss))
                        {
                            logger.LogMessage("GetSymbolName : Fetched monthly expiry symbol" + " " + userID, MessageType.Informational);
                            isSymbolFound = true;
                        }
                    }
                }
                else
                {
                    logger.LogMessage("GetSymbolName : Getting LTP of NIFTY 50" + " " + userID, MessageType.Informational);
                    LTP = AlgoOMS.GetLastPrice(userID, "NIFTY 50");
                    if (ExpiryPeriod == "Weekly")
                    {
                        if (CheckWeeklyExpiry(TradingSymbol, Exchange, ExpiryDate, LTP, RoundOffValue, orderType, marginLimit, putStrikePrice, callStrikePrice, quantity, noOfLotsize, stoploss))
                        {
                            logger.LogMessage("GetSymbolName : Fetched weekly expiry symbol" + " " + userID, MessageType.Informational);
                            isSymbolFound = true;
                        }
                        else if (CheckMonthlyExpiry(TradingSymbol, Exchange, ExpiryDate, LTP, orderType, marginLimit, RoundOffValue, putStrikePrice, callStrikePrice, quantity, noOfLotsize, stoploss))
                        {
                            logger.LogMessage("GetSymbolName : Fetched monthly expiry symbol" + " " + userID, MessageType.Informational);
                            isSymbolFound = true;
                        }
                    }
                    else
                    {
                        if (CheckMonthlyExpiry(TradingSymbol, Exchange, ExpiryDate, LTP, orderType, marginLimit, RoundOffValue, putStrikePrice, callStrikePrice, quantity, noOfLotsize, stoploss))
                        {
                            logger.LogMessage("GetSymbolName : Fetched monthly expiry symbol " + userID, MessageType.Informational);
                            isSymbolFound = true;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                logger.LogMessage("GetSymbolName : Exception Error Message = " + e.Message + " " + userID, MessageType.Exception);
            }
            return isSymbolFound;
        }

        public string GetWeeklyOptionSymbolName(string TradingSymbol, string ExpiryDate, double LTP, string OptionType, int roundOffValue, int StrikePrice)
        {
            string newSymbolName = "NA";
            try
            {
                logger.LogMessage("GetWeeklyOptionSymbolName : TradingSymbol " + TradingSymbol + " ExpiryDate " + ExpiryDate + " LTP " + LTP + " OptionType " + OptionType + " StrikePrice " + StrikePrice + " " + userID, MessageType.Informational);
                int RoundedLTP = Convert.ToInt32(Math.Round(LTP / roundOffValue, 0) * roundOffValue);
                string year = ExpiryDate.Substring(ExpiryDate.Length - 2);
                string month = ExpiryDate.Substring(ExpiryDate.Length - 5, 3).ToUpper();
                int monthInInt = DateTime.ParseExact(month, "MMM", CultureInfo.CurrentCulture).Month;
                string Date = ExpiryDate.Substring(0, 2);
                double Price = 0;
                int finalStrikePrice = StrikePrice * roundOffValue;
                Price = Math.Abs(RoundedLTP + finalStrikePrice);
                newSymbolName = TradingSymbol + year + monthInInt + Date + Price + OptionType;
                logger.LogMessage("GetWeeklyOptionSymbolName : RoundedLTP " + RoundedLTP + " newSymbolName " + newSymbolName + " finalStrikePrice " + finalStrikePrice + " StrikePrice " + StrikePrice + " Price " + Price + " " + userID, MessageType.Informational);
            }
            catch (Exception e)
            {
                logger.LogMessage("GetWeeklyOptionSymbolName : Exception Error Message = " + e.Message + " " + userID, MessageType.Exception);
            }
            return newSymbolName;
        }

        public string GetMonthlyOptionSymbolName(string TradingSymbol, string ExpiryDate, double LTP, string OptionType, int Roundoff, int StrikePrice)
        {
            string newSymbolName = "NA";
            try
            {
                //logger.LogMessage("GetMonthlyOptionSymbolName : TradingSymbol " + TradingSymbol + " ExpiryDate " + ExpiryDate + " LTP " + LTP + " OptionType " + OptionType + " StrikePrice " + StrikePrice, MessageType.Informational);
                //double RoundedLTP = Math.Round(LTP / Roundoff, 0) * Roundoff;
                //string year = ExpiryDate.Substring(ExpiryDate.Length - 2);
                //string month = ExpiryDate.Substring(ExpiryDate.Length - 5, 3).ToUpper();
                //double Price = 0;
                //StrikePrice *= RoundedLTP;
                //Price = RoundedLTP + StrikePrice;
                //newSymbolName = TradingSymbol + year + month + Price + OptionType;
                //logger.LogMessage("GetMonthlyOptionSymbolName : RoundedLTP " + RoundedLTP + " newSymbolName " + newSymbolName, MessageType.Informational);

                logger.LogMessage("GetMonthlyOptionSymbolName : TradingSymbol " + TradingSymbol + " ExpiryDate " + ExpiryDate + " LTP " + LTP + " OptionType " + OptionType + " StrikePrice " + StrikePrice + " " + userID, MessageType.Informational);
                int RoundedLTP = Convert.ToInt32(Math.Round(LTP / Roundoff, 0) * Roundoff);
                string year = ExpiryDate.Substring(ExpiryDate.Length - 2);
                string month = ExpiryDate.Substring(ExpiryDate.Length - 5, 3).ToUpper();
                double Price = 0;
                int finalStrikePrice = StrikePrice * Roundoff;
                Price = Math.Abs(RoundedLTP + finalStrikePrice);
                newSymbolName = TradingSymbol + year + month + Price + OptionType;
                logger.LogMessage("GetMonthlyOptionSymbolName : RoundedLTP " + RoundedLTP + " newSymbolName " + newSymbolName + " " + userID, MessageType.Informational);
            }
            catch (Exception e)
            {
                logger.LogMessage("GetMonthlyOptionSymbolName : Exception Error Message = " + e.Message + " " + userID, MessageType.Exception);
            }
            return newSymbolName;
        }

        public void StoreSymbolNameInStorage(string Exchange, string CESymbol, string PESymbol, string OrderType, double MarginLimit, double quantity, int noOfLotSize, double stoploss)
        {
            try
            {
                List<string> symbolNames = new List<string>();
                symbolNames.Add(CESymbol + "." + Constants.EXCHANGE_NFO_OPT);
                m_BasicOperation.AddOrUpdateOptionSymbols(CESymbol, Exchange);
                m_BasicOperation.AddOrUpdateSymbolName(CESymbol, CESymbol);
                symbolNames.Add(PESymbol + "." + Constants.EXCHANGE_NFO_OPT);
                m_BasicOperation.AddOrUpdateOptionSymbols(PESymbol, Exchange);
                m_BasicOperation.AddOrUpdateSymbolName(PESymbol, PESymbol);
                m_BasicOperation.AddOrUpdateSymbolName(PESymbol, CESymbol);
                m_BasicOperation.AddOrUpdateSymbolName(CESymbol, PESymbol);
                m_BasicOperation.AddOrUpdateOrderType(PESymbol, OrderType);
                m_BasicOperation.AddOrUpdateOrderType(CESymbol, OrderType);
                m_BasicOperation.AddOrUpdateMarginLimit(CESymbol, MarginLimit);
                m_BasicOperation.AddOrUpdateMarginLimit(PESymbol, MarginLimit);
                m_BasicOperation.AddOrUpdateNoOfLotSize(CESymbol, noOfLotSize);
                m_BasicOperation.AddOrUpdateNoOfLotSize(PESymbol, noOfLotSize);
                m_BasicOperation.AddOrUpdateQuantityMultiplier(CESymbol, quantity);
                m_BasicOperation.AddOrUpdateQuantityMultiplier(PESymbol, quantity);
                m_BasicOperation.AddOrUpdateStopLoss(CESymbol, stoploss);
                m_BasicOperation.AddOrUpdateStopLoss(PESymbol, stoploss);
                AlgoOMS.CheckTradingSymbolPresentOrNot(userID, symbolNames);
                logger.LogMessage("StoreSymbolNameInStorage : symbols name stored in storage and passed to subscription " + userID, MessageType.Informational);

            }
            catch (Exception e)
            {
                logger.LogMessage("StoreSymbolNameInStorage : Exception Error Message = " + e.Message + " " + userID, MessageType.Exception);
            }
        }


        public bool CheckWeeklyExpiry(string TradingSymbol, string Exchange, string ExpiryDate, double LTP, int RoundOffValue, string OrderType, double MarginLimit, int putStrikePrice, int callStrikePrice, double quatity, int noOfLotSize, double stoploss)
        {
            try
            {
                string symbol = GetWeeklyOptionSymbolName(TradingSymbol, ExpiryDate, LTP, "CE", RoundOffValue, callStrikePrice);
                string CESymbol = symbol;
                string Token = AlgoOMS.GetOptionsInstrumentToken(userID, symbol, "OPTIONS");
                if (Token != "")
                {
                    symbol = GetWeeklyOptionSymbolName(TradingSymbol, ExpiryDate, LTP, "PE", RoundOffValue, putStrikePrice);
                    Token = AlgoOMS.GetOptionsInstrumentToken(userID, symbol, "OPTIONS");
                    if (Token != "")
                    {
                        logger.LogMessage("CheckWeeklyExpiry : Weekly symbol available for CE and PE CEsymbol " + CESymbol + " PEsymbol " + symbol + " " + userID, MessageType.Informational);
                        StoreSymbolNameInStorage(Exchange, CESymbol, symbol, OrderType, MarginLimit, quatity, noOfLotSize, stoploss);
                        //IRDS::Jyoti::16-07-2021::Added for restore changes
                        m_BasicOperation.AddOrUpdateOriginalSymbols(TradingSymbol, Exchange);
                        return true;
                    }
                    else
                    {
                        logger.LogMessage("CheckWeeklyExpiry : Weekly symbol not available for PE symbol " + symbol + " " + userID, MessageType.Informational);
                    }
                }
                logger.LogMessage("CheckWeeklyExpiry : Weekly symbol not available CE , Not going to create PE symbol creation symbol " + CESymbol + " " + userID, MessageType.Informational);
            }
            catch (Exception e)
            {
                logger.LogMessage("CheckWeeklyExpiry : Exception Error Message = " + e.Message + " " + userID, MessageType.Exception);
            }
            return false;
        }

        public bool CheckMonthlyExpiry(string TradingSymbol, string Exchange, string ExpiryDate, double LTP, string OrderType, double MarginLimit, int RoundOff, int putStrikePrice, int callStrikePrice, double quantity, int noOfLotSize, double stoploss)
        {
            try
            {
                string symbol = GetMonthlyOptionSymbolName(TradingSymbol, ExpiryDate, LTP, "CE", RoundOff, callStrikePrice);
                string CESymbol = symbol;
                string Token = AlgoOMS.GetOptionsInstrumentToken(userID, symbol, "OPTIONS");
                if (Token != "")
                {
                    symbol = GetMonthlyOptionSymbolName(TradingSymbol, ExpiryDate, LTP, "PE", RoundOff, putStrikePrice);
                    Token = AlgoOMS.GetOptionsInstrumentToken(userID, symbol, "OPTIONS");
                    if (Token != "")
                    {
                        logger.LogMessage("CheckMonthlyExpiry : monthly symbol available for CE and PE CEsymbol " + CESymbol + " PEsymbol " + symbol + " " + userID, MessageType.Informational);
                        StoreSymbolNameInStorage(Exchange, CESymbol, symbol, OrderType, MarginLimit, quantity, noOfLotSize, stoploss);
                        //IRDS::Jyoti::16-07-2021::Added for restore changes
                        m_BasicOperation.AddOrUpdateOriginalSymbols(TradingSymbol, Exchange);
                        return true;
                    }
                    else
                    {
                        logger.LogMessage("CheckMonthlyExpiry : monthly symbol not available for PE symbol " + symbol + " " + userID, MessageType.Informational);
                    }
                }
                AlgoOMS.AddNotificationInQueueForToast(userID, CESymbol + " monthly symbol not available in db");
                logger.LogMessage("CheckMonthlyExpiry : monthly symbol not available CE , Not going to create PE symbol creation symbol " + CESymbol + " " + userID, MessageType.Informational);

            }
            catch (Exception e)
            {
                logger.LogMessage("CheckMonthlyExpiry : Exception Error Message = " + e.Message + " " + userID, MessageType.Exception);
            }
            return false;
        }

        public void CheckForClosingOrder(string TradingSymbol, string Exchange)
        {
            string symbol = m_BasicOperation.GetSymbolName(TradingSymbol, "CE");
            AlgoOMS.CloseOrder(userID, TradingSymbol, Exchange, m_BasicOperation.GetOrderType(TradingSymbol));
        }
        public int GetQuantity(string TradingSymbol, string Exchange)
        {
            //int lotfromDB = AlgoOMS.GetLotSize(userID, TradingSymbol, Exchange);
            WriteUniquelogs(TradingSymbol + " " + userID, " no of lot size " + m_BasicOperation.GetNoOfLotSize(TradingSymbol) + " quantity multiplier = " + m_BasicOperation.GetQuantityMultiplier(TradingSymbol), MessageType.Informational);
            int quantity = Convert.ToInt32(m_BasicOperation.GetNoOfLotSize(TradingSymbol) * m_BasicOperation.GetQuantityMultiplier(TradingSymbol));
            return quantity;
        }
        //load values from ini file
        public void loadINIValues()
        {
            try
            {
                m_StartEntryTime = m_ReadSettings.m_StartEntryTime;
                int Interval = 1;
                DateTime dt1 = DateTime.ParseExact(m_StartEntryTime, "HH:mm", null);
                FirstTickEnd = m_StartEntryTime;
                StrategyStartTime = FirstTickEnd;
                End = dt1.AddSeconds(60 * (Interval + 1)).ToString("HH:mm");
                m_EndEntryTime = m_ReadSettings.m_EndEntryTime;
                m_EndExitTime = m_ReadSettings.m_EndExitTime;
                DateTime dt = DateTime.ParseExact(m_EndExitTime, "HH:mm", null);
                EndTime = dt.AddMinutes(Interval).ToString("HH:mm");
                //m_Interval = m_ReadSettings.m_Interval;
                //m_NoOfLots = m_ReadSettings.m_NoOfLots;
                //m_CallStrikeValue = m_ReadSettings.m_CallStrikeValue;
                //m_PutStrikeValue = m_ReadSettings.m_PutStrikeValue;
                //m_StopLossPercent = m_ReadSettings.m_StopLossPercent;
                m_BreakEvenStop = m_ReadSettings.m_BreakEvenStop;
                //m_ReducePercentInLot = m_ReadSettings.m_ReducePercentInLot;
                m_StoplossRejectTimeInterval = m_ReadSettings.m_StoplossRejectTimeInterval;
                ListOfTradingSymbolsInfo = m_ReadSettings.TradingSymbolsInfoList;
            }
            catch (Exception e)
            {
                logger.LogMessage("loadINIValues - Exception Error Message = " + e.Message + " " + userID, MessageType.Exception);
            }
        }

        //function to start thread
        public bool startThread(string userID, ReadSettings Settings)
        {
            //m_ReadSettings = new ReadSettings(this.logger, AlgoOMS);
            m_ReadSettings = Settings;
            bool res = m_ReadSettings.ReadINIFile(m_TradingIniFilePath);
            if (!res)
            {
                return res;
            }
            loadINIValues();
            //jyoti::25-Jun-2021::Added for start automation
            if (automation == null || (!automation.IsAlive))
            {
                AlgoOMS.AddNotificationInQueue(userID, "UserId : " + userID + " Automation Started Time : " + DateTime.Now.ToString("HH:mm:ss"));
                logger.LogMessage("Strating StartAutomation thread " + userID, MessageType.Informational);
                //IRDS::Jyoti::30-Jul-21::Added to solve exe lock issue
                DateTime startDateTime = DateTime.ParseExact(StrategyStartTime, "HH:mm", null);
                DateTime marketEndTime = DateTime.ParseExact("15:30", "HH:mm", null);
                if (File.Exists(m_BinFileName + "_" + userID + ".bin") && DateTime.Now.ToOADate() > startDateTime.ToOADate() && DateTime.Now.ToOADate() < marketEndTime.ToOADate())
                {
                    string title = "RealStock Strategy";
                    string message = "Do you want to store last execution?";
                    MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                    DialogResult dialog = System.Windows.Forms.MessageBox.Show(new Form() { TopMost = true }, message, title, buttons, MessageBoxIcon.Exclamation);
                    //IRDS::Jyoti::22-07-2021::Added for restore automation changes
                    if (dialog == DialogResult.Yes)
                    {
                        WriteUniquelogs("RestoreStructure", "LoadStructure : Click on Yes", MessageType.Informational);
                        //do something
                        //sanika::25-sep-2020::added for resume exe
                        if (File.Exists(m_BinFileName + "_" + userID + ".bin"))
                        {
                            m_BasicOperation.LoadStructure(m_BinFileName + "_" + userID + ".bin");
                            WriteUniquelogs("RestoreStructure", "LoadStructure : loaded structure", MessageType.Informational);
                        }
                        else
                        {
                            WriteUniquelogs("RestoreStructure", "LoadStructure : Bin file not exists", MessageType.Informational);
                        }
                    }
                    else if (dialog == DialogResult.No)
                    {
                        WriteUniquelogs("RestoreStructure", "LoadStructure : Click on No so resetting storage", MessageType.Informational);
                        ResetStorage();
                        //IRDS::Jyoti::16-07-2021::Added for close all and start changes
                        m_IsOptionSymbolNameCreated = false;
                    }
                }
                //IRDS::Jyoti::26-07-2021::Added as sir's suggestion for restore structure issue
                else if (DateTime.Now.ToOADate() < startDateTime.ToOADate())
                {
                    ResetStorage();
                }

                automation = new Thread(() => ExecuteSyncLogic());
                automation.Start();
            }
            return true;
        }

        //add symbols to inticker 
        public void CheckTradingSymbolPresentOrNot()
        {
            try
            {
                AlgoOMS.CheckTradingSymbolPresentOrNot(userID, m_ReadSettings.TradingsymbolList);
            }
            catch (Exception e)
            {
                logger.LogMessage("CheckTradingSymbolPresentOrNot : Exception Error Message = " + e.Message + " " + userID, MessageType.Exception);
            }
        }

        //stop thread
        public bool StopThread()
        {
            // if (AlgoOMS.stopThread())
            {
                if (automation != null)
                {//jyoti::25-Jun-2021::added try-catch-finally for start automation
                    try
                    {
                        if (automation.IsAlive)
                        {
                            logger.LogMessage("Stopped Thread " + userID, MessageType.Informational);
                            automation.Abort();
                            Thread.Sleep(1000);
                            if (!automation.IsAlive)
                            {
                                automation = null; return true;
                            }
                            automation = null;
                        }
                    }
                    catch (Exception e)
                    {
                        automation = null;
                    }
                    finally
                    {
                        automation = null;
                    }
                }
            }
            return true;
        }

        //sanika::16-July-2021::stop thread function to stop threads from dll
        public bool StopMainThreads()
        {
            AlgoOMS.stopThread();
            logger.LogMessage("Stopped Thread main threads from algotrader" + userID, MessageType.Informational);
            return true;
        }

        //function to copy object of form_Auto
        public void loadObject(StrategyForm form_Auto)
        {
            this.form_Auto = form_Auto;
        }

        //function for write logs
        public void WriteUniquelogs(string symbol, string message, MessageType msgType)
        {
            try
            {
                SymbolLogs log = new SymbolLogs();
                //log = SymbolLogs.InstanceSymbolLogs;
                log.createFile(symbol);
                log.LogMessage(message, MessageType.Informational);
                log = null;
            }
            catch (Exception e)
            {
                Console.WriteLine("WriteUniquelogs : Exception : " + e.Message);
            }
        }

        //Read symbols
        public void readTradingSymbol(ReadSettings readSettings)
        {
            if (readSettings != null)
                this.m_ReadSettings = readSettings;
            //IRDS::Jyoti::16-07-2021::Added for restore changes
            //if (readSettings.TradingsymbolList.Count == 0)
            readSettings.ReadINIFile(m_TradingIniFilePath);
            if (readSettings.TradingsymbolList.Count > ListOfTradingSymbolsInfo.Count)
            {
                m_IsOptionSymbolNameCreated = false;
            }
        }

        //close all order after clikcing stop automation and close order
        public bool CloseAllOrder()
        {
            bool isCloseAllOrder = false;
            try
            {
                Dictionary<string, string> optionSymbolNames = m_BasicOperation.GetOptionSymbols();
                if (optionSymbolNames != null && optionSymbolNames.Count > 0)
                {
                    foreach (var symbolInfo in optionSymbolNames) //loop for symbols from ini file
                    {
                        string TradingSymbol = symbolInfo.Key;
                        string Exchange = symbolInfo.Value;
                        if (AlgoOMS.CloseOrder(userID, TradingSymbol, Exchange, m_BasicOperation.GetOrderType(TradingSymbol)))
                        {
                            WriteUniquelogs(TradingSymbol + " " + userID, "CloseAllOrder : Order closed because of exit time", MessageType.Informational);
                            isCloseAllOrder = true;
                        }
                        else
                        {
                            WriteUniquelogs(TradingSymbol + " " + userID, "CloseAllOrder : Ordernot able to closed", MessageType.Informational);
                            isCloseAllOrder = false;
                        }

                    }
                }
                logger.LogMessage("CloseAllOrder : closed all order " + userID, MessageType.Informational);
            }
            catch (Exception e)
            {
                logger.LogMessage("CloseAllOrder : Exception Error Message = " + e.Message + " " + userID, MessageType.Exception);
            }

            return isCloseAllOrder;
        }

        //sanika::28-sep-2020::added to save bin file
        public void SaveBinFile()
        {
            try
            {
                m_BasicOperation.SaveBinFile(m_BinFileName + "_" + userID + ".bin");
                // WriteUniquelogs("RestoreStructure", "SaveBinFile : File saved sucessfully", MessageType.Informational);
            }
            catch (Exception e)
            {
                WriteUniquelogs("RestoreStructure", "SaveBinFile : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        //sanika::28-sep-2020::added to delete bin file
        public void DeleteBinFile()
        {
            try
            {
                if (File.Exists(m_BinFileName + "_" + userID + ".bin"))
                {
                    File.Delete(m_BinFileName + "_" + userID + ".bin");
                    WriteUniquelogs("RestoreStructure", "DeleteBinFile : File deleted sucessfully", MessageType.Informational);
                }
                else
                {
                    WriteUniquelogs("RestoreStructure", "DeleteBinFile : File not exists", MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs("RestoreStructure", "DeleteBinFile : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        //sanika::28-sep-2020::added new function to load old structure 
        public bool LoadStructure(BasicOperation openOrderInfo)
        {
            try
            {
                m_ReadSettings = new ReadSettings(this.logger, AlgoOMS);
                bool res = m_ReadSettings.ReadINIFile(m_TradingIniFilePath);
                if (!res)
                {
                    WriteUniquelogs("RestoreStructure", "LoadStructure : Not able to read time", MessageType.Informational);
                    return res;
                }
                loadINIValues();
                WriteUniquelogs("RestoreStructure", "LoadStructure : Read time from ini file", MessageType.Informational);
                DateTime startDateTime = DateTime.ParseExact(StrategyStartTime, "HH:mm", null);
                DateTime marketEndTime = DateTime.ParseExact("15:30", "HH:mm", null);
                if (File.Exists(m_BinFileName + "_" + userID + ".bin") && DateTime.Now.ToOADate() > startDateTime.ToOADate() && DateTime.Now.ToOADate() < marketEndTime.ToOADate())
                {
                    WriteUniquelogs("RestoreStructure", "LoadStructure : Time condition for restore structure true", MessageType.Informational);
                    //IRDS::Jyoti::23-07-2021::Commented as per sir's suggestion
                    //DialogResult dialogResult = MessageBox.Show("Do you want to restore last execution?", "IRDSStrategyExecutor", MessageBoxButtons.YesNo);
                    //if (dialogResult == DialogResult.Yes)
                    //{
                    //    WriteUniquelogs("RestoreStructure", "LoadStructure : Click on Yes", MessageType.Informational);
                    //    //do something
                    //    //sanika::25-sep-2020::added for resume exe
                    //    if (File.Exists(m_BinFileName + "_" + userID+".bin"))
                    //    {
                    //        m_BasicOperation.LoadStructure(m_BinFileName + "_" + userID + ".bin");

                    //    }
                    //    else
                    //    {                            
                    //        WriteUniquelogs("RestoreStructure", "LoadStructure : Bin file not exists", MessageType.Informational);
                    //    }
                    //}
                    //else if (dialogResult == DialogResult.No)
                    //{
                    //    DeleteBinFile();
                    //    WriteUniquelogs("RestoreStructure", "LoadStructure : Click on No", MessageType.Informational);
                    //    //IRDS::Jyoti::16-07-2021::Added for close all and start changes
                    //    m_IsOptionSymbolNameCreated = false;
                    //}

                }
                else
                {
                    //IRDS::Jyoti::26-07-2021::Added for restore structure issue
                    //DeleteBinFile();
                    ResetStorage();
                    WriteUniquelogs("RestoreStructure", "LoadStructure : Time condition not match to restore data StrategyStartTime " + StrategyStartTime, MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs("RestoreStructure", "LoadStructure : Exception Error Message = " + e.Message, MessageType.Exception);
            }
            return true;
        }

        //to increase the time of modify reversal order
        public void changeTime(string tickCurrentTime)
        {
            if ((TimeSpan.Parse(tickCurrentTime) >= TimeSpan.Parse(FirstTickEnd)))
            {
                DateTime dt1 = DateTime.ParseExact(FirstTickEnd, "HH:mm", null);
                FirstTickEnd = dt1.AddSeconds(60 * Convert.ToInt32(m_Interval)).ToString("HH:mm");
                End = dt1.AddSeconds(60 * ((Convert.ToInt32(m_Interval)) + 1)).ToString("HH:mm");
            }
        }

        public void CloseAndCancelOrders(string TradingSymbol, string Exchange)
        {
            try
            {
                if (AlgoOMS.IsPositionOpen(userID, TradingSymbol, Exchange))
                {
                    Thread.Sleep(100);
                    if (AlgoOMS.IsPositionOpen(userID, TradingSymbol, Exchange))
                    {
                        bool isClose = false;
                        if (m_OrderClosed.ContainsKey(TradingSymbol))
                        {
                            isClose = m_OrderClosed[TradingSymbol];
                        }
                        if (isClose == false)
                        {
                            AlgoOMS.CloseOrder(userID, TradingSymbol, Exchange);
                            m_OrderClosed.Add(TradingSymbol, true);
                        }
                    }
                }
                if (AlgoOMS.IsPendingStopOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_BUY))
                    AlgoOMS.CancelPendingStopOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_BUY);
                if (AlgoOMS.IsPendingStopOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_SELL))
                    AlgoOMS.CancelPendingStopOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_SELL);
                WriteUniquelogs(TradingSymbol + " " + userID, "CloseAndCancelOrders : closed order and ignore for this symbol", MessageType.Informational);
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + userID, "CloseAndCancelOrders : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

        public void CheckRejectedOrders(string TradingSymbol, string Exchange)
        {
            try
            {
                string symbol = m_BasicOperation.GetCESymbolName(TradingSymbol);
                string orderId = m_BasicOperation.GetOpenOrderId(symbol);
                string status = AlgoOMS.GetOrderStatusByOrderID(userID, symbol, Exchange, orderId);

                if (status == Constants.ORDER_STATUS_REJECTED)
                {
                    //sanika::14-Jun-2021::added logs
                    WriteUniquelogs(symbol + " " + userID, "CheckRejectedOrders : orderId " + orderId + " status = " + status, MessageType.Informational);
                    WriteUniquelogs(symbol + " " + userID, "CheckRejectedOrders : Found CE reject order ", MessageType.Informational);
                    string PEName = m_BasicOperation.GetPESymbolName(TradingSymbol);
                    if (AlgoOMS.IsPendingLimitOrder(userID, PEName, Exchange, Constants.TRANSACTION_TYPE_SELL, m_BasicOperation.GetOrderType(TradingSymbol)))
                    {
                        WriteUniquelogs(symbol + " " + userID, "CheckRejectedOrders : Found PE pending order ", MessageType.Informational);
                        AlgoOMS.CloseOrder(userID, PEName, Exchange, m_BasicOperation.GetOrderType(TradingSymbol));
                        m_BasicOperation.AddOrUpdateCompletionList(PEName, symbol, true);
                    }
                }
                symbol = m_BasicOperation.GetPESymbolName(TradingSymbol);
                orderId = m_BasicOperation.GetOpenOrderId(symbol);
                status = AlgoOMS.GetOrderStatusByOrderID(userID, symbol, Exchange, orderId);

                if (status == Constants.ORDER_STATUS_REJECTED)
                {
                    //sanika::14-Jun-2021::added logs
                    WriteUniquelogs(symbol + " " + userID, "CheckRejectedOrders : orderId " + orderId + " status = " + status, MessageType.Informational);
                    WriteUniquelogs(symbol + " " + userID, "CheckRejectedOrders : Found PE reject order ", MessageType.Informational);
                    string CEName = m_BasicOperation.GetCESymbolName(TradingSymbol);
                    if (AlgoOMS.IsPendingLimitOrder(userID, CEName, Exchange, Constants.TRANSACTION_TYPE_SELL, m_BasicOperation.GetOrderType(TradingSymbol)))
                    {
                        WriteUniquelogs(symbol + " " + userID, "CheckRejectedOrders : Found CE pending order ", MessageType.Informational);
                        AlgoOMS.CloseOrder(userID, CEName, Exchange, m_BasicOperation.GetOrderType(TradingSymbol));
                        m_BasicOperation.AddOrUpdateCompletionList(CEName, symbol, true);
                    }
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + userID, "CheckRejectedOrders : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }
    }
}
