﻿using IRDSAlgoOMS;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace RealStockA1BasicOperationsZerodha
{
    //sanika::25-sep-2020::added for resume 
    [Serializable()]
    public class OpenOrderInfo
    {        
        private Dictionary<string, OpenOrderInfo> symbolWiseOrderInfo;      
        int BuyOrSell = 0; 
        double EntryPriceCE { get; set; }
        double EntryPricePE { get; set; }

        string SellEntryOrderIdCE = null;
        string SellEntryOrderIdPE = null;
        bool m_IsStopLossOrderPlaced = false;
        bool m_IsOrderModified = false;
        string m_StopLossOrderIdCE { get; set; }
        string m_StopLossOrderIdPE { get; set; }
        DateTime m_LastTimeCE { get; set; }
        DateTime m_LastTimePE { get; set; }
        DateTime m_EntryOrderTimeCE { get; set; }
        DateTime m_EntryOrderTimePE { get; set; }
        string m_SymbolNamePE { get; set; }
        string m_SymbolNameCE { get; set; }
        int m_QuantityPE { get; set; }
        int m_QuantityCE { get; set; }

        int m_OrderCounter = 0;

        bool m_IsComplete = false;
        string m_OrderType { get; set; }
        DateTime m_SLOrderTimeCE { get; set; }
        DateTime m_SLOrderTimePE { get; set; }
        double m_MarginLimit { get; set; }
        double m_QuantityMultiplier { get; set; }
        int m_NoOfLotSize { get; set; }
        bool m_IsMarketOrderPlacedPE { get; set; }
        bool m_IsMarketOrderPlacedCE { get; set; }
        double m_StopLoss { get; set; }
        string m_TransactionType { get; set; }

        private Dictionary<string, string> m_optionSymbols;
        private Dictionary<string, string> m_originalSymbols;
        public OpenOrderInfo()
        {
            symbolWiseOrderInfo = new Dictionary<string, OpenOrderInfo>();
            m_optionSymbols = new Dictionary<string, string>();
            m_originalSymbols = new Dictionary<string, string>();
        }
        public void AddOrUpdateSymbolName(string TradingSymbol, string SymbolName)
        {
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    if (SymbolName.EndsWith("CE"))
                        symbolWiseOrderInfo[TradingSymbol].m_SymbolNameCE = SymbolName;
                    else if (SymbolName.EndsWith("PE"))
                        symbolWiseOrderInfo[TradingSymbol].m_SymbolNamePE = SymbolName;
                }
                else
                {
                    OpenOrderInfo orderInfo = new OpenOrderInfo();
                    if (SymbolName.EndsWith("CE"))
                        orderInfo.m_SymbolNameCE = SymbolName;
                    else if (TradingSymbol.EndsWith("PE"))
                        orderInfo.m_SymbolNamePE = SymbolName;
                    symbolWiseOrderInfo.Add(TradingSymbol, orderInfo);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);               
            }
        }

        public void AddOrUpdateMarketOrderPlaced(string TradingSymbol, bool value)
        {
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    if (TradingSymbol.EndsWith("CE"))
                        symbolWiseOrderInfo[TradingSymbol].m_IsMarketOrderPlacedCE = value;
                    else if (TradingSymbol.EndsWith("PE"))
                        symbolWiseOrderInfo[TradingSymbol].m_IsMarketOrderPlacedPE = value;
                }
                else
                {
                    OpenOrderInfo orderInfo = new OpenOrderInfo();
                    if (TradingSymbol.EndsWith("CE"))
                        orderInfo.m_IsMarketOrderPlacedCE = value;
                    else if (TradingSymbol.EndsWith("PE"))
                        orderInfo.m_IsMarketOrderPlacedPE = value;
                    symbolWiseOrderInfo.Add(TradingSymbol, orderInfo);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
            }
        }

        public void AddOrUpdateQuantity(string TradingSymbol, int Quantity)
        {
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    if (TradingSymbol.EndsWith("CE"))
                        symbolWiseOrderInfo[TradingSymbol].m_QuantityCE = Quantity;
                    else if (TradingSymbol.EndsWith("PE"))
                        symbolWiseOrderInfo[TradingSymbol].m_QuantityPE = Quantity;
                }
                else
                {
                    OpenOrderInfo orderInfo = new OpenOrderInfo();
                    if (TradingSymbol.EndsWith("CE"))
                        orderInfo.m_QuantityCE = Quantity;
                    else if (TradingSymbol.EndsWith("PE"))
                        orderInfo.m_QuantityPE = Quantity;
                    symbolWiseOrderInfo.Add(TradingSymbol, orderInfo);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
            }
        }

        public void AddOrUpdateOrderCounter(string TradingSymbol)
        {
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {                   
                    symbolWiseOrderInfo[TradingSymbol].m_OrderCounter++;                  
                }
                else
                {
                    OpenOrderInfo orderInfo = new OpenOrderInfo();                   
                        orderInfo.m_OrderCounter = 1;                   
                    symbolWiseOrderInfo.Add(TradingSymbol, orderInfo);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
            }
        }
        public bool AddOpenPrice(string TradingSymbol, double OpenPrice)
        {
            bool isAdd = false;
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    if(TradingSymbol.EndsWith("CE"))
                     symbolWiseOrderInfo[TradingSymbol].EntryPriceCE = OpenPrice;
                    else if (TradingSymbol.EndsWith("PE"))
                        symbolWiseOrderInfo[TradingSymbol].EntryPricePE = OpenPrice;
                    isAdd = true;
                }
                else
                {
                    OpenOrderInfo orderInfo = new OpenOrderInfo();
                    if (TradingSymbol.EndsWith("CE"))
                        orderInfo.EntryPriceCE = OpenPrice;
                    else if (TradingSymbol.EndsWith("PE"))
                        orderInfo.EntryPricePE = OpenPrice;
                    symbolWiseOrderInfo.Add(TradingSymbol, orderInfo);
                    isAdd = true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
                isAdd = false;
            }
            return isAdd;
        }
        public string GetOpenOrderId(string TradingSymbol)
        {
            string orderId = "NA";
            if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
            {
                if(TradingSymbol.EndsWith("CE"))
                    orderId = symbolWiseOrderInfo[TradingSymbol].SellEntryOrderIdCE;
                else if (TradingSymbol.EndsWith("PE"))
                    orderId = symbolWiseOrderInfo[TradingSymbol].SellEntryOrderIdPE;
            } 
            return orderId;
        }

        public bool IsMarketOrderPlaced(string TradingSymbol)
        {
            bool isPlaced = false;
            if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
            {
                if (TradingSymbol.EndsWith("CE"))
                    isPlaced = symbolWiseOrderInfo[TradingSymbol].m_IsMarketOrderPlacedCE;
                else if (TradingSymbol.EndsWith("PE"))
                    isPlaced = symbolWiseOrderInfo[TradingSymbol].m_IsMarketOrderPlacedPE;
            }
            return isPlaced;
        }

        public int GetOrderCounter(string TradingSymbol)
        {
            int orderCounter = 0;
            if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
            {
                orderCounter = symbolWiseOrderInfo[TradingSymbol].m_OrderCounter;
            }
            return orderCounter;
        }

        public string GetOpenOrderIdWithOptionType(string TradingSymbol,string optionType)
        {
            string orderId = "NA";
            if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
            {
                if (optionType == "CE")
                    orderId = symbolWiseOrderInfo[TradingSymbol].SellEntryOrderIdCE;
                else if (optionType == "PE")
                    orderId = symbolWiseOrderInfo[TradingSymbol].SellEntryOrderIdPE;
            }
            return orderId;
        }

        public double GetEntryOrderPrice(string TradingSymbol)
        {
            double orderPrice = 0;
            if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
            {
                if (TradingSymbol.EndsWith("CE"))
                    orderPrice = symbolWiseOrderInfo[TradingSymbol].EntryPriceCE;
                else if (TradingSymbol.EndsWith("PE"))
                    orderPrice = symbolWiseOrderInfo[TradingSymbol].EntryPricePE;
            }
            return orderPrice;
        }

        public double GetEntryOrderPriceWithOptionType(string TradingSymbol, string optionType)
        {
            double orderPrice = 0;
            if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
            {
                if (optionType == "CE")
                    orderPrice = symbolWiseOrderInfo[TradingSymbol].EntryPriceCE;
                else if (optionType == "PE")
                    orderPrice = symbolWiseOrderInfo[TradingSymbol].EntryPricePE;
            }
            return orderPrice;
        }

        public int GetQuantity(string TradingSymbol)
        {
            int qty = 0;
            if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
            {
                if (TradingSymbol.EndsWith("CE"))
                    qty = symbolWiseOrderInfo[TradingSymbol].m_QuantityCE;
                else if (TradingSymbol.EndsWith("PE"))
                    qty = symbolWiseOrderInfo[TradingSymbol].m_QuantityPE;
            }
            return qty;
        }
        public void AddOrUpdateOrderId(string TradingSymbol, string OrderId)
        {           
            if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
            {
                if (TradingSymbol.EndsWith("CE"))
                    symbolWiseOrderInfo[TradingSymbol].SellEntryOrderIdCE = OrderId;
                else if (TradingSymbol.EndsWith("PE"))
                    symbolWiseOrderInfo[TradingSymbol].SellEntryOrderIdPE = OrderId;
            }
            else
            {
                OpenOrderInfo openOrderInfo = new OpenOrderInfo();
                if (TradingSymbol.EndsWith("CE"))
                    openOrderInfo.SellEntryOrderIdCE = OrderId;
                else if (TradingSymbol.EndsWith("PE"))
                    openOrderInfo.SellEntryOrderIdPE = OrderId;
                symbolWiseOrderInfo.Add(TradingSymbol, openOrderInfo);
            }  
        }
    
        public void ResetOrderId(string TradingSymbol)
        {
            if(symbolWiseOrderInfo.ContainsKey(TradingSymbol))
            {                
                symbolWiseOrderInfo[TradingSymbol].SellEntryOrderIdPE = null;
                symbolWiseOrderInfo[TradingSymbol].SellEntryOrderIdCE = null;                            
                symbolWiseOrderInfo[TradingSymbol].BuyOrSell = 0;
            }
        }

        public void AddInformationIntoLog()
        {
            try
            {
                foreach (var data in symbolWiseOrderInfo)
                {
                    WriteUniquelogs("RestoreStructure", "AddInformationIntoLog : ########## " + data.Key + "##########", MessageType.Informational);
                    WriteUniquelogs("RestoreStructure", " BuyOrSell = " + data.Value.BuyOrSell , MessageType.Informational);             
                    WriteUniquelogs("RestoreStructure", " SellEntryOrderIdCE = " + data.Value.SellEntryOrderIdCE + " SellEntryOrderIdPE = " + data.Value.SellEntryOrderIdPE, MessageType.Informational);
                    WriteUniquelogs("RestoreStructure", "AddInformationIntoLog : ####################", MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs("RestoreStructure", "AddInformationIntoLog : Exception Error Message = " + e.Message, MessageType.Informational);
            }
        }

        public void WriteUniquelogs(string symbol, string message, MessageType msgType)
        {
            try
            {
                SymbolLogs log = new SymbolLogs();
                //log = SymbolLogs.InstanceSymbolLogs;
                log.createFile(symbol);
                log.LogMessage(message, MessageType.Informational);
                log = null;
            }
            catch (Exception e)
            {
                Console.WriteLine("WriteUniquelogs : Exception : " + e.Message);
            }
        }

        public bool AddOrUpdateIsStopLossOrderPlaced(string TradingSymbol, bool Value)
        {
            bool isAddOrUpdate = false;
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    symbolWiseOrderInfo[TradingSymbol].m_IsStopLossOrderPlaced = Value;
                    isAddOrUpdate = true;
                }
                else
                {
                    OpenOrderInfo orderInfo = new OpenOrderInfo();
                    orderInfo.m_IsStopLossOrderPlaced = Value;
                    symbolWiseOrderInfo.Add(TradingSymbol, orderInfo);
                    isAddOrUpdate = true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
                isAddOrUpdate = false;
            }
            return isAddOrUpdate;
        }

        public bool AddOrUpdateIsOrderModified(string TradingSymbol, bool Value)
        {
            bool isAddOrUpdate = false;
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    symbolWiseOrderInfo[TradingSymbol].m_IsOrderModified = Value;
                    isAddOrUpdate = true;
                }
                else
                {
                    OpenOrderInfo orderInfo = new OpenOrderInfo();
                    orderInfo.m_IsOrderModified = Value;
                    symbolWiseOrderInfo.Add(TradingSymbol, orderInfo);
                    isAddOrUpdate = true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
                isAddOrUpdate = false;
            }
            return isAddOrUpdate;
        }

        public bool GetIsStopLossOrderPlaced(string TradingSymbol)
        {
            bool isPlaced = false;
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    isPlaced = symbolWiseOrderInfo[TradingSymbol].m_IsStopLossOrderPlaced;
                }
                else
                {
                    isPlaced = false;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
                isPlaced = false;
            }
            return isPlaced;
        }

        public bool GetOrderModified(string TradingSymbol)
        {
            bool isPlaced = false;
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    isPlaced = symbolWiseOrderInfo[TradingSymbol].m_IsOrderModified;
                }
                else
                {
                    isPlaced = false;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
                isPlaced = false;
            }
            return isPlaced;
        }


            public string GetStopLossOrderId(string TradingSymbol)
        {
            string orderId = "";
            if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
            {
                if (TradingSymbol.EndsWith("CE"))
                    orderId = symbolWiseOrderInfo[TradingSymbol].m_StopLossOrderIdCE;
                else if (TradingSymbol.EndsWith("PE"))
                    orderId = symbolWiseOrderInfo[TradingSymbol].m_StopLossOrderIdPE;

            }            
            return orderId;
        }

        public void AddOrUpdateStopLossOrderId(string TradingSymbol, string orderId)
        {
            bool isAddOrUpdate = false;
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    if (TradingSymbol.EndsWith("CE"))
                        symbolWiseOrderInfo[TradingSymbol].m_StopLossOrderIdCE = orderId;
                    else if (TradingSymbol.EndsWith("PE"))
                        symbolWiseOrderInfo[TradingSymbol].m_StopLossOrderIdPE = orderId;
                    isAddOrUpdate = true;
                }
                else
                {
                    OpenOrderInfo orderInfo = new OpenOrderInfo();
                    if (TradingSymbol.EndsWith("CE"))
                        orderInfo.m_StopLossOrderIdCE = orderId;
                    else if (TradingSymbol.EndsWith("PE"))
                        orderInfo.m_StopLossOrderIdPE = orderId;
                    symbolWiseOrderInfo.Add(TradingSymbol, orderInfo);
                    isAddOrUpdate = true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
                isAddOrUpdate = false;
            }
        }      

        public DateTime GetTime(string TradingSymbol)
        {
            DateTime time = new DateTime();
            if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
            {
                if (TradingSymbol.EndsWith("CE"))
                    time = symbolWiseOrderInfo[TradingSymbol].m_LastTimeCE;
                else if (TradingSymbol.EndsWith("PE"))
                    time = symbolWiseOrderInfo[TradingSymbol].m_LastTimePE;
            }
            return time;
        }

        public void AddOrUpdateTime(string TradingSymbol, DateTime Time)
        {
            bool isAddOrUpdate = false;
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    if (TradingSymbol.EndsWith("CE"))
                        symbolWiseOrderInfo[TradingSymbol].m_LastTimeCE = Time;
                    else if (TradingSymbol.EndsWith("PE"))
                        symbolWiseOrderInfo[TradingSymbol].m_LastTimePE = Time;
                    isAddOrUpdate = true;
                }
                else
                {
                    OpenOrderInfo orderInfo = new OpenOrderInfo();
                    if (TradingSymbol.EndsWith("CE"))
                        orderInfo.m_LastTimeCE = Time;
                    else if (TradingSymbol.EndsWith("PE"))
                        orderInfo.m_LastTimePE = Time;
                    symbolWiseOrderInfo.Add(TradingSymbol, orderInfo);
                    isAddOrUpdate = true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
                isAddOrUpdate = false;
            }
        }

        public void AddOrUpdateEntryOrderTime(string TradingSymbol, DateTime Time)
        {           
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    if(TradingSymbol.EndsWith("CE"))
                        symbolWiseOrderInfo[TradingSymbol].m_EntryOrderTimeCE = Time;  
                    else if(TradingSymbol.EndsWith("PE"))
                        symbolWiseOrderInfo[TradingSymbol].m_EntryOrderTimePE = Time;
                }
                else
                {
                    OpenOrderInfo orderInfo = new OpenOrderInfo();
                    if (TradingSymbol.EndsWith("CE"))
                        orderInfo.m_EntryOrderTimeCE = Time;
                    else if (TradingSymbol.EndsWith("PE"))
                        orderInfo.m_EntryOrderTimePE = Time;
                    symbolWiseOrderInfo.Add(TradingSymbol, orderInfo);                  
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);               
            }
        }


        public void AddOrUpdateSLOrderTime(string TradingSymbol, DateTime Time)
        {
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    if (TradingSymbol.EndsWith("CE"))
                        symbolWiseOrderInfo[TradingSymbol].m_SLOrderTimeCE = Time;
                    else if (TradingSymbol.EndsWith("PE"))
                        symbolWiseOrderInfo[TradingSymbol].m_SLOrderTimePE = Time;
                }
                else
                {
                    OpenOrderInfo orderInfo = new OpenOrderInfo();
                    if (TradingSymbol.EndsWith("CE"))
                        orderInfo.m_SLOrderTimeCE = Time;
                    else if (TradingSymbol.EndsWith("PE"))
                        orderInfo.m_SLOrderTimePE = Time;
                    symbolWiseOrderInfo.Add(TradingSymbol, orderInfo);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
            }
        }

        public DateTime GetEntryOrderTime(string TradingSymbol)
        {
            DateTime time = new DateTime();
            if(symbolWiseOrderInfo.ContainsKey(TradingSymbol))
            {
                if (TradingSymbol.EndsWith("CE"))
                    time = symbolWiseOrderInfo[TradingSymbol].m_EntryOrderTimeCE;
                else if (TradingSymbol.EndsWith("PE"))
                    time = symbolWiseOrderInfo[TradingSymbol].m_EntryOrderTimePE;
            }
            return time;
        }

        public DateTime GetSLOrderTime(string TradingSymbol)
        {
            DateTime time = new DateTime();
            if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
            {
                if (TradingSymbol.EndsWith("CE"))
                    time = symbolWiseOrderInfo[TradingSymbol].m_SLOrderTimeCE;
                else if (TradingSymbol.EndsWith("PE"))
                    time = symbolWiseOrderInfo[TradingSymbol].m_SLOrderTimePE;
            }
            return time;
        }

        public string GetSymbolName(string TradingSymbol,string optionType)
        {
            string symbol = "NA";
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    if (optionType == "CE")
                        symbol = symbolWiseOrderInfo[TradingSymbol].m_SymbolNameCE;
                    else if (optionType == "PE")
                        symbol = symbolWiseOrderInfo[TradingSymbol].m_SymbolNamePE;
                }                
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
            }
            return symbol;
        }

        public string GetCESymbolName(string TradingSymbol)
        {
            string symbol = "NA";
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {                    
                    symbol = symbolWiseOrderInfo[TradingSymbol].m_SymbolNameCE;                   
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
            }
            return symbol;
        }

        public string GetPESymbolName(string TradingSymbol)
        {
            string symbol = "NA";
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {                    
                    symbol = symbolWiseOrderInfo[TradingSymbol].m_SymbolNamePE;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
            }
            return symbol;
        }

        public string GetOrderType(string TradingSymbol)
        {
            string orderType = "NA";
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    orderType = symbolWiseOrderInfo[TradingSymbol].m_OrderType;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
            }
            return orderType;
        }

        public double GetMarginLimit(string TradingSymbol)
        {
            double marginLimit = 0;
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    marginLimit = symbolWiseOrderInfo[TradingSymbol].m_MarginLimit;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
            }
            return marginLimit;
        }

        public string GetSymbolNameWithOptionType(string optionType)
        {
            string symbol = "NA";
            try
            {
                foreach (var sym in symbolWiseOrderInfo)
                {
                    string symbolname = sym.Key;
                    if (symbolname.EndsWith(optionType))
                    {
                        if (optionType == "CE")
                            symbol = sym.Value.m_SymbolNameCE;
                        else if (optionType == "PE")
                            symbol = sym.Value.m_SymbolNamePE;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
            }
            return symbol;
        }


        public void ClearOptionSymbols()
        {
            m_optionSymbols.Clear();
            m_originalSymbols.Clear();
        }

        public Dictionary<string,string> GetOptionSymbols()
        {
            return m_optionSymbols;
        }

        public void AddOrUpdateOptionSymbols(string TradingSymbol, string Exchange)
        {
            if(!m_optionSymbols.ContainsKey(TradingSymbol))
            {
                m_optionSymbols.Add(TradingSymbol, Exchange);
            }
        }

        public void AddOrUpdateOrderType(string TradingSymbol, string OrderType)
        {
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    symbolWiseOrderInfo[TradingSymbol].m_OrderType = OrderType;
                }
                else
                {
                    OpenOrderInfo orderInfo = new OpenOrderInfo();
                    orderInfo.m_OrderType = OrderType;
                    symbolWiseOrderInfo.Add(TradingSymbol, orderInfo);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
            }
        }
        public void AddOrUpdateMarginLimit(string TradingSymbol, double marginLimit)
        {
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    symbolWiseOrderInfo[TradingSymbol].m_MarginLimit = marginLimit;
                }
                else
                {
                    OpenOrderInfo orderInfo = new OpenOrderInfo();
                    orderInfo.m_MarginLimit = marginLimit;
                    symbolWiseOrderInfo.Add(TradingSymbol, orderInfo);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
            }
        }

        public bool AddOrUpdateCompletionList(string TradingSymbol, string Symbol,bool value)
        {
            bool isAddOrUpdate = false;
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    symbolWiseOrderInfo[TradingSymbol].m_IsComplete = value;
                    isAddOrUpdate = true;
                }
                else
                {
                    OpenOrderInfo orderInfo = new OpenOrderInfo();
                    orderInfo.m_IsComplete = value;
                    symbolWiseOrderInfo.Add(TradingSymbol, orderInfo);
                    isAddOrUpdate = true;
                }

                if (symbolWiseOrderInfo.ContainsKey(Symbol))
                {
                    symbolWiseOrderInfo[Symbol].m_IsComplete = value;
                    isAddOrUpdate = true;
                }
                else
                {
                    OpenOrderInfo orderInfo = new OpenOrderInfo();
                    orderInfo.m_IsComplete = value;
                    symbolWiseOrderInfo.Add(Symbol, orderInfo);
                    isAddOrUpdate = true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
                isAddOrUpdate = false;
            }
            return isAddOrUpdate;
        }

        public bool GetCycleCompletionFlag(string TradingSymbol)
        {
            bool symbol = false;
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    symbol = symbolWiseOrderInfo[TradingSymbol].m_IsComplete;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
            }
            return symbol;
        }

        public void AddOrUpdateNoOfLotSize(string TradingSymbol, int noOfLotSize)
        {
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    symbolWiseOrderInfo[TradingSymbol].m_NoOfLotSize = noOfLotSize;
                }
                else
                {
                    OpenOrderInfo orderInfo = new OpenOrderInfo();
                    orderInfo.m_NoOfLotSize = noOfLotSize;
                    symbolWiseOrderInfo.Add(TradingSymbol, orderInfo);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
            }
        }
        public void AddOrUpdateQuantityMultiplier(string TradingSymbol, double quantity)
        {
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    symbolWiseOrderInfo[TradingSymbol].m_QuantityMultiplier = quantity;
                }
                else
                {
                    OpenOrderInfo orderInfo = new OpenOrderInfo();
                    orderInfo.m_QuantityMultiplier = quantity;
                    symbolWiseOrderInfo.Add(TradingSymbol, orderInfo);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
            }
        }

        public double GetQuantityMultiplier(string TradingSymbol)
        {
            double QuantityMultiplier = 0;
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    QuantityMultiplier = symbolWiseOrderInfo[TradingSymbol].m_QuantityMultiplier;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
            }
            return QuantityMultiplier;
        }

        public int GetNoOfLotSize(string TradingSymbol)
        {
            int noOfLotSize = 0;
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    noOfLotSize = symbolWiseOrderInfo[TradingSymbol].m_NoOfLotSize;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
            }
            return noOfLotSize;
        }

        public void ResetStorage()
        {
           
            try
            {
                symbolWiseOrderInfo.Clear();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
            }
        }
        public Dictionary<string, string> GetOriginalSymbols()
        {
            return m_originalSymbols;
        }

        public void AddOrUpdateOriginalSymbols(string TradingSymbol, string Exchange)
        {
            if (!m_originalSymbols.ContainsKey(TradingSymbol))
            {
                m_originalSymbols.Add(TradingSymbol, Exchange);
            }
        }
        public void AddOrUpdateStopLoss(string TradingSymbol, double stoploss)
        {
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    symbolWiseOrderInfo[TradingSymbol].m_StopLoss = stoploss;
                }
                else
                {
                    OpenOrderInfo orderInfo = new OpenOrderInfo();
                    orderInfo.m_StopLoss = stoploss;
                    symbolWiseOrderInfo.Add(TradingSymbol, orderInfo);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
            }
        }

        public double GetStopLoss(string TradingSymbol)
        {
            double StopLoss = 0;
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    StopLoss = symbolWiseOrderInfo[TradingSymbol].m_StopLoss;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
            }
            return StopLoss;
        }

        public void AddOrUpdateTrasactionType(string TradingSymbol, string Type)
        {
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    symbolWiseOrderInfo[TradingSymbol].m_TransactionType = Type;
                }
                else
                {
                    OpenOrderInfo orderInfo = new OpenOrderInfo();
                    orderInfo.m_TransactionType = Type;
                    symbolWiseOrderInfo.Add(TradingSymbol, orderInfo);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
            }
        }

        public string GetTrasactionType(string TradingSymbol)
        {
            string type = "NA";
            try
            {
                if (symbolWiseOrderInfo.ContainsKey(TradingSymbol))
                {
                    type = symbolWiseOrderInfo[TradingSymbol].m_TransactionType;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception = " + e.Message);
            }
            return type;
        }

    }
}
