﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using IRDSAlgoOMS;
using IRDSStategyExecutor;
using IRDSStrategyExecutor.GUI;

namespace IntellirichOHLSystem
{
    public partial class form_autoTrading : Form
    {
        private IRDSAlgoOMS.AlgoOMS AlgoOMS = new IRDSAlgoOMS.AlgoOMS();
        AutomationWithFile automationWithFile;
        EMAAutomation eMAAutomation;
        Automation automation;
        bool login = false;
        bool automationStart = false;
        Logger logger = Logger.Instance;
        string userID = " ";
        ReadSettings Settings;
        bool isEMAstrategySelect = false;
        bool isChanbreakstrategySelect = false;
        bool isOpenHighStrategySelect = false;
        public bool isChanbreakStopped = false;
        public bool isOpenHighStopped = false;
        string m_BrokerName = "Zerodha";

        public delegate void UpdateControlsDelegate();

        public form_autoTrading()
        {
            InitializeComponent();
            string path = Directory.GetCurrentDirectory();
            path += @"\Configuration\Setting.ini";
            INIFile iNIFile = new INIFile(path);
            m_BrokerName = GetBroker(iNIFile);
            if(m_BrokerName == "Zerodha")
            {
                //logger.LogMessage("In constructor", MessageType.Informational);
                path = Directory.GetCurrentDirectory();
                // string iniFile = path + @"\Configuration\" + "SamcoSettings.ini";
                string iniFile = path + @"\Configuration\" + "APISetting.ini";
                if (File.Exists(iniFile))
                {
                    Settings = new ReadSettings(logger, AlgoOMS);
                    userID = Settings.readUserId(iniFile);
                }
                else
                {
                    MessageBox.Show("INI file does not exit!!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }               
                AlgoOMS.SetBroker(0);
                AlgoOMS.CreateKiteConnectObject(userID, "APISetting.ini",true);
                //AlgoOMS.CreateKiteConnectObject(userID, "SamcoSettings.ini");
            }
            else if(m_BrokerName == "Samco")
            {
                //logger.LogMessage("In constructor", MessageType.Informational);
                path = Directory.GetCurrentDirectory();
                // string iniFile = path + @"\Configuration\" + "SamcoSettings.ini";
                string iniFile = path + @"\Configuration\" + "SamcoSettings.ini";
                if (File.Exists(iniFile))
                {
                    Settings = new ReadSettings(logger, AlgoOMS);
                    userID = Settings.readUserId(iniFile);
                }
                else
                {
                    MessageBox.Show("INI file does not exit!!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                AlgoOMS.SetBroker(1);
                AlgoOMS.CreateKiteConnectObject(userID, "SamcoSettings.ini",true);
                //AlgoOMS.CreateKiteConnectObject(userID, "SamcoSettings.ini");
            }
        }

        static void OnNetworkAvailabilityChanged(object sender, NetworkAvailabilityEventArgs e)
        {
            if (e.IsAvailable)
            {
                Console.WriteLine("Network has become available");
            }
            else
            {
                bool flag = false;
                try
                {
                    using (var client = new WebClient())
                    using (client.OpenRead("http://clients3.google.com/generate_204"))
                    {
                        flag = true;
                    }
                }
                catch
                {
                    flag = false;
                }
                if(!flag)
                {
                    MessageBox.Show("Please check internet connection!!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                
            }
        }

        private void form_autoTrading_Load(object sender, EventArgs e)
        {
            try
            {

                //automationWithFile.loadObject(this);
                NetworkChange.NetworkAvailabilityChanged +=
                 new NetworkAvailabilityChangedEventHandler(OnNetworkAvailabilityChanged);
                if (m_BrokerName == "Zerodha")
                {
                    var path = Directory.GetCurrentDirectory();
                    string iniFile = path + @"\Configuration\" + "APISetting.ini";
                    //string iniFile = path + @"\Configuration\" + "SamcoSettings.ini";
                    if (File.Exists(iniFile))
                    {
                        Settings = new ReadSettings(logger, AlgoOMS);
                        userID = Settings.readUserId(iniFile);
                    }
                    else
                    {
                        MessageBox.Show("INI file does not exit!!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                else if(m_BrokerName == "Samco")
                {
                    var path = Directory.GetCurrentDirectory();
                    string iniFile = path + @"\Configuration\" + "SamcoSettings.ini";
                    //string iniFile = path + @"\Configuration\" + "SamcoSettings.ini";
                    if (File.Exists(iniFile))
                    {
                        Settings = new ReadSettings(logger, AlgoOMS);
                        userID = Settings.readUserId(iniFile);
                    }
                    else
                    {
                        MessageBox.Show("INI file does not exit!!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }

                loginToAccount();
                //Thread.Sleep(1000 * 30);
                //StartAutomation();
            }
            catch (Exception er)
            {
                Trace.WriteLine("exception from form load : " + er.Message);
            }
        }

        public void loginToAccount()
        {            
            if (ChanBreakStrategy.Checked)
                isChanbreakstrategySelect = true;
            if (OpenHighStrategy.Checked)
                isOpenHighStrategySelect = true;
            if (EMAStrategy.Checked)
                isEMAstrategySelect = true;


            if (isChanbreakstrategySelect == false && isOpenHighStrategySelect == false && isEMAstrategySelect == false)
            {
                MessageBox.Show("Please Select strategy first", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                btnLogin.Enabled = false;
                OpenHighStrategy.Enabled = false;
                ChanBreakStrategy.Enabled = false;
                EMAStrategy.Enabled = false;
                Cursor.Current = Cursors.WaitCursor;
                string error = "";
                int loginCount = 0;
                try
                {
                    logger.LogMessage("Clicked on login button", MessageType.Informational);
                    AlgoOMS.Connect(userID);
                    while (loginCount < 3)
                    {
                        if (AlgoOMS.kiteWrapperConnect != null)
                        {
                            if (AlgoOMS.GetLoginStatus(userID))
                            {
                                login = true;
                                if (isEMAstrategySelect)
                                {
                                    eMAAutomation = new EMAAutomation(AlgoOMS, logger, userID);

                                    eMAAutomation.loadObject(this);

                                    eMAAutomation.readTradingSymbol(Settings);
                                    //eMAAutomation.loadOpenPositions(userID, AlgoOMS, logger);
                                }
                                if (isChanbreakstrategySelect)
                                {
                                    automationWithFile = new AutomationWithFile(AlgoOMS, logger, userID);

                                    automationWithFile.loadObject(this);

                                    automationWithFile.readTradingSymbol(Settings);
                                    //automationWithFile.loadOpenPositions(userID, AlgoOMS, logger);

                                }
                                if (isOpenHighStrategySelect)
                                {
                                    automation = new Automation();

                                    automation.loadObject(this);
                                    automation.readTradingSymbol(Settings);
                                    //automation.loadOpenPositions(userID, AlgoOMS, logger);

                                }
                                break;
                            }
                            else
                            {
                                login = false;
                                logger.LogMessage("Not able to login trying " + loginCount, MessageType.Informational);
                                //AlgoOMS.ForeceFulRelogin(userID);
                            }
                        }
                        loginCount++;
                    }                    

                }
                catch (Exception er)
                {
                    error = er.Message;
                    if (error.Contains("empty") && error.Contains("api_key") && error.Contains("access_token"))
                    {
                        logger.LogMessage("Error : " + er.Message, MessageType.Exception);
                        MessageBox.Show("Error : Please check account credentials", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        logger.LogMessage("Error : " + er.Message, MessageType.Exception);
                        MessageBox.Show("Error : " + er.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                finally
                {
                    if (error.Contains("System.Data.SQLite"))
                    {
                        MessageBox.Show("Please add dll for sqlite", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        logger.LogMessage("Please add dll for sqlite", MessageType.Informational);
                        //bool res = automation.StopThread();
                        this.Close();
                    }
                }                
                Cursor.Current = Cursors.Default;
            }
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            loginToAccount();
            if (!login)
            {
                MessageBox.Show("Not able to login.Please try with forcefully login!!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                logger.LogMessage("Not able to login.Please try with forcefully login", MessageType.Informational);
            }
            else if (login)
            {
                //IRDSPM::Pratiksha::29-07-2020::Change the title
                MessageBox.Show("Logged in successfully!!", "IRDS Also Trader", MessageBoxButtons.OK, MessageBoxIcon.Information);
                logger.LogMessage("Logged on successfully", MessageType.Informational);
            }
        }


        private void btnStart_Click(object sender, EventArgs e)
        {
            StartAutomation();  
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    const string FileName = @"../../../Chanbreak.bin";
            //    IRDSStategyExecutor.demo d = new IRDSStategyExecutor.demo();
            //    if (File.Exists(FileName))
            //    {
            //        Console.WriteLine("Reading saved file");
            //        Stream openFileStream = File.OpenRead(FileName);
            //        if (openFileStream.Length != 0)
            //        {
            //            BinaryFormatter deserializer = new BinaryFormatter();
            //            d = (IRDSStategyExecutor.demo)deserializer.Deserialize(openFileStream);
            //        }
            //        openFileStream.Close();
            //    }
            //    return;
            //}
            //catch(Exception er)
            //{

            //}
            try
            {               
                logger.LogMessage("Clicked on stop button", MessageType.Informational);
                if (login)
                {
                    if(automationStart)
                    {
                        bool res = false;
                        if (isChanbreakstrategySelect)
                        {
                            res = automationWithFile.StopThread();                           
                        }
                        if (isEMAstrategySelect)
                        {
                            res = eMAAutomation.StopThread();
                        }
                        if (isOpenHighStrategySelect)
                        {
                            res = automation.StopThread();                           
                        }
                        if (res)
                        {
                            //IRDSPM::Pratiksha::29-07-2020::Change the title
                            MessageBox.Show("Automation stopped", "IRDS Algo Trader", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            logger.LogMessage("Automation stopped", MessageType.Informational);
                            automationStart = false;
                            btnStart.Enabled = true;
                            ChanBreakStrategy.Enabled = true;
                            OpenHighStrategy.Enabled = true;
                        }
                        else
                        {
                            MessageBox.Show("Unable to stop automation", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            logger.LogMessage("Unable to stop automation", MessageType.Informational);
                        }

                    }
                    else
                    {
                        MessageBox.Show("To perform stop. first Start the automation","Information",MessageBoxButtons.OK,MessageBoxIcon.Information);
                        logger.LogMessage("To perform stop. first Start the automation", MessageType.Informational);
                    }
                }
                else
                {
                    MessageBox.Show("Stop operation not able to perform","Warning",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                    logger.LogMessage("Stop operation not able to perform", MessageType.Informational);
                }
            }
            catch(Exception er)
            {
                MessageBox.Show("Error : " + er.Message,"Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
                logger.LogMessage("Error : " + er.Message, MessageType.Informational);
            }
        }

       

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            logger.LogMessage("clicked on refresh button  ", MessageType.Informational);
            if (login)
            {
                try
                {
                    if (isChanbreakstrategySelect)
                    {
                        automationWithFile.readTradingSymbol(Settings);
                        //automationWithFile.loadOpenPositions(userID, AlgoOMS);
                    }
                    if (isOpenHighStrategySelect)
                    {
                        automation.readTradingSymbol(Settings);
                        //automation.loadOpenPositions(userID, AlgoOMS);
                    }
                }
                catch (Exception ew)
                {
                    logger.LogMessage("Exception from clicking refresh button : " + ew.Message, MessageType.Exception);
                }
            }
            else
            {
                logger.LogMessage("Please login first to check open positions", MessageType.Informational);
                MessageBox.Show("Please login first to check open positions", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            Cursor.Current = Cursors.Default;
        }

        public void enableButton()
        {           
            InvokeUpdateControls();
        }

        public void InvokeUpdateControls()
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new UpdateControlsDelegate(UpdateControls));
            }
            else
            {
                UpdateControls();
            }
        }

        private void UpdateControls()
        {
            if(isChanbreakStopped && isOpenHighStopped)
            {
                btnStart.Enabled = true;
            }            
        }
        

        private void ChanBreakStrategy_CheckedChanged_1(object sender, EventArgs e)
        {
            isChanbreakstrategySelect = true;
        }

        private void OpenHighStrategy_CheckedChanged(object sender, EventArgs e)
        {
            isOpenHighStrategySelect = true;
        }

        private void btnStopCloseOrder_Click(object sender, EventArgs e)
        {
            try
            {
                logger.LogMessage("Clicked on stop automation and close all order button", MessageType.Informational);
                if (login)
                {
                    if (automationStart)
                    {
                        bool res = false;
                        if (isEMAstrategySelect)
                        {
                            res = eMAAutomation.StopThread();
                            eMAAutomation.CloseAllOrder();
                        }
                        if (isChanbreakstrategySelect)
                        {
                            AlgoOMS.AddNotificationInQueue(userID, "Client : " + userID + "\nAutomation stopped and closed open orders. Clicked on Stop Automation and close all orders button\n"+DateTime.Now);
                            res = automationWithFile.StopThread();
                            automationWithFile.CloseAllOrder();
                        }
                        if (isOpenHighStrategySelect)
                        {
                            res = automation.StopThread();
                            automation.CloseAllOrder();
                        }
                        if (res)
                        {
                            //IRDSPM::Pratiksha::29-07-2020::Change the title
                            MessageBox.Show("Automation stopped", "IRDS Algo Trader", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            logger.LogMessage("Automation stopped", MessageType.Informational);
                            automationStart = false;
                            btnStart.Enabled = true;
                            ChanBreakStrategy.Enabled = true;
                            OpenHighStrategy.Enabled = true;
                            EMAStrategy.Enabled = true;
                        }
                        else
                        {
                            MessageBox.Show("Unable to stop automation", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            logger.LogMessage("Unable to stop automation", MessageType.Informational);
                        }

                    }
                    else
                    {
                        MessageBox.Show("To perform stop. first Start the automation", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        logger.LogMessage("To perform stop. first Start the automation", MessageType.Informational);
                    }
                }
                else
                {
                    MessageBox.Show("Stop operation not able to perform", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    logger.LogMessage("Stop operation not able to perform", MessageType.Informational);
                }
            }
            catch (Exception er)
            {
                MessageBox.Show("Error : " + er.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                logger.LogMessage("Error : " + er.Message, MessageType.Informational);
            }
        }

        public void DownloadData()
        {
            logger.LogMessage("Started Download", MessageType.Informational);
            var path = Directory.GetCurrentDirectory();
            string iniFile = path + @"\Configuration\";
            if (isChanbreakstrategySelect)
            {                
                Settings.readTradingSymbols(iniFile + "Chan_Sandip.ini");
                Settings.readDayLimit(iniFile + "Chan_Sandip.ini");
                AlgoOMS.DownloadData(userID, Settings.TradingsymbolList, Settings.daysForHistroricalData);
            }
            if (isEMAstrategySelect)
            {
                Settings.readTradingSymbols(iniFile + "EMA_Sandip.ini");
                Settings.readDayLimit(iniFile + "EMA_Sandip.ini");
                AlgoOMS.DownloadData(userID, Settings.TradingsymbolList, Settings.daysForHistroricalData);
            }
        }

        private void btnHistorical_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                if (login)
                {
                    
                    Cursor.Current = Cursors.Default;     
                    DownloadData();
                    logger.LogMessage("Stopped Download", MessageType.Informational);
                    //IRDSPM::Pratiksha::29-07-2020::Change the title
                    MessageBox.Show("Successfully Downloaded", "IRDS Algo Trader", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    //else if(isOpenHighStrategySelect)
                    //{
                    //    Settings.readTradingSymbols(iniFile + "OHL_Sandip.ini");
                    //    Settings.readDayLimit(iniFile + "Chan_Sandip.ini");
                    //    AlgoOMS.DownloadData(userID,Settings.TradingsymbolList);
                    //}
                }
                else
                {
                    logger.LogMessage("Please login first to download data", MessageType.Informational);
                    MessageBox.Show("Please login first to download data", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch(Exception ex)
            {
                logger.LogMessage("error in  btnHistorical_Click "+ex.Message, MessageType.Informational);
            }
        }

        private void EMAStrategy_CheckedChanged(object sender, EventArgs e)
        {
            isEMAstrategySelect = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AlgoOMS.AddNotificationInQueue(userID, "Client : " + userID + "\nExe closed.\n"+DateTime.Now);
            this.Close();
            //form_autoTrading_FormClosing(sender,e);
        }

        //sanika::25-sep-2020::added for resume testing
        public OpenOrderInfo GetObj()
        {
            if(automationWithFile != null)
                return automationWithFile.m_OrderInfo;
            return null;
        }

        //sanika::25-sep-2020::added for resume testing
        public void SaveBin()
        {
            var path = Directory.GetCurrentDirectory();
            string FileName = path+ @"\\Chanbreak.bin";//sanika::12-Feb-2021::changed file name
            Stream SaveFileStream = File.Create(FileName);
            BinaryFormatter serializer = new BinaryFormatter();
            //sanika::22-dec-2020::Added for exception
            OpenOrderInfo openOrderInfo =  GetObj();
            if(openOrderInfo != null)
                serializer.Serialize(SaveFileStream, openOrderInfo);
            SaveFileStream.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            PipeServer pipe = new PipeServer();
            pipe.serverStartMethod();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //PipeClient pipe = new PipeClient();
            PipeServer pipe = new PipeServer();
            pipe.StartClientMethod();
        }

        private void form_autoTrading_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                string message = "Do you really want to exit from application?";
                //IRDSPM::Pratiksha::29-07-2020::Change the title
                string title = "IRDS Algo Trader";
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                DialogResult dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                if (dialog == DialogResult.Yes)
                {
                    logger.LogMessage("Message box display and clicked on yes!!", MessageType.Informational);
                    //sanika::25-sep-2020::added for resume testing
                    SaveBin();
                    logger.LogMessage("clicked on close form", MessageType.Informational);
                    if (login)
                    {
                        bool res = false;
                        if (AlgoOMS.stopThread())
                        {
                            logger.LogMessage("All threads are closed", MessageType.Informational);
                        }
                        if (isChanbreakstrategySelect)
                        {
                            //automationWithFile.SaveBin();
                            res = automationWithFile.StopThread();
                            if (res)
                            {
                                logger.LogMessage("Chanbreak threads are closed", MessageType.Informational);

                            }
                            else
                                logger.LogMessage("Threads are not able to close", MessageType.Informational);
                        }
                        if (isOpenHighStrategySelect)
                        {
                            res = automation.StopThread();
                            if (res)
                            {
                                logger.LogMessage("Open-High threads are closed", MessageType.Informational);
                                Environment.Exit(0);
                            }
                            else
                                logger.LogMessage("Threads are not able to close", MessageType.Informational);
                        }
                        Environment.Exit(0);
                    }
                }
                else
                {
                    e.Cancel = true;
                    logger.LogMessage("Message box display and clicked on No!!", MessageType.Informational);
                }

            }
            catch (Exception er)
            {
                MessageBox.Show("Error : " + er.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                logger.LogMessage("Error : " + er.Message, MessageType.Exception);
            }
        }

        string GetBroker(INIFile iNIFile)
        {
            string brokerName = "";
            try
            {
                brokerName = iNIFile.IniReadValue("BROKER", "brokername");
            }
            catch (Exception e)
            {
                Trace.WriteLine("exception " + e.Message);
            }
            if (brokerName == "")
            {
                brokerName = "Zerodha";
            }
            return brokerName;
        }

        private void btnModifyOrder_Click(object sender, EventArgs e)
        {
            if(automationStart)
            {
                if(automationWithFile != null)
                {
                    frmStopLossPercent frmStopLossPercent = new frmStopLossPercent(automationWithFile);
                    frmStopLossPercent.Show();
                }
            }
            else
            {
                MessageBox.Show("Please start automation first to modify orders", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void StartAutomation()
        {
            btnStart.Enabled = false;
            ChanBreakStrategy.Enabled = false;
            OpenHighStrategy.Enabled = false;
            EMAStrategy.Enabled = false;
            try
            {
                logger.LogMessage("Clicked on start button", MessageType.Informational);
                if (login)
                {
                    automationStart = true;
                    if (isEMAstrategySelect)
                    {
                        DownloadData();
                        eMAAutomation.loadObject(this);
                        bool res = eMAAutomation.startThread(userID);
                        if (!res)
                        {
                            btnStart.Enabled = true;
                            MessageBox.Show("Some values are missing in TRADINGSYMBOL section.. Please check ini file", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }
                    }
                    if (isChanbreakstrategySelect)
                    {
                        //sanika::10-dec-2020::Added check to auto download historical data
                        var path = Directory.GetCurrentDirectory();
                        string iniFile = path + @"\Configuration\";
                        bool autoDownload = Settings.ReadAutoDownloadFlag(iniFile + "Chan_Sandip.ini");
                        if (autoDownload)
                        {
                            logger.LogMessage("autoDownload flag for historical data is true", MessageType.Informational);
                            DownloadData();
                        }
                        else
                        {
                            logger.LogMessage("autoDownload flag for historical data is false", MessageType.Informational);
                        }
                        automationWithFile.loadObject(this);
                        bool res = automationWithFile.startThread(userID);                        
                        if (!res)
                        {
                            btnStart.Enabled = true;
                            MessageBox.Show("Some values are missing in TRADINGSYMBOL section.. Please check ini file", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }

                    }
                    if (isOpenHighStrategySelect)
                    {
                        bool res = automation.startThread(userID);
                        if (!res)
                        {
                            btnStart.Enabled = true;
                            MessageBox.Show("Some values are missing in TRADINGSYMBOL section.. Please check ini file", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }
                    }
                }
                else
                {
                    if (isOpenHighStrategySelect == false && isChanbreakstrategySelect == false && isEMAstrategySelect == false)
                    {
                        MessageBox.Show("Please Select strategy first", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        btnStart.Enabled = true;
                        ChanBreakStrategy.Enabled = true;
                        OpenHighStrategy.Enabled = true;
                        EMAStrategy.Enabled = true;
                    }
                    else
                    {
                        AlgoOMS.CreateKiteConnectObject(userID, "APISetting.ini",true);
                        login = true;
                        btnStart.Enabled = false;
                        // MessageBox.Show("Login successfully!!", "Successful", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        logger.LogMessage("Login successfully", MessageType.Informational);
                        automationStart = true;
                        btnLogin.Enabled = false;
                        if (isChanbreakstrategySelect)
                        {
                            automationWithFile = new AutomationWithFile(AlgoOMS, logger, userID);
                            automationWithFile.loadObject(this);
                            automationWithFile.readTradingSymbol(Settings);
                            //automationWithFile.loadOpenPositions(userID, AlgoOMS, logger);
                            bool res = automationWithFile.startThread(userID);
                            if (!res)
                            {
                                btnStart.Enabled = true;
                                ChanBreakStrategy.Enabled = true;
                                OpenHighStrategy.Enabled = true;
                                EMAStrategy.Enabled = true;
                                MessageBox.Show("Some values are missing in TRADINGSYMBOL section.. Please check ini file", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            }
                        }
                        if (isOpenHighStrategySelect)
                        {
                            automation = new Automation();
                            automation.loadObject(this);

                            automation.readTradingSymbol(Settings);
                            //automation.loadOpenPositions(userID, AlgoOMS, logger);
                            bool res = automation.startThread(userID);
                            if (!res)
                            {
                                btnStart.Enabled = true;
                                ChanBreakStrategy.Enabled = true;
                                OpenHighStrategy.Enabled = true;
                                EMAStrategy.Enabled = true;
                                MessageBox.Show("Some values are missing in TRADINGSYMBOL section.. Please check ini file", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            }
                        }
                    }
                }
            }
            catch (Exception er)
            {
                MessageBox.Show("Error : " + er.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                logger.LogMessage("Error : " + er.Message, MessageType.Exception);
            }
        }

        private void tradingSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Logger logger = Logger.Instance;
            TradingSetting obj = new TradingSetting(logger, AlgoOMS);
            obj.ShowDialog();
        }

        private void symbolSettingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IRDSStrategyExecutor.GUI.SymbolSetting obj = new IRDSStrategyExecutor.GUI.SymbolSetting(AlgoOMS, Settings);
            obj.ShowDialog();
        }
    }
}
