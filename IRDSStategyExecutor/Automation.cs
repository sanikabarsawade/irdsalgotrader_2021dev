﻿using IRDSAlgoOMS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace IntellirichOHLSystem
{
    class Automation
    {
        Thread startAutomation = null;
        ReadSettings readSettings;
        int OrderCount = 1;
        int MaxOrderCount = 0;
        int IndividualOrderCount = 0;
        string StartTime = "";
        int Interval = 0;
        string FirstTickEnd = "";
        string End = "";
        string TimeForExitAllOpenPosition = "";
        string TimeLimitToPlaceOrder = "";
#pragma warning disable CS0649 // Field 'Automation.AlgoOMS' is never assigned to, and will always have its default value null
        AlgoOMS AlgoOMS;
#pragma warning restore CS0649 // Field 'Automation.AlgoOMS' is never assigned to, and will always have its default value null
#pragma warning disable CS0649 // Field 'Automation.logger' is never assigned to, and will always have its default value null
        Logger logger;
#pragma warning restore CS0649 // Field 'Automation.logger' is never assigned to, and will always have its default value null
        string userID = "";
        string resForPlaceBOLimitOrder;
        decimal Slippage = 0;
        Dictionary<string, int> dictionaryForPlacedOrder = new Dictionary<string, int>();
        Dictionary<string, int> dictionaryForPlacedOrderCounter = new Dictionary<string, int>();
        public List<TradingSymbolInfo> ListOfTradingSymbolsInfo; 
        static string path = Directory.GetCurrentDirectory();
        string iniFile = path + @"\Configuration\" + "OHL_Sandip.ini";
        form_autoTrading form_Auto;
        double OverallProfit = 0;
        double OverallLoss = 0;
        OpenOrderInfo m_OpenOrderInfo;
        public Automation()
        {
            m_OpenOrderInfo = new OpenOrderInfo();
        }
        public bool startThread(string userID)
        {
            logger.LogMessage("In startThread", MessageType.Informational);
            if (File.Exists(iniFile))
            {
                if (readSettings != null)
                    readSettings = new ReadSettings(logger,AlgoOMS);
                bool res = readSettings.readOHL_SandipConfigFile(iniFile);
                if (!res)
                {
                    return res;
                }
            }
            if (startAutomation == null)
            {
                logger.LogMessage("Strating StartAutomation thread", MessageType.Informational);
                startAutomation = new Thread(() => StartAutomation(userID));
                startAutomation.Start();
            }
            return true;
        }

        public void StartAutomation(string userID)
        {
            bool isStopThread = false;
            try
            {
                loadINIValues();
                CheckTradingSymbolPresentOrNot();
                while (true)
                {
                    string TickCurrentTime = AlgoOMS.getTickCurrentTime();
                    if (TickCurrentTime == "")
                    {
                        continue;
                    }
                    if (calculateProfit())
                    {
                        isStopThread = true;
                        //loadOpenPositions();
                        StopThread();
                        break;
                    }
                    Thread.Sleep(1000);
                    if (TimeSpan.Parse(TickCurrentTime) >= TimeSpan.Parse(TimeForExitAllOpenPosition))
                    {
                        logger.LogMessage("condition true for close all position", MessageType.Informational);
                        AlgoOMS.CloseAllOrder(userID);
                    }
                    else
                    {
                        if ((TimeSpan.Parse(TickCurrentTime) >= TimeSpan.Parse(FirstTickEnd)) && (TimeSpan.Parse(TickCurrentTime) < TimeSpan.Parse(End)))
                        {
                            if (OrderCount <= MaxOrderCount)
                            {
                                foreach (var symbolInfo in ListOfTradingSymbolsInfo)
                                {
                                    int Quantity = Convert.ToInt32(symbolInfo.getQuantity());
                                    string TradingSymbol = symbolInfo.getSymbol();
                                    decimal VolumeLimit = symbolInfo.getVolume();
                                    double ProfitPercent = symbolInfo.getProfitPercent();
                                    double StopLoss = symbolInfo.getStopLoss();
                                    string Exchange = symbolInfo.getExchange();
                                    decimal Difference1 = symbolInfo.getDifference1();
                                    decimal Difference2 = symbolInfo.getDifference2();

                                    string symbol = TradingSymbol;
                                    if (Exchange == "NFO")
                                    {
                                        TradingSymbol = this.AlgoOMS.GetFutureSymbolName(userID, TradingSymbol);
                                    }

                                    List<string> OHLCList = this.AlgoOMS.getOHCLValues(TradingSymbol);
                                    if (OHLCList == null)
                                    {
                                        WriteUniquelogs(TradingSymbol + " " + userID, "ohlc list count is 0", MessageType.Informational);
                                        continue;
                                    }
                                    

                                    decimal open = Convert.ToDecimal(OHLCList[2]);
                                    decimal high = Convert.ToDecimal(OHLCList[3]);
                                    decimal low = Convert.ToDecimal(OHLCList[4]);
                                    decimal close = Convert.ToDecimal(OHLCList[5]);
                                    decimal volume = Convert.ToDecimal(OHLCList[6]);

                                    decimal smallValue = ((open) * Slippage) / 100;

                                    WriteUniquelogs(TradingSymbol + " " + userID, "Slippage " + Slippage + " smallValue " + smallValue, MessageType.Informational);
                                    WriteUniquelogs(TradingSymbol + " " + userID, "Difference1 " + Difference1 + " Difference2 " + Difference2, MessageType.Informational);

                                    decimal percentage1 = (open * Difference1) / 100;
                                    decimal percentage2 = (open * Difference2) / 100;

                                    decimal OpenMinusClose = open - close;
                                    decimal CloseMinusOpen = close - open;
                                    WriteUniquelogs(TradingSymbol + " " + userID, " percentage1  " + percentage1 + " OpenMinusClose " + OpenMinusClose + " CloseMinusOpen " + CloseMinusOpen + " percentage2 " + percentage2, MessageType.Informational);

                                    double calculatedOpenBuy = Convert.ToDouble(AlgoOMS.RoundOff(userID, TradingSymbol, (open - smallValue), true));
                                    double calculatedOpenSell = Convert.ToDouble(AlgoOMS.RoundOff(userID, TradingSymbol, (open + smallValue), false));
                                    double roundedLow = Convert.ToDouble(AlgoOMS.RoundOff(userID, TradingSymbol, low, true));
                                    double roundedHigh = Convert.ToDouble(AlgoOMS.RoundOff(userID, TradingSymbol, high, false));

                                    WriteUniquelogs(TradingSymbol + " " + userID, " calculatedOpenBuy  " + calculatedOpenBuy + " calculatedOpenSell " + calculatedOpenSell, MessageType.Informational);
                                    WriteUniquelogs(TradingSymbol + " " + userID, " roundedLow  " + roundedLow + " roundedHigh " + roundedHigh, MessageType.Informational);

                                    double lastPrice = AlgoOMS.GetLastPrice(userID, TradingSymbol);
                                    int individualOrderCount = 0;
                                    if (dictionaryForPlacedOrderCounter.ContainsKey(TradingSymbol))
                                    {
                                        individualOrderCount = dictionaryForPlacedOrderCounter[TradingSymbol];
                                    }                                    

                                    if ((calculatedOpenBuy <= roundedLow) && (volume >= VolumeLimit) && (OrderCount <= MaxOrderCount) && (CloseMinusOpen >= percentage1) && (CloseMinusOpen <= percentage2) && (individualOrderCount < IndividualOrderCount))
                                    {
                                        if ((TimeSpan.Parse(TickCurrentTime) <= TimeSpan.Parse(TimeLimitToPlaceOrder)))
                                        {
                                            bool res = placeOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_BUY, Quantity, Convert.ToDecimal( lastPrice), Convert.ToDecimal(StopLoss),Convert.ToDecimal(ProfitPercent));
                                            if (res)
                                            {                                               
                                                //loadOpenPositions();
                                            }
                                        }
                                        else
                                        {
                                            CloseOrder(TradingSymbol, Constants.TRANSACTION_TYPE_BUY, Exchange);
                                            //loadOpenPositions();
                                        }
                                    }

                                    else if ((calculatedOpenSell >= roundedHigh) && (volume >= VolumeLimit) && (OrderCount <= MaxOrderCount) && (OpenMinusClose >= percentage1) && (OpenMinusClose <= percentage2) && (individualOrderCount < IndividualOrderCount))
                                    {
                                        if ((TimeSpan.Parse(TickCurrentTime) <= TimeSpan.Parse(TimeLimitToPlaceOrder)))
                                        {
                                            bool res = placeOrder(userID, TradingSymbol, Exchange, Constants.TRANSACTION_TYPE_SELL, Quantity, Convert.ToDecimal(lastPrice), Convert.ToDecimal(StopLoss), Convert.ToDecimal(ProfitPercent));
                                            if (res)
                                            {                                                
                                                //loadOpenPositions();
                                            }
                                        }
                                        else
                                        {
                                            CloseOrder(TradingSymbol, Constants.TRANSACTION_TYPE_SELL, Exchange);
                                            //loadOpenPositions();
                                        }
                                    }
                                    else
                                    {
                                        WriteUniquelogs(TradingSymbol + " " + userID, "condition not matched for buy or sell order of " + TradingSymbol + " or max length reached " + OrderCount, MessageType.Informational);
                                    }
                                }
                            }
                            else
                            {
                                logger.LogMessage("Maxcount reached ordercount " + OrderCount, MessageType.Informational);
                            }
                            changeTime(TickCurrentTime);
                        }
                        else
                        {
                            
                        }
                    }

                }
            }
            catch (Exception e)
            {
                logger.LogMessage("stratAutmation thread Exception " + e.Message, MessageType.Exception);
            }
            finally
            {
                if (isStopThread)
                {
                    form_Auto.isOpenHighStopped = true;
                    form_Auto.enableButton();
                }
            }            
        }

        public void loadINIValues()
        {
            try
            {
                logger.LogMessage("In loadINIValues", MessageType.Informational);
                string currentTime = DateTime.Now.ToString("HH:mm") + ":00";
                MaxOrderCount = Convert.ToInt32(readSettings.maxCount);
                IndividualOrderCount = Convert.ToInt32(readSettings.individualCount);
                Slippage = readSettings.Slippage;
                StartTime = readSettings.startTime;
                OverallLoss =Convert.ToDouble( readSettings.overallLoss);
                OverallProfit = Convert.ToDouble(readSettings.overallProfit);
                Interval = Convert.ToInt32(readSettings.interval);
                if (TimeSpan.Parse(StartTime) < TimeSpan.Parse(currentTime))
                {
                    StartTime = currentTime;
                }
                DateTime dt1 = DateTime.ParseExact(StartTime, "HH:mm:ss", null);
                FirstTickEnd = dt1.AddSeconds(60 * Interval).ToString("HH:mm:ss");
                End = dt1.AddSeconds(60 * (Interval + 1)).ToString("HH:mm:ss");
                TimeForExitAllOpenPosition = readSettings.TimeForExitAllOpenPosition;
                TimeLimitToPlaceOrder = readSettings.TimeLimitToPlaceOrder;
                ListOfTradingSymbolsInfo = readSettings.TradingSymbolsInfoList;
                logger.LogMessage("loadINIValues - End", MessageType.Informational);
            }
#pragma warning disable CS0168 // The variable 'e' is declared but never used
            catch (Exception e)
#pragma warning restore CS0168 // The variable 'e' is declared but never used
            {
                logger.LogMessage("loadINIValues - exception", MessageType.Exception);
            }
        }

        public bool StopThread()
        {
            logger.LogMessage("In Stop Thread", MessageType.Informational);
           // if (AlgoOMS.stopThread())
            {
                if (startAutomation != null)
                {
                    if (startAutomation.IsAlive)
                    {
                        logger.LogMessage("Stopped Thread", MessageType.Informational);
                        startAutomation.Abort();
                        Thread.Sleep(1000);
                        if (!startAutomation.IsAlive)
                            return true;
                    }
                }
            }
            return true;
        }

        public void CheckTradingSymbolPresentOrNot()
        {
            try
            {
                logger.LogMessage("calling CheckTradingSymbolPresentOrNot", MessageType.Informational);
                AlgoOMS.CheckTradingSymbolPresentOrNot(userID, readSettings.TradingsymbolList);
            }
            catch (Exception e)
            {
                logger.LogMessage("CheckTradingSymbolPresentOrNot exception " + e.Message, MessageType.Informational);
            }
        }

        public void readTradingSymbol(ReadSettings readSettings)
        {
            if (readSettings != null)
                this.readSettings = readSettings;
            if (readSettings.TradingsymbolList.Count == 0)
                readSettings.readOHL_SandipConfigFile(iniFile);
        }

        //public void loadOpenPositions(string user = "", AlgoOMS AlgoOMS = null, Logger logger = null)
        //{
        //    if (user != "")
        //        userID = user;
        //    if (AlgoOMS != null)
        //        this.AlgoOMS = AlgoOMS;
        //    if (logger != null)
        //        this.logger = logger;
        //    try
        //    {
        //        this.logger.LogMessage("In LoadOpenPostion", MessageType.Informational);
        //        PositionResponse positionResponseForOverall = this.AlgoOMS.GetOpenPosition(userID);
        //        if (positionResponseForOverall.Net.Count != 0)
        //        {
        //            foreach (var Position in positionResponseForOverall.Net)
        //            {
        //                if (readSettings.TradingsymbolListWithFutureName.Contains(Position.TradingSymbol))
        //                {
                            
        //                    string transactionType = "";
        //                    string price = "";
        //                    if (Position.Quantity > 0)
        //                    {
        //                        transactionType = Constants.TRANSACTION_TYPE_BUY;
        //                        price = Position.BuyPrice.ToString();
        //                    }
        //                    else if (Position.Quantity < 0)
        //                    {
        //                        transactionType = Constants.TRANSACTION_TYPE_SELL;
        //                        price = Position.SellPrice.ToString();
        //                    }                           
        //                    //var filtered = mydt.AsEnumerable().Where(r => r.Field<String>("Symbol").Contains(Position.TradingSymbol));
        //                    //if (!ListForGridView.Contains(Position.TradingSymbol))
        //                    //{
        //                    //    addIntoDataGridView(Position.TradingSymbol, transactionType, Position.Quantity.ToString(), price);
        //                    //}
        //                    //else 
        //                    //{
        //                    //    updateIntoDataGridView(Position.TradingSymbol, transactionType, Position.Quantity.ToString(), price);
        //                    //}                           
                          
        //                }
        //            }
        //            this.logger.LogMessage("LoadOpenPostion -End", MessageType.Informational);
        //        }
        //    }
        //    catch (Exception er)
        //    {
        //        this.logger.LogMessage("Exception from LoadOpenPostion" + er.Message, MessageType.Informational);
        //    }
        //}

        public bool calculateProfit()
        {          
            try
            {
                PositionResponse positionResponseForOverall = AlgoOMS.GetOpenPosition(userID);
                if (positionResponseForOverall.Net.Count != 0)
                {
                    logger.LogMessage("In calculateProfit", MessageType.Informational);
                    double overallProfit = OverallProfit;
                    double overallLoss = OverallLoss;
                    double profitLoss = 0;
                    foreach (var symbol in ListOfTradingSymbolsInfo)
                    {
                        double lastPrice = 0;
                        string tradingSymbol = symbol.getSymbol();
                        string exchange = symbol.getExchange();
                        if (exchange == "NFO")
                        {
                            tradingSymbol = AlgoOMS.GetFutureSymbolName(userID, tradingSymbol);
                        }
                        double OpenPrice = AlgoOMS.GetOpenPostionPrice(userID, tradingSymbol);
                        if (OpenPrice != 0)
                        {
                            logger.LogMessage(tradingSymbol + " OpenPrice " + OpenPrice, MessageType.Informational);
                            lastPrice = AlgoOMS.GetLastPrice(userID, tradingSymbol);
                            logger.LogMessage(tradingSymbol + " lastPrice " + lastPrice, MessageType.Informational);
                            int quantity = Convert.ToInt32(symbol.getQuantity());
                            logger.LogMessage(tradingSymbol + " quantity " + quantity, MessageType.Informational);
                            int value = 0;
                            if (dictionaryForPlacedOrder.ContainsKey(tradingSymbol))
                            {
                                value = dictionaryForPlacedOrder[tradingSymbol];
                            }                                
                            double profitOrLoss = 0; ;
                            if (value > 0)
                            {                               
                                profitOrLoss = (lastPrice - OpenPrice) * quantity;
                            }
                            else if(value < 0)
                            {
                                profitOrLoss = (OpenPrice - lastPrice) * quantity;
                            }                            
                            logger.LogMessage(tradingSymbol + " profitOrLoss " + profitOrLoss, MessageType.Informational);
                            profitLoss += profitOrLoss;
                        }
                        else
                        {
                            logger.LogMessage("OpenPrice is zero for " + tradingSymbol, MessageType.Informational);
                        }
                    }

                    if (profitLoss > overallProfit || profitLoss < overallLoss)
                    {
                        logger.LogMessage("Condition true for closeallpostion", MessageType.Informational);
                        logger.LogMessage("profitLoss " + profitLoss + " overallProfit " + overallProfit + " overallLoss " + overallLoss, MessageType.Informational);
                        foreach (var symbol in ListOfTradingSymbolsInfo)
                        {
                            string tradingSymbol = symbol.getSymbol();
                            string exchange = symbol.getExchange();
                            if (exchange == "NFO")
                            {
                                tradingSymbol = AlgoOMS.GetFutureSymbolName(userID, tradingSymbol);
                            }
                            AlgoOMS.CloseOrder(userID, tradingSymbol,exchange);
                        }
                        logger.LogMessage("closed all successfully", MessageType.Informational);
                        return true;
                    }
                    else
                    {
                        logger.LogMessage("Condition false for closeallpostion", MessageType.Informational);
                        logger.LogMessage(" profitLoss " + profitLoss + " overallProfit " + overallProfit + " overallLoss " + overallLoss, MessageType.Informational);
                        return false;
                    }
                }
            }
            catch (Exception e)
            {
                logger.LogMessage("Exception from calculateProfit " + e.Message, MessageType.Exception);
            }
            return false;
        }

        public bool placeOrder(string userID, string TradingSymbol, string Exchange, string TransactionType, int Quantity, decimal Price, decimal Stoploss, decimal ProfitPercent)
        {
            try
            {
                bool resForcheckForPreviousOrder = checkForPreviousOrder(TradingSymbol, TransactionType, Exchange);
                if (resForcheckForPreviousOrder)
                {
                    decimal stoploss = (Price * Stoploss) / 100;
                    WriteUniquelogs(TradingSymbol + " " + userID, "PlaceBOLimitOrder : stoploss After Calculating " + stoploss, MessageType.Informational);
                    decimal squareoff = (Price * ProfitPercent) / 100;
                    WriteUniquelogs(TradingSymbol + " " + userID, "PlaceBOLimitOrder : squareoff After Calculating " + squareoff, MessageType.Informational);
                    decimal calulatedPrice = 0;
                    if (TransactionType == Constants.TRANSACTION_TYPE_BUY)
                    {
                        decimal percentPrice = (Price * Convert.ToDecimal(0.5)) / 100;
                        WriteUniquelogs(TradingSymbol + " " + userID, "PlaceBOLimitOrder : percentPrice After Calculating " + percentPrice, MessageType.Informational);
                        decimal calPrice = Price + percentPrice;
                        WriteUniquelogs(TradingSymbol + " " + userID, "PlaceBOLimitOrder : calPrice After Calculating " + calPrice, MessageType.Informational);
                        calulatedPrice = Convert.ToDecimal(AlgoOMS.RoundOff(userID, TradingSymbol, calulatedPrice, true));
                        WriteUniquelogs(TradingSymbol + " " + userID, "PlaceBOLimitOrder : calulatedPrice : " + calulatedPrice, MessageType.Informational);
                    }
                    else
                    {
                        decimal percentPrice = (Price * Convert.ToDecimal(0.5)) / 100;
                        WriteUniquelogs(TradingSymbol + " " + userID, "PlaceBOLimitOrder : percentPrice After Calculating " + percentPrice, MessageType.Informational);
                        decimal calPrice = Price - percentPrice;
                        WriteUniquelogs(TradingSymbol + " " + userID, "PlaceBOLimitOrder : calPrice After Calculating " + calPrice, MessageType.Informational);
                        calulatedPrice = Convert.ToDecimal(AlgoOMS.RoundOff(userID, TradingSymbol, calulatedPrice, true));
                        WriteUniquelogs(TradingSymbol + " " + userID, "PlaceBOLimitOrder : calulatedPrice : " + calulatedPrice, MessageType.Informational);
                    }

                    //**place bo**//
                    if (resForPlaceBOLimitOrder != "NA")
                    {
                        resForPlaceBOLimitOrder = AlgoOMS.PlaceBOLimitOrder(userID, Exchange, TradingSymbol, TransactionType, Quantity, calulatedPrice, squareoff, stoploss);
                    }
                    if (resForPlaceBOLimitOrder != "NA")
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "PlaceBOLimitOrder order placed successfully", MessageType.Informational);
                        addOrUpdateDictionary(TradingSymbol, TransactionType);
                        return true;
                    }
                    else
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "PlaceBOLimitOrder order not placed", MessageType.Informational);
                        //**place market**//
                        string resForPlaceMarketOrder = AlgoOMS.PlaceMarketOrder(userID, Exchange, TradingSymbol, TransactionType, Quantity);
                        if (resForPlaceMarketOrder != "NA")
                        {
                            m_OpenOrderInfo.AddOrUpdateOrderInfo(TradingSymbol, TransactionType, resForPlaceMarketOrder);
                            WriteUniquelogs(TradingSymbol + " " + userID, "PlaceMarketOrder order placed successfully", MessageType.Informational);
                            double openPrice = 0;
                            int trial = 0;
                            while (trial < 5 && openPrice == 0)
                            {
                                openPrice = AlgoOMS.GetOpenPostionPricebyOrderID(userID, TradingSymbol, Exchange, m_OpenOrderInfo.GetOpenOrderId(TradingSymbol, TransactionType));
                                trial++;
                                Thread.Sleep(1000);
                            }
                            if (openPrice == 0)
                            {
                                openPrice = Convert.ToDouble(Price);
                                WriteUniquelogs(TradingSymbol + " " + userID, "PlaceMarketOrder :lastPrice used as price come 0 " + openPrice, MessageType.Informational);
                            }
                            WriteUniquelogs(TradingSymbol + " " + userID, "PlaceMarketOrder : openPrice " + openPrice, MessageType.Informational);
                            decimal calculatedPrice = 0;
                            decimal PercentValue = Stoploss * Convert.ToDecimal(openPrice) / 100;
                            WriteUniquelogs(TradingSymbol + " " + userID, "PlaceStopOrder : PercentValue After Calculating " + PercentValue, MessageType.Informational);
                            string TransactionTypeForStopOrder = "";
                            if (TransactionType == Constants.TRANSACTION_TYPE_BUY)
                            {
                                TransactionTypeForStopOrder = Constants.TRANSACTION_TYPE_SELL;
                            }
                            else
                            {
                                TransactionTypeForStopOrder = Constants.TRANSACTION_TYPE_BUY;
                            }
                            if (TransactionTypeForStopOrder == Constants.TRANSACTION_TYPE_BUY)
                            {
                                decimal CalPrice = Convert.ToDecimal(openPrice) + PercentValue;
                                WriteUniquelogs(TradingSymbol + " " + userID, "PlaceStopOrder :without round Trigger Price = " + CalPrice, MessageType.Informational);
                                calculatedPrice = Convert.ToDecimal(AlgoOMS.RoundOff(userID, TradingSymbol, CalPrice, true));
                                WriteUniquelogs(TradingSymbol + " " + userID, "PlaceStopOrder :Trigger Price = " + calculatedPrice, MessageType.Informational);
                            }
                            else
                            {
                                decimal CalPrice = Convert.ToDecimal(openPrice) - PercentValue;
                                WriteUniquelogs(TradingSymbol + " " + userID, "PlaceStopOrder :without round Trigger Price = " + CalPrice, MessageType.Informational);
                                calculatedPrice = Convert.ToDecimal(AlgoOMS.RoundOff(userID, TradingSymbol, CalPrice, false));
                                WriteUniquelogs(TradingSymbol + " " + userID, "PlaceStopOrder :Trigger Price = " + calculatedPrice, MessageType.Informational);
                            }

                            //**place stoporder for market**//
                            string resForPlaceStopOrder = AlgoOMS.PlaceStopOrder(userID, Exchange, TradingSymbol, TransactionTypeForStopOrder, Quantity, 0, calculatedPrice);

                            if (resForPlaceStopOrder != "NA")
                            {
                                OrderCount++;
                                if (dictionaryForPlacedOrderCounter.ContainsKey(TradingSymbol))
                                {
                                    dictionaryForPlacedOrderCounter[TradingSymbol] = dictionaryForPlacedOrderCounter[TradingSymbol] + 1;
                                }
                                else
                                {
                                    dictionaryForPlacedOrderCounter.Add(TradingSymbol, 1);
                                }
                                WriteUniquelogs(TradingSymbol + " " + userID, "individualcount"+ dictionaryForPlacedOrderCounter[TradingSymbol], MessageType.Informational);
                                WriteUniquelogs(TradingSymbol + " " + userID, "PlaceStopOrder order placed successfully", MessageType.Informational);
                                addOrUpdateDictionary(TradingSymbol, TransactionType);
                                return true;
                            }
                        }
                    }
                }
                else
                {
                    WriteUniquelogs(TradingSymbol + " " + userID, "PlaceOrder found previous order", MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + userID, "PlaceOrder Exception " + e.Message, MessageType.Exception);
            }
            return false;
        }

        public bool checkForPreviousOrder(string TradingSymbol, string TransactionType,string Exchange)
        {
            try
            {
                WriteUniquelogs(TradingSymbol + " " + userID, "In checkForPreviousOrder", MessageType.Informational);

                if (dictionaryForPlacedOrder.ContainsKey(TradingSymbol))
                {
                    if (checkClosedOrNot(TradingSymbol))
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "dictionaryForPlacedOrder contains symbol", MessageType.Informational);
                        int value = dictionaryForPlacedOrder[TradingSymbol];
                        if ((value < 0) && (TransactionType == Constants.TRANSACTION_TYPE_BUY))
                        {
                            WriteUniquelogs(TradingSymbol + " " + userID, "checkForPreviousOrder : Going to close order- opposite direction found", MessageType.Informational);
                            bool res = AlgoOMS.CloseOrder(userID, TradingSymbol, Exchange);
                            //loadOpenPositions();
                            if (res)
                                dictionaryForPlacedOrder.Remove(TradingSymbol);
                        }
                        else if ((value > 0) && (TransactionType == Constants.TRANSACTION_TYPE_BUY))
                        {
                            WriteUniquelogs(TradingSymbol + " " + userID, "checkForPreviousOrder : Same direction found for " + TradingSymbol, MessageType.Informational);
                            return false;
                        }
                        else if ((value > 0) && (TransactionType == Constants.TRANSACTION_TYPE_SELL))
                        {
                            WriteUniquelogs(TradingSymbol + " " + userID, "checkForPreviousOrder : Going to close order-  opposite direction found", MessageType.Informational);
                            bool res = AlgoOMS.CloseOrder(userID, TradingSymbol, Exchange);
                            //loadOpenPositions();
                            if (res)
                                dictionaryForPlacedOrder.Remove(TradingSymbol);

                        }
                        else if ((value < 0) && (TransactionType == Constants.TRANSACTION_TYPE_SELL))
                        {
                            WriteUniquelogs(TradingSymbol + " " + userID, "checkForPreviousOrder : Same direction found for " + TradingSymbol, MessageType.Informational);
                            return false;
                        }

                        return false;

                    }
                    else
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "dictionaryForPlacedOrder - removed symbol", MessageType.Informational);
                        dictionaryForPlacedOrder.Remove(TradingSymbol);
                        return true;
                    }
                }
                else
                {
                    WriteUniquelogs(TradingSymbol + " " + userID, "checkForPreviousOrder : dictionaryForPlacedOrder not contains symbol", MessageType.Informational);
                    return true;
                }

            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + userID, "checkForPreviousOrder : Exception " + e.Message, MessageType.Informational);
                return false;
            }
        }

        public bool CloseOrder(string TradingSymbol, String TransactionType,string Exchange)
        {
            bool res = false;
            try
            {
                if (dictionaryForPlacedOrder.ContainsKey(TradingSymbol))
                {
                    WriteUniquelogs(TradingSymbol + " " + userID, "CloseOrder : dictionaryForPlacedOrder contains symbol", MessageType.Informational);
                    int value = dictionaryForPlacedOrder[TradingSymbol];
                    if (value < 0 && TransactionType == Constants.TRANSACTION_TYPE_BUY)
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "CloseOrder : Condition true for close order", MessageType.Informational);
                        if (AlgoOMS.getVariety(TradingSymbol,Exchange) == Constants.VARIETY_BO)
                        {
                            res = AlgoOMS.CloseBOOrder(userID, TradingSymbol,Exchange);
                            if (res)
                                WriteUniquelogs(TradingSymbol + " " + userID, "order closed successfully", MessageType.Informational);
                        }
                        else
                        {
                            res = AlgoOMS.CloseOrder(userID, TradingSymbol, Exchange);
                            if (res)
                                WriteUniquelogs(TradingSymbol + " " + userID, "order closed successfully", MessageType.Informational);
                        }

                    }
                    else if (value > 0 && TransactionType == Constants.TRANSACTION_TYPE_SELL)
                    {
                        WriteUniquelogs(TradingSymbol + " " + userID, "CloseOrder : Condition true for close order", MessageType.Informational);
                        if (AlgoOMS.getVariety(TradingSymbol,Exchange) == Constants.VARIETY_BO)
                        {
                            res = AlgoOMS.CloseBOOrder(userID, TradingSymbol,Exchange);
                            if (res)
                                WriteUniquelogs(TradingSymbol + " " + userID, "order closed successfully", MessageType.Informational);
                        }
                        else
                        {
                            res = AlgoOMS.CloseOrder(userID, TradingSymbol, Exchange);
                            if (res)
                                WriteUniquelogs(TradingSymbol + " " + userID, "order closed successfully", MessageType.Informational);
                        }
                    }
                }
                else
                {
                    WriteUniquelogs(TradingSymbol + " " + userID, "CloseOrder : dictionaryForPlacedOrder not contains symbol", MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                WriteUniquelogs(TradingSymbol + " " + userID, "CloseOrder : Exception " + e.Message, MessageType.Exception);
            }
            return res;
        }       

        public void addOrUpdateDictionary(string TradingSymbol, string TransactionType)
        {
            try
            {
                if (dictionaryForPlacedOrder.ContainsKey(TradingSymbol))
                {
                    if (TransactionType == Constants.TRANSACTION_TYPE_BUY)
                    {
                        dictionaryForPlacedOrder[TradingSymbol] = Math.Abs(dictionaryForPlacedOrder[TradingSymbol]) + 1;
                    }
                    else
                    {
                        dictionaryForPlacedOrder[TradingSymbol] = (Math.Abs(dictionaryForPlacedOrder[TradingSymbol]) + 1) * -1;
                    }
                    WriteUniquelogs(TradingSymbol + " " + userID, "addOrUpdateDictionary dictionary updated", MessageType.Informational);
                }
                else
                {
                    if (TransactionType == Constants.TRANSACTION_TYPE_BUY)
                    {
                        dictionaryForPlacedOrder.Add(TradingSymbol, 1);
                    }
                    else
                    {
                        dictionaryForPlacedOrder.Add(TradingSymbol, -1);
                    }
                    WriteUniquelogs(TradingSymbol + " " + userID, "addOrUpdateDictionary added in dictionary", MessageType.Informational);
                }
            }
            catch (Exception e)
            {
                logger.LogMessage("addOrUpdateDictionary Exception : " + e.Message, MessageType.Exception);
            }
        }

        public void changeTime(string tickCurrentTime)
        {
            logger.LogMessage("tickCurrentTime " + tickCurrentTime, MessageType.Informational);
            if ((TimeSpan.Parse(tickCurrentTime) >= TimeSpan.Parse(FirstTickEnd)))
            {
                logger.LogMessage("Before change Time - FirstTickEnd " + FirstTickEnd, MessageType.Informational);
                logger.LogMessage("Before change Time - End " + End, MessageType.Informational);
                DateTime dt1 = DateTime.ParseExact(FirstTickEnd, "HH:mm:ss", null);
                FirstTickEnd = dt1.AddSeconds(60 * Convert.ToInt32(Interval)).ToString("HH:mm:ss");
                End = dt1.AddSeconds(60 * ((Convert.ToInt32(Interval)) + 1)).ToString("HH:mm:ss");
                logger.LogMessage("After change Time - FirstTickEnd " + FirstTickEnd, MessageType.Informational);
                logger.LogMessage("After change Time - End " + End, MessageType.Informational);
            }
        }

        public bool checkClosedOrNot(string TradingSymbol)
        {
            WriteUniquelogs(TradingSymbol + " " + userID, "checkClosedOrNot In function", MessageType.Informational);
            double price = AlgoOMS.GetOpenPostionPrice(userID, TradingSymbol);
            if (price == 0)
            {
                WriteUniquelogs(TradingSymbol + " " + userID, "checkClosedOrNot already closed", MessageType.Informational);
                return false;
            }
            else
            {
                WriteUniquelogs(TradingSymbol + " " + userID, "checkClosedOrNot not closed", MessageType.Informational);
                return true;
            }
        }

        public void WriteUniquelogs(string symbol, string message, MessageType msgType)
        {
            try
            {
                SymbolLogs log = new SymbolLogs();
                //log = SymbolLogs.InstanceSymbolLogs;
                log.createFile(symbol);
                log.LogMessage(message, MessageType.Informational);
                log = null;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : " + e.Message);               
            }
        }

        //public void addIntoDataGridView(string TradingSymbol,string TransactionType,string Quantity,string Price)
        //{
        //    WriteUniquelogs(TradingSymbol + " " + userID, "addIntoDataGridView In function ", MessageType.Informational);
        //    try
        //    {
        //        mydt.Rows.Add(new object[] { TradingSymbol, TransactionType, Quantity, Price });
        //        ListForGridView.Add(TradingSymbol);
        //    }
        //    catch(Exception e)
        //    {
        //        WriteUniquelogs(TradingSymbol + " " + userID, "addIntoDataGridView Exception " + e.Message, MessageType.Informational);
        //    }
        //}

        //public void updateIntoDataGridView(string TradingSymbol,string TransactionType,string Quantity,string Price)
        //{
        //    WriteUniquelogs(TradingSymbol + " " + userID, "updateIntoDataGridView In function ", MessageType.Informational);
        //    try
        //    {
        //        foreach (DataRow dr in mydt.Rows)
        //        {
        //            if ((dr["Symbol"]).ToString() == TradingSymbol)
        //            {
        //                WriteUniquelogs(TradingSymbol + " " + userID, "updateIntoDataGridView Found symbol "+TradingSymbol, MessageType.Informational);
        //                dr["Transaction Type"] = TransactionType;
        //                dr["Quantity"] = Quantity;
        //                dr["Price"] = Price;
        //                break;
        //            }
        //        }
        //    }
        //    catch(Exception e)
        //    {
        //        WriteUniquelogs(TradingSymbol + " " + userID, "updateIntoDataGridView Exception "+e.Message, MessageType.Informational);
        //    }
        //}

        public void loadObject(form_autoTrading form_Auto)
        {
            this.form_Auto = form_Auto;
        }

        public void CloseAllOrder()
        {
            try
            {
                foreach (var symbolInfo in ListOfTradingSymbolsInfo)
                {
                    string TradingSymbol = symbolInfo.getSymbol();
                    string Exchange = symbolInfo.getExchange();
                    AlgoOMS.CloseOrder(userID, TradingSymbol, Exchange);
                }
                logger.LogMessage("CloseAllOrder : closed all order", MessageType.Informational);
            }
            catch (Exception e)
            {
                logger.LogMessage("CloseAllOrder : Exception Error Message = " + e.Message, MessageType.Exception);
            }
        }

    }
}
