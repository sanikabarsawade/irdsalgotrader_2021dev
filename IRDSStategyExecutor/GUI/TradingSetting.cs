﻿using IRDSAlgoOMS;
using IntellirichOHLSystem;
using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace IRDSStrategyExecutor.GUI
{
    public partial class TradingSetting : Form
    {
        [field: NonSerialized()]
        AlgoOMS AlgoOMSObj;
        //sanika::25-sep-2020::added for resume 
        [field: NonSerialized()]
        IntellirichOHLSystem.ReadSettings readSettings;
        //sanika::25-sep-2020::added for resume 
        [field: NonSerialized()]
        Logger logger;
        string path = System.IO.Directory.GetCurrentDirectory();

        string message = "";
        string title = "RealStockA1 Strategy";
        MessageBoxButtons buttons;
        string m_iniChoose = "";
        DialogResult dialog;
        INIFile iniObj = null;
        string userID = null;
        static string ConfigFilePath = System.AppDomain.CurrentDomain.BaseDirectory + "Configuration";
        public TradingSetting(Logger logger, AlgoOMS obj)
        {
            InitializeComponent();
            try
            {
                this.AlgoOMSObj = obj;
                this.logger = logger;
                readSettings = new IntellirichOHLSystem.ReadSettings(this.logger, this.AlgoOMSObj);
                FetchDetailsFromINI(path);
            }
            catch (Exception ex)
            {
                WriteUniquelogs("TradingSetting", "InitializeComponent : Exception Error Message = " + ex.Message, MessageType.Informational);
            }
        }

        private void FetchDetailsFromINI(string Path)
        {
            try
            {
                string filePath = path + @"\Configuration\Chan_Sandip.ini";
                if (File.Exists(filePath))
                {
                    if (readSettings == null)
                    {
                        readSettings = new IntellirichOHLSystem.ReadSettings(logger);
                    }
                    readSettings.readChan_SandipConfigFile(filePath);
                    txtStartTime.Value = Convert.ToDateTime(readSettings.startTime);
                    txtEndTime.Value = Convert.ToDateTime(readSettings.endTime);
                    txtTimeLimitToPlaceOrder.Value = Convert.ToDateTime(readSettings.TimeLimitToPlaceOrder);
                    txtTimeForExitAllOpenPosition.Value = Convert.ToDateTime(readSettings.TimeForExitAllOpenPosition);
                    txtPercentQuantity.Text = readSettings.m_PercentQuantity.ToString();
                    txttotalmoney.Text = readSettings.totalMoney.ToString();
                    txtBarInterval.Text = readSettings.m_BarInterval.ToString();
                    txtMaxStrategyOrderCount.Text = readSettings.m_MaxStrategyOrderCount.ToString();
                    txtindiviualCount.Text = readSettings.individualCount.ToString();
                    txtdaysForHistroricalData.Text = readSettings.m_DaysForHistroricalData.ToString();
                    txtPriceDiffPercent.Text = readSettings.priceDiffPercent.ToString();
                    txtPlacedOrderPriceDiffPercent.Text = readSettings.PlacedOrderPriceDiffPercent.ToString();
                    txtRiskPercentForStock.Text = readSettings.m_RiskPercentForStock.ToString();
                    txtRiskPercentForFuture.Text = readSettings.m_RiskPercentForFuture.ToString();
                    if (readSettings.isReadDataFromDB.ToString().ToLower() == "true")
                    {
                        TruereadDataFromDB.Checked = true;
                    }
                    else
                    {
                        FalsereadDataFromDB.Checked = true;
                    }

                    if (readSettings.m_AutoDownloadHistoricalData.ToString().ToLower() == "true")
                    {
                        TrueAutoDownloadHistoricalData.Checked = true;
                    }
                    else
                    {
                        FalseAutoDownloadHistoricalData.Checked = true;
                    }

                    if (readSettings.m_IsNeedToRunParallel.ToString().ToLower() == "true")
                    {
                        TrueIsNeedToParallel.Checked = true;
                    }
                    else
                    {
                        FalseIsNeedToParallel.Checked = true;
                    }
                    txtMaxLossValue.Text = readSettings.m_MaxLossValueForIndividualSymbol.ToString();
                    txtMaxLossPercent.Text = readSettings.m_MaxLossPercentForIndividualSymbol.ToString();
                    txtOverallLoss.Text = readSettings.m_OverallLoss.ToString();
                    txtOverallProfitAmt.Text = readSettings.m_OverallProfitAmt.ToString();
                    txtOverallProfitPercent.Text = readSettings.m_OverallProfitPercent.ToString();
                    txtTotalOpenPositions.Text = readSettings.m_TotalOpenPositions.ToString();
                    //Pratiksha::19-08-2021::New field StopLossChangeTime
                    txtStopLossChangeTime.Text = readSettings.m_StopLossChangeTime.ToString();
                    WriteUniquelogs("TradingSetting", "FetchDetailsFromINI : Reading details from INI file.", MessageType.Informational);
                }
                else
                {
                    Clearfields();
                    WriteUniquelogs("TradingSetting", "FetchDetailsFromINI : INI file not exist.", MessageType.Informational);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("TradingSetting", "FetchDetailsFromINI : Exception Error Message = " + ex.Message, MessageType.Informational);
            }
        }
        private void btnApply_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtStartTime.Text != "" && txtEndTime.Text != "" && txtTimeLimitToPlaceOrder.Text != "" && txtTimeForExitAllOpenPosition.Text != "" && txtPercentQuantity.Text != ""
                    && txttotalmoney.Text != "" && txtBarInterval.Text != "" && txtMaxStrategyOrderCount.Text != "" && txtindiviualCount.Text != "" &&
                    txtdaysForHistroricalData.Text != "" && txtPriceDiffPercent.Text != "" && txtPlacedOrderPriceDiffPercent.Text != "" && txtRiskPercentForStock.Text != "" &&
                    txtRiskPercentForFuture.Text != "" && txtMaxLossValue.Text != "" && txtMaxLossPercent.Text != "" && txtOverallLoss.Text != "" && txtOverallProfitAmt.Text != "" &&
                    txtOverallProfitPercent.Text != "" && txtTotalOpenPositions.Text != "" && txtStopLossChangeTime.Text != "")
                {
                    message = "Do you want to save the changes?";
                    buttons = MessageBoxButtons.YesNo;
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Warning);
                    if (dialog == DialogResult.Yes)
                    {
                        string filePath = ConfigFilePath + "\\Chan_Sandip.ini";
                        iniObj = new INIFile(filePath);
                        string[] key = new string[24];
                        string[] value = new string[24];
                        key[0] = "startTime";
                        key[1] = "endTime";
                        key[2] = "TimeLimitToPlaceOrder";
                        key[3] = "TimeForExitAllOpenPosition";
                        key[4] = "PercentQuantity";
                        key[5] = "totalmoney";
                        key[6] = "BarInterval";
                        key[7] = "maxStrategyOrderCount";
                        key[8] = "individualCount";
                        key[9] = "daysForHistroricalData";
                        key[10] = "PriceDiffPercent";
                        key[11] = "PlacedOrderPriceDiffPercent";
                        key[12] = "RiskPercentForStock";
                        key[13] = "RiskPercentForFuture";
                        key[14] = "readDataFromDB";
                        key[15] = "AutoDownloadHistoricalData";
                        key[16] = "IsNeedToParallel";
                        key[17] = "MaxLossValue";
                        key[18] = "MaxLossPercent";
                        key[19] = "OverallLoss";
                        key[20] = "OverallProfitAmt";
                        key[21] = "OverallProfitPercent";
                        key[22] = "TotalOpenPositions";
                        //Pratiksha::19-08-2021::New field StopLossChangeTime
                        key[23] = "StopLossChangeTime";

                        value[0] = txtStartTime.Text;
                        value[1] = txtEndTime.Text;
                        value[2] = txtTimeLimitToPlaceOrder.Text;
                        value[3] = txtTimeForExitAllOpenPosition.Text;
                        value[4] = txtPercentQuantity.Text;
                        value[5] = txttotalmoney.Text;
                        value[6] = txtBarInterval.Text;
                        value[7] = txtMaxStrategyOrderCount.Text;
                        value[8] = txtindiviualCount.Text;
                        value[9] = txtdaysForHistroricalData.Text;
                        value[10] = txtPriceDiffPercent.Text;
                        value[11] = txtPlacedOrderPriceDiffPercent.Text;
                        value[12] = txtRiskPercentForStock.Text;
                        value[13] = txtRiskPercentForFuture.Text;

                        if (TruereadDataFromDB.Checked == true)
                        {
                            value[14] = "true";
                        }
                        else if (FalsereadDataFromDB.Checked == true)
                        {
                            value[14] = "false";
                        }

                        if (TrueAutoDownloadHistoricalData.Checked == true)
                        {
                            value[15] = "true";
                        }
                        else if (FalseAutoDownloadHistoricalData.Checked == true)
                        {
                            value[15] = "false";
                        }

                        if (TrueIsNeedToParallel.Checked == true)
                        {
                            value[16] = "true";
                        }
                        else if (FalseIsNeedToParallel.Checked == true)
                        {
                            value[16] = "false";
                        }

                        value[17] = txtMaxLossValue.Text;
                        value[18] = txtMaxLossPercent.Text;
                        value[19] = txtOverallLoss.Text;
                        value[20] = txtOverallProfitAmt.Text;
                        value[21] = txtOverallProfitPercent.Text;
                        value[22] = txtTotalOpenPositions.Text;
                        //Pratiksha::19-08-2021::New field StopLossChangeTime
                        value[23] = txtStopLossChangeTime.Text;

                        for (int i = 0; i < 24; i++)
                        {
                            iniObj.clearTestingSymbol("SDATA", key[i], value[i], filePath);
                            WriteUniquelogs("TradingSetting", "btnApply_Click : " + key[i] + " = " + value[i], MessageType.Informational);
                        }
                        message = "Saved changes successfully!!!";
                        buttons = MessageBoxButtons.OK;
                        dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                        if (dialog == DialogResult.OK)
                        {
                            this.Close();
                        }

                        WriteUniquelogs("TradingSetting", "btnApply_Click :  Details saved in ini file.", MessageType.Informational);
                    }
                    else
                    {
                        this.Close();
                        WriteUniquelogs("TradingSetting", "btnApply_Click : You have clicked No, no data updated in trade setting.ini file.", MessageType.Informational);
                    }
                }
                else
                {
                    string parameter = "";
                    if (txtStartTime.Text == "") { parameter += "\nStart Time"; }
                    if (txtEndTime.Text == "") { parameter += "\nEnd Time"; }
                    if (txtTimeLimitToPlaceOrder.Text == "") { parameter += "\nTime Limit To Place Order"; }
                    if (txtTimeForExitAllOpenPosition.Text == "") { parameter += "\nTime For Exit All Open Position"; }
                    if (txtPercentQuantity.Text == "") { parameter += "\nPercent Quantity"; }
                    if (txttotalmoney.Text == "") { parameter += "\nTotal Money"; }
                    if (txtBarInterval.Text == "") { parameter += "\nBar Interval"; }
                    if (txtMaxStrategyOrderCount.Text == "") { parameter += "\nMax Strategy Order Count"; }
                    if (txtindiviualCount.Text == "") { parameter += "\nIndiviual Count"; }
                    if (txtPriceDiffPercent.Text == "") { parameter += "\nPrice Diff Percent"; }
                    if (txtPlacedOrderPriceDiffPercent.Text == "") { parameter += "\nPlaced Order Price Diff Percent"; }
                    if (txtRiskPercentForStock.Text == "") { parameter += "\nRisk Percent For Stock"; }
                    if (txtRiskPercentForFuture.Text == "") { parameter += "\nRisk Percent For Future"; }
                    if (txtMaxLossValue.Text == "") { parameter += "\nMax Loss Value"; }
                    if (txtMaxLossPercent.Text == "") { parameter += "\nMax Loss Percent"; }
                    if (txtOverallLoss.Text == "") { parameter += "\nOverall Loss"; }
                    if (txtOverallProfitAmt.Text == "") { parameter += "\nOverall Profit Amt"; }
                    if (txtOverallProfitPercent.Text == "") { parameter += "\nOverall Profit Percent"; }
                    if (txtTotalOpenPositions.Text == "") { parameter += "\nTotal Open Positions"; }
                    //Pratiksha::19-08-2021::New field StopLossChangeTime
                    if (txtStopLossChangeTime.Text == "") { parameter += "\nStopLoss Change Time"; }
                    if (txtdaysForHistroricalData.Text == "") { parameter += "\nDays For Histrorical Data"; }
                    message = "Please enter proper values for following field: " + parameter;
                    buttons = MessageBoxButtons.OK;
                    dialog = System.Windows.Forms.MessageBox.Show(message, title, buttons, MessageBoxIcon.Information);
                    WriteUniquelogs("TradingSetting", "btnApply_Click : Please enter proper values.", MessageType.Informational);
                }
            }
            catch (Exception ex)
            {
                WriteUniquelogs("TradingSetting", "btnApply_Click : Exception Error Message = " + ex.Message, MessageType.Informational);
            }
        }
        public void WriteUniquelogs(string symbol, string message, MessageType msgType)
        {
            try
            {
                SymbolLogs log = new SymbolLogs();
                log.createFile(symbol);
                log.LogMessage(message, MessageType.Informational);
                log = null;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception : " + e.Message);
                Trace.WriteLine("Exception : " + e.Message);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
                WriteUniquelogs("TradingSetting", "btnCancel_Click : Cancel click.", MessageType.Informational);
            }
            catch (Exception ex)
            {
                WriteUniquelogs("TradingSetting", "btnCancel_Click : Exception Error Message = " + ex.Message, MessageType.Informational);
            }
        }


        private void Clearfields()
        {
            try
            {
                txtStartTime.Text = null;
                txtEndTime.Text = null;
                txtTimeLimitToPlaceOrder.Text = null;
                txtTimeForExitAllOpenPosition.Text = null;
                txtPercentQuantity.Text = null;
                txttotalmoney.Text = null;
                txtBarInterval.Text = null;
                txtMaxStrategyOrderCount.Text = null;
                txtindiviualCount.Text = null;
                txtdaysForHistroricalData.Text = null;
                txtPriceDiffPercent.Text = null;
                txtPlacedOrderPriceDiffPercent.Text = null;
                txtRiskPercentForStock.Text = null;
                txtRiskPercentForFuture.Text = null;
                FalsereadDataFromDB.Checked = true;
                FalseAutoDownloadHistoricalData.Checked = true;
                FalseIsNeedToParallel.Checked = true;
                txtMaxLossValue.Text = null;
                txtMaxLossPercent.Text = null;
                txtOverallLoss.Text = null;
                txtOverallProfitAmt.Text = null;
                txtOverallProfitPercent.Text = null;
                txtTotalOpenPositions.Text = null;
                //Pratiksha::19-08-2021::New field StopLossChangeTime
                txtStopLossChangeTime.Text = null;
                WriteUniquelogs("TradingSetting", "Clearfields : All fields cleared.", MessageType.Informational);
            }
            catch (Exception ex)
            {
                WriteUniquelogs("TradingSetting", "Clearfields : Exception Error Message = " + ex.Message, MessageType.Informational);
            }
        }

        private void btnOpenSymbolSetting_Click(object sender, EventArgs e)
        {
            try
            {
                SymbolSetting symbolObj = new SymbolSetting(this.AlgoOMSObj, readSettings);
                symbolObj.ShowDialog();
            }
            catch (Exception ex)
            {
                WriteUniquelogs("TradingSetting", "btnOpenSymbolSetting_Click : Exception Error Message = " + ex.Message, MessageType.Informational);
            }
        }
    }
}

