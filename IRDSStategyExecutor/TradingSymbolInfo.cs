﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntellirichOHLSystem
{
    public class TradingSymbolInfo
    {
        public string SymbolWithExchange;
        public double Quantity;
        public decimal Volume;
        public double ProfitPercent;
        public double Stoploss;
        public string TradingSymbol;
        public string Exchange;
        public decimal Difference1;
        public decimal Difference2;
        public double BarCount;
        public int MaxPeriodEMA;
        public int MinPeriodEMA;
        public double HighLowPercent;
        //sanika::17-May-2021::added inputs symbolwise
        public double ProfitTrail;
        public double UpdatedProfitTrail;
        public double StopLoss;
        public double UpdatedStopLoss;
        //Pratiksha::17-08-2021::Added 2 new fields for percentage
        public double NiftyPercent;
        public double BankNiftyPercent;
        //sanika::4-Aug-2021::values remains same for after last symbol stored in storage
        public double High;
        public double Low;
        public double Middle;

        //sanika::18-Aug-2021::added dynamic
        public double StopLossChange;
        public double updatedStoplossChange;

        //public string TradingSymbol;
        //public string PeriodofBar;
        // public string ProfitPercent;
        //public string Stoploss;

        //public string NA1;
        //public string NA2;
        //public string NA3;
        //public string NA4;
        //public string NA5;
        public string getSymbol()
        {
            return TradingSymbol;
        }

        public double getQuantity()
        {
            return Quantity;
        }

        public decimal getVolume()
        {
            return Volume;
        }

        public double getProfitPercent()
        {
            return ProfitPercent;
        }

        public double getStopLoss()
        {
            return Stoploss;
        }

        public string getExchange()
        {
            return Exchange;
        }

        public decimal getDifference1()
        {
            return Difference1;
        }

        public decimal getDifference2()
        {
            return Difference2;
        }

        public double getBarCount()
        {
            return BarCount;
        }

        public int getMaxPeriodEMA()
        {
            return MaxPeriodEMA;
        }

        public int getMinPeriodEMA()
        {
            return MinPeriodEMA;
        }

        //sanika::19-oct-2020::return highlow percent
        public double getHighLowPercent()
        {
            return HighLowPercent;
        }

        //sanika::17-May-2021::added inputs symbolwise
        public double getProfitTrail()
        {
            return ProfitTrail;
        }
        public double getUpdatedProfitTrail()
        {
            return UpdatedProfitTrail;
        }
        public double getUpdatedStopLoss()
        {
            return UpdatedStopLoss;
        }
        //Pratiksha::17-08-2021::Added 2 new fields for percentage
        public double getNiftyPercent()
        {
            return NiftyPercent;
        }
        public double getBankNiftyPercent()
        {
            return BankNiftyPercent;
        }
        public double getStopLossChange()
        {
            return StopLossChange;
        }
        //sanika::18-Aug-2021::added dynamic
        public double getupdatedStoplossChange()
        {
            return updatedStoplossChange;
        }
    }
}
