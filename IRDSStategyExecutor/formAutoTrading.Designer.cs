﻿namespace IntellirichOHLSystem
{
    partial class form_autoTrading
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(form_autoTrading));
            this.btnLogin = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.btnHistorical = new System.Windows.Forms.Button();
            this.btnStopCloseOrder = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.EMAStrategy = new System.Windows.Forms.CheckBox();
            this.ChanBreakStrategy = new System.Windows.Forms.CheckBox();
            this.OpenHighStrategy = new System.Windows.Forms.CheckBox();
            this.btnModifyOrder = new System.Windows.Forms.Button();
            this.menuStripSettings = new System.Windows.Forms.MenuStrip();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tradingSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.symbolSettingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.menuStripSettings.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnLogin
            // 
            this.btnLogin.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogin.Location = new System.Drawing.Point(8, 25);
            this.btnLogin.Margin = new System.Windows.Forms.Padding(4);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(185, 47);
            this.btnLogin.TabIndex = 0;
            this.btnLogin.Text = "Login";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // btnStart
            // 
            this.btnStart.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStart.Location = new System.Drawing.Point(24, 173);
            this.btnStart.Margin = new System.Windows.Forms.Padding(4);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(185, 47);
            this.btnStart.TabIndex = 2;
            this.btnStart.Text = "Start Automation";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnStop
            // 
            this.btnStop.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStop.Location = new System.Drawing.Point(231, 173);
            this.btnStop.Margin = new System.Windows.Forms.Padding(4);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(185, 47);
            this.btnStop.TabIndex = 3;
            this.btnStop.Text = "Stop Automation";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.btnHistorical);
            this.groupBox2.Controls.Add(this.btnLogin);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(16, 54);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(645, 89);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(432, 25);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(185, 47);
            this.button1.TabIndex = 7;
            this.button1.Text = "Close";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnHistorical
            // 
            this.btnHistorical.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHistorical.Location = new System.Drawing.Point(215, 25);
            this.btnHistorical.Margin = new System.Windows.Forms.Padding(4);
            this.btnHistorical.Name = "btnHistorical";
            this.btnHistorical.Size = new System.Drawing.Size(185, 47);
            this.btnHistorical.TabIndex = 5;
            this.btnHistorical.Text = "Historical Data";
            this.btnHistorical.UseVisualStyleBackColor = true;
            this.btnHistorical.Click += new System.EventHandler(this.btnHistorical_Click);
            // 
            // btnStopCloseOrder
            // 
            this.btnStopCloseOrder.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStopCloseOrder.Location = new System.Drawing.Point(448, 173);
            this.btnStopCloseOrder.Margin = new System.Windows.Forms.Padding(4);
            this.btnStopCloseOrder.Name = "btnStopCloseOrder";
            this.btnStopCloseOrder.Size = new System.Drawing.Size(185, 47);
            this.btnStopCloseOrder.TabIndex = 4;
            this.btnStopCloseOrder.Text = "Stop & Close Orders";
            this.btnStopCloseOrder.UseVisualStyleBackColor = true;
            this.btnStopCloseOrder.Click += new System.EventHandler(this.btnStopCloseOrder_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.EMAStrategy);
            this.groupBox3.Controls.Add(this.ChanBreakStrategy);
            this.groupBox3.Controls.Add(this.OpenHighStrategy);
            this.groupBox3.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(16, 345);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox3.Size = new System.Drawing.Size(645, 114);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Strategy Selection";
            // 
            // EMAStrategy
            // 
            this.EMAStrategy.AutoSize = true;
            this.EMAStrategy.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EMAStrategy.Location = new System.Drawing.Point(457, 49);
            this.EMAStrategy.Margin = new System.Windows.Forms.Padding(4);
            this.EMAStrategy.Name = "EMAStrategy";
            this.EMAStrategy.Size = new System.Drawing.Size(131, 23);
            this.EMAStrategy.TabIndex = 4;
            this.EMAStrategy.Text = "EMA Strategy";
            this.EMAStrategy.UseVisualStyleBackColor = true;
            this.EMAStrategy.CheckedChanged += new System.EventHandler(this.EMAStrategy_CheckedChanged);
            // 
            // ChanBreakStrategy
            // 
            this.ChanBreakStrategy.AutoSize = true;
            this.ChanBreakStrategy.Checked = true;
            this.ChanBreakStrategy.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ChanBreakStrategy.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChanBreakStrategy.Location = new System.Drawing.Point(229, 49);
            this.ChanBreakStrategy.Margin = new System.Windows.Forms.Padding(4);
            this.ChanBreakStrategy.Name = "ChanBreakStrategy";
            this.ChanBreakStrategy.Size = new System.Drawing.Size(179, 23);
            this.ChanBreakStrategy.TabIndex = 3;
            this.ChanBreakStrategy.Text = "ChanBreak Strategy";
            this.ChanBreakStrategy.UseVisualStyleBackColor = true;
            this.ChanBreakStrategy.CheckedChanged += new System.EventHandler(this.ChanBreakStrategy_CheckedChanged_1);
            // 
            // OpenHighStrategy
            // 
            this.OpenHighStrategy.AutoSize = true;
            this.OpenHighStrategy.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OpenHighStrategy.Location = new System.Drawing.Point(20, 49);
            this.OpenHighStrategy.Margin = new System.Windows.Forms.Padding(4);
            this.OpenHighStrategy.Name = "OpenHighStrategy";
            this.OpenHighStrategy.Size = new System.Drawing.Size(175, 23);
            this.OpenHighStrategy.TabIndex = 2;
            this.OpenHighStrategy.Text = "Open-High Strategy";
            this.OpenHighStrategy.UseVisualStyleBackColor = true;
            this.OpenHighStrategy.CheckedChanged += new System.EventHandler(this.OpenHighStrategy_CheckedChanged);
            // 
            // btnModifyOrder
            // 
            this.btnModifyOrder.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModifyOrder.Location = new System.Drawing.Point(24, 250);
            this.btnModifyOrder.Margin = new System.Windows.Forms.Padding(4);
            this.btnModifyOrder.Name = "btnModifyOrder";
            this.btnModifyOrder.Size = new System.Drawing.Size(185, 47);
            this.btnModifyOrder.TabIndex = 7;
            this.btnModifyOrder.Text = "ModifyOrders";
            this.btnModifyOrder.UseVisualStyleBackColor = true;
            this.btnModifyOrder.Click += new System.EventHandler(this.btnModifyOrder_Click);
            // 
            // menuStripSettings
            // 
            this.menuStripSettings.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStripSettings.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStripSettings.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingsToolStripMenuItem});
            this.menuStripSettings.Location = new System.Drawing.Point(0, 0);
            this.menuStripSettings.Name = "menuStripSettings";
            this.menuStripSettings.Size = new System.Drawing.Size(686, 27);
            this.menuStripSettings.TabIndex = 8;
            this.menuStripSettings.Text = "Settings";
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tradingSettingsToolStripMenuItem,
            this.symbolSettingToolStripMenuItem});
            this.settingsToolStripMenuItem.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(81, 23);
            this.settingsToolStripMenuItem.Text = "Settings";
            // 
            // tradingSettingsToolStripMenuItem
            // 
            this.tradingSettingsToolStripMenuItem.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tradingSettingsToolStripMenuItem.Name = "tradingSettingsToolStripMenuItem";
            this.tradingSettingsToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.tradingSettingsToolStripMenuItem.Text = "Trading Setting";
            this.tradingSettingsToolStripMenuItem.Click += new System.EventHandler(this.tradingSettingsToolStripMenuItem_Click);
            // 
            // symbolSettingToolStripMenuItem
            // 
            this.symbolSettingToolStripMenuItem.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.symbolSettingToolStripMenuItem.Name = "symbolSettingToolStripMenuItem";
            this.symbolSettingToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.symbolSettingToolStripMenuItem.Text = "Symbol Setting";
            this.symbolSettingToolStripMenuItem.Click += new System.EventHandler(this.symbolSettingToolStripMenuItem_Click);
            // 
            // form_autoTrading
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(686, 490);
            this.Controls.Add(this.btnModifyOrder);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnStopCloseOrder);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.menuStripSettings);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStripSettings;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "form_autoTrading";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "IntellirichOHLSystem";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.form_autoTrading_FormClosing);
            this.Load += new System.EventHandler(this.form_autoTrading_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.menuStripSettings.ResumeLayout(false);
            this.menuStripSettings.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox ChanBreakStrategy;
        private System.Windows.Forms.CheckBox OpenHighStrategy;
        private System.Windows.Forms.Button btnStopCloseOrder;
        private System.Windows.Forms.Button btnHistorical;
        private System.Windows.Forms.CheckBox EMAStrategy;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnModifyOrder;
        private System.Windows.Forms.MenuStrip menuStripSettings;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tradingSettingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem symbolSettingToolStripMenuItem;
    }
}

